// This is NOT a valid TypeScript with type definitions.
// This is a public API signature, sorted alphabetically, used to guard breaking changes.

export declare class DbBlock {
    hash: string;
    predecessor: string;
    level: number;
    timestamp: Date;
}
export declare class InsertCommand<TEntity extends object> implements DbCommand<InsertResult> {
    readonly entityType: Constructor<TEntity>;
    readonly description: string;
    readonly entities: readonly DeepPartial<TEntity>[];
    readonly ignoreIfExists: boolean;
    constructor(entityType: Constructor<TEntity>, entities: ReadonlyArrayOrSingle<DeepPartial<TEntity>>, options?: {
        readonly ignoreIfExists?: boolean;
    });
    execute(dbManager: EntityManager): Promise<InsertResult>;
}
export declare const DAPPETIZER_DB_ENTITIES: readonly Constructor[];
export declare const DATABASE_PACKAGE_JSON_DI_TOKEN: InjectionToken<DatabasePackageJson>;
export declare const DB_DATA_SOURCE_PROVIDER_DI_TOKEN: InjectionToken<DbDataSourceProvider>;
export declare const DB_EXECUTOR_DI_TOKEN: InjectionToken<DbExecutor>;
export declare const bigNumberDbTransformer: DbValueTransformer<BigNumber, string>;
export declare const commonDbColumns: Readonly<{
    blockHash: Readonly<Omit<PrimaryColumnOptions, "nullable">>;
    chainId: Readonly<Omit<PrimaryColumnOptions, "nullable">>;
    operationGroupHash: Readonly<Omit<PrimaryColumnOptions, "nullable">>;
    address: Readonly<Omit<PrimaryColumnOptions, "nullable">>;
    bigNumber: Readonly<Omit<PrimaryColumnOptions, "nullable">>;
}>;
export declare const dappetizerNamingStrategy: NamingStrategyInterface;
export declare const versionInfo: Readonly<{
    readonly COMMIT_HASH: "";
    readonly VERSION: string;
}>;
export declare function canContainSchema(options: DataSourceOptions): options is DataSourceOptionsWithSchema;
export declare function dbColumnName<TEntity>(column: keyof TEntity & string): string;
export declare function dbTableName(entityType: Constructor): string;
export declare function getEntityName(dbEntity: DbEntity): string;
export declare function registerDappetizerWithDatabase(diContainer: DependencyContainer): void;
export declare function registerDbInitializer(diContainer: DependencyContainer, dbInitializerProvider: Provider<DbInitializer>): void;
export declare function resolveSchema(options: DataSourceOptions): string | null;
export declare function resolveSchema(options: DataSourceOptionsWithSchema): string;
export interface DappetizerConfigUsingDb extends DappetizerConfig {
    database: StrictOmit<DataSourceOptions, 'entities'>;
    hasura?: HasuraDappetizerConfig;
}
export interface DbCommand<TResult = void> {
    readonly description: string;
    execute(dbManager: EntityManager): Promise<TResult>;
}
export interface DbContext {
    readonly transaction: EntityManager;
    readonly transactionRunner: QueryRunner;
    readonly globalDataSource: DataSource;
}
export interface DbDataSourceProvider {
    getDataSource(): Promise<DataSource>;
}
export interface DbExecutor {
    execute<TResult>(dbManager: EntityManager, command: DbCommand<TResult>): Promise<TResult>;
}
export interface DbInitializer {
    readonly name: string;
    readonly order?: number;
    beforeSynchronization?(dataSource: DataSource): AsyncOrSync<void>;
    afterSynchronization?(dataSource: DataSource): AsyncOrSync<void>;
}
export interface DbValueTransformer<TAppValue, TDbValue> extends ValueTransformer {
    to(appValue: Nullish<TAppValue>): Nullish<TDbValue>;
    from(dbValue: Nullish<TDbValue>): Nullish<TAppValue>;
}
export interface HasuraDappetizerConfig {
    dropExistingTracking?: boolean;
    autotrackEntities?: boolean;
    url?: string;
    adminSecret?: string;
    source?: string;
    allowAggregations?: boolean;
    selectLimit?: number;
    namingStyle?: 'noTransformation' | 'snakeCase';
    customMappings?: {
        tableName: string;
        graphQLTypeName: string;
        graphQLFieldName: string;
        graphQLPluralFieldName?: string;
        graphQLAggregateFieldName?: string;
        columns?: {
            columnName: string;
            graphQLName?: string;
            hidden?: boolean;
        }[];
        relationships?: {
            name: string;
            columns: string[];
            referencedTableName: string;
            referencedColumns: string[];
            type?: 'oneToMany' | 'manyToOne';
        }[];
    }[];
    autotrackedMappings?: {
        entityName: string;
        graphQLTypeName?: string;
        graphQLFieldName?: string;
        graphQLPluralFieldName?: string;
        graphQLAggregateFieldName?: string;
        skipped?: boolean;
        columns?: {
            propertyName: string;
            graphQLName?: string;
            hidden?: boolean;
        }[];
        relationships?: {
            name: string;
            columns: string[];
            referencedTableName: string;
            referencedColumns: string[];
            type?: 'oneToMany' | 'manyToOne';
        }[];
    }[];
}
export interface IndexerModuleUsingDb<TContextData = unknown> extends IndexerModule<DbContext, TContextData> {
    readonly dbEntities: readonly DbEntity[];
}
export type BlockDataIndexerUsingDb<TContextData = unknown> = BlockDataIndexer<DbContext, TContextData>;
export type ContractIndexerUsingDb<TContextData = unknown, TContractData = unknown> = ContractIndexer<DbContext, TContextData, TContractData>;
export type DataSourceOptionsWithSchema = CockroachConnectionOptions | OracleConnectionOptions | PostgresConnectionOptions | SapConnectionOptions | SpannerConnectionOptions | SqlServerConnectionOptions;
export type DbColumnOptions = Readonly<StrictOmit<PrimaryColumnOptions, 'nullable'>>;
export type DbEntity = Function | string | EntitySchema;
export type IndexerModuleUsingDbFactory<TContextData = unknown> = IndexerModuleFactory<DbContext, TContextData>;
export type IndexingCycleHandlerUsingDb<TContextData = unknown> = IndexingCycleHandler<DbContext, TContextData>;
export type SmartRollupIndexerUsingDb<TContextData = unknown, TRollupData = unknown> = SmartRollupIndexer<DbContext, TContextData, TRollupData>;