// This is NOT a valid TypeScript with type definitions.
// This is a public API signature, sorted alphabetically, used to guard breaking changes.

export declare class MichelsonSchema {
    #private;
    readonly michelson: Michelson;
    readonly generated: DeepReadonly<taquitoMichelson.TokenSchema>;
    constructor(michelson: Michelson);
    execute<T>(valueMichelson: Michelson): T;
}
export declare const CONTRACT_PROVIDER_DI_TOKEN: InjectionToken<ContractProvider>;
export declare const INDEXER_MODULES_DI_TOKEN: InjectionToken<IndexerModule<unknown>>;
export declare const INDEXER_MODULE_EXPORT_NAME = "indexerModule";
export declare const INDEXER_PACKAGE_JSON_DI_TOKEN: InjectionToken<IndexerPackageJson>;
export declare const SELECTIVE_INDEXING_CONFIG_DI_TOKEN: InjectionToken<SelectiveIndexingConfig>;
export declare const SMART_ROLLUP_PROVIDER_DI_TOKEN: InjectionToken<SmartRollupProvider>;
export declare const TAQUITO_TEZOS_TOOLKIT_DI_TOKEN: InjectionToken<taquito.TezosToolkit>;
export declare const michelsonDataPrims: Readonly<{
    UNIT: "Unit";
    TRUE: "True";
    FALSE: "False";
    PAIR: "Pair";
    LEFT: "Left";
    RIGHT: "Right";
    SOME: "Some";
    NONE: "None";
    ELT: "Elt";
}>;
export declare const michelsonInstructionPrims: Readonly<{
    EMIT: "EMIT";
}>;
export declare const michelsonTypePrims: Readonly<{
    ADDRESS: "address";
    BIG_MAP: "big_map";
    BLS12_381_FR: "bls12_381_fr";
    BLS12_381_G1: "bls12_381_g1";
    BLS12_381_G2: "bls12_381_g2";
    BOOL: "bool";
    BYTES: "bytes";
    CHEST: "chest";
    CHEST_KEY: "chest_key";
    CHAIN_ID: "chain_id";
    CODE: "code";
    CONSTANT: "constant";
    CONTRACT: "contract";
    INT: "int";
    KEY: "key";
    KEY_HASH: "key_hash";
    LAMBDA: "lambda";
    LIST: "list";
    MAP: "map";
    MUTEZ: "mutez";
    NAT: "nat";
    NEVER: "never";
    OPERATION: "operation";
    OPTION: "option";
    OR: "or";
    PAIR: "pair";
    PARAMETER: "parameter";
    SAPLING_STATE: "sapling_state";
    SAPLING_TRANSACTION: "sapling_transaction";
    SAPLING_TRANSACTION_DEPRECATED: "sapling_transaction_deprecated";
    SET: "set";
    SIGNATURE: "signature";
    STORAGE: "storage";
    STRING: "string";
    TICKET: "ticket";
    TICKET_DEPRECATED: "ticket_deprecated";
    TIMESTAMP: "timestamp";
    TX_ROLLUP_L2_ADDRESS: "tx_rollup_l2_address";
    UNIT: "unit";
    VIEW: "view";
}>;
export declare const versionInfo: Readonly<{
    readonly COMMIT_HASH: "";
    readonly VERSION: string;
}>;
export declare enum BalanceUpdateCategory {
    BakingBonuses = "BakingBonuses",
    BakingRewards = "BakingRewards",
    BlockFees = "BlockFees",
    Bonds = "Bonds",
    Bootstrap = "Bootstrap",
    Burned = "Burned",
    Commitment = "Commitment",
    Deposits = "Deposits",
    DoubleSigningEvidenceRewards = "DoubleSigningEvidenceRewards",
    EndorsingRewards = "EndorsingRewards",
    Invoice = "Invoice",
    LegacyDeposits = "LegacyDeposits",
    LegacyFees = "LegacyFees",
    LegacyRewards = "LegacyRewards",
    LostEndorsingRewards = "LostEndorsingRewards",
    Minted = "Minted",
    NonceRevelationRewards = "NonceRevelationRewards",
    Punishments = "Punishments",
    StorageFees = "StorageFees",
    Subsidy = "Subsidy",
}
export declare enum BalanceUpdateKind {
    Accumulator = "Accumulator",
    Burned = "Burned",
    Commitment = "Commitment",
    Contract = "Contract",
    Freezer = "Freezer",
    Minted = "Minted",
}
export declare enum BalanceUpdateOrigin {
}
export declare enum Ballot {
    Nay = "Nay",
    Pass = "Pass",
    Yay = "Yay"
}
export declare enum BigMapDiffAction {
    Alloc = "Alloc",
    Copy = "Copy",
    Remove = "Remove",
    Update = "Update"
}
export declare enum OperationKind {
    ActivateAccount = "ActivateAccount",
    Ballot = "Ballot",
    Delegation = "Delegation",
    DoubleBakingEvidence = "DoubleBakingEvidence",
    DoubleEndorsementEvidence = "DoubleEndorsementEvidence",
    DoublePreendorsementEvidence = "DoublePreendorsementEvidence",
    DrainDelegate = "DrainDelegate",
    Endorsement = "Endorsement",
    Event = "Event",
    IncreasePaidStorage = "IncreasePaidStorage",
    Origination = "Origination",
    Preendorsement = "Preendorsement",
    Proposals = "Proposals",
    RegisterGlobalConstant = "RegisterGlobalConstant",
    Reveal = "Reveal",
    SeedNonceRevelation = "SeedNonceRevelation",
    SetDepositsLimit = "SetDepositsLimit",
    SmartRollupAddMessages = "SmartRollupAddMessages",
    SmartRollupCement = "SmartRollupCement",
    SmartRollupExecuteOutboxMessage = "SmartRollupExecuteOutboxMessage",
    SmartRollupOriginate = "SmartRollupOriginate",
    SmartRollupPublish = "SmartRollupPublish",
    SmartRollupRecoverBond = "SmartRollupRecoverBond",
    SmartRollupRefute = "SmartRollupRefute",
    SmartRollupTimeout = "SmartRollupTimeout",
    Transaction = "Transaction",
    TransferTicket = "TransferTicket",
    UpdateConsensusKey = "UpdateConsensusKey",
    VdfRevelation = "VdfRevelation"
}
export declare enum OperationResultStatus {
    Applied = "Applied",
    Failed = "Failed",
    Skipped = "Skipped",
    Backtracked = "Backtracked"
}
export declare enum SmartRollupGameStatusKind {
    Draw = "Draw",
    Loser = "Loser",
    Ongoing = "Ongoing"
}
export declare enum SmartRollupGameStatusLoserReason {
    ConflictResolved = "ConflictResolved",
    Timeout = "Timeout"
}
export declare enum SmartRollupRefutationKind {
    Start = "Start",
    Move = "Move"
}
export declare function cleanAnnot(annot: Nullish<string>): string | null;
export declare function createPrim(prim: string, args: readonly Michelson[] | undefined, annots: readonly string[] | undefined): MichelsonPrim;
export declare function flatRightCombPairs(michelson: Michelson): Michelson;
export declare function getSingleAnnot(michelsonPrim: MichelsonPrim): string | null;
export declare function getSingleAnnotCleaned(michelsonPrim: MichelsonPrim): string | null;
export declare function getUnderlyingMichelson(schema: taquitoMichelson.Schema | taquitoMichelson.ParameterSchema): Michelson;
export declare function isApplied<TOperation extends OperationWithResult>(operation: TOperation): operation is Applied<TOperation>;
export declare function isInternalOperationWithBalanceUpdatesInResult(operation: InternalOperation): operation is InternalOperationWithBalanceUpdatesInResult;
export declare function isMainOperationWithBalanceUpdatesInMetadata(operation: MainOperation): operation is MainOperationWithBalanceUpdatesInMetadata;
export declare function isMainOperationWithBalanceUpdatesInResult(operation: MainOperation): operation is MainOperationWithBalanceUpdatesInResult;
export declare function isMainOperationWithResult(operation: MainOperation): operation is MainOperationWithResult;
export declare function isPrim(michelson: Michelson): michelson is MichelsonPrim;
export declare function isRawValue(michelson: Michelson): michelson is MichelsonRawValue;
export declare function isSmartRollupOperation(operation: MainOperation): operation is SmartRollupOperation;
export declare function loadDappetizerNetworkConfigs(dirPath: string): DappetizerConfig['networks'];
export declare function registerBlockLevelIndexingListener(diContainer: DependencyContainer, listenerProvider: Provider<BlockLevelIndexingListener>): void;
export declare function registerDappetizerIndexer(diContainer: DependencyContainer): void;
export declare function registerExplicitBlocksToIndex(diContainer: DependencyContainer, explicitBlocksToIndexProvider: Provider<ExplicitBlocksToIndex>): void;
export declare function registerIndexerDatabaseHandler(diContainer: DependencyContainer, handlerProvider: Provider<IndexerDatabaseHandler<unknown>>): void;
export declare function registerPartialIndexerModuleVerifier(diContainer: DependencyContainer, partialIndexerModuleVerifier: Provider<PartialIndexerModuleVerifier>): void;
export interface ActivateAccountOperation extends EntityToIndex<TaquitoRpcActivateAccountOperation> {
    readonly kind: OperationKind.ActivateAccount;
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly pkh: string;
    readonly secret: string;
}
export interface AppliedDelegationResult extends AppliedOperationResult<TaquitoRpcDelegationResult> {
}
export interface AppliedEventResult extends AppliedOperationResult<TaquitoRpcEventResult> {
}
export interface AppliedIncreasePaidStorageResult extends AppliedOperationResult<TaquitoRpcIncreasePaidStorageResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
}
export interface AppliedOperationResult<TRpcData = object> extends EntityToIndex<TRpcData> {
    readonly status: OperationResultStatus.Applied;
    readonly consumedGas: BigNumber | null;
    readonly consumedMilligas: BigNumber | null;
}
export interface AppliedOriginationResult extends AppliedOperationResult<TaquitoRpcOriginationResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly bigMapDiffs: readonly LazyBigMapDiff[];
    readonly originatedContract: LazyContract;
    readonly storageSize: BigNumber | null;
    readonly paidStorageSizeDiff: BigNumber | null;
    getInitialStorage(): Promise<LazyMichelsonValue>;
}
export interface AppliedRegisterGlobalConstantResult extends AppliedOperationResult<TaquitoRpcRegisterGlobalConstantResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly globalAddress: string;
    readonly storageSize: BigNumber | null;
}
export interface AppliedRevealResult extends AppliedOperationResult<TaquitoRpcRevealResult> {
}
export interface AppliedSetDepositsLimitResult extends AppliedOperationResult<TaquitoRpcSetDepositsLimitResult> {
}
export interface AppliedSmartRollupAddMessagesResult extends AppliedOperationResult<TaquitoRpcSmartRollupAddMessagesResult> {
}
export interface AppliedSmartRollupCementResult extends AppliedOperationResult<TaquitoRpcSmartRollupCementResult> {
    readonly inboxLevel: number;
    readonly commitmentHash: string;
}
export interface AppliedSmartRollupExecuteOutboxMessageResult extends AppliedOperationResult<TaquitoRpcSmartRollupExecuteOutboxMessageResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly paidStorageSizeDiff: BigNumber | null;
    readonly ticketUpdates: readonly TicketUpdate[];
}
export interface AppliedSmartRollupOriginateResult extends AppliedOperationResult<TaquitoRpcSmartRollupOriginateResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly genesisCommitmentHash: string;
    readonly originatedRollup: SmartRollup;
    readonly size: BigNumber;
}
export interface AppliedSmartRollupPublishResult extends AppliedOperationResult<TaquitoRpcSmartRollupPublishResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly publishedAtLevel: number;
    readonly stakedHash: string;
}
export interface AppliedSmartRollupRecoverBondResult extends AppliedOperationResult<TaquitoRpcSmartRollupRecoverBondResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
}
export interface AppliedSmartRollupRefuteResult extends AppliedOperationResult<TaquitoRpcSmartRollupRefuteResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly gameStatus: SmartRollupGameStatus;
}
export interface AppliedSmartRollupTimeoutResult extends AppliedOperationResult<TaquitoRpcSmartRollupTimeoutResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly gameStatus: SmartRollupGameStatus;
}
export interface AppliedTransactionResult extends AppliedOperationResult<TaquitoRpcTransactionResult> {
    readonly allocatedDestinationContract: boolean;
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly bigMapDiffs: readonly LazyBigMapDiff[];
    readonly paidStorageSizeDiff: BigNumber | null;
    readonly storageChange: LazyStorageChange | null;
    readonly storageSize: BigNumber | null;
    readonly ticketHash: string | null;
    readonly ticketReceipts: readonly TicketUpdate[];
    readonly ticketUpdates: readonly TicketUpdate[];
}
export interface AppliedTransferTicketResult extends AppliedOperationResult<TaquitoRpcTransferTicketResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly paidStorageSizeDiff: BigNumber | null;
    readonly ticketUpdates: readonly TicketUpdate[];
}
export interface AppliedUpdateConsensusKeyResult extends AppliedOperationResult<TaquitoRpcUpdateConsensusKeyResult> {
}
export interface BacktrackedOperationResult<TRpcData = object> extends EntityToIndex<TRpcData> {
    readonly status: OperationResultStatus.Backtracked;
    readonly consumedGas: BigNumber | null;
    readonly consumedMilligas: BigNumber | null;
    readonly errors: readonly OperationError[];
}
export interface BalanceUpdateAccumulator extends BaseBalanceUpdateSimple {
    readonly kind: BalanceUpdateKind.Accumulator;
    readonly category: BalanceUpdateCategory.BlockFees;
}
export interface BalanceUpdateBaker {
    readonly type: 'Baker' | 'BakerEdge';
    readonly bakerAddress: string;
    readonly contractAddress: null;
    readonly delegateAddress: null;
}
export interface BalanceUpdateBurned extends BaseBalanceUpdateSimple {
    readonly kind: BalanceUpdateKind.Burned;
    readonly category: BalanceUpdateCategory.Burned | BalanceUpdateCategory.Punishments | BalanceUpdateCategory.SmartRollupRefutationPunishments | BalanceUpdateCategory.StorageFees;
}
export interface BalanceUpdateBurnedLostRewards extends BaseBalanceUpdate {
    readonly kind: BalanceUpdateKind.Burned;
    readonly category: BalanceUpdateCategory.LostEndorsingRewards | BalanceUpdateCategory.LostAttestingRewards;
    readonly origin: BalanceUpdateOrigin;
    readonly delegateAddress: string;
    readonly participation: boolean;
    readonly revelation: boolean;
    readonly bondRollupAddress: null;
    readonly committer: null;
    readonly contractAddress: null;
    readonly cycle: null;
    readonly delegatorContractAddress: null;
    readonly staker: null;
}
export interface BalanceUpdateCommitment extends BaseBalanceUpdate {
    readonly origin: BalanceUpdateOrigin;
    readonly kind: BalanceUpdateKind.Commitment;
    readonly category: BalanceUpdateCategory.Commitment;
    readonly committer: string;
    readonly bondRollupAddress: null;
    readonly contractAddress: null;
    readonly cycle: null;
    readonly delegateAddress: null;
    readonly delegatorContractAddress: null;
    readonly participation: null;
    readonly revelation: null;
    readonly staker: null;
}
export interface BalanceUpdateContract extends BaseBalanceUpdate {
    readonly kind: BalanceUpdateKind.Contract;
    readonly category: null;
    readonly contractAddress: string;
    readonly bondRollupAddress: null;
    readonly committer: null;
    readonly cycle: null;
    readonly delegateAddress: null;
    readonly delegatorContractAddress: null;
    readonly participation: null;
    readonly revelation: null;
    readonly staker: null;
}
export interface BalanceUpdateFreezerBonds extends BaseBalanceUpdate {
    readonly kind: BalanceUpdateKind.Freezer;
    readonly category: BalanceUpdateCategory.Bonds;
    readonly origin: BalanceUpdateOrigin;
    readonly bondRollupAddress: string;
    readonly contractAddress: string;
    readonly committer: null;
    readonly cycle: null;
    readonly delegateAddress: null;
    readonly delegatorContractAddress: null;
    readonly participation: null;
    readonly revelation: null;
    readonly staker: null;
}
export interface BalanceUpdateFreezerDeposits extends BaseBalanceUpdateFreezerDeposits {
    readonly category: BalanceUpdateCategory.Deposits;
    readonly staker: BalanceUpdateFrozenStaker;
    readonly cycle: null;
}
export interface BalanceUpdateFreezerLegacy extends BaseBalanceUpdate {
    readonly kind: BalanceUpdateKind.Freezer;
    readonly category: BalanceUpdateCategory.LegacyDeposits | BalanceUpdateCategory.LegacyFees | BalanceUpdateCategory.LegacyRewards;
    readonly origin: StrictExclude<BalanceUpdateOrigin, BalanceUpdateOrigin.DelayedOperation> | null;
    readonly cycle: number | null;
    readonly delegateAddress: string;
    readonly bondRollupAddress: null;
    readonly committer: null;
    readonly contractAddress: null;
    readonly delayedOperationHash: null;
    readonly delegatorContractAddress: null;
    readonly participation: null;
    readonly revelation: null;
    readonly staker: null;
}
export interface BalanceUpdateFreezerUnstakedDeposits extends BaseBalanceUpdateFreezerDeposits {
    readonly category: BalanceUpdateCategory.UnstakedDeposits;
    readonly cycle: number;
    readonly staker: BalanceUpdateStaker;
}
export interface BalanceUpdateMinted extends BaseBalanceUpdateSimple {
    readonly kind: BalanceUpdateKind.Minted;
    readonly category: BalanceUpdateCategory.AttestingRewards | BalanceUpdateCategory.BakingBonuses | BalanceUpdateCategory.BakingRewards | BalanceUpdateCategory.Bootstrap | BalanceUpdateCategory.Commitment | BalanceUpdateCategory.DoubleSigningEvidenceRewards | BalanceUpdateCategory.EndorsingRewards | BalanceUpdateCategory.Invoice | BalanceUpdateCategory.Minted | BalanceUpdateCategory.NonceRevelationRewards | BalanceUpdateCategory.SmartRollupRefutationRewards | BalanceUpdateCategory.Subsidy;
}
export interface BalanceUpdateSharedStaker {
    readonly type: 'Shared';
    readonly delegateAddress: string;
    readonly bakerAddress: null;
    readonly contractAddress: null;
}
export interface BalanceUpdateSingleStaker {
    readonly type: 'Single';
    readonly contractAddress: string;
    readonly delegateAddress: string;
    readonly bakerAddress: null;
}
export interface BalanceUpdateStakingDelegateDenominator extends BaseBalanceUpdateStaking {
    readonly category: BalanceUpdateCategory.StakingDelegateDenominator;
    readonly delegateAddress: string;
    readonly delegatorContractAddress: null;
}
export interface BalanceUpdateStakingDelegatorNumerator extends BaseBalanceUpdateStaking {
    readonly category: BalanceUpdateCategory.StakingDelegatorNumerator;
    readonly delegatorContractAddress: string;
    readonly delegateAddress: null;
}
export interface BallotOperation extends EntityToIndex<TaquitoRpcBallotOperation> {
    readonly kind: OperationKind.Ballot;
    readonly sourceAddress: string;
    readonly period: number;
    readonly proposal: string;
    readonly ballot: Ballot;
}
export interface BaseBalanceUpdate extends EntityToIndex<TaquitoRpcBalanceUpdate> {
    readonly change: BigNumber;
    readonly origin: BalanceUpdateOrigin | null;
    readonly delayedOperationHash: string | null;
}
export interface BaseBalanceUpdateFreezerDeposits extends BaseBalanceUpdate {
    readonly kind: BalanceUpdateKind.Freezer;
    readonly origin: BalanceUpdateOrigin;
    readonly bondRollupAddress: null;
    readonly committer: null;
    readonly contractAddress: null;
    readonly delegateAddress: null;
    readonly delegatorContractAddress: null;
    readonly participation: null;
    readonly revelation: null;
}
export interface BaseBalanceUpdateSimple extends BaseBalanceUpdate {
    readonly origin: BalanceUpdateOrigin;
    readonly bondRollupAddress: null;
    readonly committer: null;
    readonly contractAddress: null;
    readonly cycle: null;
    readonly delegateAddress: null;
    readonly delegatorContractAddress: null;
    readonly participation: null;
    readonly revelation: null;
    readonly staker: null;
}
export interface BaseBalanceUpdateStaking extends BaseBalanceUpdate {
    readonly kind: BalanceUpdateKind.Staking;
    readonly origin: BalanceUpdateOrigin;
    readonly bondRollupAddress: null;
    readonly committer: null;
    readonly contractAddress: null;
    readonly cycle: null;
    readonly participation: null;
    readonly revelation: null;
    readonly staker: null;
}
export interface BaseBigMapDiff extends EntityToIndex<TaquitoRpcBigMapDiff> {
}
export interface BaseInternalOperation extends EntityToIndex<TaquitoRpcInternalOperation> {
    readonly kind: OperationKind;
    readonly isInternal: true;
    readonly nonce: number;
    readonly sourceAddress: string;
    readonly sourceContract: LazyContract | null;
    readonly source: LazyContract | SmartRollup;
}
export interface BaseMainOperationWithResult<TRpcData = object> extends EntityToIndex<TRpcData> {
    readonly kind: OperationKind;
    readonly isInternal: false;
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly counter: BigNumber;
    readonly fee: BigNumber;
    readonly gasLimit: BigNumber;
    readonly internalOperations: readonly InternalOperation[];
    readonly sourceAddress: string;
    readonly storageLimit: BigNumber;
}
export interface BigMapAlloc extends BaseBigMapDiff {
    readonly action: BigMapDiffAction.Alloc;
    readonly bigMap: BigMapInfo | null;
    readonly keyType: MichelsonSchema;
    readonly valueType: MichelsonSchema;
    readonly destinationBigMap: null;
    readonly key: null;
    readonly keyHash: null;
    readonly sourceBigMap: null;
    readonly value: null;
}
export interface BigMapCopy extends BaseBigMapDiff {
    readonly action: BigMapDiffAction.Copy;
    readonly sourceBigMap: BigMapInfo | null;
    readonly destinationBigMap: BigMapInfo | null;
    readonly bigMap: null;
    readonly key: null;
    readonly keyHash: null;
    readonly keyType: null;
    readonly value: null;
    readonly valueType: null;
}
export interface BigMapDiffIndexingContext<TData = unknown, TContractData = unknown> extends ContractIndexingContext<TData, TContractData> {
    readonly bigMapDiff: BigMapDiff;
}
export interface BigMapInfo {
    readonly lastKnownId: BigNumber;
    readonly name: string | null;
    readonly path: readonly string[];
    readonly pathStr: string;
    readonly keySchema: MichelsonSchema;
    readonly valueSchema: MichelsonSchema;
}
export interface BigMapRemove extends BaseBigMapDiff {
    readonly action: BigMapDiffAction.Remove;
    readonly bigMap: BigMapInfo | null;
    readonly destinationBigMap: null;
    readonly key: null;
    readonly keyHash: null;
    readonly keyType: null;
    readonly sourceBigMap: null;
    readonly value: null;
    readonly valueType: null;
}
export interface BigMapUpdate extends BaseBigMapDiff {
    readonly action: BigMapDiffAction.Update;
    readonly bigMap: BigMapInfo | null;
    readonly keyHash: string;
    readonly key: LazyMichelsonValue | null;
    readonly value: LazyMichelsonValue | null;
    readonly destinationBigMap: null;
    readonly keyType: null;
    readonly sourceBigMap: null;
    readonly valueType: null;
}
export interface BigMapUpdateIndexingContext<TData = unknown, TContractData = unknown> extends BigMapDiffIndexingContext<TData, TContractData> {
    readonly bigMapDiff: BigMapUpdate;
}
export interface Block extends EntityToIndex<TaquitoRpcBlock> {
    readonly level: number;
    readonly predecessorBlockHash: string;
    readonly proto: number;
    readonly timestamp: Date;
    readonly chainId: string;
    readonly hash: string;
    readonly protocol: string;
    readonly metadataBalanceUpdates: readonly BalanceUpdate[];
    readonly operationGroups: readonly OperationGroup[];
}
export interface BlockDataIndexer<TDbContext, TContextData = unknown> {
    readonly name?: string;
    indexBlock?(block: Block, dbContext: TDbContext, indexingContext: BlockIndexingContext<TContextData>): void | PromiseLike<void>;
    indexOperationGroup?(operationGroup: OperationGroup, dbContext: TDbContext, indexingContext: OperationGroupIndexingContext<TContextData>): void | PromiseLike<void>;
    indexMainOperation?(mainOperation: MainOperation, dbContext: TDbContext, indexingContext: MainOperationIndexingContext<TContextData>): void | PromiseLike<void>;
    indexOperationWithResult?(operationWithResult: OperationWithResult, dbContext: TDbContext, indexingContext: OperationWithResultIndexingContext<TContextData>): void | PromiseLike<void>;
    indexBalanceUpdate?(balanceUpdate: BalanceUpdate, dbContext: TDbContext, indexingContext: BalanceUpdateIndexingContext<TContextData>): void | PromiseLike<void>;
}
export interface BlockHeader extends EntityToIndex<TaquitoRpcBlockHeader> {
    readonly level: number;
    readonly predecessorBlockHash: string;
    readonly proto: number;
    readonly timestamp: Date;
}
export interface BlockIndexingContext<TData = unknown> {
    readonly data: TData;
}
export interface BlockLevelIndexingListener {
    onToBlockLevelReached(): AsyncOrSync<void>;
}
export interface BlockMetadataBalanceUpdateIndexingContext<TData = unknown> extends OperationGroupIndexingContext<TData> {
    readonly balanceUpdateSource: 'BlockMetadata';
    readonly operationGroup: null;
    readonly mainOperation: null;
    readonly internalOperation: null;
}
export interface BlockRange {
    readonly fromLevel: number;
    readonly toLevel: number;
}
export interface Contract {
    readonly address: string;
    readonly config: ContractConfig;
    readonly abstraction: ContractAbstraction;
    readonly bigMaps: readonly BigMapInfo[];
    readonly parameterSchemas: ContractParameterSchemas;
    readonly storageSchema: MichelsonSchema;
    readonly events: readonly ContractEventInfo[];
}
export interface ContractConfig {
    readonly toBeIndexed: boolean;
    readonly name: string | null;
}
export interface ContractEventInfo {
    readonly tag: string | null;
    readonly schema: MichelsonSchema | null;
}
export interface ContractIndexer<TDbContext, TContextData = unknown, TContractData = unknown> {
    readonly name?: string;
    shouldIndex?(lazyContract: LazyContract, dbContext: TDbContext): TContractData | false | PromiseLike<TContractData | false>;
    indexOrigination?(origination: ContractOrigination, dbContext: TDbContext, indexingContext: OriginationIndexingContext<TContextData, TContractData>): void | PromiseLike<void>;
    indexTransaction?(transactionParameter: TransactionParameter, dbContext: TDbContext, indexingContext: TransactionIndexingContext<TContextData, TContractData>): void | PromiseLike<void>;
    indexStorageChange?(storageChange: StorageChange, dbContext: TDbContext, indexingContext: StorageChangeIndexingContext<TContextData, TContractData>): void | PromiseLike<void>;
    indexBigMapDiff?(bigMapDiff: BigMapDiff, dbContext: TDbContext, indexingContext: BigMapDiffIndexingContext<TContextData, TContractData>): void | PromiseLike<void>;
    indexBigMapUpdate?(bigMapUpdate: BigMapUpdate, dbContext: TDbContext, indexingContext: BigMapUpdateIndexingContext<TContextData, TContractData>): void | PromiseLike<void>;
    indexEvent?(event: ContractEvent, dbContext: TDbContext, indexingContext: EventIndexingContext<TContextData, TContractData>): void | PromiseLike<void>;
}
export interface ContractIndexingContext<TData = unknown, TContractData = unknown> extends OperationWithResultIndexingContext<TData> {
    readonly bigMapDiffs: readonly BigMapDiff[];
    readonly contract: Contract;
    readonly contractData: TContractData;
    readonly events: readonly ContractEvent[];
    readonly operationWithResult: Applied<OriginationOperation | OriginationInternalOperation | TransactionOperation | TransactionInternalOperation>;
    readonly storageChange: StorageChange | null;
    readonly transactionParameter: TransactionParameter | null;
}
export interface ContractParameterSchemas {
    readonly default: MichelsonSchema;
    readonly entrypoints: ContractEntrypointSchemas;
}
export interface ContractProvider {
    getContract(address: string): Promise<Contract>;
}
export interface DappetizerConfig {
    httpServer?: HttpServerDappetizerConfig;
    ipfs?: IpfsDappetizerConfig;
    logging?: LoggingDappetizerConfig;
    modules: NonEmptyArray<{
        id: string;
        config?: unknown;
    }>;
    networks: {
        [network: string]: {
            indexing: IndexingDappetizerConfig;
            tezosNode: TezosNodeDappetizerConfig;
        };
    };
    time?: TimeDappetizerConfig;
    usageStatistics?: UsageStatisticsDappetizerConfig;
}
export interface DelegationInternalOperation extends BaseInternalOperation {
    readonly kind: OperationKind.Delegation;
    readonly delegateAddress: string | null;
    readonly result: DelegationResult;
}
export interface DelegationOperation extends BaseMainOperationWithResult<TaquitoRpcDelegationOperation> {
    readonly kind: OperationKind.Delegation;
    readonly delegateAddress: string | null;
    readonly result: DelegationResult;
}
export interface DoubleBakingEvidenceOperation extends EntityToIndex<TaquitoRpcDoubleBakingEvidenceOperation> {
    readonly kind: OperationKind.DoubleBakingEvidence;
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly blockHeader1: BlockHeader;
    readonly blockHeader2: BlockHeader;
}
export interface DoubleEndorsementEvidenceOperation extends EntityToIndex<TaquitoRpcDoubleEndorsementEvidenceOperation> {
    readonly kind: OperationKind.DoubleEndorsementEvidence;
    readonly balanceUpdates: readonly BalanceUpdate[];
}
export interface DoublePreendorsementEvidenceOperation extends EntityToIndex<TaquitoRpcDoublePreendorsementEvidenceOperation> {
    readonly kind: OperationKind.DoublePreendorsementEvidence;
    readonly balanceUpdates: readonly BalanceUpdate[];
}
export interface DrainDelegateOperation extends EntityToIndex<TaquitoRpcDrainDelegateOperation> {
    readonly kind: OperationKind.DrainDelegate;
    readonly allocatedDestinationContract: boolean;
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly consensusKey: string;
    readonly delegateAddress: string | null;
    readonly destinationAddress: string | null;
}
export interface EndorsementOperation extends EntityToIndex<TaquitoRpcEndorsementOperation | TaquitoRpcEndorsementWithSlotOperation> {
    readonly kind: OperationKind.Endorsement;
    readonly balanceUpdates: readonly BalanceUpdate[];
}
export interface EntityToIndex<TRpcData = object> {
    readonly uid: string;
    readonly rpcData: TRpcData;
}
export interface EventIndexingContext<TData = unknown, TContractData = unknown> extends ContractIndexingContext<TData, TContractData> {
    readonly event: ContractEvent;
}
export interface EventInternalOperation extends BaseInternalOperation {
    readonly kind: OperationKind.Event;
    readonly payload: LazyMichelsonValue | null;
    readonly result: EventResult;
    readonly tag: string | null;
    readonly typeSchema: MichelsonSchema;
}
export interface FailedOperationResult<TRpcData = object> extends EntityToIndex<TRpcData> {
    readonly status: OperationResultStatus.Failed;
    readonly errors: readonly OperationError[];
}
export interface IncreasePaidStorageOperation extends BaseMainOperationWithResult<TaquitoRpcIncreasePaidStorageOperation> {
    readonly kind: OperationKind.IncreasePaidStorage;
    readonly amount: BigNumber;
    readonly destinationAddress: string;
    readonly result: IncreasePaidStorageResult;
}
export interface IndexedBlock {
    readonly level: number;
    readonly hash: string;
}
export interface IndexerDatabaseHandler<TDbContext> {
    startTransaction(): TDbContext | PromiseLike<TDbContext>;
    commitTransaction(dbContext: TDbContext): void | PromiseLike<void>;
    rollBackTransaction(dbContext: TDbContext): void | PromiseLike<void>;
    insertBlock(block: Block, dbContext: TDbContext): void | PromiseLike<void>;
    deleteBlock(block: IndexedBlock, dbContext: TDbContext): void | PromiseLike<void>;
    getIndexedChainId(): string | null | undefined | PromiseLike<string | null | undefined>;
    getLastIndexedBlock(): IndexedBlock | null | undefined | PromiseLike<IndexedBlock | null | undefined>;
    getIndexedBlock(level: number): IndexedBlock | null | undefined | PromiseLike<IndexedBlock | null | undefined>;
}
export interface IndexerModule<TDbContext, TContextData = unknown> {
    readonly name: string;
    readonly indexingCycleHandler?: IndexingCycleHandler<TDbContext, TContextData>;
    readonly blockDataIndexers?: readonly BlockDataIndexer<TDbContext, TContextData>[];
    readonly contractIndexers?: readonly ContractIndexer<TDbContext, TContextData>[];
    readonly smartRollupIndexers?: readonly SmartRollupIndexer<TDbContext, TContextData>[];
}
export interface IndexingCycleHandler<TDbContext, TContextData = unknown> {
    createContextData?(block: Block, dbContext: TDbContext): TContextData | PromiseLike<TContextData>;
    beforeIndexersExecute?(block: Block, dbContext: TDbContext, contextData: TContextData): void | PromiseLike<void>;
    afterIndexersExecuted?(block: Block, dbContext: TDbContext, contextData: TContextData): void | PromiseLike<void>;
    afterBlockIndexed?(block: Block, contextData: TContextData): void | PromiseLike<void>;
    rollBackOnReorganization?(blockToRollback: IndexedBlock, dbContext: TDbContext): void | PromiseLike<void>;
}
export interface IndexingDappetizerConfig {
    fromBlockLevel: number;
    toBlockLevel?: number;
    contracts?: {
        addresses: NonEmptyArray<string>;
        name?: string;
    }[];
    contractCacheMillis?: number;
    smartRollups?: {
        addresses: NonEmptyArray<string>;
        name?: string;
    }[];
    contractBoost?: {
        type?: 'disabled' | 'tzkt';
        apiUrl?: string;
    };
    retryDelaysMillis?: number[];
    retryIndefinitely?: boolean;
    blockQueueSize?: number;
    headLevelOffset?: number;
    health?: {
        lastSuccessAge?: {
            degradedMillis?: number;
            unhealthyMillis?: number;
        };
    };
}
export interface InternalOperationResultBalanceUpdateIndexingContext<TData = unknown> extends OperationGroupIndexingContext<TData> {
    readonly balanceUpdateSource: 'InternalOperationAppliedResult';
    readonly operationGroup: OperationGroup;
    readonly mainOperation: MainOperationWithResult;
    readonly internalOperation: InternalOperationWithBalanceUpdatesInResult;
}
export interface IpfsDappetizerConfig {
    gateways?: NonEmptyArray<string>;
    timeoutMillis?: number;
}
export interface LazyBigMapDiff extends EntityToIndex<TaquitoRpcBigMapDiff> {
    readonly action: BigMapDiffAction;
    getBigMapDiff(): Promise<BigMapDiff>;
}
export interface LazyContract {
    readonly type: 'Contract';
    readonly address: string;
    readonly config: ContractConfig;
    getContract(): Promise<Contract>;
}
export interface LazyMichelsonValue {
    readonly michelson: Michelson;
    readonly schema: MichelsonSchema;
    convert<T>(): T;
}
export interface LazyStorageChange extends EntityToIndex<Michelson> {
    getStorageChange(): Promise<StorageChange>;
}
export interface LazyTransactionParameter extends EntityToIndex<TaquitoRpcTransactionParameter> {
    getTransactionParameter(): Promise<TransactionParameter>;
}
export interface MainOperationIndexingContext<TData = unknown> extends OperationGroupIndexingContext<TData> {
    readonly operationGroup: OperationGroup;
}
export interface MainOperationMetadataBalanceUpdateIndexingContext<TData = unknown> extends OperationGroupIndexingContext<TData> {
    readonly balanceUpdateSource: 'MainOperationMetadata';
    readonly operationGroup: OperationGroup;
    readonly mainOperation: MainOperationWithBalanceUpdatesInMetadata;
    readonly internalOperation: null;
}
export interface MainOperationResultBalanceUpdateIndexingContext<TData = unknown> extends OperationGroupIndexingContext<TData> {
    readonly balanceUpdateSource: 'MainOperationAppliedResult';
    readonly operationGroup: OperationGroup;
    readonly mainOperation: MainOperationWithBalanceUpdatesInResult;
    readonly internalOperation: null;
}
export interface MichelsonPrim {
    readonly prim: string;
    readonly args?: readonly Michelson[];
    readonly annots?: readonly string[];
}
export interface MichelsonRawValue {
    readonly int?: string;
    readonly string?: string;
    readonly bytes?: string;
}
export interface ModuleCreationOptions {
    readonly config: unknown;
    readonly configElement: ConfigElement;
    readonly diContainer: DependencyContainer;
    getLogger(category: LoggerCategory): Logger;
}
export interface OperationError extends EntityToIndex<TaquitoRpcOperationError> {
    readonly kind: string;
    readonly id: string;
}
export interface OperationGroup extends EntityToIndex<TaquitoRpcOperationGroup> {
    readonly branch: string;
    readonly chainId: string;
    readonly hash: string;
    readonly protocol: string;
    readonly signature: string | null;
    readonly operations: readonly MainOperation[];
}
export interface OperationGroupIndexingContext<TData = unknown> extends BlockIndexingContext<TData> {
    readonly block: Block;
}
export interface OperationWithResultIndexingContext<TData = unknown> extends MainOperationIndexingContext<TData> {
    readonly mainOperation: MainOperationWithResult;
}
export interface OriginationIndexingContext<TData = unknown, TContractData = unknown> extends ContractIndexingContext<TData, TContractData> {
    readonly operationWithResult: Applied<OriginationOperation | OriginationInternalOperation>;
}
export interface OriginationInternalOperation extends BaseInternalOperation {
    readonly kind: OperationKind.Origination;
    readonly balance: BigNumber;
    readonly delegateAddress: string | null;
    readonly result: OriginationResult;
}
export interface OriginationOperation extends BaseMainOperationWithResult<TaquitoRpcOriginationOperation> {
    readonly kind: OperationKind.Origination;
    readonly balance: BigNumber;
    readonly delegateAddress: string | null;
    readonly result: OriginationResult;
}
export interface PartialIndexerModuleVerifier {
    verify(module: IndexerModule<unknown>): void;
}
export interface PreendorsementOperation extends EntityToIndex<TaquitoRpcPreendorsementOperation> {
    readonly kind: OperationKind.Preendorsement;
    readonly balanceUpdates: readonly BalanceUpdate[];
}
export interface ProposalsOperation extends EntityToIndex<TaquitoRpcProposalsOperation> {
    readonly kind: OperationKind.Proposals;
    readonly sourceAddress: string;
    readonly period: number;
    readonly proposals: readonly string[];
}
export interface RegisterGlobalConstantOperation extends BaseMainOperationWithResult<TaquitoRpcRegisterGlobalConstantOperation> {
    readonly kind: OperationKind.RegisterGlobalConstant;
    readonly value: Michelson;
    readonly result: RegisterGlobalConstantResult;
}
export interface RevealOperation extends BaseMainOperationWithResult<TaquitoRpcRevealOperation> {
    readonly kind: OperationKind.Reveal;
    readonly publicKey: string;
    readonly result: RevealResult;
}
export interface SeedNonceRevelationOperation extends EntityToIndex<TaquitoRpcSeedNonceRevelationOperation> {
    readonly kind: OperationKind.SeedNonceRevelation;
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly level: number;
    readonly nonce: string;
}
export interface SelectiveIndexingConfig {
    readonly configPropertyPath: string;
    readonly contracts: Readonly<NonEmptyArray<SelectiveIndexingItemConfig>> | null;
}
export interface SelectiveIndexingItemConfig {
    readonly addresses: Readonly<NonEmptyArray<string>>;
    readonly name: string | null;
}
export interface SetDepositsLimitOperation extends BaseMainOperationWithResult<TaquitoRpcSetDepositsLimitOperation> {
    readonly kind: OperationKind.SetDepositsLimit;
    readonly limit: BigNumber | null;
    readonly result: SetDepositsLimitResult;
}
export interface SkippedOperationResult<TRpcData = object> extends EntityToIndex<TRpcData> {
    readonly status: OperationResultStatus.Skipped;
}
export interface SmartRollup {
    readonly type: 'SmartRollup';
    readonly address: string;
    readonly config: SmartRollupConfig;
}
export interface SmartRollupAddMessagesIndexingContext<TData = unknown> extends OperationWithResultIndexingContext<TData> {
    readonly mainOperation: Applied<SmartRollupAddMessagesOperation>;
}
export interface SmartRollupAddMessagesOperation extends BaseMainOperationWithResult<TaquitoRpcSmartRollupAddMessagesOperation> {
    readonly kind: OperationKind.SmartRollupAddMessages;
    readonly message: readonly string[];
    readonly result: SmartRollupAddMessagesResult;
}
export interface SmartRollupCementIndexingContext<TData = unknown, TRollupData = unknown> extends SmartRollupIndexingContext<TData, TRollupData> {
    readonly mainOperation: Applied<SmartRollupCementOperation>;
}
export interface SmartRollupCementOperation extends BaseMainOperationWithResult<TaquitoRpcSmartRollupCementOperation> {
    readonly kind: OperationKind.SmartRollupCement;
    readonly result: SmartRollupCementResult;
    readonly rollup: SmartRollup;
}
export interface SmartRollupCommitment {
    readonly compressedState: string;
    readonly inboxLevel: number;
    readonly numberOfTicks: BigNumber;
    readonly predecessorHash: string;
}
export interface SmartRollupConfig {
    readonly toBeIndexed: boolean;
    readonly name: string | null;
}
export interface SmartRollupExecuteOutboxMessageIndexingContext<TData = unknown, TRollupData = unknown> extends SmartRollupIndexingContext<TData, TRollupData> {
    readonly mainOperation: Applied<SmartRollupExecuteOutboxMessageOperation>;
}
export interface SmartRollupExecuteOutboxMessageOperation extends BaseMainOperationWithResult<TaquitoRpcSmartRollupExecuteOutboxMessageOperation> {
    readonly kind: OperationKind.SmartRollupExecuteOutboxMessage;
    readonly cementedCommitment: string;
    readonly outputProof: string;
    readonly result: SmartRollupExecuteOutboxMessageResult;
    readonly rollup: SmartRollup;
}
export interface SmartRollupGameStatusLoser {
    readonly kind: SmartRollupGameStatusKind.Loser;
    readonly reason: SmartRollupGameStatusLoserReason;
    readonly playerAddress: string;
}
export interface SmartRollupGameStatusOngoingOrDraw {
    readonly kind: SmartRollupGameStatusKind.Ongoing | SmartRollupGameStatusKind.Draw;
    readonly reason: null;
    readonly playerAddress: null;
}
export interface SmartRollupIndexer<TDbContext, TContextData = unknown, TRollupData = unknown> {
    readonly name?: string;
    shouldIndex?(rollup: SmartRollup, dbContext: TDbContext): TRollupData | false | PromiseLike<TRollupData | false>;
    indexAddMessages?(operation: Applied<SmartRollupAddMessagesOperation>, dbContext: TDbContext, indexingContext: SmartRollupAddMessagesIndexingContext<TContextData>): void | PromiseLike<void>;
    indexCement?(operation: Applied<SmartRollupCementOperation>, dbContext: TDbContext, indexingContext: SmartRollupCementIndexingContext<TContextData, TRollupData>): void | PromiseLike<void>;
    indexExecuteOutboxMessage?(operation: Applied<SmartRollupExecuteOutboxMessageOperation>, dbContext: TDbContext, indexingContext: SmartRollupExecuteOutboxMessageIndexingContext<TContextData, TRollupData>): void | PromiseLike<void>;
    indexOriginate?(operation: Applied<SmartRollupOriginateOperation>, dbContext: TDbContext, indexingContext: SmartRollupOriginateIndexingContext<TContextData, TRollupData>): void | PromiseLike<void>;
    indexPublish?(operation: Applied<SmartRollupPublishOperation>, dbContext: TDbContext, indexingContext: SmartRollupPublishIndexingContext<TContextData, TRollupData>): void | PromiseLike<void>;
    indexRecoverBond?(operation: Applied<SmartRollupRecoverBondOperation>, dbContext: TDbContext, indexingContext: SmartRollupRecoverBondIndexingContext<TContextData, TRollupData>): void | PromiseLike<void>;
    indexRefute?(operation: Applied<SmartRollupRefuteOperation>, dbContext: TDbContext, indexingContext: SmartRollupRefuteIndexingContext<TContextData, TRollupData>): void | PromiseLike<void>;
    indexTimeout?(operation: Applied<SmartRollupTimeoutOperation>, dbContext: TDbContext, indexingContext: SmartRollupTimeoutIndexingContext<TContextData, TRollupData>): void | PromiseLike<void>;
}
export interface SmartRollupIndexingContext<TData = unknown, TRollupData = unknown> extends OperationWithResultIndexingContext<TData> {
    readonly rollupData: TRollupData;
    readonly rollup: SmartRollup;
}
export interface SmartRollupOriginateIndexingContext<TData = unknown, TRollupData = unknown> extends SmartRollupIndexingContext<TData, TRollupData> {
    readonly mainOperation: Applied<SmartRollupOriginateOperation>;
}
export interface SmartRollupOriginateOperation extends BaseMainOperationWithResult<TaquitoRpcSmartRollupOriginateOperation> {
    readonly kind: OperationKind.SmartRollupOriginate;
    readonly kernel: string;
    readonly parametersSchema: MichelsonSchema;
    readonly pvmKind: string;
    readonly result: SmartRollupOriginateResult;
}
export interface SmartRollupProvider {
    getRollup(address: string): SmartRollup;
}
export interface SmartRollupPublishIndexingContext<TData = unknown, TRollupData = unknown> extends SmartRollupIndexingContext<TData, TRollupData> {
    readonly mainOperation: Applied<SmartRollupPublishOperation>;
}
export interface SmartRollupPublishOperation extends BaseMainOperationWithResult<TaquitoRpcSmartRollupPublishOperation> {
    readonly kind: OperationKind.SmartRollupPublish;
    readonly commitment: SmartRollupCommitment;
    readonly result: SmartRollupPublishResult;
    readonly rollup: SmartRollup;
}
export interface SmartRollupRecoverBondIndexingContext<TData = unknown, TRollupData = unknown> extends SmartRollupIndexingContext<TData, TRollupData> {
    readonly mainOperation: Applied<SmartRollupRecoverBondOperation>;
}
export interface SmartRollupRecoverBondOperation extends BaseMainOperationWithResult<TaquitoRpcSmartRollupRecoverBondOperation> {
    readonly kind: OperationKind.SmartRollupRecoverBond;
    readonly result: SmartRollupRecoverBondResult;
    readonly rollup: SmartRollup;
    readonly stakerAddress: string;
}
export interface SmartRollupRefutationMove {
    readonly kind: SmartRollupRefutationKind.Move;
    readonly choice: BigNumber;
    readonly opponentCommitmentHash: null;
    readonly playerCommitmentHash: null;
}
export interface SmartRollupRefutationStart {
    readonly kind: SmartRollupRefutationKind.Start;
    readonly opponentCommitmentHash: string;
    readonly playerCommitmentHash: string;
    readonly choice: null;
}
export interface SmartRollupRefuteIndexingContext<TData = unknown, TRollupData = unknown> extends SmartRollupIndexingContext<TData, TRollupData> {
    readonly mainOperation: Applied<SmartRollupRefuteOperation>;
}
export interface SmartRollupRefuteOperation extends BaseMainOperationWithResult<TaquitoRpcSmartRollupRefuteOperation> {
    readonly kind: OperationKind.SmartRollupRefute;
    readonly opponentAddress: string;
    readonly refutation: SmartRollupRefutation;
    readonly result: SmartRollupRefuteResult;
    readonly rollup: SmartRollup;
}
export interface SmartRollupTimeoutIndexingContext<TData = unknown, TRollupData = unknown> extends SmartRollupIndexingContext<TData, TRollupData> {
    readonly mainOperation: Applied<SmartRollupTimeoutOperation>;
}
export interface SmartRollupTimeoutOperation extends BaseMainOperationWithResult<TaquitoRpcSmartRollupTimeoutOperation> {
    readonly kind: OperationKind.SmartRollupTimeout;
    readonly aliceStakerAddress: string;
    readonly bobStakerAddress: string;
    readonly result: SmartRollupTimeoutResult;
    readonly rollup: SmartRollup;
}
export interface StorageChange extends EntityToIndex<Michelson> {
    readonly newValue: LazyMichelsonValue;
    readonly oldValue: LazyMichelsonValue;
}
export interface StorageChangeIndexingContext<TData = unknown, TContractData = unknown> extends ContractIndexingContext<TData, TContractData> {
    readonly storageChange: StorageChange;
}
export interface Ticket {
    readonly amount: BigNumber;
    readonly contents: LazyMichelsonValue;
    readonly ticketerAddress: string;
}
export interface TicketToken {
    readonly content: LazyMichelsonValue;
    readonly ticketerAddress: string;
    equals(otherToken: TicketToken): boolean;
}
export interface TicketUpdate extends EntityToIndex<TaquitoRpcTicketUpdate> {
    readonly accountAddress: string;
    readonly amount: BigNumber;
    readonly token: TicketToken;
}
export interface TransactionIndexingContext<TData = unknown, TContractData = unknown> extends ContractIndexingContext<TData, TContractData> {
    readonly transactionParameter: TransactionParameter;
    readonly operationWithResult: Applied<TransactionOperation | TransactionInternalOperation>;
}
export interface TransactionInternalOperation extends BaseInternalOperation {
    readonly kind: OperationKind.Transaction;
    readonly amount: BigNumber;
    readonly destinationAddress: string;
    readonly destinationContract: LazyContract | null;
    readonly destination: UserTransactionDestination | LazyContract | SmartRollup;
    readonly transactionParameter: LazyTransactionParameter | null;
    readonly result: TransactionResult;
}
export interface TransactionOperation extends BaseMainOperationWithResult<TaquitoRpcTransactionOperation> {
    readonly kind: OperationKind.Transaction;
    readonly amount: BigNumber;
    readonly destinationAddress: string;
    readonly destinationContract: LazyContract | null;
    readonly destination: UserTransactionDestination | LazyContract;
    readonly transactionParameter: LazyTransactionParameter | null;
    readonly result: TransactionResult;
}
export interface TransactionParameter extends EntityToIndex<TaquitoRpcTransactionParameter> {
    readonly entrypoint: string;
    readonly entrypointPath: Readonly<NonEmptyArray<string>>;
    readonly value: LazyMichelsonValue;
}
export interface TransferTicketOperation extends BaseMainOperationWithResult<TaquitoRpcTransferTicketOperation> {
    readonly kind: OperationKind.TransferTicket;
    readonly destinationAddress: string;
    readonly entrypoint: string;
    readonly result: TransferTicketResult;
    readonly ticket: Ticket;
    readonly ticketAmount: BigNumber;
    readonly ticketToken: TicketToken;
}
export interface UpdateConsensusKeyOperation extends BaseMainOperationWithResult<TaquitoRpcUpdateConsensusKeyOperation> {
    readonly kind: OperationKind.UpdateConsensusKey;
    readonly pk: string;
    readonly result: UpdateConsensusKeyResult;
}
export interface UserTransactionDestination {
    readonly type: 'User';
    readonly address: string;
}
export interface VdfRevelationOperation extends EntityToIndex<TaquitoRpcVdfRevelationOperation> {
    readonly kind: OperationKind.VdfRevelation;
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly solution: readonly string[];
}
export type Applied<TOperation extends OperationWithResult> = TOperation & {
    readonly result: Extract<TOperation['result'], {
        status: OperationResultStatus.Applied;
    }>;
};
export type BalanceUpdate = BalanceUpdateAccumulator | BalanceUpdateBurned | BalanceUpdateBurnedLostRewards | BalanceUpdateCommitment | BalanceUpdateContract | BalanceUpdateFreezerLegacy | BalanceUpdateFreezerBonds | BalanceUpdateFreezerDeposits | BalanceUpdateFreezerUnstakedDeposits | BalanceUpdateMinted | BalanceUpdateStakingDelegateDenominator | BalanceUpdateStakingDelegatorNumerator;
export type BalanceUpdateFrozenStaker = BalanceUpdateSingleStaker | BalanceUpdateSharedStaker | BalanceUpdateBaker;
export type BalanceUpdateIndexingContext<TData = unknown> = BlockMetadataBalanceUpdateIndexingContext<TData> | MainOperationMetadataBalanceUpdateIndexingContext<TData> | MainOperationResultBalanceUpdateIndexingContext<TData> | InternalOperationResultBalanceUpdateIndexingContext<TData>;
export type BalanceUpdateStaker = BalanceUpdateSingleStaker | BalanceUpdateSharedStaker;
export type BigMapDiff = BigMapAlloc | BigMapCopy | BigMapRemove | BigMapUpdate;
export type ContractAbstraction = taquito.ContractAbstraction<taquito.ContractProvider> & ReturnType<typeof taquitoTzip12.tzip12> & ReturnType<typeof taquitoTzip16.tzip16>;
export type ContractEntrypointSchemas = Readonly<Record<string, MichelsonSchema>>;
export type ContractEvent = Applied<EventInternalOperation>;
export type ContractOrigination = Applied<OriginationOperation | OriginationInternalOperation>;
export type DelegationResult = OperationResult<AppliedDelegationResult>;
export type EventResult = OperationResult<AppliedEventResult>;
export type ExplicitBlocksToIndex = Readonly<NonEmptyArray<BlockRange>>;
export type IncreasePaidStorageResult = OperationResult<AppliedIncreasePaidStorageResult>;
export type IndexerModuleFactory<TDbContext, TContextData = unknown> = (options: ModuleCreationOptions) => IndexerModule<TDbContext, TContextData>;
export type InternalOperation = DelegationInternalOperation | EventInternalOperation | OriginationInternalOperation | TransactionInternalOperation;
export type InternalOperationKind = OperationKind.Delegation | OperationKind.Event | OperationKind.Origination | OperationKind.Transaction;
export type InternalOperationWithBalanceUpdatesInResult = Applied<OriginationInternalOperation | TransactionInternalOperation>;
export type MainOperation = ActivateAccountOperation | BallotOperation | DelegationOperation | DoubleBakingEvidenceOperation | DoubleEndorsementEvidenceOperation | DoublePreendorsementEvidenceOperation | DrainDelegateOperation | EndorsementOperation | IncreasePaidStorageOperation | OriginationOperation | PreendorsementOperation | ProposalsOperation | RegisterGlobalConstantOperation | RevealOperation | SeedNonceRevelationOperation | SetDepositsLimitOperation | SmartRollupAddMessagesOperation | SmartRollupCementOperation | SmartRollupExecuteOutboxMessageOperation | SmartRollupOriginateOperation | SmartRollupPublishOperation | SmartRollupRecoverBondOperation | SmartRollupRefuteOperation | SmartRollupTimeoutOperation | TransactionOperation | TransferTicketOperation | UpdateConsensusKeyOperation | VdfRevelationOperation;
export type MainOperationWithBalanceUpdatesInMetadata = ActivateAccountOperation | DelegationOperation | DoubleBakingEvidenceOperation | DoubleEndorsementEvidenceOperation | DoublePreendorsementEvidenceOperation | DrainDelegateOperation | EndorsementOperation | IncreasePaidStorageOperation | PreendorsementOperation | OriginationOperation | RegisterGlobalConstantOperation | RevealOperation | SeedNonceRevelationOperation | SetDepositsLimitOperation | SmartRollupAddMessagesOperation | SmartRollupCementOperation | SmartRollupExecuteOutboxMessageOperation | SmartRollupOriginateOperation | SmartRollupPublishOperation | SmartRollupRecoverBondOperation | SmartRollupRefuteOperation | SmartRollupTimeoutOperation | TransactionOperation | TransferTicketOperation | VdfRevelationOperation | UpdateConsensusKeyOperation;
export type MainOperationWithBalanceUpdatesInResult = Applied<IncreasePaidStorageOperation | OriginationOperation | RegisterGlobalConstantOperation | SmartRollupExecuteOutboxMessageOperation | SmartRollupOriginateOperation | SmartRollupPublishOperation | SmartRollupRecoverBondOperation | SmartRollupRefuteOperation | SmartRollupTimeoutOperation | TransactionOperation | TransferTicketOperation>;
export type MainOperationWithResult = DelegationOperation | IncreasePaidStorageOperation | OriginationOperation | RegisterGlobalConstantOperation | RevealOperation | SetDepositsLimitOperation | SmartRollupAddMessagesOperation | SmartRollupCementOperation | SmartRollupExecuteOutboxMessageOperation | SmartRollupOriginateOperation | SmartRollupPublishOperation | SmartRollupRecoverBondOperation | SmartRollupRefuteOperation | SmartRollupTimeoutOperation | TransactionOperation | TransferTicketOperation | UpdateConsensusKeyOperation;
export type Michelson = MichelsonPrim | MichelsonRawValue | readonly Michelson[];
export type OperationResult<TApplied extends AppliedOperationResult = AppliedOperationResult> = TApplied extends AppliedOperationResult<infer TRpcData> ? TApplied | BacktrackedOperationResult<TRpcData> | FailedOperationResult<TRpcData> | SkippedOperationResult<TRpcData> : never;
export type OperationWithResult = MainOperationWithResult | InternalOperation;
export type OriginationResult = OperationResult<AppliedOriginationResult>;
export type RegisterGlobalConstantResult = OperationResult<AppliedRegisterGlobalConstantResult>;
export type RevealResult = OperationResult<AppliedRevealResult>;
export type SetDepositsLimitResult = OperationResult<AppliedSetDepositsLimitResult>;
export type SmartRollupAddMessagesResult = OperationResult<AppliedSmartRollupAddMessagesResult>;
export type SmartRollupCementResult = OperationResult<AppliedSmartRollupCementResult>;
export type SmartRollupExecuteOutboxMessageResult = OperationResult<AppliedSmartRollupExecuteOutboxMessageResult>;
export type SmartRollupGameStatus = SmartRollupGameStatusOngoingOrDraw | SmartRollupGameStatusLoser;
export type SmartRollupOperation = SmartRollupAddMessagesOperation | SmartRollupCementOperation | SmartRollupExecuteOutboxMessageOperation | SmartRollupOriginateOperation | SmartRollupPublishOperation | SmartRollupRecoverBondOperation | SmartRollupRefuteOperation | SmartRollupTimeoutOperation;
export type SmartRollupOriginateResult = OperationResult<AppliedSmartRollupOriginateResult>;
export type SmartRollupPublishResult = OperationResult<AppliedSmartRollupPublishResult>;
export type SmartRollupRecoverBondResult = OperationResult<AppliedSmartRollupRecoverBondResult>;
export type SmartRollupRefutation = SmartRollupRefutationStart | SmartRollupRefutationMove;
export type SmartRollupRefuteResult = OperationResult<AppliedSmartRollupRefuteResult>;
export type SmartRollupTimeoutResult = OperationResult<AppliedSmartRollupTimeoutResult>;
export type TaquitoRpcActivateAccountOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultActivateAccount>;
export type TaquitoRpcBalanceUpdate = DeepReadonly<taquitoRpc.OperationMetadataBalanceUpdates>;
export type TaquitoRpcBallotOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultBallot>;
export type TaquitoRpcBigMapDiff = TaquitoRpcLegacyBigMapDiff | TaquitoRpcLazyStorageBigMapDiff;
export type TaquitoRpcBlock = DeepReadonly<taquitoRpc.BlockResponse>;
export type TaquitoRpcBlockHeader = DeepReadonly<taquitoRpc.BlockFullHeader>;
export type TaquitoRpcDelegationOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultDelegation>;
export type TaquitoRpcDelegationResult = DeepReadonly<taquitoRpc.OperationResultDelegation>;
export type TaquitoRpcDoubleBakingEvidenceOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultDoubleBaking>;
export type TaquitoRpcDoubleEndorsementEvidenceOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultDoubleEndorsement>;
export type TaquitoRpcDoublePreendorsementEvidenceOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultDoublePreEndorsement>;
export type TaquitoRpcDrainDelegateOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultDrainDelegate>;
export type TaquitoRpcEndorsementOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultEndorsement>;
export type TaquitoRpcEndorsementWithSlotOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultEndorsementWithDal>;
export type TaquitoRpcEventResult = DeepReadonly<taquitoRpc.OperationResultEvent>;
export type TaquitoRpcIncreasePaidStorageOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultIncreasePaidStorage>;
export type TaquitoRpcIncreasePaidStorageResult = DeepReadonly<taquitoRpc.OperationResultIncreasePaidStorage>;
export type TaquitoRpcInternalOperation = DeepReadonly<taquitoRpc.InternalOperationResult>;
export type TaquitoRpcLazyStorageBigMapDiff = DeepReadonly<taquitoRpc.LazyStorageDiffBigMap>;
export type TaquitoRpcLegacyBigMapDiff = DeepReadonly<taquitoRpc.ContractBigMapDiffItem>;
export type TaquitoRpcOperation = DeepReadonly<taquitoRpc.OperationContentsAndResult>;
export type TaquitoRpcOperationError = DeepReadonly<taquitoRpc.TezosGenericOperationError>;
export type TaquitoRpcOperationGroup = DeepReadonly<taquitoRpc.OperationEntry>;
export type TaquitoRpcOriginationOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultOrigination>;
export type TaquitoRpcOriginationResult = DeepReadonly<taquitoRpc.OperationResultOrigination>;
export type TaquitoRpcPreendorsementOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultPreEndorsement>;
export type TaquitoRpcProposalsOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultProposals>;
export type TaquitoRpcRegisterGlobalConstantOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultRegisterGlobalConstant>;
export type TaquitoRpcRegisterGlobalConstantResult = DeepReadonly<taquitoRpc.OperationResultRegisterGlobalConstant>;
export type TaquitoRpcRevealOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultReveal>;
export type TaquitoRpcRevealResult = DeepReadonly<taquitoRpc.OperationResultReveal>;
export type TaquitoRpcSeedNonceRevelationOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultRevelation>;
export type TaquitoRpcSetDepositsLimitOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSetDepositsLimit>;
export type TaquitoRpcSetDepositsLimitResult = DeepReadonly<taquitoRpc.OperationResultSetDepositsLimit>;
export type TaquitoRpcSmartRollupAddMessagesOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSmartRollupAddMessages>;
export type TaquitoRpcSmartRollupAddMessagesResult = DeepReadonly<taquitoRpc.OperationResultSmartRollupAddMessages>;
export type TaquitoRpcSmartRollupCementOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSmartRollupCement>;
export type TaquitoRpcSmartRollupCementResult = DeepReadonly<taquitoRpc.OperationResultSmartRollupCement>;
export type TaquitoRpcSmartRollupExecuteOutboxMessageOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSmartRollupExecuteOutboxMessage>;
export type TaquitoRpcSmartRollupExecuteOutboxMessageResult = DeepReadonly<taquitoRpc.OperationResultSmartRollupExecuteOutboxMessage>;
export type TaquitoRpcSmartRollupOriginateOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSmartRollupOriginate>;
export type TaquitoRpcSmartRollupOriginateResult = DeepReadonly<taquitoRpc.OperationResultSmartRollupOriginate>;
export type TaquitoRpcSmartRollupPublishOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSmartRollupPublish>;
export type TaquitoRpcSmartRollupPublishResult = DeepReadonly<taquitoRpc.OperationResultSmartRollupPublish>;
export type TaquitoRpcSmartRollupRecoverBondOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSmartRollupRecoverBond>;
export type TaquitoRpcSmartRollupRecoverBondResult = DeepReadonly<taquitoRpc.OperationResultSmartRollupRecoverBond>;
export type TaquitoRpcSmartRollupRefuteOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSmartRollupRefute>;
export type TaquitoRpcSmartRollupRefuteResult = DeepReadonly<taquitoRpc.OperationResultSmartRollupRefute>;
export type TaquitoRpcSmartRollupTimeoutOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSmartRollupTimeout>;
export type TaquitoRpcSmartRollupTimeoutResult = DeepReadonly<taquitoRpc.OperationResultSmartRollupTimeout>;
export type TaquitoRpcTicketUpdate = DeepReadonly<taquitoRpc.TicketUpdates['updates'][number]>;
export type TaquitoRpcTransactionOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultTransaction>;
export type TaquitoRpcTransactionParameter = DeepReadonly<taquitoRpc.TransactionOperationParameter>;
export type TaquitoRpcTransactionResult = DeepReadonly<taquitoRpc.OperationResultTransaction>;
export type TaquitoRpcTransferTicketOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultTransferTicket>;
export type TaquitoRpcTransferTicketResult = DeepReadonly<taquitoRpc.OperationResultTransferTicket>;
export type TaquitoRpcUpdateConsensusKeyOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultUpdateConsensusKey>;
export type TaquitoRpcUpdateConsensusKeyResult = DeepReadonly<taquitoRpc.OperationResultUpdateConsensusKey>;
export type TaquitoRpcVdfRevelationOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultVdfRevelation>;
export type TransactionResult = OperationResult<AppliedTransactionResult>;
export type TransferTicketResult = OperationResult<AppliedTransferTicketResult>;
export type UpdateConsensusKeyResult = OperationResult<AppliedUpdateConsensusKeyResult>;