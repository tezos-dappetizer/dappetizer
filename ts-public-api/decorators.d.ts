// This is NOT a valid TypeScript with type definitions.
// This is a public API signature, sorted alphabetically, used to guard breaking changes.

export declare function contractFilter(options: ContractFilterOptions): ClassDecorator;
export declare function createContractIndexerFromDecorators<TDbContext, TContextData = undefined, TContractData = boolean>(decoratedIndexer: object): ContractIndexer<TDbContext, TContextData, TContractData>;
export declare function createSmartRollupIndexerFromDecorators<TDbContext, TContextData = undefined, TRollupData = boolean>(decoratedIndexer: object): SmartRollupIndexer<TDbContext, TContextData, TRollupData>;
export declare function indexAddMessages(): MethodDecorator;
export declare function indexBigMapDiff(options?: IndexBigMapDiffOptions): MethodDecorator;
export declare function indexBigMapUpdate(options: IndexBigMapUpdateOptions): MethodDecorator;
export declare function indexCement(): MethodDecorator;
export declare function indexEntrypoint(entrypoint: string): MethodDecorator;
export declare function indexEvent(tag: string | null): MethodDecorator;
export declare function indexExecuteOutboxMessage(): MethodDecorator;
export declare function indexOriginate(): MethodDecorator;
export declare function indexOrigination(): MethodDecorator;
export declare function indexPublish(): MethodDecorator;
export declare function indexRecoverBond(): MethodDecorator;
export declare function indexRefute(): MethodDecorator;
export declare function indexStorageChange(): MethodDecorator;
export declare function indexTimeout(): MethodDecorator;
export declare function indexTransaction(): MethodDecorator;
export declare function shouldIndex(): MethodDecorator;
export declare function smartRollupFilter(options: SmartRollupFilterOptions): ClassDecorator;
export type ContractFilterOptions = {
    readonly name: string;
    anyOfAddresses?: undefined;
} | {
    readonly anyOfAddresses: Readonly<NonEmptyArray<string>>;
    name?: undefined;
};
export type IndexBigMapDiffOptions = IndexBigMapOptions;
export type IndexBigMapOptions = {
    readonly path: readonly string[];
    name?: undefined;
} | {
    readonly name: string;
    path?: undefined;
};
export type IndexBigMapUpdateOptions = IndexBigMapOptions;
export type SmartRollupFilterOptions = {
    readonly name: string;
    anyOfAddresses?: undefined;
} | {
    readonly anyOfAddresses: Readonly<NonEmptyArray<string>>;
    name?: undefined;
};