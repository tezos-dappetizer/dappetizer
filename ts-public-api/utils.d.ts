// This is NOT a valid TypeScript with type definitions.
// This is a public API signature, sorted alphabetically, used to guard breaking changes.

export declare abstract class BaseConfigElement<TValue = unknown> {
    readonly value: TValue;
    readonly filePath: string;
    readonly propertyPath: string;
    constructor(value: TValue, filePath: string, propertyPath: string);
}
export declare abstract class BaseLogger implements Logger {
    #private;
    readonly category: string;
    constructor(category: string, scope?: LoggerScope);
    get isCriticalEnabled(): boolean;
    get isErrorEnabled(): boolean;
    get isWarningEnabled(): boolean;
    get isInformationEnabled(): boolean;
    get isDebugEnabled(): boolean;
    get isTraceEnabled(): boolean;
    log(level: LogLevel, message: string, data?: LogData): void;
    runWithData<TResult>(data: LogData, scopeFunc: () => TResult): TResult;
    logCritical(message: string, data?: LogData): void;
    logError(message: string, data?: LogData): void;
    logWarning(message: string, data?: LogData): void;
    logInformation(message: string, data?: LogData): void;
    logDebug(message: string, data?: LogData): void;
    logTrace(message: string, data?: LogData): void;
    abstract isEnabled(level: LogLevel): boolean;
    protected abstract writeToLog(level: LogLevel, message: string, data: LogData): void;
}
export declare class AbortError extends Error {
    readonly name = "AbortError";
    constructor();
}
export declare class ArgError extends Error {
    readonly argName: string;
    readonly specifiedValue: unknown;
    readonly details: string;
    constructor(options: {
        readonly argName: string;
        readonly specifiedValue: unknown;
        readonly details: string;
    });
}
export declare class CacheMap<TKey, TValue> {
    #private;
    constructor(options: {
        stringifyKey: (key: TKey) => string;
        itemFactory: (key: TKey) => TValue;
    });
    get(key: TKey): TValue;
}
export declare class ConfigElement extends BaseConfigElement {
    #private;
    constructor(value: unknown, filePath: string, propertyPath: string, validators?: {
        address: import("../basics/public-api").AddressValidator;
        array: import("../basics/public-api").ArrayValidator;
        number: import("../basics/public-api").NumberValidator;
        oneOfSupported: import("../basics/public-api").OneOfSupportedValidator;
        string: import("../basics/public-api").StringValidator;
        type: import("../basics/public-api").TypeValidator;
    });
    asAbsoluteUrl(): string;
    asAddress(prefixes?: readonly AddressPrefix[]): string;
    asArray(limits?: ArrayLimits): ConfigElement[];
    asBoolean(): boolean;
    asEnum<T extends string>(enumType: Record<string, T>): T;
    asInteger(limits?: StrictOmit<NumberLimits, 'integer'>): number;
    asObject<TKey extends string>(): ConfigObjectElement<TKey>;
    asOneOf<T extends string>(supportedValues: readonly T[]): T;
    asString(limits?: StringLimits): string;
}
export declare class ConfigError extends Error {
    readonly value: unknown;
    readonly filePath: string;
    readonly propertyPath: string;
    readonly reason: string;
    constructor(options: {
        readonly value: unknown;
        readonly filePath: string;
        readonly propertyPath: string;
        readonly reason: string;
    });
}
export declare class ConfigObjectElement<TProperty extends string = string> extends BaseConfigElement<Readonly<Record<string, unknown>>> {
    constructor(value: Readonly<Record<string, unknown>>, filePath: string, propertyPath: string);
    get(propertyName: TProperty): ConfigElement;
    getOptional(propertyName: TProperty): ConfigElement | null;
}
export declare class DappetizerBug extends Error {
}
export declare class LoggerScope {
    #private;
    constructor();
    runWithData<TResult>(data: LogData, scopeFunc: () => TResult): TResult;
    enrichData(data: LogData): LogData;
}
export declare class NullRootLogger implements RootLogger, RootLoggerFactory {
    constructor();
    create(): RootLogger;
    isEnabled(_level: LogLevel): boolean;
    log(_entry: LogEntry): void;
    close(): Promise<void>;
}
export declare class Stopwatch {
    private readonly clock;
    readonly startTimestamp: number;
    constructor(clock: Clock);
    get elapsedMillis(): number;
}
export declare class TaquitoHttpBackend extends taquitoHttp.HttpBackend {
    #private;
    constructor(axiosAbstraction?: typeof axios);
    createRequest<T>(options: taquitoHttp.HttpRequestOptions, data?: object | string): Promise<T>;
    execute(request: TaquitoHttpRequest): Promise<unknown>;
}
export declare const APP_SHUTDOWN_MANAGER_DI_TOKEN: InjectionToken<AppShutdownManager>;
export declare const CLOCK_DI_TOKEN: InjectionToken<Clock>;
export declare const CONFIG_NETWORK_DI_TOKEN: InjectionToken<string>;
export declare const DI_CONTAINER_DI_TOKEN: InjectionToken<DependencyContainer>;
export declare const ENABLE_TRACE = "(set minLevel to trace)";
export declare const EXPRESS_APP_DI_TOKEN: InjectionToken<ExpressApplication>;
export declare const FILE_SYSTEM_DI_TOKEN: InjectionToken<FileSystem>;
export declare const HTTP_FETCHER_DI_TOKEN: InjectionToken<HttpFetcher>;
export declare const JSON_FETCHER_DI_TOKEN: InjectionToken<JsonFetcher>;
export declare const LOGGER_FACTORY_DI_TOKEN: InjectionToken<LoggerFactory>;
export declare const LONG_EXECUTION_HELPER_DI_TOKEN: InjectionToken<LongExecutionHelper>;
export declare const MAINNET = "mainnet";
export declare const NETWORK_SPECIFIC_CONFIG_ELEMENT_DI_TOKEN: InjectionToken<ConfigObjectElement>;
export declare const PACKAGE_JSON_LOADER_DI_TOKEN: InjectionToken<PackageJsonLoader>;
export declare const ROOT_CONFIG_ELEMENT_DI_TOKEN: InjectionToken<ConfigObjectElement>;
export declare const ROOT_LOGGER_DI_TOKEN: InjectionToken<RootLogger>;
export declare const ROOT_LOGGER_FACTORY_DI_TOKEN: InjectionToken<RootLoggerFactory>;
export declare const RPC_BLOCK_MONITOR_DI_TOKEN: InjectionToken<RpcBlockMonitor>;
export declare const RPC_MEMPOOL_MONITOR_DI_TOKEN: InjectionToken<RpcMempoolMonitor>;
export declare const SLEEP_HELPER_DI_TOKEN: InjectionToken<SleepHelper>;
export declare const TAQUITO_HTTP_BACKEND_DI_TOKEN: InjectionToken<TaquitoHttpBackend>;
export declare const TAQUITO_RPC_CLIENT_DI_TOKEN: InjectionToken<taquitoRpc.RpcClient>;
export declare const TIMER_HELPER_DI_TOKEN: InjectionToken<TimerHelper>;
export declare const addressValidator: AddressValidator;
export declare const allAddressPrefixes: readonly AddressPrefix[];
export declare const argGuard: ArgGuard;
export declare const arrayValidator: ArrayValidator;
export declare const candidateConfigFileNames: readonly string[];
export declare const contentEncodings: Readonly<{
    DEFLATE: "deflate";
    GZIP: "gzip";
}>;
export declare const contentTypes: Readonly<{
    JSON: "application/json";
    TEXT: "text/plain";
}>;
export declare const emptyStringSanitizer: StringSanitizer;
export declare const httpHeaders: Readonly<{
    ACCEPT: "Accept";
    ACCEPT_ENCODING: "Accept-Encoding";
    CONTENT_TYPE: "Content-Type";
}>;
export declare const httpMethods: Readonly<{
    DELETE: "DELETE";
    GET: "GET";
    HEAD: "HEAD";
    OPTIONS: "OPTIONS";
    PATCH: "PATCH";
    POST: "POST";
    PUT: "PUT";
}>;
export declare const httpStatusCodes: Readonly<{
    INTERNAL_SERVER_ERROR: 500;
    OK: 200;
}>;
export declare const npmModules: {
    readonly BIGNUMBER: "bignumber.js";
    readonly dappetizer: {
        readonly CLI: "@tezos-dappetizer/cli";
        readonly DATABASE: "@tezos-dappetizer/database";
        readonly DECORATORS: "@tezos-dappetizer/decorators";
        readonly INDEXER: "@tezos-dappetizer/indexer";
        readonly UTILS: "@tezos-dappetizer/utils";
    };
    readonly taquito: {
        readonly TAQUITO: "@taquito/taquito";
        readonly MICHELSON_ENCODER: "@taquito/michelson-encoder";
    };
    readonly TYPEORM: "typeorm";
    readonly TYPESCRIPT: "typescript";
};
export declare const numberValidator: NumberValidator;
export declare const oneOfSupportedValidator: OneOfSupportedValidator;
export declare const stringValidator: StringValidator;
export declare const typeValidator: TypeValidator;
export declare const tzAddressPrefixes: readonly AddressPrefix[];
export declare const versionInfo: Readonly<{
    readonly COMMIT_HASH: "";
    readonly VERSION: string;
}>;
export declare enum HealthStatus {
    Degraded = "Degraded",
    Healthy = "Healthy",
    Unhealthy = "Unhealthy"
}
export declare enum LogLevel {
    Critical = "critical",
    Error = "error",
    Warning = "warning",
    Information = "information",
    Debug = "debug",
    Trace = "trace"
}
export declare function addAbortListener(signal: AbortSignal, listener: () => void): () => void;
export declare function addMillis(date: Date, millis: number): Date;
export declare function appendUrl(baseUrl: string, relativeUrl: string): string;
export declare function asReadonly<T>(items: T[]): readonly T[];
export declare function asReadonly<T>(obj: T): Readonly<T>;
export declare function asRequiredLabels<TLabels extends string>(counter: Counter<TLabels>): CounterWithRequiredLabels<TLabels>;
export declare function asRequiredLabels<TLabels extends string>(counter: Gauge<TLabels>): GaugeWithRequiredLabels<TLabels>;
export declare function asRequiredLabels<TLabels extends string>(counter: Histogram<TLabels>): HistogramWithRequiredLabels<TLabels>;
export declare function cacheFuncResult<T>(factory: () => T): () => T;
export declare function deduplicate<T>(getKey: (value: T) => string): (source: Wrappable<T>) => IterableIterator<T>;
export declare function deduplicate<T>(getKey: (value: T) => string, source: Wrappable<T>): IterableIterator<T>;
export declare function deepFreeze<T>(obj: T): DeepReadonly<T>;
export declare function dumpToString(value: ValueToDump): string;
export declare function dumpType(value: unknown): string;
export declare function entriesOf<TKey extends string, TValue>(record: Readonly<Partial<Record<TKey, TValue>>>): [TKey, TValue][];
export declare function equalsIgnoreCase(str1: string, str2: string): boolean;
export declare function errorObjToString(error: Error): string;
export declare function errorToString(error: unknown, options?: {
    onlyMessage?: boolean;
}): string;
export declare function findNonNullish<TSource, TResult>(sourceItems: Iterable<TSource>, mapItem: (item: TSource) => TResult | null | undefined): TResult | null;
export declare function freezeResult(): MethodDecorator;
export declare function fullCliOption(name: keyof typeof cliOptions, value?: string): string;
export declare function getConstructorName(value: NonNullish): string;
export declare function getEnumValues<T extends string>(enumType: Readonly<Record<string, T>>): T[];
export declare function getOrCreate<TKey, TValue extends NonNullish>(map: Map<TKey, TValue>, key: TKey, factory: (key: TKey) => TValue): TValue;
export declare function getVariableName(variableWrapper: Record<string, unknown>): string;
export declare function hasConstructor<T extends NonNullish>(value: unknown, type: Constructor<T>): value is T;
export declare function hasLength<T>(array: Nullish<T[]>, lengthToCheck: 1): array is [T];
export declare function hasLength<T>(array: Nullish<T[]>, lengthToCheck: 2): array is [T, T];
export declare function hasLength<T>(array: Nullish<T[]>, lengthToCheck: 3): array is [T, T, T];
export declare function hasLength<T>(array: Nullish<readonly T[]>, lengthToCheck: 1): array is readonly [T];
export declare function hasLength<T>(array: Nullish<readonly T[]>, lengthToCheck: 2): array is readonly [T, T];
export declare function hasLength<T>(array: Nullish<readonly T[]>, lengthToCheck: 3): array is readonly [T, T, T];
export declare function hasToJSON<T extends object>(obj: T): obj is T & ToJSON;
export declare function injectAllOptional<T>(token: InjectionToken<T>): ReturnType<typeof injectWithTransform>;
export declare function injectExplicit(argIndex?: number): ReturnType<typeof inject>;
export declare function injectLogger(category: LoggerCategory): ReturnType<typeof injectWithTransform>;
export declare function injectOptional<T>(token: InjectionToken<T>): ReturnType<typeof injectWithTransform>;
export declare function injectValue(value: unknown): ReturnType<typeof injectWithTransform>;
export declare function isAbortError(error: unknown): boolean;
export declare function isNonEmpty<T>(array: Nullish<T[]>): array is NonEmptyArray<T>;
export declare function isNonEmpty<T>(array: Nullish<readonly T[]>): array is Readonly<NonEmptyArray<T>>;
export declare function isNotNullish<TValue>(value: Nullish<TValue>): value is TValue;
export declare function isNullish<TValue>(value: Nullish<TValue>): value is null | undefined;
export declare function isObject(value: unknown): value is Record<string, unknown>;
export declare function isObjectLiteral(value: unknown): value is Record<string, unknown>;
export declare function isReadOnlyArray(value: unknown): value is readonly unknown[];
export declare function isWhiteSpace(str: Nullish<string>): str is null | undefined | '';
export declare function keyof<T>(key: keyof T & string): keyof T & string;
export declare function keysOf<TKey extends string>(record: Readonly<Partial<Record<TKey, unknown>>>): TKey[];
export declare function last<T>(array: Readonly<NonEmptyArray<T>>): T;
export declare function last<T>(array: readonly T[]): T | undefined;
export declare function listAllPropertyNames<T extends object>(obj: T): (keyof T & string)[];
export declare function locatePackageJsonInCurrentPackage(pathWithinPackage: string): string;
export declare function pascalCase(str: string): string;
export declare function registerBackgroundWorker(diContainer: DependencyContainer, backgroundWorkerProvider: Provider<BackgroundWorker>): void;
export declare function registerConfigWithDiagnostics(diContainer: DependencyContainer, configProvider: Provider<ConfigWithDiagnostics>): void;
export declare function registerDappetizerUtils(diContainer?: DependencyContainer): void;
export declare function registerExplicitConfigFilePath(diContainer: DependencyContainer, configFilePathProvider: Provider<string>): void;
export declare function registerHealthCheck(diContainer: DependencyContainer, healthCheckProvider: Provider<HealthCheck>): void;
export declare function registerHttpHandler(diContainer: DependencyContainer, httpHandlerProvider: Provider<HttpHandler>): void;
export declare function registerLoggedPackageInfo(diContainer: DependencyContainer, versionInfoProvider: Provider<LoggedPackageInfo>): void;
export declare function registerMetricsUpdater(diContainer: DependencyContainer, metricsUpdaterProvider: Provider<MetricsUpdater>): void;
export declare function registerOnce(diContainer: DependencyContainer, name: string, registerFunc: () => void): void;
export declare function registerPublicApi<T>(diContainer: DependencyContainer, publicToken: InjectionToken<T>, options: {
    useInternalToken: InjectionToken<T>;
    createProxy: (internalValue: T) => T;
}): void;
export declare function registerSingleton<T>(diContainer: DependencyContainer, token: InjectionToken<T>, provider: Provider<T>): void;
export declare function removeRequiredPrefix(str: string, prefixToRemove: string): string;
export declare function removeRequiredSuffix(str: string, suffixToRemove: string): string;
export declare function replaceAll(str: string, search: string, replacement: string): string;
export declare function reportUsageStatistics(diContainer: DependencyContainer, consoleCommand: string, additionalStats?: Attributes): Promise<void>;
export declare function resolveAllOptional<T extends NonNullish>(diContainer: DependencyContainer, token: InjectionToken<T>): T[];
export declare function resolveLogger(diContainer: DependencyContainer, category: LoggerCategory): Logger;
export declare function resolveOptional<T extends NonNullish>(diContainer: DependencyContainer, token: InjectionToken<T>): T | undefined;
export declare function resolveWithExplicitArgs<TResult extends NonNullish, TExplicitArgs extends unknown[]>(diContainer: DependencyContainer, resultType: new (...args: [...TExplicitArgs, ...any]) => TResult, // eslint-disable-line @typescript-eslint/no-explicit-any
explicitArgs: [...TExplicitArgs]): TResult;
export declare function runApp(diContainer?: DependencyContainer, indexerModuleIds?: readonly string[]): Promise<void>;
export declare function runInPerformanceSection<TResult>(logger: Logger, scopeFunc: () => TResult, performanceSection?: string): TResult;
export declare function safeJsonStringify(value: unknown, options?: {
    indent?: string | number;
}): string;
export declare function sanitizeForJson(value: unknown, stringSanitizer?: StringSanitizer): unknown;
export declare function throwIfAborted(signal: AbortSignal): void;
export declare function typed<T>(value: T): T;
export declare function withIndex<T>(iterable: Iterable<T>): Iterable<[T, number]>;
export interface AddressValidator extends Validator<string, readonly AddressPrefix[] | void> {
}
export interface AppShutdownManager {
    readonly signal: AbortSignal;
    shutdown(options?: {
        exitCode: number;
    }): Promise<void>;
}
export interface ArgGuard {
    address(value: string, argName: string, prefixes?: readonly AddressPrefix[]): string;
    function<T extends Function>(value: T, argName: string): T;
    object<T extends object>(value: T, argName: string, constructor?: Constructor<T>): T;
    array<T extends readonly unknown[]>(value: T, argName: string): T;
    nonEmptyArray<T extends readonly unknown[]>(value: T, argName: string): T;
    string(value: string, argName: string, limits?: StringLimits): string;
    nonEmptyString(value: string, argName: string): string;
    nonWhiteSpaceString(value: string, argName: string): string;
    number(value: number, argName: string, limits?: NumberLimits): number;
    oneOf<TSupported extends string, TValue extends TSupported>(value: TValue, argName: string, supportedValues: readonly TSupported[]): TValue;
    enum<TEnum extends string>(value: TEnum, argName: string, enumType: Readonly<Record<string, TEnum>>): TEnum;
    ifNotNullish<T>(value: Nullish<T>, validate: (value: T) => T): Nullish<T>;
}
export interface ArrayValidator extends Validator<readonly unknown[], ArrayLimits | void> {
}
export interface AsyncIterableProvider<TItem> {
    iterate(): AsyncIterableIterator<TItem>;
}
export interface BackgroundWorker {
    readonly name: string;
    start(): AsyncOrSync<void>;
    stop?(): AsyncOrSync<void>;
}
export interface Clock {
    getNowDate(): Date;
    getNowTimestamp(): number;
}
export interface ConfigWithDiagnostics {
    getDiagnostics(): [string, unknown];
}
export interface CounterWithRequiredLabels<TLabels extends string> {
    inc(labels: RequiredLabelValues<TLabels>): void;
}
export interface FileSystem {
    existsAsFile(filePath: string): Promise<boolean>;
    readFileTextIfExists(filePath: string): Promise<string | null>;
    writeFileText(filePath: string, contents: string): Promise<void>;
}
export interface GaugeWithRequiredLabels<TLabels extends string> {
    set(labels: RequiredLabelValues<TLabels>, value: number): void;
}
export interface HealthCheck {
    readonly name: string;
    checkHealth(): AsyncOrSync<HealthCheckResult>;
}
export interface HealthCheckResult {
    readonly status: HealthStatus;
    readonly data: unknown;
    readonly evaluatedTime?: Date;
}
export interface HealthDappetizerConfig {
    healthStatusCacheSeconds?: number;
}
export interface HistogramWithRequiredLabels<TLabels extends string> {
    observe(labels: RequiredLabelValues<TLabels>, value: number): void;
}
export interface HttpFetcher {
    fetch(url: RequestInfo, init?: RequestOptions): Promise<Response>;
}
export interface HttpHandler {
    readonly urlPath: string;
    handle(request: ExpressRequest, response: ExpressResponse): AsyncOrSync<void>;
}
export interface HttpServerDappetizerConfig {
    enabled?: boolean;
    host?: string;
    port?: number;
}
export interface JsonFetchCommand<TResponse> {
    readonly description: string;
    readonly url: string;
    readonly requestOptions?: RequestOptions;
    transformResponse(rawResponse: unknown): TResponse;
}
export interface JsonFetcher {
    execute<TResponse>(command: JsonFetchCommand<TResponse>): Promise<TResponse>;
}
export interface LogEntry {
    readonly timestamp: Date;
    readonly category: string;
    readonly level: LogLevel;
    readonly message: string;
    readonly data: LogData;
    readonly globalData: unknown;
    readonly packages: unknown;
}
export interface LoggedPackageInfo {
    readonly name: string;
    readonly commitHash: string;
    readonly version: string;
}
export interface Logger {
    readonly category: string;
    readonly isCriticalEnabled: boolean;
    readonly isErrorEnabled: boolean;
    readonly isWarningEnabled: boolean;
    readonly isInformationEnabled: boolean;
    readonly isDebugEnabled: boolean;
    readonly isTraceEnabled: boolean;
    isEnabled(level: LogLevel): boolean;
    log(level: LogLevel, message: string, data?: LogData): void;
    runWithData<TResult>(data: LogData, scopeFunc: () => TResult): TResult;
    logCritical(message: string, data?: LogData): void;
    logError(message: string, data?: LogData): void;
    logWarning(message: string, data?: LogData): void;
    logInformation(message: string, data?: LogData): void;
    logDebug(message: string, data?: LogData): void;
    logTrace(message: string, data?: LogData): void;
}
export interface LoggerFactory {
    getLogger(category: LoggerCategory): Logger;
}
export interface LoggingDappetizerConfig {
    console?: {
        minLevel?: 'off' | 'critical' | 'error' | 'warning' | 'information' | 'debug' | 'trace';
        format?: 'json' | 'messages' | 'coloredMessages' | 'coloredSimpleMessages';
    };
    file?: {
        minLevel?: 'off' | 'critical' | 'error' | 'warning' | 'information' | 'debug' | 'trace';
        path?: string;
        format?: 'json' | 'messages' | 'coloredMessages' | 'coloredSimpleMessages';
        maxSize?: string;
        maxFiles?: string;
    };
    globalData?: unknown;
    maxStringLength?: number;
}
export interface LongExecutionHelper {
    warn<TResult>(operation: LongExecutionOperation<TResult>): Promise<TResult>;
}
export interface LongExecutionOperation<TResult = unknown> {
    readonly description: string;
    execute(): Promise<TResult>;
}
export interface MetricsUpdater {
    update(): AsyncOrSync<void>;
}
export interface NumberLimits {
    readonly integer?: boolean;
    readonly min?: number;
    readonly max?: number;
}
export interface NumberValidator extends Validator<number, NumberLimits | void> {
}
export interface OneOfSupportedValidator extends Validator<string, readonly string[]> {
    validate<TValue extends string>(value: unknown, supportedValues: readonly TValue[]): TValue;
}
export interface PackageJson {
    getVersion(): string;
    getDependencyVersion(dependencyName: string): string;
    getDevDependencyVersion(dependencyName: string): string;
}
export interface PackageJsonLoader {
    load(filePath: string): PackageJson;
}
export interface RootLogger {
    isEnabled(level: LogLevel): boolean;
    log(entry: LogEntry): void;
    close(): Promise<void>;
}
export interface RootLoggerFactory {
    create(): RootLogger;
}
export interface RpcMempoolOperationGroup {
    protocol: string;
    branch: string;
    signature: string;
    contents: (taquitoRpc.OperationContents | taquitoRpc.OperationContentsAndResult)[];
    chain_id?: undefined;
    hash?: undefined;
}
export interface RpcMonitorBlockHeader {
    hash: string;
    level: number;
    proto: number;
    predecessor: string;
    timestamp: string;
    validation_pass: number;
    operations_hash: string;
    fitness: string[];
    context: string;
    protocol_data: string;
}
export interface SleepHelper {
    sleep(sleepMillis: number): Promise<void>;
}
export interface StringSanitizer {
    sanitizeString(str: string): string;
}
export interface StringValidator extends Validator<string, StringLimits | void> {
}
export interface TaquitoHttpRequest {
    data: object | string | undefined;
    headers: Record<string, string>;
    json: boolean;
    method: 'GET' | 'POST';
    signal: AbortSignal | undefined;
    timeout: number | undefined;
    url: string;
}
export interface TezosNodeDappetizerConfig {
    url: string;
    retryDelaysMillis?: number[];
    monitor?: {
        reconnectIfStuckMillis?: number;
        chunkCount?: {
            warn?: number;
            fail?: number;
        };
    };
    health?: {
        watcherCacheMillis?: number;
        resultCacheMillis?: number;
        timeoutMillis?: number;
        unhealthyAfterMillis?: number;
    };
    timeoutMillis?: number;
}
export interface TimeDappetizerConfig {
    longExecutionWarningMillis?: number;
}
export interface Timeout {
    clear(): void;
}
export interface TimerHelper {
    setTimeout(delayMillis: number, callback: () => void): Timeout;
}
export interface ToJSON {
    toJSON(parentIndex: string): unknown;
}
export interface TypeValidator extends Validator<unknown, TypeOfName> {
    validate(value: unknown, expectedType: 'bigint'): bigint;
    validate(value: unknown, expectedType: 'boolean'): boolean;
    validate(value: unknown, expectedType: 'function'): Function;
    validate(value: unknown, expectedType: 'number'): number;
    validate(value: unknown, expectedType: 'object'): object;
    validate(value: unknown, expectedType: 'string'): string;
    validate(value: unknown, expectedType: 'symbol'): symbol;
    validate(value: unknown, expectedType: 'undefined'): undefined;
    validate(value: unknown, expectedType: TypeOfName): unknown;
}
export interface UsageStatisticsDappetizerConfig {
    enabled?: boolean;
}
export interface Validator<TValue, TLimits> {
    validate(value: unknown, limits: TLimits): TValue;
    getExpectedValueDescription(limits: TLimits): string;
}
export type AddressPrefix = 'tz' | 'tz1' | 'tz2' | 'tz3' | 'KT1' | 'txr1' | 'sr1';
export type ArrayLimits = 'NotEmpty';
export type Constructor<T extends NonNullish = NonNullish> = new (...args: any[]) => T;
export type DefaultConstructor<T extends NonNullish = NonNullish> = new () => T;
export type KeyOfType<TObject extends NonNullish, TProperty> = {
    [P in keyof TObject]: TObject[P] extends TProperty ? P : never;
}[keyof TObject];
export type LogData = {
    readonly [key: string]: unknown;
};
export type LoggerCategory = string | Constructor;
export type NonNullish = {};
export type Nullish<T> = T | null | undefined;
export type RecordToDump = {
    readonly [key: string]: ValueToDump;
};
export type RequestOptions = StrictOmit<RequestInit, 'signal'> & {
    signal?: AbortSignal | null;
};
export type RequiredLabelValues<T extends string> = Record<T, string | number>;
export type RpcBlockMonitor = AsyncIterableProvider<RpcMonitorBlockHeader>;
export type RpcMempoolMonitor = AsyncIterableProvider<RpcMempoolOperationGroup>;
export type StringKeyOfType<TObject extends NonNullish, TProperty> = KeyOfType<TObject, TProperty> & string;
export type StringLimits = 'NotEmpty' | 'NotWhiteSpace';
export type TypeOfName = 'bigint' | 'boolean' | 'function' | 'number' | 'object' | 'string' | 'symbol' | 'undefined';
export type ValueToDump = undefined | null | BigNumber | boolean | Date | number | string | readonly ValueToDump[] | RecordToDump;