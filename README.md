# Tezos Dappetizer

[Dappetizer](https://dappetizer.dev) is a framework for building Tezos indexer apps using TypeScript or JavaScript.

To learn more about using Dappetizer, please check out https://docs.dappetizer.dev/

## Developing Dappetizer

### Prerequisites

- [Node 16.3](https://nodejs.org/) or above.
- NPM 7+
   - You may need to run: `npm install -g npm`

### Initial steps

1. `npm install`

### Building

1. Run `npm run build`.

### Running demos

1. Run `docker-compose up -d` in `services` folder to run PostgreSQL database in the background (if the demo requires database).
2. Run `npm run build`.
3. Run one of the demos:
   - `npm run demo-console`
   - `npm run demo-database`

## Disclaimer

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
