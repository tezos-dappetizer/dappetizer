const glob = require('glob');

const excludedPackages = ['cli', 'generator', 'taqueria'];
const entryPoints = glob.sync(`packages/!(${excludedPackages.join('|')})/src/public-api.ts`);

module.exports = {
    categorizeByGroup: false,
    entryPoints,
    out: 'dist/typedoc',
    readme: 'README.md',
    name: 'Dappetizer',
    excludePrivate: true,
    excludeInternal: true,
    treatWarningsAsErrors: true,
    disableSources: true,
};
