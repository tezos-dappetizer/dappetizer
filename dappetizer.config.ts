import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';

export const config: DappetizerConfigUsingDb = {
    modules: [
        { id: '.' }, // The dir where re-exported must have package.json which defines an indexer module.
    ],
    networks: {
        mainnet: {
            indexing: {
                fromBlockLevel: 3504523,
            },
            tezosNode: {
                url: process.env.TEZOS_NODE_URL || 'https://prod.tcinfra.net/rpc/mainnet/',
                // 'https://mainnet-tezos.giganode.io',
                // https://api.tezos.org.ua
                // https://testnet-tezos.giganode.io
            },
        },
    },
    database: {
        type: 'postgres',
        host: 'localhost',
        port: 5432,
        username: 'postgres',
        password: 'postgrespassword',
        database: 'postgres',
        schema: 'app_indexer_demo',
    },
    hasura: {
        url: 'http://localhost:8080',
        autotrackEntities: true,
    },
    httpServer: {
        enabled: true,
    },
};

export default config;
