import {
    BalanceUpdate,
    BalanceUpdateIndexingContext,
    Block,
    BlockDataIndexer,
    BlockIndexingContext,
    MainOperation,
    MainOperationIndexingContext,
    OperationGroup,
    OperationGroupIndexingContext,
    OperationWithResult,
    OperationWithResultIndexingContext,
} from '@tezos-dappetizer/indexer';

import { IntContextData } from './int-context-data';
import { IntDbContext, verifyContextDataFlow } from './int-db-context';

export class IntBlockDataIndexer implements BlockDataIndexer<IntDbContext, IntContextData> {
    indexBlock(
        block: Block,
        dbContext: IntDbContext,
        indexingContext: BlockIndexingContext<IntContextData>,
    ): void {
        verifyContextDataFlow(dbContext, indexingContext.data);

        dbContext.executedMethods.push({
            method: this.getMethodName('indexBlock'),
            block,
            indexingContext: { // Grab sample properties from indexingContext to verify it.
                data: indexingContext.data,
            },
        });
        dbContext.bdd.blocks.push({ hash: block.hash });
    }

    async indexOperationGroup(
        operationGroup: OperationGroup,
        dbContext: IntDbContext,
        indexingContext: OperationGroupIndexingContext<IntContextData>,
    ): Promise<void> {
        verifyContextDataFlow(dbContext, indexingContext.data);

        dbContext.executedMethods.push({
            method: this.getMethodName('indexOperationGroup'),
            operationGroup,
            indexingContext: { // Grab sample properties from indexingContext to verify it.
                data: indexingContext.data,
                blockHash: indexingContext.block.hash,
            },
        });
        dbContext.bdd.operationGroups.push({ hash: operationGroup.hash });
        return Promise.resolve(); // To test async.
    }

    indexMainOperation(
        mainOperation: MainOperation,
        dbContext: IntDbContext,
        indexingContext: MainOperationIndexingContext<IntContextData>,
    ): void {
        verifyContextDataFlow(dbContext, indexingContext.data);

        dbContext.executedMethods.push({
            method: this.getMethodName('indexMainOperation'),
            mainOperation,
            indexingContext: { // Grab sample properties from indexingContext to verify it.
                data: indexingContext.data,
                blockHash: indexingContext.block.hash,
                operationGroupHash: indexingContext.operationGroup.hash,
            },
        });
        dbContext.bdd.operations.push({
            kind: mainOperation.kind,
            operationGroupHash: indexingContext.operationGroup.hash,
        });
    }

    async indexOperationWithResult(
        operationWithResult: OperationWithResult,
        dbContext: IntDbContext,
        indexingContext: OperationWithResultIndexingContext<IntContextData>,
    ): Promise<void> {
        verifyContextDataFlow(dbContext, indexingContext.data);

        dbContext.executedMethods.push({
            method: this.getMethodName('indexOperationWithResult'),
            operationWithResult,
            indexingContext: { // Grab sample properties from indexingContext to verify it.
                data: indexingContext.data,
                blockHash: indexingContext.block.hash,
                operationGroupHash: indexingContext.operationGroup.hash,
                mainOperationKind: indexingContext.mainOperation.kind,
            },
        });
        dbContext.bdd.operationResults.push({
            status: operationWithResult.result.status,
            operationKind: operationWithResult.kind,
            operationGroupHash: indexingContext.operationGroup.hash,
        });
        return Promise.resolve(); // To test async.
    }

    indexBalanceUpdate(
        balanceUpdate: BalanceUpdate,
        dbContext: IntDbContext,
        indexingContext: BalanceUpdateIndexingContext<IntContextData>,
    ): void {
        verifyContextDataFlow(dbContext, indexingContext.data);

        dbContext.executedMethods.push({
            method: this.getMethodName('indexBalanceUpdate'),
            balanceUpdate,
            indexingContext: { // Grab sample properties from indexingContext to verify it.
                data: indexingContext.data,
                blockHash: indexingContext.block.hash,
                operationGroupHash: indexingContext.operationGroup?.hash ?? null,
                mainOperationKind: indexingContext.mainOperation?.kind ?? null,
                operationWithBalanceUpdateKind: indexingContext.internalOperation?.kind ?? indexingContext.mainOperation?.kind ?? null,
            },
        });
        dbContext.bdd.balanceUpdates.push({
            kind: balanceUpdate.kind,
            contractOrDelegate: balanceUpdate.contractAddress ?? balanceUpdate.delegateAddress,
            operationGroupHash: indexingContext.operationGroup?.hash ?? '(block metadata)',
        });
    }

    private getMethodName(methodName: keyof typeof this & string): string {
        return `${this.constructor.name}.${methodName}`;
    }
}
