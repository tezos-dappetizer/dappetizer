import {
    BigMapDiff,
    BigMapDiffIndexingContext,
    BigMapUpdate,
    BigMapUpdateIndexingContext,
    ContractIndexer,
    ContractIndexingContext,
    ContractOrigination,
    LazyContract,
    OriginationIndexingContext,
    StorageChange,
    StorageChangeIndexingContext,
    TransactionIndexingContext,
    TransactionParameter,
} from '@tezos-dappetizer/indexer';
import { isNullish, keyof } from '@tezos-dappetizer/utils';

import { IntContextData } from './int-context-data';
import { IntDbContext, verifyContextDataFlow } from './int-db-context';

interface ContractData {
    bar: string;
}

export class IntContractIndexer implements ContractIndexer<IntDbContext, IntContextData, ContractData> {
    async shouldIndex(lazyContract: LazyContract): Promise<ContractData> {
        return Promise.resolve({ bar: `contract-data-${lazyContract.address}` });
    }

    async indexOrigination(
        origination: ContractOrigination,
        dbContext: IntDbContext,
        indexingContext: OriginationIndexingContext<IntContextData, ContractData>,
    ): Promise<void> {
        await Promise.resolve();
        verifyContextDataFlow(dbContext, indexingContext.data);

        dbContext.executedMethods.push({
            method: `${this.constructor.name}.${keyof<typeof this>('indexOrigination')}`,
            origination,
            indexingContext: grabIndexingContext(indexingContext),
        });
        dbContext.bdd.contractOriginations.push({
            contractAddress: indexingContext.contract.address,
            operationGroupHash: indexingContext.operationGroup.hash,
        });
    }

    async indexTransaction(
        transactionParameter: TransactionParameter,
        dbContext: IntDbContext,
        indexingContext: TransactionIndexingContext<IntContextData, ContractData>,
    ): Promise<void> {
        await Promise.resolve();
        verifyContextDataFlow(dbContext, indexingContext.data);

        dbContext.executedMethods.push({
            method: `${this.constructor.name}.${keyof<typeof this>('indexTransaction')}`,
            transactionParameter,
            indexingContext: {
                ...grabIndexingContext(indexingContext),
                transactionParameterEntrypoint: indexingContext.transactionParameter.entrypoint,
            },
        });
        dbContext.bdd.contractTransactions.push({
            entrypoint: transactionParameter.entrypoint,
            contractAddress: indexingContext.contract.address,
            operationGroupHash: indexingContext.operationGroup.hash,
        });
    }

    async indexStorageChange(
        storageChange: StorageChange,
        dbContext: IntDbContext,
        indexingContext: StorageChangeIndexingContext<IntContextData, ContractData>,
    ): Promise<void> {
        await Promise.resolve();
        verifyContextDataFlow(dbContext, indexingContext.data);

        dbContext.executedMethods.push({
            method: `${this.constructor.name}.${keyof<typeof this>('indexStorageChange')}`,
            storageChange,
            indexingContext: {
                ...grabIndexingContext(indexingContext),
                storageChangeMichelsonType: Object.keys(indexingContext.storageChange.newValue.michelson),
            },
        });
        dbContext.bdd.contractStorageChanges.push({
            contractAddress: indexingContext.contract.address,
            operationGroupHash: indexingContext.operationGroup.hash,
        });
    }

    async indexBigMapDiff(
        bigMapDiff: BigMapDiff,
        dbContext: IntDbContext,
        indexingContext: BigMapDiffIndexingContext<IntContextData, ContractData>,
    ): Promise<void> {
        await Promise.resolve();
        verifyContextDataFlow(dbContext, indexingContext.data);

        dbContext.executedMethods.push({
            method: `${this.constructor.name}.${keyof<typeof this>('indexBigMapDiff')}`,
            bigMapDiff,
            indexingContext: {
                ...grabIndexingContext(indexingContext),
                bigMapDiffAction: indexingContext.bigMapDiff.action,
            },
        });
        dbContext.bdd.contratBigMapDiffs.push({
            action: bigMapDiff.action,
            contractAddress: indexingContext.contract.address,
            operationGroupHash: indexingContext.operationGroup.hash,
        });
    }

    async indexBigMapUpdate(
        bigMapUpdate: BigMapUpdate,
        dbContext: IntDbContext,
        indexingContext: BigMapUpdateIndexingContext<IntContextData, ContractData>,
    ): Promise<void> {
        await Promise.resolve();
        verifyContextDataFlow(dbContext, indexingContext.data);

        dbContext.executedMethods.push({
            method: this.getMethodName('indexBigMapUpdate'),
            bigMapUpdate,
            indexingContext: {
                ...grabIndexingContext(indexingContext),
                bigMapDiffId: indexingContext.bigMapDiff.bigMap?.lastKnownId ?? null,
            },
        });
    }

    private getMethodName(methodName: keyof typeof this & string): string {
        return `${this.constructor.name}.${methodName}`;
    }
}

function grabIndexingContext(context: ContractIndexingContext<IntContextData, ContractData>): object {
    return { // Grab sample properties from indexingContext to verify it.
        data: context.data,
        blockHash: context.block.hash,
        operationGroupHash: context.operationGroup.hash,
        mainOperationKind: context.mainOperation.kind,
        operationWithBalanceUpdateKind: context.operationWithResult.kind,
        contractAddress: context.contract.address,
        contractEntrypointCount: Object.keys(context.contract.abstraction.entrypoints.entrypoints).length,
        contractData: context.contractData,

        // Grab lazily resolved values.
        lazyTransactionParameterPath: context.transactionParameter?.entrypointPath ?? null,
        lazyStorageChangeIsNullish: isNullish(context.storageChange?.newValue),
        bigMapDiffCount: context.bigMapDiffs.length,
    };
}
