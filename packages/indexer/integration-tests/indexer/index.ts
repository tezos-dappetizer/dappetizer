import { IndexerModuleFactory, registerIndexerDatabaseHandler } from '@tezos-dappetizer/indexer';

import { IntBlockDataIndexer } from './int-block-data-indexer';
import { IntContextData } from './int-context-data';
import { IntContractIndexer } from './int-contract-indexer';
import { IntDbContext } from './int-db-context';
import { IntIndexerDatabaseHandler } from './int-indexer-database-handler';
import { IntIndexingCycleHandler } from './int-indexing-cycle-handler';

export const indexerModule: IndexerModuleFactory<IntDbContext, IntContextData> = options => {
    const outDir = options.configElement.asObject().get('outDir').asString('NotWhiteSpace');
    registerIndexerDatabaseHandler(options.diContainer, { useValue: new IntIndexerDatabaseHandler(outDir) });

    return {
        name: 'IntegrationTests',
        indexingCycleHandler: new IntIndexingCycleHandler(),
        blockDataIndexers: [new IntBlockDataIndexer()],
        contractIndexers: [new IntContractIndexer()],
    };
};
