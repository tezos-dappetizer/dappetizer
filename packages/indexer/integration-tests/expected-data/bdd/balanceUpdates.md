# Story: Indexing of Balance Updates

**As** an indexer developer using Dappetizer,  
**When** indexing a block  
**I want** Dappetizer to call my indexer code providing respective balance updates,  
**So that** I can index them.

## Scenario: Indexing Block 5771696

**When** Dappetizer indexes block 5771696  
**Then** Dappetizer calls my index method with respective data one-by-one as follows:

| Kind | Contract Or Delegate | Operation Group Hash |
| --- | --- | --- |
| Accumulator |  | (block metadata) |
| Contract | tz3ZbP2pM3nwvXztbuMJwJnDvV5xdpU8UkkD | (block metadata) |
| Minted |  | (block metadata) |
| Freezer |  | (block metadata) |
| Minted |  | (block metadata) |
| Contract | tz3ZbP2pM3nwvXztbuMJwJnDvV5xdpU8UkkD | (block metadata) |
| Minted |  | (block metadata) |
| Freezer |  | (block metadata) |
| Minted |  | (block metadata) |
| Contract | tz3ZbP2pM3nwvXztbuMJwJnDvV5xdpU8UkkD | (block metadata) |
| Contract | tz1QLWXD5Ci93Kqzo1cPCzQU6Q3w5TXbjPrV | ongp9yC7eBYYQxScswxb3uxRg2EDvgtBDZMFUg6P15aM3dLfwzY |
| Accumulator |  | ongp9yC7eBYYQxScswxb3uxRg2EDvgtBDZMFUg6P15aM3dLfwzY |
| Contract | tz1PwTyhYTyi7iaXFjATa1TiJvb27ynvj4Ge | ootfQ18AU3K5XWyEC81AMCbxX2QFY3doJw5D3aD53gFPopHJqvW |
| Burned |  | ootfQ18AU3K5XWyEC81AMCbxX2QFY3doJw5D3aD53gFPopHJqvW |
| Contract | KT1CeFqjJRJPNVvhvznQrWfHad2jCiDZ6Lyj | ootfQ18AU3K5XWyEC81AMCbxX2QFY3doJw5D3aD53gFPopHJqvW |
| Contract | tz1XrutuvkFRG15HmV2gdon86F38NMMGMAXr | ootfQ18AU3K5XWyEC81AMCbxX2QFY3doJw5D3aD53gFPopHJqvW |
| Contract | tz1PwTyhYTyi7iaXFjATa1TiJvb27ynvj4Ge | ootfQ18AU3K5XWyEC81AMCbxX2QFY3doJw5D3aD53gFPopHJqvW |
| Accumulator |  | ootfQ18AU3K5XWyEC81AMCbxX2QFY3doJw5D3aD53gFPopHJqvW |
| Contract | tz1cMWTNwecApUicCrHfTRwHEhBcZGjkUwCw | opVEV5j7S2F2qWfSA4324tFc5GhFgJwy6du1hJFKTKvaB3E3Dez |
| Accumulator |  | opVEV5j7S2F2qWfSA4324tFc5GhFgJwy6du1hJFKTKvaB3E3Dez |
| Contract | tz1RcNpckrQ61pNjgHhmq5qcoqukNfQhd4nh | onm1EFR1dc4dPLTosv7im2GySVxS6YiMEGR5WwGN7iKp6JXgbfs |
| Contract | KT1GbyoDi7H1sfXmimXpptZJuCdHMh66WS9u | onm1EFR1dc4dPLTosv7im2GySVxS6YiMEGR5WwGN7iKp6JXgbfs |
| Contract | tz1RcNpckrQ61pNjgHhmq5qcoqukNfQhd4nh | onm1EFR1dc4dPLTosv7im2GySVxS6YiMEGR5WwGN7iKp6JXgbfs |
| Accumulator |  | onm1EFR1dc4dPLTosv7im2GySVxS6YiMEGR5WwGN7iKp6JXgbfs |

## Scenario: Indexing Block 5771697

**When** Dappetizer indexes block 5771697  
**Then** Dappetizer calls my index method with respective data one-by-one as follows:

| Kind | Contract Or Delegate | Operation Group Hash |
| --- | --- | --- |
| Accumulator |  | (block metadata) |
| Contract | tz3S6BBeKgJGXxvLyZ1xzXzMPn11nnFtq5L9 | (block metadata) |
| Minted |  | (block metadata) |
| Freezer |  | (block metadata) |
| Minted |  | (block metadata) |
| Contract | tz3S6BBeKgJGXxvLyZ1xzXzMPn11nnFtq5L9 | (block metadata) |
| Minted |  | (block metadata) |
| Freezer |  | (block metadata) |
| Minted |  | (block metadata) |
| Contract | tz3S6BBeKgJGXxvLyZ1xzXzMPn11nnFtq5L9 | (block metadata) |
| Contract | tz1cMWTNwecApUicCrHfTRwHEhBcZGjkUwCw | opT9uWRsK5A5j7bDEyA3iCsddTNCg9oERKrpn4DncTdbgGPcyHj |
| Accumulator |  | opT9uWRsK5A5j7bDEyA3iCsddTNCg9oERKrpn4DncTdbgGPcyHj |
