# Story: Indexing of Blocks

**As** an indexer developer using Dappetizer,  
**When** indexing a block  
**I want** Dappetizer to call my indexer code providing respective blocks,  
**So that** I can index them.

## Scenario: Indexing Block 5771696

**When** Dappetizer indexes block 5771696  
**Then** Dappetizer calls my index method with respective data one-by-one as follows:

| Hash |
| --- |
| BLEKp1bT2GDETbDjxQpjrBcTZwovTTZVzBEUsCCVn5YhV1xP9xU |

## Scenario: Indexing Block 5771697

**When** Dappetizer indexes block 5771697  
**Then** Dappetizer calls my index method with respective data one-by-one as follows:

| Hash |
| --- |
| BLxK7jPxTRWHgMNjYtRoUhstHgPiTntvMFHNjHvwUR1xEs8pJgg |
