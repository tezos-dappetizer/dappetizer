# Story: Indexing of Contrat Big Map Diffs

**As** an indexer developer using Dappetizer,  
**When** indexing a block  
**I want** Dappetizer to call my indexer code providing respective contrat big map diffs,  
**So that** I can index them.

## Scenario: Indexing Block 5771696

**When** Dappetizer indexes block 5771696  
**Then** Dappetizer calls my index method with respective data one-by-one as follows:

| Action | Contract Address | Operation Group Hash |
| --- | --- | --- |
| Update | KT1GbyoDi7H1sfXmimXpptZJuCdHMh66WS9u | onm1EFR1dc4dPLTosv7im2GySVxS6YiMEGR5WwGN7iKp6JXgbfs |
