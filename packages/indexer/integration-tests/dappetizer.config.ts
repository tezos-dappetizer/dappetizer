import { DappetizerConfig } from '@tezos-dappetizer/indexer';
import path from 'path';

import { tezosNodeUrlForIntegrationTests } from '../../../test-utilities/int-tezos-node';

const config: DappetizerConfig = {
    modules: [
        {
            id: './indexer/dist',
            config: { outDir: path.resolve(__dirname, '.tmp-received-data') },
        },
    ],
    networks: {
        mainnet: {
            indexing: {
                fromBlockLevel: 5771696,
                toBlockLevel: 5771698,
                retryDelaysMillis: [],
                retryIndefinitely: false,
            },
            tezosNode: {
                url: tezosNodeUrlForIntegrationTests,
            },
        },
    },
    usageStatistics: {
        enabled: false,
    },
};

export default config;
