import fetch from 'node-fetch';

import { expectToThrowAsync } from '../../../test-utilities/mocks';
import { JsonFetcherImpl } from '../../utils/src/http-fetch/json-fetcher-impl';
import { LastIndexedLevelQuery } from '../src/block-source/contract-boost/tzkt/queries/last-indexed-level-query';
import { OriginationBlockLevelsQuery } from '../src/block-source/contract-boost/tzkt/queries/origination-block-levels-query';
import { TransactionBlockLevelsQuery } from '../src/block-source/contract-boost/tzkt/queries/transaction-block-levels-query';
import { DEFAULT_TZKT_URL } from '../src/config/indexing/indexing-config';

describe('TzKT', () => {
    let fetcher: JsonFetcherImpl;

    const tezosDomainsAddress = 'KT191reDVKrLxU9rjTSxg53wRqj6zh8pnHgr';

    beforeAll(() => {
        fetcher = new JsonFetcherImpl({ fetch } as any);
    });

    describe(LastIndexedLevelQuery.name, () => {
        it('should get data correctly', async () => {
            const query = new LastIndexedLevelQuery(DEFAULT_TZKT_URL);

            const block = await fetcher.execute(query);

            expect(block.level).toBeGreaterThan(2_000_000);
            expect(block.chainId).toMatch(/\w+/u);
        });
    });

    describe(OriginationBlockLevelsQuery.name, () => {
        it('should get data correctly', async () => {
            const query = new OriginationBlockLevelsQuery(
                DEFAULT_TZKT_URL,
                [tezosDomainsAddress],
                { fromBlockLevel: 0 },
            );

            const levels = await fetcher.execute(query);

            expect(levels).toEqual([1417351]);
        });
    });

    describe(TransactionBlockLevelsQuery.name, () => {
        it('should get data correctly', async () => {
            const query = new TransactionBlockLevelsQuery(
                DEFAULT_TZKT_URL,
                [tezosDomainsAddress],
                { fromBlockLevel: 1468136 },
            );

            const transactions = await fetcher.execute(query);

            expect(transactions.levels).toHaveLength(10_000);
            expect(transactions.levels.slice(0, 5)).toEqual([1468136, 1468136, 1468137, 1468137, 1468138]);
            expect(transactions.hasMore).toBeTrue();
        });
    });

    it('should throw if invalid request', async () => {
        const query = new TransactionBlockLevelsQuery(
            DEFAULT_TZKT_URL,
            [tezosDomainsAddress],
            { fromBlockLevel: 'wtf' as any },
        );

        const error = await expectToThrowAsync(async () => fetcher.execute(query));

        expect(error.message).toIncludeMultiple([DEFAULT_TZKT_URL, 'wtf']);
    });
});
