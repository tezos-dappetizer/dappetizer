import fs from 'fs';
import path from 'path';

import { deleteFileOrDir, readFileJson, readFileLines, runDappetizer } from '../../../test-utilities/mocks';

const paths = {
    configFile: `${__dirname}/dappetizer.config.ts`,
    expectedDataDir: `${__dirname}/expected-data`,
    receivedDataDir: `${__dirname}/.tmp-received-data`,
} as const;

describe('integration tests', () => {
    const expectedRelFiles = Array.from(listRelFiles(paths.expectedDataDir));
    let actualRelFiles: string[];

    beforeAll(async () => {
        deleteFileOrDir(paths.receivedDataDir);

        await runDappetizer(['start', paths.configFile], { expectedExitCode: 1 });

        actualRelFiles = Array.from(listRelFiles(paths.receivedDataDir));
    });

    it.each(expectedRelFiles)('should match indexed data in file: %s', relFile => {
        const readFile = path.extname(relFile) === '.json' ? readFileJson : readFileLines;
        const expectedJson = readFile(path.resolve(paths.expectedDataDir, relFile));
        const actualJson = readFile(path.resolve(paths.receivedDataDir, relFile));

        expect(actualJson).toEqual(expectedJson);
    });

    it('there should NOT be unexpected indexed data', () => {
        const unexpectedFilePaths = actualRelFiles.filter(f => !expectedRelFiles.includes(f));

        expect(unexpectedFilePaths).toBeEmpty();
    });
});

function *listRelFiles(dirPath: string): Iterable<string> {
    for (const item of fs.readdirSync(dirPath, { withFileTypes: true })) {
        if (item.isFile()) {
            yield item.name;
        } else if (item.isDirectory()) {
            for (const relFile of listRelFiles(path.resolve(dirPath, item.name))) {
                yield `${item.name}/${relFile}`;
            }
        }
    }
}
