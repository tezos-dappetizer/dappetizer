import { BLOCK_COUNT, getConfig } from './dappetizer.base.config';

export default getConfig({
    protocol: 'granada',
    firstLevel: 1_589_248,
    lastLevel: 1_916_928,

    // Aliquot according to 327_680 blocks of this protocol vs other protocols.
    levelRangeSizeToGenerate: 3 * BLOCK_COUNT,
});
