import { DappetizerConfig } from '@tezos-dappetizer/indexer';

import { tezosNodeUrlForIntegrationTests } from '../../../../test-utilities/int-tezos-node';

const config: DappetizerConfig = {
    modules: [
        { id: '../indexer/dist' },
    ],
    networks: {
        mainnet: {
            indexing: {
                fromBlockLevel: 2_000_000,
                toBlockLevel: 2_010_000,
                retryIndefinitely: false,
                retryDelaysMillis: [],
            },
            tezosNode: {
                url: process.env.TEZOS_NODE_URL_2 || tezosNodeUrlForIntegrationTests,
            },
        },
    },
    logging: {
        console: {
            minLevel: 'warning',
        },
    },
    time: {
        longExecutionWarningMillis: 5_000,
    },
    usageStatistics: {
        enabled: false,
    },
};

export default config;
