import { BLOCK_COUNT, getConfig } from './dappetizer.base.config';

export default getConfig({
    protocol: 'delphi',
    firstLevel: 1_212_417,
    lastLevel: 1_343_488,

    // Aliquot according to 131_071 blocks of this protocol vs other protocols.
    levelRangeSizeToGenerate: 1 * BLOCK_COUNT,
});
