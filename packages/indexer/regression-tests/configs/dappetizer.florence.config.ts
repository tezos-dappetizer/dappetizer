import { BLOCK_COUNT, getConfig } from './dappetizer.base.config';

export default getConfig({
    protocol: 'florence',
    firstLevel: 1_466_368,
    lastLevel: 1_589_247,

    // Aliquot according to 122_879 blocks of this protocol vs other protocols.
    levelRangeSizeToGenerate: 1 * BLOCK_COUNT,
});
