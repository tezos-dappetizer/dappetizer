import { BLOCK_COUNT, getConfig } from './dappetizer.base.config';

export default getConfig({
    protocol: 'carthage',
    firstLevel: 851_969,
    lastLevel: 1_212_416,

    // Aliquot according to 360_447 blocks of this protocol vs other protocols.
    levelRangeSizeToGenerate: 3 * BLOCK_COUNT,
});
