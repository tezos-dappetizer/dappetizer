import { BLOCK_COUNT, getConfig } from './dappetizer.base.config';

export default getConfig({
    protocol: 'hangzhou',
    firstLevel: 1_916_929,
    lastLevel: 2_244_608,

    // Aliquot according to 327_679 blocks of this protocol vs other protocols.
    levelRangeSizeToGenerate: 3 * BLOCK_COUNT,
});
