import { BLOCK_COUNT, getConfig } from './dappetizer.base.config';

export default getConfig({
    protocol: 'jakarta',
    firstLevel: 2_490_369,
    lastLevel: 2_736_128,

    // Aliquot according to 245_760 blocks of this protocol vs other protocols.
    levelRangeSizeToGenerate: 2 * BLOCK_COUNT,
});
