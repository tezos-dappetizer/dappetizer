import { anything, instance, mock, verify, when } from 'ts-mockito';

import { ContextData, DbContext } from '../../../../../test-utilities/mocks';
import { TrustedSmartRollupIndexer } from '../../../src/client-indexers/wrappers/trusted-indexer-interfaces';
import { BoundSmartRollupIndexer } from '../../../src/smart-rollups/bound-indexer/bound-smart-rollup-indexer';
import { BoundSmartRollupIndexerBinder } from '../../../src/smart-rollups/bound-indexer/bound-smart-rollup-indexer-binder';
import {
    BoundSmartRollupIndexerResolver,
} from '../../../src/smart-rollups/bound-indexer/bound-smart-rollup-indexer-resolver';
import { SmartRollup } from '../../../src/smart-rollups/smart-rollup';

describe(BoundSmartRollupIndexerResolver.name, () => {
    let target: BoundSmartRollupIndexerResolver<DbContext, ContextData>;
    let factory: BoundSmartRollupIndexerBinder<DbContext>;
    let rawIndexers: readonly TrustedSmartRollupIndexer<DbContext, ContextData, unknown>[];
    let db: DbContext;

    beforeEach(() => {
        factory = mock();
        rawIndexers = 'mockedRawIndexers' as any;
        target = new BoundSmartRollupIndexerResolver(instance(factory), rawIndexers);

        db = 'mockedDb' as any;
    });

    it('should create rollup by address and cache it', async () => {
        const rollup1 = { address: 'sr1LhGA2zC9VcYALSifpRBCgDiQfDSQ6bb4x' } as SmartRollup;
        const rollup2 = { address: 'sr1QZkk1swognQW3dmiXvga3wVkEgBq7QFjE' } as SmartRollup;
        const indexer1: BoundSmartRollupIndexer<DbContext, ContextData> | null = 'mockedIndexer1' as any;
        const indexer2: BoundSmartRollupIndexer<DbContext, ContextData> | null = null;
        when(factory.bind(rollup1, rawIndexers, db)).thenResolve(indexer1);
        when(factory.bind(rollup2, rawIndexers, db)).thenResolve(indexer2);

        const result1_1 = await target.resolve(rollup1, db);
        const result2_1 = await target.resolve(rollup2, db);
        const result1_2 = await target.resolve(rollup1, db);
        const result1_3 = await target.resolve(rollup1, db);
        const result2_2 = await target.resolve(rollup2, db);

        expect([result1_1, result1_2, result1_3, result2_1, result2_2]).toEqual([indexer1, indexer1, indexer1, indexer2, indexer2]);
        verify(factory.bind(anything(), anything(), anything())).times(2);
    });
});
