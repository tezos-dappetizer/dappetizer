import { capture, deepEqual, instance, mock, verify } from 'ts-mockito';

import { ContextData, DbContext, RollupData } from '../../../../../test-utilities/mocks';
import { keysOf } from '../../../../utils/src/basics/public-api';
import { SmartRollupIndexingContext } from '../../../src/client-indexers/smart-rollup-indexer';
import { TrustedSmartRollupIndexer } from '../../../src/client-indexers/wrappers/trusted-indexer-interfaces';
import { BoundSmartRollupIndexer } from '../../../src/smart-rollups/bound-indexer/bound-smart-rollup-indexer';
import { BoundSmartRollupIndexerFactory } from '../../../src/smart-rollups/bound-indexer/bound-smart-rollup-indexer-factory';

describe(BoundSmartRollupIndexerFactory.name, () => {
    let resultIndexer: BoundSmartRollupIndexer<DbContext, ContextData>;
    let wrappedIndexer: TrustedSmartRollupIndexer<DbContext, ContextData, RollupData>;
    let rollupData: RollupData;

    beforeEach(() => {
        wrappedIndexer = mock();
        rollupData = { rlpVal: 'rrr' };
        resultIndexer = new BoundSmartRollupIndexerFactory().create(instance(wrappedIndexer), rollupData);
    });

    const methodsToTest: Record<keyof typeof resultIndexer, ''> = {
        indexCement: '',
        indexExecuteOutboxMessage: '',
        indexOriginate: '',
        indexPublish: '',
        indexRecoverBond: '',
        indexRefute: '',
        indexTimeout: '',
    };
    describe.each(keysOf(methodsToTest))('%s', methodName => {
        it('should be delegated to wrapped indexer with rollup data in indexing context', async () => {
            const operation: any = 'mockedOperation';
            const db: DbContext = { dbVal: 'ddd' };
            const context = {
                block: { hash: 'haha' },
                rollupData: null,
            } as SmartRollupIndexingContext<ContextData, null>;

            await resultIndexer[methodName](operation, db, context as any);

            verify(wrappedIndexer[methodName]!(operation, db, deepEqual({
                block: { hash: 'haha' },
                rollupData,
            } as any))).once();
            const receivedContext = capture(wrappedIndexer[methodName] as any).first()[2];
            expect(receivedContext).toBeFrozen();
        });
    });
});
