import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { ContextData, DbContext } from '../../../../../test-utilities/mocks';
import { IndexerInfo } from '../../../src/client-indexers/wrappers/indexer-info';
import { TrustedSmartRollupIndexer } from '../../../src/client-indexers/wrappers/trusted-indexer-interfaces';
import { IndexerComposer } from '../../../src/helpers/indexer-composer';
import { BoundSmartRollupIndexer } from '../../../src/smart-rollups/bound-indexer/bound-smart-rollup-indexer';
import { BoundSmartRollupIndexerBinder } from '../../../src/smart-rollups/bound-indexer/bound-smart-rollup-indexer-binder';
import { BoundSmartRollupIndexerFactory } from '../../../src/smart-rollups/bound-indexer/bound-smart-rollup-indexer-factory';
import { SmartRollup } from '../../../src/smart-rollups/smart-rollup';

interface RollupData1 { r1: string }
interface RollupData2 { r2: string }

describe(BoundSmartRollupIndexerBinder.name, () => {
    let target: BoundSmartRollupIndexerBinder<DbContext>;
    let boundIndexerFactory: BoundSmartRollupIndexerFactory<DbContext>;
    let indexerComposer: IndexerComposer;

    let indexer1: TrustedSmartRollupIndexer<DbContext, ContextData, RollupData1>;
    let indexer2: TrustedSmartRollupIndexer<DbContext, ContextData, RollupData2>;
    let rollup: SmartRollup;
    let db: DbContext;

    let rollupData1: RollupData1;
    let rollupData2: RollupData2;
    let boundIndexer1: BoundSmartRollupIndexer<DbContext, ContextData>;
    let boundIndexer2: BoundSmartRollupIndexer<DbContext, ContextData>;
    let composedIndexer: BoundSmartRollupIndexer<DbContext, ContextData>;

    const act = async () => target.bind(rollup, [instance(indexer1), instance(indexer2)], db);

    beforeEach(() => {
        [boundIndexerFactory, indexerComposer, indexer1, indexer2] = [mock(), mock(), mock(), mock(), mock()];
        target = new BoundSmartRollupIndexerBinder(instance(boundIndexerFactory), instance(indexerComposer));

        rollup = { address: 'sr1AE6U3GNzE8iKzj6sKS5wh1U32ogeULCoN' } as SmartRollup;
        rollupData1 = { r1: 'data1' };
        rollupData2 = { r2: 'data2' };
        boundIndexer1 = 'mockedBound1' as any;
        boundIndexer2 = 'mockedBound2' as any;
        composedIndexer = 'mockedComposite' as any;
        db = 'mockedDb' as any;

        when(indexer1.info).thenReturn({ indexerName: 'ix1', moduleName: 'md1' } as IndexerInfo);
        when(indexer2.info).thenReturn({ indexerName: 'ix2', moduleName: 'md2' } as IndexerInfo);
        when(indexer1.shouldIndex(rollup, db)).thenReturn(rollupData1);
        when(indexer2.shouldIndex(rollup, db)).thenReturn(Promise.resolve(rollupData2));
        when(boundIndexerFactory.create(instance(indexer1), rollupData1)).thenReturn(boundIndexer1);
        when(boundIndexerFactory.create(instance(indexer2), rollupData2)).thenReturn(boundIndexer2);
        when(indexerComposer.createComposite(anything(), anything())).thenReturn(composedIndexer);
    });

    it('should bind indexers with their data', async () => {
        const result = await act();

        expect(result).toBe(composedIndexer);
        verify(indexerComposer.createComposite(deepEqual([boundIndexer1, boundIndexer2]), anything())).once();
    });

    it.each([
        ['false', false as const],
        ['false promise', Promise.resolve<false>(false)],
    ])('should skip indexer if its shouldIndex() returns %s', async (_desc, negativeRollupData) => {
        when(indexer1.shouldIndex(rollup, db)).thenReturn(negativeRollupData);

        const result = await act();

        expect(result).toBe(composedIndexer);
        verify(indexerComposer.createComposite(deepEqual([boundIndexer2]), anything())).once();
        verify(boundIndexerFactory.create(instance(indexer1), anything())).never();
    });
});
