import { instance, mock, when } from 'ts-mockito';

import { SelectiveIndexingConfigResolver } from '../../src/helpers/selective-indexing-config-resolver';
import { SmartRollup, SmartRollupConfig } from '../../src/smart-rollups/smart-rollup';
import { SmartRollupProviderImpl } from '../../src/smart-rollups/smart-rollup-provider-impl';

describe(SmartRollupProviderImpl.name, () => {
    let target: SmartRollupProviderImpl;
    let configResolver: SelectiveIndexingConfigResolver;

    beforeEach(() => {
        configResolver = mock();
        target = new SmartRollupProviderImpl(instance(configResolver));
    });

    it('should resolve rollup correctly', () => {
        const config: SmartRollupConfig = 'mockedConfig' as any;
        when(configResolver.resolve('sr1LhGA2zC9VcYALSifpRBCgDiQfDSQ6bb4x', 'smartRollups')).thenReturn(config);

        const rollup = target.getRollup('sr1LhGA2zC9VcYALSifpRBCgDiQfDSQ6bb4x');

        expect(rollup).toEqual<SmartRollup>({
            address: 'sr1LhGA2zC9VcYALSifpRBCgDiQfDSQ6bb4x',
            config,
            type: 'SmartRollup',
        });
        expect(rollup).toBeFrozen();
    });
});
