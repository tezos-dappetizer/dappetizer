import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { ContextData, DbContext } from '../../../../../test-utilities/mocks';
import { IndexerInfo } from '../../../src/client-indexers/wrappers/indexer-info';
import { TrustedContractIndexer } from '../../../src/client-indexers/wrappers/trusted-indexer-interfaces';
import { BoundContractIndexer } from '../../../src/contracts/bound-indexer/bound-contract-indexer';
import { BoundContractIndexerBinder } from '../../../src/contracts/bound-indexer/bound-contract-indexer-binder';
import { BoundContractIndexerFactory } from '../../../src/contracts/bound-indexer/bound-contract-indexer-factory';
import { IndexerComposer } from '../../../src/helpers/indexer-composer';
import { LazyContract } from '../../../src/rpc-data/rpc-data-interfaces';

interface ContractData1 { c1: string }
interface ContractData2 { c2: string }

describe(BoundContractIndexerBinder.name, () => {
    let target: BoundContractIndexerBinder<DbContext>;
    let boundIndexerFactory: BoundContractIndexerFactory<DbContext>;
    let indexerComposer: IndexerComposer;

    let indexer1: TrustedContractIndexer<DbContext, ContextData, ContractData1>;
    let indexer2: TrustedContractIndexer<DbContext, ContextData, ContractData2>;
    let contract: LazyContract;
    let db: DbContext;

    let rollupData1: ContractData1;
    let rollupData2: ContractData2;
    let boundIndexer1: BoundContractIndexer<DbContext, ContextData>;
    let boundIndexer2: BoundContractIndexer<DbContext, ContextData>;
    let composedIndexer: BoundContractIndexer<DbContext, ContextData>;

    const act = async () => target.bind(contract, [instance(indexer1), instance(indexer2)], db);

    beforeEach(() => {
        [boundIndexerFactory, indexerComposer, indexer1, indexer2] = [mock(), mock(), mock(), mock(), mock()];
        target = new BoundContractIndexerBinder(instance(boundIndexerFactory), instance(indexerComposer));

        contract = { address: 'KT1ViAbsAM5rp89yVydEkbQozp1S12zqirwS' } as LazyContract;
        rollupData1 = { c1: 'data1' };
        rollupData2 = { c2: 'data2' };
        boundIndexer1 = 'mockedBound1' as any;
        boundIndexer2 = 'mockedBound2' as any;
        composedIndexer = 'mockedComposite' as any;
        db = 'mockedDb' as any;

        when(indexer1.info).thenReturn({ indexerName: 'ix1', moduleName: 'md1' } as IndexerInfo);
        when(indexer2.info).thenReturn({ indexerName: 'ix2', moduleName: 'md2' } as IndexerInfo);
        when(indexer1.shouldIndex(contract, db)).thenReturn(rollupData1);
        when(indexer2.shouldIndex(contract, db)).thenReturn(Promise.resolve(rollupData2));
        when(boundIndexerFactory.create(instance(indexer1), rollupData1)).thenReturn(boundIndexer1);
        when(boundIndexerFactory.create(instance(indexer2), rollupData2)).thenReturn(boundIndexer2);
        when(indexerComposer.createComposite(anything(), anything())).thenReturn(composedIndexer);
    });

    it('should bind indexers with their data', async () => {
        const result = await act();

        expect(result).toBe(composedIndexer);
        verify(indexerComposer.createComposite(deepEqual([boundIndexer1, boundIndexer2]), anything())).once();
    });

    it.each([
        ['false', false as const],
        ['false promise', Promise.resolve<false>(false)],
    ])('should skip indexer if its shouldIndex() returns %s', async (_desc, negativeRollupData) => {
        when(indexer1.shouldIndex(contract, db)).thenReturn(negativeRollupData);

        const result = await act();

        expect(result).toBe(composedIndexer);
        verify(indexerComposer.createComposite(deepEqual([boundIndexer2]), anything())).once();
        verify(boundIndexerFactory.create(instance(indexer1), anything())).never();
    });
});
