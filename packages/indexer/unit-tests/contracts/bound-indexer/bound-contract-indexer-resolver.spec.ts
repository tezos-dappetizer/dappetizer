import { anything, instance, mock, verify, when } from 'ts-mockito';

import { ContextData, DbContext } from '../../../../../test-utilities/mocks';
import { TrustedContractIndexer } from '../../../src/client-indexers/wrappers/trusted-indexer-interfaces';
import { BoundContractIndexer } from '../../../src/contracts/bound-indexer/bound-contract-indexer';
import { BoundContractIndexerBinder } from '../../../src/contracts/bound-indexer/bound-contract-indexer-binder';
import {
    BoundContractIndexerResolver,
} from '../../../src/contracts/bound-indexer/bound-contract-indexer-resolver';
import { LazyContract } from '../../../src/rpc-data/rpc-data-interfaces';

describe(BoundContractIndexerResolver.name, () => {
    let target: BoundContractIndexerResolver<DbContext, ContextData>;
    let factory: BoundContractIndexerBinder<DbContext>;
    let rawIndexers: readonly TrustedContractIndexer<DbContext, ContextData, unknown>[];
    let db: DbContext;

    beforeEach(() => {
        factory = mock();
        rawIndexers = 'mockedRawIndexers' as any;
        target = new BoundContractIndexerResolver(instance(factory), rawIndexers);

        db = 'mockedDb' as any;
    });

    it('should create rollup by address and cache it', async () => {
        const contract1 = { address: 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5' } as LazyContract;
        const contract2 = { address: 'KT1MQL8VjVQckk5A6uBfN9Qv2YUVJstG1CyH' } as LazyContract;
        const indexer1: BoundContractIndexer<DbContext, ContextData> | null = 'mockedIndexer1' as any;
        const indexer2: BoundContractIndexer<DbContext, ContextData> | null = null;
        when(factory.bind(contract1, rawIndexers, db)).thenResolve(indexer1);
        when(factory.bind(contract2, rawIndexers, db)).thenResolve(indexer2);

        const result1_1 = await target.resolve(contract1, db);
        const result2_1 = await target.resolve(contract2, db);
        const result1_2 = await target.resolve(contract1, db);
        const result1_3 = await target.resolve(contract1, db);
        const result2_2 = await target.resolve(contract2, db);

        expect([result1_1, result1_2, result1_3, result2_1, result2_2]).toEqual([indexer1, indexer1, indexer1, indexer2, indexer2]);
        verify(factory.bind(anything(), anything(), anything())).times(2);
    });
});
