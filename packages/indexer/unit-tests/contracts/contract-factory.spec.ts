import * as taquito from '@taquito/taquito';
import { anything, instance, mock, when } from 'ts-mockito';

import { expectToThrowAsync, TestLogger } from '../../../../test-utilities/mocks';
import { Contract, ContractAbstraction, ContractConfig } from '../../src/contracts/contract';
import { ContractFactory } from '../../src/contracts/contract-factory';
import { ContractImpl } from '../../src/contracts/contract-impl';
import { BigMapInfo } from '../../src/contracts/data/big-map-info/big-map-info';
import { BigMapInfosFactory } from '../../src/contracts/data/big-map-info/big-map-infos-factory';
import { ContractEventInfo } from '../../src/contracts/data/contract-event/contract-event-info';
import { ContractEventInfosResolver } from '../../src/contracts/data/contract-event/contract-event-infos-resolver';
import { ContractParameterSchemas } from '../../src/contracts/data/schemas/contract-parameter-schemas';
import { ContractParameterSchemasFactory } from '../../src/contracts/data/schemas/contract-parameter-schemas-factory';
import { ContractStorageSchemaFactory } from '../../src/contracts/data/schemas/contract-storage-schema-factory';
import { SelectiveIndexingConfigResolver } from '../../src/helpers/selective-indexing-config-resolver';
import { MichelsonSchema } from '../../src/michelson/michelson-schema';

describe(ContractFactory.name, () => {
    let target: ContractFactory;
    let taquitoToolkitContract: taquito.ContractProvider;
    let bigMapInfosFactory: BigMapInfosFactory;
    let parameterSchemasFactory: ContractParameterSchemasFactory;
    let storageSchemaFactory: ContractStorageSchemaFactory;
    let configResolver: SelectiveIndexingConfigResolver;
    let eventsResolver: ContractEventInfosResolver;
    let logger: TestLogger;

    let abstraction: ContractAbstraction;
    let storage: any;
    let config: ContractConfig;
    let bigMaps: readonly BigMapInfo[];
    let storageSchema: MichelsonSchema;
    let parameterSchemas: ContractParameterSchemas;
    let events: readonly ContractEventInfo[];

    const act = async () => target.create('KT1AEfeckNbdEYwaMKkytBwPJPycz7jdSGea');

    beforeEach(() => {
        target = new ContractFactory(
            { contract: instance(taquitoToolkitContract = mock()) } as taquito.TezosToolkit,
            instance(bigMapInfosFactory = mock()),
            instance(parameterSchemasFactory = mock()),
            instance(storageSchemaFactory = mock()),
            instance(configResolver = mock()),
            instance(eventsResolver = mock()),
            logger = new TestLogger(),
        );

        abstraction = { address: 'KT1AEfeckNbdEYwaMKkytBwPJPycz7jdSGea' } as typeof abstraction;
        storage = 'mockedStorage';
        config = 'mockedConfig' as any;
        bigMaps = 'mockedBigMaps' as any;
        storageSchema = 'mockedStorageSchema' as any;
        parameterSchemas = 'mockedParameterSchemas' as any;
        events = 'mockedEvents' as any;

        when(configResolver.resolve('KT1AEfeckNbdEYwaMKkytBwPJPycz7jdSGea', 'contracts')).thenReturn(config);
        when(taquitoToolkitContract.at('KT1AEfeckNbdEYwaMKkytBwPJPycz7jdSGea', anything())).thenCall(async () => {
            logger.logDebug('TOOLKIT');
            return Promise.resolve(abstraction);
        });
        abstraction.storage = async <T>() => {
            logger.logDebug('STORAGE');
            return Promise.resolve(storage as T);
        };
        when(bigMapInfosFactory.create(storage)).thenReturn(bigMaps);
        when(storageSchemaFactory.create(abstraction)).thenReturn(storageSchema);
        when(parameterSchemasFactory.create(abstraction)).thenReturn(parameterSchemas);
        when(eventsResolver.resolve(abstraction)).thenReturn(events);
    });

    it('should create contract correctly with configuredToBeIndexed %s', async () => {
        const contract = await act();

        expect(contract).toBeInstanceOf(ContractImpl);
        expect(contract).toEqual<Contract>({
            address: 'KT1AEfeckNbdEYwaMKkytBwPJPycz7jdSGea',
            config,
            abstraction,
            bigMaps,
            events,
            parameterSchemas,
            storageSchema,
        });

        logger.verifyLoggedCount(2);
        logger.logged(0).verify('Debug', { performanceSection: logger.category }).verifyMessage('TOOLKIT');
        logger.logged(1).verify('Debug', { performanceSection: logger.category }).verifyMessage('STORAGE');
    });

    it('should wrap taquito abstraction errors', async () => {
        when(taquitoToolkitContract.at('KT1AEfeckNbdEYwaMKkytBwPJPycz7jdSGea', anything())).thenReject(new Error('Oups'));

        await runThrowTest({ expectedError: '@taquito abstraction' });
    });

    it('should wrap taquito storage errors', async () => {
        abstraction.storage = async () => Promise.reject(new Error('Oups'));

        await runThrowTest({ expectedError: '@taquito deserialized storage' });
    });

    it('should wrap other errors', async () => {
        when(bigMapInfosFactory.create(storage)).thenThrow(new Error('Oups'));

        await runThrowTest();
    });

    async function runThrowTest(options?: { expectedError: string }) {
        const error = await expectToThrowAsync(act);

        expect(error.message).toIncludeMultiple([
            options?.expectedError ?? '',
            `contract 'KT1AEfeckNbdEYwaMKkytBwPJPycz7jdSGea'`,
            `Oups`,
        ]);
    }
});
