import { Schema } from '@taquito/michelson-encoder';
import * as taquito from '@taquito/taquito';
import { BigNumber } from 'bignumber.js';

import { expectToThrow } from '../../../../../../test-utilities/mocks';
import { BigMapInfo } from '../../../../src/contracts/data/big-map-info/big-map-info';
import { BigMapInfoImpl } from '../../../../src/contracts/data/big-map-info/big-map-info-impl';
import { BigMapInfosFactory } from '../../../../src/contracts/data/big-map-info/big-map-infos-factory';
import { michelsonTypePrims } from '../../../../src/michelson/michelson-prims';
import { MichelsonSchema } from '../../../../src/michelson/michelson-schema';

describe(BigMapInfosFactory.name, () => {
    const target = new BigMapInfosFactory();

    it('should collect all big maps', () => {
        const storage = {
            setting1: 'abc',
            bigMap1: new taquito.BigMapAbstraction(
                new BigNumber('123'),
                new Schema({
                    prim: michelsonTypePrims.BIG_MAP,
                    args: [
                        { prim: 'address', annots: [':owner'] },
                        { prim: 'nat', annots: [':amount'] },
                    ],
                }),
                null!,
            ),
            deep: {
                setting2: 123,
                nested: {
                    bigMap2: new taquito.BigMapAbstraction(
                        new BigNumber('456'),
                        new Schema({
                            prim: michelsonTypePrims.BIG_MAP,
                            args: [
                                { prim: 'nat', annots: ['%token_id'] },
                                { prim: 'nat', annots: ['%value'] },
                            ],
                        }),
                        null!,
                    ),
                    setting3: 'xyz',
                },
            },
        };

        const bigMaps = target.create(storage);

        expect(bigMaps).toMatchObject<BigMapInfo[]>([
            {
                lastKnownId: new BigNumber('123'),
                name: 'bigMap1',
                path: ['bigMap1'],
                pathStr: 'bigMap1',
                keySchema: new MichelsonSchema({ prim: 'address', annots: [':owner'] }),
                valueSchema: new MichelsonSchema({ prim: 'nat', annots: [':amount'] }),
            },
            {
                lastKnownId: new BigNumber('456'),
                name: 'bigMap2',
                path: ['deep', 'nested', 'bigMap2'],
                pathStr: 'deep.nested.bigMap2',
                keySchema: new MichelsonSchema({ prim: 'nat', annots: ['%token_id'] }),
                valueSchema: new MichelsonSchema({ prim: 'nat', annots: ['%value'] }),
            },
        ]);
        expect(bigMaps).toBeFrozen();
        bigMaps.forEach(m => expect(m).toBeInstanceOf(BigMapInfoImpl));
    });

    it('should support big map at root', () => {
        const storage = new taquito.BigMapAbstraction(
            new BigNumber('123'),
            new Schema({
                prim: michelsonTypePrims.BIG_MAP,
                args: [
                    { prim: 'address', annots: [':owner'] },
                    { prim: 'nat', annots: [':amount'] },
                ],
            }),
            null!,
        );

        const bigMaps = target.create(storage);

        expect(bigMaps).toMatchObject<BigMapInfo[]>([{
            lastKnownId: new BigNumber('123'),
            name: null,
            path: [],
            pathStr: '',
            keySchema: new MichelsonSchema({ prim: 'address', annots: [':owner'] }),
            valueSchema: new MichelsonSchema({ prim: 'nat', annots: [':amount'] }),
        }]);
    });

    it('should wrap errors', () => {
        const storage = new taquito.BigMapAbstraction(new BigNumber('123'), 'wtf' as any, null!);

        const error = expectToThrow(() => target.create(storage));

        expect(error.message).toIncludeMultiple([
            'find big maps',
            JSON.stringify(storage),
            'Taquito BigMapAbstraction API has changed',
        ]);
    });
});
