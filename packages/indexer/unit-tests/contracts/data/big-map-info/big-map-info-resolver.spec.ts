import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { Writable } from 'ts-essentials';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { expectToThrowAsync, TestLogger } from '../../../../../../test-utilities/mocks';
import { asReadonly } from '../../../../../utils/src/public-api';
import { Contract } from '../../../../src/contracts/contract';
import { BigMapInfo } from '../../../../src/contracts/data/big-map-info/big-map-info';
import { BigMapInfoImpl } from '../../../../src/contracts/data/big-map-info/big-map-info-impl';
import { BigMapInfoResolver } from '../../../../src/contracts/data/big-map-info/big-map-info-resolver';
import { scopedLogKeys } from '../../../../src/helpers/scoped-log-keys';
import { michelsonTypePrims } from '../../../../src/michelson/michelson-prims';
import { MichelsonSchema } from '../../../../src/michelson/michelson-schema';
import { LazyContract, Michelson } from '../../../../src/rpc-data/rpc-data-interfaces';

describe(BigMapInfoResolver.name, () => {
    let target: BigMapInfoResolver;
    let rpcClient: taquitoRpc.RpcClient;
    let logger: TestLogger;

    let lazyContract: LazyContract;
    let contract: Writable<Contract>;
    let storageMichelson: Michelson;

    const act = async (id: number) => target.resolve(new BigNumber(id), instance(lazyContract), 'blk-haha');

    beforeEach(() => {
        rpcClient = mock();
        logger = new TestLogger();
        target = new BigMapInfoResolver(instance(rpcClient), logger);

        lazyContract = mock();
        contract = {
            address: 'KT1MTFjUeqBeZoFeW1NLSrzJdcS5apFiUXoB',
            bigMaps: asReadonly<BigMapInfo>([
                new BigMapInfoImpl(['bigMap1'], 11, null!, null!),
                new BigMapInfoImpl(['nested', 'bigMap2'], 22, null!, null!),
            ]),
            storageSchema: new MichelsonSchema({
                prim: michelsonTypePrims.PAIR,
                args: [
                    {
                        prim: michelsonTypePrims.BIG_MAP,
                        args: [
                            { prim: michelsonTypePrims.STRING },
                            { prim: michelsonTypePrims.INT },
                        ],
                        annots: ['%bigMap1'],
                    },
                    {
                        prim: michelsonTypePrims.PAIR,
                        args: [
                            { prim: michelsonTypePrims.STRING, annots: ['%strProperty'] },
                            {
                                prim: michelsonTypePrims.BIG_MAP,
                                args: [
                                    { prim: michelsonTypePrims.STRING },
                                    { prim: michelsonTypePrims.INT },
                                ],
                                annots: ['%bigMap2'],
                            },
                        ],
                        annots: ['%nested'],
                    },
                ],
            }),
        } as Contract;

        storageMichelson = {
            prim: michelsonTypePrims.PAIR,
            args: [
                { int: '911' },
                {
                    prim: michelsonTypePrims.PAIR,
                    args: [
                        { string: 'omg' },
                        { int: '922' },
                    ],
                },
            ],
        };

        when(lazyContract.getContract()).thenResolve(contract);
        when(rpcClient.getStorage(contract.address, deepEqual({ block: 'blk-haha' })))
            .thenCall(async () => Promise.resolve(storageMichelson));
    });

    it.each([
        ['bigMap1', 11],
        ['bigMap2', 22],
    ])('should find big map %s by id %s', async (expectedName, bigMapId) => {
        const bigMap = await act(bigMapId);

        expect(bigMap!.name).toBe(expectedName);
        verify(rpcClient.getStorage(anything(), anything())).never();
        logger.verifyNothingLogged();
    });

    it('should return null if temporary big map (id less than zero)', async () => {
        const bigMap = await act(-33);

        expect(bigMap).toBeNull();
        verify(lazyContract.getContract()).never();
        verify(rpcClient.getStorage(anything(), anything())).never();
        logger.verifyNothingLogged();
    });

    describe('after reload of big map ids', () => {
        afterEach(() => {
            verify(rpcClient.getStorage(anything(), anything())).times(1);
        });

        it.each([
            ['bigMap1', 911],
            ['bigMap2', 922],
        ])('should resolve %s if no last known id found and use RPC %s', async (expectedName, bigMapId) => {
            const bigMap = await act(bigMapId);

            expect(bigMap!.name).toBe(expectedName);
            expect(contract.bigMaps[0]?.lastKnownId).toEqual(new BigNumber(911));
            expect(contract.bigMaps[1]?.lastKnownId).toEqual(new BigNumber(922));
            logger.verifyNothingLogged();
        });

        it('should return null if big map not found', async () => {
            const scopedLogData = {
                [scopedLogKeys.INDEXED_BLOCK]: 'blk-ha',
                [scopedLogKeys.INDEXED_OPERATION_GROUP_HASH]: 'ogh',
                [scopedLogKeys.INDEXER_MODULE]: 'mod',
            };

            await logger.runWithData(scopedLogData, async () => {
                const bigMap = await act(66);

                expect(bigMap).toBeNull();
                logger.loggedSingle().verify('Warning', {
                    bigMapId: new BigNumber(66),
                    contract: contract.address,
                    knownBigMapIds: [new BigNumber(911), new BigNumber(922)],
                    ...scopedLogData,
                });
            });
        });

        it('should throw if invalid storage', async () => {
            storageMichelson = { string: 'wtf' };

            await runThrowTest([
                `executing Michelson schema`,
                JSON.stringify(storageMichelson),
            ]);
        });

        it('should throw if unexpected storage field', async () => {
            storageMichelson = {
                prim: michelsonTypePrims.PAIR,
                args: [
                    { int: '911' },
                    {
                        prim: michelsonTypePrims.PAIR,
                        args: [
                            { string: 'omg' },
                            [],
                        ],
                    },
                ],
            };

            await runThrowTest([
                `must be a BigNumber integer`,
                `"omg"`,
                `of type 'object'`,
                `big map 'nested.bigMap2'`,
            ]);
        });

        it('should throw if no matching field in storage', async () => {
            contract.bigMaps = [{
                lastKnownId: new BigNumber(11),
                name: 'wtf',
                path: asReadonly(['nested', 'wtf']),
                pathStr: 'nested.wtf',
            } as BigMapInfo];

            await runThrowTest([
                `object with 'wtf' property`,
                `big map 'nested.wtf'`,
            ]);
        });

        async function runThrowTest(expectedError: string[]) {
            const error = await expectToThrowAsync(async () => act(66));

            expect(error.message).toIncludeMultiple([
                ...expectedError,
                `contract 'KT1MTFjUeqBeZoFeW1NLSrzJdcS5apFiUXoB'`,
                `block 'blk-haha'`,
            ]);
        }
    });
});
