import { expectToThrow } from '../../../../../../test-utilities/mocks';
import { ContractAbstraction } from '../../../../src/contracts/contract';
import { ContractParameterSchemasFactory } from '../../../../src/contracts/data/schemas/contract-parameter-schemas-factory';
import { MichelsonSchema } from '../../../../src/michelson/michelson-schema';

describe(ContractParameterSchemasFactory.name, () => {
    let target: ContractParameterSchemasFactory;
    let contract: ContractAbstraction;

    beforeEach(() => {
        target = new ContractParameterSchemasFactory();

        contract = {
            entrypoints: {
                entrypoints: {
                    ['foo' as string]: { prim: 'int' },
                    ['bar' as string]: { prim: 'string' },
                },
            },
            parameterSchema: { root: { val: { prim: 'nat' } } } as any,
        } as ContractAbstraction;
    });

    it('should create schemas correctly', () => {
        const schemas = target.create(contract);

        expect(schemas.default).toEqual(new MichelsonSchema({ prim: 'nat' }));
        expect(schemas.entrypoints).toEqual({
            foo: new MichelsonSchema({ prim: 'int' }),
            bar: new MichelsonSchema({ prim: 'string' }),
        });
    });

    it('should wrap named entrypoint errors', () => {
        contract = {
            ...contract,
            entrypoints: { entrypoints: { ['foo' as string]: 'wtf' as any } },
        } as ContractAbstraction;

        const error = expectToThrow(() => target.create(contract));

        expect(error.message).toIncludeMultiple([
            `prepare schema`,
            `entrypoint 'foo'`,
            `"wtf"`,
        ]);
    });

    it(`should wrap 'default' entrypoint errors`, () => {
        contract = {
            ...contract,
            parameterSchema: 'wtf' as any,
        } as ContractAbstraction;

        const error = expectToThrow(() => target.create(contract));

        expect(error.message).toIncludeMultiple([
            `prepare schema`,
            `default entrypoint`,
            `"wtf"`,
        ]);
    });
});
