import { container as globalContainer, DependencyContainer } from 'tsyringe';

import {
    shouldResolveImplsForDappetizerFeatures,
    shouldResolvePublicApi,
    throwShouldNotBeCalled,
} from '../../../test-utilities/mocks';
import { registerExplicitConfigFilePath, RPC_BLOCK_MONITOR_DI_TOKEN } from '../../utils/src/public-api';
import { BLOCK_INDEXING } from '../src/block-processing/block-indexing-background-worker';
import {
    ForbidBlockDataIndexerIfContractBoostVerifier,
} from '../src/block-source/contract-boost/forbid-block-data-indexer-if-contract-boost-verifier';
import { IndexingConfig } from '../src/config/indexing/indexing-config';
import { IpfsConfig } from '../src/helpers/ipfs/ipfs-config';
import {
    CONTRACT_PROVIDER_DI_TOKEN,
    INDEXER_MODULES_DI_TOKEN,
    INDEXER_PACKAGE_JSON_DI_TOKEN,
    registerDappetizerIndexer,
    registerIndexerDatabaseHandler,
    SELECTIVE_INDEXING_CONFIG_DI_TOKEN,
    SMART_ROLLUP_PROVIDER_DI_TOKEN,
    TAQUITO_TEZOS_TOOLKIT_DI_TOKEN,
} from '../src/public-api';

describe('dependency injection', () => {
    let diContainer: DependencyContainer;

    beforeAll(() => {
        diContainer = globalContainer.createChildContainer();
        registerDappetizerIndexer(diContainer);
        registerExplicitConfigFilePath(diContainer, { useValue: '../../dappetizer.config.ts' });

        registerIndexerDatabaseHandler(diContainer, {
            useValue: {
                startTransaction: throwShouldNotBeCalled,
                commitTransaction: throwShouldNotBeCalled,
                rollBackTransaction: throwShouldNotBeCalled,
                insertBlock: throwShouldNotBeCalled,
                deleteBlock: throwShouldNotBeCalled,
                getIndexedChainId: throwShouldNotBeCalled,
                getLastIndexedBlock: throwShouldNotBeCalled,
                getIndexedBlock: throwShouldNotBeCalled,
            },
        });
        diContainer.registerInstance(INDEXER_MODULES_DI_TOKEN, {
            name: 'Dummy',
            indexingCycleHandler: { createContextData: throwShouldNotBeCalled },
            blockDataIndexers: [{ indexBlock: throwShouldNotBeCalled }],
        });
    });

    shouldResolvePublicApi(() => diContainer, [
        [CONTRACT_PROVIDER_DI_TOKEN, 'frozen'],
        [INDEXER_PACKAGE_JSON_DI_TOKEN, 'frozen'],
        [SELECTIVE_INDEXING_CONFIG_DI_TOKEN, 'frozen'],
        [SMART_ROLLUP_PROVIDER_DI_TOKEN, 'frozen'],
        TAQUITO_TEZOS_TOOLKIT_DI_TOKEN,

        RPC_BLOCK_MONITOR_DI_TOKEN, // Should register Dappetizer Utils too.
    ]);

    shouldResolveImplsForDappetizerFeatures(() => diContainer, {
        backgroundWorkerNames: [BLOCK_INDEXING],
        configsWithDiagnostics: [IndexingConfig, IpfsConfig],
        healthCheckNames: [BLOCK_INDEXING],
        partialIndexerModuleVerifiers: [ForbidBlockDataIndexerIfContractBoostVerifier],
    });
});
