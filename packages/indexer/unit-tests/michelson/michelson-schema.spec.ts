import * as taquitoMichelson from '@taquito/michelson-encoder';
import { BigNumber } from 'bignumber.js';

import { describeMember, expectToThrow } from '../../../../test-utilities/mocks';
import { michelsonTypePrims } from '../../src/michelson/michelson-prims';
import { MichelsonSchema } from '../../src/michelson/michelson-schema';
import { Michelson } from '../../src/rpc-data/rpc-data-interfaces';

describe(MichelsonSchema.name, () => {
    const getTarget = () => new MichelsonSchema(schemaMichelson);
    let schemaMichelson: Michelson;

    beforeEach(() => {
        schemaMichelson = {
            prim: michelsonTypePrims.PAIR,
            args: [
                { prim: michelsonTypePrims.ADDRESS, annots: ['%owner'] },
                { prim: michelsonTypePrims.NAT, annots: ['%amount'] },
            ],
        };
    });

    describe('constructor', () => {
        it('should be created from schema Michelson correctly', () => {
            const target = getTarget();

            expect(target.michelson).toEqual(schemaMichelson);
            expect(target.generated).toEqual<taquitoMichelson.TokenSchema>({
                __michelsonType: 'pair',
                schema: {
                    owner: { __michelsonType: 'address', schema: 'address' },
                    amount: { __michelsonType: 'nat', schema: 'nat' },
                },
            });
            expect(target).toBeFrozen();
            expect(target.michelson).toBeFrozen();
            expect(target.generated).toBeFrozen();
        });

        it('should include Michelson if failed', () => {
            schemaMichelson = { prim: 'OMG' };

            const error = expectToThrow(getTarget);

            expect(error.message).toIncludeMultiple([
                JSON.stringify(schemaMichelson),
                'Malformed data',
            ]);
        });
    });

    describeMember<MichelsonSchema>('execute', () => {
        it('should deserialize value correctly', () => {
            const valueMichelson: Michelson = {
                prim: michelsonTypePrims.PAIR,
                args: [
                    { string: 'tz3gtoUxdudfBRcNY7iVdKPHCYYX6xdPpoRS' },
                    { int: '123' },
                ],
            };

            const value = getTarget().execute(valueMichelson);

            expect(value).toEqual({
                owner: 'tz3gtoUxdudfBRcNY7iVdKPHCYYX6xdPpoRS',
                amount: new BigNumber(123),
            });
        });

        it('should include Michelsons in error message if failed', () => {
            const valueMichelson: Michelson = { prim: 'WTF' };
            const target = getTarget();

            const error = expectToThrow(() => target.execute(valueMichelson));

            expect(error.message).toIncludeMultiple([
                JSON.stringify(valueMichelson),
                JSON.stringify(schemaMichelson),
                'TokenArgumentValidationError',
            ]);
        });
    });
});
