import * as taquitoMichelson from '@taquito/michelson-encoder';

import { expectToThrow } from '../../../../test-utilities/mocks';
import {
    cleanAnnot,
    createPrim,
    flatRightCombPairs,
    getSingleAnnot,
    getSingleAnnotCleaned,
    getUnderlyingMichelson,
    isPrim,
    isRawValue,
} from '../../src/michelson/michelson-utils';
import { Michelson, MichelsonPrim } from '../../src/rpc-data/rpc-data-interfaces';

describe('michelson utils', () => {
    describe(isPrim.name, () => {
        it.each([
            [true, 'prim', { prim: 'test' }],
            [false, 'raw value', { int: '12' }],
            [false, 'array', [{ int: '12' }]],
        ])('should return %s for %s', (expected, _desc, michelson) => {
            expect(isPrim(michelson)).toBe(expected);
        });
    });

    describe(isRawValue.name, () => {
        it.each([
            [true, 'int', { int: '12' }],
            [true, 'string', { string: 'ab' }],
            [true, 'bytes', { bytes: 'xy' }],
            [false, 'prim', { prim: 'test' }],
            [false, 'array', [{ int: '12' }]],
        ])('should return %s for %s', (expected, _desc, michelson) => {
            expect(isRawValue(michelson)).toBe(expected);
        });
    });

    describe(getUnderlyingMichelson.name, () => {
        it.each([
            [taquitoMichelson.Schema.name, taquitoMichelson.Schema],
            [taquitoMichelson.ParameterSchema.name, taquitoMichelson.ParameterSchema],
        ])('should get michelson from %s', (_desc, schemaType) => {
            const testMichelson = { prim: 'address' };
            const schema = new schemaType(testMichelson);

            const michelson = getUnderlyingMichelson(schema);

            expect(michelson).toBe(testMichelson);
        });

        it.each([
            ['no root object', {}],
            ['no val object', { root: {} }],
            ['unsupported val object', { root: { val: 123 } }],
        ])('should throw if %s', (_desc, invalidSchema) => {
            const schema = invalidSchema as unknown as taquitoMichelson.Schema;

            expect(() => getUnderlyingMichelson(schema)).toThrow();
        });
    });

    describe(flatRightCombPairs.name, () => {
        it('should flat right combined pairs deeply', () => {
            const input: Michelson = {
                prim: 'pair',
                args: [
                    { prim: 'nat', annots: [':balance'], args: [] }, // Empty args.
                    {
                        prim: 'pair',
                        args: [
                            { prim: 'address', annots: ['%user'] },
                            {
                                prim: 'pair', // Deep pair.
                                args: [
                                    { prim: 'nat', annots: [] }, // Empty annots.
                                    { prim: 'address' }, // No annots.
                                ],
                            },
                        ],
                    },
                ],
            };

            const result = flatRightCombPairs(input);

            expect(result).toEqual<Michelson>({
                prim: 'pair',
                args: [
                    { prim: 'nat', annots: [':balance'] },
                    { prim: 'address', annots: ['%user'] },
                    { prim: 'nat' },
                    { prim: 'address' },
                ],
            });
            expect(result).toBeFrozen();
        });

        it('should not flat right combined pair if has annots', () => {
            const input: Michelson = {
                prim: 'pair',
                args: [
                    { prim: 'nat', annots: [':balance'] },
                    {
                        prim: 'pair',
                        args: [
                            { prim: 'address', annots: ['%user'] },
                            { prim: 'nat', annots: ['%level'] },
                        ],
                        annots: ['%details'],
                    },
                ],
            };

            const result = flatRightCombPairs(input);

            expect(result).toEqual(input);
            expect(result).toBeFrozen();
        });

        it('should not flat if left combined pair', () => {
            const input: Michelson = {
                prim: 'pair',
                args: [
                    {
                        prim: 'pair',
                        args: [
                            { prim: 'nat' },
                            { prim: 'nat', annots: [':state'] },
                        ],
                    },
                    { prim: 'address', annots: ['%user'] },
                ],
            };

            const result = flatRightCombPairs(input);

            expect(result).toEqual(input);
            expect(result).toBeFrozen();
        });
    });

    describe(createPrim.name, () => {
        it.each([
            ['with all properties', [{ int: '11' }], ['%amount'], { prim: 'test', args: [{ int: '11' }], annots: ['%amount'] }],
            ['with empty args', [], ['%amount'], { prim: 'test', annots: ['%amount'] }],
            ['with undefined args', undefined, ['%amount'], { prim: 'test', annots: ['%amount'] }],
            ['with empty annots', [{ int: '11' }], [], { prim: 'test', args: [{ int: '11' }] }],
            ['with undefined annots', [{ int: '11' }], undefined, { prim: 'test', args: [{ int: '11' }] }],
            ['with no properties', undefined, undefined, { prim: 'test' }],
        ])('should create correctly %s', (_desc, args, annots, expected) => {
            const prim = createPrim('test', args, annots);

            expect(prim).toEqual(expected);
        });

        it('should create frozen object', () => {
            const prim = createPrim('test', [{ int: '11' }], ['%amount']);

            expect(prim).toBeFrozen();
            expect(prim.args).toBeFrozen();
            expect(prim.annots).toBeFrozen();
        });
    });

    describe(getSingleAnnotCleaned.name, () => {
        it('should return single annot cleaned', () => {
            const annot = getSingleAnnotCleaned({ prim: 'pp', annots: ['%foo'] });

            expect(annot).toBe('foo');
        });
    });

    describe(getSingleAnnot.name, () => {
        it.each<[string, MichelsonPrim, null | string]>([
            ['not existing', { prim: 'pp' }, null],
            ['undefined', { prim: 'pp', annots: undefined }, null],
            ['empty', { prim: 'pp', annots: [] }, null],
            ['ok', { prim: 'pp', annots: ['foo'] }, 'foo'],
        ])('should return correct value if annots are %s', (_desc, michelson, expected) => {
            expect(getSingleAnnot(michelson)).toBe(expected);
        });

        it('should throw if multiple values in annots', () => {
            const michelson: MichelsonPrim = { prim: 'pp', annots: ['a', 'b'] };

            const error = expectToThrow(() => getSingleAnnot(michelson));

            expect(error.message).toIncludeMultiple(['max 1 value', JSON.stringify(michelson)]);
        });
    });

    describe(cleanAnnot.name, () => {
        it.each([
            ['with leading "%"', '%foo', 'foo'],
            ['with leading ":"', ':foo', 'foo'],
            ['without leading char', 'foo', 'foo'],
            ['null', null, null],
            ['undefined', undefined, null],
        ])('should clean annot correctly if %s', (_desc, input, expected) => {
            expect(cleanAnnot(input)).toBe(expected);
        });
    });
});
