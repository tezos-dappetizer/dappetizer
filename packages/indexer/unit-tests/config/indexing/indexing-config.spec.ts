import { range } from 'lodash';
import { StrictOmit } from 'ts-essentials';

import { describeMember, expectToThrow, verifyPropertyNames } from '../../../../../test-utilities/mocks';
import { ConfigError, ConfigObjectElement, MAINNET } from '../../../../utils/src/public-api';
import { DappetizerConfig } from '../../../src/config/dappetizer-config';
import {
    ContractsBoostType,
    DEFAULT_TZKT_URL,
    IndexingConfig,
    TzktContractsBoostConfig,
} from '../../../src/config/indexing/indexing-config';
import { IndexingDappetizerConfig } from '../../../src/config/indexing/indexing-dappetizer-config';

type IndexingConfigProperties = StrictOmit<IndexingConfig, 'getDiagnostics'>;

describe(IndexingConfig.name, () => {
    function act(thisJson: DappetizerConfig['networks'][string]['indexing'], network?: string) {
        const rootJson = { [IndexingConfig.ROOT_NAME]: thisJson };
        return new IndexingConfig(new ConfigObjectElement(rootJson, 'file.json', '$.root'), network ?? MAINNET);
    }

    it.each([true, false])('should be created if all values specified esp. retryIndefinitely %s', retryIndefinitely => {
        const inputJson: IndexingDappetizerConfig = {
            fromBlockLevel: 11,
            toBlockLevel: 22,
            blockQueueSize: 33,
            contracts: [
                { addresses: ['KT1Ezht4PDKZri7aVppVGT4Jkw39sesaFnww'], name: 'Foo' },
                { addresses: ['KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton', 'KT1NKm2wUFFEeKbq5BCBhyb75fCYELY1s13y'] },
            ],
            smartRollups: [
                { addresses: ['sr1AE6U3GNzE8iKzj6sKS5wh1U32ogeULCoN'], name: 'Bar' },
            ],
            contractBoost: {
                type: 'tzkt',
                apiUrl: 'https://custom.tzkt.org/',
            },
            contractCacheMillis: 77,
            health: {
                lastSuccessAge: {
                    degradedMillis: 44,
                    unhealthyMillis: 55,
                },
            },
            retryDelaysMillis: [1, 2, 3],
            retryIndefinitely,
            headLevelOffset: 66,
        };

        const config = act(inputJson);

        expect(config).toEqual<IndexingConfigProperties>({
            configPropertyPath: '$.root.indexing',
            fromBlockLevel: 11,
            toBlockLevel: 22,
            blockQueueSize: 33,
            contracts: [
                { addresses: ['KT1Ezht4PDKZri7aVppVGT4Jkw39sesaFnww'], name: 'Foo' },
                { addresses: ['KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton', 'KT1NKm2wUFFEeKbq5BCBhyb75fCYELY1s13y'], name: null },
            ],
            smartRollups: [
                { addresses: ['sr1AE6U3GNzE8iKzj6sKS5wh1U32ogeULCoN'], name: 'Bar' },
            ],
            contractBoost: {
                type: ContractsBoostType.Tzkt,
                apiUrl: 'https://custom.tzkt.org/',
            },
            contractCacheMillis: 77,
            health: {
                lastSuccessAge: {
                    degradedMillis: 44,
                    unhealthyMillis: 55,
                },
            },
            retryDelaysMillis: [1, 2, 3],
            retryIndefinitely,
            headLevelOffset: 66,
        });
    });

    it.each([
        ['only necessary', { fromBlockLevel: 123 }],
        ['empty health', { fromBlockLevel: 123, health: {} }],
        ['empty health.lastSuccessAge', { fromBlockLevel: 123, health: { lastSuccessAge: {} } }],
    ])('should be created with defaults if %s', (_desc, inputJson) => {
        const config = act(inputJson);

        expect(config).toEqual<IndexingConfigProperties>({
            configPropertyPath: '$.root.indexing',
            fromBlockLevel: 123,
            toBlockLevel: null,
            blockQueueSize: 5,
            contracts: null,
            smartRollups: null,
            contractBoost: null,
            contractCacheMillis: 600_000,
            health: {
                lastSuccessAge: {
                    degradedMillis: 300_000,
                    unhealthyMillis: 600_000,
                },
            },
            retryDelaysMillis: [100, 1_000, 5_000, 60_000],
            retryIndefinitely: true,
            headLevelOffset: 1,
        });
    });

    describeMember<IndexingConfig>('contracts', () => {
        it('should throw if too many contract addresses', () => {
            const inputJson: IndexingDappetizerConfig = {
                fromBlockLevel: 11,
                contracts: [
                    { addresses: ['KT1Ezht4PDKZri7aVppVGT4Jkw39sesaFnww', ...range(119).map(() => 'KT1Ezht4PDKZri7aVppVGT4Jkw39sesaFnww')] },
                ],
            };
            runThrowTest({
                inputJson,
                expectedError: {
                    reason: ['limited to 100', 'there are 120', 'shouldIndex()'],
                    propertyPath: '$.root.indexing.contracts',
                    value: inputJson.contracts,
                },
            });
        });

        it('should throw if contract addresses are repeated', () => {
            const inputJson: IndexingDappetizerConfig = {
                fromBlockLevel: 11,
                contracts: [
                    { addresses: ['KT1Ezht4PDKZri7aVppVGT4Jkw39sesaFnww'] },
                    { addresses: ['KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton', 'KT1Ezht4PDKZri7aVppVGT4Jkw39sesaFnww'] },
                ],
            };
            runThrowTest({
                inputJson,
                expectedError: {
                    reason: [`address`, `'KT1Ezht4PDKZri7aVppVGT4Jkw39sesaFnww'`],
                    propertyPath: '$.root.indexing.contracts',
                    value: inputJson.contracts,
                },
            });
        });
    });

    describeMember<IndexingConfig>('contractBoost', () => {
        it('should use default TzKT endpoint if for mainnet', () => {
            const config = act({
                fromBlockLevel: 123,
                contractBoost: { type: 'tzkt' },
                contracts: [{ addresses: ['KT1Ezht4PDKZri7aVppVGT4Jkw39sesaFnww'], name: 'Foo' }],
            });

            expect(config.contractBoost).toEqual<TzktContractsBoostConfig>({
                type: ContractsBoostType.Tzkt,
                apiUrl: DEFAULT_TZKT_URL,
            });
        });

        it('should require explicit URL if not mainnet', () => {
            runThrowTest({
                inputJson: {
                    fromBlockLevel: 123,
                    contractBoost: { type: 'tzkt' },
                    contracts: [{ addresses: ['KT1Ezht4PDKZri7aVppVGT4Jkw39sesaFnww'], name: 'Foo' }],
                },
                network: 'skynet',
                expectedError: {
                    propertyPath: '$.root.indexing.contractBoost.apiUrl',
                    value: undefined,
                },
            });
        });

        it('should throw if no contracts specified', () => {
            const inputJson: IndexingDappetizerConfig = {
                fromBlockLevel: 123,
                contractBoost: { type: 'tzkt' },
            };
            runThrowTest({
                inputJson,
                expectedError: {
                    reason: [`no 'contracts'`],
                    propertyPath: '$.root.indexing.contractBoost',
                    value: inputJson.contractBoost,
                },
            });
        });
    });

    function runThrowTest(testCase: {
        inputJson: IndexingDappetizerConfig;
        network?: string;
        expectedError: { reason?: string[]; value: unknown; propertyPath: string };
    }) {
        const error = expectToThrow(() => act(testCase.inputJson, testCase.network), ConfigError);

        expect(error.reason).toIncludeMultiple(testCase.expectedError.reason ?? []);
        expect(error).toMatchObject<Partial<ConfigError>>({
            filePath: 'file.json',
            propertyPath: testCase.expectedError.propertyPath,
            value: testCase.expectedError.value,
        });
    }

    it('should expose its properties in DappetizerConfig', () => {
        expect(IndexingConfig.ROOT_NAME).toBe<keyof DappetizerConfig['networks'][string]>('indexing');

        const config = act({
            fromBlockLevel: 123,
            contracts: [{ addresses: ['KT1Ezht4PDKZri7aVppVGT4Jkw39sesaFnww'] }],
            smartRollups: [{ addresses: ['sr1H6pEjm9nLpvXfuYWGH2Ja29PdA2884YTh'] }],
            contractBoost: { type: 'tzkt' },
        });

        verifyPropertyNames<IndexingDappetizerConfig>(config, [
            'blockQueueSize',
            'configPropertyPath' as any,
            'contractBoost',
            'contractCacheMillis',
            'contracts',
            'fromBlockLevel',
            'headLevelOffset',
            'health',
            'retryDelaysMillis',
            'retryIndefinitely',
            'smartRollups',
            'toBlockLevel',
        ]);
        verifyPropertyNames<NonNullable<IndexingDappetizerConfig['contracts']>[number]>(config.contracts![0], ['addresses', 'name']);
        verifyPropertyNames<NonNullable<IndexingDappetizerConfig['smartRollups']>[number]>(config.smartRollups![0], ['addresses', 'name']);
        verifyPropertyNames<NonNullable<IndexingDappetizerConfig['contractBoost']>>(config.contractBoost!, ['apiUrl', 'type']);
        type HealthConfig = NonNullable<IndexingDappetizerConfig['health']>;
        verifyPropertyNames<HealthConfig>(config.health, ['lastSuccessAge']);
        verifyPropertyNames<HealthConfig['lastSuccessAge']>(config.health.lastSuccessAge, ['degradedMillis', 'unhealthyMillis']);
    });
});
