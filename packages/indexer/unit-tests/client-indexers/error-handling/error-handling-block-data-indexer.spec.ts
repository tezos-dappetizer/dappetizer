import { deepEqual, instance, mock, when } from 'ts-mockito';

import { ContextData, DbContext } from '../../../../../test-utilities/mocks';
import { keyof } from '../../../../utils/src/public-api';
import { BlockDataIndexer } from '../../../src/client-indexers/block-data-indexer';
import { ClientIndexerErrorHandler } from '../../../src/client-indexers/error-handling/client-indexer-error-handler';
import {
    createErrorHandlingBlockDataIndexer,
    IndexMethodOptions,
} from '../../../src/client-indexers/error-handling/error-handling-block-data-indexer';
import { IndexerInfo } from '../../../src/client-indexers/wrappers/indexer-info';
import { BallotOperation } from '../../../src/rpc-data/rpc-data-interfaces';

describe(createErrorHandlingBlockDataIndexer.name, () => {
    let wrappedIndexer: BlockDataIndexer<DbContext, ContextData>;
    let info: IndexerInfo;
    let errorHandler: ClientIndexerErrorHandler;

    beforeEach(() => {
        wrappedIndexer = 'mockedWrappedIndexer' as any;
        errorHandler = mock();
        info = 'mockedInfo' as any;
    });

    it('should wrap indexer correctly', () => {
        setupErrorHandling('indexBalanceUpdate', 'mocked-indexBalanceUpdate');
        setupErrorHandling('indexBlock', 'mocked-indexBlock');
        setupErrorHandling('indexMainOperation', 'mocked-indexMainOperation');
        setupErrorHandling('indexOperationGroup', 'mocked-indexOperationGroup');
        setupErrorHandling('indexOperationWithResult', 'mocked-indexOperationWithResult');

        const resultIndexer = createErrorHandlingBlockDataIndexer(wrappedIndexer, info, instance(errorHandler));

        expect(resultIndexer).toEqual<typeof resultIndexer>({
            indexBalanceUpdate: 'mocked-indexBalanceUpdate' as any,
            indexBlock: 'mocked-indexBlock' as any,
            indexMainOperation: 'mocked-indexMainOperation' as any,
            indexOperationGroup: 'mocked-indexOperationGroup' as any,
            indexOperationWithResult: 'mocked-indexOperationWithResult' as any,
        });
    });

    function setupErrorHandling(method: (keyof typeof wrappedIndexer) & `index${string}`, mockedResult: any) {
        when(errorHandler.wrap<any, any, any>(wrappedIndexer, method, info, deepEqual(new IndexMethodOptions(method)))).thenReturn(mockedResult);
    }
});

describe(IndexMethodOptions.name, () => {
    const target = new IndexMethodOptions('indexFooBar');

    it(`${keyof<typeof target>('getArgsToReport')} should get correct args`, () => {
        const args = target.getArgsToReport({ uid: 'uuu', sourceAddress: 'src' } as BallotOperation, 'db', 'ctx');

        expect(args).toEqual({ uid: 'uuu' });
    });

    it(`${keyof<typeof target>('getArgsToTrace')} should get correct args`, () => {
        const args = target.getArgsToTrace('mockedEntity' as any, 'db', 'ctx');

        expect(args).toEqual({
            fooBar: 'mockedEntity',
            indexingContext: 'ctx',
        });
    });
});
