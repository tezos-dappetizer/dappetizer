import { AsyncOrSync } from 'ts-essentials';
import { anything, instance, mock, when } from 'ts-mockito';

import { expectToThrowAsync, TestClock, TestLogger, verifyCalled } from '../../../../../test-utilities/mocks';
import { ENABLE_TRACE } from '../../../../utils/src/public-api';
import { ClientIndexerErrorHandler } from '../../../src/client-indexers/error-handling/client-indexer-error-handler';
import { IndexerInfo } from '../../../src/client-indexers/wrappers/indexer-info';

interface FooArg { fooVal: string }
interface BarArg { barVal: number }

interface TestIndexer {
    indexVoid?(foo: FooArg, bar: BarArg): AsyncOrSync<void>;
    createVal?(foo: FooArg): AsyncOrSync<BarArg>;
}

describe(ClientIndexerErrorHandler.name, () => {
    let target: ClientIndexerErrorHandler;
    let logger: TestLogger;
    let clock: TestClock;

    let wrappedIndexer: TestIndexer;
    let info: IndexerInfo;
    let testFoo: FooArg;
    let testBar: BarArg;

    beforeEach(() => {
        logger = new TestLogger();
        clock = new TestClock();
        target = new ClientIndexerErrorHandler(clock, logger);

        wrappedIndexer = mock();
        info = { indexerName: 'indexerX', moduleName: 'moduleX', moduleMemberName: 'memberX' };
        testFoo = { fooVal: 'fff' };
        testBar = { barVal: 123 };
    });

    describe.each([true, false])('with logger tracing %s', isLoggerTracing => {
        beforeEach(() => logger.enableLevel('Trace', isLoggerTracing));

        it('should delegate to wrapped indexer if void method', async () => {
            when(wrappedIndexer.indexVoid!(anything(), anything())).thenCall(async () => {
                clock.tick(66);
                return Promise.resolve();
            });
            const method = wrapVoidMethod();
            verifyCalled(wrappedIndexer.indexVoid!).never();

            await method(testFoo, testBar);

            verifyCalled(wrappedIndexer.indexVoid!).onceWith(testFoo, testBar);
            logger.logged(0).verifyMessage('Executing').verify('Debug', {
                ...getExpectedLogData('indexVoid', { foo: 'fff', bar: 123 }),
                argsFull: isLoggerTracing ? { foo: testFoo, bar: testBar } : ENABLE_TRACE,
            });
            logger.logged(1).verifyMessage('executed').verify('Debug', {
                ...getExpectedLogData('indexVoid', { foo: 'fff', bar: 123 }),
                durationMillis: 66,
            });
            logger.verifyLoggedCount(2);
        });

        it('should delegate to wrapped indexer if return method', async () => {
            when(wrappedIndexer.createVal!(anything())).thenCall(async () => {
                clock.tick(66);
                return Promise.resolve(testBar);
            });
            const method = wrapReturnMethod();
            verifyCalled(wrappedIndexer.createVal!).never();

            const bar = await method(testFoo);

            expect(bar).toBe(testBar);
            verifyCalled(wrappedIndexer.createVal!).onceWith(testFoo);
            logger.logged(0).verifyMessage('Executing').verify('Debug', {
                ...getExpectedLogData('createVal', { foo: 'fff' }),
                argsFull: isLoggerTracing ? { foo: testFoo } : ENABLE_TRACE,
            });
            logger.logged(1).verifyMessage('executed', 'ADDITIONAL {bar}').verify('Debug', {
                ...getExpectedLogData('createVal', { foo: 'fff' }),
                durationMillis: 66,
                returnValue: isLoggerTracing ? testBar : ENABLE_TRACE,
                bar: 123,
            });
            logger.verifyLoggedCount(2);
        });

        it('should wrap errors from wrapped indexer', async () => {
            when(wrappedIndexer.indexVoid!(anything(), anything())).thenCall(async () => {
                clock.tick(66);
                return Promise.reject(new Error('oups'));
            });
            const method = wrapVoidMethod();
            verifyCalled(wrappedIndexer.indexVoid!).never();

            const error = await expectToThrowAsync(async () => method(testFoo, testBar));

            expect(error.message).toIncludeMultiple([
                `indexVoid({ foo: 'fff', bar: 123 })`,
                `Error: oups`,
                `'indexerX'`,
                `'moduleX'`,
            ]);
            logger.logged(0).verifyMessage('Executing');
            logger.logged(1).verifyMessage('Failed').verify('Debug', {
                ...getExpectedLogData('indexVoid', { foo: 'fff', bar: 123 }),
                error: new Error('oups'),
                durationMillis: 66,
            });
            logger.verifyLoggedCount(2);
        });

        it('should just pass if void method is undefined on wrapped indexer', async () => {
            when(wrappedIndexer.indexVoid).thenReturn(undefined);
            const method = wrapVoidMethod();

            await method(testFoo, testBar);

            logger.loggedSingle().verifyMessage('Skipping').verify('Debug', getExpectedLogData('indexVoid', { foo: 'fff', bar: 123 }));
        });

        it('should just pass if return method is undefined on wrapped indexer', async () => {
            when(wrappedIndexer.createVal).thenReturn(undefined);
            const method = wrapReturnMethod();

            const bar = await method(testFoo);

            expect(bar).toEqual<BarArg>({ barVal: 999 });
            logger.loggedSingle().verifyMessage('Skipping', 'ADDITIONAL UNDEFINED').verify('Debug', getExpectedLogData('createVal', { foo: 'fff' }));
        });
    });

    function wrapVoidMethod() {
        return target.wrap(instance(wrappedIndexer), 'indexVoid', info, {
            getArgsToReport: (foo: FooArg, bar: BarArg) => ({ foo: foo.fooVal, bar: bar.barVal }),
            getArgsToTrace: (foo: FooArg, bar: BarArg) => ({ foo, bar }),
        });
    }

    function wrapReturnMethod() {
        return target.wrap(instance(wrappedIndexer), 'createVal', info, {
            getArgsToReport: (foo: FooArg) => ({ foo: foo.fooVal }),
            getArgsToTrace: (foo: FooArg) => ({ foo }),
            ifMethodNotDefined: {
                logMessage: 'ADDITIONAL UNDEFINED',
                returnValue: { barVal: 999 },
            },
            afterExecuted: {
                logMessage: 'ADDITIONAL {bar}',
                logMessageData: bar => ({ bar: bar.barVal }),
            },
        });
    }

    function getExpectedLogData(method: keyof TestIndexer, args: object) {
        return { indexer: 'indexerX', module: 'moduleX', method, args };
    }
});
