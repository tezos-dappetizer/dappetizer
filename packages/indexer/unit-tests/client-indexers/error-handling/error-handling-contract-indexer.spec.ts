import { anyOfClass, deepEqual, instance, mock, when } from 'ts-mockito';

import { ContextData, ContractData, DbContext, TestLogger } from '../../../../../test-utilities/mocks';
import { keyof } from '../../../../utils/src/public-api';
import { ContractIndexer } from '../../../src/client-indexers/contract-indexer';
import { ClientIndexerErrorHandler, ErrorHandlingOptions } from '../../../src/client-indexers/error-handling/client-indexer-error-handler';
import {
    createErrorHandlingContractIndexer,
    IndexMethodOptions,
    ShouldIndexOptions,
} from '../../../src/client-indexers/error-handling/error-handling-contract-indexer';
import { IndexerInfo } from '../../../src/client-indexers/wrappers/indexer-info';
import { Contract } from '../../../src/contracts/contract';
import { EntityToIndex, LazyContract, TransactionOperation } from '../../../src/rpc-data/rpc-data-interfaces';

describe(createErrorHandlingContractIndexer.name, () => {
    let wrappedIndexer: ContractIndexer<DbContext, ContextData, ContractData>;
    let info: IndexerInfo;
    let errorHandler: ClientIndexerErrorHandler;

    beforeEach(() => {
        wrappedIndexer = 'mockedWrappedIndexer' as any;
        errorHandler = mock();
        info = 'mockedInfo' as any;
    });

    it('should wrap indexer correctly', () => {
        setupErrorHandling('shouldIndex', anyOfClass(ShouldIndexOptions), 'mockedShouldIndex');
        setupIndexErrorHandling('indexBigMapDiff', 'mocked-indexBigMapDiff');
        setupIndexErrorHandling('indexBigMapUpdate', 'mocked-indexBigMapUpdate');
        setupIndexErrorHandling('indexEvent', 'mocked-indexEvent');
        setupIndexErrorHandling('indexOrigination', 'mocked-indexOrigination');
        setupIndexErrorHandling('indexStorageChange', 'mocked-indexStorageChange');
        setupIndexErrorHandling('indexTransaction', 'mocked-indexTransaction');

        const resultIndexer = createErrorHandlingContractIndexer(wrappedIndexer, info, instance(errorHandler));

        expect(resultIndexer).toEqual<typeof resultIndexer>({
            info,
            shouldIndex: 'mockedShouldIndex' as any,
            indexBigMapDiff: 'mocked-indexBigMapDiff' as any,
            indexBigMapUpdate: 'mocked-indexBigMapUpdate' as any,
            indexEvent: 'mocked-indexEvent' as any,
            indexOrigination: 'mocked-indexOrigination' as any,
            indexStorageChange: 'mocked-indexStorageChange' as any,
            indexTransaction: 'mocked-indexTransaction' as any,
        });
    });

    function setupIndexErrorHandling(method: (keyof typeof wrappedIndexer) & `index${string}`, mockedResult: any) {
        setupErrorHandling(method, deepEqual(new IndexMethodOptions(method)), mockedResult);
    }

    function setupErrorHandling(method: keyof typeof wrappedIndexer, expectedOptions: ErrorHandlingOptions<any, any>, mockedResult: any) {
        when(errorHandler.wrap<any, any, any>(wrappedIndexer, method, info, expectedOptions)).thenReturn(mockedResult);
    }
});

describe(IndexMethodOptions.name, () => {
    let target: IndexMethodOptions<EntityToIndex, unknown, { contract: Contract }>;
    let operation: TransactionOperation;
    let indexingContext: { contract: Contract };

    beforeEach(() => {
        target = new IndexMethodOptions('indexFooBar');
        operation = { uid: 'uuu', sourceAddress: 'src' } as typeof operation;
        indexingContext = { contract: { address: 'KT1WNoqaFFN3cp3fZTaPY1uSuzT1xV51u1cq' } } as typeof indexingContext;
    });

    it(`${keyof<typeof target>('getArgsToReport')} should get correct args`, () => {
        const args = target.getArgsToReport(operation, 'db', indexingContext);

        expect(args).toEqual({ uid: 'uuu', contract: 'KT1WNoqaFFN3cp3fZTaPY1uSuzT1xV51u1cq' });
    });

    it(`${keyof<typeof target>('getArgsToTrace')} should get correct args`, () => {
        const args = target.getArgsToTrace(operation, 'db', indexingContext);

        expect(args).toEqual({ fooBar: operation, indexingContext });
    });
});

describe(ShouldIndexOptions.name, () => {
    const target = new ShouldIndexOptions();

    it.each([
        ['false if false', false, false],
        ['true if valid', 'mockedData', true],
    ])(`${keyof<typeof target>('afterExecuted')} should log %s contract data`, (_desc, contractData, expectedWillIndex) => {
        const logger = new TestLogger();

        const logData = target.afterExecuted.logMessageArgs(contractData);
        logger.logDebug(target.afterExecuted.logMessage, logData);

        logger.loggedSingle().verifyData({ willIndex: expectedWillIndex });
    });

    it(`${keyof<typeof target>('getArgsToReport')} should get correct args`, () => {
        const contract = { address: 'KT1WNoqaFFN3cp3fZTaPY1uSuzT1xV51u1cq' } as LazyContract;

        const args = target.getArgsToReport(contract, 'db');

        expect(args).toEqual({ contract: 'KT1WNoqaFFN3cp3fZTaPY1uSuzT1xV51u1cq' });
    });

    it(`${keyof<typeof target>('getArgsToTrace')} should get correct args`, () => {
        const args = target.getArgsToTrace('mockedContract' as any, 'db');

        expect(args).toEqual({ contract: 'mockedContract' });
    });
});
