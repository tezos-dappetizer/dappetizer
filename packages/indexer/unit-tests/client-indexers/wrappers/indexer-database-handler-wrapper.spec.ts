import { instance, mock, verify, when } from 'ts-mockito';

import { DbContext, describeMemberFactory, expectToThrowAsync } from '../../../../../test-utilities/mocks';
import { IndexerDatabaseHandler } from '../../../src/client-indexers/indexer-database-handler';
import { IndexerDatabaseHandlerWrapper } from '../../../src/client-indexers/wrappers/indexer-database-handler-wrapper';
import { Block } from '../../../src/rpc-data/rpc-data-interfaces';

class FooHandler {}

describe(IndexerDatabaseHandlerWrapper.name, () => {
    let nextHandler: IndexerDatabaseHandler<DbContext>;
    let db: DbContext;
    let testBlock: Block;

    const getTarget = () => new IndexerDatabaseHandlerWrapper(instance(nextHandler));

    beforeEach(() => {
        nextHandler = mock();
        db = 'mockedDb' as any;
        testBlock = {
            hash: 'haha',
            level: 666,
            predecessorBlockHash: 'pred',
        } as Block;

        instance(nextHandler).constructor = FooHandler;
    });

    const describeMember = describeMemberFactory<IndexerDatabaseHandlerWrapper>();

    describeMember('name', () => {
        it('should resolve constructor name', () => {
            expect(getTarget().name).toBe('FooHandler');
        });

        it('should resolve default name if no constructor e.g. object literal', () => {
            instance(nextHandler).constructor = Object;

            expect(getTarget().name).toBe('IndexerDatabaseHandler');
        });
    });

    shouldDelegateCorrectly({
        methodName: 'startTransaction',
        act: t => t.startTransaction(),
        successTestCases: [{ testResult: 'mockedDb' as any }],
        expectedError: `startTransaction()`,
    });

    shouldDelegateCorrectly({
        methodName: 'commitTransaction',
        act: t => t.commitTransaction(db),
        expectedError: 'commitTransaction()',
    });

    shouldDelegateCorrectly({
        methodName: 'rollBackTransaction',
        act: t => t.rollBackTransaction(db),
        expectedError: 'rollBackTransaction()',
    });

    shouldDelegateCorrectly({
        methodName: 'insertBlock',
        act: t => t.insertBlock(testBlock, db),
        expectedError: `insertBlock({ block: { hash: 'haha', level: 666 } })`,
    });

    shouldDelegateCorrectly({
        methodName: 'deleteBlock',
        act: t => t.deleteBlock(testBlock, db),
        expectedError: `deleteBlock({ block: { hash: 'haha', level: 666 } })`,
    });

    shouldDelegateCorrectly({
        methodName: 'getIndexedChainId',
        act: t => t.getIndexedChainId(),
        successTestCases: [
            { description: 'undefined', testResult: undefined, expectedResult: null },
            { description: 'null', testResult: null },
            { description: 'valid chain id', testResult: 'ccc' },
        ],
        expectedError: `getIndexedChainId()`,
    });

    shouldDelegateCorrectly({
        methodName: 'getLastIndexedBlock',
        act: t => t.getLastIndexedBlock(),
        successTestCases: [
            { description: 'undefined', testResult: undefined, expectedResult: null },
            { description: 'null', testResult: null },
            { description: 'valid block', testResult: { hash: 'hihi', level: 777 } },
        ],
        expectedError: `getLastIndexedBlock()`,
    });

    shouldDelegateCorrectly({
        methodName: 'getIndexedBlock',
        act: t => t.getIndexedBlock(123),
        successTestCases: [
            { description: 'undefined', testResult: undefined, expectedResult: null },
            { description: 'null', testResult: null },
            { description: 'valid block', testResult: { hash: 'haha', level: 666 } },
        ],
        expectedError: `getIndexedBlock({ level: 123 })`,
    });

    function shouldDelegateCorrectly<T>(options: {
        methodName: keyof IndexerDatabaseHandlerWrapper;
        act: (i: IndexerDatabaseHandler<DbContext>) => T | PromiseLike<T>;
        successTestCases?: {
            description?: string;
            testResult: T;
            expectedResult?: T;
        }[];
        expectedError: string;
    }) {
        describe(options.methodName, () => {
            // Add undefined test case for void methods.
            (options.successTestCases ?? [{ testResult: undefined as T }]).forEach(testCase => {
                it(`should delegate to wrapped handler ${testCase.description ?? ''}`.trimEnd(), async () => {
                    when(options.act(nextHandler)).thenReturn(Promise.resolve(testCase.testResult));

                    const result = await options.act(getTarget());

                    expect(result).toEqual('expectedResult' in testCase ? testCase.expectedResult : testCase.testResult);
                    expect(result).toBeFrozen();
                    verify(options.act(nextHandler)).once();
                });
            });

            it('should wrap errors from wrapped handler', async () => {
                when(options.act(nextHandler)).thenReject(new Error('oups'));
                const target = getTarget();

                // eslint-disable-next-line @typescript-eslint/require-await
                const error = await expectToThrowAsync(async () => options.act(target));

                expect(error.message).toIncludeMultiple([`'FooHandler'`, 'oups', options.expectedError]);
            });
        });
    }
});
