import { Writable } from 'ts-essentials';
import { anything, capture, instance, mock, verify, when } from 'ts-mockito';

import { MockedIndexerModule, mockIndexerModule } from './traverser-test-utils';
import { ContextData, DbContext, TestLogger } from '../../../../test-utilities/mocks';
import { entriesOf } from '../../../utils/src/basics/public-api';
import { asReadonly } from '../../../utils/src/public-api';
import { BalanceUpdateTraverser } from '../../src/block-traversers/balance-update-traverser';
import { MainOperationTraverser } from '../../src/block-traversers/main-operation-traverser';
import { OperationWithResultTraverser } from '../../src/block-traversers/operation-with-result-traverser';
import { SmartRollupOperationTraverser } from '../../src/block-traversers/smart-rollup-operation-traverser';
import { BalanceUpdateIndexingContext, MainOperationIndexingContext } from '../../src/client-indexers/block-data-indexer';
import { BalanceUpdate, InternalOperation, MainOperation, OperationKind } from '../../src/rpc-data/rpc-data-interfaces';

describe(MainOperationTraverser.name, () => {
    let target: MainOperationTraverser<DbContext>;
    let smartRollupOperationTraverser: SmartRollupOperationTraverser<DbContext>;
    let operationWithResultTraverser: OperationWithResultTraverser<DbContext>;
    let balanceUpdateTraverser: BalanceUpdateTraverser<DbContext>;
    let logger: TestLogger;

    let indexerModule: MockedIndexerModule;
    let db: DbContext;
    let mainOperation: Writable<MainOperation>;
    let internalOp1: InternalOperation;
    let internalOp2: InternalOperation;
    let balanceUpdates: readonly BalanceUpdate[];
    let indexingContext: MainOperationIndexingContext<ContextData>;

    const act = async () => target.traverse(indexerModule, db, mainOperation, indexingContext);

    beforeEach(() => {
        target = new MainOperationTraverser(
            instance(smartRollupOperationTraverser = mock()),
            instance(operationWithResultTraverser = mock()),
            instance(balanceUpdateTraverser = mock()),
            logger = new TestLogger(),
        );

        indexerModule = mockIndexerModule();
        db = 'mockedDb' as any;
        internalOp1 = 'mockedInternalOp1' as any;
        internalOp2 = 'mockedInternalOp2' as any;
        balanceUpdates = 'mockedBalanceUpdates' as any;
        mainOperation = {
            kind: OperationKind.Transaction,
            internalOperations: asReadonly([internalOp1, internalOp2]),
            balanceUpdates,
        } as typeof mainOperation;
        indexingContext = { data: { ctxVal: 'abc' } } as typeof indexingContext;
    });

    interface ExpectedTraversed {
        smartRollupOperation?: true;
        operationWithResult?: true;
        balanceUpdates?: true;
    }

    const testCases: Record<MainOperation['kind'], ExpectedTraversed> = {
        ActivateAccount: { balanceUpdates: true },
        Ballot: {},
        Delegation: { balanceUpdates: true, operationWithResult: true },
        DoubleBakingEvidence: { balanceUpdates: true },
        DoubleEndorsementEvidence: { balanceUpdates: true },
        DoublePreendorsementEvidence: { balanceUpdates: true },
        DrainDelegate: { balanceUpdates: true },
        Endorsement: { balanceUpdates: true },
        IncreasePaidStorage: { balanceUpdates: true, operationWithResult: true },
        Origination: { balanceUpdates: true, operationWithResult: true },
        Preendorsement: { balanceUpdates: true },
        Proposals: {},
        RegisterGlobalConstant: { balanceUpdates: true, operationWithResult: true },
        Reveal: { balanceUpdates: true, operationWithResult: true },
        SeedNonceRevelation: { balanceUpdates: true },
        SetDepositsLimit: { balanceUpdates: true, operationWithResult: true },
        SmartRollupAddMessages: { balanceUpdates: true, operationWithResult: true, smartRollupOperation: true },
        SmartRollupCement: { balanceUpdates: true, operationWithResult: true, smartRollupOperation: true },
        SmartRollupExecuteOutboxMessage: { balanceUpdates: true, operationWithResult: true, smartRollupOperation: true },
        SmartRollupOriginate: { balanceUpdates: true, operationWithResult: true, smartRollupOperation: true },
        SmartRollupPublish: { balanceUpdates: true, operationWithResult: true, smartRollupOperation: true },
        SmartRollupRecoverBond: { balanceUpdates: true, operationWithResult: true, smartRollupOperation: true },
        SmartRollupRefute: { balanceUpdates: true, operationWithResult: true, smartRollupOperation: true },
        SmartRollupTimeout: { balanceUpdates: true, operationWithResult: true, smartRollupOperation: true },
        Transaction: { balanceUpdates: true, operationWithResult: true },
        TransferTicket: { balanceUpdates: true, operationWithResult: true },
        UpdateConsensusKey: { balanceUpdates: true, operationWithResult: true },
        VdfRevelation: { balanceUpdates: true },
    };

    it.each(entriesOf(testCases))('should index %s and traverse %s', async (kind, expectedTraversed) => {
        mainOperation.kind = kind;

        await act();

        verify(indexerModule.mocks.blockDataIndexer.indexMainOperation(mainOperation, db, indexingContext)).once();

        if (expectedTraversed.smartRollupOperation) {
            verify(smartRollupOperationTraverser.traverse(indexerModule, db, mainOperation as any, indexingContext)).once();
        } else {
            verify(smartRollupOperationTraverser.traverse(anything(), anything(), anything(), anything())).never();
        }

        if (expectedTraversed.operationWithResult) {
            const receivedContext = capture<any, any, any, any>(operationWithResultTraverser.traverse).first()[3];
            expect(receivedContext).toEqual({ ...indexingContext, mainOperation });
            expect(receivedContext).toBeFrozen();

            verify(operationWithResultTraverser.traverse(anything(), anything(), anything(), anything())).times(3);
            verify(operationWithResultTraverser.traverse(indexerModule, db, mainOperation as any, receivedContext)).once();
            verify(operationWithResultTraverser.traverse(indexerModule, db, internalOp1, receivedContext)).once();
            verify(operationWithResultTraverser.traverse(indexerModule, db, internalOp2, receivedContext)).once();
        } else {
            verify(operationWithResultTraverser.traverse(anything(), anything(), anything(), anything())).never();
        }

        if (expectedTraversed.balanceUpdates) {
            const receivedContext = capture<any, any, any, any>(balanceUpdateTraverser.traverse).first()[3];
            expect(receivedContext).toEqual<BalanceUpdateIndexingContext<ContextData>>({
                ...indexingContext,
                balanceUpdateSource: 'MainOperationMetadata',
                mainOperation: mainOperation as any,
                internalOperation: null,
            });
            expect(receivedContext).toBeFrozen();

            verify(balanceUpdateTraverser.traverse(indexerModule, db, balanceUpdates, receivedContext)).once();
        } else {
            verify(balanceUpdateTraverser.traverse(anything(), anything(), anything(), anything())).never();
        }
    });

    it('should put operation kind to log scope', async () => {
        when(indexerModule.mocks.blockDataIndexer.indexMainOperation(anything(), anything(), anything())).thenCall(() => {
            logger.logDebug('SCOPED');
        });

        await act();

        logger.loggedSingle().verify('Debug', { indexedMainOperationKind: OperationKind.Transaction }).verifyMessage('SCOPED');
    });
});
