import { anything, verify } from 'ts-mockito';

import { MockedIndexerModule, mockIndexerModule } from './traverser-test-utils';
import { ContextData, DbContext } from '../../../../test-utilities/mocks';
import { BalanceUpdateTraverser } from '../../src/block-traversers/balance-update-traverser';
import { BalanceUpdateIndexingContext } from '../../src/client-indexers/block-data-indexer';
import { BalanceUpdate } from '../../src/rpc-data/rpc-data-interfaces';

describe(BalanceUpdateTraverser.name, () => {
    let target: BalanceUpdateTraverser<DbContext>;

    let indexerModule: MockedIndexerModule;
    let db: DbContext;
    let balanceUpdate1: BalanceUpdate;
    let balanceUpdate2: BalanceUpdate;
    let indexingContext: BalanceUpdateIndexingContext<ContextData>;

    const act = async () => target.traverse(indexerModule, db, [balanceUpdate1, balanceUpdate2], indexingContext);

    beforeEach(() => {
        target = new BalanceUpdateTraverser();

        indexerModule = mockIndexerModule();
        db = 'mockedDb' as any;
        balanceUpdate1 = 'mockedUpdate1' as any;
        balanceUpdate2 = 'mockedUpdate2' as any;
        indexingContext = { data: { ctxVal: 'abc' } } as typeof indexingContext;
    });

    it('should index balance updates', async () => {
        await act();

        verify(indexerModule.mocks.blockDataIndexer.indexBalanceUpdate(anything(), anything(), anything())).times(2);
        verify(indexerModule.mocks.blockDataIndexer.indexBalanceUpdate(balanceUpdate1, db, indexingContext)).once();
        verify(indexerModule.mocks.blockDataIndexer.indexBalanceUpdate(balanceUpdate2, db, indexingContext)).once();
    });
});
