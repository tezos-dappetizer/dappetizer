import { StrictExclude, Writable } from 'ts-essentials';
import { anything, capture, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { MockedIndexerModule, mockIndexerModule } from './traverser-test-utils';
import { ContextData, DbContext, fixedMock } from '../../../../test-utilities/mocks';
import { keysOf } from '../../../utils/src/basics/public-api';
import { SmartRollupOperationTraverser } from '../../src/block-traversers/smart-rollup-operation-traverser';
import { MainOperationIndexingContext } from '../../src/client-indexers/block-data-indexer';
import {
    SmartRollupAddMessagesIndexingContext,
    SmartRollupIndexingContext,
} from '../../src/client-indexers/smart-rollup-indexer';
import { TrustedSmartRollupIndexer } from '../../src/client-indexers/wrappers/trusted-indexer-interfaces';
import {
    OperationKind,
    OperationResultStatus,
    SmartRollupOperation,
    SmartRollupOriginateOperation,
} from '../../src/rpc-data/rpc-data-interfaces';
import {
    BoundSmartRollupIndexer,
    UNBOUND_ROLLUP_DATA,
    UnboundRollupData,
} from '../../src/smart-rollups/bound-indexer/bound-smart-rollup-indexer';
import { SmartRollup } from '../../src/smart-rollups/smart-rollup';

describe(SmartRollupOperationTraverser.name, () => {
    const target = new SmartRollupOperationTraverser<DbContext>();

    let indexerModule: MockedIndexerModule;
    let rawIndexer1: TrustedSmartRollupIndexer<DbContext, ContextData, unknown>;
    let rawIndexer2: TrustedSmartRollupIndexer<DbContext, ContextData, unknown>;
    let boundIndexer: BoundSmartRollupIndexer<DbContext, ContextData>;
    let db: DbContext;
    let operation: Writable<SmartRollupOperation>;
    let indexingContext: MainOperationIndexingContext<ContextData>;
    let rollup: SmartRollup;

    const act = async () => target.traverse(indexerModule, db, operation, indexingContext);

    beforeEach(() => {
        indexerModule = mockIndexerModule();
        db = 'mockedDb' as any;
        rollup = 'mockedRollup' as any;
        operation = {
            kind: OperationKind.SmartRollupCement,
            result: { status: OperationResultStatus.Applied },
            rollup,
        } as typeof operation;
        indexingContext = { data: { ctxVal: 'abc' } } as typeof indexingContext;

        [rawIndexer1, rawIndexer2, boundIndexer] = [mock(), mock(), fixedMock()];
        when(indexerModule.mocks.smartRollupIndexers!.rawIndexers).thenReturn([instance(rawIndexer1), instance(rawIndexer2)]);
        when(indexerModule.mocks.smartRollupIndexers!.resolve(rollup, db)).thenResolve(instance(boundIndexer));
    });

    it('should not index anything if no smart rollup indexers', async () => {
        indexerModule.smartRollupIndexers = null;

        await act();
    });

    it.each([
        OperationResultStatus.Backtracked,
        OperationResultStatus.Failed,
        OperationResultStatus.Skipped,
    ])('should not index anything if operation is %s', async status => {
        operation.result = { status } as typeof operation.result;

        await act();

        verify(indexerModule.mocks.smartRollupIndexers!.resolve(anything(), anything())).never();
        verify(indexerModule.mocks.smartRollupIndexers!.rawIndexers).never();
    });

    it(`should index ${OperationKind.SmartRollupAddMessages} on all raw indexers`, async () => {
        operation.kind = OperationKind.SmartRollupAddMessages;

        await act();

        const receivedContext = capture(rawIndexer1.indexAddMessages).first()[2];
        expect(receivedContext).toEqual({
            data: { ctxVal: 'abc' },
            mainOperation: operation,
        } as SmartRollupAddMessagesIndexingContext<ContextData>);
        expect(receivedContext).toBeFrozen();

        verify(rawIndexer1.indexAddMessages(operation as any, db, receivedContext)).once();
        verify(rawIndexer2.indexAddMessages(operation as any, db, receivedContext)).once();
        verifyBoundIndexerCalledCorrectly();
    });

    it(`should index ${OperationKind.SmartRollupOriginate} on bound indexer correctly`, async () => {
        operation = {
            kind: OperationKind.SmartRollupOriginate,
            result: {
                status: OperationResultStatus.Applied,
                originatedRollup: rollup,
            },
        } as SmartRollupOriginateOperation;

        await act();

        verifyBoundIndexerCalledCorrectly();
    });

    type SpecialKinds = OperationKind.SmartRollupAddMessages | OperationKind.SmartRollupOriginate;
    const kindsToTest: Record<StrictExclude<SmartRollupOperation['kind'], SpecialKinds>, ''> = {
        SmartRollupCement: '',
        SmartRollupExecuteOutboxMessage: '',
        SmartRollupPublish: '',
        SmartRollupRecoverBond: '',
        SmartRollupRefute: '',
        SmartRollupTimeout: '',
    };
    it.each(keysOf(kindsToTest))('should index %s on bound indexer correctly', async kind => {
        operation.kind = kind;

        await act();

        verifyBoundIndexerCalledCorrectly();
    });

    function verifyBoundIndexerCalledCorrectly() {
        const expectedContext: any = {
            data: { ctxVal: 'abc' },
            mainOperation: operation,
            rollup,
            rollupData: UNBOUND_ROLLUP_DATA,
        } as SmartRollupIndexingContext<ContextData, UnboundRollupData>;

        if (operation.kind === OperationKind.SmartRollupCement) {
            verify(boundIndexer.indexCement(operation as any, db, deepEqual(expectedContext))).once();
        } else {
            verify(boundIndexer.indexCement(anything(), anything(), anything())).never();
        }
        if (operation.kind === OperationKind.SmartRollupExecuteOutboxMessage) {
            verify(boundIndexer.indexExecuteOutboxMessage(operation as any, db, deepEqual(expectedContext))).once();
        } else {
            verify(boundIndexer.indexExecuteOutboxMessage(anything(), anything(), anything())).never();
        }
        if (operation.kind === OperationKind.SmartRollupOriginate) {
            verify(boundIndexer.indexOriginate(operation as any, db, deepEqual(expectedContext))).once();
        } else {
            verify(boundIndexer.indexOriginate(anything(), anything(), anything())).never();
        }
        if (operation.kind === OperationKind.SmartRollupPublish) {
            verify(boundIndexer.indexPublish(operation as any, db, deepEqual(expectedContext))).once();
        } else {
            verify(boundIndexer.indexPublish(anything(), anything(), anything())).never();
        }
        if (operation.kind === OperationKind.SmartRollupRecoverBond) {
            verify(boundIndexer.indexRecoverBond(operation as any, db, deepEqual(expectedContext))).once();
        } else {
            verify(boundIndexer.indexRecoverBond(anything(), anything(), anything())).never();
        }
        if (operation.kind === OperationKind.SmartRollupRefute) {
            verify(boundIndexer.indexRefute(operation as any, db, deepEqual(expectedContext))).once();
        } else {
            verify(boundIndexer.indexRefute(anything(), anything(), anything())).never();
        }
        if (operation.kind === OperationKind.SmartRollupTimeout) {
            verify(boundIndexer.indexTimeout(operation as any, db, deepEqual(expectedContext))).once();
        } else {
            verify(boundIndexer.indexTimeout(anything(), anything(), anything())).never();
        }
    }
});
