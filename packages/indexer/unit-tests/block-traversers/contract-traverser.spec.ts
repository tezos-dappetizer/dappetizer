import { randomUUID } from 'crypto';
import { Writable } from 'ts-essentials';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { MockedIndexerModule, mockIndexerModule } from './traverser-test-utils';
import { ContextData, DbContext, describeMemberFactory, expectToThrowAsync, fixedMock, TestLogger } from '../../../../test-utilities/mocks';
import { asReadonly } from '../../../utils/src/public-api';
import { ContractTraverser, ContractTraversingContext } from '../../src/block-traversers/contract-traverser';
import { ContractEvent } from '../../src/client-indexers/contract-indexer';
import { BoundContractIndexer, UNBOUND_CONTRACT_DATA } from '../../src/contracts/bound-indexer/bound-contract-indexer';
import { Contract, ContractConfig } from '../../src/contracts/contract';
import { scopedLogKeys } from '../../src/helpers/scoped-log-keys';
import { MichelsonSchema } from '../../src/michelson/michelson-schema';
import {
    Applied,
    BigMapAlloc,
    BigMapDiffAction,
    BigMapUpdate,
    EventInternalOperation,
    LazyBigMapDiff,
    LazyContract,
    LazyStorageChange,
    LazyTransactionParameter,
    OperationKind,
    OperationResultStatus,
    OriginationOperation,
    StorageChange,
    TransactionOperation,
    TransactionParameter,
} from '../../src/rpc-data/rpc-data-interfaces';

describe(ContractTraverser.name, () => {
    let target: ContractTraverser<DbContext>;
    let logger: TestLogger;

    let indexerModule: MockedIndexerModule;
    let db: DbContext;
    let contractIndexer: BoundContractIndexer<DbContext, ContextData>;
    let indexingContext: Writable<ContractTraversingContext<ContextData>>;

    let lazyContract: LazyContract;
    let contract: Contract;
    let lazyTransactionParameter: LazyTransactionParameter;
    let transactionParameter: TransactionParameter;
    let lazyStorageChange: LazyStorageChange;
    let storageChange: StorageChange;
    let lazyBigMapDiff1: LazyBigMapDiff;
    let lazyBigMapDiff2: LazyBigMapDiff;
    let lazyBigMapDiff3: LazyBigMapDiff;
    let bigMapDiff1: BigMapUpdate;
    let bigMapDiff2: BigMapAlloc;
    let bigMapDiff3: BigMapUpdate;
    let event1: ContractEvent;
    let event2: ContractEvent;

    const act = async () => target.traverse(indexerModule, db, indexingContext);

    beforeEach(() => {
        logger = new TestLogger();
        target = new ContractTraverser(logger);

        [lazyContract, lazyTransactionParameter, lazyStorageChange] = [mock(), mock(), mock()];
        contract = 'contractMock' as any;
        transactionParameter = 'transactionParamMock' as any;
        storageChange = 'storageChangeMock' as any;
        [lazyBigMapDiff1, lazyBigMapDiff2, lazyBigMapDiff3] = [mock(), mock(), mock()];
        bigMapDiff1 = { action: BigMapDiffAction.Update, keyHash: 'k1' } as BigMapUpdate;
        bigMapDiff2 = { action: BigMapDiffAction.Alloc, keyType: new MichelsonSchema({ prim: 'int' }) } as BigMapAlloc;
        bigMapDiff3 = { action: BigMapDiffAction.Update, keyHash: 'k2' } as BigMapUpdate;
        event1 = mockEvent('KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5', OperationResultStatus.Applied) as ContractEvent;
        event2 = mockEvent('KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5', OperationResultStatus.Applied) as ContractEvent;

        indexerModule = mockIndexerModule();
        db = 'mockedDb' as any;
        contractIndexer = fixedMock();
        indexingContext = {
            block: 'blockMock' as any,
            contract: instance(lazyContract),
            transactionParameter: instance(lazyTransactionParameter),
            storageChange: instance(lazyStorageChange),
            bigMapDiffs: asReadonly([instance(lazyBigMapDiff1), instance(lazyBigMapDiff2), instance(lazyBigMapDiff3)]),
            operationWithResult: { kind: OperationKind.Transaction },
            mainOperation: {
                internalOperations: asReadonly([
                    { kind: OperationKind.Delegation },
                    mockEvent('KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5', OperationResultStatus.Failed), // Excluded from events b/c failed.
                    event1,
                    mockEvent('KT1QwuX9tHLjvGEkSU2zDhQ3iibFeDuC8HDP', OperationResultStatus.Applied), // Excluded from events b/c different contract.
                    event2,
                ]),
            },
        } as typeof indexingContext;

        when(lazyContract.address).thenReturn('KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5');
        when(lazyContract.config).thenReturn({ toBeIndexed: true } as ContractConfig);
        when(lazyContract.getContract()).thenResolve(contract);
        when(lazyTransactionParameter.getTransactionParameter()).thenResolve(transactionParameter);
        when(lazyStorageChange.getStorageChange()).thenResolve(storageChange);
        when(lazyBigMapDiff1.getBigMapDiff()).thenResolve(bigMapDiff1);
        when(lazyBigMapDiff2.getBigMapDiff()).thenResolve(bigMapDiff2);
        when(lazyBigMapDiff3.getBigMapDiff()).thenResolve(bigMapDiff3);

        when(indexerModule.mocks.contractIndexers!.resolve(instance(lazyContract), db)).thenResolve(instance(contractIndexer));
    });

    const describeMember = describeMemberFactory<typeof contractIndexer>();

    describe('common logic before indexing', () => {
        it('should not index anything if no data', async () => {
            indexingContext.transactionParameter = null;
            indexingContext.storageChange = null;
            indexingContext.bigMapDiffs = [];

            await runNoIndexingTest();
        });

        it('should handle no contract indexers', async () => {
            indexerModule.contractIndexers = null;

            await runNoIndexingTest();
        });

        async function runNoIndexingTest() {
            await act();

            verify(indexerModule.mocks.contractIndexers!.resolve(anything(), anything())).never();
            verify(lazyContract!.getContract()).never();
            verifyNothingIndexed();
        }

        it('should not index anything if contract not configured to be indexed', async () => {
            when(lazyContract!.config).thenReturn({ toBeIndexed: false } as ContractConfig);

            await act();

            verify(indexerModule.mocks.contractIndexers!.resolve(anything(), anything())).never();
            verify(lazyContract!.getContract()).never();
            verifyNothingIndexed();
        });

        it('should not index anything if contract should not be indexed in runtime', async () => {
            when(indexerModule.mocks.contractIndexers!.resolve(anything(), anything())).thenResolve(null);

            await act();

            verify(indexerModule.mocks.contractIndexers!.resolve(instance(lazyContract!), db)).once();
            verify(lazyContract!.getContract()).never();
            verifyNothingIndexed();
        });

        it('should put contract address to log scope', async () => {
            when(lazyContract!.getContract()).thenCall(async () => {
                logger.logDebug('SCOPED 2');
                return Promise.resolve(contract);
            });

            await act();

            logger.loggedSingle().verify('Debug', { [scopedLogKeys.INDEXED_CONTRACT_ADDRESS]: 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5' });
        });

        function verifyNothingIndexed() {
            verify(contractIndexer.indexOrigination(anything(), anything(), anything())).never();
            verify(contractIndexer.indexTransaction(anything(), anything(), anything())).never();
            verify(contractIndexer.indexStorageChange(anything(), anything(), anything())).never();
            verify(contractIndexer.indexBigMapDiff(anything(), anything(), anything())).never();
            verify(contractIndexer.indexBigMapUpdate(anything(), anything(), anything())).never();

            verify(lazyTransactionParameter.getTransactionParameter()).never();
            verify(lazyStorageChange.getStorageChange()).never();
            verify(lazyBigMapDiff1.getBigMapDiff()).never();
        }
    });

    describeMember('indexOrigination', () => {
        beforeEach(() => {
            setupOperationWithResult(OperationKind.Origination);
            indexingContext.transactionParameter = null;
        });

        it('should index correctly', async () => {
            await act();

            verify(contractIndexer.indexOrigination(indexingContext.operationWithResult as any, db, deepEqual({
                ...indexingContext,
                contract,
                contractData: UNBOUND_CONTRACT_DATA,
                transactionParameter: null,
                storageChange,
                bigMapDiffs: [bigMapDiff1, bigMapDiff2, bigMapDiff3],
                operationWithResult: indexingContext.operationWithResult as Applied<OriginationOperation>,
                events: [event1, event2],
            }))).once();
        });

        it('should not index if not origination', async () => {
            setupOperationWithResult(OperationKind.Transaction);

            await act();

            verify(contractIndexer.indexOrigination(anything(), anything(), anything())).never();
        });
    });

    describeMember('indexTransaction', () => {
        it('should index correctly', async () => {
            await act();

            verify(contractIndexer.indexTransaction(transactionParameter, db, deepEqual({
                ...indexingContext,
                contract,
                contractData: UNBOUND_CONTRACT_DATA,
                storageChange,
                bigMapDiffs: [bigMapDiff1, bigMapDiff2, bigMapDiff3],
                operationWithResult: indexingContext.operationWithResult as Applied<TransactionOperation>,
                events: [event1, event2],
                transactionParameter,
            }))).once();
        });

        it('should not index if no transaction parameter', async () => {
            indexingContext.transactionParameter = null!;

            await act();

            verify(contractIndexer.indexTransaction(anything(), anything(), anything())).never();
        });

        it('should throw if not transaction', async () => {
            setupOperationWithResult(OperationKind.Origination);

            const error = await expectToThrowAsync(act);

            expect(error.message).toIncludeMultiple(['transaction', OperationKind.Origination]);
        });
    });

    describeMember('indexStorageChange', () => {
        it('should index correctly', async () => {
            await act();

            verify(contractIndexer.indexStorageChange(storageChange, db, deepEqual({
                ...indexingContext,
                contract,
                contractData: UNBOUND_CONTRACT_DATA,
                transactionParameter,
                bigMapDiffs: [bigMapDiff1, bigMapDiff2, bigMapDiff3],
                events: [event1, event2],
                storageChange,
            }))).once();
        });

        it('should not index if no storage change', async () => {
            indexingContext.storageChange = null!;

            await act();

            verify(contractIndexer.indexStorageChange(anything(), anything(), anything())).never();
        });
    });

    describeMember('indexBigMapDiff', () => {
        it('should index correctly', async () => {
            await act();

            verify(contractIndexer.indexBigMapDiff(anything(), anything(), anything())).times(3);
            for (const bigMapDiff of [bigMapDiff1, bigMapDiff2, bigMapDiff3]) {
                verify(contractIndexer.indexBigMapDiff(bigMapDiff, db, deepEqual({
                    ...indexingContext,
                    contract,
                    contractData: UNBOUND_CONTRACT_DATA,
                    transactionParameter,
                    storageChange,
                    bigMapDiffs: [bigMapDiff1, bigMapDiff2, bigMapDiff3],
                    events: [event1, event2],
                    bigMapDiff,
                }))).once();
            }

            verify(contractIndexer.indexBigMapUpdate(anything(), anything(), anything())).times(2);
            for (const bigMapDiff of [bigMapDiff1, bigMapDiff3]) {
                verify(contractIndexer.indexBigMapUpdate(bigMapDiff, db, deepEqual({
                    ...indexingContext,
                    contract,
                    contractData: UNBOUND_CONTRACT_DATA,
                    transactionParameter,
                    storageChange,
                    bigMapDiffs: [bigMapDiff1, bigMapDiff2, bigMapDiff3],
                    events: [event1, event2],
                    bigMapDiff,
                }))).once();
            }
        });
    });

    describeMember('indexEvent', () => {
        it('should index correctly', async () => {
            await act();

            verify(contractIndexer.indexEvent(anything(), anything(), anything())).times(2);
            for (const event of [event1, event2]) {
                verify(contractIndexer.indexEvent(event, db, deepEqual({
                    ...indexingContext,
                    contract,
                    contractData: UNBOUND_CONTRACT_DATA,
                    transactionParameter,
                    storageChange,
                    bigMapDiffs: [bigMapDiff1, bigMapDiff2, bigMapDiff3],
                    events: [event1, event2],
                    event,
                }))).once();
            }
        });
    });

    function setupOperationWithResult(kind: typeof indexingContext.operationWithResult.kind) {
        indexingContext.operationWithResult = { kind } as typeof indexingContext.operationWithResult;
    }

    function mockEvent(sourceContractAddress: string, status: OperationResultStatus) {
        return {
            kind: OperationKind.Event,
            tag: randomUUID(),
            source: { address: sourceContractAddress, type: 'Contract' },
            result: { status },
        } as EventInternalOperation;
    }
});
