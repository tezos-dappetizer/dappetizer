import { anything, capture, deepEqual, instance, mock, verify } from 'ts-mockito';

import { MockedIndexerModule, mockIndexerModule } from './traverser-test-utils';
import { ContextData, DbContext } from '../../../../test-utilities/mocks';
import { typed } from '../../../utils/src/public-api';
import { BalanceUpdateTraverser } from '../../src/block-traversers/balance-update-traverser';
import { ContractTraverser } from '../../src/block-traversers/contract-traverser';
import { OperationWithResultTraverser } from '../../src/block-traversers/operation-with-result-traverser';
import {
    BalanceUpdateIndexingContext,
    OperationWithResultIndexingContext,
} from '../../src/client-indexers/block-data-indexer';
import {
    BalanceUpdate,
    LazyBigMapDiff,
    LazyContract,
    LazyStorageChange,
    LazyTransactionParameter,
    OperationKind,
    OperationResultStatus,
    OperationWithResult,
} from '../../src/rpc-data/rpc-data-interfaces';

describe(OperationWithResultTraverser.name, () => {
    let target: OperationWithResultTraverser<DbContext>;
    let contractTraverser: ContractTraverser<DbContext>;
    let balanceUpdateTraverser: BalanceUpdateTraverser<DbContext>;

    let indexerModule: MockedIndexerModule;
    let db: DbContext;
    let bigMapDiffs: readonly LazyBigMapDiff[];
    let balanceUpdates: readonly BalanceUpdate[];
    let contract: LazyContract;
    let transactionParameter: LazyTransactionParameter;
    let storageChange: LazyStorageChange;
    let indexingContext: OperationWithResultIndexingContext<ContextData>;

    const act = async (op: OperationWithResult) => target.traverse(indexerModule, db, op, indexingContext);

    beforeEach(() => {
        [contractTraverser, balanceUpdateTraverser] = [mock(), mock()];
        target = new OperationWithResultTraverser(instance(contractTraverser), instance(balanceUpdateTraverser));

        indexerModule = mockIndexerModule();
        db = 'mockedDb' as any;
        bigMapDiffs = 'bigMapDiffsMock' as any;
        balanceUpdates = 'balanceUpdatesMock' as any;
        contract = { address: 'contractMock', type: 'Contract' } as typeof contract;
        transactionParameter = 'mockedTransactionParameter' as any;
        storageChange = 'storageChangeMock' as any;
        indexingContext = {
            data: { ctxVal: 'abc' },
            mainOperation: 'mockedMainOp' as any,
        } as typeof indexingContext;
    });

    const allKindsWithResult = Object.keys(typed<Record<OperationWithResult['kind'], ''>>({
        Delegation: '',
        Event: '',
        IncreasePaidStorage: '',
        Origination: '',
        RegisterGlobalConstant: '',
        Reveal: '',
        SetDepositsLimit: '',
        SmartRollupAddMessages: '',
        SmartRollupCement: '',
        SmartRollupExecuteOutboxMessage: '',
        SmartRollupOriginate: '',
        SmartRollupPublish: '',
        SmartRollupRecoverBond: '',
        SmartRollupRefute: '',
        SmartRollupTimeout: '',
        Transaction: '',
        TransferTicket: '',
        UpdateConsensusKey: '',
    })).map(k => k as OperationKind);

    describe(OperationResultStatus.Applied, () => {
        const kindsWithNothingDeeper = [
            OperationKind.Delegation,
            OperationKind.Event,
            OperationKind.Reveal,
            OperationKind.SetDepositsLimit,
            OperationKind.SmartRollupAddMessages,
            OperationKind.SmartRollupCement,
            OperationKind.UpdateConsensusKey,
        ] as const;
        it.each(kindsWithNothingDeeper)('should only index operation if %s', async kind => {
            const operation = { kind, result: { status: OperationResultStatus.Applied } } as OperationWithResult;

            await act(operation);

            verifyOperationWithResultIndexed(operation);
            verifyNoContractTraversed();
            verifyNoBalanceUpdatesTraversed();
        });

        const kindsWithBalanceUpdatesInResult = [
            OperationKind.IncreasePaidStorage,
            OperationKind.RegisterGlobalConstant,
            OperationKind.SmartRollupExecuteOutboxMessage,
            OperationKind.SmartRollupOriginate,
            OperationKind.SmartRollupPublish,
            OperationKind.SmartRollupRecoverBond,
            OperationKind.SmartRollupRefute,
            OperationKind.SmartRollupTimeout,
            OperationKind.TransferTicket,
        ];
        it.each(kindsWithBalanceUpdatesInResult)(`should index operation, traverse balance updates if %s`, async kind => {
            const operation = {
                kind,
                isInternal: false,
                result: { status: OperationResultStatus.Applied, balanceUpdates },
            } as OperationWithResult;

            await act(operation);

            verifyOperationWithResultIndexed(operation);
            verifyNoContractTraversed();
            verifyBalanceUpdatesTraversed(operation);
        });

        it.each([true, false])(`should index operation, traverse contract and balance updates`
            + ` if ${OperationKind.Origination} and isInternal %s`, async isInternal => {
            const operation = {
                kind: OperationKind.Origination,
                isInternal,
                result: {
                    status: OperationResultStatus.Applied,
                    balanceUpdates,
                    bigMapDiffs,
                    originatedContract: contract,
                },
            } as OperationWithResult;

            await act(operation);

            verifyOperationWithResultIndexed(operation);
            verify(contractTraverser.traverse(indexerModule, db, deepEqual({
                ...indexingContext,
                contract,
                bigMapDiffs,
                transactionParameter: null,
                storageChange: null,
                operationWithResult: operation as any,
            }))).once();
            verifyBalanceUpdatesTraversed(operation);
        });

        it.each([true, false])(`should index operation with result, traverse contract and balance updates`
            + ` if ${OperationKind.Transaction} and isInternal %s`, async isInternal => {
            const operation = {
                kind: OperationKind.Transaction,
                isInternal,
                destination: contract,
                transactionParameter,
                result: {
                    status: OperationResultStatus.Applied,
                    balanceUpdates,
                    bigMapDiffs,
                    storageChange,
                },
            } as OperationWithResult;

            await act(operation);

            verifyOperationWithResultIndexed(operation);
            verify(contractTraverser.traverse(indexerModule, db, deepEqual({
                ...indexingContext,
                contract,
                bigMapDiffs,
                transactionParameter,
                storageChange,
                operationWithResult: operation as any,
            }))).once();
            verifyBalanceUpdatesTraversed(operation);
        });

        it('should test all operation kinds', () => {
            const testedKinds = [
                ...kindsWithNothingDeeper,
                ...kindsWithBalanceUpdatesInResult,
                OperationKind.Origination,
                OperationKind.Transaction,
            ];
            expect(testedKinds.sort()).toEqual(allKindsWithResult.sort());
        });
    });

    describe.each([
        OperationResultStatus.Backtracked,
        OperationResultStatus.Failed,
        OperationResultStatus.Skipped,
    ] as const)('%s', status => {
        it.each(allKindsWithResult)('should only index operation if %s', async kind => {
            const operation = { kind, result: { status } } as OperationWithResult;

            await act(operation);

            verifyOperationWithResultIndexed(operation);
            verifyNoContractTraversed();
            verifyNoBalanceUpdatesTraversed();
        });
    });

    function verifyOperationWithResultIndexed(operation: OperationWithResult) {
        verify(indexerModule.mocks.blockDataIndexer.indexOperationWithResult(operation, db, indexingContext)).once();
    }

    function verifyBalanceUpdatesTraversed(operation: OperationWithResult) {
        const receivedContext = capture<any, any, any, any>(balanceUpdateTraverser.traverse).first()[3];
        expect(receivedContext).toEqual<BalanceUpdateIndexingContext>(operation.isInternal
            ? {
                ...indexingContext,
                balanceUpdateSource: 'InternalOperationAppliedResult',
                internalOperation: operation as any,
            }
            : {
                ...indexingContext,
                balanceUpdateSource: 'MainOperationAppliedResult',
                mainOperation: operation as any,
                internalOperation: null,
            });
        expect(receivedContext).toBeFrozen();

        verify(balanceUpdateTraverser.traverse(anything(), anything(), anything(), anything())).once();
        verify(balanceUpdateTraverser.traverse(indexerModule, db, balanceUpdates, receivedContext)).once();
    }

    const verifyNoBalanceUpdatesTraversed = () => verify(balanceUpdateTraverser.traverse(anything(), anything(), anything(), anything())).never();

    const verifyNoContractTraversed = () => verify(contractTraverser.traverse(anything(), anything(), anything())).never();
});
