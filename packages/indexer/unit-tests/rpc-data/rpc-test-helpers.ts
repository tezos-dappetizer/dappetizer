import {
    BaseOperationResultConverter,
} from '../../src/rpc-data/operations/common/operation-result/base-operation-result-converter';
import { TaquitoRpcInternalOperation } from '../../src/rpc-data/operations/common/operation-result/operation-results';
import { PrimitiveConverter } from '../../src/rpc-data/primitives/primitive-converter';
import { RpcConvertOptions } from '../../src/rpc-data/rpc-convert-options';
import { RpcConverter, RpcObjectConverter } from '../../src/rpc-data/rpc-converter';
import { RpcSkippableArrayConverter } from '../../src/rpc-data/rpc-skippable-array-converter';

export function convertValue<TRpc, TTarget, TOptions>(
    converter: RpcConverter<TRpc, TTarget, TOptions>,
    rpcData: TRpc,
    uid: string,
    options: TOptions = 'ignoredOptions' as any,
): TTarget {
    return (converter as any).convertValue(rpcData, uid, options);
}

export function convertObject<TRpc extends object, TTarget, TOptions>(
    converter: RpcObjectConverter<TRpc, TTarget, TOptions> | RpcSkippableArrayConverter<TRpc, TTarget, TOptions>,
    rpcData: TRpc,
    uid: string,
    options: TOptions = 'ignoredOptions' as any,
): TTarget {
    return (converter as any).convertObject(rpcData, uid, options);
}

export function convertPrimitive<TRpc, TTarget, TOptions>(
    converter: PrimitiveConverter<TRpc, TTarget, TOptions>,
    rpcData: unknown,
    options: TOptions = 'ignoredOptions' as any,
): TTarget {
    return (converter as any).convertPrimitive(rpcData, options);
}

export function getTypeDescription<TOptions>(
    converter: PrimitiveConverter<unknown, unknown, TOptions>,
    options: TOptions = 'ignoredOptions' as any,
): string {
    return (converter as any).getTypeDescription(options);
}

export function convertAppliedResultParts<TRpc extends TaquitoRpcInternalOperation['result'], TransactionResultParts, TOptions>(
    converter: BaseOperationResultConverter<TRpc, TransactionResultParts, TOptions>,
    rpcData: TRpc,
    uid: string,
    options: TOptions = 'ignoredOptions' as any,
): TransactionResultParts {
    return (converter as any).convertAppliedResultParts(rpcData, uid, options);
}

export function mockRpcConvertOptions(): RpcConvertOptions {
    return {
        blockHash: 'haha',
        predecessorBlockHash: 'pred-haha',
        lastStorageWithinBlock: new Map([['mockedStorageAddr', { prim: 'mocked' }]]),
    };
}
