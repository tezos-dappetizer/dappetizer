import { anything, deepEqual, spy, verify, when } from 'ts-mockito';

import { describeMember, expectToThrow, TestLogger } from '../../../../test-utilities/mocks';
import { Nullish } from '../../../utils/src/public-api';
import { RpcConversionError } from '../../src/rpc-data/rpc-conversion-error';
import { RpcSkippableArrayConverter, RpcSkippableConversionError } from '../../src/rpc-data/rpc-skippable-array-converter';

interface ConvertOptions { aaa: string }
interface Foo { val: number }

class TargetConverter extends RpcSkippableArrayConverter<Foo, string, ConvertOptions> {
    override convertObject(_rpcData: Foo, _uid: string, _options: ConvertOptions): string {
        throw new Error('Should be mocked.');
    }
}

describe(RpcSkippableArrayConverter.name, () => {
    let target: TargetConverter;
    let logger: TestLogger;
    let targetSpy: TargetConverter;
    let options: ConvertOptions;

    beforeEach(() => {
        logger = new TestLogger();
        target = new TargetConverter(logger);
        targetSpy = spy(target);
        options = { aaa: 'bbb' };
    });

    describeMember<typeof target>('convertArray', () => {
        const act = (v: Nullish<Foo[]>) => target.convertArray(v, 'arr/id', options);

        it('should convert items in the array, skipping failed', () => {
            const rpcError = new RpcSkippableConversionError('deepPath', 'oups');
            whenConvertItem({ val: 11 }, 'arr/id/0').thenReturn('aa');
            whenConvertItem({ val: 22 }, 'arr/id/1').thenThrow(rpcError);
            whenConvertItem({ val: 33 }, 'arr/id/2').thenReturn('cc');

            const items = act([{ val: 11 }, { val: 22 }, { val: 33 }]);

            expect(items).toEqual(['aa', 'cc']);
            expect(items).toBeFrozen();
            logger.loggedSingle()
                .verify('Debug', { itemUid: 'arr/id/1' })
                .verifyMessage('not supported');
        });

        it.each([null, undefined])('should return empty array if %s input', input => {
            const items = act(input);

            expect(items).toBeEmpty();
            expect(items).toBeFrozen();
            logger.verifyNothingLogged();
        });

        it('should throw if not an array', () => {
            runThrowTest({
                input: 'wtf' as any,
                expectedUid: 'arr/id',
                expectedError: ['must be an array', `is 'string'`],
            });
        });

        it('should throw if array item is not object', () => {
            runThrowTest({
                input: ['wtf' as any],
                expectedUid: 'arr/id/0',
                expectedError: ['must be an object', `is 'string'`],
            });
        });

        it('should throw if RPC error', () => {
            whenConvertItem({ val: 11 }, 'arr/id/0').thenThrow(new RpcConversionError('deep/id', 'oups'));

            runThrowTest({
                input: [{ val: 11 }],
                expectedUid: 'deep/id',
                expectedError: ['oups'],
            });
        });

        it('should throw if other error', () => {
            whenConvertItem({ val: 11 }, 'arr/id/0').thenThrow(new Error('oups'));

            runThrowTest({
                input: [{ val: 11 }],
                expectedUid: 'arr/id/0',
                expectedError: ['oups'],
            });
        });

        function whenConvertItem(item: Foo, uid: string) {
            return when(targetSpy.convertObject(deepEqual(item), uid, options));
        }

        function runThrowTest(testCase: { input: Nullish<Foo[]>; expectedUid: string; expectedError: string[] }) {
            const error = expectToThrow(() => act(testCase.input), RpcConversionError);

            expect(error.dataDescription).toIncludeMultiple(testCase.expectedError);
            expect(error.uid).toBe(testCase.expectedUid);
            logger.verifyNothingLogged();
        }
    });

    describeMember<typeof target>('convertArrayProperty', () => {
        it('should get property value and convert it', () => {
            const rpcParentData = { arrayProp: [{ val: 66 }, { val: 77 }] };
            const convertedItems = ['aa', 'bb'];
            when(targetSpy.convertArray(anything(), anything(), anything())).thenReturn(convertedItems);

            const items = target.convertArrayProperty(rpcParentData, 'arrayProp', 'parent/id', options);

            expect(items).toBe(convertedItems);
            verify(targetSpy.convertArray(rpcParentData.arrayProp, 'parent/id/arrayProp', options)).once();
        });
    });
});
