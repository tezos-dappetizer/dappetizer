import { expectToThrow } from '../../../../../test-utilities/mocks';
import { BooleanConverter } from '../../../src/rpc-data/primitives/boolean-converter';
import { convertPrimitive } from '../rpc-test-helpers';

describe(BooleanConverter.name, () => {
    const target = new BooleanConverter();

    const act = (d: unknown) => convertPrimitive(target, d);

    it.each([true, false])('should pass %s', value => {
        expect(act(value)).toBe(value);
    });

    it.each([undefined, null, 123])('should throw if value %s', rpcData => {
        const error = expectToThrow(() => act(rpcData));

        expect(error.message).toContain('NOT a boolean');
    });
});
