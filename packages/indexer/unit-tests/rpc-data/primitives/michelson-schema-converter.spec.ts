import { MichelsonSchema } from '../../../src/michelson/michelson-schema';
import { MichelsonSchemaConverter } from '../../../src/rpc-data/primitives/michelson-schema-converter';
import { Michelson } from '../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../rpc-test-helpers';

describe(MichelsonSchemaConverter.name, () => {
    const target = new MichelsonSchemaConverter();

    it('should convert schema correctly', () => {
        const rpcData: Michelson = { prim: 'string' };

        const schema = convertObject(target, rpcData, 'val/id');

        expect(schema).toBeInstanceOf(MichelsonSchema);
        expect(schema.michelson).toEqual(rpcData);
    });
});
