import { instance, mock, when } from 'ts-mockito';

import { StringLimits, StringValidator } from '../../../../utils/src/public-api';
import { StringConverter } from '../../../src/rpc-data/primitives/string-converter';
import { convertPrimitive, getTypeDescription } from '../rpc-test-helpers';

describe(StringConverter.name, () => {
    let target: StringConverter;
    let limits: StringLimits;
    let validator: StringValidator;

    beforeEach(() => {
        limits = 'mockedStringLimits' as any;
        validator = mock();
        target = new StringConverter(limits, instance(validator));
    });

    describe(getTypeDescription.name, () => {
        it('should be delegated to validator', () => {
            when(validator.getExpectedValueDescription(limits)).thenReturn('foo bar');

            expect(getTypeDescription(target)).toBe('foo bar');
        });
    });

    describe(convertPrimitive.name, () => {
        it('should be delegated to validator', () => {
            when(validator.validate('rpc', limits)).thenReturn('str');

            expect(convertPrimitive(target, 'rpc')).toBe('str');
        });
    });
});
