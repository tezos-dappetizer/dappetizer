import { anything, spy, verify, when } from 'ts-mockito';

import { describeMemberFactory, expectToThrow } from '../../../../../test-utilities/mocks';
import { EnumConverter } from '../../../src/rpc-data/primitives/enum-converter';
import { RpcConversionError } from '../../../src/rpc-data/rpc-conversion-error';
import { RpcSkippableConversionError } from '../../../src/rpc-data/rpc-skippable-array-converter';

enum Foo {
    First = 'First',
    Second = 'Second',
}

describe(EnumConverter.name, () => {
    let target: EnumConverter;
    let targetSpy: EnumConverter;
    let mapping: Readonly<Record<string, Foo>>;

    beforeEach(() => {
        target = new EnumConverter();
        targetSpy = spy(target);

        mapping = {
            aa: Foo.First,
            bb: Foo.Second,
        };
    });

    const describeMember = describeMemberFactory<typeof target>();

    describeMember('convertNullableProperty', () => {
        it('should convert non-nullish value', () => {
            const rpcParentData = { testProp: 'abc' };
            when(targetSpy.convertProperty(rpcParentData, 'testProp', 'val/id', mapping)).thenReturn(Foo.First);

            const result = target.convertNullableProperty(rpcParentData, 'testProp', 'val/id', mapping);

            expect(result).toBe(Foo.First);
        });

        it.each([null, undefined])('should return null without conversion if %s input', testProp => {
            const rpcParentData = { testProp };

            const result = target.convertNullableProperty(rpcParentData, 'testProp', 'val/id', mapping);

            expect(result).toBeNull();
            verify(targetSpy.convertProperty(anything(), anything(), anything(), anything())).never();
        });
    });

    describeMember('convertProperty', () => {
        it.each([
            ['aa', Foo.First],
            ['bb', Foo.Second],
        ])('should map value %s to target enum %s', (input, expected) => {
            const result = target.convertProperty({ input }, 'input', 'val/id', mapping);

            expect(result).toBe(expected);
        });

        it.each([
            ['undefined', undefined, RpcConversionError],
            ['null', null, RpcConversionError],
            ['empty', '', RpcConversionError],
            ['white-space', '  \t', RpcConversionError],
            ['not a string', 123, RpcConversionError],
            ['unsupported', 'cc', RpcSkippableConversionError],
        ])('should throw if %s', (_desc, testProp: any, expectedError) => {
            const rpcData = { testProp, other: 'abc' };

            const error = expectToThrow(() => target.convertProperty(rpcData, 'testProp', 'val/id', mapping), expectedError);

            expect(error.uid).toBe('val/id/testProp');
            expect(error.dataDescription).toIncludeMultiple([
                JSON.stringify(testProp),
                `supported values: ['aa', 'bb']`,
            ]);
        });
    });
});
