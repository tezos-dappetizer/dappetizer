import { expectToThrow } from '../../../../../test-utilities/mocks';
import { MichelsonConverter } from '../../../src/rpc-data/primitives/michelson-converter';
import { RpcConversionError } from '../../../src/rpc-data/rpc-conversion-error';
import { Michelson } from '../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../rpc-test-helpers';

describe(MichelsonConverter.name, () => {
    const target = new MichelsonConverter();

    const act = (d: Michelson) => convertObject(target, d, 'val/id');

    describe('raw value', () => {
        describe('int', () => {
            shouldPass('non-empty str', { int: '123' });

            shouldThrowIfNotString(str => ({ int: str }), 'val/id/int');
        });

        describe('string', () => {
            shouldPass('non-empty str', { string: 'abc' });

            shouldThrowIfNotString(str => ({ string: str }), 'val/id/string');
        });

        describe('bytes', () => {
            shouldPass('non-empty str', { bytes: '0000d515389874ca41fb8ee003637666fe030abb3fae' });

            shouldThrowIfNotString(str => ({ bytes: str }), 'val/id/bytes');
        });

        function shouldThrowIfNotString(createMichelson: (s: string) => Michelson, expectedUid: string) {
            shouldThrowIf('not a valid string', {
                input: createMichelson(123 as any),
                expectedUid,
                expectedError: ['must be a string'],
            });
        }
    });

    describe('prim', () => {
        shouldPass('empty prim', { prim: 'WTF' });
        shouldPass('prim with annots and args', { prim: 'LOL', annots: ['%haha'], args: [{ string: 'abc' }] });

        shouldThrowIf('args not array', {
            input: { prim: 'OMG', args: 'WTF' as any },
            expectedUid: 'val/id/args',
            expectedError: ['must be an array', `type is 'string'`],
        });

        shouldThrowIf('invalid michelson in args', {
            input: { prim: 'OMG', args: [getInvalidMichelson()] },
            expectedUid: 'val/id/args/0',
            expectedError: ['Invalid Michelson', JSON.stringify(getInvalidMichelson())],
        });

        shouldThrowIf('annots not array', {
            input: { prim: 'OMG', annots: 'WTF' as any },
            expectedUid: 'val/id/annots',
            expectedError: ['must be an array', `type is 'string'`],
        });

        shouldThrowIf('invalid string in annots', {
            input: { prim: 'OMG', annots: ['abc', 123 as any] },
            expectedUid: 'val/id/annots/1',
            expectedError: ['must be a string'],
        });
    });

    describe('array of michelsons', () => {
        shouldPass('empty array', []);
        shouldPass('valid michelsons', [{ int: '123' }]);

        shouldThrowIf('invalid michelson in the array', {
            input: [{ int: '123' }, getInvalidMichelson()],
            expectedUid: 'val/id/1',
            expectedError: ['Invalid Michelson', JSON.stringify(getInvalidMichelson())],
        });
    });

    it('should throw if invalid michelson', () => {
        const error = expectToThrow(() => act(getInvalidMichelson()));

        expect(error.message).toIncludeMultiple(['Invalid Michelson', JSON.stringify(getInvalidMichelson())]);
    });

    function shouldPass(desc: string, michelson: Michelson) {
        it(`should pass if ${desc}`, () => {
            expect(act(michelson)).toBe(michelson);
        });
    }

    function shouldThrowIf(desc: string, testCase: { input: Michelson; expectedUid: string; expectedError: string[] }) {
        it(`should throw if ${desc}`, () => {
            const error = expectToThrow(() => act(testCase.input), RpcConversionError);

            expect(error.uid).toBe(testCase.expectedUid);
            expect(error.dataDescription).toIncludeMultiple(testCase.expectedError);
        });
    }

    function getInvalidMichelson() {
        return { val: 'WTF' } as Michelson;
    }
});
