import { deepEqual, instance, mock, when } from 'ts-mockito';

import { AddressValidator } from '../../../../utils/src/public-api';
import { AddressConverter } from '../../../src/rpc-data/primitives/address-converter';
import { convertPrimitive, getTypeDescription } from '../rpc-test-helpers';

describe(AddressConverter.name, () => {
    let target: AddressConverter;
    let validator: AddressValidator;

    beforeEach(() => {
        validator = mock();
        target = new AddressConverter(instance(validator));
    });

    describe(getTypeDescription.name, () => {
        it('should be delegated to validator', () => {
            when(validator.getExpectedValueDescription(deepEqual(['KT1', 'tz2']))).thenReturn('foo bar');

            expect(getTypeDescription(target, ['KT1', 'tz2'])).toBe('foo bar');
        });
    });

    describe(convertPrimitive.name, () => {
        it('should be delegated to validator', () => {
            when(validator.validate('rpc', deepEqual(['KT1', 'tz2']))).thenReturn('addr');

            expect(convertPrimitive(target, 'rpc', ['KT1', 'tz2'])).toBe('addr');
        });
    });
});
