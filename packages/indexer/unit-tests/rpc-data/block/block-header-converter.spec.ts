import { BlockHeader, TaquitoRpcBlockHeader } from '../../../src/rpc-data/block/block-header';
import { BlockHeaderConverter } from '../../../src/rpc-data/block/block-header-converter';
import { convertObject } from '../rpc-test-helpers';

describe(BlockHeaderConverter.name, () => {
    let target: BlockHeaderConverter;

    let rpcData: TaquitoRpcBlockHeader;

    const act = () => convertObject(target, rpcData, 'blk/id');

    beforeEach(() => {
        target = new BlockHeaderConverter();

        rpcData = {
            level: 1532061,
            proto: 9,
            predecessor: 'BLnGWWitVvNfsDhLmhnJVreiMqxbDHNWFxowd4EWEZP6dhajniu',
            timestamp: '2021-06-26T12:59:46Z',
            validation_pass: 4,
            operations_hash: 'LLoaYbeWgdwo6Eg223gxKLp8XBVnaaLBJjTbiRSxnPfpdwbW6iYmP',
            fitness: ['01', '00000000000d609d'],
            context: 'CoV5doQKE2x4Lws3dDcjy95ZU4CdHAEGvAryoXX3jsA5hm45TZgh',
            seed_nonce_hash: 'abc',
            priority: 0,
            proof_of_work_nonce: 'bc2cc86f58360000',
            signature: 'siggm3EHxNrY4A8SPRHD5zmKvv6QoWDRMyRV7yjjqP2cZozcWjdQtmH91qbHd4HXGhS79gYExcZuecfUzw7ocMVwjqs2M6sq',
        };
    });

    it('should convert correctly', () => {
        const header = act();

        expect(header).toEqual<BlockHeader>({
            rpcData,
            level: 1532061,
            proto: 9,
            predecessorBlockHash: 'BLnGWWitVvNfsDhLmhnJVreiMqxbDHNWFxowd4EWEZP6dhajniu',
            timestamp: new Date('2021-06-26T12:59:46Z'),
            uid: 'blk/id',
        });
        expect(header).toBeFrozen();
    });
});
