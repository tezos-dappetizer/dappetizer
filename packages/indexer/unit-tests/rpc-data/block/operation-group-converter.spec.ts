import { Writable } from 'ts-essentials';
import { anything, deepEqual, instance, mock, when } from 'ts-mockito';

import { expectToThrow, TestLogger } from '../../../../../test-utilities/mocks';
import { asReadonly } from '../../../../utils/src/public-api';
import { scopedLogKeys } from '../../../src/helpers/scoped-log-keys';
import { OperationGroupConverter } from '../../../src/rpc-data/block/operation-group-converter';
import { MainOperationConverter } from '../../../src/rpc-data/operations/main-operation-converter';
import { RpcConversionError } from '../../../src/rpc-data/rpc-conversion-error';
import { RpcConvertOptions } from '../../../src/rpc-data/rpc-convert-options';
import {
    MainOperation,
    OperationGroup,
    TaquitoRpcOperation,
    TaquitoRpcOperationGroup,
} from '../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../rpc-test-helpers';

describe(OperationGroupConverter.name, () => {
    let target: OperationGroupConverter;
    let operationConverter: MainOperationConverter;
    let logger: TestLogger;

    let rpcData: Writable<TaquitoRpcOperationGroup>;
    let options: RpcConvertOptions;
    let operations: MainOperation[];

    const act = () => convertObject(target, rpcData, 'generic/id', options);

    beforeEach(() => {
        operationConverter = mock();
        logger = new TestLogger();
        target = new OperationGroupConverter(instance(operationConverter), logger);

        const rpcOp1 = { metadata: {} } as TaquitoRpcOperation;
        const rpcOp2 = { metadata: {} } as TaquitoRpcOperation;
        rpcData = {
            protocol: 'PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV',
            chain_id: 'NetXdQprcVkpaWU',
            hash: 'opBjFU4gtbf4iNK5VEXXh4aMsFL5TXZSBdaGiz8V9dbhn1HMTwy',
            branch: 'BLgDhpjbnNgtjsfDf7wTKiHT1Uj8AyK69umAHeVXMbKh6P2pfgT',
            signature: 'sigb5a1WUhfDBfHUHePJhv5ZZbZmQm6rpCDx3taSHMfUJFRzFEumihKj5x88QCYzyfkfXz3tRMzfPxFCcSQQBBJTUHELPFtd',
            contents: asReadonly([rpcOp1, rpcOp2]),
        } as typeof rpcData;
        options = { blockHash: 'haha' } as typeof options;
        operations = 'mockedOperations' as any;

        when(operationConverter.convertArray(
            deepEqual([rpcOp1, rpcOp2]),
            'haha/opBjFU4gtbf4iNK5VEXXh4aMsFL5TXZSBdaGiz8V9dbhn1HMTwy',
            options,
        )).thenReturn(operations);
    });

    it('should convert correctly', () => {
        const group = act();

        expect(group).toEqual<OperationGroup>({
            rpcData,
            protocol: 'PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV',
            chainId: 'NetXdQprcVkpaWU',
            hash: 'opBjFU4gtbf4iNK5VEXXh4aMsFL5TXZSBdaGiz8V9dbhn1HMTwy',
            branch: 'BLgDhpjbnNgtjsfDf7wTKiHT1Uj8AyK69umAHeVXMbKh6P2pfgT',
            signature: 'sigb5a1WUhfDBfHUHePJhv5ZZbZmQm6rpCDx3taSHMfUJFRzFEumihKj5x88QCYzyfkfXz3tRMzfPxFCcSQQBBJTUHELPFtd',
            operations,
            uid: 'haha/opBjFU4gtbf4iNK5VEXXh4aMsFL5TXZSBdaGiz8V9dbhn1HMTwy',
        });
    });

    it('should throw if no metadata', () => {
        rpcData.contents = [
            { metadata: {} } as TaquitoRpcOperation,
            {} as TaquitoRpcOperation,
        ];

        const error = expectToThrow(act, RpcConversionError);

        expect(error.uid).toBe('haha/opBjFU4gtbf4iNK5VEXXh4aMsFL5TXZSBdaGiz8V9dbhn1HMTwy/contents/1/metadata');
    });

    it('should put hash to log scope', () => {
        when(operationConverter.convertArray(anything(), anything(), anything())).thenCall(() => {
            logger.logDebug('OPERATIONS');
            return operations;
        });

        act();

        const expectedData = { [scopedLogKeys.INDEXED_OPERATION_GROUP_HASH]: 'opBjFU4gtbf4iNK5VEXXh4aMsFL5TXZSBdaGiz8V9dbhn1HMTwy' };
        logger.loggedSingle().verify('Debug', expectedData).verifyMessage('OPERATIONS');
    });
});
