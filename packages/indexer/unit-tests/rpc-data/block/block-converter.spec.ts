import { anything, capture, instance, mock, verify, when } from 'ts-mockito';

import { asReadonly } from '../../../../utils/src/public-api';
import { Block, TaquitoRpcBlock } from '../../../src/rpc-data/block/block';
import { BlockConverter } from '../../../src/rpc-data/block/block-converter';
import { BlockHeader } from '../../../src/rpc-data/block/block-header';
import { BlockHeaderConverter } from '../../../src/rpc-data/block/block-header-converter';
import { OperationGroup } from '../../../src/rpc-data/block/operation-group';
import { OperationGroupConverter } from '../../../src/rpc-data/block/operation-group-converter';
import { BalanceUpdateConverter } from '../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import { BalanceUpdate, TaquitoRpcOperationGroup } from '../../../src/rpc-data/rpc-data-interfaces';

describe(BlockConverter, () => {
    let target: BlockConverter;
    let blockHeaderConverter: BlockHeaderConverter;
    let operationGroupConverter: OperationGroupConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;

    let rpcData: TaquitoRpcBlock;
    let rpcGroup1: TaquitoRpcOperationGroup;
    let rpcGroup2: TaquitoRpcOperationGroup;
    let rpcGroup3: TaquitoRpcOperationGroup;
    let header: BlockHeader;
    let operationGroup1: OperationGroup;
    let operationGroup2: OperationGroup;
    let operationGroup3: OperationGroup;
    let metadataBalanceUpdates: BalanceUpdate[];

    const act = () => target.convert(rpcData);

    beforeEach(() => {
        [blockHeaderConverter, operationGroupConverter, balanceUpdateConverter] = [mock(), mock(), mock()];
        target = new BlockConverter(instance(blockHeaderConverter), instance(operationGroupConverter), instance(balanceUpdateConverter));

        rpcGroup1 = 'mockedRpcGroup1' as any;
        rpcGroup2 = 'mockedRpcGroup2' as any;
        rpcGroup3 = 'mockedRpcGroup3' as any;
        rpcData = {
            chain_id: 'NetXdQprcVkpaWU',
            hash: 'haha',
            protocol: 'PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV',
            operations: asReadonly([
                asReadonly([rpcGroup1, rpcGroup2]),
                asReadonly([rpcGroup3]),
            ]),
        } as TaquitoRpcBlock;
        header = {
            level: 123,
            predecessorBlockHash: 'pred-haha',
            proto: 66,
            rpcData: 'ignored' as any,
            timestamp: new Date(),
            uid: 'header/id',
        };
        operationGroup1 = { hash: 'op1' } as OperationGroup;
        operationGroup2 = { hash: 'op2' } as OperationGroup;
        operationGroup3 = { hash: 'op3' } as OperationGroup;
        metadataBalanceUpdates = 'mockedBalanceUpdates' as any;

        when(blockHeaderConverter.convertProperty(rpcData, 'header', 'haha'))
            .thenReturn(header);
        when(operationGroupConverter.convertArray(anything(), anything(), anything()))
            .thenReturn([operationGroup1, operationGroup2])
            .thenReturn([operationGroup3]);
        when(balanceUpdateConverter.convertFromMetadataProperty(rpcData, 'haha'))
            .thenReturn(metadataBalanceUpdates);
    });

    it('should convert correctly', () => {
        const block = act();

        expect(block).toEqual<Block>({
            chainId: 'NetXdQprcVkpaWU',
            metadataBalanceUpdates,
            hash: 'haha',
            level: header.level,
            operationGroups: [operationGroup1, operationGroup2, operationGroup3],
            predecessorBlockHash: header.predecessorBlockHash,
            proto: header.proto,
            protocol: 'PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV',
            rpcData,
            timestamp: header.timestamp,
            uid: 'haha',
        });
        verify(operationGroupConverter.convertArray(anything(), anything(), anything())).times(2);
        const calls = capture(operationGroupConverter.convertArray);
        expect(calls.first()).toEqual([
            [rpcGroup1, rpcGroup2],
            'haha/operations/0',
            { blockHash: 'haha', predecessorBlockHash: 'pred-haha', lastStorageWithinBlock: new Map() },
        ]);
        expect(calls.last()).toEqual([
            [rpcGroup3],
            'haha/operations/1',
            { blockHash: 'haha', predecessorBlockHash: 'pred-haha', lastStorageWithinBlock: new Map() },
        ]);
        expect(calls.first()[2].lastStorageWithinBlock).toBe(calls.last()[2].lastStorageWithinBlock);
    });
});
