import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { anything, instance, mock, when } from 'ts-mockito';

import { TicketTokenConverter } from '../../../../../src/rpc-data/common/ticket/converters/ticket-token-converter';
import {
    CommonMainOperationWithResult,
    CommonMainOperationWithResultConverter,
} from '../../../../../src/rpc-data/operations/common/main-operation/common-main-operation-with-result-converter';
import {
    TransferTicketOperationConverter,
} from '../../../../../src/rpc-data/operations/transfer-ticket/converters/transfer-ticket-operation-converter';
import {
    TransferTicketResultConverter,
} from '../../../../../src/rpc-data/operations/transfer-ticket/converters/transfer-ticket-result-converter';
import { RpcConvertOptions } from '../../../../../src/rpc-data/rpc-convert-options';
import {
    OperationKind,
    TicketToken,
    TransferTicketOperation,
    TransferTicketResult,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../rpc-test-helpers';
import { setupCommonOpWithResultProperties } from '../../operation-rpc-test-helpers';

describe(TransferTicketOperationConverter.name, () => {
    let target: TransferTicketOperationConverter;
    let commonOperationConverter: CommonMainOperationWithResultConverter;
    let resultConverter: TransferTicketResultConverter;
    let ticketTokenConverter: TicketTokenConverter;

    let rpcData: taquitoRpc.OperationContentsAndResultTransferTicket;
    let options: RpcConvertOptions;
    let commonProperties: CommonMainOperationWithResult;
    let opResult: TransferTicketResult;
    let ticketToken: TicketToken;

    const act = () => convertObject(target, rpcData, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, resultConverter, ticketTokenConverter] = [mock(), mock(), mock()];
        target = new TransferTicketOperationConverter(
            instance(commonOperationConverter),
            instance(resultConverter),
            instance(ticketTokenConverter),
        );

        rpcData = {
            destination: 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5',
            entrypoint: 'ee',
            ticket_amount: '45',
        } as typeof rpcData;
        options = 'mockedOptions' as any;
        opResult = 'mockedResult' as any;
        ticketToken = {
            content: 'mockedTicketContent' as any,
            ticketerAddress: 'sr1Ta2ZGVaDgJg2DRN6wifTvfHm6pWbwdzXj',
        } as TicketToken;

        commonProperties = setupCommonOpWithResultProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(resultConverter.convertFromMetadataOperationResult(rpcData, 'op/id')).thenReturn(opResult);
        when(ticketTokenConverter.convertFromProperties(rpcData, 'op/id', anything())).thenReturn(ticketToken);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<TransferTicketOperation>({
            balanceUpdates: commonProperties.balanceUpdates,
            counter: commonProperties.counter,
            destinationAddress: 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5',
            entrypoint: 'ee',
            fee: commonProperties.fee,
            gasLimit: commonProperties.gasLimit,
            internalOperations: commonProperties.internalOperations,
            isInternal: commonProperties.isInternal,
            kind: OperationKind.TransferTicket,
            rpcData,
            sourceAddress: commonProperties.sourceAddress,
            storageLimit: commonProperties.storageLimit,
            result: opResult,
            ticket: {
                amount: new BigNumber(45),
                contents: ticketToken.content,
                ticketerAddress: 'sr1Ta2ZGVaDgJg2DRN6wifTvfHm6pWbwdzXj',
            },
            ticketAmount: new BigNumber(45),
            ticketToken,
            uid: commonProperties.uid,
        });
        expect(operation).toBeFrozen();
    });
});
