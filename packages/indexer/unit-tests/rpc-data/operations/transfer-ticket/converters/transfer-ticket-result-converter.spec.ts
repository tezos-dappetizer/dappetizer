import { BigNumber } from 'bignumber.js';
import { instance, mock, when } from 'ts-mockito';

import {
    BalanceUpdateConverter,
} from '../../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import { TicketUpdatesConverter } from '../../../../../src/rpc-data/common/ticket/converters/ticket-updates-converter';
import {
    TransferTicketResultConverter,
    TransferTicketResultParts,
} from '../../../../../src/rpc-data/operations/transfer-ticket/converters/transfer-ticket-result-converter';
import {
    BalanceUpdate,
    TaquitoRpcTransferTicketResult,
    TicketUpdate,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertAppliedResultParts } from '../../../rpc-test-helpers';

describe(TransferTicketResultConverter, () => {
    let target: TransferTicketResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;
    let ticketUpdatesConverter: TicketUpdatesConverter;

    let rpcData: TaquitoRpcTransferTicketResult;
    let balanceUpdates: BalanceUpdate[];
    let ticketUpdates: TicketUpdate[];

    const act = () => convertAppliedResultParts(target, rpcData, 'res/id');

    beforeEach(() => {
        [balanceUpdateConverter, ticketUpdatesConverter] = [mock(), mock()];
        target = new TransferTicketResultConverter(null!, instance(balanceUpdateConverter), instance(ticketUpdatesConverter));

        rpcData = { paid_storage_size_diff: '11' } as typeof rpcData;
        balanceUpdates = 'mockedBalanceUpdates' as any;
        ticketUpdates = 'mockedTicketUpdates' as any;

        when(balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', 'res/id')).thenReturn(balanceUpdates);
        when(ticketUpdatesConverter.convertArrayProperty(rpcData, 'ticket_updates', 'res/id')).thenReturn(ticketUpdates);
    });

    it('should convert correctly', () => {
        const result = act();

        expect(result).toEqual<TransferTicketResultParts>({
            balanceUpdates,
            paidStorageSizeDiff: new BigNumber(11),
            ticketUpdates,
        });
    });
});
