import { BallotOperationConverter } from '../../../../src/rpc-data/operations/ballot/ballot-operation-converter';
import {
    Ballot,
    BallotOperation,
    OperationKind,
    TaquitoRpcBallotOperation,
} from '../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../rpc-test-helpers';

describe(BallotOperationConverter.name, () => {
    let target: BallotOperationConverter;

    let rpcData: TaquitoRpcBallotOperation;

    const act = () => convertObject(target, rpcData, 'op/id');

    beforeEach(() => {
        target = new BallotOperationConverter();

        rpcData = {
            source: 'tz1PMWrbSda1RvsRi8Xq7GRd3P7739jtyaM8',
            period: 59,
            proposal: 'PtHangzHogokSuiMHemCuowEavgYTP8J5qQ9fQS793MHYFpCY3r',
            ballot: 'yay',
        } as typeof rpcData;
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<BallotOperation>({
            ballot: Ballot.Yay,
            kind: OperationKind.Ballot,
            period: 59,
            proposal: 'PtHangzHogokSuiMHemCuowEavgYTP8J5qQ9fQS793MHYFpCY3r',
            rpcData,
            sourceAddress: 'tz1PMWrbSda1RvsRi8Xq7GRd3P7739jtyaM8',
            uid: 'op/id',
        });
        expect(operation).toBeFrozen();
    });
});
