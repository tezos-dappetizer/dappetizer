import * as taquitoRpc from '@taquito/rpc';
import { Writable } from 'ts-essentials';
import { instance, mock, when } from 'ts-mockito';

import {
    CommonInternalOperation,
    CommonInternalOperationConverter,
} from '../../../../../src/rpc-data/operations/common/internal-operation/common-internal-operation-converter';
import {
    DelegationInternalOperationConverter,
} from '../../../../../src/rpc-data/operations/delegation/converters/delegation-internal-operation-converter';
import {
    DelegationResultConverter,
} from '../../../../../src/rpc-data/operations/delegation/converters/delegation-result-converter';
import {
    DelegationInternalOperation,
    DelegationResult,
    OperationKind,
    TaquitoRpcInternalOperation,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../rpc-test-helpers';

describe(DelegationInternalOperationConverter.name, () => {
    let target: DelegationInternalOperationConverter;
    let commonInternalOperationConverter: CommonInternalOperationConverter;
    let resultConverter: DelegationResultConverter;

    let rpcData: Writable<TaquitoRpcInternalOperation>;
    let commonProperties: CommonInternalOperation;
    let opResult: DelegationResult;

    const act = () => convertObject(target, rpcData, 'op/id');

    beforeEach(() => {
        [commonInternalOperationConverter, resultConverter] = [mock(), mock()];
        target = new DelegationInternalOperationConverter(instance(commonInternalOperationConverter), instance(resultConverter));

        rpcData = {
            kind: taquitoRpc.OpKind.DELEGATION,
            delegate: 'tz1NGQWcfuY9WfZmPjwjn2Vy3kbAcNLtFqhQ',
        } as typeof rpcData;
        opResult = 'mockedResult' as any;
        commonProperties = {
            isInternal: 'mocked-isInternal' as any,
            nonce: 'mocked-nonce' as any,
            source: 'mocked-source' as any,
            sourceAddress: 'mocked-sourceAddress',
            uid: 'mocked-uid',
        } as typeof commonProperties;

        when(commonInternalOperationConverter.convert(rpcData, 'op/id')).thenReturn(commonProperties);
        when(resultConverter.convertProperty(rpcData, 'result', 'op/id')).thenReturn(opResult);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<DelegationInternalOperation>({
            delegateAddress: 'tz1NGQWcfuY9WfZmPjwjn2Vy3kbAcNLtFqhQ',
            isInternal: commonProperties.isInternal,
            kind: OperationKind.Delegation,
            nonce: commonProperties.nonce,
            result: opResult,
            rpcData: commonProperties.rpcData,
            source: commonProperties.source,
            sourceAddress: commonProperties.sourceAddress, // eslint-disable-line deprecation/deprecation
            sourceContract: commonProperties.sourceContract, // eslint-disable-line deprecation/deprecation
            uid: commonProperties.uid,
        });
        expect(operation).toBeFrozen();
    });

    it('should fail if not delegation', () => {
        rpcData.kind = 'wtf' as any;

        expect(act).toThrow();
    });
});
