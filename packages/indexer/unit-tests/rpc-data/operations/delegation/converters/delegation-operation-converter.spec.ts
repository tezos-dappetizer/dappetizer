import { instance, mock, when } from 'ts-mockito';

import {
    CommonMainOperationWithResult,
    CommonMainOperationWithResultConverter,
} from '../../../../../src/rpc-data/operations/common/main-operation/common-main-operation-with-result-converter';
import {
    DelegationOperationConverter,
} from '../../../../../src/rpc-data/operations/delegation/converters/delegation-operation-converter';
import {
    DelegationResultConverter,
} from '../../../../../src/rpc-data/operations/delegation/converters/delegation-result-converter';
import { RpcConvertOptions } from '../../../../../src/rpc-data/rpc-convert-options';
import {
    DelegationOperation,
    DelegationResult,
    OperationKind,
    TaquitoRpcDelegationOperation,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../rpc-test-helpers';
import { setupCommonOpWithResultProperties } from '../../operation-rpc-test-helpers';

describe(DelegationOperationConverter.name, () => {
    let target: DelegationOperationConverter;
    let commonOperationConverter: CommonMainOperationWithResultConverter;
    let resultConverter: DelegationResultConverter;

    let rpcData: TaquitoRpcDelegationOperation;
    let options: RpcConvertOptions;
    let opResult: DelegationResult;
    let commonProperties: CommonMainOperationWithResult;

    const act = () => convertObject(target, rpcData, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, resultConverter] = [mock(), mock()];
        target = new DelegationOperationConverter(instance(commonOperationConverter), instance(resultConverter));

        rpcData = { delegate: 'tz1NGQWcfuY9WfZmPjwjn2Vy3kbAcNLtFqhQ' } as typeof rpcData;
        options = 'mockedOptions' as any;
        opResult = 'mockedResult' as any;

        commonProperties = setupCommonOpWithResultProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(resultConverter.convertFromMetadataOperationResult(rpcData, 'op/id')).thenReturn(opResult);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<DelegationOperation>({
            balanceUpdates: commonProperties.balanceUpdates,
            counter: commonProperties.counter,
            delegateAddress: 'tz1NGQWcfuY9WfZmPjwjn2Vy3kbAcNLtFqhQ',
            fee: commonProperties.fee,
            gasLimit: commonProperties.gasLimit,
            internalOperations: commonProperties.internalOperations,
            isInternal: commonProperties.isInternal,
            kind: OperationKind.Delegation,
            result: opResult,
            rpcData,
            sourceAddress: commonProperties.sourceAddress,
            storageLimit: commonProperties.storageLimit,
            uid: commonProperties.uid,
        });
        expect(operation).toBeFrozen();
    });
});
