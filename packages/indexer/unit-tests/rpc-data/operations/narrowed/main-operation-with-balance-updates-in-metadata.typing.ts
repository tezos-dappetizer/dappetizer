import { assert, IsExact } from 'conditional-type-checks';

import { MainOperation, MainOperationWithBalanceUpdatesInMetadata } from '../../../../src/rpc-data/rpc-data-interfaces';

// eslint-disable-next-line @typescript-eslint/ban-types
assert<IsExact<MainOperationWithBalanceUpdatesInMetadata, Extract<MainOperation, {
    balanceUpdates: readonly unknown[];
}>>>(true);
