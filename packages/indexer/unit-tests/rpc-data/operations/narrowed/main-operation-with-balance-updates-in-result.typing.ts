import { assert, IsExact } from 'conditional-type-checks';

import { Applied } from '../../../../src/rpc-data/operations/narrowed/applied';
import {
    MainOperationWithBalanceUpdatesInResult,
} from '../../../../src/rpc-data/operations/narrowed/main-operation-with-balance-updates-in-result';
import { MainOperationWithResult } from '../../../../src/rpc-data/operations/narrowed/main-operation-with-result';

// eslint-disable-next-line @typescript-eslint/ban-types
assert<IsExact<MainOperationWithBalanceUpdatesInResult, Extract<Applied<MainOperationWithResult>, {
    result: { balanceUpdates: readonly unknown[] };
}>>>(true);
