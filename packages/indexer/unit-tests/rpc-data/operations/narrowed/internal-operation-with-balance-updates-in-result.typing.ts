import { assert, IsExact } from 'conditional-type-checks';

import { InternalOperation } from '../../../../src/rpc-data/operations/internal-operation';
import { Applied } from '../../../../src/rpc-data/operations/narrowed/applied';
import {
    InternalOperationWithBalanceUpdatesInResult,
} from '../../../../src/rpc-data/operations/narrowed/internal-operation-with-balance-updates-in-result';

// eslint-disable-next-line @typescript-eslint/ban-types
assert<IsExact<InternalOperationWithBalanceUpdatesInResult, Extract<Applied<InternalOperation>, {
    result: { balanceUpdates: readonly unknown[] };
}>>>(true);
