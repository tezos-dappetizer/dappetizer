import { StrictExclude } from 'ts-essentials';

import { MainOperation } from '../../../../src/rpc-data/operations/main-operation';
import {
    isSmartRollupOperation,
    SmartRollupOperation,
} from '../../../../src/rpc-data/operations/narrowed/smart-rollup-operation';

describe(isSmartRollupOperation.name, () => {
    const positiveTestCases: Record<SmartRollupOperation['kind'], true> = {
        SmartRollupAddMessages: true,
        SmartRollupCement: true,
        SmartRollupExecuteOutboxMessage: true,
        SmartRollupOriginate: true,
        SmartRollupPublish: true,
        SmartRollupRecoverBond: true,
        SmartRollupRefute: true,
        SmartRollupTimeout: true,
    };
    const negativeTestCases: Record<StrictExclude<MainOperation['kind'], SmartRollupOperation['kind']>, false> = {
        ActivateAccount: false,
        Ballot: false,
        Delegation: false,
        DoubleBakingEvidence: false,
        DoubleEndorsementEvidence: false,
        DoublePreendorsementEvidence: false,
        DrainDelegate: false,
        Endorsement: false,
        IncreasePaidStorage: false,
        Origination: false,
        Preendorsement: false,
        Proposals: false,
        RegisterGlobalConstant: false,
        Reveal: false,
        SeedNonceRevelation: false,
        SetDepositsLimit: false,
        Transaction: false,
        TransferTicket: false,
        UpdateConsensusKey: false,
        VdfRevelation: false,
    };

    it.each(
        Object.entries({ ...positiveTestCases, ...negativeTestCases }),
    )('should narrow %s to %s', (kind, expectedNarrowing) => {
        const isNarrowed = isSmartRollupOperation({ kind } as MainOperation);

        expect(isNarrowed).toBe(expectedNarrowing);
    });
});
