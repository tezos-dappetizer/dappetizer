import { StrictExclude } from 'ts-essentials';

import {
    isMainOperationWithBalanceUpdatesInMetadata,
    MainOperation,
    MainOperationWithBalanceUpdatesInMetadata,
} from '../../../../src/rpc-data/rpc-data-interfaces';

describe(isMainOperationWithBalanceUpdatesInMetadata.name, () => {
    const positiveTestCases: Record<MainOperationWithBalanceUpdatesInMetadata['kind'], true> = {
        ActivateAccount: true,
        Delegation: true,
        DoubleBakingEvidence: true,
        DoubleEndorsementEvidence: true,
        DoublePreendorsementEvidence: true,
        DrainDelegate: true,
        Endorsement: true,
        IncreasePaidStorage: true,
        Preendorsement: true,
        Origination: true,
        RegisterGlobalConstant: true,
        Reveal: true,
        SeedNonceRevelation: true,
        SetDepositsLimit: true,
        SmartRollupAddMessages: true,
        SmartRollupCement: true,
        SmartRollupExecuteOutboxMessage: true,
        SmartRollupOriginate: true,
        SmartRollupPublish: true,
        SmartRollupRecoverBond: true,
        SmartRollupRefute: true,
        SmartRollupTimeout: true,
        Transaction: true,
        TransferTicket: true,
        UpdateConsensusKey: true,
        VdfRevelation: true,
    };
    const negativeTestCases: Record<StrictExclude<MainOperation['kind'], MainOperationWithBalanceUpdatesInMetadata['kind']>, false> = {
        Ballot: false,
        Proposals: false,
    };

    it.each(
        Object.entries({ ...positiveTestCases, ...negativeTestCases }),
    )('should narrow %s to %s', (kind, expectedNarrowing) => {
        const isNarrowed = isMainOperationWithBalanceUpdatesInMetadata({ kind } as MainOperation);

        expect(isNarrowed).toBe(expectedNarrowing);
    });
});
