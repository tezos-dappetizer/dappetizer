/* eslint-disable @typescript-eslint/ban-types */
import { assert, IsExact } from 'conditional-type-checks';

import { DelegationInternalOperation } from '../../../../src/rpc-data/operations/delegation/delegation-internal-operation';
import { Applied } from '../../../../src/rpc-data/operations/narrowed/applied';
import { AppliedRevealResult } from '../../../../src/rpc-data/operations/reveal/reveal-result';
import { TransactionOperation } from '../../../../src/rpc-data/operations/transaction/transaction-operation';
import { AppliedTransactionResult } from '../../../../src/rpc-data/operations/transaction/transaction-result';

assert<IsExact<Applied<TransactionOperation>, TransactionOperation & { result: AppliedTransactionResult }>>(true);
assert<IsExact<Applied<DelegationInternalOperation>, DelegationInternalOperation & { result: AppliedRevealResult }>>(true);
