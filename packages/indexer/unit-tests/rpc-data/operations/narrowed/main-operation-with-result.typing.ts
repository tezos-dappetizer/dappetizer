import { assert, IsExact } from 'conditional-type-checks';

import { MainOperation } from '../../../../src/rpc-data/operations/main-operation';
import { MainOperationWithResult } from '../../../../src/rpc-data/operations/narrowed/main-operation-with-result';

// eslint-disable-next-line @typescript-eslint/ban-types
assert<IsExact<MainOperationWithResult, Extract<MainOperation, { result: unknown }>>>(true);
