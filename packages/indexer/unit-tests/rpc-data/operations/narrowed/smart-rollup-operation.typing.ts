/* eslint-disable @typescript-eslint/ban-types */
import { assert, IsExact } from 'conditional-type-checks';

import { MainOperation } from '../../../../src/rpc-data/operations/main-operation';
import { SmartRollupOperation } from '../../../../src/rpc-data/operations/narrowed/smart-rollup-operation';
import {
    SmartRollupAddMessagesOperation,
} from '../../../../src/rpc-data/operations/smart-rollup/add-messages/smart-rollup-add-messages-operation';
import {
    SmartRollupOriginateOperation,
} from '../../../../src/rpc-data/operations/smart-rollup/originate/smart-rollup-originate-operation';

type WithRollup = Extract<MainOperation, { rollup: unknown }>;

assert<IsExact<SmartRollupOperation, WithRollup | SmartRollupAddMessagesOperation | SmartRollupOriginateOperation>>(true);
