import { instance, mock, when } from 'ts-mockito';

import { BlockHeaderConverter } from '../../../../src/rpc-data/block/block-header-converter';
import { BalanceUpdateConverter } from '../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import {
    DoubleBakingEvidenceOperationConverter,
} from '../../../../src/rpc-data/operations/double-baking-evidence/double-baking-evidence-operation-converter';
import {
    BalanceUpdate,
    BlockHeader,
    DoubleBakingEvidenceOperation,
    OperationKind,
    TaquitoRpcDoubleBakingEvidenceOperation,
} from '../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../rpc-test-helpers';

describe(DoubleBakingEvidenceOperationConverter.name, () => {
    let target: DoubleBakingEvidenceOperationConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;
    let blockHeaderConverter: BlockHeaderConverter;

    let rpcData: TaquitoRpcDoubleBakingEvidenceOperation;
    let balanceUpdates: BalanceUpdate[];
    let blockHeader1: BlockHeader;
    let blockHeader2: BlockHeader;

    const act = () => convertObject(target, rpcData, 'op/id');

    beforeEach(() => {
        [balanceUpdateConverter, blockHeaderConverter] = [mock(), mock()];
        target = new DoubleBakingEvidenceOperationConverter(instance(balanceUpdateConverter), instance(blockHeaderConverter));

        rpcData = 'mockedRpcData' as any;
        balanceUpdates = 'mockedBalanceUpdates' as any;
        blockHeader1 = 'mockedBlockHeader1' as any;
        blockHeader2 = 'mockedBlockHeader2' as any;

        when(balanceUpdateConverter.convertFromMetadataProperty(rpcData, 'op/id')).thenReturn(balanceUpdates);
        when(blockHeaderConverter.convertProperty(rpcData, 'bh1', 'op/id')).thenReturn(blockHeader1);
        when(blockHeaderConverter.convertProperty(rpcData, 'bh2', 'op/id')).thenReturn(blockHeader2);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<DoubleBakingEvidenceOperation>({
            balanceUpdates,
            blockHeader1,
            blockHeader2,
            kind: OperationKind.DoubleBakingEvidence,
            rpcData,
            uid: 'op/id',
        });
        expect(operation).toBeFrozen();
    });
});
