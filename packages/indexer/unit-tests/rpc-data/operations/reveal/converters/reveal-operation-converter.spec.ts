import * as taquitoRpc from '@taquito/rpc';
import { instance, mock, when } from 'ts-mockito';

import {
    CommonMainOperationWithResult,
    CommonMainOperationWithResultConverter,
} from '../../../../../src/rpc-data/operations/common/main-operation/common-main-operation-with-result-converter';
import {
    RevealOperationConverter,
} from '../../../../../src/rpc-data/operations/reveal/converters/reveal-operation-converter';
import { RevealResultConverter } from '../../../../../src/rpc-data/operations/reveal/converters/reveal-result-converter';
import { RpcConvertOptions } from '../../../../../src/rpc-data/rpc-convert-options';
import { OperationKind, RevealOperation, RevealResult } from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../rpc-test-helpers';
import { setupCommonOpWithResultProperties } from '../../operation-rpc-test-helpers';

describe(RevealOperationConverter.name, () => {
    let target: RevealOperationConverter;
    let commonOperationConverter: CommonMainOperationWithResultConverter;
    let resultConverter: RevealResultConverter;

    let rpcData: taquitoRpc.OperationContentsAndResultReveal;
    let options: RpcConvertOptions;
    let commonProperties: CommonMainOperationWithResult;
    let opResult: RevealResult;

    const act = () => convertObject(target, rpcData, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, resultConverter] = [mock(), mock()];
        target = new RevealOperationConverter(instance(commonOperationConverter), instance(resultConverter));

        rpcData = { public_key: 'kkk' } as typeof rpcData;
        options = 'mockedOptions' as any;
        opResult = 'mockedResult' as any;

        commonProperties = setupCommonOpWithResultProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(resultConverter.convertFromMetadataOperationResult(rpcData, 'op/id')).thenReturn(opResult);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<RevealOperation>({
            balanceUpdates: commonProperties.balanceUpdates,
            counter: commonProperties.counter,
            fee: commonProperties.fee,
            gasLimit: commonProperties.gasLimit,
            internalOperations: commonProperties.internalOperations,
            isInternal: commonProperties.isInternal,
            kind: OperationKind.Reveal,
            publicKey: 'kkk',
            rpcData,
            sourceAddress: commonProperties.sourceAddress,
            storageLimit: commonProperties.storageLimit,
            result: opResult,
            uid: commonProperties.uid,
        });
        expect(operation).toBeFrozen();
    });
});
