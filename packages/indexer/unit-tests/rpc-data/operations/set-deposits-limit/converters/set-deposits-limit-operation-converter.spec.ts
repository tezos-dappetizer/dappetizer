import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { instance, mock, when } from 'ts-mockito';

import {
    CommonMainOperationWithResult,
    CommonMainOperationWithResultConverter,
} from '../../../../../src/rpc-data/operations/common/main-operation/common-main-operation-with-result-converter';
import {
    SetDepositsLimitOperationConverter,
} from '../../../../../src/rpc-data/operations/set-deposits-limit/converters/set-deposits-limit-operation-converter';
import {
    SetDepositsLimitResultConverter,
} from '../../../../../src/rpc-data/operations/set-deposits-limit/converters/set-deposits-limit-result-converter';
import { RpcConvertOptions } from '../../../../../src/rpc-data/rpc-convert-options';
import {
    OperationKind,
    SetDepositsLimitOperation,
    SetDepositsLimitResult,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../rpc-test-helpers';
import { setupCommonOpWithResultProperties } from '../../operation-rpc-test-helpers';

describe(SetDepositsLimitOperationConverter.name, () => {
    let target: SetDepositsLimitOperationConverter;
    let commonOperationConverter: CommonMainOperationWithResultConverter;
    let resultConverter: SetDepositsLimitResultConverter;

    let rpcData: taquitoRpc.OperationContentsAndResultSetDepositsLimit;
    let options: RpcConvertOptions;
    let commonProperties: CommonMainOperationWithResult;
    let opResult: SetDepositsLimitResult;

    const act = () => convertObject(target, rpcData, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, resultConverter] = [mock(), mock()];
        target = new SetDepositsLimitOperationConverter(instance(commonOperationConverter), instance(resultConverter));

        rpcData = { limit: '123' } as typeof rpcData;
        options = 'mockedOptions' as any;
        opResult = 'mockedResult' as any;

        commonProperties = setupCommonOpWithResultProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(resultConverter.convertFromMetadataOperationResult(rpcData, 'op/id')).thenReturn(opResult);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<SetDepositsLimitOperation>({
            balanceUpdates: commonProperties.balanceUpdates,
            counter: commonProperties.counter,
            fee: commonProperties.fee,
            gasLimit: commonProperties.gasLimit,
            internalOperations: commonProperties.internalOperations,
            isInternal: commonProperties.isInternal,
            kind: OperationKind.SetDepositsLimit,
            limit: new BigNumber(123),
            rpcData,
            sourceAddress: commonProperties.sourceAddress,
            storageLimit: commonProperties.storageLimit,
            result: opResult,
            uid: commonProperties.uid,
        });
        expect(operation).toBeFrozen();
    });
});
