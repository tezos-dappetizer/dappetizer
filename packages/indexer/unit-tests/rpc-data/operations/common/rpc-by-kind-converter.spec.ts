import { anything, instance, mock, verify, when } from 'ts-mockito';

import { expectToThrow, TestLogger } from '../../../../../../test-utilities/mocks';
import { RpcByKindConverter } from '../../../../src/rpc-data/operations/common/rpc-by-kind-converter';
import { RpcConversionError } from '../../../../src/rpc-data/rpc-conversion-error';
import { RpcConvertOptions } from '../../../../src/rpc-data/rpc-convert-options';
import { RpcConverter } from '../../../../src/rpc-data/rpc-converter';
import { RpcSkippableConversionError } from '../../../../src/rpc-data/rpc-skippable-array-converter';
import { convertObject } from '../../rpc-test-helpers';

interface RpcFoo {
    kind: string;
    data: number;
}

describe(RpcByKindConverter.name, () => {
    let target: RpcByKindConverter<RpcFoo, string>;
    let aaaConverter: RpcConverter<RpcFoo, string, RpcConvertOptions>;
    let bbbCccConverter: RpcConverter<RpcFoo, string, RpcConvertOptions>;

    let rpcData: RpcFoo;
    let options: RpcConvertOptions;

    const act = () => convertObject(target, rpcData, 'op/id', options);

    beforeEach(() => {
        [aaaConverter, bbbCccConverter] = [mock(), mock()];
        target = new RpcByKindConverter(
            {
                aaa: instance(aaaConverter),
                bbb_ccc: instance(bbbCccConverter),
            },
            new TestLogger(),
        );

        rpcData = { kind: 'aaa', data: 123 };
        options = {} as RpcConvertOptions;

        when(aaaConverter.convert(rpcData, 'op/id[aaa]', options)).thenReturn('mockedAaa');
        when(bbbCccConverter.convert(rpcData, 'op/id[bbb_ccc]', options)).thenReturn('mockedBbbCcc');
    });

    it.each([
        ['aaa', 'mockedAaa'],
        ['bbb_ccc', 'mockedBbbCcc'],
    ])('should correctly convert e.g. %s', (rpcKind, expected) => {
        rpcData.kind = rpcKind;

        const operation = act();

        expect(operation).toBe(expected);
    });

    it.each([
        ['undefined', undefined, RpcConversionError],
        ['null', null, RpcConversionError],
        ['mistyped', 123, RpcConversionError],
        ['empty', '', RpcConversionError],
        ['white-space', '  \t', RpcConversionError],
        ['unknown', 'wtf', RpcSkippableConversionError],
    ])('should fail if %s kind', (_desc, kind: any, expectedError) => {
        rpcData.kind = kind;

        const error = expectToThrow(act, expectedError);

        expect(error.uid).toBe('op/id/kind');
        expect(error.dataDescription).toInclude(`['aaa', 'bbb_ccc']`);
        verify(aaaConverter.convert(anything(), anything(), anything())).never();
        verify(bbbCccConverter.convert(anything(), anything(), anything())).never();
    });
});
