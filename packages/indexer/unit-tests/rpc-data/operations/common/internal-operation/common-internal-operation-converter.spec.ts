import * as taquitoRpc from '@taquito/rpc';
import { Writable } from 'ts-essentials';
import { instance, mock, when } from 'ts-mockito';

import { expectToThrow } from '../../../../../../../test-utilities/mocks';
import { LazyContractConverter } from '../../../../../src/rpc-data/common/lazy-contract/lazy-contract-converter';
import {
    CommonInternalOperation,
    CommonInternalOperationConverter,
    guardKind,
} from '../../../../../src/rpc-data/operations/common/internal-operation/common-internal-operation-converter';
import {
    SmartRollupConverter,
} from '../../../../../src/rpc-data/operations/smart-rollup/common/converters/smart-rollup-converter';
import { LazyContract, TaquitoRpcInternalOperation } from '../../../../../src/rpc-data/rpc-data-interfaces';
import { SmartRollup } from '../../../../../src/smart-rollups/smart-rollup';

describe(guardKind.name, () => {
    it('should pass if required kind', () => {
        const rpcData = { kind: taquitoRpc.OpKind.TRANSACTION } as TaquitoRpcInternalOperation;

        guardKind(rpcData, taquitoRpc.OpKind.TRANSACTION, 'op/id');
    });

    it('should throw if NOT required kind', () => {
        const rpcData = { kind: taquitoRpc.OpKind.TRANSACTION } as TaquitoRpcInternalOperation;

        const error = expectToThrow(() => guardKind(rpcData, taquitoRpc.OpKind.DELEGATION, 'op/id'));

        expect(error.message).toIncludeMultiple([`"transaction"`, `"delegation"`, JSON.stringify(rpcData)]);
    });
});

describe(CommonInternalOperationConverter.name, () => {
    let target: CommonInternalOperationConverter;
    let contractConverter: LazyContractConverter;
    let smartRollupConverter: SmartRollupConverter;

    let rpcData: Writable<TaquitoRpcInternalOperation>;
    let contract: LazyContract;
    let rollup: SmartRollup;

    const act = () => target.convert(rpcData, 'op/id');

    beforeEach(() => {
        [contractConverter, smartRollupConverter] = [mock(), mock()];
        target = new CommonInternalOperationConverter(instance(contractConverter), instance(smartRollupConverter));

        rpcData = {
            nonce: 123,
            source: 'KT1RKZcsTS7kLuRd6JygiVkC1RJ4pfwznwkf',
        } as TaquitoRpcInternalOperation;
        contract = { address: 'mockedContract', type: 'Contract' } as typeof contract;
        rollup = 'mockedRollup' as any;

        when(contractConverter.convertProperty(rpcData, 'source', 'op/id')).thenReturn(contract);
        when(smartRollupConverter.convertProperty(rpcData, 'source', 'op/id')).thenReturn(rollup);
    });

    it('should convert correctly with contract', () => {
        const operation = act();

        expect(operation).toEqual<CommonInternalOperation>({
            isInternal: true,
            nonce: 123,
            rpcData,
            source: contract,
            sourceAddress: 'KT1RKZcsTS7kLuRd6JygiVkC1RJ4pfwznwkf',
            sourceContract: contract,
            uid: 'op/id',
        });
    });

    it('should convert correctly with smart rollup', () => {
        rpcData.source = 'sr1Ghq66tYK9y3r8CC1Tf8i8m5nxh8nTvZEf';

        const operation = act();

        expect(operation).toEqual<CommonInternalOperation>({
            isInternal: true,
            nonce: 123,
            rpcData,
            source: rollup,
            sourceAddress: 'sr1Ghq66tYK9y3r8CC1Tf8i8m5nxh8nTvZEf',
            sourceContract: null,
            uid: 'op/id',
        });
    });
});
