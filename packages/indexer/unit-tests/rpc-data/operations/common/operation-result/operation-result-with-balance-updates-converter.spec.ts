import { instance, mock, when } from 'ts-mockito';

import {
    BalanceUpdateConverter,
} from '../../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import {
    OperationResultWithBalanceUpdatesConverter,
    OperationResultWithBalanceUpdatesParts,
    RpcOperationResultWithBalanceUpdates,
} from '../../../../../src/rpc-data/operations/common/operation-result/operation-result-with-balance-updates-converter';
import { BalanceUpdate } from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertAppliedResultParts } from '../../../rpc-test-helpers';

class FooResultConverter extends OperationResultWithBalanceUpdatesConverter<RpcOperationResultWithBalanceUpdates> {}

describe(OperationResultWithBalanceUpdatesConverter, () => {
    let target: FooResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;

    let rpcData: RpcOperationResultWithBalanceUpdates;
    let balanceUpdates: BalanceUpdate[];

    const act = () => convertAppliedResultParts(target, rpcData, 'res/id');

    beforeEach(() => {
        balanceUpdateConverter = mock();
        target = new FooResultConverter(null!, instance(balanceUpdateConverter));

        rpcData = {} as RpcOperationResultWithBalanceUpdates;
        balanceUpdates = 'mockedBalanceUpdates' as any;

        when(balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', 'res/id')).thenReturn(balanceUpdates);
    });

    it('should convert correctly', () => {
        const result = act();

        expect(result).toEqual<OperationResultWithBalanceUpdatesParts>({ balanceUpdates });
    });
});
