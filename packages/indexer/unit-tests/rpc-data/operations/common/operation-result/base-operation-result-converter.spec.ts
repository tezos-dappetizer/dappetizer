import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { Writable } from 'ts-essentials';
import { anything, instance, mock, spy, verify, when } from 'ts-mockito';

import { asReadonly } from '../../../../../../utils/src/public-api';
import { OperationErrorConverter } from '../../../../../src/rpc-data/common/operation-error/operation-error-converter';
import {
    AppliedResult,
    BaseOperationResultConverter,
    BaseRpcOperationResult,
} from '../../../../../src/rpc-data/operations/common/operation-result/base-operation-result-converter';
import {
    BacktrackedOperationResult,
    FailedOperationResult,
    OperationError,
    OperationResultStatus,
    SkippedOperationResult,
    TaquitoRpcInternalOperation,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../rpc-test-helpers';

interface FooResultParts { foo: string }
interface Options { opts: string }

class FooConverter extends BaseOperationResultConverter<TaquitoRpcInternalOperation['result'], FooResultParts, Options> {
    override convertAppliedResultParts(_rpcData: TaquitoRpcInternalOperation['result'], _uid: string, _options: Options): FooResultParts {
        return { foo: 'bar' };
    }
}

describe(BaseOperationResultConverter.name, () => {
    let target: FooConverter;
    let targetSpy: FooConverter;
    let operationErrorConverter: OperationErrorConverter;

    let rpcData: Writable<TaquitoRpcInternalOperation['result']>;
    let options: Options;
    let errors: OperationError[];

    const act = () => convertObject(target, rpcData, 'res/id', options);

    beforeEach(() => {
        operationErrorConverter = mock();
        target = new FooConverter(instance(operationErrorConverter));
        targetSpy = spy(target);

        rpcData = {
            consumed_gas: '11',
            consumed_milligas: '22',
            errors: asReadonly([{}]),
        } as TaquitoRpcInternalOperation['result'];
        options = { opts: 'ooo' };
        errors = 'mockedErrors' as any;

        when(operationErrorConverter.convertArrayProperty(rpcData, 'errors', 'res/id')).thenReturn(errors);
    });

    it(`should convert ${OperationResultStatus.Applied} result`, () => {
        rpcData.status = 'applied';

        const result = act();

        expect(result).toEqual<AppliedResult<TaquitoRpcInternalOperation['result'], FooResultParts>>({
            consumedGas: new BigNumber(11),
            consumedMilligas: new BigNumber(22),
            foo: 'bar',
            rpcData,
            status: OperationResultStatus.Applied,
            uid: 'res/id',
        });
        expect(result).toBeFrozen();
        verify(targetSpy.convertAppliedResultParts(rpcData, 'res/id', options)).once();
        verify(operationErrorConverter.convertArrayProperty(anything(), anything(), anything())).never();
    });

    it(`should convert ${OperationResultStatus.Backtracked} result`, () => {
        rpcData.status = 'backtracked';

        const opResult = act();

        expect(opResult).toEqual<BacktrackedOperationResult>({
            consumedGas: new BigNumber(11),
            consumedMilligas: new BigNumber(22),
            errors,
            rpcData,
            status: OperationResultStatus.Backtracked,
            uid: 'res/id',
        });
        expect(opResult).toBeFrozen();
        verify(targetSpy.convertAppliedResultParts(anything(), anything(), anything())).never();
    });

    it(`should convert ${OperationResultStatus.Failed} result`, () => {
        rpcData.status = 'failed';

        const opResult = act();

        expect(opResult).toEqual<FailedOperationResult>({
            rpcData,
            status: OperationResultStatus.Failed,
            errors,
            uid: 'res/id',
        });
        expect(opResult).toBeFrozen();
        verify(targetSpy.convertAppliedResultParts(anything(), anything(), anything())).never();
    });

    it(`should convert ${OperationResultStatus.Skipped} result`, () => {
        rpcData.status = 'skipped';

        const opResult = act();

        expect(opResult).toEqual<SkippedOperationResult>({
            rpcData,
            status: OperationResultStatus.Skipped,
            uid: 'res/id',
        });
        expect(opResult).toBeFrozen();
        verify(targetSpy.convertAppliedResultParts(anything(), anything(), anything())).never();
        verify(operationErrorConverter.convertArrayProperty(anything(), anything(), anything())).never();
    });

    describe('should handle gas correctly', () => {
        it.each<[string, string | undefined, string | undefined, Partial<AppliedResult<BaseRpcOperationResult, FooResultParts>>]>([
            ['no consumed_milligas', '123', undefined, { consumedGas: new BigNumber(123), consumedMilligas: null }],
            ['no consumed_gas', undefined, '12001', { consumedGas: new BigNumber(13), consumedMilligas: new BigNumber(12001) }],
            ['no gas', undefined, undefined, { consumedGas: null, consumedMilligas: null }],
        ])('%s', (_desc, rpcConsumedGas, rpcConsumedMilligas, expected) => {
            rpcData.status = 'applied';
            (rpcData as taquitoRpc.OperationResultDelegation).consumed_gas = rpcConsumedGas;
            rpcData.consumed_milligas = rpcConsumedMilligas;

            const opResult = act();

            expect(opResult).toMatchObject(expected);
        });
    });
});
