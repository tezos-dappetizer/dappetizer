import {
    BaseRpcOperationResult,
} from '../../../../../src/rpc-data/operations/common/operation-result/base-operation-result-converter';
import {
    SimpleOperationResultConverter,
} from '../../../../../src/rpc-data/operations/common/operation-result/simple-operation-result-converter';
import { convertAppliedResultParts } from '../../../rpc-test-helpers';

describe(SimpleOperationResultConverter.name, () => {
    const target = new SimpleOperationResultConverter<BaseRpcOperationResult>(null!);

    it('should convert empty result specific parts', () => {
        const result = convertAppliedResultParts(target, null!, null!);

        expect(result).toEqual({});
    });
});
