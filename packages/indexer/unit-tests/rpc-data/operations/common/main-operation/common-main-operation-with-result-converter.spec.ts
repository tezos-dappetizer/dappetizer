import { BigNumber } from 'bignumber.js';
import { instance, mock, when } from 'ts-mockito';

import { asReadonly } from '../../../../../../utils/src/public-api';
import {
    BalanceUpdateConverter,
} from '../../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import {
    CommonMainOperationWithResult,
    CommonMainOperationWithResultConverter,
    TaquitoRpcMainOperationWithResult,
} from '../../../../../src/rpc-data/operations/common/main-operation/common-main-operation-with-result-converter';
import { InternalOperationConverter } from '../../../../../src/rpc-data/operations/internal-operation-converter';
import { RpcConvertOptions } from '../../../../../src/rpc-data/rpc-convert-options';
import { BalanceUpdate, InternalOperation } from '../../../../../src/rpc-data/rpc-data-interfaces';

describe(CommonMainOperationWithResultConverter.name, () => {
    let target: CommonMainOperationWithResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;
    let internalOperationConverter: InternalOperationConverter;

    let rpcData: TaquitoRpcMainOperationWithResult;
    let options: RpcConvertOptions;
    let balanceUpdates: BalanceUpdate[];
    let internalOperations: InternalOperation[];

    const act = () => target.convert(rpcData, 'op/id', options);

    beforeEach(() => {
        [balanceUpdateConverter, internalOperationConverter] = [mock(), mock()];
        target = new CommonMainOperationWithResultConverter(instance(balanceUpdateConverter), instance(internalOperationConverter));

        rpcData = {
            counter: '11',
            fee: '22',
            gas_limit: '33',
            source: 'tz1RjoHc98dBoqaH2jawF62XNKh7YsHwbkEv',
            storage_limit: '44',
            metadata: {
                balance_updates: asReadonly([{}]),
                internal_operation_results: asReadonly([{}]),
            },
        } as TaquitoRpcMainOperationWithResult;
        options = {} as RpcConvertOptions;
        balanceUpdates = 'mockedBalanceUpdates' as any;
        internalOperations = 'mockedInternalOperations' as any;

        when(balanceUpdateConverter.convertFromMetadataProperty(rpcData, 'op/id')).thenReturn(balanceUpdates);
        when(internalOperationConverter.convertArray(
            rpcData.metadata.internal_operation_results,
            'op/id/metadata/internal_operation_results',
            options,
        )).thenReturn(internalOperations);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<CommonMainOperationWithResult>({
            balanceUpdates,
            counter: new BigNumber(11),
            fee: new BigNumber(22),
            gasLimit: new BigNumber(33),
            internalOperations,
            isInternal: false,
            sourceAddress: 'tz1RjoHc98dBoqaH2jawF62XNKh7YsHwbkEv',
            storageLimit: new BigNumber(44),
            uid: 'op/id',
        });
    });
});
