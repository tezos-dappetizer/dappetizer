import { assert, IsExact } from 'conditional-type-checks';

import { Result } from '../../../../../src/rpc-data/operations/common/operation-result/base-operation-result-converter';
import {
    TransactionResultParts,
} from '../../../../../src/rpc-data/operations/transaction/converters/transaction-result-converter';
import {
    AppliedOperationResult,
    OperationResult,
    TaquitoRpcTransactionResult,
    TransactionResult,
} from '../../../../../src/rpc-data/rpc-data-interfaces';

interface TaquitoFooResult {
    readonly rpc: string;
}

interface FooResultParts {
    readonly foo: string;
}

interface AppliedFooResult extends AppliedOperationResult<TaquitoFooResult> {
    readonly foo: string;
}

type FooResult = OperationResult<AppliedFooResult>;

assert<IsExact<Result<TaquitoFooResult, FooResultParts>, FooResult>>(true);

// Tests sample operation result.
assert<IsExact<Result<TaquitoRpcTransactionResult, TransactionResultParts>, TransactionResult>>(true);
