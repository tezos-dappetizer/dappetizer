import { instance, mock, when } from 'ts-mockito';

import {
    CommonSmartRollupOperation,
    CommonSmartRollupOperationConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/common/converters/common-smart-rollup-operation-converter';
import {
    SmartRollupRecoverBondOperationConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/recover-bond/converters/smart-rollup-recover-bond-operation-converter';
import {
    SmartRollupRecoverBondResultConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/recover-bond/converters/smart-rollup-recover-bond-result-converter';
import { RpcConvertOptions } from '../../../../../../src/rpc-data/rpc-convert-options';
import {
    OperationKind,
    SmartRollupRecoverBondOperation,
    SmartRollupRecoverBondResult,
    TaquitoRpcOperation,
    TaquitoRpcSmartRollupRecoverBondOperation,
} from '../../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../../rpc-test-helpers';
import { setupCommonSmartRollupOpProperties } from '../../../operation-rpc-test-helpers';

describe(SmartRollupRecoverBondOperationConverter.name, () => {
    let target: SmartRollupRecoverBondOperationConverter;
    let commonOperationConverter: CommonSmartRollupOperationConverter;
    let resultConverter: SmartRollupRecoverBondResultConverter;

    let rpcData: TaquitoRpcSmartRollupRecoverBondOperation;
    let options: RpcConvertOptions;
    let commonProperties: CommonSmartRollupOperation;
    let opResult: SmartRollupRecoverBondResult;

    const act = () => convertObject(target, rpcData as unknown as TaquitoRpcOperation, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, resultConverter] = [mock(), mock()];
        target = new SmartRollupRecoverBondOperationConverter(instance(commonOperationConverter), instance(resultConverter));

        rpcData = {
            rollup: 'sr1AE6U3GNzE8iKzj6sKS5wh1U32ogeULCoN',
            staker: 'tz1QD39GBmSzccuDxWMevj2gudiTX1pZL5kC',
        } as typeof rpcData;
        options = 'mockedOptions' as any;
        opResult = 'mockedResult' as any;

        commonProperties = setupCommonSmartRollupOpProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(resultConverter.convertFromMetadataOperationResult(rpcData, 'op/id')).thenReturn(opResult as any);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<SmartRollupRecoverBondOperation>({
            balanceUpdates: commonProperties.balanceUpdates,
            counter: commonProperties.counter,
            fee: commonProperties.fee,
            gasLimit: commonProperties.gasLimit,
            internalOperations: commonProperties.internalOperations,
            isInternal: commonProperties.isInternal,
            kind: OperationKind.SmartRollupRecoverBond,
            result: opResult,
            rollup: commonProperties.rollup,
            rpcData,
            sourceAddress: commonProperties.sourceAddress,
            stakerAddress: 'tz1QD39GBmSzccuDxWMevj2gudiTX1pZL5kC',
            storageLimit: commonProperties.storageLimit,
            uid: commonProperties.uid,
        });
        expect(operation).toBeFrozen();
    });
});
