import { instance, mock, when } from 'ts-mockito';

import {
    BalanceUpdateConverter,
} from '../../../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import {
    SmartRollupRecoverBondResultConverter,
    SmartRollupRecoverBondResultParts,
} from '../../../../../../src/rpc-data/operations/smart-rollup/recover-bond/converters/smart-rollup-recover-bond-result-converter';
import { BalanceUpdate, TaquitoRpcSmartRollupRecoverBondResult } from '../../../../../../src/rpc-data/rpc-data-interfaces';
import { convertAppliedResultParts } from '../../../../rpc-test-helpers';

describe(SmartRollupRecoverBondResultConverter, () => {
    let target: SmartRollupRecoverBondResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;

    let rpcData: TaquitoRpcSmartRollupRecoverBondResult;
    let balanceUpdates: BalanceUpdate[];

    const act = () => convertAppliedResultParts(target, rpcData, 'res/id');

    beforeEach(() => {
        balanceUpdateConverter = mock();
        target = new SmartRollupRecoverBondResultConverter(null!, instance(balanceUpdateConverter));

        rpcData = 'mockedRpcData' as any;
        balanceUpdates = 'mockedBalanceUpdates' as any;

        when(balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', 'res/id')).thenReturn(balanceUpdates);
    });

    it('should convert correctly', () => {
        const result = act();

        expect(result).toEqual<SmartRollupRecoverBondResultParts>({ balanceUpdates });
    });
});
