import { instance, mock, when } from 'ts-mockito';

import {
    CommonSmartRollupOperation,
    CommonSmartRollupOperationConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/common/converters/common-smart-rollup-operation-converter';
import {
    SmartRollupTimeoutOperationConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/timeout/converters/smart-rollup-timeout-operation-converter';
import {
    SmartRollupTimeoutResultConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/timeout/converters/smart-rollup-timeout-result-converter';
import { RpcConvertOptions } from '../../../../../../src/rpc-data/rpc-convert-options';
import {
    OperationKind,
    SmartRollupTimeoutOperation,
    SmartRollupTimeoutResult,
    TaquitoRpcOperation,
    TaquitoRpcSmartRollupTimeoutOperation,
} from '../../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../../rpc-test-helpers';
import { setupCommonSmartRollupOpProperties } from '../../../operation-rpc-test-helpers';

describe(SmartRollupTimeoutOperationConverter.name, () => {
    let target: SmartRollupTimeoutOperationConverter;
    let commonOperationConverter: CommonSmartRollupOperationConverter;
    let resultConverter: SmartRollupTimeoutResultConverter;

    let rpcData: TaquitoRpcSmartRollupTimeoutOperation;
    let options: RpcConvertOptions;
    let commonProperties: CommonSmartRollupOperation;
    let opResult: SmartRollupTimeoutResult;

    const act = () => convertObject(target, rpcData as unknown as TaquitoRpcOperation, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, resultConverter] = [mock(), mock()];
        target = new SmartRollupTimeoutOperationConverter(instance(commonOperationConverter), instance(resultConverter));

        rpcData = {
            rollup: 'sr1AE6U3GNzE8iKzj6sKS5wh1U32ogeULCoN',
            stakers: {
                alice: 'tz1TecRhYLVV9bTKRKU9g1Hhpb1Ymw3ynzWS',
                bob: 'tz1iFnSQ6V2d8piVMPMjtDNdkYNMaUfKwsoy',
            },
        } as typeof rpcData;
        options = 'mockedOptions' as any;
        opResult = 'mockedResult' as any;

        commonProperties = setupCommonSmartRollupOpProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(resultConverter.convertFromMetadataOperationResult(rpcData, 'op/id')).thenReturn(opResult as any);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<SmartRollupTimeoutOperation>({
            aliceStakerAddress: 'tz1TecRhYLVV9bTKRKU9g1Hhpb1Ymw3ynzWS',
            balanceUpdates: commonProperties.balanceUpdates,
            bobStakerAddress: 'tz1iFnSQ6V2d8piVMPMjtDNdkYNMaUfKwsoy',
            counter: commonProperties.counter,
            fee: commonProperties.fee,
            gasLimit: commonProperties.gasLimit,
            internalOperations: commonProperties.internalOperations,
            isInternal: commonProperties.isInternal,
            kind: OperationKind.SmartRollupTimeout,
            result: opResult,
            rollup: commonProperties.rollup,
            rpcData,
            sourceAddress: commonProperties.sourceAddress,
            storageLimit: commonProperties.storageLimit,
            uid: commonProperties.uid,
        });
        expect(operation).toBeFrozen();
    });
});
