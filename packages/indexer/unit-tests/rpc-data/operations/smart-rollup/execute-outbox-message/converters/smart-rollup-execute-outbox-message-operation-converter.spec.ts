import { instance, mock, when } from 'ts-mockito';

import {
    CommonSmartRollupOperation,
    CommonSmartRollupOperationConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/common/converters/common-smart-rollup-operation-converter';
import {
    SmartRollupExecuteOutboxMessageOperationConverter,
// eslint-disable-next-line max-len
} from '../../../../../../src/rpc-data/operations/smart-rollup/execute-outbox-message/converters/smart-rollup-execute-outbox-message-operation-converter';
import {
    SmartRollupExecuteOutboxMessageResultConverter,
// eslint-disable-next-line max-len
} from '../../../../../../src/rpc-data/operations/smart-rollup/execute-outbox-message/converters/smart-rollup-execute-outbox-message-result-converter';
import { RpcConvertOptions } from '../../../../../../src/rpc-data/rpc-convert-options';
import {
    OperationKind,
    SmartRollupExecuteOutboxMessageOperation,
    SmartRollupExecuteOutboxMessageResult,
    TaquitoRpcSmartRollupExecuteOutboxMessageOperation,
} from '../../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../../rpc-test-helpers';
import { setupCommonSmartRollupOpProperties } from '../../../operation-rpc-test-helpers';

describe(SmartRollupExecuteOutboxMessageOperationConverter.name, () => {
    let target: SmartRollupExecuteOutboxMessageOperationConverter;
    let commonOperationConverter: CommonSmartRollupOperationConverter;
    let resultConverter: SmartRollupExecuteOutboxMessageResultConverter;

    let rpcData: TaquitoRpcSmartRollupExecuteOutboxMessageOperation;
    let options: RpcConvertOptions;
    let commonProperties: CommonSmartRollupOperation;
    let opResult: SmartRollupExecuteOutboxMessageResult;

    const act = () => convertObject(target, rpcData, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, resultConverter] = [mock(), mock()];
        target = new SmartRollupExecuteOutboxMessageOperationConverter(instance(commonOperationConverter), instance(resultConverter));

        rpcData = {
            cemented_commitment: 'ccc',
            output_proof: 'ppp',
        } as typeof rpcData;
        options = 'mockedOptions' as any;
        opResult = 'mockedResult' as any;

        commonProperties = setupCommonSmartRollupOpProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(resultConverter.convertFromMetadataOperationResult(rpcData, 'op/id')).thenReturn(opResult);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<SmartRollupExecuteOutboxMessageOperation>({
            balanceUpdates: commonProperties.balanceUpdates,
            cementedCommitment: 'ccc',
            counter: commonProperties.counter,
            fee: commonProperties.fee,
            gasLimit: commonProperties.gasLimit,
            internalOperations: commonProperties.internalOperations,
            isInternal: commonProperties.isInternal,
            kind: OperationKind.SmartRollupExecuteOutboxMessage,
            outputProof: 'ppp',
            result: opResult,
            rollup: commonProperties.rollup,
            rpcData,
            sourceAddress: commonProperties.sourceAddress,
            storageLimit: commonProperties.storageLimit,
            uid: commonProperties.uid,
        });
        expect(operation).toBeFrozen();
    });
});
