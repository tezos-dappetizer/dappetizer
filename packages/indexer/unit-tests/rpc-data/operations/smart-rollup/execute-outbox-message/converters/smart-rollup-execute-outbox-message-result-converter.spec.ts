import { BigNumber } from 'bignumber.js';
import { instance, mock, when } from 'ts-mockito';

import {
    BalanceUpdateConverter,
} from '../../../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import { TicketUpdatesConverter } from '../../../../../../src/rpc-data/common/ticket/converters/ticket-updates-converter';
import {
    SmartRollupExecuteOutboxMessageResultConverter,
    SmartRollupExecuteOutboxMessageResultParts,
// eslint-disable-next-line max-len
} from '../../../../../../src/rpc-data/operations/smart-rollup/execute-outbox-message/converters/smart-rollup-execute-outbox-message-result-converter';
import {
    BalanceUpdate,
    TaquitoRpcSmartRollupExecuteOutboxMessageResult,
    TicketUpdate,
} from '../../../../../../src/rpc-data/rpc-data-interfaces';
import { convertAppliedResultParts } from '../../../../rpc-test-helpers';

describe(SmartRollupExecuteOutboxMessageResultConverter, () => {
    let target: SmartRollupExecuteOutboxMessageResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;
    let ticketUpdatesConverter: TicketUpdatesConverter;

    let rpcData: TaquitoRpcSmartRollupExecuteOutboxMessageResult;
    let balanceUpdates: BalanceUpdate[];
    let ticketUpdates: TicketUpdate[];

    const act = () => convertAppliedResultParts(target, rpcData, 'res/id');

    beforeEach(() => {
        [balanceUpdateConverter, ticketUpdatesConverter] = [mock(), mock()];
        target = new SmartRollupExecuteOutboxMessageResultConverter(null!, instance(balanceUpdateConverter), instance(ticketUpdatesConverter));

        rpcData = { paid_storage_size_diff: '123' } as typeof rpcData;
        balanceUpdates = 'mockedBalanceUpdates' as any;
        ticketUpdates = 'mockedTicketUpdates' as any;

        when(balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', 'res/id')).thenReturn(balanceUpdates);
        when(ticketUpdatesConverter.convertArrayProperty(rpcData, 'ticket_updates', 'res/id')).thenReturn(ticketUpdates);
    });

    it('should convert correctly', () => {
        const result = act();

        expect(result).toEqual<SmartRollupExecuteOutboxMessageResultParts>({
            balanceUpdates,
            paidStorageSizeDiff: new BigNumber(123),
            ticketUpdates,
        });
    });
});
