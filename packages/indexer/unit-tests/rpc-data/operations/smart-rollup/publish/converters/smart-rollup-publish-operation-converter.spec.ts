import { BigNumber } from 'bignumber.js';
import { instance, mock, when } from 'ts-mockito';

import {
    CommonSmartRollupOperation,
    CommonSmartRollupOperationConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/common/converters/common-smart-rollup-operation-converter';
import {
    SmartRollupCommitmentConverter,
    SmartRollupPublishOperationConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/publish/converters/smart-rollup-publish-operation-converter';
import {
    SmartRollupPublishResultConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/publish/converters/smart-rollup-publish-result-converter';
import { RpcConvertOptions } from '../../../../../../src/rpc-data/rpc-convert-options';
import {
    OperationKind,
    SmartRollupCommitment,
    SmartRollupPublishOperation,
    SmartRollupPublishResult,
    TaquitoRpcOperation,
    TaquitoRpcSmartRollupPublishOperation,
} from '../../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../../rpc-test-helpers';
import { setupCommonSmartRollupOpProperties } from '../../../operation-rpc-test-helpers';

describe(SmartRollupCommitmentConverter.name, () => {
    let target: SmartRollupCommitmentConverter;
    let rpcData: TaquitoRpcSmartRollupPublishOperation['commitment'];

    const act = () => convertObject(target, rpcData, 'op/id');

    beforeEach(() => {
        target = new SmartRollupCommitmentConverter();

        rpcData = {
            compressed_state: 'ccc',
            inbox_level: 111,
            number_of_ticks: '222',
            predecessor: 'ppp',
        } as typeof rpcData;
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<SmartRollupCommitment>({
            compressedState: 'ccc',
            inboxLevel: 111,
            numberOfTicks: new BigNumber(222),
            predecessorHash: 'ppp',
        });
        expect(operation).toBeFrozen();
    });
});

describe(SmartRollupPublishOperationConverter.name, () => {
    let target: SmartRollupPublishOperationConverter;
    let commonOperationConverter: CommonSmartRollupOperationConverter;
    let resultConverter: SmartRollupPublishResultConverter;
    let commitmentConverter: SmartRollupCommitmentConverter;

    let rpcData: TaquitoRpcSmartRollupPublishOperation;
    let options: RpcConvertOptions;
    let commonProperties: CommonSmartRollupOperation;
    let opResult: SmartRollupPublishResult;
    let commitment: SmartRollupCommitment;

    const act = () => convertObject(target, rpcData as unknown as TaquitoRpcOperation, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, resultConverter, commitmentConverter] = [mock(), mock(), mock()];
        target = new SmartRollupPublishOperationConverter(
            instance(commonOperationConverter),
            instance(resultConverter),
            instance(commitmentConverter),
        );

        rpcData = { rollup: 'sr1AE6U3GNzE8iKzj6sKS5wh1U32ogeULCoN' } as typeof rpcData;
        options = 'mockedOptions' as any;
        opResult = 'mockedResult' as any;
        commitment = 'mockedCommitment' as any;

        commonProperties = setupCommonSmartRollupOpProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(resultConverter.convertFromMetadataOperationResult(rpcData, 'op/id')).thenReturn(opResult as any);
        when(commitmentConverter.convertProperty(rpcData, 'commitment', 'op/id')).thenReturn(commitment);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<SmartRollupPublishOperation>({
            balanceUpdates: commonProperties.balanceUpdates,
            commitment,
            counter: commonProperties.counter,
            fee: commonProperties.fee,
            gasLimit: commonProperties.gasLimit,
            internalOperations: commonProperties.internalOperations,
            isInternal: commonProperties.isInternal,
            kind: OperationKind.SmartRollupPublish,
            result: opResult,
            rollup: commonProperties.rollup,
            rpcData,
            sourceAddress: commonProperties.sourceAddress,
            storageLimit: commonProperties.storageLimit,
            uid: commonProperties.uid,
        });
        expect(operation).toBeFrozen();
    });
});
