import { instance, mock, when } from 'ts-mockito';

import {
    BalanceUpdateConverter,
} from '../../../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import {
    SmartRollupPublishResultConverter,
    SmartRollupPublishResultParts,
} from '../../../../../../src/rpc-data/operations/smart-rollup/publish/converters/smart-rollup-publish-result-converter';
import { BalanceUpdate, TaquitoRpcSmartRollupPublishResult } from '../../../../../../src/rpc-data/rpc-data-interfaces';
import { convertAppliedResultParts } from '../../../../rpc-test-helpers';

describe(SmartRollupPublishResultConverter, () => {
    let target: SmartRollupPublishResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;

    let rpcData: TaquitoRpcSmartRollupPublishResult;
    let balanceUpdates: BalanceUpdate[];

    const act = () => convertAppliedResultParts(target, rpcData, 'res/id');

    beforeEach(() => {
        balanceUpdateConverter = mock();
        target = new SmartRollupPublishResultConverter(null!, instance(balanceUpdateConverter));

        rpcData = {
            published_at_level: 123,
            staked_hash: 'sss',
        } as typeof rpcData;
        balanceUpdates = 'mockedBalanceUpdates' as any;

        when(balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', 'res/id')).thenReturn(balanceUpdates);
    });

    it('should convert correctly', () => {
        const result = act();

        expect(result).toEqual<SmartRollupPublishResultParts>({
            balanceUpdates,
            publishedAtLevel: 123,
            stakedHash: 'sss',
        });
    });
});
