import { instance, mock, when } from 'ts-mockito';

import { MichelsonSchema } from '../../../../../../src/michelson/michelson-schema';
import {
    CommonMainOperationWithResult,
    CommonMainOperationWithResultConverter,
} from '../../../../../../src/rpc-data/operations/common/main-operation/common-main-operation-with-result-converter';
import {
    SmartRollupOriginateOperationConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/originate/converters/smart-rollup-originate-operation-converter';
import {
    SmartRollupOriginateResultConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/originate/converters/smart-rollup-originate-result-converter';
import { RpcConvertOptions } from '../../../../../../src/rpc-data/rpc-convert-options';
import {
    OperationKind,
    SmartRollupOriginateOperation,
    SmartRollupOriginateResult,
    TaquitoRpcSmartRollupOriginateOperation,
} from '../../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../../rpc-test-helpers';
import { setupCommonOpWithResultProperties } from '../../../operation-rpc-test-helpers';

describe(SmartRollupOriginateOperationConverter.name, () => {
    let target: SmartRollupOriginateOperationConverter;
    let commonOperationConverter: CommonMainOperationWithResultConverter;
    let resultConverter: SmartRollupOriginateResultConverter;

    let rpcData: TaquitoRpcSmartRollupOriginateOperation;
    let options: RpcConvertOptions;
    let commonProperties: CommonMainOperationWithResult;
    let opResult: SmartRollupOriginateResult;

    const act = () => convertObject(target, rpcData, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, resultConverter] = [mock(), mock()];
        target = new SmartRollupOriginateOperationConverter(instance(commonOperationConverter), instance(resultConverter));

        rpcData = {
            kernel: 'kkk',
            parameters_ty: { prim: 'bytes' },
            pvm_kind: 'wasm_2_0_0',
        } as typeof rpcData;
        options = 'mockedOptions' as any;
        opResult = 'mockedResult' as any;

        commonProperties = setupCommonOpWithResultProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(resultConverter.convertFromMetadataOperationResult(rpcData, 'op/id')).thenReturn(opResult);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<SmartRollupOriginateOperation>({
            balanceUpdates: commonProperties.balanceUpdates,
            counter: commonProperties.counter,
            fee: commonProperties.fee,
            gasLimit: commonProperties.gasLimit,
            internalOperations: commonProperties.internalOperations,
            isInternal: commonProperties.isInternal,
            kernel: 'kkk',
            kind: OperationKind.SmartRollupOriginate,
            parametersSchema: new MichelsonSchema({ prim: 'bytes' }),
            pvmKind: 'wasm_2_0_0',
            result: opResult,
            rpcData,
            sourceAddress: commonProperties.sourceAddress,
            storageLimit: commonProperties.storageLimit,
            uid: commonProperties.uid,
        });
        expect(operation).toBeFrozen();
    });
});
