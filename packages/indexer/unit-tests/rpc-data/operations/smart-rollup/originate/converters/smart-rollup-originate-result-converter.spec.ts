import { BigNumber } from 'bignumber.js';
import { instance, mock, when } from 'ts-mockito';

import {
    BalanceUpdateConverter,
} from '../../../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import {
    SmartRollupConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/common/converters/smart-rollup-converter';
import {
    SmartRollupOriginateResultConverter,
    SmartRollupOriginateResultParts,
} from '../../../../../../src/rpc-data/operations/smart-rollup/originate/converters/smart-rollup-originate-result-converter';
import { BalanceUpdate, TaquitoRpcSmartRollupOriginateResult } from '../../../../../../src/rpc-data/rpc-data-interfaces';
import { SmartRollup } from '../../../../../../src/smart-rollups/smart-rollup';
import { convertAppliedResultParts } from '../../../../rpc-test-helpers';

describe(SmartRollupOriginateResultConverter, () => {
    let target: SmartRollupOriginateResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;
    let rollupConverter: SmartRollupConverter;

    let rpcData: TaquitoRpcSmartRollupOriginateResult;
    let balanceUpdates: BalanceUpdate[];
    let originatedRollup: SmartRollup;

    const act = () => convertAppliedResultParts(target, rpcData, 'res/id');

    beforeEach(() => {
        [balanceUpdateConverter, rollupConverter] = [mock(), mock()];
        target = new SmartRollupOriginateResultConverter(null!, instance(balanceUpdateConverter), instance(rollupConverter));

        rpcData = {
            genesis_commitment_hash: 'src12huqn6n2noPqfuTuX7zg7GzeN1iCGnykC8bgb4Ypvm4YNWGZ5B',
            size: '6552',
        } as typeof rpcData;
        balanceUpdates = 'mockedBalanceUpdates' as any;
        originatedRollup = 'mockedRollup' as any;

        when(balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', 'res/id')).thenReturn(balanceUpdates);
        when(rollupConverter.convertProperty(rpcData, 'address', 'res/id')).thenReturn(originatedRollup);
    });

    it('should convert correctly', () => {
        const result = act();

        expect(result).toEqual<SmartRollupOriginateResultParts>({
            balanceUpdates,
            genesisCommitmentHash: 'src12huqn6n2noPqfuTuX7zg7GzeN1iCGnykC8bgb4Ypvm4YNWGZ5B',
            originatedRollup,
            size: new BigNumber(6552),
        });
    });
});
