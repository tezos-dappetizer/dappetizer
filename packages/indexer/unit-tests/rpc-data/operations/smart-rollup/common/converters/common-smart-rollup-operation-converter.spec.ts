import { instance, mock, when } from 'ts-mockito';

import {
    CommonMainOperationWithResult,
    CommonMainOperationWithResultConverter,
} from '../../../../../../src/rpc-data/operations/common/main-operation/common-main-operation-with-result-converter';
import {
    CommonSmartRollupOperation,
    CommonSmartRollupOperationConverter,
    TaquitoRpcCommonSmartRollupOperation,
} from '../../../../../../src/rpc-data/operations/smart-rollup/common/converters/common-smart-rollup-operation-converter';
import {
    SmartRollupConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/common/converters/smart-rollup-converter';
import { RpcConvertOptions } from '../../../../../../src/rpc-data/rpc-convert-options';
import { SmartRollup } from '../../../../../../src/smart-rollups/smart-rollup';
import { convertObject } from '../../../../rpc-test-helpers';
import { setupCommonOpWithResultProperties } from '../../../operation-rpc-test-helpers';

describe(CommonSmartRollupOperationConverter.name, () => {
    let target: CommonSmartRollupOperationConverter;
    let commonOperationConverter: CommonMainOperationWithResultConverter;
    let rollupConverter: SmartRollupConverter;

    let rpcData: TaquitoRpcCommonSmartRollupOperation;
    let options: RpcConvertOptions;
    let commonProperties: CommonMainOperationWithResult;
    let rollup: SmartRollup;

    const act = () => convertObject(target, rpcData, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, rollupConverter] = [mock(), mock()];
        target = new CommonSmartRollupOperationConverter(instance(commonOperationConverter), instance(rollupConverter));

        rpcData = 'mockedRpcData' as any;
        options = 'mockedOptions' as any;
        rollup = 'mockedRollup' as any;

        commonProperties = setupCommonOpWithResultProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(rollupConverter.convertProperty(rpcData, 'rollup', 'op/id')).thenReturn(rollup);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<CommonSmartRollupOperation>({ ...commonProperties, rollup });
    });
});
