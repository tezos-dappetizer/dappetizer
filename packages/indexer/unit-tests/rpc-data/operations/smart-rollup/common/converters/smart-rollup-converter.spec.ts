import { instance, mock, when } from 'ts-mockito';

import {
    SmartRollupConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/common/converters/smart-rollup-converter';
import { SmartRollup } from '../../../../../../src/smart-rollups/smart-rollup';
import { SmartRollupProvider } from '../../../../../../src/smart-rollups/smart-rollup-provider';
import { convertValue } from '../../../../rpc-test-helpers';

describe(SmartRollupConverter.name, () => {
    let target: SmartRollupConverter;
    let rollupProvider: SmartRollupProvider;

    const act = (rpcData: string) => convertValue(target, rpcData, 'res/id');

    beforeEach(() => {
        rollupProvider = mock();
        target = new SmartRollupConverter(instance(rollupProvider));
    });

    it('should convert correctly', () => {
        const rollup: SmartRollup = 'mockedRollup' as any;
        when(rollupProvider.getRollup('sr1LhGA2zC9VcYALSifpRBCgDiQfDSQ6bb4x')).thenReturn(rollup);

        const result = act('sr1LhGA2zC9VcYALSifpRBCgDiQfDSQ6bb4x');

        expect(result).toBe(rollup);
    });
});
