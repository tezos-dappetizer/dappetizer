import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { Writable } from 'ts-essentials';
import { instance, mock, when } from 'ts-mockito';

import {
    CommonSmartRollupOperation,
    CommonSmartRollupOperationConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/common/converters/common-smart-rollup-operation-converter';
import {
    SmartRollupRefutationConverter,
    SmartRollupRefuteOperationConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/refute/converters/smart-rollup-refute-operation-converter';
import {
    SmartRollupRefuteResultConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/refute/converters/smart-rollup-refute-result-converter';
import { RpcConvertOptions } from '../../../../../../src/rpc-data/rpc-convert-options';
import {
    OperationKind,
    SmartRollupRefutation,
    SmartRollupRefutationKind,
    SmartRollupRefutationMove,
    SmartRollupRefutationStart,
    SmartRollupRefuteOperation,
    SmartRollupRefuteResult,
    TaquitoRpcOperation,
    TaquitoRpcSmartRollupRefuteOperation,
} from '../../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../../rpc-test-helpers';
import { setupCommonSmartRollupOpProperties } from '../../../operation-rpc-test-helpers';

describe(SmartRollupRefutationConverter.name, () => {
    let target: SmartRollupRefutationConverter;
    let rpcData: Writable<TaquitoRpcSmartRollupRefuteOperation['refutation']>;

    const act = () => convertObject(target, rpcData, 'op/id');

    beforeEach(() => {
        target = new SmartRollupRefutationConverter();

        rpcData = {
            refutation_kind: '' as any,
            choice: '123',
            opponent_commitment_hash: 'ooo',
            player_commitment_hash: 'ppp',
        } as typeof rpcData;
    });

    it(`should convert '${SmartRollupRefutationKind.Move}' correctly`, () => {
        rpcData.refutation_kind = taquitoRpc.SmartRollupRefutationOptions.MOVE;

        const operation = act();

        expect(operation).toEqual<SmartRollupRefutationMove>({
            choice: new BigNumber(123),
            kind: SmartRollupRefutationKind.Move,
            opponentCommitmentHash: null,
            playerCommitmentHash: null,
        });
        expect(operation).toBeFrozen();
    });

    it(`should convert '${SmartRollupRefutationKind.Start}' correctly`, () => {
        rpcData.refutation_kind = taquitoRpc.SmartRollupRefutationOptions.START;

        const operation = act();

        expect(operation).toEqual<SmartRollupRefutationStart>({
            choice: null,
            kind: SmartRollupRefutationKind.Start,
            opponentCommitmentHash: 'ooo',
            playerCommitmentHash: 'ppp',
        });
        expect(operation).toBeFrozen();
    });
});

describe(SmartRollupRefuteOperationConverter.name, () => {
    let target: SmartRollupRefuteOperationConverter;
    let commonOperationConverter: CommonSmartRollupOperationConverter;
    let resultConverter: SmartRollupRefuteResultConverter;
    let refutationConverter: SmartRollupRefutationConverter;

    let rpcData: TaquitoRpcSmartRollupRefuteOperation;
    let options: RpcConvertOptions;
    let commonProperties: CommonSmartRollupOperation;
    let opResult: SmartRollupRefuteResult;
    let refutation: SmartRollupRefutation;

    const act = () => convertObject(target, rpcData as unknown as TaquitoRpcOperation, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, resultConverter, refutationConverter] = [mock(), mock(), mock()];
        target = new SmartRollupRefuteOperationConverter(
            instance(commonOperationConverter),
            instance(resultConverter),
            instance(refutationConverter),
        );

        rpcData = {
            opponent: 'tz1QD39GBmSzccuDxWMevj2gudiTX1pZL5kC',
            rollup: 'sr1AE6U3GNzE8iKzj6sKS5wh1U32ogeULCoN',
        } as typeof rpcData;
        options = 'mockedOptions' as any;
        opResult = 'mockedResult' as any;
        refutation = 'mockedRefutation' as any;

        commonProperties = setupCommonSmartRollupOpProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(resultConverter.convertFromMetadataOperationResult(rpcData, 'op/id')).thenReturn(opResult as any);
        when(refutationConverter.convertProperty(rpcData, 'refutation', 'op/id')).thenReturn(refutation);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<SmartRollupRefuteOperation>({
            balanceUpdates: commonProperties.balanceUpdates,
            counter: commonProperties.counter,
            fee: commonProperties.fee,
            gasLimit: commonProperties.gasLimit,
            internalOperations: commonProperties.internalOperations,
            isInternal: commonProperties.isInternal,
            kind: OperationKind.SmartRollupRefute,
            opponentAddress: 'tz1QD39GBmSzccuDxWMevj2gudiTX1pZL5kC',
            refutation,
            result: opResult,
            rollup: commonProperties.rollup,
            rpcData,
            sourceAddress: commonProperties.sourceAddress,
            storageLimit: commonProperties.storageLimit,
            uid: commonProperties.uid,
        });
        expect(operation).toBeFrozen();
    });
});
