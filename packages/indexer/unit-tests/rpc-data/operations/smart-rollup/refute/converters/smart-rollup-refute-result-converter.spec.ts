import { instance, mock, when } from 'ts-mockito';

import {
    BalanceUpdateConverter,
} from '../../../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import {
    SmartRollupGameStatusConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/common/converters/smart-rollup-game-status-converter';
import {
    SmartRollupRefuteResultConverter,
    SmartRollupRefuteResultParts,
} from '../../../../../../src/rpc-data/operations/smart-rollup/refute/converters/smart-rollup-refute-result-converter';
import { BalanceUpdate, SmartRollupGameStatus, TaquitoRpcSmartRollupRefuteResult } from '../../../../../../src/rpc-data/rpc-data-interfaces';
import { convertAppliedResultParts } from '../../../../rpc-test-helpers';

describe(SmartRollupRefuteResultConverter, () => {
    let target: SmartRollupRefuteResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;
    let gameStatusConverter: SmartRollupGameStatusConverter;

    let rpcData: TaquitoRpcSmartRollupRefuteResult;
    let balanceUpdates: BalanceUpdate[];
    let gameStatus: SmartRollupGameStatus;

    const act = () => convertAppliedResultParts(target, rpcData, 'res/id');

    beforeEach(() => {
        [balanceUpdateConverter, gameStatusConverter] = [mock(), mock()];
        target = new SmartRollupRefuteResultConverter(null!, instance(balanceUpdateConverter), instance(gameStatusConverter));

        rpcData = 'mockedRpcData' as any;
        balanceUpdates = 'mockedBalanceUpdates' as any;
        gameStatus = 'mockedGameStatus' as any;

        when(balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', 'res/id')).thenReturn(balanceUpdates);
        when(gameStatusConverter.convertProperty(rpcData, 'game_status', 'res/id')).thenReturn(gameStatus);
    });

    it('should convert correctly', () => {
        const result = act();

        expect(result).toEqual<SmartRollupRefuteResultParts>({ balanceUpdates, gameStatus });
    });
});
