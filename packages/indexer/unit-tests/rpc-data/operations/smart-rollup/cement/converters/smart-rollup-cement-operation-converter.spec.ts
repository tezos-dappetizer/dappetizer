import { instance, mock, when } from 'ts-mockito';

import {
    SmartRollupCementOperationConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/cement/converters/smart-rollup-cement-operation-converter';
import {
    SmartRollupCementResultConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/cement/converters/smart-rollup-cement-result-converter';
import {
    CommonSmartRollupOperation,
    CommonSmartRollupOperationConverter,
} from '../../../../../../src/rpc-data/operations/smart-rollup/common/converters/common-smart-rollup-operation-converter';
import { RpcConvertOptions } from '../../../../../../src/rpc-data/rpc-convert-options';
import {
    OperationKind,
    SmartRollupCementOperation,
    SmartRollupCementResult,
    TaquitoRpcOperation,
    TaquitoRpcSmartRollupCementOperation,
} from '../../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../../rpc-test-helpers';
import { setupCommonSmartRollupOpProperties } from '../../../operation-rpc-test-helpers';

describe(SmartRollupCementOperationConverter.name, () => {
    let target: SmartRollupCementOperationConverter;
    let commonOperationConverter: CommonSmartRollupOperationConverter;
    let resultConverter: SmartRollupCementResultConverter;

    let rpcData: TaquitoRpcSmartRollupCementOperation;
    let options: RpcConvertOptions;
    let commonProperties: CommonSmartRollupOperation;
    let opResult: SmartRollupCementResult;

    const act = () => convertObject(target, rpcData as unknown as TaquitoRpcOperation, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, resultConverter] = [mock(), mock()];
        target = new SmartRollupCementOperationConverter(instance(commonOperationConverter), instance(resultConverter));

        rpcData = {} as typeof rpcData;
        options = 'mockedOptions' as any;
        opResult = 'mockedResult' as any;

        commonProperties = setupCommonSmartRollupOpProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(resultConverter.convertFromMetadataOperationResult(rpcData, 'op/id')).thenReturn(opResult);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<SmartRollupCementOperation>({
            balanceUpdates: commonProperties.balanceUpdates,
            counter: commonProperties.counter,
            fee: commonProperties.fee,
            gasLimit: commonProperties.gasLimit,
            internalOperations: commonProperties.internalOperations,
            isInternal: commonProperties.isInternal,
            kind: OperationKind.SmartRollupCement,
            result: opResult,
            rollup: commonProperties.rollup,
            rpcData,
            sourceAddress: commonProperties.sourceAddress,
            storageLimit: commonProperties.storageLimit,
            uid: commonProperties.uid,
        });
        expect(operation).toBeFrozen();
    });
});
