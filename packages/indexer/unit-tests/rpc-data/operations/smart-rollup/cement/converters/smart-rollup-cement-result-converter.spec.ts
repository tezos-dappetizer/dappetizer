import { Writable } from 'ts-essentials';

import {
    SmartRollupCementResultConverter,
    SmartRollupCementResultParts,
} from '../../../../../../src/rpc-data/operations/smart-rollup/cement/converters/smart-rollup-cement-result-converter';
import { TaquitoRpcSmartRollupCementResult } from '../../../../../../src/rpc-data/rpc-data-interfaces';
import { convertAppliedResultParts } from '../../../../rpc-test-helpers';

describe(SmartRollupCementResultConverter, () => {
    let target: SmartRollupCementResultConverter;
    let rpcData: Writable<TaquitoRpcSmartRollupCementResult>;

    const act = () => convertAppliedResultParts(target, rpcData, 'res/id');

    beforeEach(() => {
        target = new SmartRollupCementResultConverter(null!);

        rpcData = {
            commitment_hash: 'haha',
            inbox_level: 123,
        } as typeof rpcData;
    });

    it('should convert correctly', () => {
        const result = act();

        expect(result).toEqual<SmartRollupCementResultParts>({
            commitmentHash: 'haha',
            inboxLevel: 123,
        });
    });
});
