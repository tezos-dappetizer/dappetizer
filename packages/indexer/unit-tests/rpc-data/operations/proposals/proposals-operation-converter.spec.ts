import { asReadonly } from '../../../../../utils/src/public-api';
import { ProposalsOperationConverter } from '../../../../src/rpc-data/operations/proposals/proposals-operation-converter';
import {
    OperationKind,
    ProposalsOperation,
    TaquitoRpcProposalsOperation,
} from '../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../rpc-test-helpers';

describe(ProposalsOperationConverter.name, () => {
    let target: ProposalsOperationConverter;

    let rpcData: TaquitoRpcProposalsOperation;

    const act = () => convertObject(target, rpcData, 'op/id');

    beforeEach(() => {
        target = new ProposalsOperationConverter();

        rpcData = {
            source: 'tz1PMWrbSda1RvsRi8Xq7GRd3P7739jtyaM8',
            period: 59,
            proposals: asReadonly(['PtHangzHogokSuiMHemCuowEavgYTP8J5qQ9fQS793MHYFpCY3r']),
        } as typeof rpcData;
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<ProposalsOperation>({
            kind: OperationKind.Proposals,
            sourceAddress: 'tz1PMWrbSda1RvsRi8Xq7GRd3P7739jtyaM8',
            period: 59,
            proposals: ['PtHangzHogokSuiMHemCuowEavgYTP8J5qQ9fQS793MHYFpCY3r'],
            rpcData,
            uid: 'op/id',
        });
        expect(operation).toBeFrozen();
    });
});
