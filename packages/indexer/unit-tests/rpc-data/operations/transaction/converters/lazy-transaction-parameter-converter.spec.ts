import { DappetizerBug } from '@tezos-dappetizer/utils';
import { StrictOmit, Writable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { nullishInstance, verifyCalled } from '../../../../../../../test-utilities/mocks';
import { Contract } from '../../../../../src/contracts/contract';
import { LazyContract } from '../../../../../src/rpc-data/common/lazy-contract/lazy-contract';
import {
    LazyTransactionParameterConverter,
    UNPARSED_BINARY,
} from '../../../../../src/rpc-data/operations/transaction/converters/lazy-transaction-parameter-converter';
import {
    TransactionParameterFactory,
} from '../../../../../src/rpc-data/operations/transaction/converters/transaction-parameter-factory';
import {
    LazyTransactionParameter,
    TransactionParameter,
} from '../../../../../src/rpc-data/operations/transaction/transaction-parameter';
import { RpcConversionError } from '../../../../../src/rpc-data/rpc-conversion-error';
import {
    OperationResultStatus,
    TaquitoRpcTransactionParameter,
    TransactionResult,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../rpc-test-helpers';

describe(LazyTransactionParameterConverter.name, () => {
    let target: LazyTransactionParameterConverter;
    let parameterFactory: TransactionParameterFactory;

    let rpcData: Writable<TaquitoRpcTransactionParameter>;
    let lazyContract: LazyContract | null;
    let transactionResult: Writable<TransactionResult>;

    const targetConvertObject = () => convertObject(target, rpcData, 'param/id', {
        lazyContract: nullishInstance(lazyContract),
        transactionResult,
    });

    beforeEach(() => {
        parameterFactory = mock();
        target = new LazyTransactionParameterConverter(instance(parameterFactory));

        rpcData = {
            entrypoint: 'foo',
            value: { string: 'abc' },
        };
        lazyContract = mock();
        transactionResult = { status: OperationResultStatus.Applied } as TransactionResult;
    });

    it('should convert correctly', () => {
        const lazyParam = targetConvertObject();

        expect(lazyParam).toMatchObject<StrictOmit<LazyTransactionParameter, 'getTransactionParameter'>>({ rpcData, uid: 'param/id' });
        expect(lazyParam).toBeFrozen();
        verifyCalled(lazyContract!.getContract).never();
        verifyCalled(parameterFactory.create).never();
    });

    it('should return null if no contract', () => {
        lazyContract = null;

        expect(targetConvertObject()).toBeNull();
    });

    it(`should return null if ${UNPARSED_BINARY}`, () => {
        rpcData.value = { [UNPARSED_BINARY]: '070701000000247' } as any;
        transactionResult.status = OperationResultStatus.Failed;

        expect(targetConvertObject()).toBeNull();
    });

    it(`should throw if ${UNPARSED_BINARY} but applied status`, () => {
        rpcData.value = { [UNPARSED_BINARY]: '070701000000247' } as any;

        expect(targetConvertObject).toThrow(DappetizerBug);
    });

    it('should get transaction parameter lazily and cache it', async () => {
        const lazyParam = targetConvertObject();
        const contract = {} as Contract;
        const createdParam = {} as TransactionParameter;
        when(lazyContract!.getContract()).thenResolve(contract);
        when(parameterFactory.create(rpcData, 'param/id', contract)).thenReturn(createdParam);

        const param1 = await lazyParam!.getTransactionParameter();
        const param2 = await lazyParam!.getTransactionParameter();

        expect(param1).toBe(createdParam);
        expect(param2).toBe(createdParam);
        verify(lazyContract!.getContract()).once();
        verify(parameterFactory.create(anything(), anything(), anything())).once();
    });

    it('should throw if invalid rpcData.entrypoint', () => {
        rpcData.entrypoint = 123 as any;

        expect(targetConvertObject).toThrow(RpcConversionError);
    });

    it('should throw if invalid rpcData.value', () => {
        rpcData.value = 'wtf' as any;

        expect(targetConvertObject).toThrow(RpcConversionError);
    });
});
