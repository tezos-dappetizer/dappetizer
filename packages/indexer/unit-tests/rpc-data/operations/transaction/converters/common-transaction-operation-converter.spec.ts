import { BigNumber } from 'bignumber.js';
import { StrictExclude, Writable } from 'ts-essentials';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { verifyCalled } from '../../../../../../../test-utilities/mocks';
import { AddressPrefix } from '../../../../../../utils/src/basics/public-api';
import { LazyContractConverter } from '../../../../../src/rpc-data/common/lazy-contract/lazy-contract-converter';
import {
    CommonTransactionOperation,
    CommonTransactionOperationConverter,
} from '../../../../../src/rpc-data/operations/transaction/converters/common-transaction-operation-converter';
import {
    LazyTransactionParameterConverter,
} from '../../../../../src/rpc-data/operations/transaction/converters/lazy-transaction-parameter-converter';
import {
    TransactionResultConverter,
} from '../../../../../src/rpc-data/operations/transaction/converters/transaction-result-converter';
import { RpcConvertOptions } from '../../../../../src/rpc-data/rpc-convert-options';
import {
    LazyContract,
    LazyTransactionParameter,
    OperationKind,
    OperationResultStatus,
    TaquitoRpcInternalOperation,
    TaquitoRpcTransactionOperation,
    TaquitoRpcTransactionResult,
    TransactionResult,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { mockRpcConvertOptions } from '../../../rpc-test-helpers';

describe(CommonTransactionOperationConverter.name, () => {
    let target: CommonTransactionOperationConverter;
    let resultConverter: TransactionResultConverter;
    let lazyContractConverter: LazyContractConverter;
    let lazyParameterConverter: LazyTransactionParameterConverter;

    let rpcData: Writable<TaquitoRpcTransactionOperation | TaquitoRpcInternalOperation>;
    let options: RpcConvertOptions;
    let rpcResult: TaquitoRpcTransactionResult;
    let mockedLazyContract: LazyContract;
    let transactionParameter: LazyTransactionParameter;
    let transactionResult: Writable<TransactionResult>;

    const act = () => target.convert(rpcData, 'op/id', {
        ...options,
        rpcResult,
        rpcResultProperty: ['result', 'path'],
    });

    beforeEach(() => {
        [resultConverter, lazyContractConverter, lazyParameterConverter] = [mock(), mock(), mock()];
        target = new CommonTransactionOperationConverter(
            instance(resultConverter),
            instance(lazyContractConverter),
            instance(lazyParameterConverter),
        );

        rpcData = {
            amount: '11',
            destination: 'KT191reDVKrLxU9rjTSxg53wRqj6zh8pnHgr',
        } as typeof rpcData;
        options = mockRpcConvertOptions();
        rpcResult = 'mockedResult' as any;
        mockedLazyContract = 'mockedContract' as any;
        transactionParameter = 'mockedEntrypoint' as any;
        transactionResult = { status: OperationResultStatus.Applied, consumedGas: new BigNumber(123) } as TransactionResult;

        when(lazyContractConverter.convertProperty(rpcData, 'destination', 'op/id')).thenReturn(mockedLazyContract);
        when(resultConverter.convert(anything(), anything(), anything())).thenReturn(transactionResult);
        when(lazyParameterConverter.convertNullableProperty(rpcData, 'parameters', 'op/id', anything())).thenReturn(transactionParameter);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<CommonTransactionOperation>({
            amount: new BigNumber(11),
            destination: mockedLazyContract,
            destinationAddress: 'KT191reDVKrLxU9rjTSxg53wRqj6zh8pnHgr',
            destinationContract: mockedLazyContract,
            kind: OperationKind.Transaction,
            transactionParameter,
            result: transactionResult,
        });
        verifyConvertResultAndParameterCalled(mockedLazyContract);
    });

    const notContractAddresses: Record<StrictExclude<AddressPrefix, 'KT1' | 'tz'>, string> = {
        tz1: 'tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj',
        tz2: 'tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL',
        tz3: 'tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm',
        txr1: 'txr1V16e1hXyVKndP4aE8cujRfryoHTiHK9fG',
        sr1: 'sr1NtrMuXaN6RoiJuxTE6uUbBEzfDmoGqccP',
    };
    it.each(Object.values(notContractAddresses))('should convert correctly if not contract address e.g. %s', address => {
        rpcData.destination = address;

        const operation = act();

        expect(operation).toMatchObject<Partial<CommonTransactionOperation>>({
            destination: { address, type: 'User' },
            destinationAddress: address,
            destinationContract: null,
        });
        expect(operation.destination).toBeFrozen();
        verify(lazyContractConverter.convertProperty(anything(), anything(), anything())).never();
        verifyConvertResultAndParameterCalled(null);
    });

    function verifyConvertResultAndParameterCalled(lazyContract: LazyContract | null) {
        verifyCalled(resultConverter.convert).onceWith(rpcResult, 'op/id/result/path', {
            blockHash: options.blockHash,
            predecessorBlockHash: options.predecessorBlockHash,
            lastStorageWithinBlock: options.lastStorageWithinBlock,
            lazyContract,
        });
        verify(lazyParameterConverter.convertNullableProperty(anything(), anything(), anything(), deepEqual({
            transactionResult,
            lazyContract,
        }))).once();
    }
});
