import * as taquitoRpc from '@taquito/rpc';
import { Writable } from 'ts-essentials';
import { deepEqual, instance, mock, when } from 'ts-mockito';

import {
    CommonInternalOperation,
    CommonInternalOperationConverter,
} from '../../../../../src/rpc-data/operations/common/internal-operation/common-internal-operation-converter';
import {
    SmartRollupConverter,
} from '../../../../../src/rpc-data/operations/smart-rollup/common/converters/smart-rollup-converter';
import {
    CommonTransactionOperation,
    CommonTransactionOperationConverter,
} from '../../../../../src/rpc-data/operations/transaction/converters/common-transaction-operation-converter';
import {
    TransactionInternalOperationConverter,
} from '../../../../../src/rpc-data/operations/transaction/converters/transaction-internal-operation-converter';
import { RpcConvertOptions } from '../../../../../src/rpc-data/rpc-convert-options';
import {
    TaquitoRpcInternalOperation,
    TaquitoRpcTransactionResult,
    TransactionInternalOperation,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { SmartRollup } from '../../../../../src/smart-rollups/smart-rollup';
import { convertObject, mockRpcConvertOptions } from '../../../rpc-test-helpers';

describe(TransactionInternalOperationConverter.name, () => {
    let target: TransactionInternalOperationConverter;
    let commonInternalOperationConverter: CommonInternalOperationConverter;
    let commonTransactionOperationConverter: CommonTransactionOperationConverter;
    let smartRollupConverter: SmartRollupConverter;

    let rpcData: Writable<TaquitoRpcInternalOperation>;
    let rpcResult: TaquitoRpcTransactionResult;
    let options: RpcConvertOptions;
    let commonIternalOpProperties: CommonInternalOperation;
    let commonTransactionProperties: Writable<CommonTransactionOperation>;
    let rollup: SmartRollup;

    const act = () => convertObject(target, rpcData, 'op/id', options);

    beforeEach(() => {
        [commonInternalOperationConverter, commonTransactionOperationConverter, smartRollupConverter] = [mock(), mock(), mock()];
        target = new TransactionInternalOperationConverter(
            instance(commonInternalOperationConverter),
            instance(commonTransactionOperationConverter),
            instance(smartRollupConverter),
        );

        rpcResult = 'mockedResult' as any;
        rpcData = { kind: taquitoRpc.OpKind.TRANSACTION, result: rpcResult } as typeof rpcData;
        options = mockRpcConvertOptions();
        commonIternalOpProperties = {
            isInternal: 'mocked-isInternal' as any,
            nonce: 'mocked-nonce' as any,
            source: 'mocked-source' as any,
            sourceAddress: 'mocked-sourceAddress',
            uid: 'mocked-uid',
        } as typeof commonIternalOpProperties;
        commonTransactionProperties = {
            destination: { address: 'tz1WASPbDoCUbD8j3oAgaettVtFs81Ez66UL' },
        } as typeof commonTransactionProperties;
        rollup = 'mockedRollup' as any;

        when(commonInternalOperationConverter.convert(rpcData, 'op/id')).thenReturn(commonIternalOpProperties);
        when(commonTransactionOperationConverter.convert(rpcData, 'op/id', deepEqual({
            blockHash: options.blockHash,
            predecessorBlockHash: options.predecessorBlockHash,
            lastStorageWithinBlock: options.lastStorageWithinBlock,
            rpcResult,
            rpcResultProperty: ['result'],
        }))).thenReturn(commonTransactionProperties);
        when(smartRollupConverter.convertProperty(rpcData, 'destination', 'op/id')).thenReturn(rollup);
    });

    it('should combine properties', () => {
        const operation = act();

        expect(operation).toEqual<TransactionInternalOperation>({
            ...commonIternalOpProperties,
            ...commonTransactionProperties,
        });
        expect(operation).toBeFrozen();
    });

    it('should fail if not transaction', () => {
        rpcData.kind = 'wtf' as any;

        expect(act).toThrow();
    });

    it('should replace source if smart rollup', () => {
        commonTransactionProperties.destination = { address: 'sr1Ghq66tYK9y3r8CC1Tf8i8m5nxh8nTvZEf', type: 'User' };

        const operation = act();

        expect(operation.destination).toBe(rollup);
    });
});
