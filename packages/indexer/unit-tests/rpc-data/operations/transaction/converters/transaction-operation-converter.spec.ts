import { deepEqual, instance, mock, when } from 'ts-mockito';

import {
    CommonMainOperationWithResult,
    CommonMainOperationWithResultConverter,
} from '../../../../../src/rpc-data/operations/common/main-operation/common-main-operation-with-result-converter';
import {
    CommonTransactionOperation,
    CommonTransactionOperationConverter,
} from '../../../../../src/rpc-data/operations/transaction/converters/common-transaction-operation-converter';
import {
    TransactionOperationConverter,
} from '../../../../../src/rpc-data/operations/transaction/converters/transaction-operation-converter';
import { RpcConvertOptions } from '../../../../../src/rpc-data/rpc-convert-options';
import {
    TaquitoRpcTransactionOperation,
    TaquitoRpcTransactionResult,
    TransactionOperation,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject, mockRpcConvertOptions } from '../../../rpc-test-helpers';
import { setupCommonOpWithResultProperties } from '../../operation-rpc-test-helpers';

describe(TransactionOperationConverter.name, () => {
    let target: TransactionOperationConverter;
    let commonOperationConverter: CommonMainOperationWithResultConverter;
    let commonTransactionOperationConverter: CommonTransactionOperationConverter;

    let rpcData: TaquitoRpcTransactionOperation;
    let rpcResult: TaquitoRpcTransactionResult;
    let options: RpcConvertOptions;
    let commonOpProperties: CommonMainOperationWithResult;
    let commonTransactionProperties: CommonTransactionOperation;

    const act = () => convertObject(target, rpcData, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, commonTransactionOperationConverter] = [mock(), mock()];
        target = new TransactionOperationConverter(instance(commonOperationConverter), instance(commonTransactionOperationConverter));

        rpcResult = 'mockedResult' as any;
        rpcData = {
            source: 'sss',
            metadata: { operation_result: rpcResult },
        } as typeof rpcData;
        options = mockRpcConvertOptions();
        commonOpProperties = { sourceAddress: 'mockedSource' } as typeof commonOpProperties;
        commonTransactionProperties = { destinationAddress: 'mockedDestination' } as typeof commonTransactionProperties;

        commonOpProperties = setupCommonOpWithResultProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(commonTransactionOperationConverter.convert(rpcData, 'op/id', deepEqual({
            blockHash: options.blockHash,
            predecessorBlockHash: options.predecessorBlockHash,
            lastStorageWithinBlock: options.lastStorageWithinBlock,
            rpcResult,
            rpcResultProperty: ['metadata', 'operation_result'],
        }))).thenReturn(commonTransactionProperties);
    });

    it('should combine properties', () => {
        const operation = act();

        expect(operation).toEqual<TransactionOperation>({
            ...commonOpProperties,
            ...commonTransactionProperties,
            rpcData,
        });
        expect(operation).toBeFrozen();
    });
});
