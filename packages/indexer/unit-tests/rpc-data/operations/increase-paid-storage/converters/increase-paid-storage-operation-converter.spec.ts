import { BigNumber } from 'bignumber.js';
import { instance, mock, when } from 'ts-mockito';

import {
    CommonMainOperationWithResult,
    CommonMainOperationWithResultConverter,
} from '../../../../../src/rpc-data/operations/common/main-operation/common-main-operation-with-result-converter';
import {
    IncreasePaidStorageOperationConverter,
} from '../../../../../src/rpc-data/operations/increase-paid-storage/converters/increase-paid-storage-operation-converter';
import {
    IncreasePaidStorageResultConverter,
} from '../../../../../src/rpc-data/operations/increase-paid-storage/converters/increase-paid-storage-result-converter';
import { RpcConvertOptions } from '../../../../../src/rpc-data/rpc-convert-options';
import {
    IncreasePaidStorageOperation,
    IncreasePaidStorageResult,
    OperationKind,
    TaquitoRpcIncreasePaidStorageOperation,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../rpc-test-helpers';
import { setupCommonOpWithResultProperties } from '../../operation-rpc-test-helpers';

describe(IncreasePaidStorageOperationConverter.name, () => {
    let target: IncreasePaidStorageOperationConverter;
    let commonOperationConverter: CommonMainOperationWithResultConverter;
    let resultConverter: IncreasePaidStorageResultConverter;

    let rpcData: TaquitoRpcIncreasePaidStorageOperation;
    let options: RpcConvertOptions;
    let opResult: IncreasePaidStorageResult;
    let commonProperties: CommonMainOperationWithResult;

    const act = () => convertObject(target, rpcData, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, resultConverter] = [mock(), mock()];
        target = new IncreasePaidStorageOperationConverter(instance(commonOperationConverter), instance(resultConverter));

        rpcData = {
            amount: '111',
            destination: 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5',
        } as typeof rpcData;
        options = 'mockedOptions' as any;
        opResult = 'mockedResult' as any;

        commonProperties = setupCommonOpWithResultProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(resultConverter.convertFromMetadataOperationResult(rpcData, 'op/id')).thenReturn(opResult);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<IncreasePaidStorageOperation>({
            amount: new BigNumber(111),
            balanceUpdates: commonProperties.balanceUpdates,
            counter: commonProperties.counter,
            destinationAddress: 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5',
            fee: commonProperties.fee,
            gasLimit: commonProperties.gasLimit,
            internalOperations: commonProperties.internalOperations,
            isInternal: commonProperties.isInternal,
            kind: OperationKind.IncreasePaidStorage,
            result: opResult,
            rpcData,
            sourceAddress: commonProperties.sourceAddress,
            storageLimit: commonProperties.storageLimit,
            uid: commonProperties.uid,
        });
        expect(operation).toBeFrozen();
    });
});
