import { instance, mock, when } from 'ts-mockito';

import { asReadonly } from '../../../../../utils/src/basics/public-api';
import { BalanceUpdateConverter } from '../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import {
    VdfRevelationOperationConverter,
} from '../../../../src/rpc-data/operations/vdf-revelation/vdf-revelation-operation-converter';
import {
    BalanceUpdate,
    OperationKind,
    TaquitoRpcVdfRevelationOperation,
    VdfRevelationOperation,
} from '../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../rpc-test-helpers';

describe(VdfRevelationOperationConverter.name, () => {
    let target: VdfRevelationOperationConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;

    let rpcData: TaquitoRpcVdfRevelationOperation;
    let balanceUpdates: BalanceUpdate[];

    const act = () => convertObject(target, rpcData, 'op/id');

    beforeEach(() => {
        balanceUpdateConverter = mock();
        target = new VdfRevelationOperationConverter(instance(balanceUpdateConverter));

        rpcData = { solution: asReadonly(['ss', 'rr']) } as typeof rpcData;
        balanceUpdates = 'mockedBalanceUpdates' as any;

        when(balanceUpdateConverter.convertFromMetadataProperty(rpcData, 'op/id')).thenReturn(balanceUpdates);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<VdfRevelationOperation>({
            balanceUpdates,
            kind: OperationKind.VdfRevelation,
            rpcData,
            solution: ['ss', 'rr'],
            uid: 'op/id',
        });
        expect(operation).toBeFrozen();
    });
});
