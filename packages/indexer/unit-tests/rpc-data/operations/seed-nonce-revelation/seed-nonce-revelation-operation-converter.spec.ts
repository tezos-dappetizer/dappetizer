import { instance, mock, when } from 'ts-mockito';

import { asReadonly } from '../../../../../utils/src/public-api';
import { BalanceUpdateConverter } from '../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import {
    SeedNonceRevelationOperationConverter,
} from '../../../../src/rpc-data/operations/seed-nonce-revelation/seed-nonce-revelation-operation-converter';
import {
    BalanceUpdate,
    OperationKind,
    SeedNonceRevelationOperation,
    TaquitoRpcSeedNonceRevelationOperation,
} from '../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../rpc-test-helpers';

describe(SeedNonceRevelationOperationConverter.name, () => {
    let target: SeedNonceRevelationOperationConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;

    let rpcData: TaquitoRpcSeedNonceRevelationOperation;
    let balanceUpdates: BalanceUpdate[];

    const act = () => convertObject(target, rpcData, 'op/id');

    beforeEach(() => {
        balanceUpdateConverter = mock();
        target = new SeedNonceRevelationOperationConverter(instance(balanceUpdateConverter));

        rpcData = {
            level: 1828928,
            nonce: '18998329ef38a927ffcda36b99e881b6eb04b4ba2578d77490e551283a2970cf',
            metadata: { balance_updates: asReadonly([{}]) },
        } as typeof rpcData;
        balanceUpdates = 'mockedBalanceUpdates' as any;

        when(balanceUpdateConverter.convertFromMetadataProperty(rpcData, 'op/id')).thenReturn(balanceUpdates);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<SeedNonceRevelationOperation>({
            balanceUpdates,
            kind: OperationKind.SeedNonceRevelation,
            level: 1828928,
            nonce: '18998329ef38a927ffcda36b99e881b6eb04b4ba2578d77490e551283a2970cf',
            rpcData,
            uid: 'op/id',
        });
        expect(operation).toBeFrozen();
    });
});
