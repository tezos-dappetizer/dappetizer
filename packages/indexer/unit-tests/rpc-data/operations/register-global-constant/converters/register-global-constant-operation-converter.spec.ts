import { instance, mock, when } from 'ts-mockito';

import {
    CommonMainOperationWithResult,
    CommonMainOperationWithResultConverter,
} from '../../../../../src/rpc-data/operations/common/main-operation/common-main-operation-with-result-converter';
import {
    RegisterGlobalConstantOperationConverter,
} from '../../../../../src/rpc-data/operations/register-global-constant/converters/register-global-constant-operation-converter';
import {
    RegisterGlobalConstantResultConverter,
} from '../../../../../src/rpc-data/operations/register-global-constant/converters/register-global-constant-result-converter';
import { RpcConvertOptions } from '../../../../../src/rpc-data/rpc-convert-options';
import {
    OperationKind,
    RegisterGlobalConstantOperation,
    RegisterGlobalConstantResult,
    TaquitoRpcRegisterGlobalConstantOperation,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../rpc-test-helpers';
import { setupCommonOpWithResultProperties } from '../../operation-rpc-test-helpers';

describe(RegisterGlobalConstantOperationConverter.name, () => {
    let target: RegisterGlobalConstantOperationConverter;
    let commonOperationConverter: CommonMainOperationWithResultConverter;
    let resultConverter: RegisterGlobalConstantResultConverter;

    let rpcData: TaquitoRpcRegisterGlobalConstantOperation;
    let options: RpcConvertOptions;
    let commonProperties: CommonMainOperationWithResult;
    let opResult: RegisterGlobalConstantResult;

    const act = () => convertObject(target, rpcData, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, resultConverter] = [mock(), mock()];
        target = new RegisterGlobalConstantOperationConverter(instance(commonOperationConverter), instance(resultConverter));

        rpcData = { value: { string: 'vvv' } } as typeof rpcData;
        options = 'mockedOptions' as any;
        opResult = 'mockedResult' as any;

        commonProperties = setupCommonOpWithResultProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(resultConverter.convertFromMetadataOperationResult(rpcData, 'op/id')).thenReturn(opResult);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<RegisterGlobalConstantOperation>({
            balanceUpdates: commonProperties.balanceUpdates,
            counter: commonProperties.counter,
            fee: commonProperties.fee,
            gasLimit: commonProperties.gasLimit,
            internalOperations: commonProperties.internalOperations,
            isInternal: commonProperties.isInternal,
            kind: OperationKind.RegisterGlobalConstant,
            result: opResult,
            rpcData,
            sourceAddress: commonProperties.sourceAddress,
            storageLimit: commonProperties.storageLimit,
            value: { string: 'vvv' },
            uid: commonProperties.uid,
        });
        expect(operation).toBeFrozen();
    });
});
