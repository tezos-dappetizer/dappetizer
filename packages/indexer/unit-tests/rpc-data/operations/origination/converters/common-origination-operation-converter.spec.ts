import { BigNumber } from 'bignumber.js';
import { anything, instance, mock, objectContaining, verify, when } from 'ts-mockito';

import {
    CommonOriginationConvertOptions,
    CommonOriginationOperation,
    CommonOriginationOperationConverter,
} from '../../../../../src/rpc-data/operations/origination/converters/common-origination-operation-converter';
import {
    OriginationConvertOptions,
    OriginationResultConverter,
} from '../../../../../src/rpc-data/operations/origination/converters/origination-result-converter';
import { OriginationResult } from '../../../../../src/rpc-data/operations/origination/origination-result';
import {
    OperationKind,
    TaquitoRpcInternalOperation,
    TaquitoRpcOriginationOperation,
    TaquitoRpcOriginationResult,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { mockRpcConvertOptions } from '../../../rpc-test-helpers';

describe(CommonOriginationOperationConverter.name, () => {
    let target: CommonOriginationOperationConverter;
    let resultConverter: OriginationResultConverter;

    let rpcData: TaquitoRpcOriginationOperation | TaquitoRpcInternalOperation;
    let rpcResult: TaquitoRpcOriginationResult;
    let options: CommonOriginationConvertOptions;
    let opResult: OriginationResult;

    const act = () => target.convert(rpcData, 'op/id', options);

    beforeEach(() => {
        resultConverter = mock();
        target = new CommonOriginationOperationConverter(instance(resultConverter));

        rpcResult = {} as TaquitoRpcOriginationResult;
        rpcData = {
            balance: '11',
            delegate: 'tz1NGQWcfuY9WfZmPjwjn2Vy3kbAcNLtFqhQ',
            metadata: { operation_result: rpcResult },
            script: { storage: { prim: 'mockedStorage' } },
        } as typeof rpcData;
        options = {
            ...mockRpcConvertOptions(),
            rpcResult,
            rpcResultProperty: ['res', 'prop'],
        };
        opResult = 'mockedResult' as any;

        when(resultConverter.convert(anything(), anything(), anything())).thenReturn(opResult);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<CommonOriginationOperation>({
            balance: new BigNumber(11),
            delegateAddress: 'tz1NGQWcfuY9WfZmPjwjn2Vy3kbAcNLtFqhQ',
            kind: OperationKind.Origination,
            result: opResult,
        });
        const expectedOptions: OriginationConvertOptions = {
            blockHash: options.blockHash,
            predecessorBlockHash: options.predecessorBlockHash,
            lastStorageWithinBlock: options.lastStorageWithinBlock,
            storageMichelson: { prim: 'mockedStorage' },
        };
        verify(resultConverter.convert(rpcResult, 'op/id/res/prop', objectContaining(expectedOptions))).once();
    });
});
