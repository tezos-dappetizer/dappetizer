import * as taquitoRpc from '@taquito/rpc';
import { Writable } from 'ts-essentials';
import { deepEqual, instance, mock, when } from 'ts-mockito';

import {
    CommonInternalOperation,
    CommonInternalOperationConverter,
} from '../../../../../src/rpc-data/operations/common/internal-operation/common-internal-operation-converter';
import {
    CommonOriginationOperation,
    CommonOriginationOperationConverter,
} from '../../../../../src/rpc-data/operations/origination/converters/common-origination-operation-converter';
import {
    OriginationInternalOperationConverter,
} from '../../../../../src/rpc-data/operations/origination/converters/origination-internal-operation-converter';
import { RpcConvertOptions } from '../../../../../src/rpc-data/rpc-convert-options';
import { OriginationInternalOperation, TaquitoRpcInternalOperation } from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../rpc-test-helpers';

describe(OriginationInternalOperationConverter.name, () => {
    let target: OriginationInternalOperationConverter;
    let commonInternalOperationConverter: CommonInternalOperationConverter;
    let commonOriginationOperationConverter: CommonOriginationOperationConverter;

    let rpcData: Writable<TaquitoRpcInternalOperation>;
    let options: RpcConvertOptions;
    let commonIternalOpProperties: CommonInternalOperation;
    let commonOriginationProperties: CommonOriginationOperation;

    const act = () => convertObject(target, rpcData, 'op/id', options);

    beforeEach(() => {
        [commonInternalOperationConverter, commonOriginationOperationConverter] = [mock(), mock()];
        target = new OriginationInternalOperationConverter(instance(commonInternalOperationConverter), instance(commonOriginationOperationConverter));

        rpcData = { kind: taquitoRpc.OpKind.ORIGINATION, result: {} } as typeof rpcData;
        options = { blockHash: 'haha' } as typeof options;
        commonIternalOpProperties = {
            isInternal: 'mocked-isInternal' as any,
            nonce: 'mocked-nonce' as any,
            source: 'mocked-source' as any,
            sourceAddress: 'mocked-sourceAddress',
            uid: 'mocked-uid',
        } as typeof commonIternalOpProperties;
        commonOriginationProperties = { delegateAddress: 'mockedDestination' } as typeof commonOriginationProperties;

        when(commonInternalOperationConverter.convert(rpcData, 'op/id')).thenReturn(commonIternalOpProperties);
        when(commonOriginationOperationConverter.convert(rpcData, 'op/id', deepEqual({
            ...options,
            rpcResult: rpcData.result,
            rpcResultProperty: ['result'],
        }))).thenReturn(commonOriginationProperties);
    });

    it('should combine properties', () => {
        const operation = act();

        expect(operation).toEqual<OriginationInternalOperation>({
            ...commonIternalOpProperties,
            ...commonOriginationProperties,
        });
        expect(operation).toBeFrozen();
    });

    it('should fail if not origination', () => {
        rpcData.kind = 'wtf' as any;

        expect(act).toThrow();
    });
});
