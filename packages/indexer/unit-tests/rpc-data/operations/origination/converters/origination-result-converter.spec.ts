import { BigNumber } from 'bignumber.js';
import { StrictOmit } from 'ts-essentials';
import { anything, deepEqual, instance, mock, objectContaining, verify, when } from 'ts-mockito';

import { expectToThrow } from '../../../../../../../test-utilities/mocks';
import { Contract } from '../../../../../src/contracts/contract';
import { MichelsonSchema } from '../../../../../src/michelson/michelson-schema';
import {
    BalanceUpdateConverter,
} from '../../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import { BigMapDiffConverter } from '../../../../../src/rpc-data/common/big-map-diff/converters/big-map-diff-converter';
import { LazyContractConverter } from '../../../../../src/rpc-data/common/lazy-contract/lazy-contract-converter';
import { LazyMichelsonValueFactory } from '../../../../../src/rpc-data/common/michelson/lazy-michelson-value-factory';
import {
    OriginationConvertOptions,
    OriginationResultConverter,
    OriginationResultParts,
} from '../../../../../src/rpc-data/operations/origination/converters/origination-result-converter';
import { RpcConversionError } from '../../../../../src/rpc-data/rpc-conversion-error';
import { RpcConvertOptionsWithLazyContract } from '../../../../../src/rpc-data/rpc-convert-options';
import {
    BalanceUpdate,
    LazyBigMapDiff,
    LazyContract,
    LazyMichelsonValue,
    TaquitoRpcOriginationResult,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertAppliedResultParts, mockRpcConvertOptions } from '../../../rpc-test-helpers';

describe(OriginationResultConverter, () => {
    let target: OriginationResultConverter;
    let balanceUpdateConverter: BalanceUpdateConverter;
    let bigMapDiffConverter: BigMapDiffConverter;
    let contractConverter: LazyContractConverter;
    let lazyValueFactory: LazyMichelsonValueFactory;

    let rpcData: TaquitoRpcOriginationResult;
    let options: OriginationConvertOptions;
    let balanceUpdates: BalanceUpdate[];
    let bigMapDiffs: LazyBigMapDiff[];
    let originatedContract: LazyContract;

    const act = () => convertAppliedResultParts(target, rpcData, 'res/id', options);

    beforeEach(() => {
        [balanceUpdateConverter, bigMapDiffConverter, contractConverter, lazyValueFactory] = [mock(), mock(), mock(), mock()];
        target = new OriginationResultConverter(
            null!,
            instance(balanceUpdateConverter),
            instance(bigMapDiffConverter),
            instance(contractConverter),
            instance(lazyValueFactory),
        );

        rpcData = {
            paid_storage_size_diff: '11',
            storage_size: '22',
        } as typeof rpcData;
        options = {
            ...mockRpcConvertOptions(),
            lastStorageWithinBlock: new Map(),
            storageMichelson: { prim: 'mockedStorage' },
        };
        balanceUpdates = 'mockedBalanceUpdates' as any;
        bigMapDiffs = 'mockedBigMapDiffs' as any;
        originatedContract = mock();

        when(contractConverter.convertArrayProperty(rpcData, 'originated_contracts', 'res/id'))
            .thenReturn([instance(originatedContract)]);
        when(originatedContract.address).thenReturn('mockedAddr');
        when(balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', 'res/id')).thenReturn(balanceUpdates);
        const bigMapOptions: RpcConvertOptionsWithLazyContract = {
            blockHash: options.blockHash,
            predecessorBlockHash: options.predecessorBlockHash,
            lastStorageWithinBlock: options.lastStorageWithinBlock,
            lazyContract: instance(originatedContract),
        };
        when(bigMapDiffConverter.convertArrayFrom(rpcData, 'res/id', objectContaining(bigMapOptions))).thenReturn(bigMapDiffs);
    });

    it('should convert correctly', () => {
        const result = act();

        expect(result).toMatchObject<StrictOmit<OriginationResultParts, 'getInitialStorage' | 'originatedContract'>>({
            balanceUpdates,
            bigMapDiffs,
            paidStorageSizeDiff: new BigNumber(11),
            storageSize: new BigNumber(22),
        });
        expect(result.originatedContract).toBe(instance(originatedContract));
        expect(options.lastStorageWithinBlock).toEqual(new Map([['mockedAddr', options.storageMichelson]]));
        verify(originatedContract.getContract()).never();
        verify(lazyValueFactory.create(anything(), anything())).never();
    });

    it('should resolve initial storage correctly', async () => {
        const result = act();
        const lazyValue: LazyMichelsonValue = 'mockedLazy' as any;
        const storageSchema: MichelsonSchema = 'mockedSchema' as any;
        when(lazyValueFactory.create(anything(), anything())).thenReturn(lazyValue);
        when(originatedContract.getContract()).thenResolve({ storageSchema } as Contract);

        const initialStorage = await result.getInitialStorage();

        expect(initialStorage).toBe(lazyValue);
        verify(lazyValueFactory.create(deepEqual({ prim: 'mockedStorage' }), storageSchema)).once();
    });

    it('should throw if no originated contracts', () => {
        runThrowTest({
            originatedContracts: [],
            expectedError: `[]`,
        });
    });

    it('should throw if multiple originated contracts', () => {
        runThrowTest({
            originatedContracts: [
                { address: 'KT1Mh3HZLw6oHCQtF3ardU9ieDGA6Q1Jqcoc' },
                { address: 'KT1MB9a7zzVzi8NBFAUmibN33kpstqkmof4g' },
            ] as LazyContract[],
            expectedError: `['KT1Mh3HZLw6oHCQtF3ardU9ieDGA6Q1Jqcoc', 'KT1MB9a7zzVzi8NBFAUmibN33kpstqkmof4g']`,
        });
    });

    function runThrowTest(testCase: { originatedContracts: LazyContract[]; expectedError: string }) {
        when(contractConverter.convertArrayProperty(anything(), anything(), anything())).thenReturn(testCase.originatedContracts);

        const error = expectToThrow(act, RpcConversionError);

        expect(error.uid).toBe('res/id/originated_contracts');
        expect(error.message).toIncludeMultiple(['must have a single', testCase.expectedError]);
    }
});
