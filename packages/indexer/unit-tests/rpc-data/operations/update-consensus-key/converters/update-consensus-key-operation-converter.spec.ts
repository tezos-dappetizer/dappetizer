import { instance, mock, when } from 'ts-mockito';

import {
    CommonMainOperationWithResult,
    CommonMainOperationWithResultConverter,
} from '../../../../../src/rpc-data/operations/common/main-operation/common-main-operation-with-result-converter';
import {
    UpdateConsensusKeyOperationConverter,
} from '../../../../../src/rpc-data/operations/update-consensus-key/converters/update-consensus-key-operation-converter';
import {
    UpdateConsensusKeyResultConverter,
} from '../../../../../src/rpc-data/operations/update-consensus-key/converters/update-consensus-key-result-converter';
import { RpcConvertOptions } from '../../../../../src/rpc-data/rpc-convert-options';
import {
    OperationKind,
    TaquitoRpcUpdateConsensusKeyOperation,
    UpdateConsensusKeyOperation,
    UpdateConsensusKeyResult,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../rpc-test-helpers';
import { setupCommonOpWithResultProperties } from '../../operation-rpc-test-helpers';

describe(UpdateConsensusKeyOperationConverter.name, () => {
    let target: UpdateConsensusKeyOperationConverter;
    let commonOperationConverter: CommonMainOperationWithResultConverter;
    let resultConverter: UpdateConsensusKeyResultConverter;

    let rpcData: TaquitoRpcUpdateConsensusKeyOperation;
    let options: RpcConvertOptions;
    let commonProperties: CommonMainOperationWithResult;
    let opResult: UpdateConsensusKeyResult;

    const act = () => convertObject(target, rpcData, 'op/id', options);

    beforeEach(() => {
        [commonOperationConverter, resultConverter] = [mock(), mock()];
        target = new UpdateConsensusKeyOperationConverter(instance(commonOperationConverter), instance(resultConverter));

        rpcData = { pk: 'ppp' } as typeof rpcData;
        options = 'mockedOptions' as any;
        opResult = 'mockedResult' as any;

        commonProperties = setupCommonOpWithResultProperties(commonOperationConverter, rpcData, 'op/id', options);
        when(resultConverter.convertFromMetadataOperationResult(rpcData, 'op/id')).thenReturn(opResult);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<UpdateConsensusKeyOperation>({
            balanceUpdates: commonProperties.balanceUpdates,
            counter: commonProperties.counter,
            fee: commonProperties.fee,
            gasLimit: commonProperties.gasLimit,
            internalOperations: commonProperties.internalOperations,
            isInternal: commonProperties.isInternal,
            kind: OperationKind.UpdateConsensusKey,
            pk: 'ppp',
            result: opResult,
            rpcData,
            sourceAddress: commonProperties.sourceAddress,
            storageLimit: commonProperties.storageLimit,
            uid: commonProperties.uid,
        });
        expect(operation).toBeFrozen();
    });
});
