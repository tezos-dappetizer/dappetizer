import * as taquitoRpc from '@taquito/rpc';
import { Writable } from 'ts-essentials';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { MichelsonSchema } from '../../../../../src/michelson/michelson-schema';
import { LazyMichelsonValueFactory } from '../../../../../src/rpc-data/common/michelson/lazy-michelson-value-factory';
import {
    CommonInternalOperation,
    CommonInternalOperationConverter,
} from '../../../../../src/rpc-data/operations/common/internal-operation/common-internal-operation-converter';
import {
    EventInternalOperationConverter,
} from '../../../../../src/rpc-data/operations/event/converters/event-internal-operation-converter';
import { EventResultConverter } from '../../../../../src/rpc-data/operations/event/converters/event-result-converter';
import {
    EventInternalOperation,
    EventResult,
    LazyMichelsonValue,
    OperationKind,
    TaquitoRpcInternalOperation,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../rpc-test-helpers';

describe(EventInternalOperationConverter.name, () => {
    let target: EventInternalOperationConverter;
    let commonInternalOperationConverter: CommonInternalOperationConverter;
    let resultConverter: EventResultConverter;
    let lazyValueFactory: LazyMichelsonValueFactory;

    let rpcData: Writable<TaquitoRpcInternalOperation>;
    let commonProperties: CommonInternalOperation;
    let result: EventResult;
    let payload: LazyMichelsonValue;

    const act = () => convertObject(target, rpcData, 'op/id');

    beforeEach(() => {
        [commonInternalOperationConverter, resultConverter, lazyValueFactory] = [mock(), mock(), mock()];
        target = new EventInternalOperationConverter(
            instance(commonInternalOperationConverter),
            instance(resultConverter),
            instance(lazyValueFactory),
        );

        rpcData = {
            kind: taquitoRpc.OpKind.EVENT,
            delegate: 'tz1NGQWcfuY9WfZmPjwjn2Vy3kbAcNLtFqhQ',
            payload: { string: 'abc' },
            tag: 'ttt',
            type: { prim: 'string' },
        } as typeof rpcData;
        result = 'mockedResult' as any;
        payload = 'mockedPayload' as any;
        commonProperties = {
            isInternal: 'mocked-isInternal' as any,
            nonce: 'mocked-nonce' as any,
            source: 'mocked-source' as any,
            sourceAddress: 'mocked-sourceAddress',
            uid: 'mocked-uid',
        } as typeof commonProperties;

        when(commonInternalOperationConverter.convert(rpcData, 'op/id')).thenReturn(commonProperties);
        when(resultConverter.convertProperty(rpcData, 'result', 'op/id')).thenReturn(result);
        when(lazyValueFactory.create(rpcData.payload!, deepEqual(new MichelsonSchema({ prim: 'string' })))).thenReturn(payload);
    });

    it('should convert correctly', () => {
        const operation = act();

        expect(operation).toEqual<EventInternalOperation>({
            isInternal: commonProperties.isInternal,
            kind: OperationKind.Event,
            nonce: commonProperties.nonce,
            payload,
            result,
            rpcData: commonProperties.rpcData,
            source: commonProperties.source,
            sourceAddress: commonProperties.sourceAddress, // eslint-disable-line deprecation/deprecation
            sourceContract: commonProperties.sourceContract, // eslint-disable-line deprecation/deprecation
            tag: 'ttt',
            typeSchema: new MichelsonSchema({ prim: 'string' }),
            uid: commonProperties.uid,
        });
        expect(operation).toBeFrozen();
    });

    it('should convert correctly if no payload', () => {
        rpcData.payload = undefined;

        const operation = act();

        expect(operation.payload).toBeNull();
        verify(lazyValueFactory.create(anything(), anything())).never();
    });

    it('should fail if not event', () => {
        rpcData.kind = 'wtf' as any;

        expect(act).toThrow();
    });
});
