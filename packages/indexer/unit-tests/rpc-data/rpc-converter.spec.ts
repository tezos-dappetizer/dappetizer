import { anything, spy, verify, when } from 'ts-mockito';

import { describeMemberFactory, expectToThrow } from '../../../../test-utilities/mocks';
import { Nullish } from '../../../utils/src/public-api';
import { RpcConversionError } from '../../src/rpc-data/rpc-conversion-error';
import { appendUidAttribute, joinUid, RpcConverter } from '../../src/rpc-data/rpc-converter';

interface ConvertOptions { aaa: string }

class TargetConverter extends RpcConverter<number, string, ConvertOptions> {
    override convertValue(_rpcData: Nullish<number>, _uid: string, _options: ConvertOptions): string {
        throw new Error('Should be mocked.');
    }
}

describe(RpcConverter.name, () => {
    let target: TargetConverter;
    let targetSpy: TargetConverter;
    let options: ConvertOptions;

    beforeEach(() => {
        target = new TargetConverter();
        targetSpy = spy(target);
        options = { aaa: 'bbb' };
    });

    const describeMember = describeMemberFactory<typeof target>();

    describeMember('convertProperty', () => {
        it('should get property value and convert it', () => {
            const rpcParentData = { numProp: 666 };
            when(targetSpy.convert(anything(), anything(), anything())).thenReturn('abc');

            const result = target.convertProperty(rpcParentData, 'numProp', 'val/id', options);

            expect(result).toBe('abc');
            verify(targetSpy.convert(rpcParentData.numProp, 'val/id/numProp', options)).once();
        });
    });

    describeMember('convert', () => {
        it.each([null, undefined, 123])('should pass input %s to be converted', input => {
            when(targetSpy.convertValue(input, 'val/id', options)).thenReturn('abc');

            const result = target.convert(input, 'val/id', options);

            expect(result).toBe('abc');
        });

        it('should wrap error', () => {
            when(targetSpy.convertValue(123, 'val/id', options)).thenThrow(new Error('oups'));

            runThrowTest({
                act: () => target.convert(123, 'val/id', options),
                expectedUid: 'val/id',
            });
        });
    });

    describeMember('convertNullableProperty', () => {
        it('should get property value and convert it', () => {
            const rpcParentData = { numProp: 666 };
            when(targetSpy.convertNullable(anything(), anything(), anything())).thenReturn('abc');

            const result = target.convertNullableProperty(rpcParentData, 'numProp', 'val/id', options);

            expect(result).toBe('abc');
            verify(targetSpy.convertNullable(rpcParentData.numProp, 'val/id/numProp', options)).once();
        });
    });

    describeMember('convertNullable', () => {
        it('should convert non-nullish value', () => {
            when(targetSpy.convertValue(123, 'val/id', options)).thenReturn('abc');

            const result = target.convertNullable(123, 'val/id', options);

            expect(result).toBe('abc');
        });

        it.each([null, undefined])('should return null without conversion if %s input', input => {
            const result = target.convertNullable(input, 'val/id', options);

            expect(result).toBeNull();
            verify(targetSpy.convertValue(anything(), anything(), anything())).never();
        });

        it('should wrap error', () => {
            when(targetSpy.convertValue(123, 'val/id', options)).thenThrow(new Error('oups'));

            runThrowTest({
                act: () => target.convertNullable(123, 'val/id', options),
                expectedUid: 'val/id',
            });
        });
    });

    describeMember('convertArrayProperty', () => {
        it('should get property value and convert it', () => {
            const rpcParentData = { arrayProp: [66, 77] };
            const convertedItems = ['aa', 'bb'];
            when(targetSpy.convertArray(anything(), anything(), anything())).thenReturn(convertedItems);

            const items = target.convertArrayProperty(rpcParentData, 'arrayProp', 'val/id', options);

            expect(items).toBe(convertedItems);
            verify(targetSpy.convertArray(rpcParentData.arrayProp, 'val/id/arrayProp', options)).once();
        });
    });

    describeMember('convertArray', () => {
        it('should convert items in the array', () => {
            when(targetSpy.convertValue(11, 'val/id/0', options)).thenReturn('aa');
            when(targetSpy.convertValue(22, 'val/id/1', options)).thenReturn('bb');

            const items = target.convertArray([11, 22], 'val/id', options);

            expect(items).toEqual(['aa', 'bb']);
            expect(items).toBeFrozen();
        });

        it.each([null, undefined])('should return empty array if %s input', input => {
            const items = target.convertArray(input, 'val/id', options);

            expect(items).toBeEmpty();
            expect(items).toBeFrozen();
        });

        it('should throw if not an array', () => {
            runThrowTest({
                act: () => target.convertArray('wtf' as any, 'val/id', options),
                expectedUid: 'val/id',
                expectedError: ['must be an array', `is 'string'`],
            });
        });

        it('should wrap item error', () => {
            when(targetSpy.convertValue(11, 'val/id/0', options)).thenReturn('aa');
            when(targetSpy.convertValue(22, 'val/id/1', options)).thenThrow(new Error('oups'));

            runThrowTest({
                act: () => target.convertArray([11, 22], 'val/id', options),
                expectedUid: 'val/id/1',
            });
        });
    });

    function runThrowTest(testCase: { act: () => unknown; expectedUid: string; expectedError?: string[] }) {
        const error = expectToThrow(testCase.act, RpcConversionError);

        expect(error.dataDescription).toIncludeMultiple(testCase.expectedError ?? ['oups']);
        expect(error.uid).toBe(testCase.expectedUid);
    }
});

describe(joinUid.name, () => {
    it('should joind uids correctly', () => {
        const uid = joinUid('parent/id', 'child/id');

        expect(uid).toBe('parent/id/child/id');
    });
});

describe(appendUidAttribute.name, () => {
    it('should append uid correctly', () => {
        const uid = appendUidAttribute('val/id', 'test AttrCasing');

        expect(uid).toBe('val/id[test AttrCasing]');
    });
});
