import { anything, spy, verify, when } from 'ts-mockito';

import { convertValue } from './rpc-test-helpers';
import { expectToThrow } from '../../../../test-utilities/mocks';
import { Nullish } from '../../../utils/src/public-api';
import { RpcConversionError } from '../../src/rpc-data/rpc-conversion-error';
import { RpcObjectConverter } from '../../src/rpc-data/rpc-converter';

class TargetConverter extends RpcObjectConverter<object, object, object> {
    convertObject(_rpcData: object, _uid: string, _options: object): object {
        throw new Error('Should be mocked.');
    }
}

describe(RpcObjectConverter.name, () => {
    let target: TargetConverter;
    let targetSpy: TargetConverter;
    let options: object;

    const act = (d: Nullish<object>) => convertValue(target, d, 'obj/id', options);

    beforeEach(() => {
        target = new TargetConverter();
        targetSpy = spy(target);
        options = {};
    });

    it('should convert object', () => {
        const srcObj = { val: 123 };
        const resObj = { val: 456 };
        when(targetSpy.convertObject(srcObj, 'obj/id', options)).thenReturn(resObj);

        const result = act(srcObj);

        expect(result).toBe(resObj);
    });

    it.each([
        ['undefined', undefined],
        ['null', null],
        ['number', 123],
        ['string', 'abc'],
    ])('should throw if not but object but %s', (desc, input: any) => {
        const error = expectToThrow(() => act(input), RpcConversionError);

        expect(error.dataDescription).toIncludeMultiple(['must be an object', desc]);
        expect(error.uid).toBe('obj/id');
        verify(targetSpy.convertObject(anything(), anything(), anything())).never();
    });
});
