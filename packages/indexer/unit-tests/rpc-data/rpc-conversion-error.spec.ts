import { describeMember } from '../../../../test-utilities/mocks';
import { RpcConversionError } from '../../src/rpc-data/rpc-conversion-error';

describe(RpcConversionError.name, () => {
    describe('constructor', () => {
        it('should include all details in message', () => {
            const target = new RpcConversionError('val/id', 'oups');

            expect(target.message).toIncludeMultiple([`'val/id'`, 'oups']);
        });
    });

    describeMember<typeof RpcConversionError>('wrap', () => {
        it(`should not wrap ${RpcConversionError.name}`, () => {
            class InheritedError extends RpcConversionError {}
            const errorToWrap = new InheritedError('deep/id', 'oups');

            const target = RpcConversionError.wrap(errorToWrap, 'val/id');

            expect(target).toBe(errorToWrap);
        });

        it(`should include entire error if it is not ${RpcConversionError.name}`, () => {
            const errorToWrap = new Error('oups');

            const target = RpcConversionError.wrap(errorToWrap, 'val/id');

            expect(target.uid).toBe('val/id');
            expect(target.dataDescription).toStartWith('Error: oups');
        });
    });
});
