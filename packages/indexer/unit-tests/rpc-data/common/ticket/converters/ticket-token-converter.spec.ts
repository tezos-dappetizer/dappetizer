import { anything, instance, mock, when } from 'ts-mockito';

import { verifyCalled } from '../../../../../../../test-utilities/mocks';
import { MichelsonSchema } from '../../../../../src/michelson/michelson-schema';
import { LazyMichelsonValueFactory } from '../../../../../src/rpc-data/common/michelson/lazy-michelson-value-factory';
import { TicketTokenConverter } from '../../../../../src/rpc-data/common/ticket/converters/ticket-token-converter';
import { TicketTokenImpl } from '../../../../../src/rpc-data/common/ticket/converters/ticket-token-impl';
import { LazyMichelsonValue, Michelson } from '../../../../../src/rpc-data/rpc-data-interfaces';

describe(TicketTokenConverter.name, () => {
    let target: TicketTokenConverter;
    let lazyValueFactory: LazyMichelsonValueFactory;

    let rpcData: {
        fooContent: Michelson;
        fooTicketer: string;
        fooType: Michelson;
    };
    let lazyValue: LazyMichelsonValue;

    beforeEach(() => {
        lazyValueFactory = mock();
        target = new TicketTokenConverter(instance(lazyValueFactory));

        rpcData = {
            fooTicketer: 'KT1JXNcJJPJMV2kVDHPuj9MsPWSWXkMMe2qx',
            fooContent: { string: 'cc' },
            fooType: { prim: 'string' },
        };
        lazyValue = 'mockedValue' as any;

        when(lazyValueFactory.create(anything(), anything())).thenReturn(lazyValue);
    });

    it('should convert correctly', () => {
        const token = target.convertFromProperties(rpcData, 'tck/id', {
            ticketer: 'fooTicketer',
            content: 'fooContent',
            contentType: 'fooType',
        });

        expect(token).toBeInstanceOf(TicketTokenImpl);
        expect(token).toMatchObject<Partial<TicketTokenImpl>>({
            content: lazyValue,
            ticketerAddress: 'KT1JXNcJJPJMV2kVDHPuj9MsPWSWXkMMe2qx',
        });
        verifyCalled(lazyValueFactory.create).onceWith(
            { string: 'cc' },
            new MichelsonSchema({ prim: 'string' }),
        );
    });
});
