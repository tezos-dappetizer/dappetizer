import { deepEqual, instance, mock, when } from 'ts-mockito';

import { TicketTokenConverter } from '../../../../../src/rpc-data/common/ticket/converters/ticket-token-converter';
import { TicketUpdateConverter } from '../../../../../src/rpc-data/common/ticket/converters/ticket-update-converter';
import {
    SingleTicketUpdatesConverter,
    TaquitoRpcTicketUpdates,
    TicketUpdatesConverter,
} from '../../../../../src/rpc-data/common/ticket/converters/ticket-updates-converter';
import { TicketToken, TicketUpdate } from '../../../../../src/rpc-data/rpc-data-interfaces';
import { convertObject } from '../../../rpc-test-helpers';

describe(SingleTicketUpdatesConverter.name, () => {
    let target: SingleTicketUpdatesConverter;
    let tokenConverter: TicketTokenConverter;
    let updateConverter: TicketUpdateConverter;

    beforeEach(() => {
        [tokenConverter, updateConverter] = [mock(), mock()];
        target = new SingleTicketUpdatesConverter(instance(tokenConverter), instance(updateConverter));
    });

    it('should convert correctly', () => {
        const rpcData = { ticket_token: 'mockedToken' as any } as TaquitoRpcTicketUpdates;
        const token: TicketToken = 'mockedToken' as any;
        const convertedUpdates: TicketUpdate[] = 'mockedUpdates' as any;
        when(tokenConverter.convertFromProperties(rpcData.ticket_token, 'tck/id/ticket_token', deepEqual({
            ticketer: 'ticketer',
            content: 'content',
            contentType: 'content_type',
        }))).thenReturn(token);
        when(updateConverter.convertArrayProperty(rpcData, 'updates', 'tck/id', token)).thenReturn(convertedUpdates);

        const updates = convertObject(target, rpcData, 'tck/id');

        expect(updates).toEqual(convertedUpdates);
    });
});

describe(TicketUpdatesConverter.name, () => {
    let target: TicketUpdatesConverter;
    let updatesConverter: SingleTicketUpdatesConverter;

    beforeEach(() => {
        updatesConverter = mock();
        target = new TicketUpdatesConverter(instance(updatesConverter));
    });

    it('should flat converted updates', () => {
        const rpcParentData: { foo_bar: TaquitoRpcTicketUpdates[] } = 'mockedRpcParentData' as any;
        const update1: TicketUpdate = 'mockedUpdate1' as any;
        const update2: TicketUpdate = 'mockedUpdate1' as any;
        const update3: TicketUpdate = 'mockedUpdate1' as any;
        when(updatesConverter.convertArrayProperty(rpcParentData, 'foo_bar', 'par/id')).thenReturn([
            [update1, update2],
            [update3],
        ]);

        const updates = target.convertArrayProperty(rpcParentData, 'foo_bar', 'par/id');

        expect(updates).toEqual([update1, update2, update3]);
        expect(updates).toBeFrozen();
    });
});
