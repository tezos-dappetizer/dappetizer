import { BigNumber } from 'bignumber.js';

import { MichelsonSchema } from '../../../../src/michelson/michelson-schema';
import { LazyMichelsonValueFactory } from '../../../../src/rpc-data/common/michelson/lazy-michelson-value-factory';
import { Michelson } from '../../../../src/rpc-data/rpc-data-interfaces';

describe(LazyMichelsonValueFactory, () => {
    let target: LazyMichelsonValueFactory;
    let michelson: Michelson;
    let schema: MichelsonSchema;

    beforeEach(() => {
        target = new LazyMichelsonValueFactory();

        michelson = {
            prim: 'Pair',
            args: [
                { string: 'tz1NGQWcfuY9WfZmPjwjn2Vy3kbAcNLtFqhQ' },
                { int: '20000000000' },
            ],
        };
        schema = new MichelsonSchema({
            prim: 'pair',
            args: [
                { prim: 'address', annots: [':owner'] },
                { prim: 'nat', annots: [':value'] },
            ],
        });
    });

    it('should be created correctly', () => {
        const lazyValue = target.create(michelson, schema);

        expect(lazyValue).toBeFrozen();
        expect(lazyValue.michelson).toBe(michelson);
        expect(lazyValue.schema).toBe(schema);
    });

    it('should deserialize value correctly', () => {
        const value = target.create(michelson, schema).convert();

        expect(value).toEqual({
            owner: 'tz1NGQWcfuY9WfZmPjwjn2Vy3kbAcNLtFqhQ',
            value: new BigNumber('20000000000'),
        });
    });
});
