import { StrictOmit } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { expectToThrow } from '../../../../../../test-utilities/mocks';
import { Contract, ContractConfig } from '../../../../src/contracts/contract';
import { ContractProvider } from '../../../../src/contracts/contract-provider';
import { SelectiveIndexingConfigResolver } from '../../../../src/helpers/selective-indexing-config-resolver';
import { LazyContractConverter } from '../../../../src/rpc-data/common/lazy-contract/lazy-contract-converter';
import { RpcConversionError } from '../../../../src/rpc-data/rpc-conversion-error';
import { LazyContract } from '../../../../src/rpc-data/rpc-data-interfaces';
import { convertValue } from '../../rpc-test-helpers';

describe(LazyContractConverter.name, () => {
    let target: LazyContractConverter;
    let contractProvider: ContractProvider;
    let configResolver: SelectiveIndexingConfigResolver;
    const testAddress = 'KT1AEfeckNbdEYwaMKkytBwPJPycz7jdSGea';

    const act = (d: unknown) => convertValue(target, d, 'rpc/id');

    beforeEach(() => {
        [contractProvider, configResolver] = [mock(), mock()];
        target = new LazyContractConverter(instance(contractProvider), instance(configResolver));
    });

    it('should convert correctly', () => {
        const config: ContractConfig = 'mockedConfig' as any;
        when(configResolver.resolve(testAddress, 'contracts')).thenReturn(config);

        const lazyContract = act(testAddress);

        expect(lazyContract).toMatchObject<StrictOmit<LazyContract, 'getContract'>>({
            address: testAddress,
            config,
            type: 'Contract',
        });
        expect(lazyContract).toBeFrozen();
        verify(contractProvider.getContract(anything())).never();
    });

    it('should get contract lazily', async () => {
        const lazyContract = act(testAddress);
        const providedContract = {} as Contract;
        when(contractProvider.getContract(testAddress)).thenResolve(providedContract);

        const contract = await lazyContract.getContract();

        expect(contract).toBe(providedContract);
    });

    it('should throw if not contract address', () => {
        const error = expectToThrow(() => act('tz1gfArv665EUkSg2ojMBzcbfwuPxAvqPvjo'));

        expect(error.message).toContain(`supported prefixes: ['KT1']`);
    });

    it('should throw if invalid address', () => {
        expect(() => act(123)).toThrow(RpcConversionError);
    });
});
