import { assert, IsExact } from 'conditional-type-checks';

import { BigMapInfo } from '../../../../src/contracts/data/big-map-info/big-map-info';
import { MichelsonSchema } from '../../../../src/michelson/michelson-schema';
import { BigMapDiff, BigMapDiffAction, LazyMichelsonValue } from '../../../../src/rpc-data/rpc-data-interfaces';

// Type of properties should be narrowed correctly.
const diff: BigMapDiff = {} as any;

assert<IsExact<BigMapDiffAction, typeof diff.action>>(true);
assert<IsExact<BigMapInfo | null, typeof diff.bigMap>>(true);
assert<IsExact<BigMapInfo | null, typeof diff.destinationBigMap>>(true);
assert<IsExact<LazyMichelsonValue | null, typeof diff.key>>(true);
assert<IsExact<string | null, typeof diff.keyHash>>(true);
assert<IsExact<MichelsonSchema | null, typeof diff.keyType>>(true);
assert<IsExact<BigMapInfo | null, typeof diff.sourceBigMap>>(true);
assert<IsExact<LazyMichelsonValue | null, typeof diff.value>>(true);
assert<IsExact<MichelsonSchema | null, typeof diff.valueType>>(true);

switch (diff.action) {
    case BigMapDiffAction.Alloc:
        // Corresponds to RPC schema:
        // { /* alloc */
        //      "action": "alloc",
        //      "big_map": $bignum,
        //      "key_type": $micheline.007-PsDELPH1.michelson_v1.expression,
        //      "value_type": $micheline.007-PsDELPH1.michelson_v1.expression }
        assert<IsExact<BigMapInfo | null, typeof diff.bigMap>>(true);
        assert<IsExact<null, typeof diff.destinationBigMap>>(true);
        assert<IsExact<null, typeof diff.key>>(true);
        assert<IsExact<null, typeof diff.keyHash>>(true);
        assert<IsExact<MichelsonSchema, typeof diff.keyType>>(true);
        assert<IsExact<null, typeof diff.sourceBigMap>>(true);
        assert<IsExact<null, typeof diff.value>>(true);
        assert<IsExact<MichelsonSchema, typeof diff.valueType>>(true);
        break;

    case BigMapDiffAction.Copy:
        // Corresponds to RPC schema:
        // { /* alloc */
        //      "action": "alloc",
        //      "big_map": $bignum,
        //      "key_type": $micheline.007-PsDELPH1.michelson_v1.expression,
        //      "value_type": $micheline.007-PsDELPH1.michelson_v1.expression }
        assert<IsExact<null, typeof diff.bigMap>>(true);
        assert<IsExact<BigMapInfo | null, typeof diff.destinationBigMap>>(true);
        assert<IsExact<null, typeof diff.key>>(true);
        assert<IsExact<null, typeof diff.keyHash>>(true);
        assert<IsExact<null, typeof diff.keyType>>(true);
        assert<IsExact<BigMapInfo | null, typeof diff.sourceBigMap>>(true);
        assert<IsExact<null, typeof diff.value>>(true);
        assert<IsExact<null, typeof diff.valueType>>(true);
        break;

    case BigMapDiffAction.Remove:
        // Corresponds to RPC schema:
        // { /* remove */
        //      "action": "remove",
        //      "big_map": $bignum }
        assert<IsExact<BigMapInfo | null, typeof diff.bigMap>>(true);
        assert<IsExact<null, typeof diff.destinationBigMap>>(true);
        assert<IsExact<null, typeof diff.key>>(true);
        assert<IsExact<null, typeof diff.keyHash>>(true);
        assert<IsExact<null, typeof diff.keyType>>(true);
        assert<IsExact<null, typeof diff.sourceBigMap>>(true);
        assert<IsExact<null, typeof diff.value>>(true);
        assert<IsExact<null, typeof diff.valueType>>(true);
        break;

    case BigMapDiffAction.Update:
        // Corresponds to RPC schema:
        // { /* update */
        //      "action": "update",
        //      "big_map": $bignum,
        //      "key_hash": $script_expr,
        //      "key": $micheline.007-PsDELPH1.michelson_v1.expression,
        //      "value"?: $micheline.007-PsDELPH1.michelson_v1.expression }
        assert<IsExact<BigMapInfo | null, typeof diff.bigMap>>(true);
        assert<IsExact<null, typeof diff.destinationBigMap>>(true);
        assert<IsExact<LazyMichelsonValue | null, typeof diff.key>>(true);
        assert<IsExact<string, typeof diff.keyHash>>(true);
        assert<IsExact<null, typeof diff.keyType>>(true);
        assert<IsExact<null, typeof diff.sourceBigMap>>(true);
        assert<IsExact<LazyMichelsonValue | null, typeof diff.value>>(true);
        assert<IsExact<null, typeof diff.valueType>>(true);
}
