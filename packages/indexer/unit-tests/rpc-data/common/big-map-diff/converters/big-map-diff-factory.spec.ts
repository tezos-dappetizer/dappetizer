import { BigNumber } from 'bignumber.js';
import { StrictOmit, Writable } from 'ts-essentials';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { describeMemberFactory } from '../../../../../../../test-utilities/mocks';
import { BigMapInfo } from '../../../../../src/contracts/data/big-map-info/big-map-info';
import { BigMapInfoResolver } from '../../../../../src/contracts/data/big-map-info/big-map-info-resolver';
import { MichelsonSchema } from '../../../../../src/michelson/michelson-schema';
import {
    BigMapDiffFactory,
    DiffCreateOptions,
    UpdateCreateOptions,
} from '../../../../../src/rpc-data/common/big-map-diff/converters/big-map-diff-factory';
import { LazyMichelsonValueFactory } from '../../../../../src/rpc-data/common/michelson/lazy-michelson-value-factory';
import {
    BigMapDiff,
    BigMapDiffAction,
    BigMapUpdate,
    LazyBigMapDiff,
    LazyContract,
    LazyMichelsonValue,
    TaquitoRpcBigMapDiff,
} from '../../../../../src/rpc-data/rpc-data-interfaces';

describe(BigMapDiffFactory.name, () => {
    let target: BigMapDiffFactory;
    let bigMapResolver: BigMapInfoResolver;
    let lazyValueFactory: LazyMichelsonValueFactory;

    let baseOptions: DiffCreateOptions;
    let baseExpected: Pick<BigMapDiff, 'rpcData' | 'uid'>;
    let blockHash: string;
    let rpcData: TaquitoRpcBigMapDiff;
    let contract: LazyContract;

    beforeEach(() => {
        [bigMapResolver, lazyValueFactory] = [mock(), mock()];
        target = new BigMapDiffFactory(instance(bigMapResolver), instance(lazyValueFactory));

        blockHash = `haha${Date.now()}`;
        contract = 'mockedContract' as any;
        rpcData = 'mockedRpcData' as any;
        baseOptions = {
            blockHash,
            contract,
            rpcData,
            uid: 'diff/id',
        };
        baseExpected = {
            rpcData,
            uid: 'diff/id',
        };
    });

    const describeMember = describeMemberFactory<typeof target>();

    describeMember('createAlloc', () => {
        it('should create correctly', async () => {
            const bigMap = setupBigMapResolver(11);
            const keyType = new MichelsonSchema({ prim: 'string' });
            const valueType = new MichelsonSchema({ prim: 'int' });

            const diff = target.createAlloc({
                ...baseOptions,
                bigMapId: bigMap.lastKnownId,
                keyType,
                valueType,
            });

            await verifyDiff(diff, {
                ...baseExpected,
                action: BigMapDiffAction.Alloc,
                bigMap,
                keyType,
                valueType,

                destinationBigMap: null,
                key: null,
                keyHash: null,
                sourceBigMap: null,
                value: null,
            });
        });
    });

    describeMember('createCopy', () => {
        it('should create correctly', async () => {
            const sourceBigMap = setupBigMapResolver(21);
            const destinationBigMap = setupBigMapResolver(22);

            const diff = target.createCopy({
                ...baseOptions,
                destinationBigMapId: destinationBigMap.lastKnownId,
                sourceBigMapId: sourceBigMap.lastKnownId,
            });

            await verifyDiff(diff, {
                ...baseExpected,
                action: BigMapDiffAction.Copy,
                destinationBigMap,
                sourceBigMap,

                bigMap: null,
                key: null,
                keyHash: null,
                keyType: null,
                value: null,
                valueType: null,
            });
        });
    });

    describeMember('createRemove', () => {
        it('should create correctly', async () => {
            const bigMap = setupBigMapResolver(31);

            const diff = target.createRemove({
                ...baseOptions,
                bigMapId: bigMap.lastKnownId,
            });

            await verifyDiff(diff, {
                ...baseExpected,
                action: BigMapDiffAction.Remove,
                bigMap,

                destinationBigMap: null,
                key: null,
                keyHash: null,
                keyType: null,
                sourceBigMap: null,
                value: null,
                valueType: null,
            });
        });
    });

    describeMember('createUpdate', () => {
        let bigMap: BigMapInfo;
        let options: Writable<UpdateCreateOptions>;
        let lazyKey: LazyMichelsonValue;
        let lazyValue: LazyMichelsonValue;

        const act = () => target.createUpdate(options);

        beforeEach(() => {
            bigMap = setupBigMapResolver(41);
            options = {
                ...baseOptions,
                bigMapId: bigMap.lastKnownId,
                keyHash: 'kk-haha',
                keyMichelson: { string: 'kk' },
                valueMichelson: { string: 'vv' },
            };
            lazyKey = 'mockedKey' as any;
            lazyValue = 'mockedValue' as any;

            when(lazyValueFactory.create(options.keyMichelson, bigMap.keySchema)).thenReturn(lazyKey);
            when(lazyValueFactory.create(options.valueMichelson!, bigMap.valueSchema)).thenReturn(lazyValue);
        });

        it('should create correctly', async () => {
            const diff = act();

            await verifyDiff(diff, {
                ...baseExpected,
                action: BigMapDiffAction.Update,
                bigMap,
                key: lazyKey,
                keyHash: 'kk-haha',
                value: lazyValue,

                destinationBigMap: null,
                keyType: null,
                sourceBigMap: null,
                valueType: null,
            });
        });

        it.each([undefined, null])(`should support 'value' equal to %s`, async value => {
            options.valueMichelson = value!;

            const diff = (await act().getBigMapDiff()) as BigMapUpdate;

            expect(diff.value).toBeNull();
            verify(lazyValueFactory.create(anything(), anything())).once();
        });

        it('should deserialize null key and value if null big map', async () => {
            when(bigMapResolver.resolve(deepEqual(options.bigMapId), anything(), anything())).thenResolve(null);

            const diff = (await act().getBigMapDiff()) as BigMapUpdate;

            expect(diff.key).toBeNull();
            expect(diff.value).toBeNull();
            verify(lazyValueFactory.create(anything(), anything())).never();
        });
    });

    async function verifyDiff(actualLazyDiff: LazyBigMapDiff, expectedDiff: BigMapDiff) {
        expect(actualLazyDiff).toMatchObject<StrictOmit<LazyBigMapDiff, 'getBigMapDiff'>>({
            action: expectedDiff.action,
            rpcData,
            uid: expectedDiff.uid,
        });
        expect(actualLazyDiff).toBeFrozen();
        verify(bigMapResolver.resolve(anything(), anything(), anything())).never();

        const actualDiff = await actualLazyDiff.getBigMapDiff();

        expect(actualDiff).toEqual(expectedDiff);
        expect(actualDiff).toBeFrozen();
    }

    function setupBigMapResolver(id: number) {
        const mockedMap = {
            name: `mockedBigMap${id}`,
            lastKnownId: new BigNumber(id),
            keySchema: 'key-schema' as any,
            valueSchema: 'val-schema' as any,
        } as BigMapInfo;

        when(bigMapResolver.resolve(deepEqual(new BigNumber(id)), contract, blockHash)).thenResolve(mockedMap);
        return mockedMap;
    }
});
