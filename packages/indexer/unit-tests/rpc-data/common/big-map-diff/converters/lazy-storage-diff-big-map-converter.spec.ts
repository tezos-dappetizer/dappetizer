/* eslint-disable no-return-assign */
import * as taquitoRpc from '@taquito/rpc';
import { DappetizerBug } from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';
import { StrictOmit, Writable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { expectToThrow, verifyCalled } from '../../../../../../../test-utilities/mocks';
import { typed } from '../../../../../../utils/src/public-api';
import { MichelsonSchema } from '../../../../../src/michelson/michelson-schema';
import {
    BigMapDiffFactory,
    DiffCreateOptions,
} from '../../../../../src/rpc-data/common/big-map-diff/converters/big-map-diff-factory';
import {
    LazyStorageDiffBigMapConverter,
    TaquitoRpcLazyStorageDiff,
} from '../../../../../src/rpc-data/common/big-map-diff/converters/lazy-storage-diff-big-map-converter';
import { RpcConversionError } from '../../../../../src/rpc-data/rpc-conversion-error';
import {
    RpcConvertOptionsWithLazyContract,
} from '../../../../../src/rpc-data/rpc-convert-options';
import { BigMapDiffAction, LazyBigMapDiff, LazyContract } from '../../../../../src/rpc-data/rpc-data-interfaces';
import { RpcSkippableConversionError } from '../../../../../src/rpc-data/rpc-skippable-array-converter';
import { convertValue, mockRpcConvertOptions } from '../../../rpc-test-helpers';

describe(LazyStorageDiffBigMapConverter.name, () => {
    let target: LazyStorageDiffBigMapConverter;
    let diffFactory: BigMapDiffFactory;

    let rpcData: Writable<TaquitoRpcLazyStorageDiff>;
    let rpcDataDiff: Writable<taquitoRpc.LazyStorageDiffBigMapItems>;
    let rpcDataDiffUpdate: Writable<taquitoRpc.LazyStorageDiffUpdatesBigMap>;

    let options: Writable<RpcConvertOptionsWithLazyContract>;
    let contract: LazyContract;
    let baseExpectedOptions: StrictOmit<DiffCreateOptions, 'uid'>;

    const act = () => convertValue(target, rpcData, 'dd', options);

    beforeEach(() => {
        diffFactory = mock();
        target = new LazyStorageDiffBigMapConverter(instance(diffFactory));

        rpcDataDiffUpdate = {
            key: { int: '100' },
            key_hash: 'kk-haha',
            value: { int: '200' },
        };
        rpcDataDiff = {
            action: 'bullshit' as any,
            key_type: { prim: 'string' },
            source: '22',
            updates: [rpcDataDiffUpdate],
            value_type: { prim: 'int' },
        };
        rpcData = {
            kind: 'big_map',
            id: '11',
            diff: rpcDataDiff,
        };
        contract = 'mockedContract' as any;
        options = { ...mockRpcConvertOptions(), lazyContract: contract };
        baseExpectedOptions = { blockHash: options.blockHash, contract, rpcData };
    });

    it(`should skip conversion if 'kind' is not 'big_map'`, () => {
        rpcData.kind = 'sapling_state';

        const diffs = act();

        expect(diffs).toBeEmpty();
        verify(diffFactory.createAlloc(anything())).never();
        verify(diffFactory.createCopy(anything())).never();
        verify(diffFactory.createRemove(anything())).never();
        verify(diffFactory.createUpdate(anything())).never();
    });

    shouldThrowIfInvalidProperty('diff.action', {
        setupValue: v => rpcDataDiff.action = v,
        expectedUid: 'dd/diff/action',
    });

    it(`should skip if 'diff.action' is unsupported`, () => {
        rpcDataDiff.action = 'unsupported' as any;

        const error = expectToThrow(act, RpcSkippableConversionError);

        expect(error.uid).toBe('dd/diff/action');
    });

    it.each([null, undefined])('should throw if contract is %s', lazyContract => {
        options.lazyContract = lazyContract!;

        expectToThrow(act, DappetizerBug);
    });

    const testedActions = {
        alloc: 'alloc',
        copy: 'copy',
        remove: 'remove',
        update: 'update',
    } as const;

    describe(BigMapDiffAction.Alloc, () => {
        beforeEach(() => {
            rpcDataDiff.action = testedActions.alloc;
        });

        it('should convert RPC data and create diff with updates correctly ', () => {
            testConversion(diffFactory.createAlloc);

            verifyCalled(diffFactory.createAlloc).onceWith({
                ...baseExpectedOptions,
                bigMapId: new BigNumber(11),
                keyType: new MichelsonSchema({ prim: 'string' }),
                valueType: new MichelsonSchema({ prim: 'int' }),
                uid: 'dd[alloc]',
            });
            verifyCreateUpdateCalled({ uid: 'dd[alloc]/diff/updates/0' });
        });

        shouldThrowIfInvalidProperty('id', {
            setupValue: v => rpcData.id = v,
            expectedUid: 'dd[alloc]/id',
        });
        shouldThrowIfInvalidProperty('diff.key_type', {
            setupValue: v => rpcDataDiff.key_type = v,
            expectedUid: 'dd[alloc]/diff/key_type',
        });
        shouldThrowIfInvalidProperty('diff.value_type', {
            setupValue: v => rpcDataDiff.value_type = v,
            expectedUid: 'dd[alloc]/diff/value_type',
        });
    });

    describe(BigMapDiffAction.Copy, () => {
        beforeEach(() => {
            rpcDataDiff.action = testedActions.copy;
        });

        it('should convert RPC data and create diff with updates correctly ', () => {
            testConversion(diffFactory.createCopy);

            verifyCalled(diffFactory.createCopy).onceWith({
                ...baseExpectedOptions,
                destinationBigMapId: new BigNumber(11),
                sourceBigMapId: new BigNumber(22),
                uid: 'dd[copy]',
            });
            verifyCreateUpdateCalled({ uid: 'dd[copy]/diff/updates/0' });
        });

        shouldThrowIfInvalidProperty('id', {
            setupValue: v => rpcData.id = v,
            expectedUid: 'dd[copy]/id',
        });
        shouldThrowIfInvalidProperty('diff.source', {
            setupValue: v => rpcDataDiff.source = v,
            expectedUid: 'dd[copy]/diff/source',
        });
    });

    describe(BigMapDiffAction.Remove, () => {
        beforeEach(() => {
            rpcDataDiff.action = testedActions.remove;
        });

        it('should convert RPC data and create diff correctly ', () => {
            when(diffFactory.createRemove(anything())).thenReturn('mockedLazyDiff' as any);

            const diffs = act();

            expect(diffs).toEqual(['mockedLazyDiff']);
            verifyCalled(diffFactory.createRemove).onceWith({
                ...baseExpectedOptions,
                bigMapId: new BigNumber(11),
                uid: 'dd[remove]',
            });
            verify(diffFactory.createUpdate(anything())).never();
        });

        shouldThrowIfInvalidProperty('id', {
            setupValue: v => rpcData.id = v,
            expectedUid: 'dd[remove]/id',
        });
    });

    describe(BigMapDiffAction.Update, () => {
        beforeEach(() => {
            rpcDataDiff.action = testedActions.update;
        });

        it('should convert RPC data and create diff correctly ', () => {
            when(diffFactory.createUpdate(anything())).thenReturn('mockedLazyDiff' as any);

            const diffs = act();

            expect(diffs).toEqual(['mockedLazyDiff']);
            verifyCreateUpdateCalled({ uid: 'dd[update]/diff/updates/0' });
        });

        shouldThrowIfInvalidProperty('id', {
            setupValue: v => rpcData.id = v,
            expectedUid: 'dd[update]/id',
        });
        shouldThrowIfInvalidProperty('diff.updates[].key', {
            setupValue: v => rpcDataDiffUpdate.key = v,
            expectedUid: 'dd[update]/diff/updates/0/key',
        });
        shouldThrowIfInvalidProperty('diff.updates[].key_hash', {
            setupValue: v => rpcDataDiffUpdate.key_hash = v,
            expectedUid: 'dd[update]/diff/updates/0/key_hash',
        });
        shouldThrowIfInvalidProperty('diff.updates[].value', {
            setupValue: v => rpcDataDiffUpdate.value = v,
            allowNullish: true,
            expectedUid: 'dd[update]/diff/updates/0/value',
        });
    });

    it('should test all actions by not compiling if all not tested', () => {
        typed<Record<taquitoRpc.DiffActionEnum, string>>(testedActions);
    });

    function testConversion(factoryMethod: (o: any) => LazyBigMapDiff) {
        when(factoryMethod(anything())).thenReturn('mockedLazyDiff1' as any);
        when(diffFactory.createUpdate(anything())).thenReturn('mockedLazyDiff2' as any);

        const diffs = act();

        expect(diffs).toEqual(['mockedLazyDiff1', 'mockedLazyDiff2']);
    }

    function verifyCreateUpdateCalled(expected: { uid: string }) {
        verifyCalled(diffFactory.createUpdate).onceWith({
            ...baseExpectedOptions,
            bigMapId: new BigNumber(11),
            keyHash: 'kk-haha',
            keyMichelson: { int: '100' },
            valueMichelson: { int: '200' },
            uid: expected.uid,
        });
    }

    function shouldThrowIfInvalidProperty(propertyDesc: string, testCase: {
        setupValue: (v: any) => void;
        expectedUid: string;
        allowNullish?: boolean;
    }) {
        it.each([
            ...testCase.allowNullish !== true ? [undefined, null] : [],
            { invalid: 123 },
        ])(`should throw if '${propertyDesc}' is %s`, (value: any) => {
            testCase.setupValue(value);

            const error = expectToThrow(act, RpcConversionError);

            expect(error.uid).toBe(testCase.expectedUid);
        });
    }
});
