import * as taquitoRpc from '@taquito/rpc';
import { DappetizerBug } from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';
import { StrictOmit, Writable } from 'ts-essentials';
import { anything, capture, instance, mock, when } from 'ts-mockito';

import { expectToThrow, verifyCalled } from '../../../../../../../test-utilities/mocks';
import { typed } from '../../../../../../utils/src/public-api';
import { MichelsonSchema } from '../../../../../src/michelson/michelson-schema';
import {
    BigMapDiffFactory,
    DiffCreateOptions,
} from '../../../../../src/rpc-data/common/big-map-diff/converters/big-map-diff-factory';
import {
    LegacyBigMapDiffConverter,
} from '../../../../../src/rpc-data/common/big-map-diff/converters/legacy-big-map-diff-converter';
import { RpcConversionError } from '../../../../../src/rpc-data/rpc-conversion-error';
import { RpcConvertOptionsWithLazyContract } from '../../../../../src/rpc-data/rpc-convert-options';
import {
    BigMapDiffAction,
    LazyBigMapDiff,
    TaquitoRpcLegacyBigMapDiff,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { RpcSkippableConversionError } from '../../../../../src/rpc-data/rpc-skippable-array-converter';
import { convertValue, mockRpcConvertOptions } from '../../../rpc-test-helpers';

describe(LegacyBigMapDiffConverter.name, () => {
    let target: LegacyBigMapDiffConverter;
    let diffFactory: BigMapDiffFactory;

    let rpcData: Writable<TaquitoRpcLegacyBigMapDiff>;
    let options: Writable<RpcConvertOptionsWithLazyContract>;
    let baseExpectedOptions: StrictOmit<DiffCreateOptions, 'uid'>;

    const act = () => convertValue(target, rpcData, 'dd', options);

    beforeEach(() => {
        diffFactory = mock();
        target = new LegacyBigMapDiffConverter(instance(diffFactory));

        rpcData = {
            big_map: '11',
            source_big_map: '22',
            destination_big_map: '33',
            key: { int: '100' },
            key_type: { prim: 'string' },
            key_hash: 'kk-haha',
            value: { int: '200' },
            value_type: { prim: 'int' },
        };
        options = {
            ...mockRpcConvertOptions(),
            lazyContract: 'mockedContract' as any,
        };
        baseExpectedOptions = {
            rpcData,
            contract: options.lazyContract!,
            blockHash: options.blockHash,
        };
    });

    shouldThrowIfInvalidProperty('action', { expectedUid: 'dd/action' });

    it(`should skip if 'action' is unsupported`, () => {
        rpcData.action = 'unsupported' as any;

        const error = expectToThrow(act, RpcSkippableConversionError);

        expect(error.uid).toBe('dd/action');
    });

    it.each([null, undefined])('should throw if contract is %s', lazyContract => {
        options.lazyContract = lazyContract!;

        expectToThrow(act, DappetizerBug);
    });

    const testedActions = {
        alloc: 'alloc',
        copy: 'copy',
        remove: 'remove',
        update: 'update',
    } as const;

    describe(BigMapDiffAction.Alloc, () => {
        beforeEach(() => {
            rpcData.action = testedActions.alloc;
        });

        shouldConvertAndCreateDiffCorrectly(() => ({
            factoryMethod: diffFactory.createAlloc,
            expectedOptions: {
                ...baseExpectedOptions,
                bigMapId: new BigNumber(11),
                keyType: new MichelsonSchema({ prim: 'string' }),
                uid: 'dd[alloc]',
                valueType: new MichelsonSchema({ prim: 'int' }),
            },
        }));

        shouldThrowIfInvalidProperty('big_map', { expectedUid: 'dd[alloc]/big_map' });
        shouldThrowIfInvalidProperty('key_type', { expectedUid: 'dd[alloc]/key_type' });
        shouldThrowIfInvalidProperty('value_type', { expectedUid: 'dd[alloc]/value_type' });
    });

    describe(BigMapDiffAction.Copy, () => {
        beforeEach(() => {
            rpcData.action = testedActions.copy;
        });

        shouldConvertAndCreateDiffCorrectly(() => ({
            factoryMethod: diffFactory.createCopy,
            expectedOptions: {
                ...baseExpectedOptions,
                destinationBigMapId: new BigNumber(33),
                sourceBigMapId: new BigNumber(22),
                uid: 'dd[copy]',
            },
        }));

        shouldThrowIfInvalidProperty('destination_big_map', { expectedUid: 'dd[copy]/destination_big_map' });
        shouldThrowIfInvalidProperty('source_big_map', { expectedUid: 'dd[copy]/source_big_map' });
    });

    describe(BigMapDiffAction.Remove, () => {
        beforeEach(() => {
            rpcData.action = testedActions.remove;
        });

        shouldConvertAndCreateDiffCorrectly(() => ({
            factoryMethod: diffFactory.createRemove,
            expectedOptions: {
                ...baseExpectedOptions,
                bigMapId: new BigNumber(11),
                uid: 'dd[remove]',
            },
        }));

        shouldThrowIfInvalidProperty('big_map', { expectedUid: 'dd[remove]/big_map' });
    });

    describe(BigMapDiffAction.Update, () => {
        beforeEach(() => {
            rpcData.action = testedActions.update;
        });

        shouldConvertAndCreateDiffCorrectly(() => ({
            factoryMethod: diffFactory.createUpdate,
            expectedOptions: {
                ...baseExpectedOptions,
                bigMapId: new BigNumber(11),
                keyMichelson: { int: '100' },
                keyHash: 'kk-haha',
                uid: 'dd[update]',
                valueMichelson: { int: '200' },
            },
        }));

        it.each([undefined, null])(`should support 'value' = %s`, rpcValue => {
            rpcData.value = rpcValue!;

            act();

            const received = capture(diffFactory.createUpdate).first()[0];
            expect(received).toMatchObject<Partial<typeof received>>({ valueMichelson: null });
        });

        shouldThrowIfInvalidProperty('big_map', { expectedUid: 'dd[update]/big_map' });
        shouldThrowIfInvalidProperty('key', { expectedUid: 'dd[update]/key' });
        shouldThrowIfInvalidProperty('key_hash', { expectedUid: 'dd[update]/key_hash' });
        shouldThrowIfInvalidProperty('value', { expectedUid: 'dd[update]/value', allowNullish: true });
    });

    it('should test all actions by not compiling if all not tested', () => {
        typed<Record<taquitoRpc.DiffActionEnum, string>>(testedActions);
    });

    function shouldConvertAndCreateDiffCorrectly<TOptions>(getTestCase: () => {
        factoryMethod: (o: TOptions) => LazyBigMapDiff;
        expectedOptions: TOptions;
    }) {
        it('should convert RPC data and create diff correctly ', () => {
            const testCase = getTestCase();
            when(testCase.factoryMethod(anything())).thenReturn('mockedLazyDiff' as any);

            const diff = act();

            expect(diff).toBe('mockedLazyDiff');
            verifyCalled(testCase.factoryMethod).onceWith(testCase.expectedOptions);
        });
    }

    function shouldThrowIfInvalidProperty(
        diffProperty: keyof taquitoRpc.ContractBigMapDiffItem,
        testCase: { expectedUid: string; allowNullish?: boolean },
    ) {
        it.each([
            ...testCase.allowNullish !== true ? [undefined, null] : [],
            { invalid: 123 },
        ])(`should throw if '${diffProperty}' is %s`, (value: any) => {
            rpcData[diffProperty] = value;

            const error = expectToThrow(act, RpcConversionError);

            expect(error.uid).toBe(testCase.expectedUid);
        });
    }
});
