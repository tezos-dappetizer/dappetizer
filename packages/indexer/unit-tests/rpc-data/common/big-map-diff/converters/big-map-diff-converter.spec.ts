import { Writable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import {
    BigMapDiffConverter,
    RpcDataWithBigMapDiffs,
} from '../../../../../src/rpc-data/common/big-map-diff/converters/big-map-diff-converter';
import {
    LazyStorageDiffBigMapConverter,
} from '../../../../../src/rpc-data/common/big-map-diff/converters/lazy-storage-diff-big-map-converter';
import { LegacyBigMapDiffConverter } from '../../../../../src/rpc-data/common/big-map-diff/converters/legacy-big-map-diff-converter';
import {
    RpcConvertOptionsWithLazyContract,
} from '../../../../../src/rpc-data/rpc-convert-options';

describe(BigMapDiffConverter, () => {
    let target: BigMapDiffConverter;
    let lazyStorageConverter: LazyStorageDiffBigMapConverter;
    let legacyConverter: LegacyBigMapDiffConverter;

    let rpcData: Writable<RpcDataWithBigMapDiffs>;
    let options: RpcConvertOptionsWithLazyContract;

    const act = () => target.convertArrayFrom(rpcData, 'uu', options);

    beforeEach(() => {
        [lazyStorageConverter, legacyConverter] = [mock(), mock()];
        target = new BigMapDiffConverter(instance(lazyStorageConverter), instance(legacyConverter));

        rpcData = {};
        options = 'mockedOptions' as any;

        when(legacyConverter.convertArrayProperty(rpcData, 'big_map_diff', 'uu', options)).thenReturn('legacyDiffs' as any);
        when(lazyStorageConverter.convertArrayProperty(rpcData, 'lazy_storage_diff', 'uu', options)).thenReturn([
            ['diff1' as any, 'diff2' as any],
            ['diff3' as any],
        ]);
    });

    it('should convert lazy storage if it exists', () => {
        rpcData.lazy_storage_diff = [];
        rpcData.big_map_diff = []; // Should be ignored.

        const diffs = act();

        expect(diffs).toEqual(['diff1', 'diff2', 'diff3']);
        expect(diffs).toBeFrozen();
        verify(legacyConverter.convertArrayProperty(anything(), anything(), anything(), anything())).never();
    });

    it('should convert legacy big map diffs if no lazy storage', () => {
        rpcData.big_map_diff = [];

        const diffs = act();

        expect(diffs).toBe('legacyDiffs');
        verify(lazyStorageConverter.convertArrayProperty(anything(), anything(), anything(), anything())).never();
    });
});
