import { BigNumber } from 'bignumber.js';
import { assert, IsExact } from 'conditional-type-checks';

import {
    BalanceUpdate,
    BalanceUpdateCategory,
    BalanceUpdateFrozenStaker,
    BalanceUpdateKind,
    BalanceUpdateOrigin,
    BalanceUpdateStaker,
    TaquitoRpcBalanceUpdate,
} from '../../../../src/rpc-data/rpc-data-interfaces';

// Type of properties should be narrowed correctly.
const update: BalanceUpdate = {} as any;

assert<IsExact<string | null, typeof update.bondRollupAddress>>(true);
assert<IsExact<BalanceUpdateCategory | null, typeof update.category>>(true);
assert<IsExact<BigNumber, typeof update.change>>(true);
assert<IsExact<string | null, typeof update.committer>>(true);
assert<IsExact<string | null, typeof update.contractAddress>>(true);
assert<IsExact<number | null, typeof update.cycle>>(true);
assert<IsExact<string | null, typeof update.delayedOperationHash>>(true);
assert<IsExact<string | null, typeof update.delegateAddress>>(true);
assert<IsExact<string | null, typeof update.delegatorContractAddress>>(true);
assert<IsExact<BalanceUpdateKind, typeof update.kind>>(true);
assert<IsExact<BalanceUpdateOrigin | null, typeof update.origin>>(true);
assert<IsExact<boolean | null, typeof update.participation>>(true);
assert<IsExact<boolean | null, typeof update.revelation>>(true);
assert<IsExact<TaquitoRpcBalanceUpdate, typeof update.rpcData>>(true);
assert<IsExact<BalanceUpdateStaker | BalanceUpdateFrozenStaker | null, typeof update.staker>>(true);
assert<IsExact<string, typeof update.uid>>(true);

switch (update.kind) {
    case BalanceUpdateKind.Accumulator:
        // Corresponds to RPC schema:
        // { /* Block_fees */
        //      "kind": "accumulator",
        //      "category": "block fees",
        //      "change": $int64,
        //      "origin": "block" || "migration" || "subsidy" || "simulation" }
        assert<IsExact<null, typeof update.bondRollupAddress>>(true);
        assert<IsExact<BalanceUpdateCategory.BlockFees, typeof update.category>>(true);
        assert<IsExact<null, typeof update.committer>>(true);
        assert<IsExact<null, typeof update.contractAddress>>(true);
        assert<IsExact<null, typeof update.cycle>>(true);
        assert<IsExact<null, typeof update.delegateAddress>>(true);
        assert<IsExact<null, typeof update.delegatorContractAddress>>(true);
        assert<IsExact<BalanceUpdateOrigin, typeof update.origin>>(true);
        assert<IsExact<null, typeof update.participation>>(true);
        assert<IsExact<null, typeof update.revelation>>(true);
        assert<IsExact<null, typeof update.staker>>(true);
        break;

    case BalanceUpdateKind.Burned:
        assert<IsExact<null, typeof update.bondRollupAddress>>(true);
        assert<IsExact<null, typeof update.committer>>(true);
        assert<IsExact<null, typeof update.contractAddress>>(true);
        assert<IsExact<null, typeof update.cycle>>(true);
        assert<IsExact<BalanceUpdateOrigin, typeof update.origin>>(true);
        assert<IsExact<null, typeof update.staker>>(true);

        switch (update.category) {
            case BalanceUpdateCategory.LostAttestingRewards:
            case BalanceUpdateCategory.LostEndorsingRewards:
                // Corresponds to RPC schema:
                // { /* Lost_endorsing_rewards */
                //      "kind": "burned",
                //      "category": "lost endorsing rewards",
                //      "delegate": $Signature.Public_key_hash,
                //      "participation": boolean,
                //      "revelation": boolean,
                //      "change": $int64,
                //      "origin": "block" || "migration" || "subsidy" || "simulation" }
                assert<IsExact<string, typeof update.delegateAddress>>(true);
                assert<IsExact<null, typeof update.delegatorContractAddress>>(true);
                assert<IsExact<boolean, typeof update.participation>>(true);
                assert<IsExact<boolean, typeof update.revelation>>(true);
                break;

            default: {
                // Corresponds to RPC schema:
                // || { /* Storage_fees */
                //      "kind": "burned",
                //      "category": "storage fees",
                //      "change": $int64,
                //      "origin": "block" || "migration" || "subsidy" || "simulation" }
                // || { /* Double_signing_punishments */
                //      "kind": "burned",
                //      "category": "punishments",
                //      "change": $int64,
                //      "origin": "block" || "migration" || "subsidy" || "simulation" }
                // || { /* Burned */
                //      "kind": "burned",
                //      "category": "burned",
                //      "change": $int64,
                //      "origin": "block" || "migration" || "subsidy" || "simulation" }
                type ExpectedBurnedCategory =
                    | BalanceUpdateCategory.Burned
                    | BalanceUpdateCategory.Punishments
                    | BalanceUpdateCategory.SmartRollupRefutationPunishments
                    | BalanceUpdateCategory.StorageFees;

                assert<IsExact<ExpectedBurnedCategory, typeof update.category>>(true);
                assert<IsExact<null, typeof update.delegateAddress>>(true);
                assert<IsExact<null, typeof update.delegatorContractAddress>>(true);
                assert<IsExact<null, typeof update.participation>>(true);
                assert<IsExact<null, typeof update.revelation>>(true);
            }
        }
        break;

    case BalanceUpdateKind.Commitment:
        // Corresponds to RPC schema:
        // { /* Commitments */
        //      "kind": "commitment",
        //      "category": "commitment",
        //      "committer": $Blinded public key hash,
        //      "change": $int64,
        //      "origin": "block" || "migration" || "subsidy" || "simulation" }
        assert<IsExact<null, typeof update.bondRollupAddress>>(true);
        assert<IsExact<BalanceUpdateCategory.Commitment, typeof update.category>>(true);
        assert<IsExact<string, typeof update.committer>>(true);
        assert<IsExact<null, typeof update.contractAddress>>(true);
        assert<IsExact<null, typeof update.cycle>>(true);
        assert<IsExact<null, typeof update.delegateAddress>>(true);
        assert<IsExact<null, typeof update.delegatorContractAddress>>(true);
        assert<IsExact<BalanceUpdateOrigin, typeof update.origin>>(true);
        assert<IsExact<null, typeof update.participation>>(true);
        assert<IsExact<null, typeof update.revelation>>(true);
        assert<IsExact<null, typeof update.staker>>(true);
        break;

    case BalanceUpdateKind.Contract:
        // Corresponds to RPC schema:
        // { /* Contract */
        //      "kind": "contract",
        //      "contract": $contract_id,
        //      "change": $int64,
        //      "origin": "block" }
        // || { /* Contract */
        //      "kind": "contract",
        //       "contract": $contract_id,
        //       "change": $int64,
        //       "origin": "migration" }
        // || { /* Contract */
        //       "kind": "contract",
        //       "contract": $contract_id,
        //       "change": $int64,
        //       "origin": "subsidy" }
        //  || { /* Contract */
        //       "kind": "contract",
        //       "contract": $contract_id,
        //       "change": $int64,
        //       "origin": "simulation" }
        //  || { /* Contract */
        //       "kind": "contract",
        //       "contract": $contract_id,
        //       "change": $int64,
        //       "origin": "delayed_operation",
        //       "delayed_operation_hash": $Operation_hash }
        assert<IsExact<null, typeof update.bondRollupAddress>>(true);
        assert<IsExact<null, typeof update.category>>(true);
        assert<IsExact<null, typeof update.committer>>(true);
        assert<IsExact<string, typeof update.contractAddress>>(true);
        assert<IsExact<null, typeof update.cycle>>(true);
        assert<IsExact<null, typeof update.delegateAddress>>(true);
        assert<IsExact<null, typeof update.delegatorContractAddress>>(true);
        assert<IsExact<BalanceUpdateOrigin | null, typeof update.origin>>(true);
        assert<IsExact<null, typeof update.participation>>(true);
        assert<IsExact<null, typeof update.revelation>>(true);
        assert<IsExact<null, typeof update.staker>>(true);
        break;

    case BalanceUpdateKind.Freezer:
        assert<IsExact<null, typeof update.committer>>(true);
        assert<IsExact<null, typeof update.delegatorContractAddress>>(true);
        assert<IsExact<null, typeof update.participation>>(true);
        assert<IsExact<null, typeof update.revelation>>(true);

        switch (update.category) {
            case BalanceUpdateCategory.Bonds:
                // Corresponds to Jakarta RPC schema:
                // { /* Frozen_bonds */
                //     "kind": "freezer",
                //     "category": "bonds",
                //     "contract": $013-PtJakart.contract_id,
                //     "bond_id": $013-PtJakart.bond_id,
                //     "change": $int64,
                //     "origin": "block" || "migration" || "subsidy" || "simulation" }
                assert<IsExact<string, typeof update.bondRollupAddress>>(true);
                assert<IsExact<string, typeof update.contractAddress>>(true);
                assert<IsExact<null, typeof update.cycle>>(true);
                assert<IsExact<null, typeof update.delegateAddress>>(true);
                assert<IsExact<null, typeof update.staker>>(true);
                break;

            case BalanceUpdateCategory.Deposits:
                // Corresponds to RPC schema:
                // { /* Deposits */
                //      "kind": "freezer",
                //      "category": "deposits",
                //      "staker": $frozen_staker,
                //      "change": $int64,
                //      "origin": "block" || "migration" || "subsidy" || "simulation" }
                assert<IsExact<null, typeof update.bondRollupAddress>>(true);
                assert<IsExact<null, typeof update.contractAddress>>(true);
                assert<IsExact<null, typeof update.cycle>>(true);
                assert<IsExact<null, typeof update.delegateAddress>>(true);
                assert<IsExact<BalanceUpdateFrozenStaker, typeof update.staker>>(true);
                break;

            case BalanceUpdateCategory.UnstakedDeposits:
                // Corresponds to RPC schema:
                // { /* Unstaked_deposits */
                //      "kind": "freezer",
                //      "category": "unstaked_deposits",
                //      "staker": $staker,
                //      "cycle": integer ∈ [-2^31-1, 2^31],
                //      "change": $int64,
                //      "origin": "block" || "migration" || "subsidy" || "simulation" }
                assert<IsExact<null, typeof update.bondRollupAddress>>(true);
                assert<IsExact<null, typeof update.contractAddress>>(true);
                assert<IsExact<number, typeof update.cycle>>(true);
                assert<IsExact<null, typeof update.delegateAddress>>(true);
                assert<IsExact<BalanceUpdateStaker, typeof update.staker>>(true);
                break;

            default:
                // Corresponds to Hangzou RPC schema:
                // { /* Deposits */
                //      "kind": "freezer",
                //      "category": "deposits",
                //      "delegate": $Signature.Public_key_hash,
                //      "cycle": integer ∈ [-2^31-1, 2^31],
                //      "change": $int64,
                //      "origin": "block" || "migration" || "subsidy" }
                // || { /* Rewards */
                //      "kind": "freezer",
                //      "category": "rewards",
                //      "delegate": $Signature.Public_key_hash,
                //      "cycle": integer ∈ [-2^31-1, 2^31],
                //      "change": $int64,
                //      "origin": "block" || "migration" || "subsidy" }
                // || { /* Fees */
                //      "kind": "freezer",
                //      "category": "fees",
                //      "delegate": $Signature.Public_key_hash,
                //      "cycle": integer ∈ [-2^31-1, 2^31],
                //      "change": $int64,
                //      "origin": "block" || "migration" || "subsidy" }
                //
                // Corresponds to Ithaca RPC schema:
                // { /* Deposits */
                //      "kind": "freezer",
                //      "category": "deposits",
                //      "delegate": $Signature.Public_key_hash,
                //      "change": $int64,
                //      "origin": "block" || "migration" || "subsidy" || "simulation" }
                // || { /* Legacy_rewards */
                //      "kind": "freezer",
                //      "category": "legacy_rewards",
                //      "delegate": $Signature.Public_key_hash,
                //      "cycle": integer ∈ [-2^31-1, 2^31],
                //      "change": $int64,
                //      "origin": "block" || "migration" || "subsidy" || "simulation" }
                // || { /* Legacy_deposits */
                //      "kind": "freezer",
                //      "category": "legacy_deposits",
                //      "delegate": $Signature.Public_key_hash,
                //      "cycle": integer ∈ [-2^31-1, 2^31],
                //      "change": $int64,
                //      "origin": "block" || "migration" || "subsidy" || "simulation" }
                // || { /* Legacy_fees */
                //      "kind": "freezer",
                //      "category": "legacy_fees",
                //      "delegate": $Signature.Public_key_hash,
                //      "cycle": integer ∈ [-2^31-1, 2^31],
                //      "change": $int64,
                //      "origin": "block" || "migration" || "subsidy" || "simulation" }
                type ExpectedFreezerCategory =
                    | BalanceUpdateCategory.LegacyDeposits
                    | BalanceUpdateCategory.LegacyFees
                    | BalanceUpdateCategory.LegacyRewards;

                assert<IsExact<null, typeof update.bondRollupAddress>>(true);
                assert<IsExact<ExpectedFreezerCategory, typeof update.category>>(true);
                assert<IsExact<null, typeof update.contractAddress>>(true);
                assert<IsExact<number | null, typeof update.cycle>>(true);
                assert<IsExact<string, typeof update.delegateAddress>>(true);
                assert<IsExact<null, typeof update.staker>>(true);
                break;
        }
        break;

    case BalanceUpdateKind.Minted:
        // Corresponds to RPC schema:
        // { /* Nonce_revelation_rewards */
        //      "kind": "minted",
        //      "category": "nonce revelation rewards",
        //      "change": $int64,
        //      "origin": "block" || "migration" || "subsidy" || "simulation" }
        // || { /* Double_signing_evidence_rewards */
        //      "kind": "minted",
        //      "category": "double signing evidence rewards",
        //      "change": $int64,
        //      "origin": "block" || "migration" || "subsidy" || "simulation" }
        // || { /* Endorsing_rewards */
        //      "kind": "minted",
        //      "category": "endorsing rewards",
        //      "change": $int64,
        //      "origin": "block" || "migration" || "subsidy" || "simulation" }
        // || { /* Baking_rewards */
        //      "kind": "minted",
        //      "category": "baking rewards",
        //      "change": $int64,
        //      "origin": "block" || "migration" || "subsidy" || "simulation" }
        // || { /* Baking_bonuses */
        //      "kind": "minted",
        //      "category": "baking bonuses",
        //      "change": $int64,
        //      "origin": "block" || "migration" || "subsidy" || "simulation" }
        // || { /* Liquidity_baking_subsidies */
        //      "kind": "minted",
        //      "category": "subsidy",
        //      "change": $int64,
        //      "origin": "block" || "migration" || "subsidy" || "simulation" }
        // || { /* Bootstrap */
        //      "kind": "minted",
        //      "category": "bootstrap",
        //      "change": $int64,
        //      "origin": "block" || "migration" || "subsidy" || "simulation" }
        // || { /* Invoice */
        //      "kind": "minted",
        //      "category": "invoice",
        //      "change": $int64,
        //      "origin": "block" || "migration" || "subsidy" || "simulation" }
        // || { /* Initial_commitments */
        //      "kind": "minted",
        //      "category": "commitment",
        //      "change": $int64,
        //      "origin": "block" || "migration" || "subsidy" || "simulation" }
        // || { /* Minted */
        //      "kind": "minted",
        //      "category": "minted",
        //      "change": $int64,
        //      "origin": "block" || "migration" || "subsidy" || "simulation" }
        // || { /* Attesting_rewards */
        //      "kind": "minted",
        //      "category": "attesting rewards",
        //      "change": $int64,
        //      "origin": "block" || "migration" || "subsidy" | "simulation" }
        // || { /* Smart_rollup_refutation_rewards */
        //      "kind": "minted",
        //      "category": "smart_rollup_refutation_rewards",
        //      "change": $int64,
        //      "origin": "block" || "migration" || "subsidy" | "simulation" }
        type ExpectedMintedCategory =
            | BalanceUpdateCategory.AttestingRewards
            | BalanceUpdateCategory.BakingBonuses
            | BalanceUpdateCategory.BakingRewards
            | BalanceUpdateCategory.Bootstrap
            | BalanceUpdateCategory.Commitment
            | BalanceUpdateCategory.DoubleSigningEvidenceRewards
            | BalanceUpdateCategory.EndorsingRewards
            | BalanceUpdateCategory.Invoice
            | BalanceUpdateCategory.Minted
            | BalanceUpdateCategory.NonceRevelationRewards
            | BalanceUpdateCategory.SmartRollupRefutationRewards
            | BalanceUpdateCategory.Subsidy;

        assert<IsExact<null, typeof update.bondRollupAddress>>(true);
        assert<IsExact<ExpectedMintedCategory, typeof update.category>>(true);
        assert<IsExact<null, typeof update.committer>>(true);
        assert<IsExact<null, typeof update.contractAddress>>(true);
        assert<IsExact<null, typeof update.cycle>>(true);
        assert<IsExact<null, typeof update.delegateAddress>>(true);
        assert<IsExact<null, typeof update.delegatorContractAddress>>(true);
        assert<IsExact<BalanceUpdateOrigin, typeof update.origin>>(true);
        assert<IsExact<null, typeof update.participation>>(true);
        assert<IsExact<null, typeof update.revelation>>(true);
        assert<IsExact<null, typeof update.staker>>(true);
        break;

    case BalanceUpdateKind.Staking:
        assert<IsExact<null, typeof update.bondRollupAddress>>(true);
        assert<IsExact<null, typeof update.committer>>(true);
        assert<IsExact<null, typeof update.contractAddress>>(true);
        assert<IsExact<null, typeof update.cycle>>(true);
        assert<IsExact<null, typeof update.participation>>(true);
        assert<IsExact<null, typeof update.revelation>>(true);
        assert<IsExact<null, typeof update.staker>>(true);

        switch (update.category) {
            case BalanceUpdateCategory.StakingDelegateDenominator:
                // Corresponds to RPC schema:
                // { /* Staking_delegate_denominator */
                //      "kind": "staking",
                //      "category": "delegate_denominator",
                //      "delegate": $Signature.Public_key_hash,
                //      "change": $int64,
                //      "origin": "block" || "migration" || "subsidy" || "simulation" }
                assert<IsExact<string, typeof update.delegateAddress>>(true);
                assert<IsExact<null, typeof update.delegatorContractAddress>>(true);
                break;

            default:
                // Corresponds to RPC schema:
                // { /* Staking_delegator_numerator */
                //      "kind": "staking",
                //      "category": "delegator_numerator",
                //      "delegator": $contract_id,
                //      "change": $int64,
                //      "origin": "block" || "migration" || "subsidy" || "simulation" }
                assert<IsExact<BalanceUpdateCategory.StakingDelegatorNumerator, typeof update.category>>(true);
                assert<IsExact<null, typeof update.delegateAddress>>(true);
                assert<IsExact<string, typeof update.delegatorContractAddress>>(true);
                break;
        }
        break;
}
