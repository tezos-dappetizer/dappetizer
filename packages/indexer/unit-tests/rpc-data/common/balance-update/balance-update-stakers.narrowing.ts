import { assert, IsExact } from 'conditional-type-checks';

import { BalanceUpdateFrozenStaker, BalanceUpdateStaker } from '../../../../src/rpc-data/rpc-data-interfaces';

verifyStaker({} as any);

// Type of properties should be narrowed correctly.
const frozenStaker: BalanceUpdateFrozenStaker = {} as any;

assert<IsExact<string | null, typeof frozenStaker.bakerAddress>>(true);
assert<IsExact<string | null, typeof frozenStaker.contractAddress>>(true);
assert<IsExact<string | null, typeof frozenStaker.delegateAddress>>(true);
assert<IsExact<'Single' | 'Shared' | 'Baker' | 'BakerEdge', typeof frozenStaker.type>>(true);

switch (frozenStaker.type) {
    case 'Baker':
    case 'BakerEdge':
        // Corresponds to RPC schema:
        // { /* Baker */
        //      "baker": $Signature.Public_key_hash }
        // || { /* Baker */
        //      "baker_own_stake": $Signature.Public_key_hash }
        // || { /* Baker_edge */
        //      "baker_edge": $Signature.Public_key_hash }
        assert<IsExact<string, typeof frozenStaker.bakerAddress>>(true);
        assert<IsExact<null, typeof frozenStaker.contractAddress>>(true);
        assert<IsExact<null, typeof frozenStaker.delegateAddress>>(true);
        break;

    case 'Shared':
    case 'Single':
        verifyStaker(frozenStaker);
        break;
}

function verifyStaker(staker: BalanceUpdateStaker) {
    // Type of properties should be narrowed correctly.
    assert<IsExact<null, typeof staker.bakerAddress>>(true);
    assert<IsExact<string | null, typeof staker.contractAddress>>(true);
    assert<IsExact<string, typeof staker.delegateAddress>>(true);
    assert<IsExact<'Single' | 'Shared', typeof staker.type>>(true);

    switch (staker.type) {
        case 'Shared':
            // Corresponds to RPC schema:
            // { /* Shared */
            //      "delegate": $Signature.Public_key_hash }
            assert<IsExact<null, typeof staker.contractAddress>>(true);
            break;

        case 'Single':
            // Corresponds to RPC schema:
            // { /* Single */
            //      "contract": $contract_id,
            //      "delegate": $Signature.Public_key_hash }
            assert<IsExact<string, typeof staker.contractAddress>>(true);
            break;
    }
}
