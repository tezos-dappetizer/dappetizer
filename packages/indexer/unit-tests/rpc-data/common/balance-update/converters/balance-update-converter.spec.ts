import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { StrictExclude, Writable } from 'ts-essentials';

import { expectToThrow, TestLogger } from '../../../../../../../test-utilities/mocks';
import { entriesOf, getEnumValues } from '../../../../../../utils/src/public-api';
import {
    BalanceUpdateConverter,
} from '../../../../../src/rpc-data/common/balance-update/converters/balance-update-converter';
import { RpcConversionError } from '../../../../../src/rpc-data/rpc-conversion-error';
import {
    BalanceUpdate,
    BalanceUpdateBurned,
    BalanceUpdateBurnedLostRewards,
    BalanceUpdateCategory,
    BalanceUpdateCommitment,
    BalanceUpdateContract,
    BalanceUpdateFreezerDeposits,
    BalanceUpdateFreezerLegacy,
    BalanceUpdateFreezerUnstakedDeposits,
    BalanceUpdateFrozenStaker,
    BalanceUpdateKind,
    BalanceUpdateOrigin,
    TaquitoRpcBalanceUpdate,
} from '../../../../../src/rpc-data/rpc-data-interfaces';
import { RpcSkippableConversionError } from '../../../../../src/rpc-data/rpc-skippable-array-converter';
import { convertObject } from '../../../rpc-test-helpers';

const rpcCategories = taquitoRpc.METADATA_BALANCE_UPDATES_CATEGORY;

describe(BalanceUpdateConverter.name, () => {
    let target: BalanceUpdateConverter;
    let logger: TestLogger;
    let rpcData: Writable<TaquitoRpcBalanceUpdate>;

    const act = () => convertObject(target, rpcData, 'xx');

    const testedKinds: string[] = [];
    const testedCategories: string[] = [];

    beforeEach(() => {
        logger = new TestLogger();
        target = new BalanceUpdateConverter(logger);

        rpcData = {
            kind: 'contract',
            bond_id: { smart_rollup: 'sr1NtrMuXaN6RoiJuxTE6uUbBEzfDmoGqccP' } as any,
            committer: 'mmm',
            contract: 'tz1WWQvVzWRhBBBJGwzzgzD49wZXpFn5AQjU',
            change: '111',
            cycle: 222,
            delayed_operation_hash: 'haha-delayed',
            delegate: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb',
            delegator: 'KT1J9FYz29RBQi1oGLw8uXyACrzXzV1dHuvb',
            origin: 'block',
            participation: true,
            revelation: true,
            staker: { delegate: 'mocked' },
        };
    });

    describeKind('accumulator', () => {
        testedCategories.push(rpcCategories.BLOCK_FEES);
        it(`should convert correctly`, () => {
            rpcData.category = rpcCategories.BLOCK_FEES;

            runConvertTest({
                kind: BalanceUpdateKind.Accumulator,
                category: BalanceUpdateCategory.BlockFees,
                change: new BigNumber(111),
                origin: BalanceUpdateOrigin.Block,
                rpcData,
                uid: 'xx[accumulator][block fees]',

                bondRollupAddress: null,
                committer: null,
                contractAddress: null,
                cycle: null,
                delayedOperationHash: null,
                delegateAddress: null,
                delegatorContractAddress: null,
                participation: null,
                revelation: null,
                staker: null,
            });
        });

        shouldThrowIfCategoryFailed({
            expectedUid: 'xx[accumulator]/category',
            expectedError: `supported values: ['block fees']`,
        });
    });

    describeKind('burned', () => {
        const testCases = [
            [rpcCategories.BURNED, BalanceUpdateCategory.Burned, 'xx[burned][burned]'],
            [rpcCategories.PUNISHMENTS, BalanceUpdateCategory.Punishments, 'xx[burned][punishments]'],
            [rpcCategories.STORAGE_FEES, BalanceUpdateCategory.StorageFees, 'xx[burned][storage fees]'],
            [
                rpcCategories.SMART_ROLLUP_REFUTATION_PUNISHMENTS,
                BalanceUpdateCategory.SmartRollupRefutationPunishments,
                'xx[burned][smart_rollup_refutation_punishments]',
            ],
        ] as const;
        testedCategories.push(...testCases.map(t => t[0]));

        it.each(testCases)(`should convert '%s' correctly`, (rpcCategory, expectedCategory, expectedUid) => {
            rpcData.category = rpcCategory;

            runConvertTest<BalanceUpdateBurned>({
                kind: BalanceUpdateKind.Burned,
                category: expectedCategory,
                change: new BigNumber(111),
                origin: BalanceUpdateOrigin.Block,
                rpcData,
                uid: expectedUid,

                bondRollupAddress: null,
                committer: null,
                contractAddress: null,
                cycle: null,
                delayedOperationHash: null,
                delegateAddress: null,
                delegatorContractAddress: null,
                participation: null,
                revelation: null,
                staker: null,
            });
        });

        const lostRewardsTestCases = [
            [rpcCategories.LOST_ENDORSING_REWARDS, BalanceUpdateCategory.LostEndorsingRewards, 'xx[burned][lost endorsing rewards]'],
            ['lost attesting rewards', BalanceUpdateCategory.LostAttestingRewards, 'xx[burned][lost attesting rewards]'],
        ] as const;
        testedCategories.push(...lostRewardsTestCases.map(t => t[0]));

        describe.each(lostRewardsTestCases)(`should convert %s correctly`, (rpcCategory, expectedCategory, expectedUid) => {
            it.each([
                [true, true],
                [true, false],
                [false, true],
                [false, false],
            ])(`participation is %s, revelation is %s`, (participation, revelation) => {
                rpcData.category = rpcCategory as any;
                rpcData.participation = participation;
                rpcData.revelation = revelation;

                runConvertTest<BalanceUpdateBurnedLostRewards>({
                    kind: BalanceUpdateKind.Burned,
                    category: expectedCategory,
                    change: new BigNumber(111),
                    delegateAddress: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb',
                    origin: BalanceUpdateOrigin.Block,
                    participation,
                    revelation,
                    rpcData,
                    uid: expectedUid,

                    bondRollupAddress: null,
                    committer: null,
                    contractAddress: null,
                    cycle: null,
                    delayedOperationHash: null,
                    delegatorContractAddress: null,
                    staker: null,
                });
            });
        });

        shouldThrowIfCategoryFailed({
            expectedUid: 'xx[burned]/category',
            expectedError: `supported values: ['burned', 'lost attesting rewards', 'lost endorsing rewards',`
                + ` 'punishments', 'smart_rollup_refutation_punishments', 'storage fees']`,
        });
    });

    describeKind('commitment', () => {
        testedCategories.push(rpcCategories.COMMITMENT);
        it(`should convert correctly`, () => {
            rpcData.category = rpcCategories.COMMITMENT;

            runConvertTest<BalanceUpdateCommitment>({
                kind: BalanceUpdateKind.Commitment,
                category: BalanceUpdateCategory.Commitment,
                committer: 'mmm',
                change: new BigNumber(111),
                origin: BalanceUpdateOrigin.Block,
                rpcData,
                uid: 'xx[commitment][commitment]',

                bondRollupAddress: null,
                contractAddress: null,
                cycle: null,
                delayedOperationHash: null,
                delegateAddress: null,
                delegatorContractAddress: null,
                participation: null,
                revelation: null,
                staker: null,
            });
        });

        shouldThrowIfCategoryFailed({
            expectedUid: 'xx[commitment]/category',
            expectedError: `supported values: ['commitment']`,
        });
    });

    describeKind('contract', () => {
        it('should convert correctly', () => {
            runConvertTest<BalanceUpdateContract>({
                kind: BalanceUpdateKind.Contract,
                change: new BigNumber(111),
                contractAddress: 'tz1WWQvVzWRhBBBJGwzzgzD49wZXpFn5AQjU',
                origin: BalanceUpdateOrigin.Block,
                rpcData,
                uid: 'xx[contract]',

                bondRollupAddress: null,
                category: null,
                committer: null,
                cycle: null,
                delayedOperationHash: null,
                delegateAddress: null,
                delegatorContractAddress: null,
                participation: null,
                revelation: null,
                staker: null,
            });
        });

        it('should convert correctly if legacy schema', () => {
            rpcData.origin = undefined;

            const update = act();

            expect(update.origin).toBeNull();
        });
    });

    describeKind('freezer', () => {
        const testCases = [
            ['fees', BalanceUpdateCategory.LegacyFees, 'xx[freezer][fees]'],
            ['legacy_deposits', BalanceUpdateCategory.LegacyDeposits, 'xx[freezer][legacy_deposits]'],
            ['legacy_fees', BalanceUpdateCategory.LegacyFees, 'xx[freezer][legacy_fees]'],
            ['legacy_rewards', BalanceUpdateCategory.LegacyRewards, 'xx[freezer][legacy_rewards]'],
            ['rewards', BalanceUpdateCategory.LegacyRewards, 'xx[freezer][rewards]'],
        ] as const;
        testedCategories.push(...testCases.map(t => t[0]));

        it.each(testCases)(`should convert '%s' correctly`, (category, expectedCategory, expectedUid) => {
            rpcData.category = category as any;

            runConvertTest<BalanceUpdateFreezerLegacy>({
                kind: BalanceUpdateKind.Freezer,
                category: expectedCategory,
                change: new BigNumber(111),
                cycle: 222,
                delegateAddress: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb',
                origin: BalanceUpdateOrigin.Block,
                rpcData,
                uid: expectedUid,

                bondRollupAddress: null,
                committer: null,
                contractAddress: null,
                delayedOperationHash: null,
                delegatorContractAddress: null,
                participation: null,
                revelation: null,
                staker: null,
            });
        });

        testedCategories.push(rpcCategories.BONDS);
        it(`should convert '${rpcCategories.BONDS}' correctly`, () => {
            rpcData.category = rpcCategories.BONDS;

            const update = act();

            expect(update).toEqual({
                kind: BalanceUpdateKind.Freezer,
                category: BalanceUpdateCategory.Bonds,
                bondRollupAddress: 'sr1NtrMuXaN6RoiJuxTE6uUbBEzfDmoGqccP',
                change: new BigNumber(111),
                contractAddress: 'tz1WWQvVzWRhBBBJGwzzgzD49wZXpFn5AQjU',
                origin: BalanceUpdateOrigin.Block,
                rpcData,
                uid: 'xx[freezer][bonds]',

                committer: null,
                cycle: null,
                delayedOperationHash: null,
                delegateAddress: null,
                delegatorContractAddress: null,
                participation: null,
                revelation: null,
                staker: null,
            });
            expect(update).toBeFrozen();
            expect(update.bondRollupAddress).toBeFrozen();
        });

        testedCategories.push(rpcCategories.DEPOSITS);
        describe(`should convert '${rpcCategories.DEPOSITS}' correctly`, () => {
            it.each<[string, taquitoRpc.FrozenStaker, BalanceUpdateFrozenStaker]>([
                ['single staker', { delegate: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb', contract: 'KT1SVgaHyuyLjwiz7GYxNsGC4FH4BqcK5oxF' }, {
                    type: 'Single',
                    bakerAddress: null,
                    contractAddress: 'KT1SVgaHyuyLjwiz7GYxNsGC4FH4BqcK5oxF',
                    delegateAddress: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb',
                }],
                ['shared staker', { delegate: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb' }, {
                    type: 'Shared',
                    bakerAddress: null,
                    contractAddress: null,
                    delegateAddress: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb',
                }],
                ['legacy baker', { baker: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb' }, {
                    type: 'Baker',
                    bakerAddress: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb',
                    contractAddress: null,
                    delegateAddress: null,
                }],
                ['own baker', { baker_own_stake: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb' } as any, {
                    type: 'Baker',
                    bakerAddress: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb',
                    contractAddress: null,
                    delegateAddress: null,
                }],
                ['baker edge', { baker_edge: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb' } as any, {
                    type: 'BakerEdge',
                    bakerAddress: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb',
                    contractAddress: null,
                    delegateAddress: null,
                }],
            ])(`with %s`, (_desc, rpcStaker, expectedStaker) => {
                rpcData.category = rpcCategories.DEPOSITS;
                rpcData.staker = rpcStaker;

                const update = runConvertTest<BalanceUpdateFreezerDeposits>({
                    kind: BalanceUpdateKind.Freezer,
                    category: BalanceUpdateCategory.Deposits,
                    change: new BigNumber(111),
                    origin: BalanceUpdateOrigin.Block,
                    rpcData,
                    staker: expectedStaker,
                    uid: 'xx[freezer][deposits]',

                    bondRollupAddress: null,
                    committer: null,
                    contractAddress: null,
                    cycle: null,
                    delayedOperationHash: null,
                    delegateAddress: null,
                    delegatorContractAddress: null,
                    participation: null,
                    revelation: null,
                });
                expect(update.staker).toBeFrozen();
            });
        });

        describe(`should convert '${rpcCategories.DEPOSITS}' correctly if legacy schema`, () => {
            it.each([
                ['with', 222],
                ['without', null],
            ])(`%s 'cycle'`, (_desc, cycle) => {
                rpcData.category = rpcCategories.DEPOSITS;
                rpcData.staker = undefined;
                rpcData.cycle = cycle ?? undefined;

                runConvertTest<BalanceUpdateFreezerLegacy>({
                    kind: BalanceUpdateKind.Freezer,
                    category: BalanceUpdateCategory.LegacyDeposits,
                    change: new BigNumber(111),
                    cycle,
                    delegateAddress: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb',
                    origin: BalanceUpdateOrigin.Block,
                    rpcData,
                    uid: 'xx[freezer][deposits]',

                    bondRollupAddress: null,
                    committer: null,
                    contractAddress: null,
                    delayedOperationHash: null,
                    delegatorContractAddress: null,
                    participation: null,
                    revelation: null,
                    staker: null,
                });
            });
        });

        testedCategories.push(rpcCategories.UNSTAKED_DEPOSITS);
        describe(rpcCategories.UNSTAKED_DEPOSITS, () => {
            it.each<[string, taquitoRpc.FrozenStaker, BalanceUpdateFreezerUnstakedDeposits['staker']]>([
                ['single staker', { delegate: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb', contract: 'KT1SVgaHyuyLjwiz7GYxNsGC4FH4BqcK5oxF' }, {
                    type: 'Single',
                    bakerAddress: null,
                    contractAddress: 'KT1SVgaHyuyLjwiz7GYxNsGC4FH4BqcK5oxF',
                    delegateAddress: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb',
                }],
                ['shared staker', { delegate: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb' }, {
                    type: 'Shared',
                    bakerAddress: null,
                    contractAddress: null,
                    delegateAddress: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb',
                }],
            ])(`should convert correctly with %s`, (_desc, rpcStaker, expectedStaker) => {
                rpcData.category = rpcCategories.UNSTAKED_DEPOSITS;
                rpcData.staker = rpcStaker;

                runConvertTest({
                    kind: BalanceUpdateKind.Freezer,
                    category: BalanceUpdateCategory.UnstakedDeposits,
                    change: new BigNumber(111),
                    cycle: 222,
                    origin: BalanceUpdateOrigin.Block,
                    rpcData,
                    staker: expectedStaker,
                    uid: 'xx[freezer][unstaked_deposits]',

                    bondRollupAddress: null,
                    committer: null,
                    contractAddress: null,
                    delayedOperationHash: null,
                    delegateAddress: null,
                    delegatorContractAddress: null,
                    participation: null,
                    revelation: null,
                });
            });

            it('should throw if baker as staker', () => {
                rpcData.category = rpcCategories.UNSTAKED_DEPOSITS;
                rpcData.staker = { baker: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb' };

                runThrowTest({
                    expectedUid: 'xx[freezer][unstaked_deposits]/staker',
                    expectedError: `unsupported value {"baker":"tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb"} of type 'object'`,
                    itemSkipped: false,
                });
            });
        });

        shouldThrowIfCategoryFailed({
            expectedUid: 'xx[freezer]/category',
            expectedError: `supported values: ['bonds', 'deposits', 'fees', 'legacy_deposits',`
                + ` 'legacy_fees', 'legacy_rewards', 'rewards', 'unstaked_deposits']`,
        });
    });

    describeKind('minted', () => {
        const testCases = [
            [rpcCategories.BAKING_BONUSES, BalanceUpdateCategory.BakingBonuses, 'xx[minted][baking bonuses]'],
            [rpcCategories.BAKING_REWARDS, BalanceUpdateCategory.BakingRewards, 'xx[minted][baking rewards]'],
            [rpcCategories.BOOTSTRAP, BalanceUpdateCategory.Bootstrap, 'xx[minted][bootstrap]'],
            [rpcCategories.COMMITMENT, BalanceUpdateCategory.Commitment, 'xx[minted][commitment]'],
            ['double signing evidence rewards', BalanceUpdateCategory.DoubleSigningEvidenceRewards, 'xx[minted][double signing evidence rewards]'],
            [rpcCategories.ENDORSING_REWARDS, BalanceUpdateCategory.EndorsingRewards, 'xx[minted][endorsing rewards]'],
            [rpcCategories.INVOICE, BalanceUpdateCategory.Invoice, 'xx[minted][invoice]'],
            [rpcCategories.MINTED, BalanceUpdateCategory.Minted, 'xx[minted][minted]'],
            [rpcCategories.NONCE_REVELATION_REWARDS, BalanceUpdateCategory.NonceRevelationRewards, 'xx[minted][nonce revelation rewards]'],
            [rpcCategories.SUBSIDY, BalanceUpdateCategory.Subsidy, 'xx[minted][subsidy]'],
            [
                rpcCategories.SMART_ROLLUP_REFUTATION_REWARDS,
                BalanceUpdateCategory.SmartRollupRefutationRewards,
                'xx[minted][smart_rollup_refutation_rewards]',
            ],
        ] as const;
        testedCategories.push(...testCases.map(t => t[0]));

        it.each(testCases)(`should convert '%s' correctly`, (rpcCategory, expectedCategory, expectedUid) => {
            rpcData.category = rpcCategory as any;

            runConvertTest({
                kind: BalanceUpdateKind.Minted,
                category: expectedCategory,
                change: new BigNumber(111),
                origin: BalanceUpdateOrigin.Block,
                rpcData,
                uid: expectedUid,

                bondRollupAddress: null,
                committer: null,
                contractAddress: null,
                cycle: null,
                delayedOperationHash: null,
                delegateAddress: null,
                delegatorContractAddress: null,
                participation: null,
                revelation: null,
                staker: null,
            });
        });

        shouldThrowIfCategoryFailed({
            expectedUid: 'xx[minted]/category',
            expectedError: `supported values: ['baking bonuses', 'baking rewards', 'bootstrap', 'commitment', 'double signing evidence rewards',`
                + ` 'endorsing rewards', 'invoice', 'minted', 'nonce revelation rewards', 'smart_rollup_refutation_rewards', 'subsidy']`,
        });
    });

    describeKind('staking', () => {
        testedCategories.push(rpcCategories.DELEGATE_DENOMINATOR);
        it(`should convert '${rpcCategories.DELEGATE_DENOMINATOR}' correctly`, () => {
            rpcData.category = rpcCategories.DELEGATE_DENOMINATOR;

            runConvertTest({
                kind: BalanceUpdateKind.Staking,
                category: BalanceUpdateCategory.StakingDelegateDenominator,
                change: new BigNumber(111),
                delegateAddress: 'tz1P2Po7YM526ughEsRbY4oR9zaUPDZjxFrb',
                origin: BalanceUpdateOrigin.Block,
                rpcData,
                uid: 'xx[staking][delegate_denominator]',

                bondRollupAddress: null,
                committer: null,
                contractAddress: null,
                cycle: null,
                delayedOperationHash: null,
                delegatorContractAddress: null,
                participation: null,
                revelation: null,
                staker: null,
            });
        });

        testedCategories.push(rpcCategories.DELEGATOR_NUMERATOR);
        it(`should convert '${rpcCategories.DELEGATOR_NUMERATOR}' correctly`, () => {
            rpcData.category = rpcCategories.DELEGATOR_NUMERATOR;

            runConvertTest({
                kind: BalanceUpdateKind.Staking,
                category: BalanceUpdateCategory.StakingDelegatorNumerator,
                change: new BigNumber(111),
                delegatorContractAddress: 'KT1J9FYz29RBQi1oGLw8uXyACrzXzV1dHuvb',
                origin: BalanceUpdateOrigin.Block,
                rpcData,
                uid: 'xx[staking][delegator_numerator]',

                bondRollupAddress: null,
                committer: null,
                contractAddress: null,
                cycle: null,
                delayedOperationHash: null,
                delegateAddress: null,
                participation: null,
                revelation: null,
                staker: null,
            });
        });

        shouldThrowIfCategoryFailed({
            expectedUid: 'xx[staking]/category',
            expectedError: `supported values: ['delegate_denominator', 'delegator_numerator']`,
        });
    });

    describe(`'kind' property`, () => {
        it.each([
            ['undefined', undefined, false],
            ['null', null, false],
            ['invalid', { wtf: 1 }, false],
            ['unsupported', 'unsupported', true],
        ])(`should throw if %s`, (_desc, rpcKind: any, itemSkipped) => {
            rpcData.kind = rpcKind;

            runThrowTest({
                expectedUid: 'xx/kind',
                expectedError: `['accumulator', 'burned', 'commitment', 'contract', 'freezer', 'minted', 'staking']`,
                itemSkipped,
            });
        });

        it('should test all values', () => {
            const untested = getEnumValues(BalanceUpdateKind).map(k => k.toLowerCase()).filter(k => !testedKinds.includes(k));
            expect(untested).toBeEmpty();
        });
    });

    it(`should test all 'category' values`, () => {
        const untested = getEnumValues(rpcCategories).filter(c => !testedCategories.includes(c));
        expect(untested).toBeEmpty();
    });

    describe(`'origin' property`, () => {
        const testCases: Record<StrictExclude<taquitoRpc.MetadataBalanceUpdatesOriginEnum, 'delayed_operation'>, BalanceUpdateOrigin> = {
            block: BalanceUpdateOrigin.Block,
            migration: BalanceUpdateOrigin.Migration,
            simulation: BalanceUpdateOrigin.Simulation,
            subsidy: BalanceUpdateOrigin.Subsidy,
        };

        it.each(entriesOf(testCases))(`should support '%s'`, (rpcOrigin, expectedOrigin) => {
            rpcData.origin = rpcOrigin;

            const update = act();

            expect(update.origin).toBe(expectedOrigin);
            expect(update.delayedOperationHash).toBeNull();
        });

        it('should support delayed operation', () => {
            rpcData.origin = 'delayed_operation';

            const update = act();

            expect(update.origin).toBe(BalanceUpdateOrigin.DelayedOperation);
            expect(update.delayedOperationHash).toBe('haha-delayed');
        });

        it.each([undefined, null])(`should support %s`, rpcOrigin => {
            rpcData.origin = rpcOrigin as any;

            const update = act();

            expect(update.origin).toBeNull();
            expect(update.delayedOperationHash).toBeNull();
        });

        it.each([
            ['invalid', { wtf: 1 }, false],
            ['unsupported', 'unsupported', true],
        ])(`should throw if %s`, (_desc, origin: any, itemSkipped) => {
            rpcData.origin = origin;

            runThrowTest({
                expectedUid: 'xx[contract]/origin',
                expectedError: `['block', 'delayed_operation', 'migration', 'simulation', 'subsidy']`,
                itemSkipped,
            });
        });
    });

    function shouldThrowIfCategoryFailed(testCases: { expectedUid: string; expectedError: string }) {
        it.each([
            ['undefined', undefined, false],
            ['invalid', { wtf: 1 }, false],
            ['unsupported', 'unsupported', true],
        ])(`should throw if %s 'category'`, (_desc, category: any, itemSkipped) => {
            rpcData.category = category;

            runThrowTest({ ...testCases, itemSkipped });
        });
    }

    function runConvertTest<TUpdate extends BalanceUpdate>(expectedUpdate: TUpdate): TUpdate {
        const update = act();

        expect(update).toEqual(expectedUpdate);
        expect(update).toBeFrozen();
        return update as TUpdate;
    }

    function runThrowTest(testCase: { expectedUid: string; expectedError?: string; itemSkipped: boolean }) {
        const expectedErrorType = testCase.itemSkipped ? RpcSkippableConversionError : RpcConversionError;

        const error = expectToThrow(act, expectedErrorType);

        expect(error.uid).toBe(testCase.expectedUid);
        expect(error.dataDescription).toContain(testCase.expectedError ?? '');
    }

    function describeKind(kind: taquitoRpc.MetadataBalanceUpdatesKindEnum, runTests: () => void) {
        describe(`'${kind}' kind`, () => {
            beforeEach(() => {
                rpcData.kind = kind;
            });
            runTests();
        });
        testedKinds.push(kind);
    }
});
