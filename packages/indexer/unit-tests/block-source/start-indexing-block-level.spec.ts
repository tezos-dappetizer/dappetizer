import { instance, mock, when } from 'ts-mockito';

import { expectToThrowAsync, TestLogger } from '../../../../test-utilities/mocks';
import { StartIndexingBlockLevel } from '../../src/block-source/start-indexing-block-level';
import { IndexedBlock } from '../../src/client-indexers/indexed-block';
import { IndexerDatabaseHandlerWrapper } from '../../src/client-indexers/wrappers/indexer-database-handler-wrapper';
import { IndexingConfig } from '../../src/config/indexing/indexing-config';

describe(StartIndexingBlockLevel.name, () => {
    let target: StartIndexingBlockLevel;
    let blockHandler: IndexerDatabaseHandlerWrapper;
    let logger: TestLogger;

    beforeEach(() => {
        const config = {
            configPropertyPath: '$.root.indexing',
            fromBlockLevel: 66,
        } as IndexingConfig;
        blockHandler = mock();
        logger = new TestLogger();
        target = new StartIndexingBlockLevel(config, instance(blockHandler), logger);

        when(blockHandler.name).thenReturn('FooHandler');
    });

    it('should return configured level if last indexed block is', async () => {
        when(blockHandler.getLastIndexedBlock()).thenResolve(null);

        const level = await target.resolveLevel();

        expect(level).toBe(66);
        logger.loggedSingle()
            .verify('Information', { '$.root.indexing.fromBlockLevel': 66 })
            .verifyMessage('Starting to index', 'provided by FooHandler');
    });

    describe('should throw if configured level is', () => {
        it.each([
            ['+2 from last indexed', 64],
            ['much greater than last indexed', 10],
        ])('', async (_desc, lastIndexedLevel) => {
            when(blockHandler.getLastIndexedBlock()).thenResolve({ level: lastIndexedLevel } as IndexedBlock);

            const error = await expectToThrowAsync(async () => target.resolveLevel());

            expect(error.message).toIncludeMultiple([
                `'$.root.indexing.fromBlockLevel' is '66'`,
                `last indexed block level '${lastIndexedLevel}'`,
                'provided by FooHandler',
                `configure '$.root.indexing.fromBlockLevel' to '${lastIndexedLevel + 1}' at most`,
            ]);
        });
    });

    describe('should continue from last indexed block if configured level is', () => {
        it.each([
            ['+1 from last indexed', 65],
            ['same as last indexed', 66],
            ['less than last indexed', 100],
        ])('%s', async (_desc, lastIndexedLevel) => {
            when(blockHandler.getLastIndexedBlock()).thenResolve({ level: lastIndexedLevel } as IndexedBlock);

            const level = await target.resolveLevel();

            expect(level).toBe(lastIndexedLevel + 1);
            logger.loggedSingle()
                .verify('Information', {
                    lastIndexedLevel,
                    '$.root.indexing.fromBlockLevel': 66,
                })
                .verifyMessage('Continuing indexing', 'provided by FooHandler');
        });
    });
});
