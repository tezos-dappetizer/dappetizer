import { Writable } from 'ts-essentials';
import { instance, mock, verify, when } from 'ts-mockito';

import { expectToThrowAsync, TestLogger } from '../../../../../test-utilities/mocks';
import {
    SameChainAsAlreadyIndexedVerifier,
} from '../../../src/block-source/verifiers/same-chain-as-already-indexed-verifier';
import { IndexerDatabaseHandlerWrapper } from '../../../src/client-indexers/wrappers/indexer-database-handler-wrapper';
import { TaquitoRpcBlock } from '../../../src/rpc-data/rpc-data-interfaces';

describe(SameChainAsAlreadyIndexedVerifier.name, () => {
    let target: SameChainAsAlreadyIndexedVerifier;
    let databaseHandler: IndexerDatabaseHandlerWrapper;
    let logger: TestLogger;

    let block: Writable<TaquitoRpcBlock>;
    let previousBlock: Writable<TaquitoRpcBlock> | null;

    const act = async () => target.verify(block, previousBlock);

    beforeEach(() => {
        databaseHandler = mock();
        logger = new TestLogger();
        target = new SameChainAsAlreadyIndexedVerifier(instance(databaseHandler), logger);

        block = { chain_id: 'ccc' } as TaquitoRpcBlock;
        previousBlock = { chain_id: 'ccc' } as TaquitoRpcBlock;
    });

    it('should pass if same chain id as previous block', async () => {
        await act();

        logger.verifyNothingLogged();
        verify(databaseHandler.getIndexedChainId()).never();
    });

    it('should pass if same chain id as indexed in db', async () => {
        previousBlock = null;
        when(databaseHandler.getIndexedChainId()).thenResolve('ccc');

        await act();

        logger.loggedSingle().verify('Information', { chainId: 'ccc' });
    });

    it('should pass if no previous chain id', async () => {
        previousBlock = null;

        await act();

        logger.loggedSingle().verify('Information', { chainId: 'ccc' });
        verify(databaseHandler.getIndexedChainId()).once();
    });

    it('should throw if different chain id in previous block', async () => {
        previousBlock!.chain_id = 'xxx';

        await runThrowTest();
    });

    it('should throw if different chain id in previous block', async () => {
        previousBlock = null;
        when(databaseHandler.getIndexedChainId()).thenResolve('xxx');

        await runThrowTest();
    });

    async function runThrowTest() {
        const error = await expectToThrowAsync(act);

        expect(error.message).toIncludeMultiple([
            `chain 'ccc'`,
            `chain 'xxx'`,
            'different Tezos Node',
        ]);
        logger.verifyNothingLogged();
    }
});
