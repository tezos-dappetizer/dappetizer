import { expectToThrow } from '../../../../../test-utilities/mocks';
import { MetadataExistenceVerifier } from '../../../src/block-source/verifiers/metadata-existence-verifier';
import { TaquitoRpcBlock } from '../../../src/rpc-data/rpc-data-interfaces';

describe(MetadataExistenceVerifier.name, () => {
    const target = new MetadataExistenceVerifier();

    it('should pass if block has metadata', () => {
        const block = { metadata: {} } as TaquitoRpcBlock;

        target.verify(block, null);
    });

    it('should throw if block is missing metadata', () => {
        const block = {} as TaquitoRpcBlock;

        const error = expectToThrow(() => target.verify(block, null));

        expect(error.message).toIncludeMultiple([
            `block and operation metadata`,
            `raising fromBlockLevel`,
            `configure tezosNode`,
            `'history_mode: archive'`,
        ]);
    });
});
