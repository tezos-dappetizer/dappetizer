import { Writable } from 'ts-essentials';
import { instance, mock, verify, when } from 'ts-mockito';

import { expectToThrowAsync, fixedMock } from '../../../../../test-utilities/mocks';
import { ContractIndexingBoost } from '../../../src/block-source/contract-boost/contract-indexing-boost';
import { ContractIndexingBoostFactory } from '../../../src/block-source/contract-boost/contract-indexing-boost-factory';
import { SameChainAsContractBoost } from '../../../src/block-source/verifiers/same-chain-as-contract-boost-verifier';
import { TaquitoRpcBlock } from '../../../src/rpc-data/rpc-data-interfaces';

describe(SameChainAsContractBoost.name, () => {
    let target: SameChainAsContractBoost;
    let boostFactory: ContractIndexingBoostFactory;

    let block: Writable<TaquitoRpcBlock>;
    let previousBlock: TaquitoRpcBlock | null;
    let boost: ContractIndexingBoost;

    const act = async () => target.verify(block, previousBlock);

    beforeEach(() => {
        boostFactory = mock();
        target = new SameChainAsContractBoost(instance(boostFactory));

        block = { chain_id: 'ccc' } as TaquitoRpcBlock;
        previousBlock = null;
        boost = fixedMock<ContractIndexingBoost>();

        when(boostFactory.create()).thenReturn(instance(boost));
        when(boost.name).thenReturn('tzwtf');
        when(boost.getChainId()).thenResolve('ccc');
    });

    it('should pass if same chain', async () => {
        await act();

        verify(boost.getChainId()).once();
    });

    it('should throw if different chain id', async () => {
        block.chain_id = 'bbb';

        const error = await expectToThrowAsync(act);

        expect(error.message).toIncludeMultiple([
            `'bbb'`,
            `'ccc'`,
            'contractBoost',
            'apiUrl',
        ]);
    });

    it('should not check if already checked (previous block exists)', async () => {
        previousBlock = 'mockedBlock' as any;

        await act();

        verify(boostFactory.create()).never();
    });

    it('should not check if boost disabled', async () => {
        when(boostFactory.create()).thenReturn(null);

        await act();
    });
});
