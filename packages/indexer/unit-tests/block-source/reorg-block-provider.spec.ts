import * as taquitoRpc from '@taquito/rpc';
import { asyncToArray, asyncWrap } from 'iter-tools';
import { DeepPartial, Writable } from 'ts-essentials';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { expectNextDone, expectNextValues, TestLogger } from '../../../../test-utilities/mocks';
import { AsyncIterableProvider } from '../../../utils/src/public-api';
import { ExplicitBlockLevelsResolver } from '../../src/block-source/explicit-blocks/explicit-block-levels-resolver';
import { ReorgBlockProvider } from '../../src/block-source/reorg-block-provider';
import { IndexedBlock } from '../../src/client-indexers/indexed-block';
import { IndexerDatabaseHandlerWrapper } from '../../src/client-indexers/wrappers/indexer-database-handler-wrapper';
import { IndexingConfig } from '../../src/config/indexing/indexing-config';
import { BlockDownloader } from '../../src/helpers/block-downloader';
import { RollBackBlockHandler } from '../../src/helpers/roll-back-block-handler';
import { TaquitoRpcBlock } from '../../src/rpc-data/rpc-data-interfaces';

describe(ReorgBlockProvider.name, () => {
    let target: AsyncIterableProvider<TaquitoRpcBlock>;
    let nextProvider: AsyncIterableProvider<TaquitoRpcBlock>;
    let blockHandler: IndexerDatabaseHandlerWrapper;
    let blockDownloader: BlockDownloader;
    let rollBackBlockHandler: RollBackBlockHandler;
    let config: Writable<IndexingConfig>;
    let explicitBlocksResolver: ExplicitBlockLevelsResolver;
    let logger: TestLogger;

    beforeEach(() => {
        [nextProvider, blockHandler, blockDownloader, rollBackBlockHandler, explicitBlocksResolver] = [mock(), mock(), mock(), mock(), mock()];
        config = {} as typeof config;
        logger = new TestLogger();
        target = new ReorgBlockProvider(
            instance(nextProvider),
            instance(blockHandler),
            instance(blockDownloader),
            instance(rollBackBlockHandler),
            config,
            instance(explicitBlocksResolver),
            logger,
        );
    });

    it.each([
        ['null', null],
        ['existing', { hash: 'h0', level: 10 }],
    ])('should just pass blocks if no reorg and %s last indexed block', async (_desc, lastIndexedBlock) => {
        const nextBlocks = setupNextProvider([
            { hash: 'h1', header: { predecessor: 'h0', level: 11 } } as TaquitoRpcBlock,
            { hash: 'h2', header: { predecessor: 'h1', level: 12 } } as TaquitoRpcBlock,
            { hash: 'h3', header: { predecessor: 'h2', level: 13 } } as TaquitoRpcBlock,
        ]);
        when(blockHandler.getLastIndexedBlock()).thenResolve(lastIndexedBlock);

        const blocks = await asyncToArray(target.iterate());

        expect(blocks).toEqual(nextBlocks);
        verifyRollbacks([]);
    });

    it('should roll back block on reorg', async () => {
        const nextBlocks = setupNextProvider([
            { hash: 'h1', header: { predecessor: 'h0', level: 11 } } as TaquitoRpcBlock,
            { hash: 'h2', header: { predecessor: 'h1', level: 12 } } as TaquitoRpcBlock,
            { hash: 'x3', header: { predecessor: 'x2', level: 13 } } as TaquitoRpcBlock,
        ]);
        when(blockHandler.getLastIndexedBlock()).thenResolve({ hash: 'h0', level: 10 });

        const iterator = target.iterate();

        await expectNextValues(iterator, [nextBlocks[0], nextBlocks[1]]);
        verify(rollBackBlockHandler.rollBack(anything())).never();

        const reorgedBlock = setupRpcBlock({ hash: 'x2', header: { predecessor: 'h1', level: 12 } });
        when(rollBackBlockHandler.rollBack(anything())).thenCall(() => {
            when(blockHandler.getLastIndexedBlock()).thenResolve({ hash: 'h1', level: 11 });
        });

        await expectNextValues(iterator, [reorgedBlock]);
        verifyRollbacks([{ block: { hash: 'h2', level: 12 }, conflict: { hash: 'x3', level: 13, predecessor: 'x2' } }]);

        await expectNextValues(iterator, [nextBlocks[2]]);
        await expectNextDone(iterator);
    });

    it('should roll back block on reorg if first indexed is reorged', async () => {
        const nextBlocks = setupNextProvider([
            { hash: 'h1', header: { predecessor: 'h0', level: 11 } } as TaquitoRpcBlock,
            { hash: 'x2', header: { predecessor: 'x1', level: 12 } } as TaquitoRpcBlock,
        ]);

        const iterator = target.iterate();

        await expectNextValues(iterator, [nextBlocks[0]]);
        verify(rollBackBlockHandler.rollBack(anything())).never();

        const reorgedBlock = setupRpcBlock({ hash: 'x1', header: { predecessor: 'x0', level: 11 } });

        await expectNextValues(iterator, [reorgedBlock]);
        verifyRollbacks([{ block: { hash: 'h1', level: 11 }, conflict: { hash: 'x2', level: 12, predecessor: 'x1' } }]);

        await expectNextValues(iterator, [nextBlocks[1]]);
        await expectNextDone(iterator);
    });

    it('should roll back multiple blocks on reorg if needed', async () => {
        const nextBlocks = setupNextProvider([
            { hash: 'h1', header: { predecessor: 'h0', level: 11 } } as TaquitoRpcBlock,
            { hash: 'h2', header: { predecessor: 'h1', level: 12 } } as TaquitoRpcBlock,
            { hash: 'h3', header: { predecessor: 'h2', level: 13 } } as TaquitoRpcBlock,
            { hash: 'x4', header: { predecessor: 'x3', level: 14 } } as TaquitoRpcBlock,
        ]);
        when(blockHandler.getLastIndexedBlock()).thenResolve({ hash: 'h0', level: 10 });

        const iterator = target.iterate();

        await expectNextValues(iterator, [nextBlocks[0], nextBlocks[1], nextBlocks[2]]);
        verify(rollBackBlockHandler.rollBack(anything())).never();

        const reorgedBlock1 = setupRpcBlock({ hash: 'x3', header: { predecessor: 'x2', level: 13 } });
        when(rollBackBlockHandler.rollBack(deepEqual({ hash: 'h3', level: 13 }))).thenCall(() => {
            when(blockHandler.getLastIndexedBlock()).thenResolve({ hash: 'h2', level: 12 });
        });
        const reorgedBlock2 = setupRpcBlock({ hash: 'x2', header: { predecessor: 'h1', level: 12 } });
        when(rollBackBlockHandler.rollBack(deepEqual({ hash: 'h2', level: 12 }))).thenCall(() => {
            when(blockHandler.getLastIndexedBlock()).thenResolve({ hash: 'h1', level: 11 });
        });

        await expectNextValues(iterator, [reorgedBlock2]);
        verifyRollbacks([
            { block: { hash: 'h3', level: 13 }, conflict: { hash: 'x4', level: 14, predecessor: 'x3' } },
            { block: { hash: 'h2', level: 12 }, conflict: { hash: 'x3', level: 13, predecessor: 'x2' } },
        ]);

        await expectNextValues(iterator, [reorgedBlock1, nextBlocks[3]]);
        await expectNextDone(iterator);
    });

    shouldNotHandleReorgIf('contract boost enabled', () => {
        config.contractBoost = {} as any;
    });

    shouldNotHandleReorgIf('explicit blocks specified', () => {
        when(explicitBlocksResolver.resolveLevels()).thenReturn([1, 2]);
    });

    function shouldNotHandleReorgIf(desc: string, setup: () => void) {
        it(`should not handle reorg if ${desc} b/c boost does not index all blocks`, () => {
            when(nextProvider.iterate()).thenReturn('mockedIterable' as any);
            setup();

            const iterable = target.iterate();

            expect(iterable).toBe('mockedIterable');
        });
    }

    function setupNextProvider(blocks: TaquitoRpcBlock[]) {
        when(nextProvider.iterate()).thenReturn(asyncWrap(blocks));
        return blocks;
    }

    function setupRpcBlock(blockValues: DeepPartial<taquitoRpc.BlockResponse>) {
        const block = blockValues as taquitoRpc.BlockResponse;
        when(blockDownloader.download(block.header.level)).thenResolve(block);
        return block;
    }

    function verifyRollbacks(rollbacks: { block: IndexedBlock; conflict: { predecessor: string; hash: string; level: number } }[]) {
        verify(rollBackBlockHandler.rollBack(anything())).times(rollbacks.length);
        logger.verifyLoggedCount(2 * rollbacks.length);

        let logIndex = 0;
        for (const rollback of rollbacks) {
            verify(rollBackBlockHandler.rollBack(deepEqual(rollback.block))).once();
            logger.logged(logIndex++)
                .verify('Information', { rolledBackBlock: rollback.block, newBlock: rollback.conflict })
                .verifyMessage('Rolling');
            logger.logged(logIndex++)
                .verify('Information', { rolledBackBlock: rollback.block })
                .verifyMessage('Successfully');
        }

        verify(blockDownloader.download(anything())).times(rollbacks.length);
        verify(blockHandler.getLastIndexedBlock()).times(1 + rollbacks.length);
    }
});
