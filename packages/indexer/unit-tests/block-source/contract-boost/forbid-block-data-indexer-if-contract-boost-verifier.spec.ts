import { Writable } from 'ts-essentials';

import { expectToThrow } from '../../../../../test-utilities/mocks';
import {
    ForbidBlockDataIndexerIfContractBoostVerifier,
} from '../../../src/block-source/contract-boost/forbid-block-data-indexer-if-contract-boost-verifier';
import { BlockDataIndexer } from '../../../src/client-indexers/block-data-indexer';
import { IndexerModule } from '../../../src/client-indexers/indexer-module';
import { PartialIndexerModuleVerifier } from '../../../src/client-indexers/verification/partial-indexer-module-verifier';
import { ContractsBoostConfig, IndexingConfig } from '../../../src/config/indexing/indexing-config';

describe(ForbidBlockDataIndexerIfContractBoostVerifier.name, () => {
    let target: PartialIndexerModuleVerifier;
    let config: Writable<IndexingConfig>;
    let module: Writable<IndexerModule<unknown>>;

    const act = () => target.verify(module);

    beforeEach(() => {
        config = {} as typeof config;
        module = {} as typeof module;
        target = new ForbidBlockDataIndexerIfContractBoostVerifier(config);
    });

    it('should throw if contract boost enabled and some block data indexers', () => {
        config.contractBoost = { type: 'Foo' as any } as ContractsBoostConfig;
        module.blockDataIndexers = [{} as BlockDataIndexer<unknown>];

        const error = expectToThrow(act);

        expect(error.message).toIncludeMultiple([
            `'Foo'`,
            `blockDataIndexers`,
        ]);
    });

    it('should just pass if nothing', act);

    it('should just pass if only contract boost enabled', () => {
        config.contractBoost = {} as ContractsBoostConfig;
    });

    it.each([
        ['empty', []],
        ['some', [{} as BlockDataIndexer<unknown>]],
    ])('should just pass if %s block data indexers', (_desc, blockDataIndexers) => {
        module.blockDataIndexers = blockDataIndexers;

        act();
    });
});
