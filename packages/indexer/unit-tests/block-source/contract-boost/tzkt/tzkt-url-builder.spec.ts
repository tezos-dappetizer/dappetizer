import { buildTzktUrl } from '../../../../src/block-source/contract-boost/tzkt/tzkt-url-builder';

describe(buildTzktUrl.name, () => {
    it('should build url correctly', () => {
        const url = buildTzktUrl('http://tzkt.wtf/root/', '/memes/lol', {
            num: 123,
            str: 'abc',
            arr: ['x', 'y', 'z'],
        });

        expect(url).toBe('http://tzkt.wtf/root/memes/lol?num=123&str=abc&arr=x,y,z');
    });

    it('should build url correctly if no query', () => {
        const url = buildTzktUrl('http://tzkt.wtf/root', 'memes/lol');

        expect(url).toBe('http://tzkt.wtf/root/memes/lol');
    });
});
