import { JsonFetcher } from '../../../../../utils/src/public-api';
import { TzktIndexingBoost } from '../../../../src/block-source/contract-boost/tzkt/tzkt-indexing-boost';
import { TzktIndexingBoostFactory } from '../../../../src/block-source/contract-boost/tzkt/tzkt-indexing-boost-factory';
import { TzktContractsBoostConfig } from '../../../../src/config/indexing/indexing-config';

describe(TzktIndexingBoostFactory.name, () => {
    let target: TzktIndexingBoostFactory;
    let fetcher: JsonFetcher;

    beforeEach(() => {
        fetcher = 'mockedFetcher' as any;
        target = new TzktIndexingBoostFactory(fetcher);
    });

    it('should create boost correctly', () => {
        const config = { apiUrl: 'http://tzkt.wtf/' } as TzktContractsBoostConfig;
        const contractAddresses = ['KT1Bn8L42sBP1EBRUmATXWeBNTGPJthdDTVf', 'KT1Ha4yFVeyzw6KRAdkzq6TxDHB97KG4pZe8'];

        const boost = target.create(config, contractAddresses);

        expect(boost).toBeInstanceOf(TzktIndexingBoost);
        expect(boost).toMatchObject({ fetcher, config, contractAddresses });
    });
});
