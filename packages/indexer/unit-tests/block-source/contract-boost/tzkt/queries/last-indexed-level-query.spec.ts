import { describeMemberFactory, expectToThrow } from '../../../../../../../test-utilities/mocks';
import {
    LastIndexedLevelQuery,
    TzktBlock,
} from '../../../../../src/block-source/contract-boost/tzkt/queries/last-indexed-level-query';

describe(LastIndexedLevelQuery.name, () => {
    let target: LastIndexedLevelQuery;

    beforeEach(() => {
        target = new LastIndexedLevelQuery('http://tzkt/api');
    });

    const describeMember = describeMemberFactory<typeof target>();

    describeMember('description', () => {
        it('should be defined', () => {
            expect(target.description).toMatch(/\w/u);
        });
    });

    describeMember('url', () => {
        it('should be correct', () => {
            expect(target.url).toBe('http://tzkt/api/v1/head');
        });
    });

    describeMember('transformResponse', () => {
        it('should pass if valid block', () => {
            const rawBlock: TzktBlock = { level: 123, chainId: 'ccc' };

            const block = target.transformResponse(rawBlock);

            expect(block).toBe(rawBlock);
        });

        it.each([
            ['undefined', undefined],
            ['null', null],
            ['not object', 'wtf'],
            ['level is undefined', { chainId: 'ccc' }],
            ['level is null', { level: null, chainId: 'ccc' }],
            ['level is not number', { level: 'wtf', chainId: 'ccc' }],
            ['level is below zero', { level: -1, chainId: 'ccc' }],
            ['chainId is undefined', { level: 123 }],
            ['chainId is null', { level: 123, chainId: null }],
            ['chainId is not string', { level: 123, chainId: 123 }],
            ['chainId is white-space', { level: 123, chainId: '  ' }],
        ])('should throw if %s', (_desc, rawBlock: any) => {
            expectToThrow(() => target.transformResponse(rawBlock));
        });
    });
});
