import { asyncToArray, asyncWrap } from 'iter-tools';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { expectToThrowAsync } from '../../../../test-utilities/mocks';
import { AsyncIterableProvider } from '../../../utils/src/public-api';
import { BlockVerifier } from '../../src/block-source/verifiers/block-verifier';
import { VerifyBlockProvider } from '../../src/block-source/verify-block-provider';
import { TaquitoRpcBlock } from '../../src/rpc-data/rpc-data-interfaces';

describe(VerifyBlockProvider.name, () => {
    let target: VerifyBlockProvider;
    let nextProvider: AsyncIterableProvider<TaquitoRpcBlock>;
    let verifier1: BlockVerifier;
    let verifier2: BlockVerifier;

    let block1: TaquitoRpcBlock;
    let block2: TaquitoRpcBlock;

    const act = async () => asyncToArray(target.iterate());

    beforeEach(() => {
        [nextProvider, verifier1, verifier2] = [mock(), mock(), mock()];
        target = new VerifyBlockProvider(instance(nextProvider), [instance(verifier1), instance(verifier2)]);

        block1 = { hash: 'ha1', header: { level: 11 } } as TaquitoRpcBlock;
        block2 = { hash: 'ha2', header: { level: 22 } } as TaquitoRpcBlock;
        when(nextProvider.iterate()).thenReturn(asyncWrap([block1, block2]));
    });

    it('should just iterate blocks', async () => {
        const blocks = await act();

        expect(blocks).toEqual([block1, block2]);
        verify(verifier1.verify(anything(), anything())).times(2);
        verify(verifier1.verify(block1, null)).once();
        verify(verifier1.verify(block2, block1)).once();
        verify(verifier2.verify(anything(), anything())).times(2);
        verify(verifier2.verify(block1, null)).once();
        verify(verifier2.verify(block2, block1)).once();
    });

    it('should wrap error from verifier', async () => {
        when(verifier1.verify(block1, anything())).thenReject(new Error('oups'));

        const error = await expectToThrowAsync(act);

        expect(error.message).toIncludeMultiple([
            `level: 11`,
            `hash: 'ha1'`,
            'RPC',
            'oups',
        ]);
    });
});
