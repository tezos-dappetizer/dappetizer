import * as taquitoRpc from '@taquito/rpc';
import { asyncToArray, asyncWrap } from 'iter-tools';
import { anything, instance, mock, when } from 'ts-mockito';

import { expectNextValues, expectToThrowAsync, PromiseSource, verifyCalled } from '../../../../test-utilities/mocks';
import { AsyncIterableProvider } from '../../../utils/src/public-api';
import { BlockIndexingHealthState } from '../../src/block-processing/health/block-indexing-health-state';
import { DownloadBlockByLevelProvider } from '../../src/block-source/download-block-by-level-provider';
import { IndexingConfig } from '../../src/config/indexing/indexing-config';
import { BlockDownloader } from '../../src/helpers/block-downloader';
import { TaquitoRpcBlock } from '../../src/rpc-data/block/block';

describe(DownloadBlockByLevelProvider.name, () => {
    let target: DownloadBlockByLevelProvider;
    let nextProvider: AsyncIterableProvider<number>;
    let blockDownloader: BlockDownloader;
    let rpcClient: taquitoRpc.RpcClient;
    let healthState: BlockIndexingHealthState;

    beforeEach(() => {
        [nextProvider, blockDownloader, rpcClient] = [mock(), mock(), mock()];
        const config = { blockQueueSize: 3 } as IndexingConfig;
        healthState = new BlockIndexingHealthState();
        target = new DownloadBlockByLevelProvider(instance(nextProvider), instance(blockDownloader), config, instance(rpcClient), healthState);

        when(blockDownloader.download(anything())).thenReturn(new Promise<TaquitoRpcBlock>(_r => {}));
        when(rpcClient.getRpcUrl()).thenReturn('http://tezos.node');
    });

    it('should download blocks by provided levels', async () => {
        when(nextProvider.iterate()).thenReturn(asyncWrap([10, 11, 12, 13, 14]));
        const expectedBlocks = [setupBlock(10), setupBlock(11), setupBlock(12), setupBlock(13), setupBlock(14)];

        const blocks = await asyncToArray(target.iterate());

        expect(blocks).toEqual(expectedBlocks);
    });

    it('should download configured number of blocks on background', async () => {
        const levelsIterated = new PromiseSource<void>();
        when(nextProvider.iterate()).thenReturn(async function *mockIterable() {
            await Promise.resolve();
            yield* [10, 11, 12, 13];
            levelsIterated.resolve();
            yield* [14, 15, 16];
        }());
        const block10 = setupBlock(10);

        const iterator = target.iterate();

        await expectNextValues(iterator, [block10]);
        await levelsIterated.promise;
        verifyCalled(blockDownloader.download)
            .with(10)
            .thenWith(11)
            .thenWith(12)
            .thenWith(13)
            .thenNoMore();
        expect(healthState.blockBeingProcessed).toEqual({ level: 10, hash: 'hash-10' });
    });

    it('should throw if block download error', async () => {
        when(nextProvider.iterate()).thenReturn(asyncWrap([10, 11]));
        const block10 = setupBlock(10);
        const blockError = new Error('oups');
        when(blockDownloader.download(11)).thenReject(blockError);

        const iterator = target.iterate();

        await expectNextValues(iterator, [block10]);
        const error = await expectToThrowAsync(async () => iterator.next());
        expect(error).toBe(blockError);
        expect(healthState.blockBeingProcessed).toEqual({ level: 11 });
    });

    it('should throw if level iteration error', async () => {
        const iterationError = new Error('oups');
        when(nextProvider.iterate()).thenReturn(async function *mockIterable() {
            await Promise.resolve();
            yield 10;
            throw iterationError;
        }());
        const block10 = setupBlock(10);

        const iterator = target.iterate();

        await expectNextValues(iterator, [block10]);
        const error = await expectToThrowAsync(async () => iterator.next());
        expect(error).toBe(iterationError);
        expect(healthState.blockBeingProcessed).toBeNull();
    });

    it.each([
        ['block is not object', 'wtf' as unknown as TaquitoRpcBlock],
        ['hash is not string', { hash: {} as any, header: { level: 10 } } as TaquitoRpcBlock],
        ['hash is white-space', { hash: ' \t', header: { level: 10 } } as TaquitoRpcBlock],
        ['header is not object', { hash: 'hh', header: 'wtf' as any } as TaquitoRpcBlock],
        ['level is not number', { hash: 'hh', header: { level: 'wtf' as any } } as TaquitoRpcBlock],
        ['level is negative', { hash: 'hh', header: { level: -66 } } as TaquitoRpcBlock],
    ])('should throw if %s', async (_desc, block) => {
        when(nextProvider.iterate()).thenReturn(asyncWrap([10]));
        when(blockDownloader.download(10)).thenResolve(block);

        const error = await expectToThrowAsync(async () => asyncToArray(target.iterate()));

        expect(error.message).toIncludeMultiple([
            'well-formed',
            'http://tezos.node/chains/main/blocks/10',
            JSON.stringify(block),
        ]);
        expect(healthState.blockBeingProcessed).toEqual({ level: 10 });
    });

    function setupBlock(level: number) {
        const block = {
            hash: `hash-${level}`,
            header: { level },
        } as TaquitoRpcBlock;

        when(blockDownloader.download(level)).thenResolve(block);
        return block;
    }
});
