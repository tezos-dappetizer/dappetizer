import { ExplicitBlockLevelsResolver } from '../../../src/block-source/explicit-blocks/explicit-block-levels-resolver';

describe(ExplicitBlockLevelsResolver.name, () => {
    it('should return explicit blocks levels if specified', () => {
        const target = new ExplicitBlockLevelsResolver([
            { fromLevel: 10, toLevel: 10 },
            { fromLevel: 3, toLevel: 7 },
            { fromLevel: 12, toLevel: 13 },
            { fromLevel: 5, toLevel: 6 },
        ]);

        const levels = target.resolveLevels();

        expect(levels).toEqual<number[]>([3, 4, 5, 6, 7, 10, 12, 13]);
    });

    it('should return iterable from next provider if no explicit blocks', () => {
        const target = new ExplicitBlockLevelsResolver(undefined);

        const levels = target.resolveLevels();

        expect(levels).toBeNull();
    });
});
