import { asyncToArray } from 'iter-tools';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { DbContext, TestLogger, verifyCalled } from '../../../../../test-utilities/mocks';
import { AsyncIterableProvider } from '../../../../utils/src/public-api';
import { ExplicitBlockLevelsProvider } from '../../../src/block-source/explicit-blocks/explicit-block-levels-provider';
import { ExplicitBlockLevelsResolver } from '../../../src/block-source/explicit-blocks/explicit-block-levels-resolver';
import { IndexedBlock } from '../../../src/client-indexers/indexed-block';
import { IndexerDatabaseHandlerWrapper } from '../../../src/client-indexers/wrappers/indexer-database-handler-wrapper';
import { RollBackBlockHandler } from '../../../src/helpers/roll-back-block-handler';

describe(ExplicitBlockLevelsProvider.name, () => {
    let target: AsyncIterableProvider<number>;
    let nextProvider: AsyncIterableProvider<number>;
    let explicitLevelsResolver: ExplicitBlockLevelsResolver;
    let rollBackBlockHandler: RollBackBlockHandler;
    let databaseHandler: IndexerDatabaseHandlerWrapper<DbContext>;
    let logger: TestLogger;

    beforeEach(() => {
        [nextProvider, explicitLevelsResolver, rollBackBlockHandler, databaseHandler] = [mock(), mock(), mock(), mock()];
        logger = new TestLogger();
        target = new ExplicitBlockLevelsProvider(
            instance(nextProvider),
            instance(explicitLevelsResolver),
            instance(rollBackBlockHandler),
            instance(databaseHandler),
            logger,
        );
    });

    it('should re-index explicitly specified blocks', async () => {
        const block7: IndexedBlock = 'mockedBlock7' as any;
        when(explicitLevelsResolver.resolveLevels()).thenReturn([4, 7, 8]);
        when(databaseHandler.getIndexedBlock(7)).thenResolve(block7);
        when(rollBackBlockHandler.rollBack(anything())).thenCall(() => logger.logDebug('TEST'));

        const levels = await asyncToArray(target.iterate());

        expect(levels).toEqual([4, 7, 8]);
        verifyCalled(nextProvider.iterate).never();
        verifyCalled(databaseHandler.getIndexedBlock).with(4).thenWith(7).thenWith(8).thenNoMore();
        verifyCalled(rollBackBlockHandler.rollBack).onceWith(block7);
        logger.logged(0).verifyMessage('Re-indexing').verify('Information', { levels });
        logger.logged(1).verifyMessage('No need').verify('Information', { level: 4 });
        logger.logged(2).verifyMessage('Rolling').verify('Information', { rolledBackBlock: block7 });
        logger.logged(3).verifyMessage('TEST').verify('Debug', { rolledBackBlock: block7 });
        logger.logged(4).verifyMessage('rolled').verify('Information', { rolledBackBlock: block7 });
        logger.logged(5).verifyMessage('No need').verify('Information', { level: 8 });
        logger.verifyLoggedCount(6);
    });

    it('should return iterable from next if no explicit blocks specified', () => {
        when(nextProvider.iterate()).thenReturn('mockedIterable' as any);

        const iterable = target.iterate();

        expect(iterable).toBe('mockedIterable');
        verify(explicitLevelsResolver.resolveLevels()).once();
        logger.verifyNothingLogged();
    });
});
