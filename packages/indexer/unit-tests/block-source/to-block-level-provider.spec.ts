import { asyncToArray, asyncWrap } from 'iter-tools';
import { Writable } from 'ts-essentials';
import { instance, mock, when } from 'ts-mockito';

import { AsyncIterableProvider } from '../../../utils/src/public-api';
import { ToBlockLevelProvider } from '../../src/block-source/to-block-level-provider';
import { IndexingConfig } from '../../src/config/indexing/indexing-config';

describe(ToBlockLevelProvider.name, () => {
    let target: AsyncIterableProvider<number>;
    let nextProvider: AsyncIterableProvider<number>;
    let config: Writable<IndexingConfig>;

    beforeEach(() => {
        nextProvider = mock();
        config = {} as typeof config;
        target = new ToBlockLevelProvider(instance(nextProvider), config);
    });

    it('should cut levels when toBlockLevel is reached', async () => {
        config.toBlockLevel = 10;
        when(nextProvider.iterate()).thenReturn(asyncWrap([7, 8, 9, 10, 11, 12, 13]));

        const levels = await asyncToArray(target.iterate());

        expect(levels).toEqual<number[]>([7, 8, 9, 10]);
    });

    it('should pass next iterable if no toBlockLevel configured', () => {
        const nextIterable = asyncWrap([1, 2, 3, 4, 5]);
        when(nextProvider.iterate()).thenReturn(nextIterable);

        const iterable = target.iterate();

        expect(iterable).toBe(nextIterable);
    });
});
