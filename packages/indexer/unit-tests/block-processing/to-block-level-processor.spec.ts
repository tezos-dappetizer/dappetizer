import { DappetizerBug } from '@tezos-dappetizer/utils';
import { Writable } from 'ts-essentials';
import { instance, mock, verify } from 'ts-mockito';

import { expectToThrowAsync, TestLogger } from '../../../../test-utilities/mocks';
import { keyof } from '../../../utils/src/public-api';
import { AsyncProcessor } from '../../src/block-processing/async-processor';
import { ToBlockLevelProcessor } from '../../src/block-processing/to-block-level-processor';
import { IndexingConfig } from '../../src/config/indexing/indexing-config';
import { BlockLevelIndexingListener } from '../../src/helpers/block-level-indexing-listener';
import { Block } from '../../src/rpc-data/rpc-data-interfaces';

describe(ToBlockLevelProcessor.name, () => {
    let target: AsyncProcessor<Block>;
    let nextProcessor: AsyncProcessor<Block>;
    let config: Writable<IndexingConfig>;
    let listener1: BlockLevelIndexingListener;
    let listener2: BlockLevelIndexingListener;
    let logger: TestLogger;

    let block: Block;

    const act = async () => target.process(block);

    beforeEach(() => {
        [nextProcessor, listener1, listener2] = [mock(), mock(), mock()];
        config = {} as typeof config;
        logger = new TestLogger();
        target = new ToBlockLevelProcessor(instance(nextProcessor), config, [instance(listener1), instance(listener2)], logger);

        block = { level: 10 } as Block;
    });

    it(`should call listeners after ${keyof<typeof config>('toBlockLevel')} is indexed`, async () => {
        config.toBlockLevel = 10;

        await act();

        verify(listener1.onToBlockLevelReached()).calledAfter(nextProcessor.process(block));
        verify(listener2.onToBlockLevelReached()).calledAfter(nextProcessor.process(block));
        logger.loggedSingle().verify('Information', { toBlockLevel: 10 });
    });

    it.each([
        ['not configured', null],
        ['above block level', 11],
    ])(`should just pass if ${keyof<typeof config>('toBlockLevel')} is %s`, async (_desc, toBlockLevel) => {
        config.toBlockLevel = toBlockLevel;

        await act();

        verify(nextProcessor.process(block)).once();
        verify(listener1.onToBlockLevelReached()).never();
        verify(listener2.onToBlockLevelReached()).never();
        logger.verifyNothingLogged();
    });

    it(`should throw if block level above  ${keyof<typeof config>('toBlockLevel')}`, async () => {
        config.toBlockLevel = 5;

        await expectToThrowAsync(act, DappetizerBug);
    });
});
