import { Gauge } from 'prom-client';
import { Writable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { expectToThrowAsync, TestLogger } from '../../../../test-utilities/mocks';
import { ENABLE_TRACE } from '../../../utils/src/public-api';
import { AsyncProcessor } from '../../src/block-processing/async-processor';
import { BlockPreparationProcessor } from '../../src/block-processing/block-preparation-processor';
import { IndexerMetrics } from '../../src/helpers/indexer-metrics';
import { scopedLogKeys } from '../../src/helpers/scoped-log-keys';
import { BlockConverter } from '../../src/rpc-data/block/block-converter';
import { Block, TaquitoRpcBlock } from '../../src/rpc-data/rpc-data-interfaces';

describe(BlockPreparationProcessor.name, () => {
    let target: AsyncProcessor<TaquitoRpcBlock>;
    let nextProcessor: AsyncProcessor<Block>;
    let converter: BlockConverter;
    let blockLevelGauge: IndexerMetrics['blockLevel'];
    let logger: TestLogger;

    let rpcBlock: TaquitoRpcBlock;
    let block: Block;

    const act = async () => target.process(rpcBlock);

    beforeEach(() => {
        [nextProcessor, converter] = [mock(), mock()];
        const metrics = {} as Writable<IndexerMetrics>;
        logger = new TestLogger();
        target = new BlockPreparationProcessor(instance(nextProcessor), instance(converter), metrics, logger);

        blockLevelGauge = mock(Gauge);
        metrics.blockLevel = instance(blockLevelGauge);

        rpcBlock = { hash: 'haha', header: { level: 111 } } as TaquitoRpcBlock;
        block = { level: 222 } as Block;
        when(converter.convert(rpcBlock)).thenReturn(block);
    });

    it('should convert block and pass processing', async () => {
        await act();

        expect(rpcBlock).toBeFrozen();
        verify(nextProcessor.process(block)).once();
        verify(blockLevelGauge.set(222)).once();
    });

    it.each(['convert', 'process'] as const)('should wrap %s error', async testCase => {
        if (testCase === 'convert') {
            when(converter.convert(rpcBlock)).thenThrow(new Error('oups'));
        } else {
            when(nextProcessor.process(block)).thenThrow(new Error('oups'));
        }

        const error = await expectToThrowAsync(act);

        expect(error.message).toIncludeMultiple([
            'Failed indexing',
            `level: 111`,
            `hash: 'haha'`,
            'oups',
        ]);
    });

    it.each([true, false])('should put block to log scope when tracing %s', async isLoggerTracing => {
        logger.enableLevel('Trace', isLoggerTracing);
        when(converter.convert(anything())).thenCall(() => {
            logger.logDebug('CONVERT');
            return block;
        });
        when(nextProcessor.process(anything())).thenCall(() => {
            logger.logDebug('PROCESS');
        });

        await act();

        const expectedData = { [scopedLogKeys.INDEXED_BLOCK]: { level: 111, hash: 'haha' } };
        logger.logged(0).verifyMessage('CONVERT').verify('Debug', expectedData);
        logger.logged(1).verifyMessage('prepared').verify('Debug', {
            ...expectedData,
            blockFull: isLoggerTracing ? block : ENABLE_TRACE,
        });
        logger.logged(2).verifyMessage('PROCESS').verify('Debug', expectedData);
        logger.verifyLoggedCount(3);
    });
});
