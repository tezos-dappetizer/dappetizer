import { asyncWrap } from 'iter-tools';
import { StrictOmit } from 'ts-essentials';
import { instance, mock, when } from 'ts-mockito';

import { expectToThrowAsync, TestClock, TestLogger, verifyCalled } from '../../../../test-utilities/mocks';
import { AsyncIterableProvider } from '../../../utils/src/public-api';
import { AsyncProcessor } from '../../src/block-processing/async-processor';
import { BlockIndexingProcessor } from '../../src/block-processing/block-indexing-processor';
import { BlockIndexingHealthState } from '../../src/block-processing/health/block-indexing-health-state';
import { BlockInfoProvider } from '../../src/helpers/block-info-provider';
import { TaquitoRpcBlock } from '../../src/rpc-data/rpc-data-interfaces';

describe(BlockIndexingProcessor.name, () => {
    let target: BlockIndexingProcessor;
    let blockProvider: AsyncIterableProvider<TaquitoRpcBlock>;
    let blockProcessor: AsyncProcessor<TaquitoRpcBlock>;
    let infoProvider: BlockInfoProvider;
    let healthState: BlockIndexingHealthState;
    let clock: TestClock;
    let logger: TestLogger;

    let block1: TaquitoRpcBlock;
    let block2: TaquitoRpcBlock;

    beforeEach(() => {
        [blockProvider, blockProcessor, infoProvider] = [mock(), mock(), mock()];
        healthState = new BlockIndexingHealthState();
        clock = new TestClock();
        logger = new TestLogger();
        target = new BlockIndexingProcessor(
            instance(blockProvider),
            instance(blockProcessor),
            instance(infoProvider),
            healthState,
            clock,
            logger,
        );

        block1 = {} as TaquitoRpcBlock;
        block2 = {} as TaquitoRpcBlock;

        when(blockProvider.iterate()).thenReturn(asyncWrap([block1, block2]));
        when(infoProvider.getInfo(block1)).thenReturn('info-1');
        when(infoProvider.getInfo(block2)).thenReturn('info-2');
    });

    it('should continuously process items', async () => {
        when(blockProcessor.process(block1)).thenCall(() => {
            clock.tick(100);
        });

        await target.process();

        verifyCalled(blockProcessor.process)
            .with(block1)
            .thenWith(block2)
            .thenNoMore();
        expect(healthState).toEqual<typeof healthState>({
            blockBeingProcessed: 'successfully finished',
            error: null,
            lastSuccess: { block: 'info-2', indexedOn: clock.nowDate },
        });
        logger.loggedSingle().verify('Information').verifyMessage('Starting');
    });

    it('should expose last error if failed iterating starting block', async () => {
        when(blockProvider.iterate()).thenReturn((async function *() {
            yield* [] as TaquitoRpcBlock[];
            await Promise.reject(new Error('oups'));
        })());

        await runThrowTest({
            blockBeingProcessed: 'determining the starting block',
            lastSuccess: null,
        });
    });

    it('should expose last error if failed iterating next block', async () => {
        when(blockProvider.iterate()).thenReturn((async function *() {
            yield* [block1, block2];
            clock.tick(66);
            await Promise.reject(new Error('oups'));
        })());

        await runThrowTest({
            blockBeingProcessed: 'determining the next block',
            lastSuccess: { block: 'info-2', indexedOn: clock.initialDate },
        });
    });

    it('should expose last error if failed processing', async () => {
        when(blockProcessor.process(block2)).thenCall(() => {
            clock.tick(77);
            throw new Error('oups');
        });

        await runThrowTest({
            blockBeingProcessed: 'info-2',
            lastSuccess: { block: 'info-1', indexedOn: clock.initialDate },
        });
    });

    async function runThrowTest(expectedState: StrictOmit<BlockIndexingHealthState, 'error'>) {
        const error = await expectToThrowAsync(async () => target.process());

        expect(error.message).toIncludeMultiple(['Failed', 'oups']);
        expect(healthState).toEqual({ error: new Error('oups'), ...expectedState });
    }

    it('should remove last error if next success', async () => {
        when(blockProvider.iterate()).thenReturn(asyncWrap([block1]));
        healthState.error = new Error('oups');

        await target.process();

        expect(healthState.error).toBeNull();
    });
});
