import { BlockIndexingHealthState } from '../../../src/block-processing/health/block-indexing-health-state';

describe(BlockIndexingHealthState.name, () => {
    it('should expose empty defaults', () => {
        const target = new BlockIndexingHealthState();

        expect(target.blockBeingProcessed).toBeNull();
        expect(target.error).toBeNull();
        expect(target.lastSuccess).toBeNull();
    });
});
