import { Writable } from 'ts-essentials';

import { TestClock } from '../../../../../test-utilities/mocks';
import { HealthStatus } from '../../../../utils/src/public-api';
import { addMillis } from '../../../../utils/src/time/date-utils';
import { BlockIndexingHealthCheck, reasons } from '../../../src/block-processing/health/block-indexing-health-check';
import { BlockIndexingHealthState } from '../../../src/block-processing/health/block-indexing-health-state';
import { IndexingConfig } from '../../../src/config/indexing/indexing-config';

describe(BlockIndexingHealthCheck.name, () => {
    let target: BlockIndexingHealthCheck;
    let healthState: BlockIndexingHealthState;
    let config: Writable<IndexingConfig>;
    let clock: TestClock;

    beforeEach(() => {
        healthState = new BlockIndexingHealthState();
        config = {} as typeof config;
        clock = new TestClock();
        target = new BlockIndexingHealthCheck(healthState, config, clock);

        config.health = {
            lastSuccessAge: {
                degradedMillis: 100,
                unhealthyMillis: 200,
            },
        };
        healthState.blockBeingProcessed = 'block-processed';
        healthState.lastSuccess = {
            indexedOn: addMillis(clock.nowDate, -50),
            block: 'block-indexed',
        };
    });

    it(`should report ${HealthStatus.Healthy} if everything fine`, () => {
        runTestExpecting(HealthStatus.Healthy, {
            reason: reasons.everythingWorks,
            blockBeingProcessed: healthState.blockBeingProcessed,
            lastSuccess: healthState.lastSuccess,
        });
    });

    it.each([null, undefined])(`should report ${HealthStatus.Unhealthy} if not started b/c processed block is %s`, block => {
        healthState.blockBeingProcessed = block;

        runTestExpecting(HealthStatus.Unhealthy, reasons.notRunning);
    });

    it.each([
        [HealthStatus.Degraded, 150],
        [HealthStatus.Unhealthy, 250],
    ])('should report %s if no success for long time', (expectedStatus, ageMillis) => {
        healthState.lastSuccess!.indexedOn = addMillis(clock.nowDate, -1 * ageMillis);
        healthState.error = 'oups';

        runTestExpecting(expectedStatus, {
            reason: reasons.noSuccessForLongTime,
            error: 'oups',
            blockBeingProcessed: healthState.blockBeingProcessed,
            lastSuccess: healthState.lastSuccess,
            lastSuccessAgeMillis: ageMillis,
            lastSuccessAgeConfig: config.health.lastSuccessAge,
        });
    });

    it(`should report ${HealthStatus.Degraded} if error occurred recently`, () => {
        healthState.error = 'oups';

        runTestExpecting(HealthStatus.Degraded, {
            reason: reasons.errorRecently,
            error: 'oups',
            blockBeingProcessed: healthState.blockBeingProcessed,
            lastSuccess: healthState.lastSuccess,
        });
    });

    function runTestExpecting(expectedStatus: HealthStatus, expectedData: unknown) {
        const health = target.checkHealth();

        expect(health.data).toEqual(expectedData);
        expect(health.status).toBe(expectedStatus);
    }
});
