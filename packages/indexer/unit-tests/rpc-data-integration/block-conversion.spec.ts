import { BigNumber } from 'bignumber.js';

import { shouldConvertRpc } from './rpc-data-test-helper';
import { Block, BalanceUpdateContract, BalanceUpdateOrigin, BalanceUpdateKind, Ballot, OperationKind } from '../../src/public-api';
import { BlockConverter } from '../../src/rpc-data/block/block-converter';

const input = {
    protocol: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
    chain_id: 'NetXdQprcVkpaWU',
    hash: 'BLbkU5hzTbtpcLGg1LJ85vLqRgDcMhFRygr2LoxPHAsujCWbvYn',
    header: {
        level: 2519427,
        proto: 13,
        predecessor: 'BMbfTi2PgjccpnaycnpggbF6HTfQU8mF57UBiVADm6YeeCoyRnh',
        timestamp: '2022-07-09T14:24:44Z',
        validation_pass: 4,
        operations_hash: 'LLoafYzEAZrvnxw3DzWiT4PvK34QJXHB7kfxPpz98X2SVtz84UskA',
        fitness: ['02', '00267183', '', 'ffffffff', '00000000'],
        context: 'CoVw3XEkgM2NdXMjyhE82BDsGsJbRKWtgnykJC7premJfUDdHtvX',
        payload_hash: 'vh2NVZjqHbHkDTQeN7ARAN5t2cHP5KhDafjJUqf9JGESUezmr7MW',
        payload_round: 0,
        proof_of_work_nonce: 'cb9f439e63920700',
        liquidity_baking_toggle_vote: 'pass',
        signature: 'sigpv5MjifCwgMjNa85XHwehErps2gPr8sWruLFR1WdZaLiMz1nNsDGNpLS1CUJSPnkyN7SgfWjZfgj2hduAXCaZwdbzASHb',
    },
    metadata: {
        protocol: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
        next_protocol: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
        test_chain_status: {
            status: 'not_running',
        },
        max_operations_ttl: 120,
        max_operation_data_length: 32768,
        max_block_header_length: 289,
        max_operation_list_length: [
            { max_size: 4194304, max_op: 2048 },
            { max_size: 32768 },
            { max_size: 135168, max_op: 132 },
            { max_size: 524288 },
        ],
        proposer: 'tz1gfArv665EUkSg2ojMBzcbfwuPxAvqPvjo',
        baker: 'tz1gfArv665EUkSg2ojMBzcbfwuPxAvqPvjo',
        level_info: {
            level: 2519427,
            level_position: 2519426,
            cycle: 501,
            cycle_position: 4482,
            expected_commitment: false,
        },
        voting_period_info: {
            voting_period: {
                index: 75,
                kind: 'proposal',
                start_position: 2490368,
            },
            position: 29058,
            remaining: 11901,
        },
        nonce_hash: null,
        consumed_gas: '168484000',
        deactivated: [],
        balance_updates: [
            {
                kind: 'contract',
                contract: 'tz1dnhzazfcdfiiooRmMyMgVLfYgJRoqL19h',
                change: '2414600000',
                origin: 'block',
            },
            {
                kind: 'contract',
                contract: 'tz1bAoZ3PJrtBkYBk9eSywX4VXm7HW9Cfxwr',
                change: '4614600000',
                origin: 'block',
            },
        ],
        liquidity_baking_toggle_ema: 112560427,
        implicit_operations_results: [],
        consumed_milligas: '168484000',
    },
    operations: [
        [
            {
                protocol: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
                chain_id: 'NetXdQprcVkpaWU',
                hash: 'ongmAk4TprnyvypRNxvmEFFLYBZ4tVm8qGZ2SRpRko67iUuSpie',
                branch: 'BMTd3SyNHyE4ikk6dbUegAHAszZNsEABs39Y9zv3TXBTcnddcHd',
                contents: [
                    {
                        kind: 'proposals',
                        source: 'tz1XGftJC11NAanLMkn69FRaEC5rmxcYrJj7',
                        period: 70,
                        proposals: ['PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY'],
                        metadata: {},
                    },
                    {
                        kind: 'ballot',
                        source: 'tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93',
                        period: 42,
                        proposal: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
                        ballot: 'yay',
                        metadata: {},
                    },
                ],
                signature: 'sigQcUwFNeS48UDxytNKJA2hnkSkYaM8Ykc4T9oUKnBwMSUmdRwBCwac43EdVGmpEMHw8tA1KVqDQuidXvFMDTgVkGNYEnAD',
            },
            {
                protocol: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
                chain_id: 'NetXdQprcVkpaWU',
                hash: 'opEKBoDevE3JVSu9Pm2WWkR8dARPopfwhu82gXoUzS5hM43SrEu',
                branch: 'BMTd3SyNHyE4ikk6dbUegAHAszZNsEABs39Y9zv3TXBTcnddcHd',
                contents: [
                    {
                        kind: 'ballot',
                        source: 'tz3bvNMQ95vfAYtG8193ymshqjSvmxiCUuR5',
                        period: 68,
                        proposal: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
                        ballot: 'nay',
                        metadata: {},
                    },
                ],
                signature: 'sigXQNaHfQAZKLByT8Zpti59YezGYBroxSDGinDuaDTdTjD5hnAFEKJFyR7VnY4mGaCKku7tmeCjxGjPN9vmvvQ6tseruCwQ',
            },
        ],
        [
            {
                protocol: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
                chain_id: 'NetXdQprcVkpaWU',
                hash: 'opDB284rNtLxiHjZP3KqcNGRBwqXMwJqJ7yrKmidU3aciUPRdMh',
                branch: 'BMTd3SyNHyE4ikk6dbUegAHAszZNsEABs39Y9zv3TXBTcnddcHd',
                contents: [
                    {
                        kind: 'ballot',
                        source: 'tz1RjoHc98dBoqaH2jawF62XNKh7YsHwbkEv',
                        period: 78,
                        proposal: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
                        ballot: 'pass',
                        metadata: {},
                    },
                ],
                signature: 'sighxKUGDc51KRw4Zy8HTgL82LqMHoqc6EM3EWX22SEmqhLz3dgke8xvNT7KssjPRRni25E4KqCZHK8AbgPV8bvCB93FxQgE',
            },
        ],
    ],
};

const expected: Block = {
    chainId: 'NetXdQprcVkpaWU',
    hash: 'BLbkU5hzTbtpcLGg1LJ85vLqRgDcMhFRygr2LoxPHAsujCWbvYn',
    level: 2519427,
    metadataBalanceUpdates: [
        {
            change: new BigNumber(2414600000),
            contractAddress: 'tz1dnhzazfcdfiiooRmMyMgVLfYgJRoqL19h',
            kind: BalanceUpdateKind.Contract,
            origin: BalanceUpdateOrigin.Block,
            rpcData: input.metadata.balance_updates[0] as any,
            uid: 'BLbkU5hzTbtpcLGg1LJ85vLqRgDcMhFRygr2LoxPHAsujCWbvYn/metadata/balance_updates/0[contract]',
        } as BalanceUpdateContract,
        {
            change: new BigNumber(4614600000),
            contractAddress: 'tz1bAoZ3PJrtBkYBk9eSywX4VXm7HW9Cfxwr',
            kind: BalanceUpdateKind.Contract,
            origin: BalanceUpdateOrigin.Block,
            rpcData: input.metadata.balance_updates[1] as any,
            uid: 'BLbkU5hzTbtpcLGg1LJ85vLqRgDcMhFRygr2LoxPHAsujCWbvYn/metadata/balance_updates/1[contract]',
        } as BalanceUpdateContract,
    ],
    operationGroups: [
        {
            branch: 'BMTd3SyNHyE4ikk6dbUegAHAszZNsEABs39Y9zv3TXBTcnddcHd',
            chainId: 'NetXdQprcVkpaWU',
            hash: 'ongmAk4TprnyvypRNxvmEFFLYBZ4tVm8qGZ2SRpRko67iUuSpie',
            operations: [
                {
                    kind: OperationKind.Proposals,
                    period: 70,
                    proposals: ['PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY'],
                    rpcData: input.operations[0]![0]!.contents[0] as any,
                    sourceAddress: 'tz1XGftJC11NAanLMkn69FRaEC5rmxcYrJj7',
                    uid: 'BLbkU5hzTbtpcLGg1LJ85vLqRgDcMhFRygr2LoxPHAsujCWbvYn/ongmAk4TprnyvypRNxvmEFFLYBZ4tVm8qGZ2SRpRko67iUuSpie/0[proposals]',
                },
                {
                    ballot: Ballot.Yay,
                    kind: OperationKind.Ballot,
                    period: 42,
                    proposal: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
                    rpcData: input.operations[0]![0]!.contents[1] as any,
                    sourceAddress: 'tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93',
                    uid: 'BLbkU5hzTbtpcLGg1LJ85vLqRgDcMhFRygr2LoxPHAsujCWbvYn/ongmAk4TprnyvypRNxvmEFFLYBZ4tVm8qGZ2SRpRko67iUuSpie/1[ballot]',
                },
            ],
            protocol: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
            rpcData: input.operations[0]![0] as any,
            signature: 'sigQcUwFNeS48UDxytNKJA2hnkSkYaM8Ykc4T9oUKnBwMSUmdRwBCwac43EdVGmpEMHw8tA1KVqDQuidXvFMDTgVkGNYEnAD',
            uid: 'BLbkU5hzTbtpcLGg1LJ85vLqRgDcMhFRygr2LoxPHAsujCWbvYn/ongmAk4TprnyvypRNxvmEFFLYBZ4tVm8qGZ2SRpRko67iUuSpie',
        },
        {
            branch: 'BMTd3SyNHyE4ikk6dbUegAHAszZNsEABs39Y9zv3TXBTcnddcHd',
            chainId: 'NetXdQprcVkpaWU',
            hash: 'opEKBoDevE3JVSu9Pm2WWkR8dARPopfwhu82gXoUzS5hM43SrEu',
            operations: [
                {
                    ballot: Ballot.Nay,
                    kind: OperationKind.Ballot,
                    period: 68,
                    proposal: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
                    rpcData: input.operations[0]![1]!.contents[0] as any,
                    sourceAddress: 'tz3bvNMQ95vfAYtG8193ymshqjSvmxiCUuR5',
                    uid: 'BLbkU5hzTbtpcLGg1LJ85vLqRgDcMhFRygr2LoxPHAsujCWbvYn/opEKBoDevE3JVSu9Pm2WWkR8dARPopfwhu82gXoUzS5hM43SrEu/0[ballot]',
                },
            ],
            protocol: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
            rpcData: input.operations[0]![1] as any,
            signature: 'sigXQNaHfQAZKLByT8Zpti59YezGYBroxSDGinDuaDTdTjD5hnAFEKJFyR7VnY4mGaCKku7tmeCjxGjPN9vmvvQ6tseruCwQ',
            uid: 'BLbkU5hzTbtpcLGg1LJ85vLqRgDcMhFRygr2LoxPHAsujCWbvYn/opEKBoDevE3JVSu9Pm2WWkR8dARPopfwhu82gXoUzS5hM43SrEu',
        },
        {
            branch: 'BMTd3SyNHyE4ikk6dbUegAHAszZNsEABs39Y9zv3TXBTcnddcHd',
            chainId: 'NetXdQprcVkpaWU',
            hash: 'opDB284rNtLxiHjZP3KqcNGRBwqXMwJqJ7yrKmidU3aciUPRdMh',
            operations: [
                {
                    ballot: Ballot.Pass,
                    kind: OperationKind.Ballot,
                    period: 78,
                    proposal: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
                    rpcData: input.operations[1]![0]!.contents[0] as any,
                    sourceAddress: 'tz1RjoHc98dBoqaH2jawF62XNKh7YsHwbkEv',
                    uid: 'BLbkU5hzTbtpcLGg1LJ85vLqRgDcMhFRygr2LoxPHAsujCWbvYn/opDB284rNtLxiHjZP3KqcNGRBwqXMwJqJ7yrKmidU3aciUPRdMh/0[ballot]',
                },
            ],
            protocol: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
            rpcData: input.operations[1]![0] as any,
            signature: 'sighxKUGDc51KRw4Zy8HTgL82LqMHoqc6EM3EWX22SEmqhLz3dgke8xvNT7KssjPRRni25E4KqCZHK8AbgPV8bvCB93FxQgE',
            uid: 'BLbkU5hzTbtpcLGg1LJ85vLqRgDcMhFRygr2LoxPHAsujCWbvYn/opDB284rNtLxiHjZP3KqcNGRBwqXMwJqJ7yrKmidU3aciUPRdMh',
        },
    ],
    predecessorBlockHash: 'BMbfTi2PgjccpnaycnpggbF6HTfQU8mF57UBiVADm6YeeCoyRnh',
    proto: 13,
    protocol: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
    rpcData: input as any,
    timestamp: new Date('2022-07-09T14:24:44Z'),
    uid: 'BLbkU5hzTbtpcLGg1LJ85vLqRgDcMhFRygr2LoxPHAsujCWbvYn',
};

shouldConvertRpc('block with operation groups', diContainer => {
    const blockConverter = diContainer.resolve(BlockConverter);

    const block = blockConverter.convert(input as any);

    expect(block).toMatchObject(expected);
});
