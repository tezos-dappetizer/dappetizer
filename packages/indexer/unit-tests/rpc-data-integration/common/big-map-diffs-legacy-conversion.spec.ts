import { StrictOmit } from 'ts-essentials';

import { BigMapDiffAction, LazyBigMapDiff, OperationKind, TransactionOperation } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    kind: 'transaction',
    source: 'tz1UQSa7mYNT8BntyxkES1oBvPJr8vRYhmEZ',
    fee: '1086',
    counter: '980443',
    gas_limit: '7820',
    storage_limit: '350',
    amount: '0',
    destination: 'KT1PHubm9HtyQEJ4BBpMTVomq6mhbfNZ9z5w',
    parameters: {
        entrypoint: 'collect',
        value: { int: '101235' },
    },
    metadata: {
        operation_result: {
            status: 'applied',
            storage: { int: '101365' },
            consumed_gas: '3260',
            consumed_milligas: '3259860',
            storage_size: '13478187',
            big_map_diff: [
                {
                    action: 'update',
                    big_map: '196862',
                    key_hash: 'exprv4nUnh1pHbgmdFVxPv6UKNCFcQCfDTnVUf8NQqJobD25oQx3ps',
                    key: { int: '5322' },
                    value: { bytes: '00008c2f6922f9806cdc7a76edf7daa48c3d704a5dbc' },
                },
                {
                    action: 'copy',
                    source_big_map: '47916',
                    destination_big_map: '5472',
                },
                {
                    action: 'alloc',
                    big_map: '65417',
                    key_type: { prim: 'nat' },
                    value_type: { prim: 'nat' },
                },
                {
                    action: 'remove',
                    big_map: '203375',
                },
            ],
        },
    },
};

const bigMapDiffs: readonly StrictOmit<LazyBigMapDiff, 'getBigMapDiff'>[] = [
    {
        action: BigMapDiffAction.Update,
        rpcData: input.metadata.operation_result.big_map_diff[0] as any,
        uid: 'ops/0[transaction]/metadata/operation_result/big_map_diff/0[update]',
    },
    {
        action: BigMapDiffAction.Copy,
        rpcData: input.metadata.operation_result.big_map_diff[1] as any,
        uid: 'ops/0[transaction]/metadata/operation_result/big_map_diff/1[copy]',
    },
    {
        action: BigMapDiffAction.Alloc,
        rpcData: input.metadata.operation_result.big_map_diff[2] as any,
        uid: 'ops/0[transaction]/metadata/operation_result/big_map_diff/2[alloc]',
    },
    {
        action: BigMapDiffAction.Remove,
        rpcData: input.metadata.operation_result.big_map_diff[3] as any,
        uid: 'ops/0[transaction]/metadata/operation_result/big_map_diff/3[remove]',
    },
];
const expected = {
    kind: OperationKind.Transaction,
    result: { bigMapDiffs },
    uid: 'ops/0[transaction]',
} as TransactionOperation;

shouldConvertRpcOperation('legacy big map diffs', { input, expected });
