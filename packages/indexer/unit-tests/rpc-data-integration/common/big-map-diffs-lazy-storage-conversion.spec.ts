import { StrictOmit } from 'ts-essentials';

import { BigMapDiffAction, LazyBigMapDiff, OperationKind, TransactionOperation } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    kind: 'transaction',
    source: 'tz1UQSa7mYNT8BntyxkES1oBvPJr8vRYhmEZ',
    fee: '1086',
    counter: '980443',
    gas_limit: '7820',
    storage_limit: '350',
    amount: '0',
    destination: 'KT1PHubm9HtyQEJ4BBpMTVomq6mhbfNZ9z5w',
    parameters: {
        entrypoint: 'collect',
        value: { int: '101235' },
    },
    metadata: {
        operation_result: {
            status: 'applied',
            storage: { int: '101365' },
            consumed_gas: '3260',
            consumed_milligas: '3259860',
            storage_size: '13478187',
            big_map_diff: [{ // Should be ignored.
                action: 'remove',
                big_map: '203375',
            }],
            lazy_storage_diff: [
                {
                    kind: 'big_map',
                    id: '287',
                    diff: {
                        action: 'alloc',
                        key_type: { prim: 'nat' },
                        value_type: { prim: 'nat' },
                        updates: [{
                            key_hash: 'expru5X1yxJG6ezR2uHMotwMLNmSzQyh5t1vUnhjx4cS6Pv9qE1Sdo',
                            key: { string: 'dgfh' },
                            value: { bytes: '697066733a2f2f516d595068317466543634704d41427539333' },
                        }],
                    },
                },
                {
                    kind: 'big_map',
                    id: '5471',
                    diff: {
                        action: 'copy',
                        source: '47916',
                        updates: [{
                            key_hash: 'exprugeA6UTjkpt21YGKB67Wha29CJ9f4nhPBeJoX8UrV3PiUH17a8',
                            key: { int: '53' },
                            value: { int: '723140175' },
                        }],
                    },
                },
                {
                    kind: 'big_map',
                    id: '203375',
                    diff: {
                        action: 'remove',
                    },
                },
                {
                    kind: 'big_map',
                    id: '172187',
                    diff: {
                        action: 'update',
                        updates: [
                            {
                                key_hash: 'expruR5Jkrzp2cQFdZdmG8LSCsVGHYTRykcC4iWbAypcwede5MCxKG',
                                key: { bytes: '000062224cef3180a83d6b1d9b54d565c298c60f8307' },
                                value: { int: '147' },
                            },
                            {
                                key_hash: 'expruh3toKwBS2dqsVAUfHoYWwTft1hXP7fJQ5aUtqRnkA1oSr99XU',
                                key: { bytes: '578545dgfgff3180a83d6b1d9b54d565c298c60f8307' },
                                value: { int: '548' },
                            },
                        ],
                    },
                },
            ],
        },
    },
};

const bigMapDiffs: readonly StrictOmit<LazyBigMapDiff, 'getBigMapDiff'>[] = [
    {
        action: BigMapDiffAction.Alloc,
        rpcData: input.metadata.operation_result.lazy_storage_diff[0] as any,
        uid: 'ops/0[transaction]/metadata/operation_result/lazy_storage_diff/0[alloc]',
    },
    {
        action: BigMapDiffAction.Update,
        rpcData: input.metadata.operation_result.lazy_storage_diff[0] as any,
        uid: 'ops/0[transaction]/metadata/operation_result/lazy_storage_diff/0[alloc]/diff/updates/0',
    },
    {
        action: BigMapDiffAction.Copy,
        rpcData: input.metadata.operation_result.lazy_storage_diff[1] as any,
        uid: 'ops/0[transaction]/metadata/operation_result/lazy_storage_diff/1[copy]',
    },
    {
        action: BigMapDiffAction.Update,
        rpcData: input.metadata.operation_result.lazy_storage_diff[1] as any,
        uid: 'ops/0[transaction]/metadata/operation_result/lazy_storage_diff/1[copy]/diff/updates/0',
    },
    {
        action: BigMapDiffAction.Remove,
        rpcData: input.metadata.operation_result.lazy_storage_diff[2] as any,
        uid: 'ops/0[transaction]/metadata/operation_result/lazy_storage_diff/2[remove]',
    },
    {
        action: BigMapDiffAction.Update,
        rpcData: input.metadata.operation_result.lazy_storage_diff[3] as any,
        uid: 'ops/0[transaction]/metadata/operation_result/lazy_storage_diff/3[update]/diff/updates/0',
    },
    {
        action: BigMapDiffAction.Update,
        rpcData: input.metadata.operation_result.lazy_storage_diff[3] as any,
        uid: 'ops/0[transaction]/metadata/operation_result/lazy_storage_diff/3[update]/diff/updates/1',
    },
];
const expected = {
    kind: OperationKind.Transaction,
    result: { bigMapDiffs },
    uid: 'ops/0[transaction]',
} as TransactionOperation;

shouldConvertRpcOperation('legacy big map diffs', { input, expected });
