import { BigNumber } from 'bignumber.js';

import {
    BalanceUpdateContract,
    BalanceUpdateKind,
    BalanceUpdateOrigin,
    BigMapDiffAction,
    InternalOperation,
    LazyBigMapDiff,
    LazyContract,
    LazyStorageChange,
    LazyTransactionParameter,
    MichelsonSchema,
    OperationKind,
    OperationResultStatus,
    TransactionOperation,
} from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    kind: 'transaction',
    source: 'tz1UQSa7mYNT8BntyxkES1oBvPJr8vRYhmEZ',
    fee: '1086',
    counter: '980443',
    gas_limit: '7820',
    storage_limit: '350',
    amount: '0',
    destination: 'tz1RAxW1rBTRumV9QEpFsw51QWPQmAKHGMei',
    metadata: {
        internal_operation_results: [
            {
                kind: 'delegation',
                source: 'KT1X3zxdTzPB9DgVzA3ad6dgZe9JEamoaeRy',
                nonce: 55,
                delegate: 'tz1KfEsrtDaA1sX7vdM4qmEPWuSytuqCDp5j',
                result: {
                    status: 'applied',
                    consumed_gas: '124',
                    consumed_milligas: '124335',
                },
            },
            {
                kind: 'origination',
                source: 'KT1Aq4wWmVanpQhq4TTfjZXB5AjFpx15iQMM',
                nonce: 1,
                balance: '5',
                script: {
                    code: [
                        { prim: 'parameter', args: [{ prim: 'string', annots: ['%name'] }] },
                        { prim: 'code', args: [{ prim: 'DUP' }] },
                        { prim: 'storage', args: [{ prim: 'string', annots: ['%label'] }] },
                    ],
                    storage: { string: 'abc' },
                },
                result: {
                    status: 'applied',
                    balance_updates: [{
                        kind: 'contract',
                        contract: 'tz1dnhzazfcdfiiooRmMyMgVLfYgJRoqL19h',
                        change: '2414600000',
                        origin: 'block',
                    }],
                    originated_contracts: ['KT1CtuukodeT9QLSw56arWc2dqGRjHbC36aJ'],
                    consumed_gas: '1764',
                    consumed_milligas: '1763073',
                    storage_size: '3528',
                    paid_storage_size_diff: '3528',
                    lazy_storage_diff: [{
                        kind: 'big_map',
                        id: '203375',
                        diff: { action: 'remove' },
                    }],
                },
            },
            {
                kind: 'transaction',
                source: 'KT1KMUVh3Mf9P6yn8tkzku197kcYjoZ3qChi',
                nonce: 2,
                amount: '8',
                destination: 'KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9',
                parameters: {
                    entrypoint: 'collect',
                    value: { int: '101235' },
                },
                result: {
                    balance_updates: [{
                        kind: 'contract',
                        contract: 'tz1NDDfcy9LiQrDWmwvUjnGBHvH636Xwz6MD',
                        change: '2414603401',
                        origin: 'block',
                    }],
                    lazy_storage_diff: [{
                        kind: 'big_map',
                        id: '5471',
                        diff: {
                            action: 'copy',
                            source: '47916',
                            updates: [],
                        },
                    }],
                    status: 'applied',
                    storage: { int: '101365' },
                    consumed_gas: '3362',
                    consumed_milligas: '3361880',
                    storage_size: '204224',
                },
            },
            {
                kind: 'event',
                source: 'sr1Ghq66tYK9y3r8CC1Tf8i8m5nxh8nTvZEf',
                nonce: 3,
                type: {
                    prim: 'or',
                    args: [
                        { prim: 'nat', annots: ['%number'] },
                        { prim: 'string', annots: ['%words'] },
                    ],
                },
                tag: 'second',
                payload: {
                    prim: 'Right',
                    args: [{ string: 'lorem ipsum' }],
                },
                result: {
                    status: 'applied',
                    consumed_milligas: '1000000',
                },
            },
        ],
        operation_result: {
            status: 'applied',
            consumed_gas: '3260',
            consumed_milligas: '3259860',
            storage_size: '13478187',
        },
    },
};

const internalOperations: readonly InternalOperation[] = [
    {
        delegateAddress: 'tz1KfEsrtDaA1sX7vdM4qmEPWuSytuqCDp5j',
        isInternal: true,
        kind: OperationKind.Delegation,
        nonce: 55,
        result: {
            consumedGas: new BigNumber(124),
            consumedMilligas: new BigNumber(124335),
            rpcData: input.metadata.internal_operation_results[0]!.result as any,
            status: OperationResultStatus.Applied,
            uid: 'ops/0[transaction]/metadata/internal_operation_results/0[delegation]/result',
        },
        rpcData: input.metadata.internal_operation_results[0] as any,
        source: {
            address: 'KT1X3zxdTzPB9DgVzA3ad6dgZe9JEamoaeRy',
            config: { name: null, toBeIndexed: true },
            getContract: expect.any(Function),
            type: 'Contract',
        },
        uid: 'ops/0[transaction]/metadata/internal_operation_results/0[delegation]',

        // Deprecated:
        sourceAddress: 'KT1X3zxdTzPB9DgVzA3ad6dgZe9JEamoaeRy',
        sourceContract: {
            address: 'KT1X3zxdTzPB9DgVzA3ad6dgZe9JEamoaeRy',
            config: { name: null, toBeIndexed: true },
            getContract: expect.any(Function),
            type: 'Contract',
        },
    },
    {
        balance: new BigNumber(5),
        delegateAddress: null,
        isInternal: true,
        kind: OperationKind.Origination,
        nonce: 1,
        result: {
            balanceUpdates: [{
                change: new BigNumber(2414600000),
                contractAddress: 'tz1dnhzazfcdfiiooRmMyMgVLfYgJRoqL19h',
                kind: BalanceUpdateKind.Contract,
                origin: BalanceUpdateOrigin.Block,
                rpcData: input.metadata.internal_operation_results[1]!.result.balance_updates![0] as any,
                uid: 'ops/0[transaction]/metadata/internal_operation_results/1[origination]/result/balance_updates/0[contract]',
            } as BalanceUpdateContract],
            bigMapDiffs: [{
                action: BigMapDiffAction.Remove,
                rpcData: input.metadata.internal_operation_results[1]!.result.lazy_storage_diff![0] as any,
                uid: 'ops/0[transaction]/metadata/internal_operation_results/1[origination]/result/lazy_storage_diff/0[remove]',
            } as LazyBigMapDiff],
            consumedGas: new BigNumber(1764),
            consumedMilligas: new BigNumber(1763073),
            getInitialStorage: expect.any(Function),
            originatedContract: { address: 'KT1CtuukodeT9QLSw56arWc2dqGRjHbC36aJ' } as LazyContract,
            paidStorageSizeDiff: new BigNumber(3528),
            rpcData: input.metadata.internal_operation_results[1]!.result as any,
            status: OperationResultStatus.Applied,
            storageSize: new BigNumber(3528),
            uid: 'ops/0[transaction]/metadata/internal_operation_results/1[origination]/result',
        },
        rpcData: input.metadata.internal_operation_results[1] as any,
        source: {
            address: 'KT1Aq4wWmVanpQhq4TTfjZXB5AjFpx15iQMM',
            config: { name: null, toBeIndexed: true },
            getContract: expect.any(Function),
            type: 'Contract',
        },
        uid: 'ops/0[transaction]/metadata/internal_operation_results/1[origination]',

        // Deprecated:
        sourceAddress: 'KT1Aq4wWmVanpQhq4TTfjZXB5AjFpx15iQMM',
        sourceContract: {
            address: 'KT1Aq4wWmVanpQhq4TTfjZXB5AjFpx15iQMM',
            config: { name: null, toBeIndexed: true },
            getContract: expect.any(Function),
            type: 'Contract',
        },
    },
    {
        amount: new BigNumber(8),
        destination: {
            address: 'KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9',
            type: 'Contract',
        } as LazyContract,
        isInternal: true,
        kind: OperationKind.Transaction,
        nonce: 2,
        result: {
            allocatedDestinationContract: false,
            balanceUpdates: [{
                change: new BigNumber(2414603401),
                contractAddress: 'tz1NDDfcy9LiQrDWmwvUjnGBHvH636Xwz6MD',
                kind: BalanceUpdateKind.Contract,
                origin: BalanceUpdateOrigin.Block,
                rpcData: input.metadata.internal_operation_results[2]!.result.balance_updates![0] as any,
                uid: 'ops/0[transaction]/metadata/internal_operation_results/2[transaction]/result/balance_updates/0[contract]',
            } as BalanceUpdateContract],
            bigMapDiffs: [{
                action: BigMapDiffAction.Copy,
                rpcData: input.metadata.internal_operation_results[2]!.result.lazy_storage_diff![0] as any,
                uid: 'ops/0[transaction]/metadata/internal_operation_results/2[transaction]/result/lazy_storage_diff/0[copy]',
            } as LazyBigMapDiff],
            consumedGas: new BigNumber(3362),
            consumedMilligas: new BigNumber(3361880),
            paidStorageSizeDiff: null,
            rpcData: input.metadata.internal_operation_results[2]!.result as any,
            storageChange: {
                rpcData: input.metadata.internal_operation_results[2]!.result.storage as any,
                uid: 'ops/0[transaction]/metadata/internal_operation_results/2[transaction]/result/storage',
            } as LazyStorageChange,
            status: OperationResultStatus.Applied,
            storageSize: new BigNumber(204224),
            ticketReceipts: [],
            ticketUpdates: [],
            ticketHash: null,
            uid: 'ops/0[transaction]/metadata/internal_operation_results/2[transaction]/result',
        },
        rpcData: input.metadata.internal_operation_results[2] as any,
        source: {
            address: 'KT1KMUVh3Mf9P6yn8tkzku197kcYjoZ3qChi',
            config: { name: null, toBeIndexed: true },
            getContract: expect.any(Function),
            type: 'Contract',
        },
        transactionParameter: {
            rpcData: input.metadata.internal_operation_results[2]!.parameters as any,
            uid: 'ops/0[transaction]/metadata/internal_operation_results/2[transaction]/parameters',
        } as LazyTransactionParameter,
        uid: 'ops/0[transaction]/metadata/internal_operation_results/2[transaction]',

        // Deprecated:
        destinationAddress: 'KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9',
        destinationContract: { address: 'KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9' } as LazyContract,
        sourceAddress: 'KT1KMUVh3Mf9P6yn8tkzku197kcYjoZ3qChi',
        sourceContract: {
            address: 'KT1KMUVh3Mf9P6yn8tkzku197kcYjoZ3qChi',
            config: { name: null, toBeIndexed: true },
            getContract: expect.any(Function),
            type: 'Contract',
        },
    },
    {
        isInternal: true,
        kind: OperationKind.Event,
        nonce: 3,
        payload: {
            convert: expect.any(Function),
            michelson: {
                prim: 'Right',
                args: [{ string: 'lorem ipsum' }],
            },
            schema: new MichelsonSchema({
                prim: 'or',
                args: [
                    { prim: 'nat', annots: ['%number'] },
                    { prim: 'string', annots: ['%words'] },
                ],
            }),
        },
        result: {
            consumedGas: new BigNumber(1000),
            consumedMilligas: new BigNumber(1000000),
            rpcData: input.metadata.internal_operation_results[3]!.result as any,
            status: OperationResultStatus.Applied,
            uid: 'ops/0[transaction]/metadata/internal_operation_results/3[event]/result',
        },
        rpcData: input.metadata.internal_operation_results[3] as any,
        source: {
            address: 'sr1Ghq66tYK9y3r8CC1Tf8i8m5nxh8nTvZEf',
            config: { name: null, toBeIndexed: true },
            type: 'SmartRollup',
        },
        tag: 'second',
        typeSchema: new MichelsonSchema({
            prim: 'or',
            args: [
                { prim: 'nat', annots: ['%number'] },
                { prim: 'string', annots: ['%words'] },
            ],
        }),
        uid: 'ops/0[transaction]/metadata/internal_operation_results/3[event]',

        // Deprecated:
        sourceAddress: 'sr1Ghq66tYK9y3r8CC1Tf8i8m5nxh8nTvZEf',
        sourceContract: null,
    },
];

const expected = {
    kind: OperationKind.Transaction,
    internalOperations,
    uid: 'ops/0[transaction]',
} as TransactionOperation;

shouldConvertRpcOperation('internal operations', { input, expected });
