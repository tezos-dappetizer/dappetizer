import * as samples from './common-samples';
import { OperationKind, VdfRevelationOperation } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    kind: 'vdf_revelation',
    solution: [
        '02001f71e0dd50d735bb1c2cd54bc6ac78a36d0071372ed8307994ddff6c7c3f6178574549b1',
        '0000f96c4ee73d3e42cea535e480c4af0a067e59beebe69c969e440821217da8e0c421934da4',
    ],
    metadata: { balance_updates: samples.balanceUpdates.input },
};

const expected: VdfRevelationOperation = {
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[vdf_revelation]/metadata'),
    kind: OperationKind.VdfRevelation,
    rpcData: input as any,
    solution: [
        '02001f71e0dd50d735bb1c2cd54bc6ac78a36d0071372ed8307994ddff6c7c3f6178574549b1',
        '0000f96c4ee73d3e42cea535e480c4af0a067e59beebe69c969e440821217da8e0c421934da4',
    ],
    uid: 'ops/0[vdf_revelation]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
