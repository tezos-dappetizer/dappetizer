import { BigNumber } from 'bignumber.js';

import * as samples from './common-samples';
import { OperationKind, OperationResultStatus, SmartRollup, SmartRollupCementOperation } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    counter: '35929',
    fee: '922',
    gas_limit: '6432',
    kind: 'smart_rollup_cement',
    metadata: {
        balance_updates: samples.balanceUpdates.input,
        internal_operation_results: samples.internalOperations.input,
        operation_result: {
            consumed_milligas: '6331052',
            inbox_level: 145631,
            status: 'applied',
            commitment_hash: 'src13u9JTPUimNBi9K1WcCuzDSBq9pBf9xDVFCQMS7W5rMswP3vLuu',
        },
    },
    rollup: 'sr1AE6U3GNzE8iKzj6sKS5wh1U32ogeULCoN',
    source: 'tz1gCe1sFpppbGGVwCt5jLRqDS9FD1W4Qca4',
    storage_limit: '580',
};

const expected: SmartRollupCementOperation = {
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[smart_rollup_cement]/metadata'),
    counter: new BigNumber(35929),
    fee: new BigNumber(922),
    gasLimit: new BigNumber(6432),
    internalOperations: samples.internalOperations.getExpected('ops/0[smart_rollup_cement]/metadata'),
    isInternal: false,
    kind: OperationKind.SmartRollupCement,
    result: {
        commitmentHash: 'src13u9JTPUimNBi9K1WcCuzDSBq9pBf9xDVFCQMS7W5rMswP3vLuu',
        consumedGas: new BigNumber(6332),
        consumedMilligas: new BigNumber(6331052),
        inboxLevel: 145631,
        rpcData: input.metadata.operation_result as any,
        status: OperationResultStatus.Applied,
        uid: 'ops/0[smart_rollup_cement]/metadata/operation_result',
    },
    rollup: { address: 'sr1AE6U3GNzE8iKzj6sKS5wh1U32ogeULCoN' } as SmartRollup,
    rpcData: input as any,
    sourceAddress: 'tz1gCe1sFpppbGGVwCt5jLRqDS9FD1W4Qca4',
    storageLimit: new BigNumber(580),
    uid: 'ops/0[smart_rollup_cement]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
