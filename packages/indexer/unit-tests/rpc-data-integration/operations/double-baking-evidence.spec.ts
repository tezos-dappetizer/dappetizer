import * as samples from './common-samples';
import { DoubleBakingEvidenceOperation, OperationKind } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    kind: 'double_baking_evidence',
    bh1: {
        level: 1943725,
        proto: 11,
        predecessor: 'BKy1bd1C6vwREvyGdLc6togWtdyVNdmVzQbh2prpe4KNUFd99KP',
        timestamp: '2021-12-14T05:32:46Z',
        validation_pass: 4,
        operations_hash: 'LLoZdizKVRszFh2wfYnJg4Y22uFWJGT3VRDLGgXCTEjo8ppPcD6Qm',
        fitness: ['01', '000000000013a8ad'],
        context: 'CoUgYFzazoGgNwKzxULfNDf8iu4q9G2HLb1DRJ8kmXgAELVM88h4',
        priority: 0,
        proof_of_work_nonce: '0e7a0e9a72810100',
        liquidity_baking_escape_vote: false,
        signature: 'sighrtHichtL9F4TrfmMW2WEBu8eEcVqpX6QK2MjsagYYsUWrMaWTbxJ6E9mPGK8d6XXpSLEukELTFnH3ttW8cRgZJtNeFus',
    },
    bh2: {
        level: 1943725,
        proto: 11,
        predecessor: 'BKy1bd1C6vwREvyGdLc6togWtdyVNdmVzQbh2prpe4KNUFd99KP',
        timestamp: '2021-12-14T05:32:46Z',
        validation_pass: 4,
        operations_hash: 'LLoaGfsrhN2rDrMtGYXHdNycwjG8wjm3uC91naXUaUt9MKuMm2VAw',
        fitness: ['01', '000000000013a8ad'],
        context: 'CoVEvdVyEeexqiBJmWZbJ261wrSrTLxzKo1rzXmLLEPutebH6XUp',
        priority: 0,
        proof_of_work_nonce: '0e7a0e9a0cf70100',
        liquidity_baking_escape_vote: false,
        signature: 'sigXLqB72jLoPWSYo2dLBfjujyMejRE6xM2PB7fhFKLFXPNCS7F6zhSWDe47fwteQgk6EPfdx8kt3DEPZe7rP4T6kssbTQfC',
    },
    metadata: { balance_updates: samples.balanceUpdates.input },
};

const expected: DoubleBakingEvidenceOperation = {
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[double_baking_evidence]/metadata'),
    blockHeader1: {
        level: 1943725,
        predecessorBlockHash: 'BKy1bd1C6vwREvyGdLc6togWtdyVNdmVzQbh2prpe4KNUFd99KP',
        proto: 11,
        rpcData: input.bh1,
        timestamp: new Date('2021-12-14T05:32:46Z'),
        uid: 'ops/0[double_baking_evidence]/bh1',
    },
    blockHeader2: {
        level: 1943725,
        predecessorBlockHash: 'BKy1bd1C6vwREvyGdLc6togWtdyVNdmVzQbh2prpe4KNUFd99KP',
        proto: 11,
        rpcData: input.bh2,
        timestamp: new Date('2021-12-14T05:32:46Z'),
        uid: 'ops/0[double_baking_evidence]/bh2',
    },
    kind: OperationKind.DoubleBakingEvidence,
    rpcData: input as any,
    uid: 'ops/0[double_baking_evidence]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
