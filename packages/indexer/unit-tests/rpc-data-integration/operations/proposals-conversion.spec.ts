import { OperationKind, ProposalsOperation } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    kind: 'proposals',
    source: 'tz1XGftJC11NAanLMkn69FRaEC5rmxcYrJj7',
    period: 70,
    proposals: ['PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY'],
    metadata: {},
};

const expected: ProposalsOperation = {
    kind: OperationKind.Proposals,
    period: 70,
    proposals: ['PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY'],
    rpcData: input as any,
    sourceAddress: 'tz1XGftJC11NAanLMkn69FRaEC5rmxcYrJj7',
    uid: 'ops/0[proposals]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
