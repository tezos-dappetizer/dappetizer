import { BigNumber } from 'bignumber.js';

import * as samples from './common-samples';
import {
    MichelsonSchema,
    OperationKind,
    OperationResultStatus,
    SmartRollup,
    SmartRollupOriginateOperation,
} from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    counter: '52381',
    fee: '1217',
    gas_limit: '2849',
    kernel: '23212f7573722f6269',
    kind: 'smart_rollup_originate',
    metadata: {
        balance_updates: samples.balanceUpdates.input,
        internal_operation_results: samples.internalOperations.input,
        operation_result: {
            address: 'sr1NtrMuXaN6RoiJuxTE6uUbBEzfDmoGqccP',
            balance_updates: samples.balanceUpdates2.input,
            consumed_milligas: '2748269',
            genesis_commitment_hash: 'src12huqn6n2noPqfuTuX7zg7GzeN1iCGnykC8bgb4Ypvm4YNWGZ5B',
            size: '6552',
            status: 'applied',
        },
    },
    parameters_ty: { prim: 'bytes' },
    pvm_kind: 'wasm_2_0_0',
    source: 'tz2C2ioEKnDegF1yP362gEQPW57RcWCcqV5c',
    storage_limit: '5661',
};

const expected: SmartRollupOriginateOperation = {
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[smart_rollup_originate]/metadata'),
    counter: new BigNumber(52381),
    fee: new BigNumber(1217),
    gasLimit: new BigNumber(2849),
    internalOperations: samples.internalOperations.getExpected('ops/0[smart_rollup_originate]/metadata'),
    isInternal: false,
    kernel: '23212f7573722f6269',
    kind: OperationKind.SmartRollupOriginate,
    parametersSchema: new MichelsonSchema({ prim: 'bytes' }),
    pvmKind: 'wasm_2_0_0',
    result: {
        balanceUpdates: samples.balanceUpdates2.getExpected('ops/0[smart_rollup_originate]/metadata/operation_result'),
        consumedGas: new BigNumber(2749),
        consumedMilligas: new BigNumber(2748269),
        genesisCommitmentHash: 'src12huqn6n2noPqfuTuX7zg7GzeN1iCGnykC8bgb4Ypvm4YNWGZ5B',
        originatedRollup: { address: 'sr1NtrMuXaN6RoiJuxTE6uUbBEzfDmoGqccP' } as SmartRollup,
        rpcData: input.metadata.operation_result as any,
        size: new BigNumber(6552),
        status: OperationResultStatus.Applied,
        uid: 'ops/0[smart_rollup_originate]/metadata/operation_result',
    },
    rpcData: input as any,
    sourceAddress: 'tz2C2ioEKnDegF1yP362gEQPW57RcWCcqV5c',
    storageLimit: new BigNumber(5661),
    uid: 'ops/0[smart_rollup_originate]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
