import * as samples from './common-samples';
import { ActivateAccountOperation, OperationKind } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    kind: 'activate_account',
    pkh: 'tz1dnhzazfcdfiiooRmMyMgVLfYgJRoqL19h',
    secret: 'deb0272e8a87f50b1324aaa22f64d5546c0eb7b4',
    metadata: { balance_updates: samples.balanceUpdates.input },
};

const expected: ActivateAccountOperation = {
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[activate_account]/metadata'),
    kind: OperationKind.ActivateAccount,
    pkh: 'tz1dnhzazfcdfiiooRmMyMgVLfYgJRoqL19h',
    rpcData: input as any,
    secret: 'deb0272e8a87f50b1324aaa22f64d5546c0eb7b4',
    uid: 'ops/0[activate_account]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
