import { BigNumber } from 'bignumber.js';

import {
    BalanceUpdateAccumulator,
    BalanceUpdateCategory,
    BalanceUpdateContract,
    BalanceUpdateKind,
    BalanceUpdateOrigin,
    DelegationInternalOperation,
    LazyContract,
    OperationKind,
    OperationResultStatus,
} from '../../../src/public-api';

export const balanceUpdates = {
    input: [{
        kind: 'contract',
        contract: 'tz1dnhzazfcdfiiooRmMyMgVLfYgJRoqL19h',
        change: '2414600000',
        origin: 'block',
    }],
    getExpected(uidPrefix: string) {
        return [{
            change: new BigNumber(2414600000),
            contractAddress: 'tz1dnhzazfcdfiiooRmMyMgVLfYgJRoqL19h',
            kind: BalanceUpdateKind.Contract,
            origin: BalanceUpdateOrigin.Block,
            rpcData: this.input[0],
            uid: `${uidPrefix}/balance_updates/0[contract]`,
        } as BalanceUpdateContract];
    },
};

export const balanceUpdates2 = {
    input: [{
        kind: 'accumulator',
        category: 'block fees',
        change: '-17336',
        origin: 'block',
    }],
    getExpected(uidPrefix: string) {
        return [{
            kind: BalanceUpdateKind.Accumulator,
            category: BalanceUpdateCategory.BlockFees,
            change: new BigNumber(-17336),
            origin: BalanceUpdateOrigin.Block,
            rpcData: this.input[0],
            uid: `${uidPrefix}/balance_updates/0[accumulator][block fees]`,
        } as BalanceUpdateAccumulator];
    },
};

export const internalOperations = {
    input: [{
        kind: 'delegation',
        source: 'KT1NXdxJkCiPkhwPvaT9CytFowuUoNcwGM1p',
        nonce: 3,
        delegate: 'tz1RAxW1rBTRumV9QEpFsw51QWPQmAKHGMei',
        result: {
            status: 'applied',
            consumed_gas: '1000',
            consumed_milligas: '1000000',
        },
    }],
    getExpected(uidPrefix: string) {
        return [{
            delegateAddress: 'tz1RAxW1rBTRumV9QEpFsw51QWPQmAKHGMei',
            kind: OperationKind.Delegation,
            isInternal: true,
            nonce: 3,
            result: {
                consumedGas: new BigNumber(1000),
                consumedMilligas: new BigNumber(1000000),
                rpcData: this.input[0]!.result,
                status: OperationResultStatus.Applied,
                uid: `${uidPrefix}/internal_operation_results/0[delegation]/result`,
            },
            rpcData: this.input[0],
            source: {
                address: 'KT1NXdxJkCiPkhwPvaT9CytFowuUoNcwGM1p',
                type: 'Contract',
            } as LazyContract,
            uid: `${uidPrefix}/internal_operation_results/0[delegation]`,

            // Deprecated:
            sourceAddress: 'KT1NXdxJkCiPkhwPvaT9CytFowuUoNcwGM1p',
            sourceContract: { address: 'KT1NXdxJkCiPkhwPvaT9CytFowuUoNcwGM1p' } as LazyContract,
        } as DelegationInternalOperation];
    },
};
