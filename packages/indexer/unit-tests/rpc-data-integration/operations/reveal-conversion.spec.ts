import { BigNumber } from 'bignumber.js';

import * as samples from './common-samples';
import { OperationKind, OperationResultStatus, RevealOperation } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    kind: 'reveal',
    source: 'tz1eepEsRWL4YnNCFqVSKw6KZ8rQ4QUCE9fd',
    fee: '374',
    counter: '68612223',
    gas_limit: '1100',
    storage_limit: '0',
    public_key: 'edpktqy3eA2MNmPfA7eNq9JR6Gm5quPaKPseDoPj7hMWf7q5NnwAha',
    metadata: {
        balance_updates: samples.balanceUpdates.input,
        internal_operation_results: samples.internalOperations.input,
        operation_result: {
            status: 'applied',
            consumed_gas: '1000',
            consumed_milligas: '1000000',
        },
    },
};

const expected: RevealOperation = {
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[reveal]/metadata'),
    counter: new BigNumber(68612223),
    fee: new BigNumber(374),
    gasLimit: new BigNumber(1100),
    internalOperations: samples.internalOperations.getExpected('ops/0[reveal]/metadata'),
    isInternal: false,
    kind: OperationKind.Reveal,
    publicKey: 'edpktqy3eA2MNmPfA7eNq9JR6Gm5quPaKPseDoPj7hMWf7q5NnwAha',
    result: {
        consumedGas: new BigNumber(1000),
        consumedMilligas: new BigNumber(1000000),
        rpcData: input.metadata.operation_result as any,
        status: OperationResultStatus.Applied,
        uid: 'ops/0[reveal]/metadata/operation_result',
    },
    rpcData: input as any,
    sourceAddress: 'tz1eepEsRWL4YnNCFqVSKw6KZ8rQ4QUCE9fd',
    storageLimit: new BigNumber(0),
    uid: 'ops/0[reveal]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
