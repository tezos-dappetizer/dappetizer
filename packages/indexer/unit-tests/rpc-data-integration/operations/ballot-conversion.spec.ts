import { Ballot, BallotOperation, OperationKind } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    kind: 'ballot',
    source: 'tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93',
    period: 73,
    proposal: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
    ballot: 'yay',
    metadata: {},
};

const expected: BallotOperation = {
    ballot: Ballot.Yay,
    kind: OperationKind.Ballot,
    period: 73,
    proposal: 'PtJakart2xVj7pYXJBXrqHgd82rdkLey5ZeeGwDgPp9rhQUbSqY',
    rpcData: input as any,
    sourceAddress: 'tz2FCNBrERXtaTtNX6iimR1UJ5JSDxvdHM93',
    uid: 'ops/0[ballot]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
