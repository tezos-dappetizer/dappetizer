import { BigNumber } from 'bignumber.js';

import * as samples from './common-samples';
import {
    MichelsonSchema,
    OperationKind,
    OperationResultStatus,
    SmartRollup,
    SmartRollupExecuteOutboxMessageOperation,
    TicketToken,
} from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    cemented_commitment: 'src13Csp6pYeTVR8nMgdpUdHqX3NmV2mJ6fxmVbfSeAhKWiYahkPcy',
    counter: '31321',
    fee: '1631',
    gas_limit: '6510',
    kind: 'smart_rollup_execute_outbox_message',
    metadata: {
        balance_updates: samples.balanceUpdates.input,
        internal_operation_results: samples.internalOperations.input,
        operation_result: {
            balance_updates: samples.balanceUpdates2.input,
            consumed_milligas: '2748269',
            paid_storage_size_diff: '637',
            status: 'applied',
            ticket_updates: [
                {
                    ticket_token: {
                        content: { string: 'ttt' },
                        content_type: { prim: 'string' },
                        ticketer: 'KT1NyVZ8QurmHaNYeZXu3acjBwG76GnqtGPT',
                    },
                    updates: [
                        {
                            account: 'KT1Q8D6ixgjvnSx2Q9wJ4Yptx79zbQzpMkms',
                            amount: '-1',
                        },
                    ],
                },
            ],
        },
    },
    output_proof: '030002eabfba0c6a08db68679144041',
    rollup: 'sr1Ta2ZGVaDgJg2DRN6wifTvfHm6pWbwdzXj',
    source: 'tz1hbDyUvU6YRwmFSmSc2b16VT3pab491ujh',
    storage_limit: '25',
};

const expected: SmartRollupExecuteOutboxMessageOperation = {
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[smart_rollup_execute_outbox_message]/metadata'),
    cementedCommitment: 'src13Csp6pYeTVR8nMgdpUdHqX3NmV2mJ6fxmVbfSeAhKWiYahkPcy',
    counter: new BigNumber(31321),
    fee: new BigNumber(1631),
    gasLimit: new BigNumber(6510),
    internalOperations: samples.internalOperations.getExpected('ops/0[smart_rollup_execute_outbox_message]/metadata'),
    isInternal: false,
    kind: OperationKind.SmartRollupExecuteOutboxMessage,
    outputProof: '030002eabfba0c6a08db68679144041',
    result: {
        balanceUpdates: samples.balanceUpdates2.getExpected('ops/0[smart_rollup_execute_outbox_message]/metadata/operation_result'),
        consumedGas: new BigNumber(2749),
        consumedMilligas: new BigNumber(2748269),
        paidStorageSizeDiff: new BigNumber(637),
        rpcData: input.metadata.operation_result as any,
        status: OperationResultStatus.Applied,
        ticketUpdates: [
            {
                accountAddress: 'KT1Q8D6ixgjvnSx2Q9wJ4Yptx79zbQzpMkms',
                amount: new BigNumber(-1),
                rpcData: input.metadata.operation_result.ticket_updates[0]!.updates[0] as any,
                token: {
                    content: {
                        michelson: { string: 'ttt' },
                        schema: new MichelsonSchema({ prim: 'string' }),
                    },
                    ticketerAddress: 'KT1NyVZ8QurmHaNYeZXu3acjBwG76GnqtGPT',
                } as TicketToken,
                uid: 'ops/0[smart_rollup_execute_outbox_message]/metadata/operation_result/ticket_updates/0/updates/0',
            },
        ],
        uid: 'ops/0[smart_rollup_execute_outbox_message]/metadata/operation_result',
    },
    rollup: { address: 'sr1Ta2ZGVaDgJg2DRN6wifTvfHm6pWbwdzXj' } as SmartRollup,
    rpcData: input as any,
    sourceAddress: 'tz1hbDyUvU6YRwmFSmSc2b16VT3pab491ujh',
    storageLimit: new BigNumber(25),
    uid: 'ops/0[smart_rollup_execute_outbox_message]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
