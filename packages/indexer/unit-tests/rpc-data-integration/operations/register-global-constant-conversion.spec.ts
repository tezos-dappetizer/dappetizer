import { BigNumber } from 'bignumber.js';

import * as samples from './common-samples';
import { OperationKind, OperationResultStatus, RegisterGlobalConstantOperation } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    kind: 'register_global_constant',
    source: 'tz1Td5qwQxz5mDZiwk7TsRGhDU2HBvXgULip',
    fee: '6130',
    counter: '36222012',
    gas_limit: '3215',
    storage_limit: '5661',
    value: { string: 'abc' },
    metadata: {
        balance_updates: samples.balanceUpdates.input,
        internal_operation_results: samples.internalOperations.input,
        operation_result: {
            balance_updates: samples.balanceUpdates2.input,
            status: 'applied',
            consumed_gas: '3115',
            storage_size: '5641',
            global_address: 'exprutNrs68aNmrp3DSif5U7Usq2e8f5ZH9xbDekcKdEErYPv11brk',
        },
    },
};

const expected: RegisterGlobalConstantOperation = {
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[register_global_constant]/metadata'),
    counter: new BigNumber(36222012),
    fee: new BigNumber(6130),
    gasLimit: new BigNumber(3215),
    internalOperations: samples.internalOperations.getExpected('ops/0[register_global_constant]/metadata'),
    isInternal: false,
    kind: OperationKind.RegisterGlobalConstant,
    result: {
        balanceUpdates: samples.balanceUpdates2.getExpected('ops/0[register_global_constant]/metadata/operation_result'),
        consumedGas: new BigNumber(3115),
        consumedMilligas: null,
        globalAddress: 'exprutNrs68aNmrp3DSif5U7Usq2e8f5ZH9xbDekcKdEErYPv11brk',
        rpcData: input.metadata.operation_result as any,
        status: OperationResultStatus.Applied,
        storageSize: new BigNumber(5641),
        uid: 'ops/0[register_global_constant]/metadata/operation_result',
    },
    rpcData: input as any,
    sourceAddress: 'tz1Td5qwQxz5mDZiwk7TsRGhDU2HBvXgULip',
    storageLimit: new BigNumber(5661),
    uid: 'ops/0[register_global_constant]',
    value: { string: 'abc' },
};

shouldConvertRpcOperation(expected.kind, { input, expected });
