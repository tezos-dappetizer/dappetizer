import * as samples from './common-samples';
import { OperationKind, SeedNonceRevelationOperation } from '../../../src/public-api';
import { shouldConvertRpcOperation } from '../rpc-data-test-helper';

const input = {
    kind: 'seed_nonce_revelation',
    level: 2498624,
    nonce: '451b7e50061a37780300f6f50e43bff08fbf61f5b99da37aa3b2e807bdbf6d90',
    metadata: { balance_updates: samples.balanceUpdates.input },
};

const expected: SeedNonceRevelationOperation = {
    balanceUpdates: samples.balanceUpdates.getExpected('ops/0[seed_nonce_revelation]/metadata'),
    kind: OperationKind.SeedNonceRevelation,
    level: 2498624,
    nonce: '451b7e50061a37780300f6f50e43bff08fbf61f5b99da37aa3b2e807bdbf6d90',
    rpcData: input as any,
    uid: 'ops/0[seed_nonce_revelation]',
};

shouldConvertRpcOperation(expected.kind, { input, expected });
