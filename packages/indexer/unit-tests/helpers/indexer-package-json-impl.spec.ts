import path from 'path';
import { instance, mock, when } from 'ts-mockito';

import { npmModules, PackageJson, PackageJsonLoader } from '../../../utils/src/public-api';
import { IndexerPackageJson } from '../../src/helpers/indexer-package-json';
import { IndexerPackageJsonImpl } from '../../src/helpers/indexer-package-json-impl';

describe(IndexerPackageJsonImpl.name, () => {
    it('should resolve values correctly', () => {
        const loader = mock<PackageJsonLoader>();
        const packageJson = mock<PackageJson>();
        when(loader.load(path.resolve('package.json'))).thenReturn(instance(packageJson));
        when(packageJson.getVersion()).thenReturn('1.1.1');
        when(packageJson.getDependencyVersion(npmModules.BIGNUMBER)).thenReturn('^2.2.2');
        when(packageJson.getDependencyVersion(npmModules.taquito.TAQUITO)).thenReturn('^3.3.3');
        when(packageJson.getDependencyVersion(npmModules.taquito.MICHELSON_ENCODER)).thenReturn('^4.4.4');

        const target = new IndexerPackageJsonImpl(instance(loader));

        expect(target).toEqual<IndexerPackageJson>({
            version: '1.1.1',
            dependencies: {
                bignumber: '^2.2.2',
                taquito: '^3.3.3',
                taquitoMichelsonEncoder: '^4.4.4',
            },
        });
        expect(target.dependencies).toBeFrozen();
    });
});
