import { instance, mock, verify } from 'ts-mockito';

import { keyof, keysOf } from '../../../utils/src/basics/public-api';
import { IndexerComposer } from '../../src/helpers/indexer-composer';

type TestIndexer1 = {
    indexNums(n1: number, n2: number, n3: number): void;
    indexStrs(s1: string, s2: string): Promise<void>;
};

type TestIndexer2 = {
    indexNums(n1: number, n2: number, n3: number): Promise<void>;
    indexStrs(s1: string, s2: string): void;
};

describe(IndexerComposer.name, () => {
    let target: IndexerComposer;
    let blueprint: Record<keyof TestIndexer1, ''>;
    let wrappedIndexer1: TestIndexer1;
    let wrappedIndexer2: TestIndexer2;

    beforeEach(() => {
        target = new IndexerComposer();
        blueprint = {
            indexNums: '',
            indexStrs: '',
        };
        [wrappedIndexer1, wrappedIndexer2] = [mock(), mock()];
    });

    it('should return null if empty wrapped indexers', () => {
        const indexer = target.createComposite([], blueprint);

        expect(indexer).toBeNull();
    });

    it('should return the indexer if single wrapped indexer', () => {
        const indexer = target.createComposite([instance(wrappedIndexer1)], blueprint);

        expect(indexer).toBe(instance(wrappedIndexer1));
    });

    describe('multiple wrapped indexers', () => {
        let compositeIndexer: TestIndexer1 | TestIndexer2;

        beforeEach(() => {
            compositeIndexer = target.createComposite([instance(wrappedIndexer1), instance(wrappedIndexer2)], blueprint)!;
        });

        it('should expose only specified methods', () => {
            const methodNames = keysOf(compositeIndexer);

            expect(methodNames).toEqual<(keyof typeof compositeIndexer)[]>(['indexNums', 'indexStrs']);
        });

        it(`should delegate ${keyof<typeof compositeIndexer>('indexNums')}() to all wrapped indexers`, async () => {
            await compositeIndexer.indexNums(1, 2, 3);

            verify(wrappedIndexer1.indexNums(1, 2, 3))
                .calledBefore(wrappedIndexer2.indexNums(1, 2, 3));
        });

        it(`should delegate ${keyof<typeof compositeIndexer>('indexStrs')}() to all wrapped indexers`, async () => {
            await compositeIndexer.indexStrs('aa', 'bb');

            verify(wrappedIndexer1.indexStrs('aa', 'bb'))
                .calledBefore(wrappedIndexer2.indexStrs('aa', 'bb'));
        });
    });
});
