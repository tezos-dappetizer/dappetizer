import { Writable } from 'ts-essentials';

import { asReadonly } from '../../../utils/src/public-api';
import { IndexingConfig } from '../../src/config/indexing/indexing-config';
import { ContractConfig } from '../../src/contracts/contract';
import { SelectiveIndexingConfigResolver } from '../../src/helpers/selective-indexing-config-resolver';

describe(SelectiveIndexingConfigResolver.name, () => {
    let target: SelectiveIndexingConfigResolver;
    let config: Writable<IndexingConfig>;

    beforeEach(() => {
        config = {
            contracts: asReadonly([
                {
                    addresses: asReadonly(['KT1-with-name-1', 'KT1-with-name-2']),
                    name: 'FooBar',
                },
                {
                    addresses: asReadonly(['KT1-without-name']),
                    name: null,
                },
            ]),
            smartRollups: asReadonly([
                {
                    addresses: asReadonly(['sr1-test']),
                    name: 'FooRoll',
                },
            ]),
        } as typeof config;
        target = new SelectiveIndexingConfigResolver(config);
    });

    it.each<['contracts' | 'smartRollups', string, boolean, string | null]>([
        ['contracts', 'KT1-with-name-1', true, 'FooBar'],
        ['contracts', 'KT1-with-name-2', true, 'FooBar'],
        ['contracts', 'KT1-without-name', true, null],
        ['contracts', 'KT1-not-indexed', false, null],
        ['smartRollups', 'sr1-test', true, 'FooRoll'],
        ['smartRollups', 'sr1-not-indexer', false, null],
    ])('should resolve correctly if selective indexing of %s', (configProperty, address, expectedToBeIndexed, expectedName) => {
        runTest({
            address,
            configProperty,
            expectedConfig: {
                toBeIndexed: expectedToBeIndexed,
                name: expectedName,
            },
        });
    });

    it('should resolve correctly if no selective indexing', () => {
        config.contracts = null;

        runTest({
            address: 'KT-whatever',
            configProperty: 'contracts',
            expectedConfig: {
                toBeIndexed: true,
                name: null,
            },
        });
    });

    function runTest(testCase: {
        address: string;
        expectedConfig: ContractConfig;
        configProperty: 'contracts' | 'smartRollups';
    }) {
        const contractConfig = target.resolve(testCase.address, testCase.configProperty);

        expect(contractConfig).toEqual<ContractConfig>(testCase.expectedConfig);
        expect(contractConfig).toBeFrozen();
    }
});
