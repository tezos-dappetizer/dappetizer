import * as taquitoRpc from '@taquito/rpc';
import { deepEqual, instance, mock, when } from 'ts-mockito';

import { TestLogger } from '../../../../test-utilities/mocks';
import { BlockDownloader } from '../../src/helpers/block-downloader';

describe(BlockDownloader.name, () => {
    let target: BlockDownloader;
    let rpcClient: taquitoRpc.RpcClient;
    let logger: TestLogger;

    beforeEach(() => {
        rpcClient = mock();
        logger = new TestLogger();
        target = new BlockDownloader(instance(rpcClient), logger);
    });

    it('should download block by provided level', async () => {
        const mockedBlock = { hash: 'haha' } as taquitoRpc.BlockResponse;
        when(rpcClient.getBlock(deepEqual({ block: '123' }))).thenCall(async () => {
            logger.logDebug('TEST');
            return Promise.resolve(mockedBlock);
        });

        const block = await target.download(123);

        expect(block).toBe(mockedBlock);
        logger.loggedSingle()
            .verify('Debug', { performanceSection: logger.category })
            .verifyMessage('TEST');
    });
});
