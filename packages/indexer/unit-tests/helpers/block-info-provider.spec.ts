import { BlockInfoProvider } from '../../src/helpers/block-info-provider';
import { TaquitoRpcBlock } from '../../src/rpc-data/rpc-data-interfaces';

describe(BlockInfoProvider.name, () => {
    const target = new BlockInfoProvider();

    it('should collect block info correctly', () => {
        const block = {
            hash: 'haha',
            header: {
                level: 123,
                predecessor: 'pred',
                timestamp: '2001-02-03',
            },
        } as TaquitoRpcBlock;

        const info = target.getInfo(block);

        expect(info).toEqual({
            hash: 'haha',
            level: 123,
            predecessor: 'pred',
            timestamp: '2001-02-03',
        });
    });
});
