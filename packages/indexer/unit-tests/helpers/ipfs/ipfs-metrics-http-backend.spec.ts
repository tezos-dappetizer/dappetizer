import * as taquitoHttp from '@taquito/http-utils';
import { instance, mock, when } from 'ts-mockito';

import { expectToThrowAsync, TestClock, verifyCalled } from '../../../../../test-utilities/mocks';
import { TaquitoHttpBackend, TaquitoHttpRequest } from '../../../../utils/src/public-api';
import { IpfsMetrics } from '../../../src/helpers/ipfs/ipfs-metrics';
import { IpfsMetricsHttpBackend } from '../../../src/helpers/ipfs/ipfs-metrics-http-backend';

describe(IpfsMetricsHttpBackend.name, () => {
    let target: IpfsMetricsHttpBackend;
    let nextBackend: TaquitoHttpBackend;
    let clock: TestClock;

    let requestCounter: IpfsMetrics['requestCount'];
    let executionDurationHistogram: IpfsMetrics['executionDuration'];
    let responseCounter: IpfsMetrics['responseCount'];

    let request: TaquitoHttpRequest;

    const act = async () => target.execute(request);

    beforeEach(() => {
        [requestCounter, executionDurationHistogram, responseCounter] = [mock(), mock(), mock()];
        const metrics = {
            requestCount: instance(requestCounter),
            executionDuration: instance(executionDurationHistogram),
            responseCount: instance(responseCounter),
        } as IpfsMetrics;

        nextBackend = mock();
        clock = new TestClock();
        target = new IpfsMetricsHttpBackend(instance(nextBackend), metrics, clock);

        request = 'mockedRequest' as any;
    });

    it('should measure successful request', async () => {
        when(nextBackend.execute(request)).thenCall(async () => {
            clock.tick(666);
            return Promise.resolve('resultVal');
        });

        const result = await act();

        expect(result).toBe('resultVal');
        verifyCalled(requestCounter.inc).once();
        verifyCalled<number>(executionDurationHistogram.observe).onceWith(666);
        verifyCalled(responseCounter.inc).onceWith({ status: '200_OK' });
    });

    it.each([
        ['HTTP error', new taquitoHttp.HttpResponseError('oups', taquitoHttp.STATUS_CODE.BAD_REQUEST, 'Foo Bar', '', ''), '400_FooBar'],
        ['generic error', new Error('oups'), 'Error'],
    ])('should measure request failed with %s', async (_desc, reqError, expectedStatus) => {
        when(nextBackend.execute(request)).thenCall(async () => {
            clock.tick(666);
            return Promise.reject(reqError);
        });

        const error = await expectToThrowAsync(act);

        expect(error).toBe(reqError);
        verifyCalled(requestCounter.inc).once();
        verifyCalled<number>(executionDurationHistogram.observe).onceWith(666);
        verifyCalled(responseCounter.inc).onceWith({ status: expectedStatus });
    });
});
