import { IndexerVerifier } from './generic-indexer-verifier';
import { BlockDataIndexer } from '../block-data-indexer';
import { IndexerInfo } from '../wrappers/indexer-info';

export function verifyBlockDataIndexer(indexer: BlockDataIndexer<unknown>, info: IndexerInfo): void {
    const verifier = new IndexerVerifier(indexer, info);

    verifier.verifyOptionalName();
    verifier.verifyAtLeastOneIndexFunctionImplemented({
        indexBalanceUpdate: '',
        indexBlock: '',
        indexMainOperation: '',
        indexOperationGroup: '',
        indexOperationWithResult: '',
    });
}
