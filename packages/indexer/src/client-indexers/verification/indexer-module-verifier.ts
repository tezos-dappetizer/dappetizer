import {
    errorToString,
    injectAllOptional,
    isObject,
    isReadOnlyArray,
    isWhiteSpace,
    keyof,
    withIndex,
} from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { verifyBlockDataIndexer } from './block-data-indexer-verifier';
import { verifyContractIndexer } from './contract-indexer-verifier';
import { verifyIndexingCycleHandler } from './indexing-cycle-handler-verifier';
import { PartialIndexerModuleVerifier } from './partial-indexer-module-verifier';
import { PARTIAL_INDEXER_MODULE_VERIFIERS_DI_TOKEN } from './partial-indexer-module-verifier-di-token';
import { verifySmartRollupIndexer } from './smart-rollup-indexer-verifier';
import { IndexerModule } from '../indexer-module';
import { getIndexerInfo, IndexerInfo } from '../wrappers/indexer-info';

/** We do not trust client indexers -> strict verification. */
@singleton()
export class IndexerModuleVerifier {
    private readonly names = new Set<string>();

    constructor(
        @injectAllOptional(PARTIAL_INDEXER_MODULE_VERIFIERS_DI_TOKEN) private readonly partialVerifiers: readonly PartialIndexerModuleVerifier[],
    ) {}

    verify(module: IndexerModule<unknown>): void {
        if (!isObject(module)) {
            throw new Error(`Some module is not an object but ${typeof module}.`);
        }

        this.verifyName(module.name);
        this.names.add(module.name);

        try {
            const info = getSafeIndexerInfo(module, module.indexingCycleHandler, 'indexingCycleHandler');
            verifyIndexingCycleHandler(module.indexingCycleHandler, info);

            this.verifyIndexerArray(module, module.blockDataIndexers, 'blockDataIndexers', verifyBlockDataIndexer);
            this.verifyIndexerArray(module, module.contractIndexers, 'contractIndexers', verifyContractIndexer);
            this.verifyIndexerArray(module, module.smartRollupIndexers, 'smartRollupIndexers', verifySmartRollupIndexer);

            for (const verifier of this.partialVerifiers) {
                verifier.verify(module);
            }
        } catch (error) {
            throw new Error(`Module with ${keyof<IndexerModule<unknown>>('name')} '${module.name}' is invalid. ${errorToString(error)}`);
        }
    }

    private verifyName(moduleName: string): void {
        if (typeof moduleName !== 'string' || isWhiteSpace(moduleName)) {
            throw new Error(`Some module has invalid property '${keyof<IndexerModule<unknown>>('name')}'`
                + ` of type '${typeof moduleName}' but it must be a string.`);
        }
        if (this.names.has(moduleName)) {
            throw new Error(`Multiple modules have property '${keyof<IndexerModule<unknown>>('name')}' with value '${moduleName}'.`
                + ` It must be unique or have you configured the module twice?`);
        }
    }

    private verifyIndexerArray<TIndexer>(
        module: IndexerModule<unknown>,
        indexers: readonly TIndexer[] | undefined,
        moduleProperty: keyof typeof module,
        verifyIndexer: (indexer: TIndexer, info: IndexerInfo) => void,
    ): void {
        if (indexers === undefined) {
            return;
        }
        if (!isReadOnlyArray(indexers)) {
            throw new Error(`Property '${moduleProperty}' must be either undefined or an array of indexers but it is ${typeof indexers}.`);
        }
        for (const [indexer, index] of withIndex(indexers)) {
            const info = getSafeIndexerInfo(module, indexer, moduleProperty, index);
            verifyIndexer(indexer, info);
        }
    }
}

function getSafeIndexerInfo(
    module: IndexerModule<unknown>,
    indexer: unknown,
    moduleProperty: keyof typeof module,
    indexerIndex?: number,
): IndexerInfo {
    // This is called before we know for sure that indexer is an object.
    const safeIndexer = isObject(indexer) ? indexer : {};
    return getIndexerInfo(module, safeIndexer, moduleProperty, indexerIndex);
}
