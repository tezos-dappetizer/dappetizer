import { dumpToString, isNullish, isObject, isWhiteSpace, keysOf, StringKeyOfType, TypeOfName } from '@tezos-dappetizer/utils';

import { IndexerInfo } from '../wrappers/indexer-info';

export class IndexerVerifier<TIndexer extends object> {
    private readonly indexer: TIndexer & Readonly<Record<string, unknown>>;

    constructor(
        indexer: TIndexer,
        private readonly info: IndexerInfo,
    ) {
        if (!isObject(indexer)) {
            throw new Error(`Property ${info.moduleMemberName} must be an object but it is ${typeof indexer}.`);
        }

        this.indexer = indexer;
    }

    verifyOptionalName(): void {
        const name = this.indexer.name;
        if (name !== undefined && (typeof name !== 'string' || isWhiteSpace(name))) {
            throw new Error(`If property 'name' of indexer '${this.info.indexerName}' is specified, then it must be a non-empty string,`
                + ` but it is ${JSON.stringify(name)}.`);
        }
    }

    verifyAtLeastOneIndexFunctionImplemented(helper: Record<StringKeyOfType<TIndexer, Function | undefined> & `index${string}`, ''>): void {
        this.verifyOptionalFunctionsWithAtLeastOneImplemented(keysOf(helper));
    }

    verifyOptionalFunctionsWithAtLeastOneImplemented(methods: readonly StringKeyOfType<TIndexer, Function | undefined>[]): void {
        this.verifyOptionalFunctions(methods);

        if (methods.every(m => isNullish(this.indexer[m]))) {
            throw new Error(`Indexer '${this.info.indexerName}' has no methods so it is useless.`
                + ` Implement at least one of supported methods: ${dumpToString(methods)}.`);
        }
    }

    verifyRequiredFunctions(methods: readonly StringKeyOfType<TIndexer, Function>[]): void {
        this.#verifyFunctions(methods, ['function']);
    }

    verifyOptionalFunctions(methods: readonly StringKeyOfType<TIndexer, Function | undefined>[]): void {
        this.#verifyFunctions(methods, ['function', 'undefined']);
    }

    #verifyFunctions(methods: readonly StringKeyOfType<TIndexer, unknown>[], expectedTypes: readonly TypeOfName[]): void {
        for (const method of methods) {
            const type = typeof this.indexer[method];

            if (!expectedTypes.includes(type)) {
                throw new Error(`Property '${method}' of '${this.info.indexerName}' must be a ${expectedTypes.join(' or ')} but it is ${type}.`);
            }
        }
    }
}
