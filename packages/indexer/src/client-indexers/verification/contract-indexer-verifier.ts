import { IndexerVerifier } from './generic-indexer-verifier';
import { ContractIndexer } from '../contract-indexer';
import { IndexerInfo } from '../wrappers/indexer-info';

export function verifyContractIndexer(indexer: ContractIndexer<unknown>, info: IndexerInfo): void {
    const verifier = new IndexerVerifier(indexer, info);

    verifier.verifyOptionalName();
    verifier.verifyOptionalFunctions([
        'shouldIndex',
    ]);
    verifier.verifyAtLeastOneIndexFunctionImplemented({
        indexBigMapDiff: '',
        indexBigMapUpdate: '',
        indexEvent: '',
        indexOrigination: '',
        indexStorageChange: '',
        indexTransaction: '',
    });
}
