/**
 * Brief info about already indexed block.
 * @category Client Indexer API
 */
export interface IndexedBlock {
    readonly level: number;
    readonly hash: string;
}
