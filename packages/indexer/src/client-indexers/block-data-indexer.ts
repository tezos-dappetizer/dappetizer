import {
    BalanceUpdate,
    Block,
    InternalOperationWithBalanceUpdatesInResult,
    MainOperation,
    MainOperationWithBalanceUpdatesInMetadata,
    MainOperationWithBalanceUpdatesInResult,
    MainOperationWithResult,
    OperationGroup,
    OperationWithResult,
} from '../rpc-data/rpc-data-interfaces';

/**
 * Provides extension points for indexing block data parts.
 * At least one of methods must be implemented.
 * @typeParam TDbContext The type of database context. Usually {@link "@tezos-dappetizer/database".DbContext},
 *      if used with standard {@link @tezos-dappetizer/database}.
 * @typeParam TContextData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @category Client Indexer API
 */
export interface BlockDataIndexer<TDbContext, TContextData = unknown> {
    /** The explicit name used e.g. to easily identify the indexer in errors. */
    readonly name?: string;

    /** Indexes a block. */
    indexBlock?(
        block: Block,
        dbContext: TDbContext,
        indexingContext: BlockIndexingContext<TContextData>,
    ): void | PromiseLike<void>;

    /** Indexes an operation group. */
    indexOperationGroup?(
        operationGroup: OperationGroup,
        dbContext: TDbContext,
        indexingContext: OperationGroupIndexingContext<TContextData>,
    ): void | PromiseLike<void>;

    /** Indexes a main (top-level, directly in operation group) operation. */
    indexMainOperation?(
        mainOperation: MainOperation,
        dbContext: TDbContext,
        indexingContext: MainOperationIndexingContext<TContextData>,
    ): void | PromiseLike<void>;

    /** Indexes an operation with a result. See what is included in {@link OperationWithResult}. */
    indexOperationWithResult?(
        operationWithResult: OperationWithResult,
        dbContext: TDbContext,
        indexingContext: OperationWithResultIndexingContext<TContextData>,
    ): void | PromiseLike<void>;

    /** Indexes a balance update. */
    indexBalanceUpdate?(
        balanceUpdate: BalanceUpdate,
        dbContext: TDbContext,
        indexingContext: BalanceUpdateIndexingContext<TContextData>,
    ): void | PromiseLike<void>;
}

/**
 * Provides all additional data according the path of the indexed entity within the block.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @category Client Indexer API
 */
export interface BlockIndexingContext<TData = unknown> {
    readonly data: TData;
}

/**
 * Provides all additional data according the path of the indexed entity within the block.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @category Client Indexer API
 */
export interface OperationGroupIndexingContext<TData = unknown> extends BlockIndexingContext<TData> {
    readonly block: Block;
}

/**
 * Provides all additional data according the path of the indexed entity within the block.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @category Client Indexer API
 */
export interface MainOperationIndexingContext<TData = unknown> extends OperationGroupIndexingContext<TData> {
    readonly operationGroup: OperationGroup;
}

/**
 * Provides all additional data according the path of the indexed entity within the block.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @category Client Indexer API
 */
export interface OperationWithResultIndexingContext<TData = unknown> extends MainOperationIndexingContext<TData> {
    readonly mainOperation: MainOperationWithResult;
}

/**
 * Provides all additional data according the path of the indexed entity within the block.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @category Client Indexer API
 */
export type BalanceUpdateIndexingContext<TData = unknown> =
    | BlockMetadataBalanceUpdateIndexingContext<TData>
    | MainOperationMetadataBalanceUpdateIndexingContext<TData>
    | MainOperationResultBalanceUpdateIndexingContext<TData>
    | InternalOperationResultBalanceUpdateIndexingContext<TData>;

/** @category Client Indexer API */
export interface BlockMetadataBalanceUpdateIndexingContext<TData = unknown> extends OperationGroupIndexingContext<TData> {
    // Narrowing properties:

    /** Usable for narrowing the type of this object from {@link BalanceUpdateIndexingContext}. */
    readonly balanceUpdateSource: 'BlockMetadata';

    // Null properties:

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdateIndexingContext} union. */
    readonly operationGroup: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdateIndexingContext} union. */
    readonly mainOperation: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdateIndexingContext} union. */
    readonly internalOperation: null;
}

/** @category Client Indexer API */
export interface MainOperationMetadataBalanceUpdateIndexingContext<TData = unknown> extends OperationGroupIndexingContext<TData> {
    // Narrowing properties:

    /** Usable for narrowing the type of this object from {@link BalanceUpdateIndexingContext}. */
    readonly balanceUpdateSource: 'MainOperationMetadata';

    // Specific properties:

    readonly operationGroup: OperationGroup;

    /** The main operation with `balanceUpdates` containing the indexed {@link BalanceUpdate}. */
    readonly mainOperation: MainOperationWithBalanceUpdatesInMetadata;

    // Null properties:

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdateIndexingContext} union. */
    readonly internalOperation: null;
}

/** @category Client Indexer API */
export interface MainOperationResultBalanceUpdateIndexingContext<TData = unknown> extends OperationGroupIndexingContext<TData> {
    // Narrowing properties:

    /** Usable for narrowing the type of this object from {@link BalanceUpdateIndexingContext}. */
    readonly balanceUpdateSource: 'MainOperationAppliedResult';

    // Specific properties:

    readonly operationGroup: OperationGroup;

    /** The main operation with `result.balanceUpdates` containing the indexed {@link BalanceUpdate}. */
    readonly mainOperation: MainOperationWithBalanceUpdatesInResult;

    // Null properties:

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdateIndexingContext} union. */
    readonly internalOperation: null;
}

/** @category Client Indexer API */
export interface InternalOperationResultBalanceUpdateIndexingContext<TData = unknown> extends OperationGroupIndexingContext<TData> {
    // Narrowing properties:

    /** Usable for narrowing the type of this object from {@link BalanceUpdateIndexingContext}. */
    readonly balanceUpdateSource: 'InternalOperationAppliedResult';

    // Specific properties:

    readonly operationGroup: OperationGroup;

    /** The main operation which contains the {@link internalOperation} */
    readonly mainOperation: MainOperationWithResult;

    /** The internal operation with `result.balanceUpdates` containing the indexed {@link BalanceUpdate}. */
    readonly internalOperation: InternalOperationWithBalanceUpdatesInResult;
}
