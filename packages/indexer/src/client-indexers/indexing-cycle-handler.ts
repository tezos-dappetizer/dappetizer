import { IndexedBlock } from './indexed-block';
import { Block } from '../rpc-data/rpc-data-interfaces';

/**
 * Central indexing component that handles special/overall cases - see its methods.
 * @typeParam TDbContext The type of database context. Usually {@link "@tezos-dappetizer/database".DbContext},
 *      if used with standard {@link @tezos-dappetizer/database}.
 * @typeParam TContextData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData}.
 * @category Client Indexer API
 */
export interface IndexingCycleHandler<TDbContext, TContextData = unknown> {
    /**
     * Creates your custom data that flows through all indexers when indexing a block.
     * It flows in `indexingContext.data` or as a dedicated parameter.
     * If not implemented, then `undefined` is used.
     */
    createContextData?(
        block: Block,
        dbContext: TDbContext,
    ): TContextData | PromiseLike<TContextData>;

    /** Executed before all other indexers execute. */
    beforeIndexersExecute?(
        block: Block,
        dbContext: TDbContext,
        contextData: TContextData,
    ): void | PromiseLike<void>;

    /**
     * Executed after all other indexers are executed.
     * This is an ideal place to refine your indexed data or write them to database in optimal way.
     */
    afterIndexersExecuted?(
        block: Block,
        dbContext: TDbContext,
        contextData: TContextData,
    ): void | PromiseLike<void>;

    /** Executed after block is already indexed, after the related database transaction is committed. */
    afterBlockIndexed?(
        block: Block,
        contextData: TContextData,
    ): void | PromiseLike<void>;

    /** Should roll back indexed changed on blockchain reorganization. */
    rollBackOnReorganization?(
        blockToRollback: IndexedBlock,
        dbContext: TDbContext,
    ): void | PromiseLike<void>;
}
