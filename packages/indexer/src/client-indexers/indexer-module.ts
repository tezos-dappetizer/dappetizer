import { InjectionToken } from 'tsyringe';

import { BlockDataIndexer } from './block-data-indexer';
import { ContractIndexer } from './contract-indexer';
import { IndexingCycleHandler } from './indexing-cycle-handler';
import { SmartRollupIndexer } from './smart-rollup-indexer';

/**
 * Groups related indexing logic together to be executed by Dappetizer.
 * @typeParam TDbContext The type of database context. Usually {@link "@tezos-dappetizer/database".DbContext},
 * if used with standard {@link @tezos-dappetizer/database}.
 * @typeParam TContextData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @category Client Indexer API
 */
export interface IndexerModule<TDbContext, TContextData = unknown> {
    readonly name: string;
    readonly indexingCycleHandler?: IndexingCycleHandler<TDbContext, TContextData>;
    readonly blockDataIndexers?: readonly BlockDataIndexer<TDbContext, TContextData>[];
    readonly contractIndexers?: readonly ContractIndexer<TDbContext, TContextData>[];
    readonly smartRollupIndexers?: readonly SmartRollupIndexer<TDbContext, TContextData>[];
}

/**
 * Tsyringe token if you are using dependency injection manually.
 * @category Client Indexer API
 */
export const INDEXER_MODULES_DI_TOKEN: InjectionToken<IndexerModule<unknown>> = 'Dappetizer:Indexer:IndexerModule';

/**
 * The property name in `module.exports` with an object implementing {@link IndexerModule} or related factory.
 * @category Client Indexer API
 */
export const INDEXER_MODULE_EXPORT_NAME = 'indexerModule';
