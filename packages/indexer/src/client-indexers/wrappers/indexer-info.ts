import { getConstructorName } from '@tezos-dappetizer/utils';

import { IndexerModule } from '../indexer-module';

export interface IndexerInfo {
    readonly indexerName: string;
    readonly moduleName: string;
    readonly moduleMemberName: string;
}

export type WithInfo<T> = T & { readonly info: IndexerInfo };

export function getIndexerInfo(
    module: IndexerModule<unknown>,
    indexer: object,
    moduleProperty: keyof typeof module,
    indexerIndex?: number,
    explicitIndexerName?: string,
): IndexerInfo {
    const moduleMemberName = indexerIndex !== undefined
        ? `${moduleProperty}[${indexerIndex}]`
        : moduleProperty;

    const indexerName = explicitIndexerName
        ?? (getConstructorName(indexer) !== Object.name ? getConstructorName(indexer) : null)
        ?? moduleMemberName;

    return {
        indexerName,
        moduleName: module.name,
        moduleMemberName,
    };
}
