import { dumpToString, errorToString, getConstructorName, Nullish, RecordToDump } from '@tezos-dappetizer/utils';
import { AsyncOrSync } from 'ts-essentials';
import { inject, singleton } from 'tsyringe';

import { Block } from '../../rpc-data/rpc-data-interfaces';
import { IndexedBlock } from '../indexed-block';
import { IndexerDatabaseHandler } from '../indexer-database-handler';
import { INDEXER_DATABASE_HANDLER_DI_TOKEN } from '../indexer-database-handler-di-token';

@singleton()
export class IndexerDatabaseHandlerWrapper<TDbContext = unknown> implements IndexerDatabaseHandler<TDbContext> {
    readonly name: string;

    constructor(@inject(INDEXER_DATABASE_HANDLER_DI_TOKEN) private readonly nextHandler: IndexerDatabaseHandler<TDbContext>) {
        const name = getConstructorName(this.nextHandler);
        this.name = name !== Object.name ? name : 'IndexerDatabaseHandler';
    }

    async startTransaction(): Promise<TDbContext> {
        return this.wrap(async () => this.nextHandler.startTransaction(), 'startTransaction');
    }

    async commitTransaction(dbContext: TDbContext): Promise<void> {
        await this.wrap(() => this.nextHandler.commitTransaction(dbContext), 'commitTransaction');
    }

    async rollBackTransaction(dbContext: TDbContext): Promise<void> {
        await this.wrap(async () => this.nextHandler.rollBackTransaction(dbContext), 'rollBackTransaction');
    }

    async insertBlock(block: Block, dbContext: TDbContext): Promise<void> {
        await this.wrap(async () => this.nextHandler.insertBlock(block, dbContext), 'insertBlock', { block: getBlock(block) });
    }

    async deleteBlock(block: IndexedBlock, dbContext: TDbContext): Promise<void> {
        await this.wrap(async () => this.nextHandler.deleteBlock(block, dbContext), 'deleteBlock', { block: getBlock(block) });
    }

    async getIndexedChainId(): Promise<string | null> {
        return this.wrap(async () => await this.nextHandler.getIndexedChainId() ?? null, 'getIndexedChainId');
    }

    async getLastIndexedBlock(): Promise<IndexedBlock | null> {
        return this.wrap(async () => getBlock(await this.nextHandler.getLastIndexedBlock()), 'getLastIndexedBlock');
    }

    async getIndexedBlock(level: number): Promise<IndexedBlock | null> {
        return this.wrap(async () => getBlock(await this.nextHandler.getIndexedBlock(level)), 'getIndexedBlock', { level });
    }

    private async wrap<T>(
        method: () => AsyncOrSync<T>,
        methodName: keyof this & string,
        argsInfo?: RecordToDump,
    ): Promise<T> {
        try {
            return await method();
        } catch (error) {
            throw new Error(`'${this.name}' failed ${methodName}(${argsInfo ? dumpToString(argsInfo) : ''}). ${errorToString(error)}`);
        }
    }
}

function getBlock(block: Nullish<IndexedBlock>): { hash: string; level: number } | null {
    return block ? Object.freeze({ hash: block.hash, level: block.level }) : null;
}
