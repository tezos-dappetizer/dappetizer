import { TrustedBlockDataIndexer } from './trusted-indexer-interfaces';
import { BoundContractIndexerResolver } from '../../contracts/bound-indexer/bound-contract-indexer-resolver';
import { BoundSmartRollupIndexerResolver } from '../../smart-rollups/bound-indexer/bound-smart-rollup-indexer-resolver';
import { IndexingCycleHandler } from '../indexing-cycle-handler';

export interface IndexerModuleWrapper<TDbContext, TContextData> {
    readonly name: string;
    readonly indexingCycleHandler: Required<IndexingCycleHandler<TDbContext, TContextData>>;
    readonly blockDataIndexer: TrustedBlockDataIndexer<TDbContext, TContextData>;
    readonly contractIndexers: BoundContractIndexerResolver<TDbContext, TContextData> | null;
    readonly smartRollupIndexers: BoundSmartRollupIndexerResolver<TDbContext, TContextData> | null;
}
