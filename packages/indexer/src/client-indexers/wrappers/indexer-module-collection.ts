import { injectAll, singleton } from 'tsyringe';

import { getIndexerInfo } from './indexer-info';
import { IndexerModuleWrapper } from './indexer-module-wrapper';
import { TrustedBlockDataIndexer } from './trusted-indexer-interfaces';
import { BoundContractIndexerBinder } from '../../contracts/bound-indexer/bound-contract-indexer-binder';
import { BoundContractIndexerResolver } from '../../contracts/bound-indexer/bound-contract-indexer-resolver';
import { IndexerComposer } from '../../helpers/indexer-composer';
import { BoundSmartRollupIndexerBinder } from '../../smart-rollups/bound-indexer/bound-smart-rollup-indexer-binder';
import { BoundSmartRollupIndexerResolver } from '../../smart-rollups/bound-indexer/bound-smart-rollup-indexer-resolver';
import { EmptyBlockDataIndexer } from '../empty/empty-block-data-indexer';
import { EmptyIndexingCycleHandler } from '../empty/empty-indexing-cycle-handler';
import { ClientIndexerErrorHandler } from '../error-handling/client-indexer-error-handler';
import { createErrorHandlingBlockDataIndexer } from '../error-handling/error-handling-block-data-indexer';
import { createErrorHandlingContractIndexer } from '../error-handling/error-handling-contract-indexer';
import { createErrorHandlingIndexingCycleHandler } from '../error-handling/error-handling-indexing-cycle-handler';
import { createErrorHandlingSmartRollupIndexer } from '../error-handling/error-handling-smart-rollup-indexer';
import { INDEXER_MODULES_DI_TOKEN, IndexerModule } from '../indexer-module';
import { IndexerModuleVerifier } from '../verification/indexer-module-verifier';

@singleton()
export class IndexerModuleCollection<TDbContext> {
    readonly modules: readonly IndexerModuleWrapper<TDbContext, unknown>[];

    constructor(
        boundContractIndexerFactory: BoundContractIndexerBinder<TDbContext>,
        boundSmartRolluContractIndexerFactory: BoundSmartRollupIndexerBinder<TDbContext>,
        @injectAll(INDEXER_MODULES_DI_TOKEN) modules: readonly IndexerModule<TDbContext>[],
        moduleVerifier: IndexerModuleVerifier,
        indexerComposer: IndexerComposer,
        errorHandler: ClientIndexerErrorHandler,
    ) {
        this.modules = modules.map(wrapModule);

        // Keep dedicated method to have strongly-typed TContextData.
        function wrapModule<TContextData>(module: IndexerModule<TDbContext, TContextData>): IndexerModuleWrapper<TDbContext, TContextData> {
            moduleVerifier.verify(module);

            const contractIndexers = (module.contractIndexers ?? []).map(
                (indexer, index) => {
                    const info = getIndexerInfo(module, indexer, 'contractIndexers', index, indexer.name);
                    return createErrorHandlingContractIndexer(indexer, info, errorHandler);
                },
            );
            const smartRollupIndexers = (module.smartRollupIndexers ?? []).map(
                (indexer, index) => {
                    const info = getIndexerInfo(module, indexer, 'smartRollupIndexers', index, indexer.name);
                    return createErrorHandlingSmartRollupIndexer(indexer, info, errorHandler);
                },
            );
            return {
                name: module.name,
                indexingCycleHandler: module.indexingCycleHandler
                    ? createErrorHandlingIndexingCycleHandler(
                        module.indexingCycleHandler,
                        getIndexerInfo(module, module.indexingCycleHandler, 'indexingCycleHandler'),
                        errorHandler,
                    )
                    : new EmptyIndexingCycleHandler(),
                blockDataIndexer: createCompositeBlockDataIndexer(indexerComposer, (module.blockDataIndexers ?? []).map(
                    (indexer, index) => {
                        const info = getIndexerInfo(module, indexer, 'blockDataIndexers', index, indexer.name);
                        return createErrorHandlingBlockDataIndexer(indexer, info, errorHandler);
                    },
                )) ?? new EmptyBlockDataIndexer(),
                contractIndexers: contractIndexers.length > 0
                    ? new BoundContractIndexerResolver(boundContractIndexerFactory, contractIndexers)
                    : null,
                smartRollupIndexers: smartRollupIndexers.length > 0
                    ? new BoundSmartRollupIndexerResolver(boundSmartRolluContractIndexerFactory, smartRollupIndexers)
                    : null,
            };
        }
    }
}

function createCompositeBlockDataIndexer<TDbContext, TContextData>(
    indexerComposer: IndexerComposer,
    indexers: readonly TrustedBlockDataIndexer<TDbContext, TContextData>[],
): TrustedBlockDataIndexer<TDbContext, TContextData> | null {
    return indexerComposer.createComposite(indexers, {
        indexBalanceUpdate: '',
        indexBlock: '',
        indexMainOperation: '',
        indexOperationGroup: '',
        indexOperationWithResult: '',
    });
}
