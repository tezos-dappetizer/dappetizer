import { OperationWithResultIndexingContext } from './block-data-indexer';
import {
    Applied,
    SmartRollupAddMessagesOperation,
    SmartRollupCementOperation,
    SmartRollupExecuteOutboxMessageOperation,
    SmartRollupOriginateOperation,
    SmartRollupPublishOperation,
    SmartRollupRecoverBondOperation,
    SmartRollupRefuteOperation,
    SmartRollupTimeoutOperation,
} from '../rpc-data/rpc-data-interfaces';
import { SmartRollup } from '../smart-rollups/smart-rollup';

/**
 * Provides extension points for block parts related to a smart rollup.
 * At least one of methods must be implemented.
 * @typeParam TDbContext The type of database context. Usually {@link "@tezos-dappetizer/database".DbContext},
 * if used with standard {@link @tezos-dappetizer/database}.
 * @typeParam TContextData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @typeParam TRollupData The type of your custom data associated with the smart rollup that flows in `indexingContext` through this indexer.
 *      See {@link SmartRollupIndexer.shouldIndex} for more details.
 * @category Client Indexer API
 * @experimental
 */
export interface SmartRollupIndexer<TDbContext, TContextData = unknown, TRollupData = unknown> {
    /** The explicit name used to easily identify the indexer, for example in error messages. */
    readonly name?: string;

    /**
     * Dynamically selects a smart rollup for indexing (remaining `index*()` methods) by this indexer.
     * Firstly, the selective indexing is evaluated based on Dappetizer config file.
     * If this method is not implemented, then all smart rollups are selected.
     * Also, it associates returned value as your custom data with the smart rollup.
     * Then the data flows in `indexingContext` through this indexer for all blocks being indexed.
     * @param rollup The smart rollup to be evaluated whether it should be indexed.
     * @param dbContext The database context but it must execute only readonly queries so that it does not interfere with block indexing.
     * @returns If `false`, then the smart rollup will not be indexed.
     *      Otherwise, your custom data to be associated with the smart rollup if it should be indexed.
     *      If you do not care about the data, then just return `true`.
     *      The method can be async, so the returned value can be a `Promise`.
     */
    shouldIndex?(
        rollup: SmartRollup,
        dbContext: TDbContext,
    ): TRollupData | false | PromiseLike<TRollupData | false>;

    /**
     * Indexes a {@link SmartRollupAddMessagesOperation} related to ANY smart rollup as far as the operation does not contain the rollup address.
     * You need to filter out messages unrelated to the selected (see {@link shouldIndex}) smart rollup yourself
     * based on decoded {@link SmartRollupAddMessagesOperation.message}.
     * Also, this can be called before {@link shouldIndex} method.
     */
    indexAddMessages?(
        operation: Applied<SmartRollupAddMessagesOperation>,
        dbContext: TDbContext,
        indexingContext: SmartRollupAddMessagesIndexingContext<TContextData>,
    ): void | PromiseLike<void>;

    /** Indexes a {@link SmartRollupCementOperation} related to the selected (see {@link shouldIndex}) smart rollup. */
    indexCement?(
        operation: Applied<SmartRollupCementOperation>,
        dbContext: TDbContext,
        indexingContext: SmartRollupCementIndexingContext<TContextData, TRollupData>,
    ): void | PromiseLike<void>;

    /** Indexes a {@link SmartRollupExecuteOutboxMessageOperation} related to the selected (see {@link shouldIndex}) smart rollup. */
    indexExecuteOutboxMessage?(
        operation: Applied<SmartRollupExecuteOutboxMessageOperation>,
        dbContext: TDbContext,
        indexingContext: SmartRollupExecuteOutboxMessageIndexingContext<TContextData, TRollupData>,
    ): void | PromiseLike<void>;

    /** Indexes a {@link SmartRollupOriginateOperation} related to the selected (see {@link shouldIndex}) smart rollup. */
    indexOriginate?(
        operation: Applied<SmartRollupOriginateOperation>,
        dbContext: TDbContext,
        indexingContext: SmartRollupOriginateIndexingContext<TContextData, TRollupData>,
    ): void | PromiseLike<void>;

    /** Indexes a {@link SmartRollupPublishOperation} related to the selected (see {@link shouldIndex}) smart rollup. */
    indexPublish?(
        operation: Applied<SmartRollupPublishOperation>,
        dbContext: TDbContext,
        indexingContext: SmartRollupPublishIndexingContext<TContextData, TRollupData>,
    ): void | PromiseLike<void>;

    /** Indexes a {@link SmartRollupRecoverBondOperation} related to the selected (see {@link shouldIndex}) smart rollup. */
    indexRecoverBond?(
        operation: Applied<SmartRollupRecoverBondOperation>,
        dbContext: TDbContext,
        indexingContext: SmartRollupRecoverBondIndexingContext<TContextData, TRollupData>,
    ): void | PromiseLike<void>;

    /** Indexes a {@link SmartRollupRefuteOperation} related to the selected (see {@link shouldIndex}) smart rollup. */
    indexRefute?(
        operation: Applied<SmartRollupRefuteOperation>,
        dbContext: TDbContext,
        indexingContext: SmartRollupRefuteIndexingContext<TContextData, TRollupData>,
    ): void | PromiseLike<void>;

    /** Indexes a {@link SmartRollupTimeoutOperation} related to the selected (see {@link shouldIndex}) smart rollup. */
    indexTimeout?(
        operation: Applied<SmartRollupTimeoutOperation>,
        dbContext: TDbContext,
        indexingContext: SmartRollupTimeoutIndexingContext<TContextData, TRollupData>,
    ): void | PromiseLike<void>;
}

/**
 * Provides all additional data related to the selected smart rollup and the indexed entity.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @typeParam TRollupData The type of your custom data associated with the smart rollup that flows in `indexingContext` through this indexer.
 *      See {@link SmartRollupIndexer.shouldIndex} for more details.
 * @category Client Indexer API
 * @experimental
 */
export interface SmartRollupIndexingContext<TData = unknown, TRollupData = unknown> extends OperationWithResultIndexingContext<TData> {
    /** Gets data associated with a rollup - returned by shouldIndex(). It is carried over all the time when indexing blocks. */
    readonly rollupData: TRollupData;

    readonly rollup: SmartRollup;
}

/**
 * Provides all additional data related to the selected smart rollup and the indexed entity.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @typeParam TRollupData The type of your custom data associated with the smart rollup that flows in `indexingContext` through this indexer.
 *      See {@link SmartRollupIndexer.shouldIndex} for more details.
 * @category Client Indexer API
 * @experimental
 */
export interface SmartRollupOriginateIndexingContext<TData = unknown, TRollupData = unknown> extends SmartRollupIndexingContext<TData, TRollupData> {
    readonly mainOperation: Applied<SmartRollupOriginateOperation>;
}

/**
 * Provides all additional data related to the indexed entity.
 * Unfortunately, there is no way how to pair it with a rollup and rollup data.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @category Client Indexer API
 * @experimental
 */
export interface SmartRollupAddMessagesIndexingContext<TData = unknown> extends OperationWithResultIndexingContext<TData> {
    readonly mainOperation: Applied<SmartRollupAddMessagesOperation>;
}

/**
 * Provides all additional data related to the selected smart rollup and the indexed entity.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @typeParam TRollupData The type of your custom data associated with the smart rollup that flows in `indexingContext` through this indexer.
 *      See {@link SmartRollupIndexer.shouldIndex} for more details.
 * @category Client Indexer API
 * @experimental
 */
export interface SmartRollupCementIndexingContext<TData = unknown, TRollupData = unknown> extends SmartRollupIndexingContext<TData, TRollupData> {
    readonly mainOperation: Applied<SmartRollupCementOperation>;
}

/**
 * Provides all additional data related to the selected smart rollup and the indexed entity.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @typeParam TRollupData The type of your custom data associated with the smart rollup that flows in `indexingContext` through this indexer.
 *      See {@link SmartRollupIndexer.shouldIndex} for more details.
 * @category Client Indexer API
 * @experimental
 */
export interface SmartRollupExecuteOutboxMessageIndexingContext<TData = unknown, TRollupData = unknown>
    extends SmartRollupIndexingContext<TData, TRollupData> {
    readonly mainOperation: Applied<SmartRollupExecuteOutboxMessageOperation>;
}

/**
 * Provides all additional data related to the selected smart rollup and the indexed entity.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @typeParam TRollupData The type of your custom data associated with the smart rollup that flows in `indexingContext` through this indexer.
 *      See {@link SmartRollupIndexer.shouldIndex} for more details.
 * @category Client Indexer API
 * @experimental
 */
export interface SmartRollupPublishIndexingContext<TData = unknown, TRollupData = unknown> extends SmartRollupIndexingContext<TData, TRollupData> {
    readonly mainOperation: Applied<SmartRollupPublishOperation>;
}

/**
 * Provides all additional data related to the selected smart rollup and the indexed entity.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @typeParam TRollupData The type of your custom data associated with the smart rollup that flows in `indexingContext` through this indexer.
 *      See {@link SmartRollupIndexer.shouldIndex} for more details.
 * @category Client Indexer API
 * @experimental
 */
export interface SmartRollupRecoverBondIndexingContext<TData = unknown, TRollupData = unknown>
    extends SmartRollupIndexingContext<TData, TRollupData> {
    readonly mainOperation: Applied<SmartRollupRecoverBondOperation>;
}

/**
 * Provides all additional data related to the selected smart rollup and the indexed entity.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @typeParam TRollupData The type of your custom data associated with the smart rollup that flows in `indexingContext` through this indexer.
 *      See {@link SmartRollupIndexer.shouldIndex} for more details.
 * @category Client Indexer API
 * @experimental
 */
export interface SmartRollupRefuteIndexingContext<TData = unknown, TRollupData = unknown> extends SmartRollupIndexingContext<TData, TRollupData> {
    readonly mainOperation: Applied<SmartRollupRefuteOperation>;
}

/**
 * Provides all additional data related to the selected smart rollup and the indexed entity.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @typeParam TRollupData The type of your custom data associated with the smart rollup that flows in `indexingContext` through this indexer.
 *      See {@link SmartRollupIndexer.shouldIndex} for more details.
 * @category Client Indexer API
 * @experimental
 */
export interface SmartRollupTimeoutIndexingContext<TData = unknown, TRollupData = unknown> extends SmartRollupIndexingContext<TData, TRollupData> {
    readonly mainOperation: Applied<SmartRollupTimeoutOperation>;
}
