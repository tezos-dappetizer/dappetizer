import { argGuard, registerSingleton } from '@tezos-dappetizer/utils';
import { DependencyContainer, Provider } from 'tsyringe';

import { IndexedBlock } from './indexed-block';
import { INDEXER_DATABASE_HANDLER_DI_TOKEN } from './indexer-database-handler-di-token';
import { Block } from '../rpc-data/rpc-data-interfaces';

/** @category Client Indexer API */
export interface IndexerDatabaseHandler<TDbContext> {
    startTransaction(): TDbContext | PromiseLike<TDbContext>;
    commitTransaction(dbContext: TDbContext): void | PromiseLike<void>;
    rollBackTransaction(dbContext: TDbContext): void | PromiseLike<void>;

    insertBlock(block: Block, dbContext: TDbContext): void | PromiseLike<void>;
    deleteBlock(block: IndexedBlock, dbContext: TDbContext): void | PromiseLike<void>;

    getIndexedChainId(): string | null | undefined | PromiseLike<string | null | undefined>;
    getLastIndexedBlock(): IndexedBlock | null | undefined | PromiseLike<IndexedBlock | null | undefined>;
    getIndexedBlock(level: number): IndexedBlock | null | undefined | PromiseLike<IndexedBlock | null | undefined>;
}

/**
 * Registers a database handler for Dappetizer indexer app.
 * It is registered as **singleton** and can be registered only **once** to make it clear which one is used.
 * @category Client Indexer API
 */
export function registerIndexerDatabaseHandler(diContainer: DependencyContainer, handlerProvider: Provider<IndexerDatabaseHandler<unknown>>): void {
    argGuard.object(diContainer, 'diContainer');

    if (diContainer.isRegistered(INDEXER_DATABASE_HANDLER_DI_TOKEN)) {
        throw new Error('IndexerDatabaseHandler is already registered. There can be only one to make it clear which one is used.');
    }
    registerSingleton(diContainer, INDEXER_DATABASE_HANDLER_DI_TOKEN, handlerProvider);
}
