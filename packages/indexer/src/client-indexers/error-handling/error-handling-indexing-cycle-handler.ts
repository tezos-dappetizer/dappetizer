import { RecordToDump } from '@tezos-dappetizer/utils';

import { ClientIndexerErrorHandler, ErrorHandlingOptions } from './client-indexer-error-handler';
import { Block } from '../../rpc-data/rpc-data-interfaces';
import { EmptyIndexingCycleHandler } from '../empty/empty-indexing-cycle-handler';
import { IndexedBlock } from '../indexed-block';
import { IndexingCycleHandler } from '../indexing-cycle-handler';
import { IndexerInfo } from '../wrappers/indexer-info';

export function createErrorHandlingIndexingCycleHandler<TDbContext, TContextData>(
    handler: Readonly<IndexingCycleHandler<TDbContext, TContextData>>,
    info: IndexerInfo,
    errorHandler: ClientIndexerErrorHandler,
    emptyHandler = new EmptyIndexingCycleHandler<TDbContext, TContextData>(),
): Required<IndexingCycleHandler<TDbContext, TContextData>> {
    return {
        createContextData: errorHandler.wrap(handler, 'createContextData', info, new CreateContextOptions(emptyHandler)),
        beforeIndexersExecute: errorHandler.wrap(handler, 'beforeIndexersExecute', info, new IndexersExecutionOptions()),
        afterIndexersExecuted: errorHandler.wrap(handler, 'afterIndexersExecuted', info, new IndexersExecutionOptions()),
        afterBlockIndexed: errorHandler.wrap(handler, 'afterBlockIndexed', info, new AfterBlockIndexedOptions()),
        rollBackOnReorganization: errorHandler.wrap(handler, 'rollBackOnReorganization', info, new RollBackOptions()),
    };
}

export class CreateContextOptions<TDbContext, TContextData> implements ErrorHandlingOptions<[Block, TDbContext], TContextData> {
    readonly ifMethodNotDefined = {
        returnValue: this.emptyHandler.createContextData(),
        logMessage: 'Therefore the data in the indexing context will be undefined.',
    } as const;

    readonly getArgsToReport: (block: Block, _dbContext: TDbContext) => RecordToDump = getBlockInfo;

    constructor(private readonly emptyHandler: EmptyIndexingCycleHandler<TDbContext, TContextData>) {}

    getArgsToTrace(block: Block, _dbContext: TDbContext): object {
        return { block };
    }
}

export class IndexersExecutionOptions<TDbContext, TContextData> implements ErrorHandlingOptions<[Block, TDbContext, TContextData]> {
    readonly getArgsToReport: (block: Block, _dbContext: TDbContext, _contextData: TContextData) => RecordToDump = getBlockInfo;

    getArgsToTrace(block: Block, _dbContext: TDbContext, contextData: TContextData): object {
        return { block, contextData };
    }
}

export class AfterBlockIndexedOptions<TContextData> implements ErrorHandlingOptions<[Block, TContextData]> {
    readonly getArgsToReport: (block: Block, _contextData: TContextData) => RecordToDump = getBlockInfo;

    getArgsToTrace(block: Block, contextData: TContextData): object {
        return { block, contextData };
    }
}

export class RollBackOptions<TDbContext> implements ErrorHandlingOptions<[IndexedBlock, TDbContext]> {
    readonly getArgsToReport: (blockToRollback: IndexedBlock, _dbContext: TDbContext) => RecordToDump = getBlockInfo;

    getArgsToTrace(block: IndexedBlock, _dbContext: TDbContext): object {
        return { block };
    }
}

function getBlockInfo(block: IndexedBlock): RecordToDump {
    return {
        block: {
            level: block.level,
            hash: block.hash,
        },
    };
}
