import { KeyOfType, RecordToDump } from '@tezos-dappetizer/utils';
import { lowerFirst } from 'lodash';
import { AsyncOrSync } from 'ts-essentials';

import { ClientIndexerErrorHandler, ErrorHandlingOptions } from './client-indexer-error-handler';
import { Contract } from '../../contracts/contract';
import { shouldIndexBasedOnData } from '../../helpers/should-index-evaluator';
import { EntityToIndex, LazyContract } from '../../rpc-data/rpc-data-interfaces';
import { ContractIndexer } from '../contract-indexer';
import { IndexerInfo } from '../wrappers/indexer-info';
import { TrustedContractIndexer } from '../wrappers/trusted-indexer-interfaces';

export function createErrorHandlingContractIndexer<TDbContext, TContextData, TContractData>(
    indexer: ContractIndexer<TDbContext, TContextData, TContractData>,
    info: IndexerInfo,
    errorHandler: ClientIndexerErrorHandler,
): TrustedContractIndexer<TDbContext, TContextData, TContractData> {
    return {
        info,
        shouldIndex: errorHandler.wrap(indexer, 'shouldIndex', info, new ShouldIndexOptions()),
        indexBigMapDiff: wrapIndexMethod('indexBigMapDiff'),
        indexBigMapUpdate: wrapIndexMethod('indexBigMapUpdate'),
        indexEvent: wrapIndexMethod('indexEvent'),
        indexOrigination: wrapIndexMethod('indexOrigination'),
        indexStorageChange: wrapIndexMethod('indexStorageChange'),
        indexTransaction: wrapIndexMethod('indexTransaction'),
    };

    function wrapIndexMethod<TEntity extends EntityToIndex, TIndexingContext extends { readonly contract: Contract }>(
        methodName: KeyOfType<typeof indexer, undefined | ((e: TEntity, d: TDbContext, c: TIndexingContext) => AsyncOrSync<void>)> & `index${string}`,
    ): (e: TEntity, d: TDbContext, c: TIndexingContext) => Promise<void> {
        return errorHandler.wrap(indexer, methodName, info, new IndexMethodOptions(methodName));
    }
}

export class IndexMethodOptions<TEntity extends EntityToIndex, TDbContext, TIndexingContext extends { readonly contract: Contract }>
implements ErrorHandlingOptions<[TEntity, TDbContext, TIndexingContext]> {
    constructor(readonly methodName: `index${string}`) {}

    getArgsToReport(entity: TEntity, _dbContext: TDbContext, indexingContext: TIndexingContext): RecordToDump {
        return {
            contract: indexingContext.contract.address,
            uid: entity.uid,
        };
    }

    getArgsToTrace(entity: TEntity, _dbContext: TDbContext, indexingContext: TIndexingContext): object {
        const entityName = lowerFirst(this.methodName.substring('index'.length));
        return { [entityName]: entity, indexingContext };
    }
}

export class ShouldIndexOptions<TDbContext, TContractData> implements ErrorHandlingOptions<[LazyContract, TDbContext], TContractData | false> {
    readonly ifMethodNotDefined = {
        returnValue: undefined as TContractData,
        logMessage: 'Therefore the contract will be indexed and its associated data will be undefined for the indexer.',
    } as const;

    readonly afterExecuted = {
        logMessage: 'As a result, the indexer {willIndex} the contract.',
        logMessageArgs: (contractData: TContractData | false) => ({ willIndex: shouldIndexBasedOnData(contractData) }),
    } as const;

    getArgsToReport(contract: LazyContract, _dbContext: TDbContext): RecordToDump {
        return { contract: contract.address };
    }

    getArgsToTrace(contract: LazyContract, _dbContext: TDbContext): object {
        return { contract };
    }
}
