import { InjectionToken } from 'tsyringe';

import { IndexerDatabaseHandler } from './indexer-database-handler';

export const INDEXER_DATABASE_HANDLER_DI_TOKEN: InjectionToken<IndexerDatabaseHandler<unknown>> = 'Dappetizer:Indexer:IndexerDatabaseHandler';
