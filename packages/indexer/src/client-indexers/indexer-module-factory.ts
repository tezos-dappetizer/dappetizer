import { ConfigElement, Logger, LoggerCategory } from '@tezos-dappetizer/utils';
import { DependencyContainer } from 'tsyringe';

import { IndexerModule } from './indexer-module';

/** @category Client Indexer API */
export interface ModuleCreationOptions {
    /** Raw module specific configuration. */
    readonly config: unknown;

    /** Module specific configuration wrapped to helper for easier deserialization. */
    readonly configElement: ConfigElement;

    /** TSyringe dependency injection container. */
    readonly diContainer: DependencyContainer;

    getLogger(category: LoggerCategory): Logger;
}

/**
 * Factory for creating {@link IndexerModule} based on {@link ModuleCreationOptions}.
 * @typeParam TDbContext See the same parameter on {@link IndexerModule}.
 * @typeParam TContextData See the same parameter on {@link IndexerModule}.
 * @category Client Indexer API
 */
export type IndexerModuleFactory<TDbContext, TContextData = unknown> = (options: ModuleCreationOptions) => IndexerModule<TDbContext, TContextData>;
