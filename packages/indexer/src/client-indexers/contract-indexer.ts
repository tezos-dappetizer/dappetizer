import { OperationWithResultIndexingContext } from './block-data-indexer';
import { Contract } from '../contracts/contract';
import {
    Applied,
    BigMapDiff,
    BigMapUpdate,
    EventInternalOperation,
    LazyContract,
    OriginationInternalOperation,
    OriginationOperation,
    StorageChange,
    TransactionInternalOperation,
    TransactionOperation,
    TransactionParameter,
} from '../rpc-data/rpc-data-interfaces';

/**
 * Provides extension points for indexing block parts related to a contract.
 * At least one of methods must be implemented.
 * @typeParam TDbContext The type of database context. Usually {@link "@tezos-dappetizer/database".DbContext},
 * if used with standard {@link @tezos-dappetizer/database}.
 * @typeParam TContextData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @typeParam TContractData The type of your custom data associated with the contract that flows in `indexingContext` through this indexer.
 *      See {@link ContractIndexer.shouldIndex} method for more details.
 * @category Client Indexer API
 */
export interface ContractIndexer<TDbContext, TContextData = unknown, TContractData = unknown> {
    /** The explicit name used to easily identify the indexer, for example in error messages. */
    readonly name?: string;

    /**
     * Dynamically selects a contract for indexing (remaining `index*()` methods) by this indexer.
     * Firstly, the selective indexing is evaluated based on Dappetizer config file.
     * If this method is not implemented, then all contracts are selected.
     * Also, it associates returned value as your custom data with the contract.
     * Then the data flows in `indexingContext` through this indexer for all blocks being indexed.
     * @param lazyContract The contract to be evaluated whether it should be indexed. Its details are resolved lazily as you need them.
     * @param dbContext The database context but it must execute only readonly queries so that it does not interfere with block indexing.
     * @returns If `false`, then the contract will not be indexed.
     *      Otherwise, your custom data to be associated with the contract if it should be indexed.
     *      If you do not care about the data, then just return `true`.
     *      The method can be async, so the returned value can be a `Promise`.
     */
    shouldIndex?(
        lazyContract: LazyContract,
        dbContext: TDbContext,
    ): TContractData | false | PromiseLike<TContractData | false>;

    /** Indexes an origination of the selected (see {@link shouldIndex}) contract. */
    indexOrigination?(
        origination: ContractOrigination,
        dbContext: TDbContext,
        indexingContext: OriginationIndexingContext<TContextData, TContractData>,
    ): void | PromiseLike<void>;

    /** Indexes a transaction which goes to the selected (see {@link shouldIndex}) contract. */
    indexTransaction?(
        transactionParameter: TransactionParameter,
        dbContext: TDbContext,
        indexingContext: TransactionIndexingContext<TContextData, TContractData>,
    ): void | PromiseLike<void>;

    /** Indexes a storage change of the selected (see {@link shouldIndex}) contract. */
    indexStorageChange?(
        storageChange: StorageChange,
        dbContext: TDbContext,
        indexingContext: StorageChangeIndexingContext<TContextData, TContractData>,
    ): void | PromiseLike<void>;

    /** Indexes a difference in a big map of the selected (see {@link shouldIndex}) contract. */
    indexBigMapDiff?(
        bigMapDiff: BigMapDiff,
        dbContext: TDbContext,
        indexingContext: BigMapDiffIndexingContext<TContextData, TContractData>,
    ): void | PromiseLike<void>;

    /** Indexes an update of a big map of the selected (see {@link shouldIndex}) contract. */
    indexBigMapUpdate?(
        bigMapUpdate: BigMapUpdate,
        dbContext: TDbContext,
        indexingContext: BigMapUpdateIndexingContext<TContextData, TContractData>,
    ): void | PromiseLike<void>;

    /** Indexes an event emitted by the selected (see {@link shouldIndex}) contract. */
    indexEvent?(
        event: ContractEvent,
        dbContext: TDbContext,
        indexingContext: EventIndexingContext<TContextData, TContractData>
    ): void | PromiseLike<void>;
}

/**
 * An alias of applied origination main or internal operation used for contract indexing.
 * @category Client Indexer API
 */
export type ContractOrigination = Applied<OriginationOperation | OriginationInternalOperation>;

/**
 * An alias of applied event operation used in contract indexing.
 * @category Client Indexer API
 */
export type ContractEvent = Applied<EventInternalOperation>;

/**
 * Provides all additional data related to the selected contract and the indexed entity.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @typeParam TContractData The type of your custom data associated with the contract that flows in `indexingContext` through this indexer.
 *      See {@link ContractIndexer.shouldIndex} for more details.
 * @category Client Indexer API
 */
export interface ContractIndexingContext<TData = unknown, TContractData = unknown> extends OperationWithResultIndexingContext<TData> {
    readonly bigMapDiffs: readonly BigMapDiff[];
    readonly contract: Contract;

    /** Gets data associated with a contract - returned by shouldIndex(). It is carried over all the time when indexing blocks. */
    readonly contractData: TContractData;

    readonly events: readonly ContractEvent[];
    readonly operationWithResult: Applied<OriginationOperation | OriginationInternalOperation | TransactionOperation | TransactionInternalOperation>;
    readonly storageChange: StorageChange | null;
    readonly transactionParameter: TransactionParameter | null;
}

/**
 * Provides all additional data related to the selected contract and the indexed entity.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @typeParam TContractData The type of your custom data associated with the contract that flows in `indexingContext` through this indexer.
 *      See {@link ContractIndexer.shouldIndex} for more details.
 * @category Client Indexer API
 */
export interface OriginationIndexingContext<TData = unknown, TContractData = unknown> extends ContractIndexingContext<TData, TContractData> {
    readonly operationWithResult: Applied<OriginationOperation | OriginationInternalOperation>;
}

/**
 * Provides all additional data related to the selected contract and the indexed entity.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @typeParam TContractData The type of your custom data associated with the contract that flows in `indexingContext` through this indexer.
 *      See {@link ContractIndexer.shouldIndex} for more details.
 * @category Client Indexer API
 */
export interface TransactionIndexingContext<TData = unknown, TContractData = unknown> extends ContractIndexingContext<TData, TContractData> {
    readonly transactionParameter: TransactionParameter;
    readonly operationWithResult: Applied<TransactionOperation | TransactionInternalOperation>;
}

/**
 * Provides all additional data related to the selected contract and the indexed entity.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @typeParam TContractData The type of your custom data associated with the contract that flows in `indexingContext` through this indexer.
 *      See {@link ContractIndexer.shouldIndex} for more details.
 * @category Client Indexer API
 */
export interface StorageChangeIndexingContext<TData = unknown, TContractData = unknown> extends ContractIndexingContext<TData, TContractData> {
    readonly storageChange: StorageChange;
}

/**
 * Provides all additional data related to the selected contract and the indexed entity.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @typeParam TContractData The type of your custom data associated with the contract that flows in `indexingContext` through this indexer.
 *      See {@link ContractIndexer.shouldIndex} for more details.
 * @category Client Indexer API
 */
export interface BigMapDiffIndexingContext<TData = unknown, TContractData = unknown> extends ContractIndexingContext<TData, TContractData> {
    readonly bigMapDiff: BigMapDiff;
}

/**
 * Provides all additional data related to the selected contract and the indexed entity.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @typeParam TContractData The type of your custom data associated with the contract that flows in `indexingContext` through this indexer.
 *      See {@link ContractIndexer.shouldIndex} for more details.
 * @category Client Indexer API
 */
export interface BigMapUpdateIndexingContext<TData = unknown, TContractData = unknown> extends BigMapDiffIndexingContext<TData, TContractData> {
    readonly bigMapDiff: BigMapUpdate;
}

/**
 * Provides all additional data related to the selected contract and the indexed entity.
 * @typeParam TData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link IndexingCycleHandler.createContextData} for more details.
 * @typeParam TContractData The type of your custom data associated with the contract that flows in `indexingContext` through this indexer.
 *      See {@link ContractIndexer.shouldIndex} for more details.
 * @category Client Indexer API
 */
export interface EventIndexingContext<TData = unknown, TContractData = unknown> extends ContractIndexingContext<TData, TContractData> {
    readonly event: ContractEvent;
}
