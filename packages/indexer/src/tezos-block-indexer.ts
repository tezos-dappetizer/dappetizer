import { injectLogger, Logger } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { AsyncProcessor } from './block-processing/async-processor';
import { BlockTraverser } from './block-traversers/block-traverser';
import { IndexingCycleHandler } from './client-indexers/indexing-cycle-handler';
import { IndexerDatabaseHandlerWrapper } from './client-indexers/wrappers/indexer-database-handler-wrapper';
import { IndexerModuleCollection } from './client-indexers/wrappers/indexer-module-collection';
import { IndexerModuleWrapper } from './client-indexers/wrappers/indexer-module-wrapper';
import { tryRollBackTransaction } from './helpers/database-handler-helpers';
import { scopedLogKeys } from './helpers/scoped-log-keys';
import { Block } from './rpc-data/block/block';

@singleton()
export class TezosBlockIndexer<TDbContext = unknown> implements AsyncProcessor<Block> {
    constructor(
        private readonly blockTraverser: BlockTraverser<TDbContext>,
        private readonly databaseHandler: IndexerDatabaseHandlerWrapper<TDbContext>,
        private readonly indexerModules: IndexerModuleCollection<TDbContext>,
        @injectLogger(TezosBlockIndexer) private readonly logger: Logger,
    ) {}

    async process(block: Block): Promise<void> {
        this.logger.logInformation(`Indexing {${scopedLogKeys.INDEXED_BLOCK}}.`);
        const dbContext = await this.databaseHandler.startTransaction();

        try {
            const contextData: Record<string, unknown> = {};
            await this.databaseHandler.insertBlock(block, dbContext);

            for (const module of this.indexerModules.modules) {
                contextData[module.name] = await this.executeIndexing(module, block, dbContext);
            }

            await this.databaseHandler.commitTransaction(dbContext);
            for (const module of this.indexerModules.modules) {
                await this.executeAfterIndexed(module, block, contextData[module.name]);
            }
            this.logger.logInformation(`Successfully indexed {${scopedLogKeys.INDEXED_BLOCK}}.`);
        } catch (error) {
            await tryRollBackTransaction(this.databaseHandler, this.logger, dbContext);
            throw error;
        }
    }

    private async executeIndexing<TContextData>(
        module: IndexerModuleWrapper<TDbContext, TContextData>,
        block: Block,
        dbContext: TDbContext,
    ): Promise<TContextData> {
        return this.runWithModuleLogged(module, async () => {
            this.logger.logDebug(`Indexing by {${scopedLogKeys.INDEXER_MODULE}}.`);
            const contextData = await module.indexingCycleHandler.createContextData(block, dbContext) as TContextData;

            await this.blockTraverser.traverse(module, dbContext, block, Object.freeze({ data: contextData }));

            await module.indexingCycleHandler.afterIndexersExecuted(block, dbContext, contextData);
            this.logger.logDebug(`Successfully indexed by {${scopedLogKeys.INDEXER_MODULE}}.`);
            return contextData;
        });
    }

    private async executeAfterIndexed<TContextData>(
        module: IndexerModuleWrapper<TDbContext, TContextData>,
        block: Block,
        contextData: TContextData,
    ): Promise<void> {
        await this.runWithModuleLogged(module, async () => {
            const methodName: keyof IndexingCycleHandler<unknown> = 'afterBlockIndexed';
            this.logger.logDebug(`Executing ${methodName}() of {${scopedLogKeys.INDEXER_MODULE}}.`);

            await module.indexingCycleHandler.afterBlockIndexed(block, contextData);
            this.logger.logDebug(`Executed ${methodName}() of {${scopedLogKeys.INDEXER_MODULE}}.`);
        });
    }

    private async runWithModuleLogged<TResult>(
        module: IndexerModuleWrapper<TDbContext, unknown>,
        func: () => Promise<TResult>,
    ): Promise<TResult> {
        const scopedLogData = { [scopedLogKeys.INDEXER_MODULE]: module.name };
        return this.logger.runWithData(scopedLogData, func);
    }
}
