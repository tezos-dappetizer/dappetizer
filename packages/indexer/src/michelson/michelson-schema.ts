import * as taquitoMichelson from '@taquito/michelson-encoder';
import * as taquitoRpc from '@taquito/rpc';
import { deepFreeze, errorToString } from '@tezos-dappetizer/utils';
import { DeepReadonly } from 'ts-essentials';

import { flatRightCombPairs } from './michelson-utils';
import { Michelson } from '../rpc-data/rpc-data-interfaces';

export class MichelsonSchema {
    readonly michelson: Michelson;
    readonly generated: DeepReadonly<taquitoMichelson.TokenSchema>;
    readonly #schema: taquitoMichelson.Schema;

    constructor(michelson: Michelson) {
        try {
            this.michelson = flatRightCombPairs(michelson);
            this.#schema = new taquitoMichelson.Schema(this.michelson as taquitoRpc.MichelsonV1Expression);
            this.generated = this.#schema.generateSchema();
        } catch (error) {
            throw new Error(`Invalid Michelson for a schema: ${JSON.stringify(michelson)}. ${errorToString(error)}`);
        }

        Object.freeze(this);
        deepFreeze(this.generated);
    }

    execute<T>(valueMichelson: Michelson): T {
        try {
            const value: unknown = this.#schema.Execute(valueMichelson);
            return value as T;
        } catch (error) {
            throw new Error(`Failed executing Michelson schema on the given value.\n`
                + `Value: ${JSON.stringify(valueMichelson)}\n`
                + `Schema: ${JSON.stringify(this.michelson)}\n`
                + `${errorToString(error)}`);
        }
    }
}
