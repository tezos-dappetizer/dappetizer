import * as taquitoMichelson from '@taquito/michelson-encoder';
import { Nullish, dappetizerAssert, hasLength, isNonEmpty, isReadOnlyArray, keyof } from '@tezos-dappetizer/utils';
import { isEmpty, trimStart } from 'lodash';

import { michelsonTypePrims } from './michelson-prims';
import { Michelson, MichelsonPrim, MichelsonRawValue } from '../rpc-data/rpc-data-interfaces';

export function isPrim(michelson: Michelson): michelson is MichelsonPrim {
    return keyof<MichelsonPrim>('prim') in michelson;
}

export function isRawValue(michelson: Michelson): michelson is MichelsonRawValue {
    return keyof<MichelsonRawValue>('bytes') in michelson
        || keyof<MichelsonRawValue>('int') in michelson
        || keyof<MichelsonRawValue>('string') in michelson;
}

export function getUnderlyingMichelson(schema: taquitoMichelson.Schema | taquitoMichelson.ParameterSchema): Michelson {
    const michelson: Michelson = (schema as any)?.root?.val; // eslint-disable-line
    dappetizerAssert(typeof michelson === 'object', 'Taquito Schema API has changed. Adapt this code.', schema);
    return michelson;
}

export function flatRightCombPairs(michelson: Michelson): Michelson {
    if (isRawValue(michelson)) {
        return michelson;
    }
    if (isReadOnlyArray(michelson)) {
        return michelson.map(flatRightCombPairs);
    }

    let newArgs = michelson.args?.map(flatRightCombPairs);

    if (michelson.prim === michelsonTypePrims.PAIR && hasLength(newArgs, 2)) {
        const [left, right] = newArgs;

        if (isPrim(right) && right.prim === michelsonTypePrims.PAIR && isEmpty(right.annots)) {
            newArgs = [left, ...right.args ?? []];
        }
    }
    return createPrim(michelson.prim, newArgs, michelson.annots);
}

/** Avoids undefined and empty properties. Also, freezes `args`, `annots` and the result. */
export function createPrim(
    prim: string,
    args: readonly Michelson[] | undefined,
    annots: readonly string[] | undefined,
): MichelsonPrim {
    return Object.freeze<MichelsonPrim>({
        prim,
        ...isNonEmpty(args) ? { args: Object.freeze(args) } : undefined,
        ...isNonEmpty(annots) ? { annots: Object.freeze(annots) } : undefined,
    });
}

export function getSingleAnnotCleaned(michelsonPrim: MichelsonPrim): string | null {
    return cleanAnnot(getSingleAnnot(michelsonPrim));
}

export function getSingleAnnot(michelsonPrim: MichelsonPrim): string | null {
    if ((michelsonPrim.annots?.length ?? 0) > 1) {
        throw Error(`Michelson can have max 1 value in 'annots' but there are more: ${JSON.stringify(michelsonPrim)}`);
    }
    return michelsonPrim.annots?.[0] ?? null;
}

export function cleanAnnot(annot: Nullish<string>): string | null {
    return annot ? trimStart(annot, '%:') : null;
}
