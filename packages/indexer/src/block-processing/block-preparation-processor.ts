import { deepFreeze, dumpToString, ENABLE_TRACE, errorToString, injectExplicit, injectLogger, Logger } from '@tezos-dappetizer/utils';
import { injectable } from 'tsyringe';

import { AsyncProcessor } from './async-processor';
import { IndexerMetrics } from '../helpers/indexer-metrics';
import { scopedLogKeys } from '../helpers/scoped-log-keys';
import { BlockConverter } from '../rpc-data/block/block-converter';
import { Block, TaquitoRpcBlock } from '../rpc-data/rpc-data-interfaces';

/** Prepares RPC block for indexing, mainly converting it to indexing-friendly format. */
@injectable()
export class BlockPreparationProcessor implements AsyncProcessor<TaquitoRpcBlock> {
    constructor(
        @injectExplicit() private readonly nextProcessor: AsyncProcessor<Block>,
        private readonly converter: BlockConverter,
        private readonly metrics: IndexerMetrics,
        @injectLogger(BlockPreparationProcessor) private readonly logger: Logger,
    ) {}

    async process(rpcBlock: TaquitoRpcBlock): Promise<void> {
        const blockInfo = {
            level: rpcBlock?.header?.level, // eslint-disable-line @typescript-eslint/no-unnecessary-condition
            hash: rpcBlock?.hash, // eslint-disable-line @typescript-eslint/no-unnecessary-condition
        };

        await this.logger.runWithData({ [scopedLogKeys.INDEXED_BLOCK]: blockInfo }, async () => {
            try {
                deepFreeze(rpcBlock);
                const block = this.converter.convert(rpcBlock);
                this.logger.logDebug(`Successfully prepared {${scopedLogKeys.INDEXED_BLOCK}}.`, {
                    blockFull: this.logger.isTraceEnabled ? block : ENABLE_TRACE,
                });

                await this.nextProcessor.process(block);

                this.metrics.blockLevel.set(block.level);
            } catch (error) {
                throw new Error(`Failed indexing block ${dumpToString(blockInfo)}. ${errorToString(error)}`);
            }
        });
    }
}
