import {
    errorToString,
    injectExplicit,
    injectLogger,
    isNullish,
    Logger,
    SLEEP_HELPER_DI_TOKEN,
    SleepHelper,
} from '@tezos-dappetizer/utils';
import { last } from 'lodash';
import { inject, injectable } from 'tsyringe';

import { AsyncProcessor } from './async-processor';
import { IndexingConfig } from '../config/indexing/indexing-config';

/** Handles block indexing errors firstly by retrying it, then by wrapping the error. */
@injectable()
export class ErrorHandlingIndexingProcessor implements AsyncProcessor {
    constructor(
        @injectExplicit() private readonly nextProcessor: AsyncProcessor,
        private readonly config: IndexingConfig,
        @inject(SLEEP_HELPER_DI_TOKEN) private readonly sleepHelper: SleepHelper,
        @injectLogger(ErrorHandlingIndexingProcessor) private readonly logger: Logger,
    ) {}

    async process(): Promise<void> {
        let isProcessed = false;
        let delayIndex = 0;

        while (!isProcessed) {
            try {
                await this.nextProcessor.process();
                isProcessed = true;
            } catch (error) {
                const maxRetries = this.config.retryDelaysMillis.length;
                const delayMillis = this.config.retryDelaysMillis[delayIndex];

                if (!isNullish(delayMillis)) {
                    this.logger.logError('Retrying block indexing for {time} with {delayMillis} (out of configured {maxRetries}) after {error}.', {
                        time: delayIndex + 1,
                        delayMillis,
                        maxRetries,
                        error,
                    });

                    delayIndex++;
                    await this.sleepHelper.sleep(delayMillis);
                } else if (this.config.retryIndefinitely) {
                    const infiniteDelayMillis = last(this.config.retryDelaysMillis) ?? 0;
                    this.logger.logError('Failed block indexing. It will retry (after {delayMillis}) indefinitely as configured. {error}', {
                        delayMillis: infiniteDelayMillis,
                        error,
                    });

                    await this.sleepHelper.sleep(infiniteDelayMillis);
                } else {
                    // Proceeds to global error handler -> halts the app.
                    throw maxRetries > 0
                        ? new Error(`Failed block indexing after ${maxRetries} retries. ${errorToString(error)}`)
                        : error;
                }
            }
        }
    }
}
