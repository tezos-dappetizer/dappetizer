import { singleton } from 'tsyringe';

@singleton()
export class BlockIndexingHealthState {
    blockBeingProcessed: unknown = null;
    error: unknown = null;
    lastSuccess: { indexedOn: Date; block: unknown } | null = null;
}
