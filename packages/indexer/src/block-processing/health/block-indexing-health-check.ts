import { Clock, CLOCK_DI_TOKEN, HealthCheck, HealthCheckResult, HealthStatus, isNullish } from '@tezos-dappetizer/utils';
import { DeepReadonly } from 'ts-essentials';
import { inject, singleton } from 'tsyringe';

import { BlockIndexingHealthState } from './block-indexing-health-state';
import { IndexingConfig } from '../../config/indexing/indexing-config';
import { BLOCK_INDEXING } from '../block-indexing-background-worker';

export const reasons = {
    notRunning: 'The worker is NOT running - it was not started yet.',
    noSuccessForLongTime: 'Last successfully processed item is older than configured age. The processing is most likely stuck.',
    errorRecently: 'Recently an error occurred.',
    everythingWorks: 'Everything works perfectly.',
} as const;

/** Reports health of the block indexing. */
@singleton()
export class BlockIndexingHealthCheck implements HealthCheck {
    readonly name = BLOCK_INDEXING;

    constructor(
        @inject(BlockIndexingHealthState) private readonly healthState: DeepReadonly<BlockIndexingHealthState>,
        private readonly config: IndexingConfig,
        @inject(CLOCK_DI_TOKEN) private readonly clock: Clock,
    ) {}

    checkHealth(): HealthCheckResult {
        if (isNullish(this.healthState.blockBeingProcessed)) {
            return { status: HealthStatus.Unhealthy, data: reasons.notRunning };
        }

        const lastSuccessAgeMillis = this.clock.getNowTimestamp() - (this.healthState.lastSuccess?.indexedOn.getTime() ?? 0);
        const lastSuccessAgeConfig = this.config.health.lastSuccessAge;
        const lastSucessStatus = (lastSuccessAgeMillis > lastSuccessAgeConfig.unhealthyMillis ? HealthStatus.Unhealthy : null)
            ?? (lastSuccessAgeMillis > lastSuccessAgeConfig.degradedMillis ? HealthStatus.Degraded : null);

        if (lastSucessStatus) {
            return this.createResult(lastSucessStatus, reasons.noSuccessForLongTime, { lastSuccessAgeMillis, lastSuccessAgeConfig });
        }

        return isNullish(this.healthState.error)
            ? this.createResult(HealthStatus.Healthy, reasons.everythingWorks, { error: undefined })
            : this.createResult(HealthStatus.Degraded, reasons.errorRecently, {});
    }

    private createResult(status: HealthStatus, reason: string, data: object): HealthCheckResult {
        return {
            status,
            data: { reason, ...this.healthState, ...data },
        };
    }
}
