import {
    dappetizerAssert,
    injectAllOptional,
    injectExplicit,
    injectLogger,
    isNullish,
    keyof,
    Logger,
} from '@tezos-dappetizer/utils';
import { injectable } from 'tsyringe';

import { AsyncProcessor } from './async-processor';
import { ToBlockLevelProvider } from '../block-source/to-block-level-provider';
import { IndexingConfig } from '../config/indexing/indexing-config';
import { BlockLevelIndexingListener } from '../helpers/block-level-indexing-listener';
import { BLOCK_LEVEL_INDEXING_LISTENERS_DI_TOKEN } from '../helpers/block-level-indexing-listener-di-token';
import { Block } from '../rpc-data/rpc-data-interfaces';

/** Executes respective listeners after `toBlockLevel` is indexed so that the app can gracefully end. */
@injectable()
export class ToBlockLevelProcessor implements AsyncProcessor<Block> {
    constructor(
        @injectExplicit() private readonly nextProcessor: AsyncProcessor<Block>,
        private readonly config: IndexingConfig,
        @injectAllOptional(BLOCK_LEVEL_INDEXING_LISTENERS_DI_TOKEN) private readonly listeners: readonly BlockLevelIndexingListener[],
        @injectLogger(ToBlockLevelProcessor) private readonly logger: Logger,
    ) {}

    async process(block: Block): Promise<void> {
        dappetizerAssert(
            isNullish(this.config.toBlockLevel) || block.level <= this.config.toBlockLevel,
            `Block level greater than configured ${keyof<typeof this.config>('toBlockLevel')} cannot be indexed.`
            + ` It should have been filtered out by ${ToBlockLevelProvider.name}.`,
            { level: block.level, toBlockLevel: this.config.toBlockLevel },
        );

        await this.nextProcessor.process(block);

        if (block.level === this.config.toBlockLevel) {
            this.logger.logInformation('Reached configured {toBlockLevel} so the indexing will end.', {
                toBlockLevel: this.config.toBlockLevel,
            });
            await Promise.all(this.listeners.map(l => l.onToBlockLevelReached()));
        }
    }
}
