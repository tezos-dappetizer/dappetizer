import {
    AsyncIterableProvider,
    Clock,
    CLOCK_DI_TOKEN,
    errorToString,
    injectExplicit,
    injectLogger,
    Logger,
} from '@tezos-dappetizer/utils';
import { inject, injectable } from 'tsyringe';

import { AsyncProcessor } from './async-processor';
import { BlockIndexingHealthState } from './health/block-indexing-health-state';
import { BlockInfoProvider } from '../helpers/block-info-provider';
import { TaquitoRpcBlock } from '../rpc-data/rpc-data-interfaces';

/** Processes blocks from given provider using given processor one-by-one in a loop. */
@injectable()
export class BlockIndexingProcessor implements AsyncProcessor {
    constructor(
        @injectExplicit(0) private readonly blockProvider: AsyncIterableProvider<TaquitoRpcBlock>,
        @injectExplicit(1) private readonly blockProcessor: AsyncProcessor<TaquitoRpcBlock>,
        private readonly infoProvider: BlockInfoProvider,
        private readonly healthState: BlockIndexingHealthState,
        @inject(CLOCK_DI_TOKEN) private readonly clock: Clock,
        @injectLogger(BlockIndexingProcessor) private readonly logger: Logger,
    ) {}

    async process(): Promise<void> {
        this.logger.logInformation('Starting to index blocks.'); // Logged on each retry by wrapping processor.
        this.healthState.blockBeingProcessed = 'determining the starting block';

        try {
            for await (const block of this.blockProvider.iterate()) {
                this.healthState.blockBeingProcessed = this.infoProvider.getInfo(block);
                await this.blockProcessor.process(block);

                this.healthState.error = null;
                this.healthState.lastSuccess = {
                    block: this.healthState.blockBeingProcessed,
                    indexedOn: this.clock.getNowDate(),
                };
                this.healthState.blockBeingProcessed = 'determining the next block';
            }
            this.healthState.blockBeingProcessed = 'successfully finished';
        } catch (error) {
            this.healthState.error = error;
            throw new Error(`Failed block indexing. ${errorToString(error)}.`);
        }
    }
}
