import {
    AddressPrefix,
    CONFIG_NETWORK_DI_TOKEN,
    ConfigElement,
    ConfigError,
    ConfigObjectElement,
    ConfigWithDiagnostics,
    getEnumValues,
    injectOptional,
    isNullish,
    keyof,
    MAINNET,
    NETWORK_SPECIFIC_CONFIG_ELEMENT_DI_TOKEN,
} from '@tezos-dappetizer/utils';
import { NonEmptyArray } from 'ts-essentials';
import { inject, singleton } from 'tsyringe';

import { SelectiveIndexingConfig, SelectiveIndexingItemConfig } from './selective-indexing-config';
import { ContractIndexer } from '../../client-indexers/contract-indexer';

export interface IndexingHealthConfig {
    readonly lastSuccessAge: IndexingLastSuccessAgeConfig;
}

export interface IndexingLastSuccessAgeConfig {
    readonly degradedMillis: number;
    readonly unhealthyMillis: number;
}

export type ContractsBoostConfig = TzktContractsBoostConfig;

export interface TzktContractsBoostConfig {
    readonly type: ContractsBoostType.Tzkt;
    readonly apiUrl: string;
}

export enum ContractsBoostType {
    Tzkt = 'tzkt',
}

@singleton()
export class IndexingConfig implements SelectiveIndexingConfig, ConfigWithDiagnostics {
    static readonly ROOT_NAME = 'indexing';

    readonly configPropertyPath: string;
    readonly fromBlockLevel: number;
    readonly toBlockLevel: number | null;
    readonly blockQueueSize: number;
    readonly health: IndexingHealthConfig;
    readonly retryDelaysMillis: readonly number[];
    readonly retryIndefinitely: boolean;
    readonly headLevelOffset: number;
    readonly contractCacheMillis: number;
    readonly contracts: Readonly<NonEmptyArray<SelectiveIndexingItemConfig>> | null;
    readonly smartRollups: Readonly<NonEmptyArray<SelectiveIndexingItemConfig>> | null;
    readonly contractBoost: ContractsBoostConfig | null;

    constructor(
        @inject(NETWORK_SPECIFIC_CONFIG_ELEMENT_DI_TOKEN) rootElement: ConfigObjectElement, // eslint-disable-line @typescript-eslint/indent
        @injectOptional(CONFIG_NETWORK_DI_TOKEN) network: string,
    ) {
        const thisElement = rootElement.get(IndexingConfig.ROOT_NAME).asObject<keyof IndexingConfig>();
        this.configPropertyPath = thisElement.propertyPath;

        this.fromBlockLevel = thisElement.get('fromBlockLevel').asInteger({ min: 0 });
        this.toBlockLevel = thisElement.getOptional('toBlockLevel')?.asInteger({ min: this.fromBlockLevel })
            ?? null;

        this.blockQueueSize = thisElement.getOptional('blockQueueSize')?.asInteger({ min: 1 })
            ?? 5;
        this.contracts = getSelectiveIndexingConfigs(thisElement.getOptional('contracts'), 'KT1');
        this.smartRollups = getSelectiveIndexingConfigs(thisElement.getOptional('smartRollups'), 'sr1');
        this.contractBoost = getContractBoostConfig(thisElement, this.contracts, network);
        this.contractCacheMillis = thisElement.getOptional('contractCacheMillis')?.asInteger({ min: 0 })
            ?? 600_000;
        this.headLevelOffset = thisElement.getOptional('headLevelOffset')?.asInteger({ min: 0 })
            ?? 1;
        this.health = getHealthConfig(thisElement);
        this.retryDelaysMillis = thisElement.getOptional('retryDelaysMillis')?.asArray().map(e => e.asInteger({ min: 0 }))
            ?? [100, 1_000, 5_000, 60_000];
        this.retryIndefinitely = thisElement.getOptional('retryIndefinitely')?.asBoolean()
            ?? true;
    }

    getDiagnostics(): [string, unknown] {
        return [IndexingConfig.ROOT_NAME, this];
    }
}

type IndexingConfigElement = ConfigObjectElement<keyof IndexingConfig>;

function getSelectiveIndexingConfigs(
    itemsElement: ConfigElement | null,
    requiredAddressPrefix: AddressPrefix,
): Readonly<NonEmptyArray<SelectiveIndexingItemConfig>> | null {
    if (!itemsElement) {
        return null;
    }

    const items = itemsElement.asArray('NotEmpty')
        .map(e => e.asObject<keyof SelectiveIndexingItemConfig>())
        .map(contractElement => Object.freeze<SelectiveIndexingItemConfig>({
            addresses: toNonEmptyReadonly(contractElement.get('addresses').asArray('NotEmpty').map(e => e.asAddress([requiredAddressPrefix]))),
            name: contractElement.getOptional('name')?.asString('NotWhiteSpace') ?? null,
        }));
    const allAddresses = items.flatMap(c => c.addresses);

    const maxAddressCount = 100;
    if (allAddresses.length > maxAddressCount) {
        const reason = `Total number of addresses is limited to ${maxAddressCount} but there are ${allAddresses.length} addresses.`
            + ` Evaluate them dynamically in ${keyof<ContractIndexer<unknown>>('shouldIndex')}().`;
        throw new ConfigError({ ...itemsElement, reason });
    }

    const addressConflict = allAddresses.find((address, index) => allAddresses.includes(address, index + 1));
    if (addressConflict) {
        const reason = `It can contain an address only once but '${addressConflict}' is specified multiple times.`;
        throw new ConfigError({ ...itemsElement, reason });
    }

    return toNonEmptyReadonly(items);
}

function toNonEmptyReadonly<T>(items: T[]): Readonly<NonEmptyArray<T>> {
    return Object.freeze(items) as NonEmptyArray<T>;
}

export const DEFAULT_TZKT_URL = 'https://api.tzkt.io/';

function getContractBoostConfig(
    indexingElement: IndexingConfigElement,
    contracts: {} | null,
    network: string,
): IndexingConfig['contractBoost'] {
    const boostElement = indexingElement.getOptional('contractBoost')?.asObject<keyof TzktContractsBoostConfig>();
    const type = boostElement?.getOptional('type')?.asOneOf(['disabled', ...getEnumValues(ContractsBoostType)]);

    if (!boostElement || type === undefined || type === 'disabled') {
        return null;
    }
    if (isNullish(contracts)) {
        const reason = `It cannot be enabled when no '${keyof<IndexingConfig>('contracts')}' are specified.`;
        throw new ConfigError({ ...boostElement, reason });
    }

    const apiUrl = network === MAINNET
        ? boostElement.getOptional('apiUrl')?.asAbsoluteUrl() ?? DEFAULT_TZKT_URL
        : boostElement.get('apiUrl').asAbsoluteUrl();

    return { type, apiUrl };
}

function getHealthConfig(indexingElement: IndexingConfigElement): IndexingConfig['health'] {
    const healthElement = indexingElement.getOptional('health')?.asObject<keyof IndexingHealthConfig>();
    const lastSuccessAgeElement = healthElement?.getOptional('lastSuccessAge')?.asObject<keyof IndexingLastSuccessAgeConfig>();

    return {
        lastSuccessAge: {
            degradedMillis: lastSuccessAgeElement?.getOptional('degradedMillis')?.asInteger({ min: 1 })
                ?? 300_000,
            unhealthyMillis: lastSuccessAgeElement?.getOptional('unhealthyMillis')?.asInteger({ min: 1 })
                ?? 600_000,
        },
    };
}
