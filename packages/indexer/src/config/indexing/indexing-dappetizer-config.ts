import { NonEmptyArray } from 'ts-essentials';

/**
 * Configures actual indexing.
 * This is used as a part of {@link "@tezos-dappetizer/database".DappetizerConfigUsingDb}.
 * @example Sample usage (all properties specified explicitly) within `dappetizer.config.ts` file:
 * ```typescript
 * import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';
 *
 * const config: DappetizerConfigUsingDb = {
 *     networks: {
 *         mainnet: {
 *             indexing: {
 *                 fromBlockLevel: 1796556,
 *                 toBlockLevel: 2000000,
 *                 contracts: [
 *                     {
 *                         addresses: ['KT191reDVKrLxU9rjTSxg53wRqj6zh8pnHgr'],
 *                         name: 'TezosDomains',
 *                     },
 *                 ],
 *                 smartRollups: [
 *                     {
 *                         addresses: ['sr1LhGA2zC9VcYALSifpRBCgDiQfDSQ6bb4x'],
 *                         name: 'TezosDomains',
 *                     },
 *                 ],
 *                 contractBoost: {
 *                     type: 'tzkt',
 *                     tzktUrl: 'https://api.custom-tzkt.io/',
 *                 },
 *                 retryDelaysMillis: [100, 1_000, 5_000, 60_000],
 *                 retryIndefinitely: true,
 *                 blockQueueSize: 5,
 *                 health: {
 *                     lastSuccessAge: {
 *                         degradedMillis: 300_000,
 *                         unhealthyMillis: 600_000,
 *                     },
 *                 },
 *             },
 *             ... // Other config parts.
 *         },
 *         testnet: {
 *             indexing: {
 *                 fromBlockLevel: 856123,
 *                 toBlockLevel: 1200000,
 *                 contracts: [
 *                     {
 *                         addresses: ['KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5'],
 *                         name: 'TezosDomains',
 *                     },
 *                 ],
 *                 contractBoost: {
 *                     type: 'tzkt',
 *                     tzktUrl: 'https://api.custom-tzkt.io/',
 *                 },
 *                 retryDelaysMillis: [100, 1_000, 5_000, 60_000],
 *                 retryIndefinitely: true,
 *                 blockQueueSize: 5,
 *                 health: {
 *                     lastSuccessAge: {
 *                         degradedMillis: 300_000,
 *                         unhealthyMillis: 600_000,
 *                     },
 *                 },
 *             },
 *             ... // Other config parts.
 *         },
 *     },
 *     ... // Other config parts.
 * };
 *
 * export default config;
 * ```
 * @category Dappetizer Config
 */
export interface IndexingDappetizerConfig {
    /**
     * The level of Tezos block from which the indexing should start.
     * @limits An integer with a minimum `0`.
     */
    fromBlockLevel: number;

    /**
     * The last level of Tezos block which should be indexed.
     * If `undefined` then indexing continues with fresh new blocks from Tezos monitor.
     * @limits An integer with a minimum {@link fromBlockLevel}.
     * @default `undefined`
     */
    toBlockLevel?: number;

    /**
     * The selective indexing of particular smart contracts. If an non-empty array is specified,
     * then {@link ContractIndexer}-s from all {@link IndexerModule}-s are executed only when particular contracts are encountered.
     * @default `undefined`
     */
    contracts?: {
        /**
         * The smart contract addresses with `KT1` prefix.
         * @limits A non-empty array.
         */
        addresses: NonEmptyArray<string>;

        /**
         * An arbitrary name to refer the smart contract conveniently.
         * @default `undefined`
         */
        name?: string;
    }[];

    /**
     * The time in milliseconds for how long {@link "@tezos-dappetizer/indexer".Contract} instances are cached.
     * The expiration is sliding according to the last contract usage.
     * The caching is needed because contract are downloaded from Tezos node which takes quite some time.
     * @limits An integer with minimum `0`.
     * @default `600_000`
     */
    contractCacheMillis?: number;

    /**
     * The selective indexing of particular smart rollups. If an non-empty array is specified,
     * then {@link SmartRollupIndexer}-s from all {@link IndexerModule}-s are executed only when particular contracts are encountered.
     * @default `[]`
     */
    smartRollups?: {
        /**
         * The smart rollup addresses with `sr1` prefix.
         * @limits A non-empty array.
         */
        addresses: NonEmptyArray<string>;

        /** An arbitrary name to refer the smart rollup conveniently.  */
        name?: string;
    }[];

    /** Boost causes Dappetizer to only index blocks where the configured {@link contracts} are used. */
    contractBoost?: {
        /**
         * Specifies what type of boost should be used.
         * @default `'disabled'`
         */
        type?: 'disabled' | 'tzkt';

        /**
         * The REST API endpoint of the TzKT instance that should be used for the boost.
         * @limits An absolute URL string.
         * @default `'https://api.tzkt.io/'` (only for `mainnet`) or `undefined` (otherwise in order to avoid inconsistencies)
         */
        apiUrl?: string;
    };

    /**
     * The array which configures how many times the entire block indexing is retried when it fails.
     * An array item is a sleep delay is milliseconds until the retry.
     * @default `[100, 1_000, 5_000, 60_000]`
     */
    retryDelaysMillis?: number[];

    /**
     * Indicates if the entire block indexing should be retried indefinitely when it fails
     * The sleep delay between retries is the last one from {@link retryDelaysMillis} or zero.
     * @default `true`
     */
    retryIndefinitely?: boolean;

    /**
     * The size of the queue for preloading blocks from Tezos node RPC for faster indexing.
     * @limits An integer with a minimum `1`.
     * @default `5`
     */
    blockQueueSize?: number;

    /**
     * The offset to delay HEAD block level. It is useful for Tenderbake protocols with many reorganizations on HEAD block.
     * @limits An integer with a minimum `0`.
     * @default `1`
     */
    headLevelOffset?: number;

    /** Configures reporting of health status of the indexing. */
    health?: {
        /**
         * The maximum age of the last successfully indexed block when indexing is considered `Healthy`.
         * If it gets older, then the related health check is changed to `Degraded`, then `Unhealthy`.
         */
        lastSuccessAge?: {
            /**
             * The age in milliseconds when related health check turns `Degraded`.
             * @limits An integer with minimum `1`.
             * @default `300_000`
             */
            degradedMillis?: number;

            /**
             * The age in milliseconds when related health check turns `Unhealthy`.
             * @limits An integer with minimum `1`.
             * @default `600_000`
             */
            unhealthyMillis?: number;
        };
    };
}
