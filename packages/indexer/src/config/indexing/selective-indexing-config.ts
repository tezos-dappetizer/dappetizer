import { NonEmptyArray } from 'ts-essentials';
import { InjectionToken } from 'tsyringe';

export interface SelectiveIndexingConfig {
    readonly configPropertyPath: string;
    readonly contracts: Readonly<NonEmptyArray<SelectiveIndexingItemConfig>> | null;
}

export interface SelectiveIndexingItemConfig {
    readonly addresses: Readonly<NonEmptyArray<string>>;
    readonly name: string | null;
}

export const SELECTIVE_INDEXING_CONFIG_DI_TOKEN: InjectionToken<SelectiveIndexingConfig> = 'Dappetizer:Indexer:SelectiveIndexingConfig';
