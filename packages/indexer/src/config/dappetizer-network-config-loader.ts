import { argGuard } from '@tezos-dappetizer/utils';

import { DappetizerConfig } from './dappetizer-config';
import { loadNetworkConfigsImpl, PREFIX, SUFFIX } from './dappetizer-network-config-loader-impl';

/** @internal Intended only for Dappetizer itself. */
export const NETWORK_CONFIG_FILE_PREFIX: string = PREFIX;

/** @internal Intended only for Dappetizer itself. */
export const NETWORK_CONFIG_FILE_SUFFIX: string = SUFFIX;

/**
 * Loads network-specific part of {@link "@tezos-dappetizer/indexer".DappetizerConfig}
 * from files with name like `dappetizer.{NETWORK}.config.json` in the specified directory.
 */
export function loadDappetizerNetworkConfigs(dirPath: string): DappetizerConfig['networks'] {
    argGuard.nonWhiteSpaceString(dirPath, 'dirPath');
    return loadNetworkConfigsImpl(dirPath);
}
