import {
    HttpServerDappetizerConfig,
    LoggingDappetizerConfig,
    UsageStatisticsDappetizerConfig,
    TezosNodeDappetizerConfig,
    TimeDappetizerConfig,
} from '@tezos-dappetizer/utils';
import { NonEmptyArray } from 'ts-essentials';

import { IndexingDappetizerConfig } from './indexing/indexing-dappetizer-config';
import { IpfsDappetizerConfig } from '../helpers/ipfs/ipfs-dappetizer-config';

/**
 * Entire config for an indexer app built using Dappetizer.
 * It corresponds to the value exported from a config file, see {@link "@tezos-dappetizer/utils".candidateConfigFileNames}.
 * If you intend to use standard Dappetizer {@link "@tezos-dappetizer/database".DbContext},
 * then {@link "@tezos-dappetizer/database".DappetizerConfigUsingDb} should be used instead.
 * @example Sample `dappetizer.config.ts` file with only mandatory values specified:
 * ```typescript
 * import { DappetizerConfig } from '@tezos-dappetizer/indexer';
 *
 * const config: DappetizerConfig = {
 *     modules: [
 *         {
 *             id: 'your-node-module-or-relative-path',
 *             config: 'module-config',
 *         },
 *     ],
 *     networks: {
 *         mainnet: {
 *             indexing: {
 *                 fromBlockLevel: 1796556,
 *             },
 *             tezosNode: {
 *                 url: 'https://mainnet-tezos.giganode.io',
 *             },
 *         },
 *         testnet: {
 *             indexing: {
 *                 fromBlockLevel: 856123,
 *             },
 *             tezosNode: {
 *                 url: 'https://testnet-tezos.giganode.io',
 *             },
 *         },
 *     },
 * };
 *
 * export default config;
 * ```
 * @category Dappetizer Config
 */
export interface DappetizerConfig {
    /** Configures HTTP server of this indexer app mainly for monitoring. */
    httpServer?: HttpServerDappetizerConfig;

    /** Configures connections to {@link https://ipfs.io/ | IPFS} for metadata retrieval. */
    ipfs?: IpfsDappetizerConfig;

    /** Configures logging of app execution. */
    logging?: LoggingDappetizerConfig;

    /** The non-empty array of {@link "@tezos-dappetizer/indexer".IndexerModule} to be executed for indexing. */
    modules: NonEmptyArray<{
        /** Node.js module name or path relative to the config file. It is used with Node.js `require()`. */
        id: string;

        /** Custom configuration specific for the module. The data type is defined by the module. */
        config?: unknown;
    }>;

    /** The indexing config specific for particular Tezos networks e.g. `mainnet`. */
    networks: {
        [network: string]: {
            /** Configures actual indexing. */
            indexing: IndexingDappetizerConfig;

            /** Configures connection to Tezos node to be used to fetch blockchain data. */
            tezosNode: TezosNodeDappetizerConfig;
        };
    };

    /** Configures generic time related features. */
    time?: TimeDappetizerConfig;

    /** Configures usage statistics. */
    usageStatistics?: UsageStatisticsDappetizerConfig;
}
