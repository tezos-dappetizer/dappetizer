import { ToJSON } from '@tezos-dappetizer/utils';

import { Contract, ContractAbstraction, ContractConfig } from './contract';
import { BigMapInfo } from './data/big-map-info/big-map-info';
import { ContractEventInfo } from './data/contract-event/contract-event-info';
import { ContractParameterSchemas } from './data/schemas/contract-parameter-schemas';
import { MichelsonSchema } from '../michelson/michelson-schema';

export class ContractImpl implements Contract, ToJSON {
    readonly address: string;

    constructor(
        readonly abstraction: ContractAbstraction,
        readonly config: ContractConfig,
        readonly bigMaps: readonly BigMapInfo[],
        readonly parameterSchemas: ContractParameterSchemas,
        readonly storageSchema: MichelsonSchema,
        readonly events: readonly ContractEventInfo[],
    ) {
        this.address = abstraction.address;
        Object.freeze(this);
    }

    toJSON(): unknown {
        const { abstraction, ...serializableProperties } = this;
        return serializableProperties;
    }
}
