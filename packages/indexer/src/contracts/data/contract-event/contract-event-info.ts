import { MichelsonSchema } from '../../../michelson/michelson-schema';

export interface ContractEventInfo {
    readonly tag: string | null;
    readonly schema: MichelsonSchema | null;
}
