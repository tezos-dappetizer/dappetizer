import { errorToString } from '@tezos-dappetizer/utils';
import { orderBy } from 'lodash';
import { singleton } from 'tsyringe';

import { ContractParameterSchemas } from './contract-parameter-schemas';
import { MichelsonSchema } from '../../../michelson/michelson-schema';
import { getUnderlyingMichelson } from '../../../michelson/michelson-utils';
import { ContractAbstraction } from '../../contract';

@singleton()
export class ContractParameterSchemasFactory {
    create(contractAbstraction: ContractAbstraction): ContractParameterSchemas {
        const entrypoints: Record<string, MichelsonSchema> = {};
        const rawEntrypoints = Object.entries(contractAbstraction.entrypoints.entrypoints).map(e => ({ name: e[0], michelson: e[1] }));

        for (const entrypoint of orderBy(rawEntrypoints, e => e.name)) {
            try {
                entrypoints[entrypoint.name] = new MichelsonSchema(entrypoint.michelson);
            } catch (error) {
                throwError(error, `entrypoint '${entrypoint.name}'`);
            }
        }

        try {
            return Object.freeze<ContractParameterSchemas>({
                default: new MichelsonSchema(getUnderlyingMichelson(contractAbstraction.parameterSchema)),
                entrypoints,
            });
        } catch (error) {
            throwError(error, 'default entrypoint');
        }
    }
}

function throwError(error: unknown, entrypointDescription: string): never {
    throw new Error(`Failed to prepare schema for ${entrypointDescription} of the contract. ${errorToString(error)}`);
}
