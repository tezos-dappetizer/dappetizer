import { BigNumber } from 'bignumber.js';

import { MichelsonSchema } from '../../../michelson/michelson-schema';

export interface BigMapInfo {
    readonly lastKnownId: BigNumber;
    readonly name: string | null;
    readonly path: readonly string[];
    readonly pathStr: string;
    readonly keySchema: MichelsonSchema;
    readonly valueSchema: MichelsonSchema;
}
