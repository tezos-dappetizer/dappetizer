import * as taquitoMichelson from '@taquito/michelson-encoder';
import * as taquito from '@taquito/taquito';
import { dappetizerAssert, errorToString, hasConstructor, hasLength, isObjectLiteral } from '@tezos-dappetizer/utils';
import { orderBy } from 'lodash';
import { singleton } from 'tsyringe';

import { BigMapInfo } from './big-map-info';
import { BigMapInfoImpl } from './big-map-info-impl';
import { michelsonTypePrims } from '../../../michelson/michelson-prims';
import { MichelsonSchema } from '../../../michelson/michelson-schema';
import { getUnderlyingMichelson, isPrim } from '../../../michelson/michelson-utils';
import { Michelson } from '../../../rpc-data/rpc-data-interfaces';

@singleton()
export class BigMapInfosFactory {
    create(storage: unknown): readonly BigMapInfo[] {
        try {
            const bigMaps = Array.from(discoverBigMaps(storage, []));
            return Object.freeze(orderBy(bigMaps, m => m.pathStr));
        } catch (error) {
            throw new Error(`Failed to find big maps from the storage.\n`
                + `${errorToString(error)}\n`
                + `Storage: ${JSON.stringify(storage)}`);
        }
    }
}

function *discoverBigMaps(storageField: unknown, path: readonly string[]): Iterable<BigMapInfo> {
    if (hasConstructor(storageField, taquito.BigMapAbstraction)) {
        const michelson = getUnderlyingMichelson(getSchema(storageField));
        const [keyMichelson, valueMichelson] = splitToKeyAndValue(michelson);

        yield new BigMapInfoImpl(
            path,
            storageField.toString(),
            new MichelsonSchema(keyMichelson),
            new MichelsonSchema(valueMichelson),
        );
    } else if (isObjectLiteral(storageField)) {
        for (const [propertyName, propertyValue] of Object.entries(storageField)) {
            yield* discoverBigMaps(propertyValue, [...path, propertyName]);
        }
    }
}

function getSchema(bigMap: taquito.BigMapAbstraction): taquitoMichelson.Schema {
    const schema: taquitoMichelson.Schema = (bigMap as any).schema; // eslint-disable-line
    dappetizerAssert(typeof schema === 'object', 'Taquito BigMapAbstraction API has changed. Adapt this code.', bigMap);
    return schema;
}

function splitToKeyAndValue(michelson: Michelson): readonly [Michelson, Michelson] {
    if (!isPrim(michelson) || michelson.prim !== michelsonTypePrims.BIG_MAP || !hasLength(michelson.args, 2)) {
        throw new Error(`Given Michelson is not a valid big map: ${JSON.stringify(michelson)}`);
    }
    return michelson.args;
}
