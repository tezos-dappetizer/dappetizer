import * as taquito from '@taquito/taquito';
import * as taquitoTzip12 from '@taquito/tzip12';
import * as taquitoTzip16 from '@taquito/tzip16';
import { errorToString, injectLogger, Logger, runInPerformanceSection } from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

import { Contract, ContractAbstraction } from './contract';
import { ContractImpl } from './contract-impl';
import { BigMapInfosFactory } from './data/big-map-info/big-map-infos-factory';
import { ContractEventInfosResolver } from './data/contract-event/contract-event-infos-resolver';
import { ContractParameterSchemasFactory } from './data/schemas/contract-parameter-schemas-factory';
import { ContractStorageSchemaFactory } from './data/schemas/contract-storage-schema-factory';
import { SelectiveIndexingConfigResolver } from '../helpers/selective-indexing-config-resolver';
import { TAQUITO_TEZOS_TOOLKIT_DI_TOKEN } from '../helpers/tezos-toolkit-di-token';

@singleton()
export class ContractFactory {
    constructor(
        @inject(TAQUITO_TEZOS_TOOLKIT_DI_TOKEN) private readonly taquitoToolkit: taquito.TezosToolkit,
        private readonly bigMapInfosFactory: BigMapInfosFactory,
        private readonly parameterSchemasFactory: ContractParameterSchemasFactory,
        private readonly storageSchemaFactory: ContractStorageSchemaFactory,
        private readonly configResolver: SelectiveIndexingConfigResolver,
        private readonly eventsResolver: ContractEventInfosResolver,
        @injectLogger(ContractFactory) private readonly logger: Logger,
    ) {}

    async create(address: string): Promise<Contract> {
        try {
            const [abstraction, storage] = await runInPerformanceSection(this.logger, async () => {
                const contractAbstraction = await this.fetchAbstraction(address);
                return [contractAbstraction, await this.fetchStorage(contractAbstraction)];
            });

            const config = this.configResolver.resolve(address, 'contracts');
            const bigMaps = this.bigMapInfosFactory.create(storage);
            const storageSchema = this.storageSchemaFactory.create(abstraction);
            const parameterSchemas = this.parameterSchemasFactory.create(abstraction);
            const events = this.eventsResolver.resolve(abstraction);

            return new ContractImpl(abstraction, config, bigMaps, parameterSchemas, storageSchema, events);
        } catch (error) {
            throw new Error(`Failed to resolve details of contract '${address}'. ${errorToString(error)}`);
        }
    }

    private async fetchAbstraction(address: string): Promise<ContractAbstraction> {
        try {
            const composer = taquito.compose(taquitoTzip12.tzip12, taquitoTzip16.tzip16);
            return await this.taquitoToolkit.contract.at(address, composer);
        } catch (error) {
            throw new Error(`Failed to retrieve @taquito abstraction of the contract. ${errorToString(error)}`);
        }
    }

    private async fetchStorage(abstraction: ContractAbstraction): Promise<unknown> {
        try {
            return await abstraction.storage();
        } catch (error) {
            throw new Error(`Failed to retrieve @taquito deserialized storage of the contract. ${errorToString(error)}`);
        }
    }
}
