import * as taquito from '@taquito/taquito';
import * as taquitoTzip12 from '@taquito/tzip12';
import * as taquitoTzip16 from '@taquito/tzip16';

import { BigMapInfo } from './data/big-map-info/big-map-info';
import { ContractEventInfo } from './data/contract-event/contract-event-info';
import { ContractParameterSchemas } from './data/schemas/contract-parameter-schemas';
import { MichelsonSchema } from '../michelson/michelson-schema';

export type ContractAbstraction =
    taquito.ContractAbstraction<taquito.ContractProvider>
    & ReturnType<typeof taquitoTzip12.tzip12>
    & ReturnType<typeof taquitoTzip16.tzip16>;

/** Properties of the contract from the local config file. */
export interface ContractConfig {
    /** Indicates selective indexing of this contract. It is also `true` if no selective indexing is configured. */
    readonly toBeIndexed: boolean;

    /** Configured name. It is `null` if no `name` is configured or selective is not configured. */
    readonly name: string | null;
}

export interface Contract {
    /** Contract `KT1` address. */
    readonly address: string;

    readonly config: ContractConfig;
    readonly abstraction: ContractAbstraction;
    readonly bigMaps: readonly BigMapInfo[];
    readonly parameterSchemas: ContractParameterSchemas;
    readonly storageSchema: MichelsonSchema;
    readonly events: readonly ContractEventInfo[];
}
