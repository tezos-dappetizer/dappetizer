import { StrictOmit } from 'ts-essentials';

import { ContractIndexer } from '../../client-indexers/contract-indexer';

export const UNBOUND_CONTRACT_DATA = Symbol('UnboundContractData');
export type UnboundContractData = typeof UNBOUND_CONTRACT_DATA;

/** Client's ContractIndexer bound to particular contract and its contract data from shouldIndex(). */
export type BoundContractIndexer<TDbContext, TContextData> =
    StrictOmit<Required<ContractIndexer<TDbContext, TContextData, UnboundContractData>>, 'name' | 'shouldIndex'>;
