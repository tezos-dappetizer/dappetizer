import { singleton } from 'tsyringe';

import { BalanceUpdateTraverser } from './balance-update-traverser';
import { OperationGroupTraverser } from './operation-group-traverser';
import {
    BalanceUpdateIndexingContext,
    BlockIndexingContext,
    OperationGroupIndexingContext,
} from '../client-indexers/block-data-indexer';
import { IndexerModuleWrapper } from '../client-indexers/wrappers/indexer-module-wrapper';
import { Block } from '../rpc-data/rpc-data-interfaces';

@singleton()
export class BlockTraverser<TDbContext> {
    constructor(
        private readonly operationGroupTraverser: OperationGroupTraverser<TDbContext>,
        private readonly balanceUpdateTraverser: BalanceUpdateTraverser<TDbContext>,
    ) {}

    async traverse<TContextData>(
        indexerModule: IndexerModuleWrapper<TDbContext, TContextData>,
        dbContext: TDbContext,
        block: Block,
        indexingContext: BlockIndexingContext<TContextData>,
    ): Promise<void> {
        await indexerModule.blockDataIndexer.indexBlock(block, dbContext, indexingContext);

        const balanceUpdatesContext = Object.freeze<BalanceUpdateIndexingContext<TContextData>>({
            ...indexingContext,
            block,
            balanceUpdateSource: 'BlockMetadata',
            operationGroup: null,
            mainOperation: null,
            internalOperation: null,
        });
        await this.balanceUpdateTraverser.traverse(indexerModule, dbContext, block.metadataBalanceUpdates, balanceUpdatesContext);

        const operationGroupContext = Object.freeze<OperationGroupIndexingContext<TContextData>>({
            ...indexingContext,
            block,
        });
        for (const operationGroup of block.operationGroups) {
            await this.operationGroupTraverser.traverse(indexerModule, dbContext, operationGroup, operationGroupContext);
        }
    }
}
