import { dappetizerAssert, injectLogger, isNullish, Logger } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { OperationWithResultIndexingContext } from '../client-indexers/block-data-indexer';
import { ContractEvent, ContractIndexingContext } from '../client-indexers/contract-indexer';
import { IndexerModuleWrapper } from '../client-indexers/wrappers/indexer-module-wrapper';
import { BoundContractIndexer, UNBOUND_CONTRACT_DATA, UnboundContractData } from '../contracts/bound-indexer/bound-contract-indexer';
import { scopedLogKeys } from '../helpers/scoped-log-keys';
import {
    BigMapDiffAction,
    isApplied,
    LazyBigMapDiff,
    LazyContract,
    LazyStorageChange,
    LazyTransactionParameter,
    OperationKind,
} from '../rpc-data/rpc-data-interfaces';

export interface ContractTraversingContext<TData> extends OperationWithResultIndexingContext<TData> {
    readonly contract: LazyContract;
    readonly bigMapDiffs: readonly LazyBigMapDiff[];
    readonly operationWithResult: ContractIndexingContext['operationWithResult'];
    readonly storageChange: LazyStorageChange | null;
    readonly transactionParameter: LazyTransactionParameter | null;
}

@singleton()
export class ContractTraverser<TDbContext> {
    constructor(
        @injectLogger(ContractTraverser) private readonly logger: Logger,
    ) {}

    async traverse<TContextData>(
        indexerModule: IndexerModuleWrapper<TDbContext, TContextData>,
        dbContext: TDbContext,
        indexingContext: ContractTraversingContext<TContextData>,
    ): Promise<void> {
        const contractIndexers = indexerModule.contractIndexers;
        const lazyContract = indexingContext.contract;
        const hasDataToIndex = indexingContext.operationWithResult.kind === OperationKind.Origination
            || !isNullish(indexingContext.transactionParameter)
            || !isNullish(indexingContext.storageChange)
            || indexingContext.bigMapDiffs.length > 0;

        if (!contractIndexers || !lazyContract.config.toBeIndexed || !hasDataToIndex) {
            return;
        }

        await this.logger.runWithData({ [scopedLogKeys.INDEXED_CONTRACT_ADDRESS]: lazyContract.address }, async () => {
            const contractIndexer = await contractIndexers.resolve(lazyContract, dbContext);
            if (!contractIndexer) {
                return;
            }

            const [contract, storageChange, transactionParameter, bigMapDiffs] = await Promise.all([
                lazyContract.getContract(),
                indexingContext.storageChange?.getStorageChange() ?? null,
                indexingContext.transactionParameter?.getTransactionParameter() ?? null,
                Promise.all(indexingContext.bigMapDiffs.map(async d => d.getBigMapDiff())),
            ]);
            await this.runIndexing(contractIndexer, dbContext, {
                ...indexingContext,
                bigMapDiffs,
                contract,
                contractData: UNBOUND_CONTRACT_DATA,
                events: Array.from(iterateEvents(indexingContext, lazyContract.address)),
                storageChange,
                transactionParameter,
            });
        });
    }

    private async runIndexing<TContextData>(
        contractIndexer: BoundContractIndexer<TDbContext, TContextData>,
        dbContext: TDbContext,
        indexingContext: ContractIndexingContext<TContextData, UnboundContractData>,
    ): Promise<void> {
        if (indexingContext.operationWithResult.kind === OperationKind.Origination) {
            await contractIndexer.indexOrigination(indexingContext.operationWithResult, dbContext, {
                ...indexingContext,
                operationWithResult: indexingContext.operationWithResult,
            });
        }

        if (indexingContext.transactionParameter) {
            const operation = indexingContext.operationWithResult;
            dappetizerAssert(operation.kind === OperationKind.Transaction, 'Operation with transaction param must be transaction', operation);

            await contractIndexer.indexTransaction(indexingContext.transactionParameter, dbContext, {
                ...indexingContext,
                operationWithResult: operation,
                transactionParameter: indexingContext.transactionParameter,
            });
        }

        if (indexingContext.storageChange) {
            await contractIndexer.indexStorageChange(indexingContext.storageChange, dbContext, {
                ...indexingContext,
                storageChange: indexingContext.storageChange,
            });
        }

        for (const bigMapDiff of indexingContext.bigMapDiffs) {
            await contractIndexer.indexBigMapDiff(bigMapDiff, dbContext, { ...indexingContext, bigMapDiff });

            if (bigMapDiff.action === BigMapDiffAction.Update) {
                await contractIndexer.indexBigMapUpdate(bigMapDiff, dbContext, { ...indexingContext, bigMapDiff });
            }
        }

        for (const event of indexingContext.events) {
            await contractIndexer.indexEvent(event, dbContext, { ...indexingContext, event });
        }
    }
}

function *iterateEvents(indexingContext: ContractTraversingContext<unknown>, contractAddress: string): Iterable<ContractEvent> {
    for (const operation of indexingContext.mainOperation.internalOperations) {
        if (operation.kind === OperationKind.Event && operation.source.address === contractAddress && isApplied(operation)) {
            yield operation;
        }
    }
}
