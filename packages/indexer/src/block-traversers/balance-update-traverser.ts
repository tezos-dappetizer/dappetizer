import { singleton } from 'tsyringe';

import { BalanceUpdateIndexingContext } from '../client-indexers/block-data-indexer';
import { IndexerModuleWrapper } from '../client-indexers/wrappers/indexer-module-wrapper';
import { BalanceUpdate } from '../rpc-data/rpc-data-interfaces';

@singleton()
export class BalanceUpdateTraverser<TDbContext> {
    async traverse<TContextData>(
        indexerModule: IndexerModuleWrapper<TDbContext, TContextData>,
        dbContext: TDbContext,
        balanceUpdates: readonly BalanceUpdate[],
        indexingContext: BalanceUpdateIndexingContext<TContextData>,
    ): Promise<void> {
        for (const balanceUpdate of balanceUpdates) {
            await indexerModule.blockDataIndexer.indexBalanceUpdate(balanceUpdate, dbContext, indexingContext);
        }
    }
}
