import { injectLogger, Logger } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { BalanceUpdateTraverser } from './balance-update-traverser';
import { OperationWithResultTraverser } from './operation-with-result-traverser';
import { SmartRollupOperationTraverser } from './smart-rollup-operation-traverser';
import {
    BalanceUpdateIndexingContext,
    MainOperationIndexingContext,
    OperationWithResultIndexingContext,
} from '../client-indexers/block-data-indexer';
import { IndexerModuleWrapper } from '../client-indexers/wrappers/indexer-module-wrapper';
import {
    isMainOperationWithBalanceUpdatesInMetadata,
    isMainOperationWithResult,
    isSmartRollupOperation,
    MainOperation,
} from '../rpc-data/rpc-data-interfaces';

@singleton()
export class MainOperationTraverser<TDbContext> {
    constructor(
        private readonly smartRollupOperationTraverser: SmartRollupOperationTraverser<TDbContext>,
        private readonly operationWithResultTraverser: OperationWithResultTraverser<TDbContext>,
        private readonly balanceUpdateTraverser: BalanceUpdateTraverser<TDbContext>,
        @injectLogger(MainOperationTraverser) private readonly logger: Logger,
    ) {}

    async traverse<TContextData>(
        indexerModule: IndexerModuleWrapper<TDbContext, TContextData>,
        dbContext: TDbContext,
        mainOperation: MainOperation,
        indexingContext: MainOperationIndexingContext<TContextData>,
    ): Promise<void> {
        await this.logger.runWithData({ indexedMainOperationKind: mainOperation.kind }, async () => {
            await indexerModule.blockDataIndexer.indexMainOperation(mainOperation, dbContext, indexingContext);

            if (isSmartRollupOperation(mainOperation)) {
                await this.smartRollupOperationTraverser.traverse(indexerModule, dbContext, mainOperation, indexingContext);
            }

            if (isMainOperationWithResult(mainOperation)) {
                const operationWithResultContext = Object.freeze<OperationWithResultIndexingContext<TContextData>>({
                    ...indexingContext,
                    mainOperation,
                });

                await this.operationWithResultTraverser.traverse(indexerModule, dbContext, mainOperation, operationWithResultContext);

                for (const internalOperation of mainOperation.internalOperations) {
                    await this.operationWithResultTraverser.traverse(indexerModule, dbContext, internalOperation, operationWithResultContext);
                }
            }

            if (isMainOperationWithBalanceUpdatesInMetadata(mainOperation)) {
                const balanceUpdatesContext = Object.freeze<BalanceUpdateIndexingContext<TContextData>>({
                    ...indexingContext,
                    balanceUpdateSource: 'MainOperationMetadata',
                    mainOperation,
                    internalOperation: null,
                });
                await this.balanceUpdateTraverser.traverse(indexerModule, dbContext, mainOperation.balanceUpdates, balanceUpdatesContext);
            }
        });
    }
}
