import { singleton } from 'tsyringe';

import { BalanceUpdateTraverser } from './balance-update-traverser';
import { ContractTraverser } from './contract-traverser';
import { BalanceUpdateIndexingContext, OperationWithResultIndexingContext } from '../client-indexers/block-data-indexer';
import { IndexerModuleWrapper } from '../client-indexers/wrappers/indexer-module-wrapper';
import {
    isApplied,
    isInternalOperationWithBalanceUpdatesInResult,
    isMainOperationWithBalanceUpdatesInResult,
    OperationKind,
    OperationWithResult,
} from '../rpc-data/rpc-data-interfaces';

@singleton()
export class OperationWithResultTraverser<TDbContext> {
    constructor(
        private readonly contractTraverser: ContractTraverser<TDbContext>,
        private readonly balanceUpdateTraverser: BalanceUpdateTraverser<TDbContext>,
    ) {}

    // eslint-disable-next-line max-lines-per-function
    async traverse<TContextData>(
        indexerModule: IndexerModuleWrapper<TDbContext, TContextData>,
        dbContext: TDbContext,
        operationWithResult: OperationWithResult,
        indexingContext: OperationWithResultIndexingContext<TContextData>,
    ): Promise<void> {
        await indexerModule.blockDataIndexer.indexOperationWithResult(operationWithResult, dbContext, indexingContext);

        if (!isApplied(operationWithResult)) {
            return;
        }

        // Traverse contract.
        switch (operationWithResult.kind) {
            case OperationKind.Origination:
                await this.contractTraverser.traverse(indexerModule, dbContext, {
                    ...indexingContext,
                    contract: operationWithResult.result.originatedContract,
                    bigMapDiffs: operationWithResult.result.bigMapDiffs,
                    transactionParameter: null,
                    storageChange: null, // TODO context.operationWithResult.script.storage
                    operationWithResult,
                });
                break;
            case OperationKind.Transaction:
                if (operationWithResult.destination.type === 'Contract') {
                    await this.contractTraverser.traverse(indexerModule, dbContext, {
                        ...indexingContext,
                        contract: operationWithResult.destination,
                        bigMapDiffs: operationWithResult.result.bigMapDiffs,
                        transactionParameter: operationWithResult.transactionParameter,
                        storageChange: operationWithResult.result.storageChange,
                        operationWithResult,
                    });
                }
                break;
            default:
                break;
        }

        // Traverse balance updates.
        if (!operationWithResult.isInternal && isMainOperationWithBalanceUpdatesInResult(operationWithResult)) {
            const balanceUpdatesContext = Object.freeze<BalanceUpdateIndexingContext<TContextData>>({
                ...indexingContext,
                balanceUpdateSource: 'MainOperationAppliedResult',
                mainOperation: operationWithResult,
                internalOperation: null,
            });
            await this.balanceUpdateTraverser.traverse(indexerModule, dbContext, operationWithResult.result.balanceUpdates, balanceUpdatesContext);
        }
        if (operationWithResult.isInternal && isInternalOperationWithBalanceUpdatesInResult(operationWithResult)) {
            const balanceUpdatesContext = Object.freeze<BalanceUpdateIndexingContext<TContextData>>({
                ...indexingContext,
                balanceUpdateSource: 'InternalOperationAppliedResult',
                internalOperation: operationWithResult,
            });
            await this.balanceUpdateTraverser.traverse(indexerModule, dbContext, operationWithResult.result.balanceUpdates, balanceUpdatesContext);
        }
    }
}
