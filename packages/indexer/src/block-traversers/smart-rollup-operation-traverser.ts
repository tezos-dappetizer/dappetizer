import { singleton } from 'tsyringe';

import { MainOperationIndexingContext } from '../client-indexers/block-data-indexer';
import { SmartRollupIndexingContext } from '../client-indexers/smart-rollup-indexer';
import { IndexerModuleWrapper } from '../client-indexers/wrappers/indexer-module-wrapper';
import { isApplied, OperationKind, SmartRollupOperation } from '../rpc-data/rpc-data-interfaces';
import { UNBOUND_ROLLUP_DATA, UnboundRollupData } from '../smart-rollups/bound-indexer/bound-smart-rollup-indexer';

@singleton()
export class SmartRollupOperationTraverser<TDbContext> {
    async traverse<TContextData>(
        indexerModule: IndexerModuleWrapper<TDbContext, TContextData>,
        dbContext: TDbContext,
        operation: SmartRollupOperation,
        indexingContext: MainOperationIndexingContext<TContextData>,
    ): Promise<void> {
        if (!indexerModule.smartRollupIndexers || !isApplied(operation)) {
            return;
        }

        if (operation.kind === OperationKind.SmartRollupAddMessages) {
            const addMessagesIndexingContext = Object.freeze({ ...indexingContext, mainOperation: operation });
            for (const indexer of indexerModule.smartRollupIndexers.rawIndexers) {
                await indexer.indexAddMessages(operation, dbContext, addMessagesIndexingContext);
            }
            return;
        }

        const rollup = operation.kind === OperationKind.SmartRollupOriginate
            ? operation.result.originatedRollup
            : operation.rollup;
        const indexer = await indexerModule.smartRollupIndexers.resolve(rollup, dbContext);

        switch (operation.kind) {
            case OperationKind.SmartRollupCement:
                return indexer?.indexCement(operation, dbContext, createContext(operation));
            case OperationKind.SmartRollupExecuteOutboxMessage:
                return indexer?.indexExecuteOutboxMessage(operation, dbContext, createContext(operation));
            case OperationKind.SmartRollupOriginate:
                return indexer?.indexOriginate(operation, dbContext, createContext(operation));
            case OperationKind.SmartRollupPublish:
                return indexer?.indexPublish(operation, dbContext, createContext(operation));
            case OperationKind.SmartRollupRecoverBond:
                return indexer?.indexRecoverBond(operation, dbContext, createContext(operation));
            case OperationKind.SmartRollupRefute:
                return indexer?.indexRefute(operation, dbContext, createContext(operation));
            case OperationKind.SmartRollupTimeout:
                return indexer?.indexTimeout(operation, dbContext, createContext(operation));
        }

        function createContext<TOperation extends SmartRollupIndexingContext['mainOperation']>(
            mainOperation: TOperation,
        ): SmartRollupIndexingContext<TContextData, UnboundRollupData> & { readonly mainOperation: TOperation } {
            return { ...indexingContext, mainOperation, rollup, rollupData: UNBOUND_ROLLUP_DATA };
        }
    }
}
