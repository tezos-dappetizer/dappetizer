import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { EntityToIndex } from '../entity-to-index';
import { MainOperation } from '../operations/main-operation';

/** @category RPC Block Data */
export type TaquitoRpcOperationGroup = DeepReadonly<taquitoRpc.OperationEntry>;

/** @category RPC Block Data */
export interface OperationGroup extends EntityToIndex<TaquitoRpcOperationGroup> {
    // Specific properties:
    readonly branch: string;
    readonly chainId: string;
    readonly hash: string;
    readonly protocol: string; // TODO expand.
    readonly signature: string | null;

    // Tree of operations:
    readonly operations: readonly MainOperation[];
}
