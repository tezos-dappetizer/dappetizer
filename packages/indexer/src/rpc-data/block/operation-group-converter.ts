import { injectLogger, keyof, Logger } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { scopedLogKeys } from '../../helpers/scoped-log-keys';
import { MainOperationConverter } from '../operations/main-operation-converter';
import { primitives } from '../primitives';
import { RpcConversionError } from '../rpc-conversion-error';
import { RpcConvertOptions } from '../rpc-convert-options';
import { joinUid, RpcObjectConverter } from '../rpc-converter';
import { OperationGroup, TaquitoRpcOperationGroup } from '../rpc-data-interfaces';

@singleton()
export class OperationGroupConverter extends RpcObjectConverter<TaquitoRpcOperationGroup, OperationGroup, RpcConvertOptions> {
    constructor(
        private readonly operationConverter: MainOperationConverter,
        @injectLogger(OperationGroupConverter) private readonly logger: Logger,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcOperationGroup,
        genericUid: string,
        options: RpcConvertOptions,
    ): OperationGroup {
        const hash = primitives.nonWhiteSpaceString.convertProperty(rpcData, 'hash', genericUid);
        const uid = joinUid(options.blockHash, hash);

        return this.logger.runWithData({ [scopedLogKeys.INDEXED_OPERATION_GROUP_HASH]: hash }, () => {
            const rpcOperations = rpcData.contents.map((rpcOperation, index) => {
                if (!('metadata' in rpcOperation)) {
                    const operationUid = joinUid(uid, keyof<typeof rpcData>('contents'), index, 'metadata');
                    throw new RpcConversionError(operationUid, 'An operation already in the block must have a metadata.');
                }
                return rpcOperation;
            });
            const operations = this.operationConverter.convertArray(rpcOperations, uid, options);

            return Object.freeze<OperationGroup>({
                branch: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'branch', uid),
                chainId: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'chain_id', uid),
                hash,
                operations,
                protocol: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'protocol', uid),
                rpcData,
                signature: primitives.nonWhiteSpaceString.convertNullableProperty(rpcData, 'signature', uid),
                uid,
            });
        });
    }
}
