import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { EntityToIndex } from '../entity-to-index';

/** @category RPC Block Data */
export type TaquitoRpcBlockHeader = DeepReadonly<taquitoRpc.BlockFullHeader>;

/** @category RPC Block Data */
export interface BlockHeader extends EntityToIndex<TaquitoRpcBlockHeader> {
    // Common block header properties:

    /** It is an integer greater than or equal to `0`. */
    readonly level: number;

    readonly predecessorBlockHash: string;

    /** It is an integer greater than or equal to `0`. */
    readonly proto: number;

    readonly timestamp: Date;
}
