import { singleton } from 'tsyringe';

import { primitives } from '../primitives';
import { RpcObjectConverter } from '../rpc-converter';
import { BlockHeader, TaquitoRpcBlockHeader } from '../rpc-data-interfaces';

@singleton()
export class BlockHeaderConverter extends RpcObjectConverter<TaquitoRpcBlockHeader, BlockHeader> {
    protected override convertObject(
        rpcData: TaquitoRpcBlockHeader,
        uid: string,
    ): BlockHeader {
        return Object.freeze<BlockHeader>({
            level: primitives.integer.convertProperty(rpcData, 'level', uid, { min: 0 }),
            predecessorBlockHash: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'predecessor', uid),
            proto: primitives.integer.convertProperty(rpcData, 'proto', uid, { min: 0 }),
            rpcData,
            timestamp: primitives.isoDate.convertProperty(rpcData, 'timestamp', uid),
            uid,
        });
    }
}
