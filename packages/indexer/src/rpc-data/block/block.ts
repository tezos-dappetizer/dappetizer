import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { OperationGroup } from './operation-group';
import { BalanceUpdate } from '../common/balance-update/balance-update';
import { EntityToIndex } from '../entity-to-index';

/** @category RPC Block Data */
export type TaquitoRpcBlock = DeepReadonly<taquitoRpc.BlockResponse>;

/** @category RPC Block Data */
export interface Block extends EntityToIndex<TaquitoRpcBlock> {
    // Common block header properties:

    /** It is an integer greater than or equal to `0`. */
    readonly level: number;

    readonly predecessorBlockHash: string;

    /** It is an integer greater than or equal to `0`. */
    readonly proto: number;

    readonly timestamp: Date;

    // Specific properties:
    readonly chainId: string;
    readonly hash: string;
    readonly protocol: string; // TODO expand.

    /** Balance updates from `metadata` property directly on the block. */
    readonly metadataBalanceUpdates: readonly BalanceUpdate[];

    // Tree of operations.
    readonly operationGroups: readonly OperationGroup[];
}
