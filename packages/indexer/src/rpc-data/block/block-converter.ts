import { keyof } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { BlockHeaderConverter } from './block-header-converter';
import { OperationGroupConverter } from './operation-group-converter';
import { BalanceUpdateConverter } from '../common/balance-update/converters/balance-update-converter';
import { primitives } from '../primitives';
import { RpcConvertOptions } from '../rpc-convert-options';
import { joinUid } from '../rpc-converter';
import { Block, TaquitoRpcBlock } from '../rpc-data-interfaces';

@singleton()
export class BlockConverter {
    constructor(
        private readonly blockHeaderConverter: BlockHeaderConverter,
        private readonly operationGroupConverter: OperationGroupConverter,
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
    ) {}

    convert(rpcData: TaquitoRpcBlock): Block {
        const hash = primitives.nonWhiteSpaceString.convertProperty(rpcData, 'hash', '(unknown block)');
        const uid = hash;

        const blockHeader = this.blockHeaderConverter.convertProperty(rpcData, 'header', uid);
        const metadataBalanceUpdates = this.balanceUpdateConverter.convertFromMetadataProperty(rpcData, uid);

        const lastStorageWithinBlock: RpcConvertOptions['lastStorageWithinBlock'] = new Map();
        const operationGroups = rpcData.operations.flatMap((rpcOperations, index) => this.operationGroupConverter.convertArray(
            rpcOperations,
            joinUid(uid, keyof<typeof rpcData>('operations'), index),
            { blockHash: hash, predecessorBlockHash: blockHeader.predecessorBlockHash, lastStorageWithinBlock },
        ));

        return Object.freeze<Block>({
            ...blockHeader,
            chainId: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'chain_id', uid),
            hash,
            metadataBalanceUpdates,
            operationGroups,
            protocol: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'protocol', uid),
            rpcData,
            uid,
        });
    }
}
