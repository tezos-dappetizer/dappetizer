/**
 * @category RPC Block Data
 * @tezosProtocol Oxford
 */
export type BalanceUpdateStaker = BalanceUpdateSingleStaker | BalanceUpdateSharedStaker;

/**
 * @category RPC Block Data
 * @tezosProtocol Oxford
 */
export type BalanceUpdateFrozenStaker = BalanceUpdateSingleStaker | BalanceUpdateSharedStaker | BalanceUpdateBaker;

/**
 * @category RPC Block Data
 * @tezosProtocol Oxford
 */
export interface BalanceUpdateSingleStaker {
    // Narrowing properties:
    readonly type: 'Single';

    // Specific properties:
    readonly contractAddress: string;
    readonly delegateAddress: string;

    /** Always `null` for this type so that the property is available on {@link BalanceUpdateStaker} and {@link BalanceUpdateFrozenStaker} unions. */
    readonly bakerAddress: null;
}

/**
 * @category RPC Block Data
 * @tezosProtocol Oxford
 */
export interface BalanceUpdateSharedStaker {
    // Narrowing properties:
    readonly type: 'Shared';

    // Specific properties:
    readonly delegateAddress: string;

    /** Always `null` for this type so that the property is available on {@link BalanceUpdateStaker} and {@link BalanceUpdateFrozenStaker} unions. */
    readonly bakerAddress: null;

    /** Always `null` for this type so that the property is available on {@link BalanceUpdateStaker} and {@link BalanceUpdateFrozenStaker} unions. */
    readonly contractAddress: null;
}

/**
 * @category RPC Block Data
 * @tezosProtocol Oxford
 */
export interface BalanceUpdateBaker {
    // Narrowing properties:
    readonly type: 'Baker' | 'BakerEdge';

    // Specific properties:
    readonly bakerAddress: string;

    /** Always `null` for this type so that the property is available on {@link BalanceUpdateStaker} and {@link BalanceUpdateFrozenStaker} unions. */
    readonly contractAddress: null;

    /** Always `null` for this type so that the property is available on {@link BalanceUpdateStaker} and {@link BalanceUpdateFrozenStaker} unions. */
    readonly delegateAddress: null;
}
