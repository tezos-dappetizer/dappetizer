import { BalanceUpdateCategory, BalanceUpdateKind, BalanceUpdateOrigin, BaseBalanceUpdate } from './base-balance-update';

/** @ignore */
export interface BaseBalanceUpdateSimple extends BaseBalanceUpdate {
    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly origin: BalanceUpdateOrigin;

    // Null properties:

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly bondRollupAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly committer: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly contractAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly cycle: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly delegateAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly delegatorContractAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly participation: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly revelation: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly staker: null;
}

/**
 * Corresponds to RPC schema:
 * ```
 * { // Block_fees
 *      "kind": "accumulator",
 *      "category": "block fees",
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * ```
 * *Oxford* protocol adds delayed `origin` for all `category` values:
 * ```
 * || { ...
 *      "origin": "delayed_operation",
 *      "delayed_operation_hash": $Operation_hash }
 * ```
 * @category RPC Block Data
 * @tezosProtocol Ithaca
 */
export interface BalanceUpdateAccumulator extends BaseBalanceUpdateSimple {
    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly kind: BalanceUpdateKind.Accumulator;

    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly category: BalanceUpdateCategory.BlockFees;
}

/**
 * Corresponds to RPC schema:
 * ```
 * { // Burned
 *      "kind": "burned",
 *      "category": "burned",
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * || { // Double_signing_punishments
 *      "kind": "burned",
 *      "category": "punishments",
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * || { // Storage_fees
 *      "kind": "burned",
 *      "category": "storage fees",
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * || { // Smart_rollup_refutation_punishments
 *      "kind": "burned",
 *      "category": "smart_rollup_refutation_punishments",
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * ```
 * *Oxford* protocol adds delayed `origin` for all `category` values:
 * ```
 * || { ...
 *      "origin": "delayed_operation",
 *      "delayed_operation_hash": $Operation_hash }
 * ```
 * @category RPC Block Data
 * @tezosProtocol Ithaca
 */
export interface BalanceUpdateBurned extends BaseBalanceUpdateSimple {
    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly kind: BalanceUpdateKind.Burned;

    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly category:
    | BalanceUpdateCategory.Burned
    | BalanceUpdateCategory.Punishments
    | BalanceUpdateCategory.SmartRollupRefutationPunishments
    | BalanceUpdateCategory.StorageFees;
}

/**
 * Corresponds to RPC schema:
 * ```
 * { // Baking_bonuses
 *      "kind": "minted",
 *      "category": "baking bonuses",
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * || { // Baking_rewards
 *      "kind": "minted",
 *      "category": "baking rewards",
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * || { // Bootstrap
 *      "kind": "minted",
 *      "category": "bootstrap",
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * || { // Initial_commitments
 *      "kind": "minted",
 *      "category": "commitment",
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * || { // Double_signing_evidence_rewards
 *      "kind": "minted",
 *      "category": "double signing evidence rewards",
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * || { // Endorsing_rewards
 *      "kind": "minted",
 *      "category": "endorsing rewards",
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * || { // Invoice
 *      "kind": "minted",
 *      "category": "invoice",
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * || { // Minted
 *      "kind": "minted",
 *      "category": "minted",
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 *  || { // Nonce_revelation_rewards
 *      "kind": "minted",
 *      "category": "nonce revelation rewards",
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * || { // Liquidity_baking_subsidies
 *      "kind": "minted",
 *      "category": "subsidy",
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * ```
 * *Oxford* protocol adds new `category` values:
 * ```
 * { // Attesting_rewards
 *      "kind": "minted",
 *      "category": "attesting rewards",
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" | "simulation" }
 * || { // Smart_rollup_refutation_rewards
 *      "kind": "minted",
 *      "category": "smart_rollup_refutation_rewards",
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" | "simulation" }
 * ```
 * *Oxford* protocol adds delayed `origin` for all `category` values:
 * ```
 * || { ...
 *      "origin": "delayed_operation",
 *      "delayed_operation_hash": $Operation_hash }
 * ```
 * @category RPC Block Data
 * @tezosProtocol Ithaca
 */
export interface BalanceUpdateMinted extends BaseBalanceUpdateSimple {
    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly kind: BalanceUpdateKind.Minted;

    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly category:
    | BalanceUpdateCategory.AttestingRewards
    | BalanceUpdateCategory.BakingBonuses
    | BalanceUpdateCategory.BakingRewards
    | BalanceUpdateCategory.Bootstrap
    | BalanceUpdateCategory.Commitment
    | BalanceUpdateCategory.DoubleSigningEvidenceRewards
    | BalanceUpdateCategory.EndorsingRewards
    | BalanceUpdateCategory.Invoice
    | BalanceUpdateCategory.Minted
    | BalanceUpdateCategory.NonceRevelationRewards
    | BalanceUpdateCategory.SmartRollupRefutationRewards
    | BalanceUpdateCategory.Subsidy;
}
