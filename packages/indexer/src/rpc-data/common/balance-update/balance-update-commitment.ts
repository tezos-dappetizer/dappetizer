import { BalanceUpdateCategory, BalanceUpdateKind, BalanceUpdateOrigin, BaseBalanceUpdate } from './base-balance-update';

/**
 * Corresponds to RPC schema:
 * ```
 * { // Commitments
 *      "kind": "commitment",
 *      "category": "commitment",
 *      "committer": $Blinded public key hash,
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * ```
 * *Oxford* protocol adds delayed `origin` for all `category` values:
 * ```
 * || { ...
 *      "origin": "delayed_operation",
 *      "delayed_operation_hash": $Operation_hash }
 * ```
 * @category RPC Block Data
 * @tezosProtocol Ithaca
 */
export interface BalanceUpdateCommitment extends BaseBalanceUpdate {
    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly origin: BalanceUpdateOrigin;

    // Narrowing properties:

    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly kind: BalanceUpdateKind.Commitment;

    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly category: BalanceUpdateCategory.Commitment;

    // Specific properties:

    readonly committer: string;

    // Null properties:

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly bondRollupAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly contractAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly cycle: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly delegateAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly delegatorContractAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly participation: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly revelation: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly staker: null;
}
