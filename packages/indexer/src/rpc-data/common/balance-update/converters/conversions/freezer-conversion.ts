import * as taquitoRpc from '@taquito/rpc';
import { isNullish, keyof, typed } from '@tezos-dappetizer/utils';
import { StrictExclude } from 'ts-essentials';

import { BaseConversion, createCategoryMapping } from './base-conversion';
import { StakerConversion } from './staker-conversion';
import { primitives } from '../../../../primitives';
import { guardRpcObject, joinUid } from '../../../../rpc-converter';
import {
    BalanceUpdate,
    BalanceUpdateCategory,
    BalanceUpdateFreezerBonds,
    BalanceUpdateFreezerDeposits,
    BalanceUpdateFreezerLegacy,
    BalanceUpdateFreezerUnstakedDeposits,
    BalanceUpdateKind,
    BalanceUpdateOrigin,
} from '../../../../rpc-data-interfaces';
import * as RpcCategories from '../rpc-categories';

export class FreezerConversion extends BaseConversion {
    override convert(): BalanceUpdate {
        const category = this.convertCategory(categoryMapping);

        switch (category) {
            case BalanceUpdateCategory.Bonds: {
                const rpcBondIdUid = joinUid(this.uid, keyof<typeof this.rpcData>('bond_id'));
                const rpcBondId = guardRpcObject(this.rpcData.bond_id as unknown as taquitoRpc.BondId, rpcBondIdUid);

                return typed<BalanceUpdateFreezerBonds>({
                    ...this.getBlueprint(category),
                    ...this.convertOriginProperties(),
                    kind: BalanceUpdateKind.Freezer,
                    bondRollupAddress: primitives.address.convertProperty(rpcBondId, 'smart_rollup', rpcBondIdUid),
                    contractAddress: primitives.address.convertProperty(this.rpcData, 'contract', this.uid),
                });
            }
            case BalanceUpdateCategory.Deposits:
                if (isNullish(this.rpcData.staker)) {
                    return this.convertLegacy(BalanceUpdateCategory.LegacyDeposits);
                }
                return typed<BalanceUpdateFreezerDeposits>({
                    ...this.getBlueprint(category),
                    ...this.convertOriginProperties(),
                    kind: BalanceUpdateKind.Freezer,
                    staker: new StakerConversion(this.rpcData, this.uid).convertFrozenStaker(),
                });
            case BalanceUpdateCategory.UnstakedDeposits:
                return typed<BalanceUpdateFreezerUnstakedDeposits>({
                    ...this.getBlueprint(category),
                    ...this.convertOriginProperties(),
                    kind: BalanceUpdateKind.Freezer,
                    cycle: primitives.integer.convertProperty(this.rpcData, 'cycle', this.uid),
                    staker: new StakerConversion(this.rpcData, this.uid).convertStaker(),
                });
            default:
                return this.convertLegacy(category);
        }
    }

    private convertLegacy(category: BalanceUpdateFreezerLegacy['category']): BalanceUpdateFreezerLegacy {
        return {
            ...this.getBlueprint(category),
            kind: BalanceUpdateKind.Freezer,
            cycle: primitives.integer.convertNullableProperty(this.rpcData, 'cycle', this.uid),
            delegateAddress: this.convertDelegateAddress(),
            delayedOperationHash: null,
            origin: primitives.enum.convertNullableProperty(this.rpcData, 'origin', this.uid, legacyOriginMapping),
        };
    }
}

const categoryMapping = createCategoryMapping<RpcCategories.Freezer, BalanceUpdateKind.Freezer>({
    bonds: BalanceUpdateCategory.Bonds,
    deposits: BalanceUpdateCategory.Deposits,
    fees: BalanceUpdateCategory.LegacyFees,
    legacy_deposits: BalanceUpdateCategory.LegacyDeposits,
    legacy_fees: BalanceUpdateCategory.LegacyFees,
    legacy_rewards: BalanceUpdateCategory.LegacyRewards,
    rewards: BalanceUpdateCategory.LegacyRewards,
    unstaked_deposits: BalanceUpdateCategory.UnstakedDeposits,
});

const legacyOriginMapping = Object.freeze<Record<
StrictExclude<taquitoRpc.MetadataBalanceUpdatesOriginEnum, 'delayed_operation'>,
StrictExclude<BalanceUpdateOrigin, BalanceUpdateOrigin.DelayedOperation>>>({
    block: BalanceUpdateOrigin.Block,
    migration: BalanceUpdateOrigin.Migration,
    simulation: BalanceUpdateOrigin.Simulation,
    subsidy: BalanceUpdateOrigin.Subsidy,
});
