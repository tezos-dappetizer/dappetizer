import { BaseConversion, createCategoryMapping } from './base-conversion';
import { BalanceUpdateCategory, BalanceUpdateKind, BalanceUpdateMinted } from '../../../../rpc-data-interfaces';
import * as RpcCategories from '../rpc-categories';

export class MintedConversion extends BaseConversion {
    override convert(): BalanceUpdateMinted {
        return {
            ...this.getBlueprint(this.convertCategory(categoryMapping)),
            ...this.convertOriginProperties(),
            kind: BalanceUpdateKind.Minted,
        };
    }
}

const categoryMapping = createCategoryMapping<RpcCategories.Minted, BalanceUpdateKind.Minted>({
    'baking bonuses': BalanceUpdateCategory.BakingBonuses,
    'baking rewards': BalanceUpdateCategory.BakingRewards,
    bootstrap: BalanceUpdateCategory.Bootstrap,
    commitment: BalanceUpdateCategory.Commitment,
    'double signing evidence rewards': BalanceUpdateCategory.DoubleSigningEvidenceRewards,
    'endorsing rewards': BalanceUpdateCategory.EndorsingRewards,
    invoice: BalanceUpdateCategory.Invoice,
    minted: BalanceUpdateCategory.Minted,
    'nonce revelation rewards': BalanceUpdateCategory.NonceRevelationRewards,
    smart_rollup_refutation_rewards: BalanceUpdateCategory.SmartRollupRefutationRewards,
    subsidy: BalanceUpdateCategory.Subsidy,
});
