import { isNullish } from '@tezos-dappetizer/utils';

import { BaseConversion } from './base-conversion';
import { primitives } from '../../../../primitives';
import { BalanceUpdateContract, BalanceUpdateKind } from '../../../../rpc-data-interfaces';

export class ContractConversion extends BaseConversion {
    override convert(): BalanceUpdateContract {
        return {
            ...this.getBlueprint(null),
            ...!isNullish(this.rpcData.origin)
                ? this.convertOriginProperties()
                : { origin: null, delayedOperationHash: null },
            kind: BalanceUpdateKind.Contract,
            category: null,
            contractAddress: primitives.address.convertProperty(this.rpcData, 'contract', this.uid),
        };
    }
}
