import * as taquitoRpc from '@taquito/rpc';

import { primitives } from '../../../../primitives';
import { appendUidAttribute } from '../../../../rpc-converter';
import {
    BalanceUpdate,
    BalanceUpdateCategory,
    BalanceUpdateKind,
    BalanceUpdateOrigin,
    TaquitoRpcBalanceUpdate,
} from '../../../../rpc-data-interfaces';

export abstract class BaseConversion {
    constructor(
        protected readonly rpcData: TaquitoRpcBalanceUpdate,
        protected uid: string,
    ) { }

    protected getBlueprint<TCategory extends BalanceUpdateCategory | null>(
        category: TCategory,
    ): Pick<BalanceUpdate, 'change' | 'rpcData' | 'uid'> &
        Record<Exclude<keyof BalanceUpdate, 'category' | 'change' | 'delayedOperationHash' | 'kind' | 'origin' | 'rpcData' | 'uid'>, null> &
        { category: TCategory } {
        return {
            category,
            change: primitives.bigInteger.convertProperty(this.rpcData, 'change', this.uid),
            rpcData: this.rpcData,
            uid: this.uid,

            // Blueprint for specific properties, only few will be overwritten:
            bondRollupAddress: null,
            committer: null,
            contractAddress: null,
            cycle: null,
            delegateAddress: null,
            delegatorContractAddress: null,
            participation: null,
            revelation: null,
            staker: null,
        };
    }

    protected convertCategory<TCategory extends string>(mapping: Readonly<Record<string, TCategory>>): TCategory {
        const category = primitives.enum.convertProperty(this.rpcData, 'category', this.uid, mapping);

        this.uid = appendUidAttribute(this.uid, this.rpcData.category);
        return category;
    }

    protected convertDelegateAddress(): string {
        return primitives.address.convertProperty(this.rpcData, 'delegate', this.uid);
    }

    protected convertOriginProperties(): { origin: BalanceUpdateOrigin; delayedOperationHash: string | null } {
        const origin = primitives.enum.convertProperty(this.rpcData, 'origin', this.uid, originMapping);

        return {
            origin,
            delayedOperationHash: origin === BalanceUpdateOrigin.DelayedOperation
                ? primitives.nonWhiteSpaceString.convertProperty(this.rpcData, 'delayed_operation_hash', this.uid)
                : null,
        };
    }

    abstract convert(): BalanceUpdate;
}

const originMapping = Object.freeze<Record<taquitoRpc.MetadataBalanceUpdatesOriginEnum, BalanceUpdateOrigin>>({
    block: BalanceUpdateOrigin.Block,
    delayed_operation: BalanceUpdateOrigin.DelayedOperation,
    migration: BalanceUpdateOrigin.Migration,
    simulation: BalanceUpdateOrigin.Simulation,
    subsidy: BalanceUpdateOrigin.Subsidy,
});

// eslint-disable-next-line @typescript-eslint/ban-types
type CategoryOf<TKind extends BalanceUpdateKind> = Extract<BalanceUpdate, { kind: TKind }>['category'];

export function createCategoryMapping<TRpcCategory extends string, TKind extends BalanceUpdateKind>(
    mapping: Record<TRpcCategory, CategoryOf<TKind>>,
): Readonly<Record<TRpcCategory, CategoryOf<TKind>>> {
    return Object.freeze(mapping);
}
