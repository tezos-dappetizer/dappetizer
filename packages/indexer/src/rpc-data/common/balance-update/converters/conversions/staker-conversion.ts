import { dumpType, freezeResult, keyof } from '@tezos-dappetizer/utils';
import { StrictExclude } from 'ts-essentials';

import { primitives } from '../../../../primitives';
import { RpcConversionError } from '../../../../rpc-conversion-error';
import { guardRpcObject, joinUid } from '../../../../rpc-converter';
import {
    BalanceUpdateBaker,
    BalanceUpdateFrozenStaker,
    BalanceUpdateSharedStaker,
    BalanceUpdateSingleStaker,
    BalanceUpdateStaker,
    TaquitoRpcBalanceUpdate,
} from '../../../../rpc-data-interfaces';

type RpcStaker = NonNullable<TaquitoRpcBalanceUpdate['staker']>;

export class StakerConversion {
    private readonly rpcData: RpcStaker;
    private readonly uid: string;

    constructor(parentRpcData: TaquitoRpcBalanceUpdate, parentUid: string) {
        this.uid = joinUid(parentUid, keyof<typeof parentRpcData>('staker'));
        this.rpcData = guardRpcObject(parentRpcData.staker, this.uid);
    }

    @freezeResult()
    convertStaker(): BalanceUpdateStaker {
        return this.tryConvertSingleStaker()
            ?? this.tryConvertSharedStaker()
            ?? this.throwUnsupportedError();
    }

    @freezeResult()
    convertFrozenStaker(): BalanceUpdateFrozenStaker {
        return this.tryConvertSingleStaker()
            ?? this.tryConvertSharedStaker()
            ?? this.tryConvertLegacyBaker()
            ?? this.tryConvertOwnBaker()
            ?? this.tryConvertBakerEdge()
            ?? this.throwUnsupportedError();
    }

    private tryConvertSingleStaker(): BalanceUpdateSingleStaker | null {
        return 'contract' in this.rpcData
            ? {
                ...blueprint,
                type: 'Single',
                contractAddress: primitives.address.convertProperty(this.rpcData, 'contract', this.uid),
                delegateAddress: primitives.address.convertProperty(this.rpcData, 'delegate', this.uid),
            }
            : null;
    }

    private tryConvertSharedStaker(): BalanceUpdateSharedStaker | null {
        return 'delegate' in this.rpcData && !('contract' in this.rpcData)
            ? {
                ...blueprint,
                type: 'Shared',
                delegateAddress: primitives.address.convertProperty(this.rpcData, 'delegate', this.uid),
            }
            : null;
    }

    private tryConvertLegacyBaker(): BalanceUpdateBaker | null {
        return 'baker' in this.rpcData
            ? {
                ...blueprint,
                type: 'Baker',
                bakerAddress: primitives.address.convertProperty(this.rpcData, 'baker', this.uid),
            }
            : null;
    }

    private tryConvertOwnBaker(): BalanceUpdateBaker | null {
        return isOwnBaker(this.rpcData)
            ? {
                ...blueprint,
                type: 'Baker',
                bakerAddress: primitives.address.convertProperty(this.rpcData, 'baker_own_stake', this.uid),
            }
            : null;
    }

    private tryConvertBakerEdge(): BalanceUpdateBaker | null {
        return 'baker_edge' in this.rpcData
            ? {
                ...blueprint,
                type: 'BakerEdge',
                bakerAddress: primitives.address.convertProperty(this.rpcData, 'baker_edge', this.uid),
            }
            : null;
    }

    private throwUnsupportedError(): never {
        const json = JSON.stringify(this.rpcData);
        throw new RpcConversionError(this.uid, `It is an unsupported value ${json} of type ${dumpType(this.rpcData)}.`);
    }
}

interface RpcOwnBaker {
    readonly baker_own_stake: string;
}

const isOwnBaker = (o: object): o is RpcOwnBaker => keyof<RpcOwnBaker>('baker_own_stake') in o;

const blueprint = Object.freeze<Record<StrictExclude<keyof BalanceUpdateStaker, 'type'>, null>>({
    bakerAddress: null,
    contractAddress: null,
    delegateAddress: null,
});
