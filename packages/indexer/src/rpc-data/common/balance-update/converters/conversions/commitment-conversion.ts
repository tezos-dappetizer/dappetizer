import { BaseConversion, createCategoryMapping } from './base-conversion';
import { primitives } from '../../../../primitives';
import { BalanceUpdateCategory, BalanceUpdateCommitment, BalanceUpdateKind } from '../../../../rpc-data-interfaces';
import * as RpcCategories from '../rpc-categories';

export class CommitmentConversion extends BaseConversion {
    override convert(): BalanceUpdateCommitment {
        return {
            ...this.getBlueprint(this.convertCategory(categoryMapping)),
            ...this.convertOriginProperties(),
            kind: BalanceUpdateKind.Commitment,
            committer: primitives.string.convertProperty(this.rpcData, 'committer', this.uid),
        };
    }
}

const categoryMapping = createCategoryMapping<RpcCategories.Commitment, BalanceUpdateKind.Commitment>({
    commitment: BalanceUpdateCategory.Commitment,
});
