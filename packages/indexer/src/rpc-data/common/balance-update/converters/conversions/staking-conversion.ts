import { BaseConversion, createCategoryMapping } from './base-conversion';
import { primitives } from '../../../../primitives';
import {
    BalanceUpdateCategory,
    BalanceUpdateKind,
    BalanceUpdateStakingDelegateDenominator,
    BalanceUpdateStakingDelegatorNumerator,
} from '../../../../rpc-data-interfaces';
import * as RpcCategories from '../rpc-categories';

export class StakingConversion extends BaseConversion {
    override convert(): BalanceUpdateStakingDelegateDenominator | BalanceUpdateStakingDelegatorNumerator {
        const category = this.convertCategory(categoryMapping);

        switch (category) {
            case BalanceUpdateCategory.StakingDelegateDenominator:
                return {
                    ...this.getBlueprint(category),
                    ...this.convertOriginProperties(),
                    kind: BalanceUpdateKind.Staking,
                    delegateAddress: this.convertDelegateAddress(),
                };
            case BalanceUpdateCategory.StakingDelegatorNumerator:
                return {
                    ...this.getBlueprint(category),
                    ...this.convertOriginProperties(),
                    kind: BalanceUpdateKind.Staking,
                    delegatorContractAddress: primitives.address.convertProperty(this.rpcData, 'delegator', this.uid),
                };
        }
    }
}

const categoryMapping = createCategoryMapping<RpcCategories.Staking, BalanceUpdateKind.Staking>({
    delegate_denominator: BalanceUpdateCategory.StakingDelegateDenominator,
    delegator_numerator: BalanceUpdateCategory.StakingDelegatorNumerator,
});
