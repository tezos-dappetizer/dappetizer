import { BalanceUpdateCategory, BalanceUpdateKind, BalanceUpdateOrigin, BaseBalanceUpdate } from './base-balance-update';

/** @ignore */
export interface BaseBalanceUpdateStaking extends BaseBalanceUpdate {
    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly kind: BalanceUpdateKind.Staking;

    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly origin: BalanceUpdateOrigin;

    // Null properties:

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly bondRollupAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly committer: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly contractAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly cycle: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly participation: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly revelation: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly staker: null;
}

/**
 * Corresponds to RPC schema:
 * ```
 * { // Staking_delegate_denominator
 *      "kind": "staking",
 *      "category": "delegate_denominator",
 *      "delegate": $Signature.Public_key_hash,
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * ```
 * *Oxford* protocol adds delayed `origin` for all `category` values:
 * ```
 * || { ...
 *      "origin": "delayed_operation",
 *      "delayed_operation_hash": $Operation_hash }
 * ```
 * @category RPC Block Data
 * @tezosProtocol Oxford
 */
export interface BalanceUpdateStakingDelegateDenominator extends BaseBalanceUpdateStaking {
    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly category: BalanceUpdateCategory.StakingDelegateDenominator;

    // Specific properties:
    readonly delegateAddress: string;

    // Null properties:

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly delegatorContractAddress: null;
}

/**
 * Corresponds to RPC schema:
 * ```
 * { // Staking_delegator_numerator
 *      "kind": "staking",
 *      "category": "delegator_numerator",
 *      "delegator": $contract_id,
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * ```
 * *Oxford* protocol adds delayed `origin` for all `category` values:
 * ```
 * || { ...
 *      "origin": "delayed_operation",
 *      "delayed_operation_hash": $Operation_hash }
 * ```
 * @category RPC Block Data
 * @tezosProtocol Oxford
 */
export interface BalanceUpdateStakingDelegatorNumerator extends BaseBalanceUpdateStaking {
    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly category: BalanceUpdateCategory.StakingDelegatorNumerator;

    // Specific properties:
    readonly delegatorContractAddress: string;

    // Null properties:

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly delegateAddress: null;
}
