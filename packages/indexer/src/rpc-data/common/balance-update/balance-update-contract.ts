import { BalanceUpdateKind, BaseBalanceUpdate } from './base-balance-update';

/**
 * Corresponds to RPC schema:
 * ```
 * { "kind": "contract",
 *      "contract": $contract_id,
 *      "change": $int64 }
 * ```
 * Newer protocols add:
 * ```
 * { // Contract
 *      "kind": "contract",
 *      "contract": $contract_id,
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * ```
 * *Oxford* protocol adds delayed `origin` for all `category` values:
 * ```
 * || { ...
 *      "origin": "delayed_operation",
 *      "delayed_operation_hash": $Operation_hash }
 * ```
 * @category RPC Block Data
 */
export interface BalanceUpdateContract extends BaseBalanceUpdate {
    // Narrowing properties:

    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly kind: BalanceUpdateKind.Contract;

    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly category: null;

    // Specific properties:
    readonly contractAddress: string;

    // Null properties:

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly bondRollupAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly committer: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly cycle: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly delegateAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly delegatorContractAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly participation: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly revelation: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly staker: null;
}
