import { BalanceUpdateBurnedLostRewards } from './balance-update-burned-lost-rewards';
import { BalanceUpdateCommitment } from './balance-update-commitment';
import { BalanceUpdateContract } from './balance-update-contract';
import { BalanceUpdateFreezerBonds } from './balance-update-freezer-bonds';
import { BalanceUpdateFreezerDeposits, BalanceUpdateFreezerUnstakedDeposits } from './balance-update-freezer-deposits';
import { BalanceUpdateFreezerLegacy } from './balance-update-freezer-legacy';
import { BalanceUpdateAccumulator, BalanceUpdateBurned, BalanceUpdateMinted } from './balance-update-simple';
import { BalanceUpdateStakingDelegateDenominator, BalanceUpdateStakingDelegatorNumerator } from './balance-update-staking';

/**
 * The update of the balance. It is the discriminated union which can be narrowed by its `kind` or `category`.
 * @category RPC Block Data
 */
export type BalanceUpdate =
    | BalanceUpdateAccumulator
    | BalanceUpdateBurned
    | BalanceUpdateBurnedLostRewards
    | BalanceUpdateCommitment
    | BalanceUpdateContract
    | BalanceUpdateFreezerLegacy
    | BalanceUpdateFreezerBonds
    | BalanceUpdateFreezerDeposits
    | BalanceUpdateFreezerUnstakedDeposits
    | BalanceUpdateMinted
    | BalanceUpdateStakingDelegateDenominator
    | BalanceUpdateStakingDelegatorNumerator;
