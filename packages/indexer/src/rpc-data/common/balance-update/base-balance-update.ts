import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';

import { EntityToIndex } from '../../entity-to-index';

/** @category RPC Block Data */
export type TaquitoRpcBalanceUpdate = DeepReadonly<taquitoRpc.OperationMetadataBalanceUpdates>;

/** @ignore */
export interface BaseBalanceUpdate extends EntityToIndex<TaquitoRpcBalanceUpdate> {
    /** The actual balance change. */
    readonly change: BigNumber;

    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly origin: BalanceUpdateOrigin | null;

    /** It has a value only if {@link origin} is {@link BalanceUpdateOrigin.DelayedOperation} */
    readonly delayedOperationHash: string | null;
}

/** @category RPC Block Data */
export enum BalanceUpdateKind {
    Accumulator = 'Accumulator',
    Burned = 'Burned',
    Commitment = 'Commitment',
    Contract = 'Contract',
    Freezer = 'Freezer',
    Minted = 'Minted',
    /** @tezosProtocol Oxford */ Staking = 'Staking',
}

/**
 * @category RPC Block Data
 * @tezosProtocol Florence
 */
export enum BalanceUpdateOrigin {
    /** @tezosProtocol Florence */ Block = 'Block',
    /** @tezosProtocol Oxford */ DelayedOperation = 'DelayedOperation',
    /** @tezosProtocol Florence */ Migration = 'Migration',
    /** @tezosProtocol Ithaca */ Simulation = 'Simulation',
    /** @tezosProtocol Granada */ Subsidy = 'Subsidy',
}

/** @category RPC Block Data */
export enum BalanceUpdateCategory {
    /** @tezosProtocol Oxford */ AttestingRewards = 'AttestingRewards',
    BakingBonuses = 'BakingBonuses',
    BakingRewards = 'BakingRewards',
    BlockFees = 'BlockFees',
    Bonds = 'Bonds',
    Bootstrap = 'Bootstrap',
    Burned = 'Burned',
    Commitment = 'Commitment',
    Deposits = 'Deposits',
    DoubleSigningEvidenceRewards = 'DoubleSigningEvidenceRewards',
    EndorsingRewards = 'EndorsingRewards',
    Invoice = 'Invoice',
    LegacyDeposits = 'LegacyDeposits',
    LegacyFees = 'LegacyFees',
    LegacyRewards = 'LegacyRewards',
    LostEndorsingRewards = 'LostEndorsingRewards',
    /** @tezosProtocol Oxford */ LostAttestingRewards = 'LostAttestingRewards',
    Minted = 'Minted',
    NonceRevelationRewards = 'NonceRevelationRewards',
    Punishments = 'Punishments',
    /** @tezosProtocol Oxford */ SmartRollupRefutationPunishments = 'SmartRollupRefutationPunishments',
    /** @tezosProtocol Oxford */ SmartRollupRefutationRewards = 'SmartRollupRefutationRewards',
    /** @tezosProtocol Oxford */ StakingDelegateDenominator = 'StakingDelegateDenominator',
    /** @tezosProtocol Oxford */ StakingDelegatorNumerator = 'StakingDelegatorNumerator',
    StorageFees = 'StorageFees',
    Subsidy = 'Subsidy',
    /** @tezosProtocol Oxford */ UnstakedDeposits = 'UnstakedDeposits',
}
