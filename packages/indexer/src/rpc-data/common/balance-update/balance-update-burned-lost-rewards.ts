import { BalanceUpdateCategory, BalanceUpdateKind, BalanceUpdateOrigin, BaseBalanceUpdate } from './base-balance-update';

/**
 * Corresponds to RPC schema:
 * ```
 * { // Lost_endorsing_rewards
 *      "kind": "burned",
 *      "category": "lost endorsing rewards",
 *      "delegate": $Signature.Public_key_hash,
 *      "participation": boolean,
 *      "revelation": boolean,
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * ```
 * *Oxford* protocol adds new `category` values:
 * ```
 * { // Lost_attesting_rewards
 *      "kind": "burned",
 *      "category": "lost attesting rewards",
 *      "delegate": $Signature.Public_key_hash,
 *      "participation": boolean,
 *      "revelation": boolean,
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * ```
  * *Oxford* protocol adds delayed `origin` for all `category` values:
 * ```
 * || { ...
 *      "origin": "delayed_operation",
 *      "delayed_operation_hash": $Operation_hash }
 * ```
 * @category RPC Block Data
 * @tezosProtocol Ithaca
 */
export interface BalanceUpdateBurnedLostRewards extends BaseBalanceUpdate {
    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly kind: BalanceUpdateKind.Burned;

    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly category: BalanceUpdateCategory.LostEndorsingRewards | BalanceUpdateCategory.LostAttestingRewards;

    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly origin: BalanceUpdateOrigin;

    // Specific properties:
    readonly delegateAddress: string;

    readonly participation: boolean;

    readonly revelation: boolean;

    // Null properties:

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly bondRollupAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly committer: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly contractAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly cycle: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly delegatorContractAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly staker: null;
}
