import { StrictExclude } from 'ts-essentials';

import { BalanceUpdateCategory, BalanceUpdateKind, BalanceUpdateOrigin, BaseBalanceUpdate } from './base-balance-update';

/**
 * Corresponds to RPC schema:
 * ```
 * { // Deposits
 *      "kind": "freezer",
 *      "category": "deposits",
 *      "delegate": $Signature.Public_key_hash,
 *      "cycle": integer ∈ [-2^31-1, 2^31],
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" }
 * || { // Rewards
 *      "kind": "freezer",
 *      "category": "rewards",
 *      "delegate": $Signature.Public_key_hash,
 *      "cycle": integer ∈ [-2^31-1, 2^31],
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" }
 * || { // Fees
 *      "kind": "freezer",
 *      "category": "fees",
 *      "delegate": $Signature.Public_key_hash,
 *      "cycle": integer ∈ [-2^31-1, 2^31],
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" }
 * ```
 * *Ithaca* protocol introduces changes:
 * ```
 * { // Deposits
 *      "kind": "freezer",
 *      "category": "deposits",
 *      "delegate": $Signature.Public_key_hash,
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * || { // Legacy_rewards
 *      "kind": "freezer",
 *      "category": "legacy_rewards",
 *      "delegate": $Signature.Public_key_hash,
 *      "cycle": integer ∈ [-2^31-1, 2^31],
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * || { // Legacy_deposits
 *      "kind": "freezer",
 *      "category": "legacy_deposits",
 *      "delegate": $Signature.Public_key_hash,
 *      "cycle": integer ∈ [-2^31-1, 2^31],
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * || { // Legacy_fees
 *      "kind": "freezer",
 *      "category": "legacy_fees",
 *      "delegate": $Signature.Public_key_hash,
 *      "cycle": integer ∈ [-2^31-1, 2^31],
 *      "change": $int64,
 *      "origin": "block" || "migration" || "subsidy" || "simulation" }
 * ```
 * @category RPC Block Data
 */
export interface BalanceUpdateFreezerLegacy extends BaseBalanceUpdate {
    // Narrowing properties:

    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly kind: BalanceUpdateKind.Freezer;

    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly category: BalanceUpdateCategory.LegacyDeposits | BalanceUpdateCategory.LegacyFees | BalanceUpdateCategory.LegacyRewards;

    /** Usable for narrowing the type of this object from {@link BalanceUpdate}. */
    readonly origin: StrictExclude<BalanceUpdateOrigin, BalanceUpdateOrigin.DelayedOperation> | null;

    // Specific properties:
    readonly cycle: number | null;
    readonly delegateAddress: string;

    // Null properties:

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly bondRollupAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly committer: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly contractAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly delayedOperationHash: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly delegatorContractAddress: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly participation: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly revelation: null;

    /** Always `null` for this type so that the property is directly available on {@link BalanceUpdate} union. */
    readonly staker: null;
}
