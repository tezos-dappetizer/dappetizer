import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';

import { TicketToken } from './ticket-token';
import { EntityToIndex } from '../../entity-to-index';

/** @category RPC Block Data */
export type TaquitoRpcTicketUpdate = DeepReadonly<taquitoRpc.TicketUpdates['updates'][number]>;

/** @category RPC Block Data */
export interface TicketUpdate extends EntityToIndex<TaquitoRpcTicketUpdate> {
    readonly accountAddress: string;

    /** It is an integer representing the change of ticket amount. So it can be negative. */
    readonly amount: BigNumber;

    readonly token: TicketToken;
}
