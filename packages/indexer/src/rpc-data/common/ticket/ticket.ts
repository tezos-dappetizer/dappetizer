import { BigNumber } from 'bignumber.js';

import { LazyMichelsonValue } from '../michelson/lazy-michelson-value';

/** @deprecated Use `TicketToken` instead.  */
export interface Ticket {
    /** It is an integer greater than or equal to `1`. */
    readonly amount: BigNumber;

    readonly contents: LazyMichelsonValue;

    /** Contract `KT1` address. */
    readonly ticketerAddress: string;
}
