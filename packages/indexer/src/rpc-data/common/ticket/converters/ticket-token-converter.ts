import { StringKeyOfType } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { TicketTokenImpl } from './ticket-token-impl';
import { primitives } from '../../../primitives';
import { guardRpcObject } from '../../../rpc-converter';
import { Michelson, TicketToken } from '../../../rpc-data-interfaces';
import { LazyMichelsonValueFactory } from '../../michelson/lazy-michelson-value-factory';

export interface RpcTicketTokenProperties<TRpcData extends object> {
    ticketer: StringKeyOfType<TRpcData, string>;
    content: StringKeyOfType<TRpcData, Michelson>;
    contentType: StringKeyOfType<TRpcData, Michelson>;
}

@singleton()
export class TicketTokenConverter {
    constructor(private readonly lazyValueFactory: LazyMichelsonValueFactory) {}

    convertFromProperties<TRpcData extends object>(
        rpcData: TRpcData,
        uid: string,
        properties: RpcTicketTokenProperties<TRpcData>,
    ): TicketToken {
        guardRpcObject(rpcData, uid);

        const ticketerAddress = primitives.address.convertProperty(rpcData, properties.ticketer, uid, ['KT1']);
        const contentMichelson = primitives.michelson.convertProperty(rpcData, properties.content, uid);
        const contentSchema = primitives.michelsonSchema.convertProperty(rpcData, properties.contentType, uid);
        const content = this.lazyValueFactory.create(contentMichelson, contentSchema);

        return new TicketTokenImpl(ticketerAddress, content);
    }
}
