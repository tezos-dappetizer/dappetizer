import { singleton } from 'tsyringe';

import { primitives } from '../../../primitives';
import { RpcObjectConverter } from '../../../rpc-converter';
import { TaquitoRpcTicketUpdate, TicketUpdate } from '../../../rpc-data-interfaces';
import { TicketToken } from '../ticket-token';

@singleton()
export class TicketUpdateConverter extends RpcObjectConverter<TaquitoRpcTicketUpdate, TicketUpdate, TicketToken> {
    protected override convertObject(
        rpcData: TaquitoRpcTicketUpdate,
        uid: string,
        token: TicketToken,
    ): TicketUpdate {
        return Object.freeze<TicketUpdate>({
            accountAddress: primitives.address.convertProperty(rpcData, 'account', uid),
            amount: primitives.bigInteger.convertProperty(rpcData, 'amount', uid),
            rpcData,
            token,
            uid,
        });
    }
}
