import { Nullish } from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

import { CONTRACT_PROVIDER_DI_TOKEN, ContractProvider } from '../../../contracts/contract-provider';
import { SelectiveIndexingConfigResolver } from '../../../helpers/selective-indexing-config-resolver';
import { primitives } from '../../primitives';
import { RpcConverter } from '../../rpc-converter';
import { LazyContract } from '../../rpc-data-interfaces';

@singleton()
export class LazyContractConverter extends RpcConverter<string, LazyContract> {
    constructor(
        @inject(CONTRACT_PROVIDER_DI_TOKEN) private readonly contractProvider: ContractProvider,
        private readonly configResolver: SelectiveIndexingConfigResolver,
    ) {
        super();
    }

    protected override convertValue(
        rpcData: Nullish<string>,
        uid: string,
    ): LazyContract {
        const address = primitives.address.convert(rpcData, uid, ['KT1']);
        const config = this.configResolver.resolve(address, 'contracts');

        return Object.freeze<LazyContract>({
            type: 'Contract',
            address,
            config,

            getContract: async () => {
                return this.contractProvider.getContract(address);
            },
        });
    }
}
