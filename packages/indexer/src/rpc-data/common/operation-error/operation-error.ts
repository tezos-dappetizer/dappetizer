import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { EntityToIndex } from '../../entity-to-index';

/** @category RPC Block Data */
export type TaquitoRpcOperationError = DeepReadonly<taquitoRpc.TezosGenericOperationError>;

/** @category RPC Block Data */
export interface OperationError extends EntityToIndex<TaquitoRpcOperationError> {
    readonly kind: string; // TODO enum?
    readonly id: string; // TODO enum?
}
