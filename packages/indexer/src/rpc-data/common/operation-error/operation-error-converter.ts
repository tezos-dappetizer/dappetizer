import { singleton } from 'tsyringe';

import { primitives } from '../../primitives';
import { RpcObjectConverter } from '../../rpc-converter';
import { OperationError, TaquitoRpcOperationError } from '../../rpc-data-interfaces';

@singleton()
export class OperationErrorConverter extends RpcObjectConverter<TaquitoRpcOperationError, OperationError> {
    protected override convertObject(
        rpcData: TaquitoRpcOperationError,
        uid: string,
    ): OperationError {
        return Object.freeze<OperationError>({
            id: primitives.string.convertProperty(rpcData, 'id', uid),
            kind: primitives.string.convertProperty(rpcData, 'kind', uid),
            rpcData,
            uid,
        });
    }
}
