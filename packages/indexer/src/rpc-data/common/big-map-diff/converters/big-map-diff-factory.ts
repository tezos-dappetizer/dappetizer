import * as taquitoRpc from '@taquito/rpc';
import { cacheFuncResult } from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';
import { singleton } from 'tsyringe';

import { BigMapInfo } from '../../../../contracts/data/big-map-info/big-map-info';
import { BigMapInfoResolver } from '../../../../contracts/data/big-map-info/big-map-info-resolver';
import { MichelsonSchema } from '../../../../michelson/michelson-schema';
import {
    BigMapDiff,
    BigMapDiffAction,
    LazyBigMapDiff,
    LazyContract,
    Michelson,
    TaquitoRpcBigMapDiff,
} from '../../../rpc-data-interfaces';
import { LazyMichelsonValueFactory } from '../../michelson/lazy-michelson-value-factory';

export interface DiffCreateOptions {
    readonly blockHash: string;
    readonly contract: LazyContract;
    readonly rpcData: TaquitoRpcBigMapDiff;
    readonly uid: string;
}

export interface AllocCreateOptions extends DiffCreateOptions {
    readonly bigMapId: BigNumber;
    readonly keyType: MichelsonSchema;
    readonly valueType: MichelsonSchema;
}

export interface CopyCreateOptions extends DiffCreateOptions {
    readonly sourceBigMapId: BigNumber;
    readonly destinationBigMapId: BigNumber;
}

export interface RemoveCreateOptions extends DiffCreateOptions {
    readonly bigMapId: BigNumber;
}

export interface UpdateCreateOptions extends DiffCreateOptions {
    readonly bigMapId: BigNumber;
    readonly keyHash: string;
    readonly keyMichelson: Michelson;
    readonly valueMichelson: Michelson | null;
}

/** Creates respective big map diffs from already valid data. So conversion from RPC should happen before. */
@singleton()
export class BigMapDiffFactory {
    constructor(
        private readonly bigMapResolver: BigMapInfoResolver,
        private readonly lazyValueFactory: LazyMichelsonValueFactory,
    ) {}

    createAlloc(options: AllocCreateOptions): LazyBigMapDiff {
        return this.createLazyDiff(BigMapDiffAction.Alloc, options, async blueprint => {
            const bigMap = await this.resolveBigMap(options.bigMapId, options);

            return { ...blueprint, bigMap, keyType: options.keyType, valueType: options.valueType };
        });
    }

    createCopy(options: CopyCreateOptions): LazyBigMapDiff {
        return this.createLazyDiff(BigMapDiffAction.Copy, options, async blueprint => {
            const sourceBigMap = await this.resolveBigMap(options.sourceBigMapId, options);
            const destinationBigMap = await this.resolveBigMap(options.destinationBigMapId, options);

            return { ...blueprint, sourceBigMap, destinationBigMap };
        });
    }

    createRemove(options: RemoveCreateOptions): LazyBigMapDiff {
        return this.createLazyDiff(BigMapDiffAction.Remove, options, async blueprint => {
            const bigMap = await this.resolveBigMap(options.bigMapId, options);

            return { ...blueprint, bigMap };
        });
    }

    createUpdate(options: UpdateCreateOptions): LazyBigMapDiff {
        return this.createLazyDiff(BigMapDiffAction.Update, options, async blueprint => {
            const bigMap = await this.resolveBigMap(options.bigMapId, options);
            const key = bigMap ? this.lazyValueFactory.create(options.keyMichelson, bigMap.keySchema) : null;
            const value = options.valueMichelson && bigMap ? this.lazyValueFactory.create(options.valueMichelson, bigMap.valueSchema) : null;

            return { ...blueprint, bigMap, key, value, keyHash: options.keyHash };
        });
    }

    private createLazyDiff<TAction extends BigMapDiffAction>(
        action: TAction,
        options: DiffCreateOptions,
        getBigMapDiff: (blueprint: BlueprintDiff<TAction>) => Promise<BigMapDiffFor<TAction>>,
    ): LazyBigMapDiff {
        const baseProperties = {
            action,
            rpcData: options.rpcData,
            uid: options.uid,
        };
        return Object.freeze<LazyBigMapDiff>({
            ...baseProperties,

            getBigMapDiff: cacheFuncResult(async () => {
                const diff = await getBigMapDiff({
                    ...baseProperties,
                    bigMap: null,
                    destinationBigMap: null,
                    key: null,
                    keyHash: null,
                    keyType: null,
                    sourceBigMap: null,
                    value: null,
                    valueType: null,
                });
                return Object.freeze(diff);
            }),
        });
    }

    private async resolveBigMap(id: BigNumber, options: DiffCreateOptions): Promise<BigMapInfo | null> {
        return this.bigMapResolver.resolve(id, options.contract, options.blockHash);
    }
}

// eslint-disable-next-line @typescript-eslint/ban-types
type BigMapDiffFor<TAction extends BigMapDiffAction> = Extract<BigMapDiff, { action: TAction }>;

type BlueprintDiff<TAction extends BigMapDiffAction> =
    Record<Exclude<keyof BigMapDiff, 'action' | 'rpcData' | 'uid'>, null>
    & { action: TAction; rpcData: TaquitoRpcBigMapDiff; uid: string };

export const actionMapping = Object.freeze<Record<taquitoRpc.DiffActionEnum, BigMapDiffAction>>({
    alloc: BigMapDiffAction.Alloc,
    copy: BigMapDiffAction.Copy,
    remove: BigMapDiffAction.Remove,
    update: BigMapDiffAction.Update,
});
