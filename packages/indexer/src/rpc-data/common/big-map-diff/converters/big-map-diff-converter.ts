import { isNullish } from '@tezos-dappetizer/utils';
import { DeepReadonly } from 'ts-essentials';
import { singleton } from 'tsyringe';

import { LazyStorageDiffBigMapConverter, TaquitoRpcLazyStorageDiff } from './lazy-storage-diff-big-map-converter';
import { LegacyBigMapDiffConverter } from './legacy-big-map-diff-converter';
import { RpcConvertOptionsWithLazyContract } from '../../../rpc-convert-options';
import { LazyBigMapDiff, TaquitoRpcLegacyBigMapDiff } from '../../../rpc-data-interfaces';

export type RpcDataWithBigMapDiffs = DeepReadonly<{
    big_map_diff?: TaquitoRpcLegacyBigMapDiff[];
    lazy_storage_diff?: TaquitoRpcLazyStorageDiff[];
}>;

/** Decides which big map diffs to convert if they exist according to protocol. */
@singleton()
export class BigMapDiffConverter {
    constructor(
        private readonly lazyStorageConverter: LazyStorageDiffBigMapConverter,
        private readonly legacyConverter: LegacyBigMapDiffConverter,
    ) {}

    convertArrayFrom(
        rpcData: RpcDataWithBigMapDiffs,
        uid: string,
        options: RpcConvertOptionsWithLazyContract,
    ): readonly LazyBigMapDiff[] {
        if (!isNullish(rpcData.lazy_storage_diff)) {
            const diffs = this.lazyStorageConverter.convertArrayProperty(rpcData, 'lazy_storage_diff', uid, options);
            return Object.freeze(diffs.flat());
        }

        return this.legacyConverter.convertArrayProperty(rpcData, 'big_map_diff', uid, options);
    }
}
