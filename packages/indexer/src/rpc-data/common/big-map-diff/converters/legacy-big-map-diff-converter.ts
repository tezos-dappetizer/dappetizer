import { singleton } from 'tsyringe';

import { actionMapping, BigMapDiffFactory } from './big-map-diff-factory';
import { primitives } from '../../../primitives';
import {
    getRequiredLazyContract,
    RpcConvertOptionsWithLazyContract,
} from '../../../rpc-convert-options';
import { appendUidAttribute, RpcObjectConverter } from '../../../rpc-converter';
import { BigMapDiffAction, LazyBigMapDiff, TaquitoRpcLegacyBigMapDiff } from '../../../rpc-data-interfaces';

/** Converts legacy (up to Ithaca protocol including) big map diffs. */
@singleton()
export class LegacyBigMapDiffConverter extends RpcObjectConverter<TaquitoRpcLegacyBigMapDiff, LazyBigMapDiff, RpcConvertOptionsWithLazyContract> {
    constructor(
        private readonly diffFactory: BigMapDiffFactory,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcLegacyBigMapDiff,
        uidPrefix: string,
        options: RpcConvertOptionsWithLazyContract,
    ): LazyBigMapDiff {
        const contract = getRequiredLazyContract(options, { uidPrefix, rpcData });
        const action = primitives.enum.convertProperty(rpcData, 'action', uidPrefix, actionMapping);
        const uid = appendUidAttribute(uidPrefix, rpcData.action);
        const diffOptions = { rpcData, uid, blockHash: options.blockHash, contract };

        switch (action) {
            case BigMapDiffAction.Alloc:
                return this.diffFactory.createAlloc({
                    ...diffOptions,
                    bigMapId: primitives.bigInteger.convertProperty(rpcData, 'big_map', uid),
                    keyType: primitives.michelsonSchema.convertProperty(rpcData, 'key_type', uid),
                    valueType: primitives.michelsonSchema.convertProperty(rpcData, 'value_type', uid),
                });
            case BigMapDiffAction.Copy:
                return this.diffFactory.createCopy({
                    ...diffOptions,
                    sourceBigMapId: primitives.bigInteger.convertProperty(rpcData, 'source_big_map', uid),
                    destinationBigMapId: primitives.bigInteger.convertProperty(rpcData, 'destination_big_map', uid),
                });
            case BigMapDiffAction.Remove:
                return this.diffFactory.createRemove({
                    ...diffOptions,
                    bigMapId: primitives.bigInteger.convertProperty(rpcData, 'big_map', uid),
                });
            case BigMapDiffAction.Update:
                return this.diffFactory.createUpdate({
                    ...diffOptions,
                    bigMapId: primitives.bigInteger.convertProperty(rpcData, 'big_map', uid),
                    keyHash: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'key_hash', uid),
                    keyMichelson: primitives.michelson.convertProperty(rpcData, 'key', uid),
                    valueMichelson: primitives.michelson.convertNullableProperty(rpcData, 'value', uid),
                });
        }
    }
}
