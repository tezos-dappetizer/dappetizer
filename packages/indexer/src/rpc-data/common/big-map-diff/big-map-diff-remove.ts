import { BaseBigMapDiff, BigMapDiffAction } from './base-big-map-diff';
import { BigMapInfo } from '../../../contracts/data/big-map-info/big-map-info';

/**
 * Corresponds to RPC schema:
 * ```
 * { // big_map
 *      "kind": "big_map",
 *      "id": $008-PtEdo2Zk.big_map_id,
 *      "diff": { // remove
 *          "action": "remove" } }
 * ```
 *
 * and until *Delphi* protocol (including) to RPC schema:
 * ```
 * { // remove
 *      "action": "remove",
 *      "big_map": $bignum }
 * ```
 * @category RPC Block Data
 */
export interface BigMapRemove extends BaseBigMapDiff {
    // Narrowing properties:

    /** Usable for narrowing the type of this object from {@link BigMapDiff}. */
    readonly action: BigMapDiffAction.Remove;

    // Specific properties:

    /** It is `null` if the corresponding ID is negative - the big map existed only within the block as temporary. */
    readonly bigMap: BigMapInfo | null;

    // Null properties:

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly destinationBigMap: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly key: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly keyHash: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly keyType: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly sourceBigMap: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly value: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly valueType: null;
}
