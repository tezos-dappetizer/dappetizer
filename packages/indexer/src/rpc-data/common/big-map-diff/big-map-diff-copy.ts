import { BaseBigMapDiff, BigMapDiffAction } from './base-big-map-diff';
import { BigMapInfo } from '../../../contracts/data/big-map-info/big-map-info';

/**
 * Corresponds to RPC schema (`updates` correspond to {@link BigMapUpdate}-s):
 * ```
 *  { // big_map
 *      "kind": "big_map",
 *      "id": $008-PtEdo2Zk.big_map_id,
 *      "diff": { // copy
 *          "action": "copy",
 *          "source": $008-PtEdo2Zk.big_map_id,
 *          "updates":
 *              [ { "key_hash": $script_expr,
 *                  "key": $micheline.008-PtEdo2Zk.michelson_v1.expression,
 *                  "value"?: $micheline.008-PtEdo2Zk.michelson_v1.expression } ... ] } }
 * ```
 *
 * and until *Delphi* protocol (including) to RPC schema:
 * ```
 * { // copy
 *      "action": "copy",
 *      "source_big_map": $bignum,
 *      "destination_big_map": $bignum }
 * ```
 * @category RPC Block Data
 */
export interface BigMapCopy extends BaseBigMapDiff {
    // Narrowing properties:

    /** Usable for narrowing the type of this object from {@link BigMapDiff}. */
    readonly action: BigMapDiffAction.Copy;

    // Specific properties:

    /** It is `null` if the corresponding ID is negative - the big map existed only within the block as temporary. */
    readonly sourceBigMap: BigMapInfo | null;

    /** It is `null` if the corresponding ID is negative - the big map existed only within the block as temporary. */
    readonly destinationBigMap: BigMapInfo | null;

    // Null properties:

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly bigMap: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly key: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly keyHash: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly keyType: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly value: null;

    /** Always `null` for this type so that the property is directly available on {@link BigMapDiff} union. */
    readonly valueType: null;
}
