import * as taquitoRpc from '@taquito/rpc';
import { TAQUITO_RPC_CLIENT_DI_TOKEN } from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

import { LazyContract, Michelson, StorageChange } from '../../rpc-data-interfaces';
import { LazyMichelsonValueFactory } from '../michelson/lazy-michelson-value-factory';

export interface StorageChangeCreateOptions {
    readonly newMichelson: Michelson;
    readonly lastMichelsonWithinBlock: Michelson | null;
    readonly uid: string;
    readonly lazyContract: LazyContract;
    readonly predecessorBlockHash: string;
}

@singleton()
export class StorageChangeFactory {
    constructor(
        private readonly lazyValueFactory: LazyMichelsonValueFactory,
        @inject(TAQUITO_RPC_CLIENT_DI_TOKEN) private readonly rpcClient: taquitoRpc.RpcClient,
    ) {}

    async create(options: StorageChangeCreateOptions): Promise<StorageChange> {
        const [contract, oldMichelson] = await Promise.all([
            options.lazyContract.getContract(),
            options.lastMichelsonWithinBlock
                ?? this.rpcClient.getStorage(options.lazyContract.address, { block: options.predecessorBlockHash }),
        ]);

        const newValue = this.lazyValueFactory.create(options.newMichelson, contract.storageSchema);
        const oldValue = this.lazyValueFactory.create(oldMichelson, contract.storageSchema);

        return Object.freeze<StorageChange>({
            newValue,
            oldValue,
            rpcData: options.newMichelson,
            uid: options.uid,
        });
    }
}
