import { EntityToIndex } from '../../entity-to-index';
import { LazyMichelsonValue } from '../michelson/lazy-michelson-value';
import { Michelson } from '../michelson/michelson';

/** @category RPC Block Data */
export interface LazyStorageChange extends EntityToIndex<Michelson> {
    getStorageChange(): Promise<StorageChange>;
}

/** @category RPC Block Data */
export interface StorageChange extends EntityToIndex<Michelson> {
    readonly newValue: LazyMichelsonValue;
    readonly oldValue: LazyMichelsonValue;
}
