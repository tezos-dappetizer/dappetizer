import { cacheFuncResult } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { StorageChangeFactory } from './storage-change-factory';
import { primitives } from '../../primitives';
import {
    getRequiredLazyContract,
    RpcConvertOptionsWithLazyContract,
} from '../../rpc-convert-options';
import { RpcObjectConverter } from '../../rpc-converter';
import { LazyStorageChange, Michelson } from '../../rpc-data-interfaces';

@singleton()
export class LazyStorageChangeConverter extends RpcObjectConverter<Michelson, LazyStorageChange, RpcConvertOptionsWithLazyContract> {
    constructor(private readonly storageChangeFactory: StorageChangeFactory) {
        super();
    }

    protected override convertObject(
        rpcData: Michelson,
        uid: string,
        options: RpcConvertOptionsWithLazyContract,
    ): LazyStorageChange {
        const lazyContract = getRequiredLazyContract(options, { uid, rpcData });
        const newMichelson = primitives.michelson.convert(rpcData, uid);

        const lastMichelsonWithinBlock = options.lastStorageWithinBlock.get(lazyContract.address) ?? null;
        options.lastStorageWithinBlock.set(lazyContract.address, newMichelson);

        return Object.freeze<LazyStorageChange>({
            rpcData: newMichelson,
            uid,

            getStorageChange: cacheFuncResult(async () => {
                return this.storageChangeFactory.create({ ...options, newMichelson, lastMichelsonWithinBlock, lazyContract, uid });
            }),
        });
    }
}
