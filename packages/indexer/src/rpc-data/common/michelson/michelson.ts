export type Michelson = MichelsonPrim | MichelsonRawValue | readonly Michelson[];

/** @category RPC Block Data */
export interface MichelsonPrim {
    readonly prim: string;
    readonly args?: readonly Michelson[];
    readonly annots?: readonly string[];
}

/** @category RPC Block Data */
export interface MichelsonRawValue {
    readonly int?: string;
    readonly string?: string;
    readonly bytes?: string;
}
