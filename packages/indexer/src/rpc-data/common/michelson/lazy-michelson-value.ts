import { Michelson } from './michelson';
import { MichelsonSchema } from '../../../michelson/michelson-schema';

/** @category RPC Block Data */
export interface LazyMichelsonValue {
    readonly michelson: Michelson;
    readonly schema: MichelsonSchema;

    /** Deserializes JavaScript value from the Michelson. */
    convert<T>(): T;
}
