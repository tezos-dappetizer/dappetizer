import { dumpToString, isNullish, isWhiteSpace, NonNullish, Nullish } from '@tezos-dappetizer/utils';

import { RpcConversionError } from '../rpc-conversion-error';
import { joinUid } from '../rpc-converter';
import { RpcSkippableConversionError } from '../rpc-skippable-array-converter';

export type EnumMapping<TTarget> = Readonly<Record<string, TTarget>>;

export class EnumConverter {
    convertNullableProperty<TTarget extends NonNullish, TProperty extends string>(
        rpcParentData: { [P in TProperty]?: Nullish<string> },
        propertyName: TProperty,
        parentUid: string,
        mapping: EnumMapping<TTarget>,
    ): TTarget | null {
        return !isNullish(rpcParentData[propertyName])
            ? this.convertProperty(rpcParentData, propertyName, parentUid, mapping)
            : null;
    }

    convertProperty<TTarget extends NonNullish, TProperty extends string>(
        rpcParentData: { [P in TProperty]?: Nullish<string> },
        propertyName: TProperty,
        parentUid: string,
        mapping: EnumMapping<TTarget>,
    ): TTarget {
        const rpcData = rpcParentData[propertyName];
        const isWellFormed = typeof rpcData === 'string' && !isWhiteSpace(rpcData);
        const mapped = isWellFormed && rpcData in mapping ? mapping[rpcData] : null;

        if (isNullish(mapped)) {
            const uid = joinUid(parentUid, propertyName);
            const dataDescription = `It must be one of supported values: ${dumpToString(Object.keys(mapping).sort())}`
                + ` but the JSON from RPC is: ${JSON.stringify(rpcData)}.`;

            throw isWellFormed
                ? new RpcSkippableConversionError(uid, dataDescription)
                : new RpcConversionError(uid, dataDescription);
        }
        return mapped;
    }
}
