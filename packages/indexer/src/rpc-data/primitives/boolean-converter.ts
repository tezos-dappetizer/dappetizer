import { PrimitiveConverter } from './primitive-converter';

export class BooleanConverter extends PrimitiveConverter<boolean, boolean> {
    protected override readonly getTypeDescription = (): string => 'boolean';

    protected override convertPrimitive(rpcData: unknown): boolean {
        if (typeof rpcData !== 'boolean') {
            throw new Error('It is NOT a boolean.');
        }
        return rpcData;
    }
}
