import { AddressConverter } from './address-converter';
import { BigIntegerConverter } from './big-integer-converter';
import { BooleanConverter } from './boolean-converter';
import { EnumConverter } from './enum-converter';
import { IntegerConverter } from './integer-converter';
import { IsoDateConverter } from './iso-date-converter';
import { MichelsonConverter } from './michelson-converter';
import { MichelsonSchemaConverter } from './michelson-schema-converter';
import { StringConverter } from './string-converter';

export const primitives = {
    address: new AddressConverter(),
    bigInteger: new BigIntegerConverter(),
    boolean: new BooleanConverter(),
    enum: new EnumConverter(),
    integer: new IntegerConverter(),
    isoDate: new IsoDateConverter(),
    michelson: new MichelsonConverter(),
    michelsonSchema: new MichelsonSchemaConverter(),
    nonWhiteSpaceString: new StringConverter('NotWhiteSpace'),
    string: new StringConverter(),
} as const;
