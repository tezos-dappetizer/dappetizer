import { dumpType, errorToString } from '@tezos-dappetizer/utils';

import { RpcConversionError } from '../rpc-conversion-error';
import { RpcConverter } from '../rpc-converter';

export abstract class PrimitiveConverter<TRpc, TTarget, TOptions = void> extends RpcConverter<TRpc, TTarget, TOptions> {
    protected override convertValue(rpcData: TRpc, uid: string, options: TOptions): TTarget {
        try {
            return this.convertPrimitive(rpcData, options);
        } catch (error) {
            const rpcDataJson = JSON.stringify(rpcData);
            const errorMessage = errorToString(error, { onlyMessage: true });
            const typeDescription = this.getTypeDescription(options);
            const dataDescription = `It must be ${typeDescription} but it is ${rpcDataJson} of type ${dumpType(rpcData)}. ${errorMessage}`;

            throw new RpcConversionError(uid, dataDescription);
        }
    }

    protected abstract convertPrimitive(rpcData: unknown, options: TOptions): TTarget;
    protected abstract getTypeDescription(options: TOptions): string;
}
