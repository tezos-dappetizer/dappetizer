import { MichelsonConverter } from './michelson-converter';
import { MichelsonSchema } from '../../michelson/michelson-schema';
import { RpcObjectConverter } from '../rpc-converter';
import { Michelson } from '../rpc-data-interfaces';

export class MichelsonSchemaConverter extends RpcObjectConverter<Michelson, MichelsonSchema> {
    private readonly michelsonConverter = new MichelsonConverter();

    protected override convertObject(rpcData: Michelson, uid: string): MichelsonSchema {
        const michelson = this.michelsonConverter.convert(rpcData, uid);
        return new MichelsonSchema(michelson);
    }
}
