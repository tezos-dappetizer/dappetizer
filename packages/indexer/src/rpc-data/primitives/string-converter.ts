import { StringLimits, stringValidator } from '@tezos-dappetizer/utils';

import { PrimitiveConverter } from './primitive-converter';

export class StringConverter extends PrimitiveConverter<string, string> {
    constructor(
        private readonly limits?: StringLimits,
        private readonly validator = stringValidator,
    ) {
        super();
    }

    protected override getTypeDescription(): string {
        return this.validator.getExpectedValueDescription(this.limits);
    }

    protected override convertPrimitive(rpcData: unknown): string {
        return this.validator.validate(rpcData, this.limits);
    }
}
