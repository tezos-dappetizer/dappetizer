import { hasConstructor } from '@tezos-dappetizer/utils';

import { PrimitiveConverter } from './primitive-converter';

const ISO_PREFIX_REGEX = /^\d{4}-(?:0[1-9]|1[012])-(?:0[1-9]|[12]\d|3[01])/u;

export class IsoDateConverter extends PrimitiveConverter<Date | string, Date> {
    protected override readonly getTypeDescription = (): string => 'Date object or timestamp string';

    protected override convertPrimitive(rpcData: unknown): Date {
        if (hasConstructor(rpcData, Date)) {
            if (isNaN(rpcData.getTime())) {
                throw new Error('The given Date is invalid.');
            }
            return rpcData;
        }

        if (typeof rpcData !== 'string') {
            throw new Error('Is is NOT a timestamp string nor Date object.');
        }

        // Validate using regex b/c new Date('123') is a valid Date.
        const date = new Date(rpcData);
        if (!ISO_PREFIX_REGEX.test(rpcData) || isNaN(date.getTime())) {
            throw new Error('The given string is NOT according to ISO 8601.');
        }
        return date;
    }
}
