import { BigNumber } from 'bignumber.js';

import { PrimitiveConverter } from './primitive-converter';

export interface BigIntegerConvertOptions {
    readonly min?: number;
    readonly max?: number;
}

export class BigIntegerConverter extends PrimitiveConverter<string, BigNumber, BigIntegerConvertOptions | void> {
    protected override getTypeDescription(options: BigIntegerConvertOptions | undefined): string {
        const min = typeof options?.min === 'number' ? `greater than or equal to ${options.min}` : '';
        const max = typeof options?.max === 'number' ? `less than or equal to ${options.max}` : '';

        return `a big integer ${min}${min && max ? ' and ' : ''}${max}`.trimEnd();
    }

    protected override convertPrimitive(rpcData: unknown, options: BigIntegerConvertOptions | undefined): BigNumber {
        const value = new BigNumber(rpcData as BigNumber.Value);

        if (value.isNaN()) {
            throw new Error('It is NOT a valid value for BigNumber.');
        }
        if (!value.isInteger()) {
            throw new Error('It is NOT an integer.');
        }
        if (typeof options?.min === 'number' && value.lt(options.min)) {
            throw new Error(`It is less than minimum ${options.min}.`);
        }
        if (typeof options?.max === 'number' && value.gt(options.max)) {
            throw new Error(`It is greater than maximum ${options.max}.`);
        }
        return value;
    }
}
