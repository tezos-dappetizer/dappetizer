import { NumberLimits, numberValidator } from '@tezos-dappetizer/utils';
import { StrictOmit } from 'ts-essentials';

import { PrimitiveConverter } from './primitive-converter';

export type IntegerConvertOptions = StrictOmit<NumberLimits, 'integer'>;

export class IntegerConverter extends PrimitiveConverter<number, number, IntegerConvertOptions | void> {
    constructor(
        private readonly validator = numberValidator,
    ) {
        super();
    }

    protected override getTypeDescription(options: IntegerConvertOptions | undefined): string {
        return this.validator.getExpectedValueDescription({ ...options, integer: true });
    }

    protected override convertPrimitive(
        rpcData: unknown,
        options: IntegerConvertOptions | undefined,
    ): number {
        return this.validator.validate(rpcData, { ...options, integer: true });
    }
}
