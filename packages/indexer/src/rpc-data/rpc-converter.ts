import { dumpType, isNullish, isReadOnlyArray, Nullish } from '@tezos-dappetizer/utils';

import { RpcConversionError } from './rpc-conversion-error';

/** Base convenient class for converters from RPC. In general, we do not trust values -> strict checks. */
export abstract class RpcConverter<TRpc, TTarget, TOptions = void> {
    convertProperty<TProperty extends string>(
        rpcParentData: { [P in TProperty]?: Nullish<TRpc> },
        propertyName: TProperty,
        parentUid: string,
        options: TOptions,
    ): TTarget {
        return this.convert(rpcParentData[propertyName], joinUid(parentUid, propertyName), options);
    }

    convert(rpcData: Nullish<TRpc>, uid: string, options: TOptions): TTarget {
        try {
            return this.convertValue(rpcData, uid, options);
        } catch (error) {
            throw RpcConversionError.wrap(error, uid);
        }
    }

    convertNullableProperty<TProperty extends string>(
        rpcParentData: { [P in TProperty]?: Nullish<TRpc> },
        propertyName: TProperty,
        parentUid: string,
        options: TOptions,
    ): TTarget | null {
        return this.convertNullable(rpcParentData[propertyName], joinUid(parentUid, propertyName), options);
    }

    convertNullable(rpcData: Nullish<TRpc>, uid: string, options: TOptions): TTarget | null {
        return !isNullish(rpcData)
            ? this.convert(rpcData, uid, options)
            : null;
    }

    convertArrayProperty<TProperty extends string>(
        rpcParentData: { [P in TProperty]?: Nullish<readonly TRpc[]> },
        propertyName: TProperty,
        parentUid: string,
        options: TOptions,
    ): readonly TTarget[] {
        return this.convertArray(rpcParentData[propertyName], joinUid(parentUid, propertyName), options);
    }

    convertArray(rpcData: Nullish<readonly TRpc[]>, uid: string, options: TOptions): readonly TTarget[] {
        if (isNullish(rpcData)) {
            return Object.freeze([]);
        }
        if (!isReadOnlyArray(rpcData)) {
            throw new RpcConversionError(uid, `It must be an array but its type is ${dumpType(rpcData)}'.`);
        }

        const result = rpcData.map((item, index) => this.convert(item, joinUid(uid, index), options));
        return Object.freeze(result);
    }

    /** Should implement strict checks. */
    protected abstract convertValue(rpcData: Nullish<TRpc>, uid: string, options: TOptions): TTarget;
}

export abstract class RpcObjectConverter<TRpc extends object, TTarget, TOptions = void> extends RpcConverter<TRpc, TTarget, TOptions> {
    protected override convertValue(rpcData: Nullish<TRpc>, uid: string, options: TOptions): TTarget {
        const rpcDataObj = guardRpcObject(rpcData, uid);
        return this.convertObject(rpcDataObj, uid, options);
    }

    protected abstract convertObject(rpcData: TRpc, uid: string, options: TOptions): TTarget;
}

export function guardRpcObject<TRpc extends object>(rpcData: Nullish<TRpc>, uid: string): TRpc {
    if (typeof rpcData !== 'object' || rpcData === null) {
        throw new RpcConversionError(uid, `It must be an object but its type is ${dumpType(rpcData)}.`);
    }
    return rpcData;
}

export function joinUid(...parts: readonly (string | number)[]): string {
    return parts.join('/');
}

export function appendUidAttribute(uid: string, attribute: Nullish<string>): string {
    return attribute ? `${uid}[${attribute}]` : uid;
}
