import { singleton } from 'tsyringe';

import { TaquitoRpcDelegationResult } from '../../../rpc-data-interfaces';
import { SimpleOperationResultConverter } from '../../common/operation-result/simple-operation-result-converter';

@singleton()
export class DelegationResultConverter extends SimpleOperationResultConverter<TaquitoRpcDelegationResult> {}
