import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../common/balance-update/balance-update';
import { AppliedOperationResult, OperationResult } from '../common/operation-result/operation-results';

/** @category RPC Block Data */
export type TaquitoRpcRegisterGlobalConstantResult = DeepReadonly<taquitoRpc.OperationResultRegisterGlobalConstant>;

/** @category RPC Block Data */
export type RegisterGlobalConstantResult = OperationResult<AppliedRegisterGlobalConstantResult>;

/** @category RPC Block Data */
export interface AppliedRegisterGlobalConstantResult extends AppliedOperationResult<TaquitoRpcRegisterGlobalConstantResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly globalAddress: string;
    readonly storageSize: BigNumber | null;
}
