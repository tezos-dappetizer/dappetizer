import { singleton } from 'tsyringe';

import { RegisterGlobalConstantResultConverter } from './register-global-constant-result-converter';
import { primitives } from '../../../primitives';
import { RpcConvertOptions } from '../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../rpc-converter';
import {
    OperationKind,
    RegisterGlobalConstantOperation,
    TaquitoRpcRegisterGlobalConstantOperation,
} from '../../../rpc-data-interfaces';
import {
    CommonMainOperationWithResultConverter,
} from '../../common/main-operation/common-main-operation-with-result-converter';

@singleton()
export class RegisterGlobalConstantOperationConverter
    extends RpcObjectConverter<TaquitoRpcRegisterGlobalConstantOperation, RegisterGlobalConstantOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonMainOperationWithResultConverter,
        private readonly resultConverter: RegisterGlobalConstantResultConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcRegisterGlobalConstantOperation,
        uid: string,
        options: RpcConvertOptions,
    ): RegisterGlobalConstantOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const result = this.resultConverter.convertFromMetadataOperationResult(rpcData, uid);

        return Object.freeze<RegisterGlobalConstantOperation>({
            ...commonProperties,
            kind: OperationKind.RegisterGlobalConstant,
            result,
            rpcData,
            value: primitives.michelson.convertProperty(rpcData, 'value', uid),
        });
    }
}
