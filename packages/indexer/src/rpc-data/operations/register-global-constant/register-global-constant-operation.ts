import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { RegisterGlobalConstantResult } from './register-global-constant-result';
import { Michelson } from '../../rpc-data-interfaces';
import { BaseMainOperationWithResult } from '../common/main-operation/base-main-operation-with-result';
import { OperationKind } from '../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcRegisterGlobalConstantOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultRegisterGlobalConstant>;

/**
 * Support for registering global constants.
 * @category RPC Block Data
 */
export interface RegisterGlobalConstantOperation extends BaseMainOperationWithResult<TaquitoRpcRegisterGlobalConstantOperation> {
    readonly kind: OperationKind.RegisterGlobalConstant;

    // Common register global constant properties:
    readonly value: Michelson;
    readonly result: RegisterGlobalConstantResult;
}
