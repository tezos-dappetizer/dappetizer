import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../common/balance-update/balance-update';
import { EntityToIndex } from '../../entity-to-index';
import { OperationKind } from '../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcPreendorsementOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultPreEndorsement>;

/**
 * A first vote for a candidate block with the aim of building a preendorsement quorum.
 * @category RPC Block Data
 */
export interface PreendorsementOperation extends EntityToIndex<TaquitoRpcPreendorsementOperation> {
    // Narrowing properties:
    // eslint-disable-next-line deprecation/deprecation
    readonly kind: OperationKind.Preendorsement;

    // Specific properties:
    readonly balanceUpdates: readonly BalanceUpdate[];
}
