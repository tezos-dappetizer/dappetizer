import * as taquitoRpc from '@taquito/rpc';
import { resolveLogger } from '@tezos-dappetizer/utils';
import { registry } from 'tsyringe';

import { RpcByKindConverter } from './common/rpc-by-kind-converter';
import { DelegationInternalOperationConverter } from './delegation/converters/delegation-internal-operation-converter';
import { EventInternalOperationConverter } from './event/converters/event-internal-operation-converter';
import { OriginationInternalOperationConverter } from './origination/converters/origination-internal-operation-converter';
import { TransactionInternalOperationConverter } from './transaction/converters/transaction-internal-operation-converter';
import { RpcConvertOptions } from '../rpc-convert-options';
import { RpcConverter } from '../rpc-converter';
import { InternalOperation, TaquitoRpcInternalOperation } from '../rpc-data-interfaces';

export type SpecificOperationConverter = RpcConverter<TaquitoRpcInternalOperation, InternalOperation, RpcConvertOptions>;

@registry([{
    token: InternalOperationConverter,
    useFactory: diContainer => {
        const converters: Record<taquitoRpc.InternalOperationResultKindEnum, SpecificOperationConverter> = {
            delegation: diContainer.resolve(DelegationInternalOperationConverter),
            event: diContainer.resolve(EventInternalOperationConverter),
            origination: diContainer.resolve(OriginationInternalOperationConverter),
            transaction: diContainer.resolve(TransactionInternalOperationConverter),
        };
        return new InternalOperationConverter(converters, resolveLogger(diContainer, InternalOperationConverter));
    },
}])
export class InternalOperationConverter extends RpcByKindConverter<TaquitoRpcInternalOperation, InternalOperation> {}
