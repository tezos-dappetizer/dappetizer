import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { AppliedOperationResult, OperationResult } from '../../common/operation-result/operation-results';

/** @category RPC Block Data */
export type TaquitoRpcSmartRollupCementResult = DeepReadonly<taquitoRpc.OperationResultSmartRollupCement>;

/** @category RPC Block Data */
export type SmartRollupCementResult = OperationResult<AppliedSmartRollupCementResult>;

/**
 * @category RPC Block Data
 * @experimental
 */
export interface AppliedSmartRollupCementResult extends AppliedOperationResult<TaquitoRpcSmartRollupCementResult> {
    /** It is an integer greater than or equal to `0`. */
    readonly inboxLevel: number;

    readonly commitmentHash: string;
}
