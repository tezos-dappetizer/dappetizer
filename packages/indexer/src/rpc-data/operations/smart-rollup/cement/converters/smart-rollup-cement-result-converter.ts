import { singleton } from 'tsyringe';

import { primitives } from '../../../../primitives';
import { AppliedSmartRollupCementResult, TaquitoRpcSmartRollupCementResult } from '../../../../rpc-data-interfaces';
import {
    BaseOperationResultConverter,
    SpecificResultParts,
} from '../../../common/operation-result/base-operation-result-converter';

export type SmartRollupCementResultParts = SpecificResultParts<AppliedSmartRollupCementResult>;

@singleton()
export class SmartRollupCementResultConverter
    extends BaseOperationResultConverter<TaquitoRpcSmartRollupCementResult, SmartRollupCementResultParts> {
    protected override convertAppliedResultParts(
        rpcData: TaquitoRpcSmartRollupCementResult,
        uid: string,
    ): SmartRollupCementResultParts {
        return {
            commitmentHash: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'commitment_hash', uid),
            inboxLevel: primitives.integer.convertProperty(rpcData, 'inbox_level', uid),
        };
    }
}
