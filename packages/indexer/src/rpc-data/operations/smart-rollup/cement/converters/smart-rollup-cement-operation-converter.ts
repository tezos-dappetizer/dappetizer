import { singleton } from 'tsyringe';

import { SmartRollupCementResultConverter } from './smart-rollup-cement-result-converter';
import { RpcConvertOptions } from '../../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../../rpc-converter';
import {
    OperationKind,
    SmartRollupCementOperation,
    TaquitoRpcSmartRollupCementOperation,
} from '../../../../rpc-data-interfaces';
import { CommonSmartRollupOperationConverter } from '../../common/converters/common-smart-rollup-operation-converter';

@singleton()
export class SmartRollupCementOperationConverter
    extends RpcObjectConverter<TaquitoRpcSmartRollupCementOperation, SmartRollupCementOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonSmartRollupOperationConverter,
        private readonly resultConverter: SmartRollupCementResultConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcSmartRollupCementOperation,
        uid: string,
        options: RpcConvertOptions,
    ): SmartRollupCementOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const result = this.resultConverter.convertFromMetadataOperationResult(rpcData, uid);

        return Object.freeze<SmartRollupCementOperation>({
            ...commonProperties,
            kind: OperationKind.SmartRollupCement,
            result,
            rpcData,
        });
    }
}
