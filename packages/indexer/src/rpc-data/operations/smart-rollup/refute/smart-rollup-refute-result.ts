import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../../common/balance-update/balance-update';
import { AppliedOperationResult, OperationResult } from '../../common/operation-result/operation-results';
import { SmartRollupGameStatus } from '../common/smart-rollup-game-status';

/** @category RPC Block Data */
export type TaquitoRpcSmartRollupRefuteResult = DeepReadonly<taquitoRpc.OperationResultSmartRollupRefute>;

/** @category RPC Block Data */
export type SmartRollupRefuteResult = OperationResult<AppliedSmartRollupRefuteResult>;

/**
 * @category RPC Block Data
 * @experimental
 */
export interface AppliedSmartRollupRefuteResult extends AppliedOperationResult<TaquitoRpcSmartRollupRefuteResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly gameStatus: SmartRollupGameStatus;
}
