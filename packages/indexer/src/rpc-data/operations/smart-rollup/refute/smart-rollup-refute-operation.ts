import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';

import { SmartRollupRefuteResult } from './smart-rollup-refute-result';
import { SmartRollup } from '../../../../smart-rollups/smart-rollup';
import { BaseMainOperationWithResult } from '../../common/main-operation/base-main-operation-with-result';
import { OperationKind } from '../../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcSmartRollupRefuteOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSmartRollupRefute>;

/**
 * It is used to start or pursue a dispute. A dispute is resolved on the Tezos blockchain through a so-called refutation game,
 * where two players seek to prove the correctness of their respective commitment. The game consists in a dissection phase,
 * where the two players narrow down their disagreement to a single execution step, and a resolution, where the players provide a proof sustaining
 * their claims. The looser of a dispute looses their frozen bond: half of it is burned, and the winner receives the other half in compensation.
 * @category RPC Block Data
 * @experimental
 */
export interface SmartRollupRefuteOperation extends BaseMainOperationWithResult<TaquitoRpcSmartRollupRefuteOperation> {
    // Narrowing properties:
    readonly kind: OperationKind.SmartRollupRefute;

    // Specific properties:
    readonly opponentAddress: string;
    readonly refutation: SmartRollupRefutation;
    readonly result: SmartRollupRefuteResult;
    readonly rollup: SmartRollup;
}

/**
 * @category RPC Block Data
 * @experimental
 */
export type SmartRollupRefutation = SmartRollupRefutationStart | SmartRollupRefutationMove;

/**
 * @category RPC Block Data
 * @experimental
 */
export enum SmartRollupRefutationKind {
    Start = 'Start',
    Move = 'Move',
}

/**
 * @category RPC Block Data
 * @experimental
 */
export interface SmartRollupRefutationStart {
    readonly kind: SmartRollupRefutationKind.Start;
    readonly opponentCommitmentHash: string;
    readonly playerCommitmentHash: string;

    /** Always `null` for this type so that the property is directly available on {@link SmartRollupRefutation} union. */
    readonly choice: null;
}

/**
 * @category RPC Block Data
 * @experimental
 */
export interface SmartRollupRefutationMove {
    readonly kind: SmartRollupRefutationKind.Move;

    /** It is an integer greater than or equal to `0`. */
    readonly choice: BigNumber;

    /** Always `null` for this type so that the property is directly available on {@link SmartRollupRefutation} union. */
    readonly opponentCommitmentHash: null;

    /** Always `null` for this type so that the property is directly available on {@link SmartRollupRefutation} union. */
    readonly playerCommitmentHash: null;
}
