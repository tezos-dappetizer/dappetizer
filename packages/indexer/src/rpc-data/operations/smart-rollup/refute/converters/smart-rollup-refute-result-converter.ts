import { singleton } from 'tsyringe';

import { BalanceUpdateConverter } from '../../../../common/balance-update/converters/balance-update-converter';
import { OperationErrorConverter } from '../../../../common/operation-error/operation-error-converter';
import { AppliedSmartRollupRefuteResult, TaquitoRpcSmartRollupRefuteResult } from '../../../../rpc-data-interfaces';
import {
    BaseOperationResultConverter,
    SpecificResultParts,
} from '../../../common/operation-result/base-operation-result-converter';
import { SmartRollupGameStatusConverter } from '../../common/converters/smart-rollup-game-status-converter';

export type SmartRollupRefuteResultParts = SpecificResultParts<AppliedSmartRollupRefuteResult>;

@singleton()
export class SmartRollupRefuteResultConverter extends BaseOperationResultConverter<TaquitoRpcSmartRollupRefuteResult, SmartRollupRefuteResultParts> {
    constructor(
        operationErrorConverter: OperationErrorConverter,
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
        private readonly gameStatusConverter: SmartRollupGameStatusConverter,
    ) {
        super(operationErrorConverter);
    }

    protected override convertAppliedResultParts(
        rpcData: TaquitoRpcSmartRollupRefuteResult,
        uid: string,
    ): SmartRollupRefuteResultParts {
        const balanceUpdates = this.balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', uid);
        const gameStatus = this.gameStatusConverter.convertProperty(rpcData, 'game_status', uid);

        return { balanceUpdates, gameStatus };
    }
}
