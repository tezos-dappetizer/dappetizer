/**
 * @category RPC Block Data
 * @experimental
 */
export type SmartRollupGameStatus = SmartRollupGameStatusOngoingOrDraw | SmartRollupGameStatusLoser;

/**
 * @category RPC Block Data
 * @experimental
 */
export enum SmartRollupGameStatusKind {
    Draw = 'Draw',
    Loser = 'Loser',
    Ongoing = 'Ongoing',
}

/**
 * @category RPC Block Data
 * @experimental
 */
export interface SmartRollupGameStatusOngoingOrDraw {
    readonly kind: SmartRollupGameStatusKind.Ongoing | SmartRollupGameStatusKind.Draw;
    readonly reason: null;
    readonly playerAddress: null;
}

/**
 * @category RPC Block Data
 * @experimental
 */
export interface SmartRollupGameStatusLoser {
    readonly kind: SmartRollupGameStatusKind.Loser;
    readonly reason: SmartRollupGameStatusLoserReason;
    readonly playerAddress: string;
}

/**
 * @category RPC Block Data
 * @experimental
 */
export enum SmartRollupGameStatusLoserReason {
    ConflictResolved = 'ConflictResolved',
    Timeout = 'Timeout',
}
