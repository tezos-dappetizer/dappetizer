import { inject, singleton } from 'tsyringe';

import { SmartRollup } from '../../../../../smart-rollups/smart-rollup';
import { SMART_ROLLUP_PROVIDER_DI_TOKEN, SmartRollupProvider } from '../../../../../smart-rollups/smart-rollup-provider';
import { primitives } from '../../../../primitives';
import { RpcConverter } from '../../../../rpc-converter';

@singleton()
export class SmartRollupConverter extends RpcConverter<string, SmartRollup> {
    constructor(
        @inject(SMART_ROLLUP_PROVIDER_DI_TOKEN) private readonly rollupProvider: SmartRollupProvider,
    ) {
        super();
    }

    protected override convertValue(rpcData: string, uid: string): SmartRollup {
        const address = primitives.address.convert(rpcData, uid, ['sr1']);

        return this.rollupProvider.getRollup(address);
    }
}
