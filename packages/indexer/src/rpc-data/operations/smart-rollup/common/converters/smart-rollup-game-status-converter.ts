import * as taquitoRpc from '@taquito/rpc';
import { freezeResult, keyof } from '@tezos-dappetizer/utils';
import { DeepReadonly } from 'ts-essentials';
import { singleton } from 'tsyringe';

import { primitives } from '../../../../primitives';
import { guardRpcObject, joinUid, RpcConverter } from '../../../../rpc-converter';
import {
    SmartRollupGameStatus,
    SmartRollupGameStatusKind,
    SmartRollupGameStatusLoserReason,
} from '../../../../rpc-data-interfaces';

export type TaquitoRpcGameStatus = DeepReadonly<taquitoRpc.SmartRollupGameStatus>;

@singleton()
export class SmartRollupGameStatusConverter extends RpcConverter<TaquitoRpcGameStatus, SmartRollupGameStatus> {
    @freezeResult()
    protected override convertValue(rpcData: TaquitoRpcGameStatus, uid: string): SmartRollupGameStatus {
        if (rpcData === taquitoRpc.SmartRollupRefuteGameStatusOptions.ONGOING) {
            return { kind: SmartRollupGameStatusKind.Ongoing, playerAddress: null, reason: null };
        }

        const resultUid = joinUid(uid, keyof<typeof rpcData>('result'));
        guardRpcObject(rpcData, uid);
        guardRpcObject(rpcData.result, resultUid);

        primitives.enum.convertProperty(rpcData.result, 'kind', resultUid, kindDummyMapping);

        return rpcData.result.kind === taquitoRpc.SmartRollupRefuteGameEndedPlayerOutcomes.DRAW
            ? { kind: SmartRollupGameStatusKind.Draw, playerAddress: null, reason: null }
            : {
                kind: SmartRollupGameStatusKind.Loser,
                playerAddress: primitives.address.convertProperty(rpcData.result, 'player', resultUid),
                reason: primitives.enum.convertProperty(rpcData.result, 'reason', resultUid, reasonMapping),
            };
    }
}

const kindDummyMapping: Readonly<Record<taquitoRpc.SmartRollupRefuteGameEndedPlayerOutcomes, ''>> = {
    draw: '',
    loser: '',
};

const reasonMapping: Readonly<Record<taquitoRpc.SmartRollupRefuteGameEndedReason, SmartRollupGameStatusLoserReason>> = {
    conflict_resolved: SmartRollupGameStatusLoserReason.ConflictResolved,
    timeout: SmartRollupGameStatusLoserReason.Timeout,
};
