import { singleton } from 'tsyringe';

import { SmartRollupPublishResultConverter } from './smart-rollup-publish-result-converter';
import { primitives } from '../../../../primitives';
import { RpcConvertOptions } from '../../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../../rpc-converter';
import {
    OperationKind,
    SmartRollupCommitment,
    SmartRollupPublishOperation,
    TaquitoRpcSmartRollupPublishOperation,
} from '../../../../rpc-data-interfaces';
import { CommonSmartRollupOperationConverter } from '../../common/converters/common-smart-rollup-operation-converter';

@singleton()
export class SmartRollupCommitmentConverter extends RpcObjectConverter<TaquitoRpcSmartRollupPublishOperation['commitment'], SmartRollupCommitment> {
    protected override convertObject(
        rpcData: TaquitoRpcSmartRollupPublishOperation['commitment'],
        uid: string,
    ): SmartRollupCommitment {
        return Object.freeze<SmartRollupCommitment>({
            compressedState: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'compressed_state', uid),
            inboxLevel: primitives.integer.convertProperty(rpcData, 'inbox_level', uid, { min: 0 }),
            numberOfTicks: primitives.bigInteger.convertProperty(rpcData, 'number_of_ticks', uid, { min: 0 }),
            predecessorHash: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'predecessor', uid),
        });
    }
}

@singleton()
export class SmartRollupPublishOperationConverter
    extends RpcObjectConverter<TaquitoRpcSmartRollupPublishOperation, SmartRollupPublishOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonSmartRollupOperationConverter,
        private readonly resultConverter: SmartRollupPublishResultConverter,
        private readonly commitmentConverter: SmartRollupCommitmentConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcSmartRollupPublishOperation,
        uid: string,
        options: RpcConvertOptions,
    ): SmartRollupPublishOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const commitment = this.commitmentConverter.convertProperty(rpcData, 'commitment', uid);
        const result = this.resultConverter.convertFromMetadataOperationResult(rpcData, uid);

        return Object.freeze<SmartRollupPublishOperation>({
            ...commonProperties,
            commitment,
            kind: OperationKind.SmartRollupPublish,
            result,
            rpcData,
        });
    }
}
