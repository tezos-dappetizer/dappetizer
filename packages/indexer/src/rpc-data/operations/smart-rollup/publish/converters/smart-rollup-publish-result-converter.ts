import { singleton } from 'tsyringe';

import { BalanceUpdateConverter } from '../../../../common/balance-update/converters/balance-update-converter';
import { OperationErrorConverter } from '../../../../common/operation-error/operation-error-converter';
import { primitives } from '../../../../primitives';
import { AppliedSmartRollupPublishResult, TaquitoRpcSmartRollupPublishResult } from '../../../../rpc-data-interfaces';
import {
    BaseOperationResultConverter,
    SpecificResultParts,
} from '../../../common/operation-result/base-operation-result-converter';

export type SmartRollupPublishResultParts = SpecificResultParts<AppliedSmartRollupPublishResult>;

@singleton()
export class SmartRollupPublishResultConverter
    extends BaseOperationResultConverter<TaquitoRpcSmartRollupPublishResult, SmartRollupPublishResultParts> {
    constructor(
        operationErrorConverter: OperationErrorConverter,
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
    ) {
        super(operationErrorConverter);
    }

    protected override convertAppliedResultParts(
        rpcData: TaquitoRpcSmartRollupPublishResult,
        uid: string,
    ): SmartRollupPublishResultParts {
        const balanceUpdates = this.balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', uid);

        return {
            balanceUpdates,
            publishedAtLevel: primitives.integer.convertProperty(rpcData, 'published_at_level', uid),
            stakedHash: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'staked_hash', uid),
        };
    }
}
