import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';

import { SmartRollupPublishResult } from './smart-rollup-publish-result';
import { SmartRollup } from '../../../../smart-rollups/smart-rollup';
import { BaseMainOperationWithResult } from '../../common/main-operation/base-main-operation-with-result';
import { OperationKind } from '../../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcSmartRollupPublishOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSmartRollupPublish>;

/**
 * It is used to regularly declare what is the new state of a given smart rollup in a so-called `commitment`. To publish commitments,
 * an implicit account has to own at least ꜩ 10,000, which are frozen as long as at least one of their commitments is disputable.
 * @category RPC Block Data
 * @experimental
 */
export interface SmartRollupPublishOperation extends BaseMainOperationWithResult<TaquitoRpcSmartRollupPublishOperation> {
    // Narrowing properties:

    readonly kind: OperationKind.SmartRollupPublish;

    // Specific properties:

    readonly commitment: SmartRollupCommitment;
    readonly result: SmartRollupPublishResult;

    readonly rollup: SmartRollup;
}

/**
 * @category RPC Block Data
 * @experimental
 */
export interface SmartRollupCommitment {
    readonly compressedState: string;

    /** It is an integer greater than or equal to `0`. */
    readonly inboxLevel: number;

    /** It is an integer greater than or equal to `0`. */
    readonly numberOfTicks: BigNumber;

    readonly predecessorHash: string;
}
