import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../../common/balance-update/balance-update';
import { AppliedOperationResult, OperationResult } from '../../common/operation-result/operation-results';

/** @category RPC Block Data */
export type TaquitoRpcSmartRollupPublishResult = DeepReadonly<taquitoRpc.OperationResultSmartRollupPublish>;

/** @category RPC Block Data */
export type SmartRollupPublishResult = OperationResult<AppliedSmartRollupPublishResult>;

/**
 * @category RPC Block Data
 * @experimental
 */
export interface AppliedSmartRollupPublishResult extends AppliedOperationResult<TaquitoRpcSmartRollupPublishResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];

    /** It is an integer greater than or equal to `0`. */
    readonly publishedAtLevel: number;

    readonly stakedHash: string;
}
