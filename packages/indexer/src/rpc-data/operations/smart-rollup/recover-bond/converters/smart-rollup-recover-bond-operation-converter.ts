import { singleton } from 'tsyringe';

import { SmartRollupRecoverBondResultConverter } from './smart-rollup-recover-bond-result-converter';
import { primitives } from '../../../../primitives';
import { RpcConvertOptions } from '../../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../../rpc-converter';
import {
    OperationKind,
    SmartRollupRecoverBondOperation,
    TaquitoRpcSmartRollupRecoverBondOperation,
} from '../../../../rpc-data-interfaces';
import { CommonSmartRollupOperationConverter } from '../../common/converters/common-smart-rollup-operation-converter';

@singleton()
export class SmartRollupRecoverBondOperationConverter
    extends RpcObjectConverter<TaquitoRpcSmartRollupRecoverBondOperation, SmartRollupRecoverBondOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonSmartRollupOperationConverter,
        private readonly resultConverter: SmartRollupRecoverBondResultConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcSmartRollupRecoverBondOperation,
        uid: string,
        options: RpcConvertOptions,
    ): SmartRollupRecoverBondOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const result = this.resultConverter.convertFromMetadataOperationResult(rpcData, uid);

        return Object.freeze<SmartRollupRecoverBondOperation>({
            ...commonProperties,
            kind: OperationKind.SmartRollupRecoverBond,
            result,
            rpcData,
            stakerAddress: primitives.address.convertProperty(rpcData, 'staker', uid),
        });
    }
}
