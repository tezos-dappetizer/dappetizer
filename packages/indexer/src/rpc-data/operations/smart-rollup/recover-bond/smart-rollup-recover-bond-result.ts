import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../../common/balance-update/balance-update';
import { AppliedOperationResult, OperationResult } from '../../common/operation-result/operation-results';

/** @category RPC Block Data */
export type TaquitoRpcSmartRollupRecoverBondResult = DeepReadonly<taquitoRpc.OperationResultSmartRollupRecoverBond>;

/** @category RPC Block Data */
export type SmartRollupRecoverBondResult = OperationResult<AppliedSmartRollupRecoverBondResult>;

/**
 * @category RPC Block Data
 * @experimental
 */
export interface AppliedSmartRollupRecoverBondResult extends AppliedOperationResult<TaquitoRpcSmartRollupRecoverBondResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
}
