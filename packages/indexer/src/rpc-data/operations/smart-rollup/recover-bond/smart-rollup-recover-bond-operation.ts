import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { SmartRollupRecoverBondResult } from './smart-rollup-recover-bond-result';
import { SmartRollup } from '../../../../smart-rollups/smart-rollup';
import { BaseMainOperationWithResult } from '../../common/main-operation/base-main-operation-with-result';
import { OperationKind } from '../../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcSmartRollupRecoverBondOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSmartRollupRecoverBond>;

/**
 * It is used by an implicit account to unfreeze their ꜩ 10,000.
 * This operation only succeeds if and only if all the commitments published by the implicit account have been cemented.
 * @category RPC Block Data
 * @experimental
 */
export interface SmartRollupRecoverBondOperation extends BaseMainOperationWithResult<TaquitoRpcSmartRollupRecoverBondOperation> {
    // Narrowing properties:
    readonly kind: OperationKind.SmartRollupRecoverBond;

    // Specific properties:
    readonly result: SmartRollupRecoverBondResult;
    readonly rollup: SmartRollup;
    readonly stakerAddress: string;
}
