import { singleton } from 'tsyringe';

import { TaquitoRpcSmartRollupAddMessagesResult } from '../../../../rpc-data-interfaces';
import { SimpleOperationResultConverter } from '../../../common/operation-result/simple-operation-result-converter';

@singleton()
export class SmartRollupAddMessagesResultConverter extends SimpleOperationResultConverter<TaquitoRpcSmartRollupAddMessagesResult> {}
