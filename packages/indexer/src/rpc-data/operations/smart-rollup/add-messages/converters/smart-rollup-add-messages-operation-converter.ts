import { singleton } from 'tsyringe';

import { SmartRollupAddMessagesResultConverter } from './smart-rollup-add-messages-result-converter';
import { primitives } from '../../../../primitives';
import { RpcConvertOptions } from '../../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../../rpc-converter';
import {
    OperationKind,
    SmartRollupAddMessagesOperation,
    TaquitoRpcSmartRollupAddMessagesOperation,
} from '../../../../rpc-data-interfaces';
import {
    CommonMainOperationWithResultConverter,
} from '../../../common/main-operation/common-main-operation-with-result-converter';

@singleton()
export class SmartRollupAddMessagesOperationConverter
    extends RpcObjectConverter<TaquitoRpcSmartRollupAddMessagesOperation, SmartRollupAddMessagesOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonMainOperationWithResultConverter,
        private readonly resultConverter: SmartRollupAddMessagesResultConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcSmartRollupAddMessagesOperation,
        uid: string,
        options: RpcConvertOptions,
    ): SmartRollupAddMessagesOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const result = this.resultConverter.convertFromMetadataOperationResult(rpcData, uid);

        return Object.freeze<SmartRollupAddMessagesOperation>({
            ...commonProperties,
            kind: OperationKind.SmartRollupAddMessages,
            message: primitives.string.convertArrayProperty(rpcData, 'message', uid),
            result,
            rpcData,
        });
    }
}
