import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { AppliedOperationResult, OperationResult } from '../../common/operation-result/operation-results';

/** @category RPC Block Data */
export type TaquitoRpcSmartRollupAddMessagesResult = DeepReadonly<taquitoRpc.OperationResultSmartRollupAddMessages>;

/** @category RPC Block Data */
export type SmartRollupAddMessagesResult = OperationResult<AppliedSmartRollupAddMessagesResult>;

/**
 * @category RPC Block Data
 * @experimental
 */
export interface AppliedSmartRollupAddMessagesResult extends AppliedOperationResult<TaquitoRpcSmartRollupAddMessagesResult> {}
