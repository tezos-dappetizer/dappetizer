import { singleton } from 'tsyringe';

import { SmartRollupOriginateResultConverter } from './smart-rollup-originate-result-converter';
import { primitives } from '../../../../primitives';
import { RpcConvertOptions } from '../../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../../rpc-converter';
import {
    OperationKind,
    SmartRollupOriginateOperation,
    TaquitoRpcSmartRollupOriginateOperation,
} from '../../../../rpc-data-interfaces';
import {
    CommonMainOperationWithResultConverter,
} from '../../../common/main-operation/common-main-operation-with-result-converter';

@singleton()
export class SmartRollupOriginateOperationConverter
    extends RpcObjectConverter<TaquitoRpcSmartRollupOriginateOperation, SmartRollupOriginateOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonMainOperationWithResultConverter,
        private readonly resultConverter: SmartRollupOriginateResultConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcSmartRollupOriginateOperation,
        uid: string,
        options: RpcConvertOptions,
    ): SmartRollupOriginateOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const result = this.resultConverter.convertFromMetadataOperationResult(rpcData, uid);

        return Object.freeze<SmartRollupOriginateOperation>({
            ...commonProperties,
            kernel: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'kernel', uid),
            kind: OperationKind.SmartRollupOriginate,
            parametersSchema: primitives.michelsonSchema.convertProperty(rpcData, 'parameters_ty', uid),
            pvmKind: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'pvm_kind', uid),
            result,
            rpcData,
        });
    }
}
