import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { SmartRollupOriginateResult } from './smart-rollup-originate-result';
import { MichelsonSchema } from '../../../../michelson/michelson-schema';
import { BaseMainOperationWithResult } from '../../common/main-operation/base-main-operation-with-result';
import { OperationKind } from '../../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcSmartRollupOriginateOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSmartRollupOriginate>;

/**
 * It is used to originate, that is, to deploy smart rollups in the Tezos blockchain.
 * @category RPC Block Data
 * @experimental
 */
export interface SmartRollupOriginateOperation extends BaseMainOperationWithResult<TaquitoRpcSmartRollupOriginateOperation> {
    readonly kind: OperationKind.SmartRollupOriginate;

    // Specific properties:

    readonly kernel: string;
    readonly parametersSchema: MichelsonSchema;

    /** Usually `'wasm_2_0_0'` or `'arith'`. */
    readonly pvmKind: string;

    readonly result: SmartRollupOriginateResult;
}
