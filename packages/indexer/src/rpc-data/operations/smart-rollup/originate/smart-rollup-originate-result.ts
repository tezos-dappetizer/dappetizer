import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';

import { SmartRollup } from '../../../../smart-rollups/smart-rollup';
import { BalanceUpdate } from '../../../common/balance-update/balance-update';
import { AppliedOperationResult, OperationResult } from '../../common/operation-result/operation-results';

/** @category RPC Block Data */
export type TaquitoRpcSmartRollupOriginateResult = DeepReadonly<taquitoRpc.OperationResultSmartRollupOriginate>;

/** @category RPC Block Data */
export type SmartRollupOriginateResult = OperationResult<AppliedSmartRollupOriginateResult>;

/**
 * @category RPC Block Data
 * @experimental
 */
export interface AppliedSmartRollupOriginateResult extends AppliedOperationResult<TaquitoRpcSmartRollupOriginateResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly genesisCommitmentHash: string;

    readonly originatedRollup: SmartRollup;

    /** It is an integer greater than or equal to `0`. */
    readonly size: BigNumber;
}
