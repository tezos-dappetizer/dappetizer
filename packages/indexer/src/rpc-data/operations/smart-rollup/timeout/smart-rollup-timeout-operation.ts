import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { SmartRollupTimeoutResult } from './smart-rollup-timeout-result';
import { SmartRollup } from '../../../../smart-rollups/smart-rollup';
import { BaseMainOperationWithResult } from '../../common/main-operation/base-main-operation-with-result';
import { OperationKind } from '../../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcSmartRollupTimeoutOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSmartRollupTimeout>;

/**
 * It is used to put an end to a dispute if one of the two players takes too much time to send their next move
 * (with a `smart_rollup_refute` operation). It is not necessary to be one of the players to send this operation.
 * @category RPC Block Data
 * @experimental
 */
export interface SmartRollupTimeoutOperation extends BaseMainOperationWithResult<TaquitoRpcSmartRollupTimeoutOperation> {
    // Narrowing properties:
    readonly kind: OperationKind.SmartRollupTimeout;

    // Specific properties:
    readonly aliceStakerAddress: string;
    readonly bobStakerAddress: string;
    readonly result: SmartRollupTimeoutResult;
    readonly rollup: SmartRollup;
}
