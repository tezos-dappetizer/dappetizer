import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../../common/balance-update/balance-update';
import { AppliedOperationResult, OperationResult } from '../../common/operation-result/operation-results';
import { SmartRollupGameStatus } from '../common/smart-rollup-game-status';

/** @category RPC Block Data */
export type TaquitoRpcSmartRollupTimeoutResult = DeepReadonly<taquitoRpc.OperationResultSmartRollupTimeout>;

/** @category RPC Block Data */
export type SmartRollupTimeoutResult = OperationResult<AppliedSmartRollupTimeoutResult>;

/**
 * @category RPC Block Data
 * @experimental
 */
export interface AppliedSmartRollupTimeoutResult extends AppliedOperationResult<TaquitoRpcSmartRollupTimeoutResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly gameStatus: SmartRollupGameStatus;
}
