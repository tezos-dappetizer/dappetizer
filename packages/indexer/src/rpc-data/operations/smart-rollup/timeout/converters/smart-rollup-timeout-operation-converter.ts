import { keyof } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { SmartRollupTimeoutResultConverter } from './smart-rollup-timeout-result-converter';
import { primitives } from '../../../../primitives';
import { RpcConvertOptions } from '../../../../rpc-convert-options';
import { guardRpcObject, joinUid, RpcObjectConverter } from '../../../../rpc-converter';
import {
    OperationKind,
    SmartRollupTimeoutOperation,
    TaquitoRpcSmartRollupTimeoutOperation,
} from '../../../../rpc-data-interfaces';
import { CommonSmartRollupOperationConverter } from '../../common/converters/common-smart-rollup-operation-converter';

@singleton()
export class SmartRollupTimeoutOperationConverter
    extends RpcObjectConverter<TaquitoRpcSmartRollupTimeoutOperation, SmartRollupTimeoutOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonSmartRollupOperationConverter,
        private readonly resultConverter: SmartRollupTimeoutResultConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcSmartRollupTimeoutOperation,
        uid: string,
        options: RpcConvertOptions,
    ): SmartRollupTimeoutOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const result = this.resultConverter.convertFromMetadataOperationResult(rpcData, uid);
        const stakers = this.convertStakers(rpcData.stakers, joinUid(uid, keyof<typeof rpcData>('stakers')));

        return Object.freeze<SmartRollupTimeoutOperation>({
            ...commonProperties,
            ...stakers,
            kind: OperationKind.SmartRollupTimeout,
            result,
            rpcData,
        });
    }

    private convertStakers(
        rpcData: TaquitoRpcSmartRollupTimeoutOperation['stakers'],
        uid: string,
    ): Pick<SmartRollupTimeoutOperation, 'aliceStakerAddress' | 'bobStakerAddress'> {
        guardRpcObject(rpcData, uid);
        return {
            aliceStakerAddress: primitives.address.convertProperty(rpcData, 'alice', uid),
            bobStakerAddress: primitives.address.convertProperty(rpcData, 'bob', uid),
        };
    }
}
