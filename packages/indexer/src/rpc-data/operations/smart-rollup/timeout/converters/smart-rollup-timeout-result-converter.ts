import { singleton } from 'tsyringe';

import { BalanceUpdateConverter } from '../../../../common/balance-update/converters/balance-update-converter';
import { OperationErrorConverter } from '../../../../common/operation-error/operation-error-converter';
import { AppliedSmartRollupTimeoutResult, TaquitoRpcSmartRollupTimeoutResult } from '../../../../rpc-data-interfaces';
import {
    BaseOperationResultConverter,
    SpecificResultParts,
} from '../../../common/operation-result/base-operation-result-converter';
import { SmartRollupGameStatusConverter } from '../../common/converters/smart-rollup-game-status-converter';

export type SmartRollupTimeoutResultParts = SpecificResultParts<AppliedSmartRollupTimeoutResult>;

@singleton()
export class SmartRollupTimeoutResultConverter
    extends BaseOperationResultConverter<TaquitoRpcSmartRollupTimeoutResult, SmartRollupTimeoutResultParts> {
    constructor(
        operationErrorConverter: OperationErrorConverter,
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
        private readonly gameStatusConverter: SmartRollupGameStatusConverter,
    ) {
        super(operationErrorConverter);
    }

    protected override convertAppliedResultParts(
        rpcData: TaquitoRpcSmartRollupTimeoutResult,
        uid: string,
    ): SmartRollupTimeoutResultParts {
        const balanceUpdates = this.balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', uid);
        const gameStatus = this.gameStatusConverter.convertProperty(rpcData, 'game_status', uid);

        return { balanceUpdates, gameStatus };
    }
}
