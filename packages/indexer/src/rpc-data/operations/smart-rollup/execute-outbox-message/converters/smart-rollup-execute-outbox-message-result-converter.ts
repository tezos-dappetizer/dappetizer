import { singleton } from 'tsyringe';

import { BalanceUpdateConverter } from '../../../../common/balance-update/converters/balance-update-converter';
import { OperationErrorConverter } from '../../../../common/operation-error/operation-error-converter';
import { TicketUpdatesConverter } from '../../../../common/ticket/converters/ticket-updates-converter';
import { primitives } from '../../../../primitives';
import {
    AppliedSmartRollupExecuteOutboxMessageResult,
    TaquitoRpcSmartRollupExecuteOutboxMessageResult,
} from '../../../../rpc-data-interfaces';
import {
    BaseOperationResultConverter,
    SpecificResultParts,
} from '../../../common/operation-result/base-operation-result-converter';

export type SmartRollupExecuteOutboxMessageResultParts = SpecificResultParts<AppliedSmartRollupExecuteOutboxMessageResult>;

@singleton()
export class SmartRollupExecuteOutboxMessageResultConverter
    extends BaseOperationResultConverter<TaquitoRpcSmartRollupExecuteOutboxMessageResult, SmartRollupExecuteOutboxMessageResultParts> {
    constructor(
        operationErrorConverter: OperationErrorConverter,
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
        private readonly ticketUpdatesConverter: TicketUpdatesConverter,
    ) {
        super(operationErrorConverter);
    }

    protected override convertAppliedResultParts(
        rpcData: TaquitoRpcSmartRollupExecuteOutboxMessageResult,
        uid: string,
    ): SmartRollupExecuteOutboxMessageResultParts {
        const balanceUpdates = this.balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', uid);
        const ticketUpdates = this.ticketUpdatesConverter.convertArrayProperty(rpcData, 'ticket_updates', uid);

        return {
            balanceUpdates,
            paidStorageSizeDiff: primitives.bigInteger.convertNullableProperty(rpcData, 'paid_storage_size_diff', uid),
            ticketUpdates,
        };
    }
}
