import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { SmartRollupExecuteOutboxMessageResult } from './smart-rollup-execute-outbox-message-result';
import { SmartRollup } from '../../../../smart-rollups/smart-rollup';
import { BaseMainOperationWithResult } from '../../common/main-operation/base-main-operation-with-result';
import { OperationKind } from '../../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcSmartRollupExecuteOutboxMessageOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSmartRollupExecuteOutboxMessage>;

/**
 * It is used to enact a transaction from a smart rollup to a smart contract, as authorized by a cemented commitment.
 * The targeted smart contract can determine if it is called by a smart rollup using the `SENDER` Michelson instruction.
 * @category RPC Block Data
 * @experimental
 */
export interface SmartRollupExecuteOutboxMessageOperation extends BaseMainOperationWithResult<TaquitoRpcSmartRollupExecuteOutboxMessageOperation> {
    // Narrowing properties:

    readonly kind: OperationKind.SmartRollupExecuteOutboxMessage;

    // Specific properties:

    readonly cementedCommitment: string;
    readonly outputProof: string;
    readonly result: SmartRollupExecuteOutboxMessageResult;

    readonly rollup: SmartRollup;
}
