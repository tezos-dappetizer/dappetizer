import { singleton } from 'tsyringe';

import { BlockHeaderConverter } from '../../block/block-header-converter';
import { BalanceUpdateConverter } from '../../common/balance-update/converters/balance-update-converter';
import { RpcConvertOptions } from '../../rpc-convert-options';
import { RpcObjectConverter } from '../../rpc-converter';
import {
    DoubleBakingEvidenceOperation,
    OperationKind,
    TaquitoRpcDoubleBakingEvidenceOperation,
} from '../../rpc-data-interfaces';

@singleton()
export class DoubleBakingEvidenceOperationConverter
    extends RpcObjectConverter<TaquitoRpcDoubleBakingEvidenceOperation, DoubleBakingEvidenceOperation, RpcConvertOptions> {
    constructor(
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
        private readonly blockHeaderConverter: BlockHeaderConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcDoubleBakingEvidenceOperation,
        uid: string,
        _options: RpcConvertOptions,
    ): DoubleBakingEvidenceOperation {
        const blockHeader1 = this.blockHeaderConverter.convertProperty(rpcData, 'bh1', uid);
        const blockHeader2 = this.blockHeaderConverter.convertProperty(rpcData, 'bh2', uid);
        const balanceUpdates = this.balanceUpdateConverter.convertFromMetadataProperty(rpcData, uid);

        return Object.freeze<DoubleBakingEvidenceOperation>({
            balanceUpdates,
            blockHeader1,
            blockHeader2,
            kind: OperationKind.DoubleBakingEvidence,
            rpcData,
            uid,
        });
    }
}
