import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../common/balance-update/balance-update';
import { EntityToIndex } from '../../entity-to-index';
import { OperationKind } from '../operation-kind';

/**
 * @category RPC Block Data
 * @deprecated Endorsements are not supported anymore because they were incomplete and with breaking changes. Let us know if you need them.
 */
export type TaquitoRpcDoublePreendorsementEvidenceOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultDoublePreEndorsement>;

/**
 * It allows for accusing a delegate of having *double-preendorsed* – i.e., of having preendorsed two different block candidates,
 * at the same level and at the same round. The bulk of the evidence, the two arguments provided, consists of the two offending preendorsements.
 * @category RPC Block Data
 * @deprecated Endorsements are not supported anymore because they were incomplete and with breaking changes. Let us know if you need them.
 */
// eslint-disable-next-line deprecation/deprecation
export interface DoublePreendorsementEvidenceOperation extends EntityToIndex<TaquitoRpcDoublePreendorsementEvidenceOperation> {
    // Narrowing properties:
    // eslint-disable-next-line deprecation/deprecation
    readonly kind: OperationKind.DoublePreendorsementEvidence;

    // Specific properties:
    readonly balanceUpdates: readonly BalanceUpdate[];
}
