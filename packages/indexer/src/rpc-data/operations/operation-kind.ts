/** @category RPC Block Data */
export enum OperationKind {
    ActivateAccount = 'ActivateAccount',
    Ballot = 'Ballot',
    Delegation = 'Delegation',
    DoubleBakingEvidence = 'DoubleBakingEvidence',

    /** @deprecated Endorsements are not supported anymore because they were incomplete and with breaking changes. Let us know if you need them. */
    DoubleEndorsementEvidence = 'DoubleEndorsementEvidence',

    /** @deprecated Endorsements are not supported anymore because they were incomplete and with breaking changes. Let us know if you need them. */
    DoublePreendorsementEvidence = 'DoublePreendorsementEvidence',

    DrainDelegate = 'DrainDelegate',

    /** @deprecated Endorsements are not supported anymore because they were incomplete and with breaking changes. Let us know if you need them. */
    Endorsement = 'Endorsement',

    Event = 'Event',
    IncreasePaidStorage = 'IncreasePaidStorage',
    Origination = 'Origination',

    /** @deprecated Endorsements are not supported anymore because they were incomplete and with breaking changes. Let us know if you need them. */
    Preendorsement = 'Preendorsement',

    Proposals = 'Proposals',
    RegisterGlobalConstant = 'RegisterGlobalConstant',
    Reveal = 'Reveal',
    SeedNonceRevelation = 'SeedNonceRevelation',
    SetDepositsLimit = 'SetDepositsLimit',
    SmartRollupAddMessages = 'SmartRollupAddMessages',
    SmartRollupCement = 'SmartRollupCement',
    SmartRollupExecuteOutboxMessage = 'SmartRollupExecuteOutboxMessage',
    SmartRollupOriginate = 'SmartRollupOriginate',
    SmartRollupPublish = 'SmartRollupPublish',
    SmartRollupRecoverBond = 'SmartRollupRecoverBond',
    SmartRollupRefute = 'SmartRollupRefute',
    SmartRollupTimeout = 'SmartRollupTimeout',
    Transaction = 'Transaction',
    TransferTicket = 'TransferTicket',
    UpdateConsensusKey = 'UpdateConsensusKey',
    VdfRevelation = 'VdfRevelation',
}
