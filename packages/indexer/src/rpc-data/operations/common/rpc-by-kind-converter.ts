import { Logger } from '@tezos-dappetizer/utils';

import { primitives } from '../../primitives';
import { RpcConvertOptions } from '../../rpc-convert-options';
import { appendUidAttribute, RpcConverter } from '../../rpc-converter';
import { RpcSkippableArrayConverter } from '../../rpc-skippable-array-converter';

export class RpcByKindConverter<TRpcItem extends { readonly kind: string }, TTargetItem>
    extends RpcSkippableArrayConverter<TRpcItem, TTargetItem, RpcConvertOptions> {
    constructor(
        private readonly convertersByKind: Readonly<Record<string, RpcConverter<TRpcItem, TTargetItem, RpcConvertOptions>>>,
        logger: Logger,
    ) {
        super(logger);
    }

    protected override convertObject(rpcData: TRpcItem, uidPrefix: string, options: RpcConvertOptions): TTargetItem {
        const converter = primitives.enum.convertProperty(rpcData, 'kind', uidPrefix, this.convertersByKind);
        const uid = appendUidAttribute(uidPrefix, rpcData.kind);

        return converter.convert(rpcData, uid, options);
    }
}
