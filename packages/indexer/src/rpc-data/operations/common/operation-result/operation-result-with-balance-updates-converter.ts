import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';
import { singleton } from 'tsyringe';

import { BaseOperationResultConverter, BaseRpcOperationResult } from './base-operation-result-converter';
import { BalanceUpdateConverter } from '../../../common/balance-update/converters/balance-update-converter';
import { OperationErrorConverter } from '../../../common/operation-error/operation-error-converter';
import { BalanceUpdate } from '../../../rpc-data-interfaces';

export type RpcOperationResultWithBalanceUpdates = BaseRpcOperationResult & DeepReadonly<{
    balance_updates?: taquitoRpc.OperationBalanceUpdates;
}>;

export interface OperationResultWithBalanceUpdatesParts {
    readonly balanceUpdates: readonly BalanceUpdate[];
}

@singleton()
export class OperationResultWithBalanceUpdatesConverter<TRpc extends RpcOperationResultWithBalanceUpdates>
    extends BaseOperationResultConverter<TRpc, OperationResultWithBalanceUpdatesParts> {
    constructor(
        operationErrorConverter: OperationErrorConverter,
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
    ) {
        super(operationErrorConverter);
    }

    protected convertAppliedResultParts(
        rpcData: TRpc,
        uid: string,
    ): OperationResultWithBalanceUpdatesParts {
        const balanceUpdates = this.balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', uid);

        return { balanceUpdates };
    }
}
