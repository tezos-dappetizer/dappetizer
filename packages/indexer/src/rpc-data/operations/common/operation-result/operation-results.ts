import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';

import { OperationResultStatus } from './operation-result-status';
import { OperationError } from '../../../common/operation-error/operation-error';
import { EntityToIndex } from '../../../entity-to-index';

/** @category RPC Block Data */
export type TaquitoRpcInternalOperation = DeepReadonly<taquitoRpc.InternalOperationResult>;

/** @category RPC Block Data */
export type OperationResult<TApplied extends AppliedOperationResult = AppliedOperationResult> =
    TApplied extends AppliedOperationResult<infer TRpcData>
        ? TApplied
        | BacktrackedOperationResult<TRpcData>
        | FailedOperationResult<TRpcData>
        | SkippedOperationResult<TRpcData>
        : never;

/** @category RPC Block Data */
export interface AppliedOperationResult<TRpcData = object> extends EntityToIndex<TRpcData> {
    /** Usable for narrowing the type of this object from respective operation result union. */
    readonly status: OperationResultStatus.Applied;

    /** If not `null` then it is an integer greater than or equal to `0`. */
    readonly consumedGas: BigNumber | null;

    /** If not `null` then it is an integer greater than or equal to `0`. */
    readonly consumedMilligas: BigNumber | null;
}

/** @category RPC Block Data */
export interface BacktrackedOperationResult<TRpcData = object> extends EntityToIndex<TRpcData> {
    /** Usable for narrowing the type of this object from respective operation result union. */
    readonly status: OperationResultStatus.Backtracked;

    /** If not `null` then it is an integer greater than or equal to `0`. */
    readonly consumedGas: BigNumber | null;

    /** If not `null` then it is an integer greater than or equal to `0`. */
    readonly consumedMilligas: BigNumber | null;

    readonly errors: readonly OperationError[];
}

/** @category RPC Block Data */
export interface FailedOperationResult<TRpcData = object> extends EntityToIndex<TRpcData> {
    /** Usable for narrowing the type of this object from respective operation result union. */
    readonly status: OperationResultStatus.Failed;

    readonly errors: readonly OperationError[];
}

/** @category RPC Block Data */
export interface SkippedOperationResult<TRpcData = object> extends EntityToIndex<TRpcData> {
    /** Usable for narrowing the type of this object from respective operation result union. */
    readonly status: OperationResultStatus.Skipped;
}
