import { keyof } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { BalanceUpdateConverter } from '../../../common/balance-update/converters/balance-update-converter';
import { primitives } from '../../../primitives';
import { RpcConvertOptions } from '../../../rpc-convert-options';
import { joinUid } from '../../../rpc-converter';
import { MainOperationWithResult } from '../../../rpc-data-interfaces';
import { InternalOperationConverter } from '../../internal-operation-converter';

export type TaquitoRpcMainOperationWithResult = MainOperationWithResult['rpcData'];

export type CommonMainOperationWithResult =
    Pick<MainOperationWithResult,
    | 'balanceUpdates' | 'counter' | 'fee' | 'gasLimit' | 'internalOperations' | 'isInternal' | 'sourceAddress' | 'storageLimit' | 'uid'>;

@singleton()
export class CommonMainOperationWithResultConverter {
    constructor(
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
        private readonly internalOperationConverter: InternalOperationConverter,
    ) {}

    convert(
        rpcData: TaquitoRpcMainOperationWithResult,
        uid: string,
        options: RpcConvertOptions,
    ): CommonMainOperationWithResult {
        const balanceUpdates = this.balanceUpdateConverter.convertFromMetadataProperty(rpcData, uid);
        const internalOperations = this.internalOperationConverter.convertArray(
            rpcData.metadata.internal_operation_results,
            joinUid(uid, keyof<typeof rpcData>('metadata'), keyof<typeof rpcData.metadata>('internal_operation_results')),
            options,
        );

        return {
            balanceUpdates,
            counter: primitives.bigInteger.convertProperty(rpcData, 'counter', uid, { min: 0 }),
            fee: primitives.bigInteger.convertProperty(rpcData, 'fee', uid, { min: 0 }),
            gasLimit: primitives.bigInteger.convertProperty(rpcData, 'gas_limit', uid, { min: 0 }),
            internalOperations,
            isInternal: false,
            sourceAddress: primitives.address.convertProperty(rpcData, 'source', uid),
            storageLimit: primitives.bigInteger.convertProperty(rpcData, 'storage_limit', uid, { min: 0 }),
            uid,
        };
    }
}
