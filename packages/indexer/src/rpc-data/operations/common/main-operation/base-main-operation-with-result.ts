import { BigNumber } from 'bignumber.js';

import { BalanceUpdate } from '../../../common/balance-update/balance-update';
import { EntityToIndex } from '../../../entity-to-index';
import { InternalOperation } from '../../internal-operation';
import { OperationKind } from '../../operation-kind';

export interface BaseMainOperationWithResult<TRpcData = object> extends EntityToIndex<TRpcData> {
    // Narrowing properties:

    /** Usable for narrowing the type of this object from the union with internal operations. */
    readonly kind: OperationKind;

    /** Usable for narrowing the type of this object from the union with internal operations. */
    readonly isInternal: false;

    // Specific properties:

    readonly balanceUpdates: readonly BalanceUpdate[];

    /** It is an integer greater than or equal to `0`. */
    readonly counter: BigNumber;

    /** It is mutez - greater than or equal to `0`. */
    readonly fee: BigNumber;

    /** It is an integer greater than or equal to `0`. */
    readonly gasLimit: BigNumber;

    readonly internalOperations: readonly InternalOperation[];

    readonly sourceAddress: string;

    /** It is an integer greater than or equal to `0`. */
    readonly storageLimit: BigNumber;
}
