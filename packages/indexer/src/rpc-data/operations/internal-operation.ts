import { DelegationInternalOperation } from './delegation/delegation-internal-operation';
import { EventInternalOperation } from './event/event-internal-operation';
import { OperationKind } from './operation-kind';
import { OriginationInternalOperation } from './origination/origination-internal-operation';
import { TransactionInternalOperation } from './transaction/transaction-internal-operation';

/** @category RPC Block Data */
export type InternalOperationKind =
    | OperationKind.Delegation
    | OperationKind.Event
    | OperationKind.Origination
    | OperationKind.Transaction;

/** @category RPC Block Data */
export type InternalOperation =
    | DelegationInternalOperation
    | EventInternalOperation
    | OriginationInternalOperation
    | TransactionInternalOperation;
