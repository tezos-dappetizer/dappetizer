import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { EntityToIndex } from '../../entity-to-index';
import { OperationKind } from '../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcBallotOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultBallot>;

/**
 * It enables delegates to participate in the Exploration and Promotion periods. Delegates use this operation to vote for ({@link Ballot.Yay}),
 * against ({@link Ballot.Nay}), or to side with the majority ({@link Ballot.Pass}), when examining a protocol amendment proposal.
 * @category RPC Block Data
 */
export interface BallotOperation extends EntityToIndex<TaquitoRpcBallotOperation> {
    // Narrowing properties:
    readonly kind: OperationKind.Ballot;

    // Specific properties:
    readonly sourceAddress: string;
    readonly period: number;
    readonly proposal: string;
    readonly ballot: Ballot;
}

/** @category RPC Block Data */
export enum Ballot {
    Nay = 'Nay',
    Pass = 'Pass',
    Yay = 'Yay',
}
