import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../common/balance-update/balance-update';
import { EntityToIndex } from '../../entity-to-index';
import { OperationKind } from '../operation-kind';

/**
 * @category RPC Block Data
 * @deprecated Endorsements are not supported anymore because they were incomplete and with breaking changes. Let us know if you need them.
 */
export type TaquitoRpcEndorsementOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultEndorsement>;

/**
 * @category RPC Block Data
 * @deprecated Endorsements are not supported anymore because they were incomplete and with breaking changes. Let us know if you need them.
 */
export type TaquitoRpcEndorsementWithSlotOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultEndorsementWithDal>;

/**
 * A vote for a candidate block for which a preendorsement quorum certificate (PQC) has been observed.
 * @category RPC Block Data
 * @deprecated Endorsements are not supported anymore because they were incomplete and with breaking changes. Let us know if you need them.
 */
// eslint-disable-next-line deprecation/deprecation
export interface EndorsementOperation extends EntityToIndex<TaquitoRpcEndorsementOperation | TaquitoRpcEndorsementWithSlotOperation> {
    // Narrowing properties:
    // eslint-disable-next-line deprecation/deprecation
    readonly kind: OperationKind.Endorsement;

    // Specific properties:
    readonly balanceUpdates: readonly BalanceUpdate[];
}
