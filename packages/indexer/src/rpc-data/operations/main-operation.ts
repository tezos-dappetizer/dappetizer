import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { ActivateAccountOperation } from './activate-account/activate-account-operation';
import { BallotOperation } from './ballot/ballot-operation';
import { DelegationOperation } from './delegation/delegation-operation';
import { DoubleBakingEvidenceOperation } from './double-baking-evidence/double-baking-evidence-operation';
import { DoubleEndorsementEvidenceOperation } from './double-endorsement-evidence/double-endorsement-evidence-operation';
import {
    DoublePreendorsementEvidenceOperation,
} from './double-preendorsement-evidence/double-preendorsement-evidence-operation';
import { DrainDelegateOperation } from './drain-delegate/drain-delegate-operation';
import { EndorsementOperation } from './endorsement/endorsement-operation';
import { IncreasePaidStorageOperation } from './increase-paid-storage/increase-paid-storage-operation';
import { OriginationOperation } from './origination/origination-operation';
import { PreendorsementOperation } from './preendorsement/preendorsement-operation';
import { ProposalsOperation } from './proposals/proposals-operation';
import { RegisterGlobalConstantOperation } from './register-global-constant/register-global-constant-operation';
import { RevealOperation } from './reveal/reveal-operation';
import { SeedNonceRevelationOperation } from './seed-nonce-revelation/seed-nonce-revelation-operation';
import { SetDepositsLimitOperation } from './set-deposits-limit/set-deposits-limit-operation';
import { SmartRollupAddMessagesOperation } from './smart-rollup/add-messages/smart-rollup-add-messages-operation';
import { SmartRollupCementOperation } from './smart-rollup/cement/smart-rollup-cement-operation';
import {
    SmartRollupExecuteOutboxMessageOperation,
} from './smart-rollup/execute-outbox-message/smart-rollup-execute-outbox-message-operation';
import { SmartRollupOriginateOperation } from './smart-rollup/originate/smart-rollup-originate-operation';
import { SmartRollupPublishOperation } from './smart-rollup/publish/smart-rollup-publish-operation';
import { SmartRollupRecoverBondOperation } from './smart-rollup/recover-bond/smart-rollup-recover-bond-operation';
import { SmartRollupRefuteOperation } from './smart-rollup/refute/smart-rollup-refute-operation';
import { SmartRollupTimeoutOperation } from './smart-rollup/timeout/smart-rollup-timeout-operation';
import { TransactionOperation } from './transaction/transaction-operation';
import { TransferTicketOperation } from './transfer-ticket/transfer-ticket-operation';
import { UpdateConsensusKeyOperation } from './update-consensus-key/update-consensus-key-operation';
import { VdfRevelationOperation } from './vdf-revelation/vdf-revelation-operation';

/** @category RPC Block Data */
export type TaquitoRpcOperation = DeepReadonly<taquitoRpc.OperationContentsAndResult>;

/**
 * The main (top-level, directly in operation groups) operation.
 * It is the discriminated union which can be narrowed by `kind`.
 * @category RPC Block Data
 */
export type MainOperation =
    | ActivateAccountOperation
    | BallotOperation
    | DelegationOperation
    | DoubleBakingEvidenceOperation
    // eslint-disable-next-line deprecation/deprecation
    | DoubleEndorsementEvidenceOperation
    // eslint-disable-next-line deprecation/deprecation
    | DoublePreendorsementEvidenceOperation
    | DrainDelegateOperation
    // eslint-disable-next-line deprecation/deprecation
    | EndorsementOperation
    | IncreasePaidStorageOperation
    | OriginationOperation
    | PreendorsementOperation
    | ProposalsOperation
    | RegisterGlobalConstantOperation
    | RevealOperation
    | SeedNonceRevelationOperation
    | SetDepositsLimitOperation
    | SmartRollupAddMessagesOperation
    | SmartRollupCementOperation
    | SmartRollupExecuteOutboxMessageOperation
    | SmartRollupOriginateOperation
    | SmartRollupPublishOperation
    | SmartRollupRecoverBondOperation
    | SmartRollupRefuteOperation
    | SmartRollupTimeoutOperation
    | TransactionOperation
    | TransferTicketOperation
    | UpdateConsensusKeyOperation
    | VdfRevelationOperation;
