import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../common/balance-update/balance-update';
import { EntityToIndex } from '../../entity-to-index';
import { OperationKind } from '../operation-kind';

/**
 * @category RPC Block Data
 * @deprecated Endorsements are not supported anymore because they were incomplete and with breaking changes. Let us know if you need them.
 */
export type TaquitoRpcDoubleEndorsementEvidenceOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultDoubleEndorsement>;

/**
 * It allows for accusing a delegate of having *double-endorsed* – i.e., of having endorsed two different block candidates
 * at the same level and the same round – by providing the two offending endorsements.
 * @category RPC Block Data
 * @deprecated Endorsements are not supported anymore because they were incomplete and with breaking changes. Let us know if you need them.
 */
// eslint-disable-next-line deprecation/deprecation
export interface DoubleEndorsementEvidenceOperation extends EntityToIndex<TaquitoRpcDoubleEndorsementEvidenceOperation> {
    // Narrowing properties:
    // eslint-disable-next-line deprecation/deprecation
    readonly kind: OperationKind.DoubleEndorsementEvidence;

    // Specific properties:
    readonly balanceUpdates: readonly BalanceUpdate[];
}
