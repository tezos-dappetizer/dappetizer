import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { EntityToIndex } from '../../entity-to-index';
import { OperationKind } from '../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcProposalsOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultProposals>;

/**
 * It enables delegates to submit (also known as to *inject*) protocol amendment proposals,
 * or to up-vote previously submitted proposals, during the Proposal period.
 * @category RPC Block Data
 */
export interface ProposalsOperation extends EntityToIndex<TaquitoRpcProposalsOperation> {
    // Narrowing properties:
    readonly kind: OperationKind.Proposals;

    // Specific properties:
    readonly sourceAddress: string;
    readonly period: number;
    readonly proposals: readonly string[];
}
