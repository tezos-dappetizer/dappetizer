import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../common/balance-update/balance-update';
import { LazyBigMapDiff } from '../../common/big-map-diff/big-map-diff';
import { LazyStorageChange } from '../../common/storage-change/storage-change';
import { TicketUpdate } from '../../common/ticket/ticket-update';
import { AppliedOperationResult, OperationResult } from '../common/operation-result/operation-results';

/** @category RPC Block Data */
export type TaquitoRpcTransactionResult = DeepReadonly<taquitoRpc.OperationResultTransaction>;

/** @category RPC Block Data */
export type TransactionResult = OperationResult<AppliedTransactionResult>;

/** @category RPC Block Data */
export interface AppliedTransactionResult extends AppliedOperationResult<TaquitoRpcTransactionResult> {
    /** It can be `true` only if the transaction goes to a contract (`KT1` address). */
    readonly allocatedDestinationContract: boolean;

    readonly balanceUpdates: readonly BalanceUpdate[];

    /** Exists (contains items) if the transaction goes to a contract (`KT1` address). */
    readonly bigMapDiffs: readonly LazyBigMapDiff[];

    readonly paidStorageSizeDiff: BigNumber | null;

    /** Exists if the transaction goes to a contract (`KT1` address). */
    readonly storageChange: LazyStorageChange | null;

    /** Exists if the transaction goes to a contract (`KT1` address). */
    readonly storageSize: BigNumber | null;

    /** Exists if the transaction goes to a rollup `txr1` address. */
    readonly ticketHash: string | null;

    readonly ticketReceipts: readonly TicketUpdate[];
    readonly ticketUpdates: readonly TicketUpdate[];
}
