import { cacheFuncResult, dappetizerAssert, isObject } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { TransactionParameterFactory } from './transaction-parameter-factory';
import { primitives } from '../../../primitives';
import { RpcObjectConverter } from '../../../rpc-converter';
import {
    LazyContract,
    LazyTransactionParameter,
    TaquitoRpcTransactionParameter,
    TransactionResult,
} from '../../../rpc-data-interfaces';
import { OperationResultStatus } from '../../common/operation-result/operation-result-status';

export interface LazyTransactionConvertOptions {
    readonly lazyContract: LazyContract | null;
    readonly transactionResult: TransactionResult;
}

@singleton()
export class LazyTransactionParameterConverter
    extends RpcObjectConverter<TaquitoRpcTransactionParameter, LazyTransactionParameter | null, LazyTransactionConvertOptions> {
    constructor(
        private readonly transactionParameterFactory: TransactionParameterFactory,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcTransactionParameter,
        uid: string,
        options: LazyTransactionConvertOptions,
    ): LazyTransactionParameter | null {
        const lazyContract = options.lazyContract;
        if (!lazyContract) {
            return null;
        }

        if (hasUnparsedBinary(rpcData)) {
            const isApplied = options.transactionResult.status === OperationResultStatus.Applied;
            dappetizerAssert(!isApplied, `There cannot be '${UNPARSED_BINARY}' if transaction is applied.`, { uid, rpcData });
            return null;
        }

        primitives.nonWhiteSpaceString.convertProperty(rpcData, 'entrypoint', uid);
        primitives.michelson.convertProperty(rpcData, 'value', uid);

        return Object.freeze<LazyTransactionParameter>({
            rpcData,
            uid,

            getTransactionParameter: cacheFuncResult(async () => {
                const contract = await lazyContract.getContract();
                return this.transactionParameterFactory.create(rpcData, uid, contract);
            }),
        });
    }
}

export const UNPARSED_BINARY = 'unparsed-binary';

function hasUnparsedBinary(rpcData: TaquitoRpcTransactionParameter): boolean {
    return isObject(rpcData.value)
        && UNPARSED_BINARY in rpcData.value
        && typeof rpcData.value[UNPARSED_BINARY] === 'string';
}
