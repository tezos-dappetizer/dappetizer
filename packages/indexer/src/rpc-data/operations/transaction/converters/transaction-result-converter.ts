import { dumpToString } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { BalanceUpdateConverter } from '../../../common/balance-update/converters/balance-update-converter';
import { BigMapDiffConverter } from '../../../common/big-map-diff/converters/big-map-diff-converter';
import { OperationErrorConverter } from '../../../common/operation-error/operation-error-converter';
import { LazyStorageChangeConverter } from '../../../common/storage-change/lazy-storage-change-converter';
import { TicketUpdatesConverter } from '../../../common/ticket/converters/ticket-updates-converter';
import { primitives } from '../../../primitives';
import { RpcConversionError } from '../../../rpc-conversion-error';
import { RpcConvertOptionsWithLazyContract } from '../../../rpc-convert-options';
import { AppliedTransactionResult, TaquitoRpcTransactionResult } from '../../../rpc-data-interfaces';
import {
    BaseOperationResultConverter,
    SpecificResultParts,
} from '../../common/operation-result/base-operation-result-converter';

export type TransactionResultParts = SpecificResultParts<AppliedTransactionResult>;

@singleton()
export class TransactionResultConverter
    extends BaseOperationResultConverter<TaquitoRpcTransactionResult, TransactionResultParts, RpcConvertOptionsWithLazyContract> {
    constructor(
        operationErrorConverter: OperationErrorConverter,
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
        private readonly bigMapDiffConverter: BigMapDiffConverter,
        private readonly storageChangeConverter: LazyStorageChangeConverter,
        private readonly ticketUpdatesConverter: TicketUpdatesConverter,
    ) {
        super(operationErrorConverter);
    }

    protected override convertAppliedResultParts(
        rpcData: TaquitoRpcTransactionResult,
        uid: string,
        options: RpcConvertOptionsWithLazyContract,
    ): TransactionResultParts {
        guardNoOriginatedContract(rpcData, uid);
        const balanceUpdates = this.balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', uid);
        const bigMapDiffs = this.bigMapDiffConverter.convertArrayFrom(rpcData, uid, options);
        const storageChange = this.storageChangeConverter.convertNullableProperty(rpcData, 'storage', uid, options);
        const ticketReceipts = this.ticketUpdatesConverter.convertArrayProperty(rpcData, 'ticket_receipt', uid);
        const ticketUpdates = this.ticketUpdatesConverter.convertArrayProperty(rpcData, 'ticket_updates', uid);

        return {
            allocatedDestinationContract: primitives.boolean.convertNullableProperty(rpcData, 'allocated_destination_contract', uid) ?? false,
            balanceUpdates,
            bigMapDiffs,
            paidStorageSizeDiff: primitives.bigInteger.convertNullableProperty(rpcData, 'paid_storage_size_diff', uid),
            storageChange,
            storageSize: primitives.bigInteger.convertNullableProperty(rpcData, 'storage_size', uid),
            ticketHash: primitives.nonWhiteSpaceString.convertNullableProperty(rpcData, 'ticket_hash', uid),
            ticketReceipts,
            ticketUpdates,
        };
    }
}

function guardNoOriginatedContract(
    rpcData: TaquitoRpcTransactionResult,
    uid: string,
): void {
    const originatedContracts = primitives.address.convertArrayProperty(rpcData, 'originated_contracts', uid);
    if (originatedContracts.length > 0) {
        throw new RpcConversionError(uid, `Transaction cannot contain any 'originated_contracts'`
            + ` but there are: ${dumpToString(originatedContracts)}.`);
    }
}
