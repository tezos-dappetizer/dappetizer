import { errorToString, hasLength, last } from '@tezos-dappetizer/utils';
import { trimStart } from 'lodash';
import { NonEmptyArray } from 'ts-essentials';
import { singleton } from 'tsyringe';

import { Contract } from '../../../../contracts/contract';
import { michelsonDataPrims, michelsonTypePrims } from '../../../../michelson/michelson-prims';
import { MichelsonSchema } from '../../../../michelson/michelson-schema';
import { isPrim } from '../../../../michelson/michelson-utils';
import { LazyMichelsonValueFactory } from '../../../common/michelson/lazy-michelson-value-factory';
import {
    Michelson,
    MichelsonPrim,
    TaquitoRpcTransactionParameter,
    TransactionParameter,
} from '../../../rpc-data-interfaces';

export const DEFAULT = 'default';

@singleton()
export class TransactionParameterFactory {
    constructor(
        private readonly lazyValueFactory: LazyMichelsonValueFactory,
    ) {}

    create(
        rpcData: TaquitoRpcTransactionParameter,
        uid: string,
        contract: Contract,
    ): TransactionParameter {
        const extracted = new NestedEntrypointExtractor(contract).extract(rpcData);

        return Object.freeze<TransactionParameter>({
            rpcData,
            entrypoint: last(extracted.entrypointPath),
            entrypointPath: Object.freeze<NonEmptyArray<string>>(extracted.entrypointPath),
            uid,
            value: this.lazyValueFactory.create(extracted.valueMichelson, extracted.schema),
        });
    }
}

interface ExtractedEntrypoint {
    readonly entrypointPath: NonEmptyArray<string>;
    readonly valueMichelson: Michelson;
    readonly schema: MichelsonSchema;
}

class NestedEntrypointExtractor {
    constructor(readonly contract: Contract) {}

    extract(rpcData: TaquitoRpcTransactionParameter): ExtractedEntrypoint {
        try {
            const topEntrypoint = this.extractExplicitFromParameter(rpcData);
            const nestedEntrypoint = this.extractNested(topEntrypoint.valueMichelson, topEntrypoint.schema.michelson);

            return nestedEntrypoint ? this.mergeEntrypoints(topEntrypoint, nestedEntrypoint) : topEntrypoint;
        } catch (error) {
            throw new Error(`Bug: failed to create transaction parameter for contract '${this.contract.address}' from (valid) RPC data.`
                + ` Known entrypoints: ${JSON.stringify(Object.keys(this.contract.parameterSchemas.entrypoints))}.`
                + ` RPC data: ${JSON.stringify(rpcData)}.`
                + ` ${errorToString(error)}`);
        }
    }

    private extractExplicitFromParameter(rpcData: TaquitoRpcTransactionParameter): ExtractedEntrypoint {
        const namedSchema = this.contract.parameterSchemas.entrypoints[rpcData.entrypoint];
        if (namedSchema) {
            return {
                entrypointPath: [rpcData.entrypoint],
                valueMichelson: rpcData.value,
                schema: namedSchema,
            };
        }

        if (rpcData.entrypoint === DEFAULT) {
            return {
                entrypointPath: [DEFAULT],
                valueMichelson: rpcData.value,
                schema: this.contract.parameterSchemas.default,
            };
        }

        throw new Error(`RPC data has entrypoint property '${rpcData.entrypoint}' which is unknown entrypoint of the contract.`);
    }

    private extractNested(valueMichelson: Michelson, schemaMichelson: Michelson): ExtractedEntrypoint | null {
        if (!isPrim(schemaMichelson)) {
            return null;
        }

        const currentEntrypoint = this.fromCurrentMichelson(valueMichelson, schemaMichelson);
        const deepEntrypoint = this.fromDeepMichelson(valueMichelson, schemaMichelson);

        return currentEntrypoint && deepEntrypoint
            ? this.mergeEntrypoints(currentEntrypoint, deepEntrypoint)
            : deepEntrypoint ?? currentEntrypoint;
    }

    private fromCurrentMichelson(valueMichelson: Michelson, schemaMichelson: MichelsonPrim): ExtractedEntrypoint | null {
        if (!hasLength(schemaMichelson.annots, 1)) {
            return null;
        }

        const annots = trimStart(schemaMichelson.annots[0], '%:');
        const entrypointSchema = this.contract.parameterSchemas.entrypoints[annots];

        return entrypointSchema
            ? {
                entrypointPath: [annots],
                valueMichelson,
                schema: entrypointSchema,
            }
            : null;
    }

    private fromDeepMichelson(valueMichelson: Michelson, schemaMichelson: MichelsonPrim): ExtractedEntrypoint | null {
        if (schemaMichelson.prim !== michelsonTypePrims.OR) {
            return null;
        }
        if (!hasLength(schemaMichelson.args, 2)) {
            throw new Error(`Expected 2 args in schema michelson: ${JSON.stringify(schemaMichelson)}.`);
        }
        if (!isPrim(valueMichelson)) {
            throw new Error(`Expected prim in value michelson but there is: ${JSON.stringify(valueMichelson)}.`);
        }
        if (!hasLength(valueMichelson.args, 1)) {
            throw new Error(`Expected 1 args in value michelson: ${JSON.stringify(valueMichelson)}.`);
        }

        switch (valueMichelson.prim) {
            case michelsonDataPrims.LEFT:
                return this.extractNested(valueMichelson.args[0], schemaMichelson.args[0]);
            case michelsonDataPrims.RIGHT:
                return this.extractNested(valueMichelson.args[0], schemaMichelson.args[1]);
            default:
                throw new Error(`Expected '${michelsonDataPrims.LEFT}' or '${michelsonDataPrims.RIGHT}' in prim`
                    + ` of value michelson: ${JSON.stringify(valueMichelson)}.`);
        }
    }

    private mergeEntrypoints(parent: ExtractedEntrypoint, child: ExtractedEntrypoint): ExtractedEntrypoint {
        return {
            entrypointPath: [...parent.entrypointPath, ...child.entrypointPath],
            schema: child.schema,
            valueMichelson: child.valueMichelson,
        };
    }
}
