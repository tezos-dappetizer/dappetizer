import * as taquitoRpc from '@taquito/rpc';
import { AddressPrefix, keyof, typed } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { CommonTransactionOperationConverter } from './common-transaction-operation-converter';
import { RpcConvertOptions } from '../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../rpc-converter';
import { TaquitoRpcInternalOperation, TransactionInternalOperation } from '../../../rpc-data-interfaces';
import {
    CommonInternalOperationConverter,
    guardKind,
} from '../../common/internal-operation/common-internal-operation-converter';
import { SmartRollupConverter } from '../../smart-rollup/common/converters/smart-rollup-converter';

@singleton()
export class TransactionInternalOperationConverter
    extends RpcObjectConverter<TaquitoRpcInternalOperation, TransactionInternalOperation, RpcConvertOptions> {
    constructor(
        private readonly commonInternalOperationConverter: CommonInternalOperationConverter,
        private readonly commonTransactionOperationConverter: CommonTransactionOperationConverter,
        private readonly smartRollupConverter: SmartRollupConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcInternalOperation,
        uid: string,
        options: RpcConvertOptions,
    ): TransactionInternalOperation {
        guardKind(rpcData, taquitoRpc.OpKind.TRANSACTION, uid);

        const commonProperties = this.commonInternalOperationConverter.convert(rpcData, uid);
        const specificProperties = this.commonTransactionOperationConverter.convert(rpcData, uid, {
            ...options,
            rpcResult: rpcData.result,
            rpcResultProperty: [keyof<typeof rpcData>('result')],
        });
        const destination = specificProperties.destination.address.startsWith(typed<AddressPrefix>('sr1'))
            ? this.smartRollupConverter.convertProperty(rpcData, 'destination', uid)
            : specificProperties.destination;

        return Object.freeze<TransactionInternalOperation>({ ...commonProperties, ...specificProperties, destination });
    }
}
