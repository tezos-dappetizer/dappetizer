import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { RevealResult } from './reveal-result';
import { BaseMainOperationWithResult } from '../common/main-operation/base-main-operation-with-result';
import { OperationKind } from '../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcRevealOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultReveal>;

/**
 * It reveals the public key of the sending manager.
 * Knowing this public key is indeed necessary to check the signature of future operations signed by this manager.
 * @category RPC Block Data
 */
export interface RevealOperation extends BaseMainOperationWithResult<TaquitoRpcRevealOperation> {
    readonly kind: OperationKind.Reveal;

    // Specific properties:

    readonly publicKey: string;
    readonly result: RevealResult;
}
