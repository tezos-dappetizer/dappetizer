import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { AppliedOperationResult, OperationResult } from '../common/operation-result/operation-results';

/** @category RPC Block Data */
export type TaquitoRpcRevealResult = DeepReadonly<taquitoRpc.OperationResultReveal>;

/** @category RPC Block Data */
export type RevealResult = OperationResult<AppliedRevealResult>;

/** @category RPC Block Data */
export interface AppliedRevealResult extends AppliedOperationResult<TaquitoRpcRevealResult> {}
