import { singleton } from 'tsyringe';

import { TaquitoRpcRevealResult } from '../../../rpc-data-interfaces';
import { SimpleOperationResultConverter } from '../../common/operation-result/simple-operation-result-converter';

@singleton()
export class RevealResultConverter extends SimpleOperationResultConverter<TaquitoRpcRevealResult> {}
