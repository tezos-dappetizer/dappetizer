import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { UpdateConsensusKeyResult } from './update-consensus-key-result';
import { BaseMainOperationWithResult } from '../common/main-operation/base-main-operation-with-result';
import { OperationKind } from '../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcUpdateConsensusKeyOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultUpdateConsensusKey>;

/**
 * It allows users to delegate the responsibility of signing blocks and consensus-related operations to another account.
 * Note that consensus keys cannot be BLS public keys.
 * @category RPC Block Data
 */
export interface UpdateConsensusKeyOperation extends BaseMainOperationWithResult<TaquitoRpcUpdateConsensusKeyOperation> {
    // Narrowing properties:
    readonly kind: OperationKind.UpdateConsensusKey;

    // Specific properties:
    readonly pk: string;
    readonly result: UpdateConsensusKeyResult;
}
