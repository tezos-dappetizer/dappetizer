import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { AppliedOperationResult, OperationResult } from '../common/operation-result/operation-results';

/** @category RPC Block Data */
export type TaquitoRpcUpdateConsensusKeyResult = DeepReadonly<taquitoRpc.OperationResultUpdateConsensusKey>;

/** @category RPC Block Data */
export type UpdateConsensusKeyResult = OperationResult<AppliedUpdateConsensusKeyResult>;

/** @category RPC Block Data */
export interface AppliedUpdateConsensusKeyResult extends AppliedOperationResult<TaquitoRpcUpdateConsensusKeyResult> {}
