import { singleton } from 'tsyringe';

import { UpdateConsensusKeyResultConverter } from './update-consensus-key-result-converter';
import { primitives } from '../../../primitives';
import { RpcConvertOptions } from '../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../rpc-converter';
import {
    OperationKind,
    TaquitoRpcUpdateConsensusKeyOperation,
    UpdateConsensusKeyOperation,
} from '../../../rpc-data-interfaces';
import {
    CommonMainOperationWithResultConverter,
} from '../../common/main-operation/common-main-operation-with-result-converter';

@singleton()
export class UpdateConsensusKeyOperationConverter
    extends RpcObjectConverter<TaquitoRpcUpdateConsensusKeyOperation, UpdateConsensusKeyOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonMainOperationWithResultConverter,
        private readonly resultConverter: UpdateConsensusKeyResultConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcUpdateConsensusKeyOperation,
        uid: string,
        options: RpcConvertOptions,
    ): UpdateConsensusKeyOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const result = this.resultConverter.convertFromMetadataOperationResult(rpcData, uid);

        return Object.freeze<UpdateConsensusKeyOperation>({
            ...commonProperties,
            kind: OperationKind.UpdateConsensusKey,
            pk: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'pk', uid),
            result,
            rpcData,
        });
    }
}
