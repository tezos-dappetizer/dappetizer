import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../common/balance-update/balance-update';
import { AppliedOperationResult, OperationResult } from '../common/operation-result/operation-results';

/** @category RPC Block Data */
export type TaquitoRpcIncreasePaidStorageResult = DeepReadonly<taquitoRpc.OperationResultIncreasePaidStorage>;

/** @category RPC Block Data */
export type IncreasePaidStorageResult = OperationResult<AppliedIncreasePaidStorageResult>;

/** @category RPC Block Data */
export interface AppliedIncreasePaidStorageResult extends AppliedOperationResult<TaquitoRpcIncreasePaidStorageResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
}
