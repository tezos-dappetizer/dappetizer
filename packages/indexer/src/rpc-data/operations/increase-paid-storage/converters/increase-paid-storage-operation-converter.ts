import { singleton } from 'tsyringe';

import { IncreasePaidStorageResultConverter } from './increase-paid-storage-result-converter';
import { primitives } from '../../../primitives';
import { RpcConvertOptions } from '../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../rpc-converter';
import {
    IncreasePaidStorageOperation,
    OperationKind,
    TaquitoRpcIncreasePaidStorageOperation,
} from '../../../rpc-data-interfaces';
import {
    CommonMainOperationWithResultConverter,
} from '../../common/main-operation/common-main-operation-with-result-converter';

@singleton()
export class IncreasePaidStorageOperationConverter
    extends RpcObjectConverter<TaquitoRpcIncreasePaidStorageOperation, IncreasePaidStorageOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonMainOperationWithResultConverter,
        private readonly resultConverter: IncreasePaidStorageResultConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcIncreasePaidStorageOperation,
        uid: string,
        options: RpcConvertOptions,
    ): IncreasePaidStorageOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const result = this.resultConverter.convertFromMetadataOperationResult(rpcData, uid);

        return Object.freeze<IncreasePaidStorageOperation>({
            ...commonProperties,
            amount: primitives.bigInteger.convertProperty(rpcData, 'amount', uid),
            destinationAddress: primitives.address.convertProperty(rpcData, 'destination', uid),
            kind: OperationKind.IncreasePaidStorage,
            result,
            rpcData,
        });
    }
}
