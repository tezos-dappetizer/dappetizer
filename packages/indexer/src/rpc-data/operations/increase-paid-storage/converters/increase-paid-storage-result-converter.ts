import { singleton } from 'tsyringe';

import { TaquitoRpcIncreasePaidStorageResult } from '../../../rpc-data-interfaces';
import {
    OperationResultWithBalanceUpdatesConverter,
} from '../../common/operation-result/operation-result-with-balance-updates-converter';

@singleton()
export class IncreasePaidStorageResultConverter extends OperationResultWithBalanceUpdatesConverter<TaquitoRpcIncreasePaidStorageResult> {}
