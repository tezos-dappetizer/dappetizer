import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';

import { IncreasePaidStorageResult } from './increase-paid-storage-result';
import { BaseMainOperationWithResult } from '../common/main-operation/base-main-operation-with-result';
import { OperationKind } from '../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcIncreasePaidStorageOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultIncreasePaidStorage>;

/**
 * It allows a sender to increase the paid storage of some previously deployed contract.
 * @category RPC Block Data
 */
export interface IncreasePaidStorageOperation extends BaseMainOperationWithResult<TaquitoRpcIncreasePaidStorageOperation> {
    readonly kind: OperationKind.IncreasePaidStorage;

    // Specific properties:
    readonly amount: BigNumber;
    readonly destinationAddress: string;
    readonly result: IncreasePaidStorageResult;
}
