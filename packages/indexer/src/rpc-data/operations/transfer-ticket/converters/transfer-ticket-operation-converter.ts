import { singleton } from 'tsyringe';

import { TransferTicketResultConverter } from './transfer-ticket-result-converter';
import { TicketTokenConverter } from '../../../common/ticket/converters/ticket-token-converter';
import { primitives } from '../../../primitives';
import { RpcConvertOptions } from '../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../rpc-converter';
import { OperationKind, TaquitoRpcTransferTicketOperation, TransferTicketOperation } from '../../../rpc-data-interfaces';
import {
    CommonMainOperationWithResultConverter,
} from '../../common/main-operation/common-main-operation-with-result-converter';

@singleton()
export class TransferTicketOperationConverter
    extends RpcObjectConverter<TaquitoRpcTransferTicketOperation, TransferTicketOperation, RpcConvertOptions> {
    constructor(
        private readonly commonOperationConverter: CommonMainOperationWithResultConverter,
        private readonly resultConverter: TransferTicketResultConverter,
        private readonly ticketTokenConverter: TicketTokenConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcTransferTicketOperation,
        uid: string,
        options: RpcConvertOptions,
    ): TransferTicketOperation {
        const commonProperties = this.commonOperationConverter.convert(rpcData, uid, options);
        const result = this.resultConverter.convertFromMetadataOperationResult(rpcData, uid);
        const ticketToken = this.ticketTokenConverter.convertFromProperties(rpcData, uid, {
            ticketer: 'ticket_ticketer',
            content: 'ticket_contents',
            contentType: 'ticket_ty',
        });
        const ticketAmount = primitives.bigInteger.convertProperty(rpcData, 'ticket_amount', uid, { min: 1 });

        return Object.freeze<TransferTicketOperation>({
            ...commonProperties,
            destinationAddress: primitives.address.convertProperty(rpcData, 'destination', uid),
            entrypoint: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'entrypoint', uid),
            kind: OperationKind.TransferTicket,
            result,
            rpcData,
            ticket: Object.freeze({
                amount: ticketAmount,
                contents: ticketToken.content,
                ticketerAddress: ticketToken.ticketerAddress,
            }),
            ticketAmount,
            ticketToken,
        });
    }
}
