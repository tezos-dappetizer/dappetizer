import { singleton } from 'tsyringe';

import { BalanceUpdateConverter } from '../../../common/balance-update/converters/balance-update-converter';
import { OperationErrorConverter } from '../../../common/operation-error/operation-error-converter';
import { TicketUpdatesConverter } from '../../../common/ticket/converters/ticket-updates-converter';
import { primitives } from '../../../primitives';
import { AppliedTransferTicketResult, TaquitoRpcTransferTicketResult } from '../../../rpc-data-interfaces';
import { BaseOperationResultConverter, SpecificResultParts } from '../../common/operation-result/base-operation-result-converter';

export type TransferTicketResultParts = SpecificResultParts<AppliedTransferTicketResult>;

@singleton()
export class TransferTicketResultConverter extends BaseOperationResultConverter<TaquitoRpcTransferTicketResult, TransferTicketResultParts> {
    constructor(
        operationErrorConverter: OperationErrorConverter,
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
        private readonly ticketUpdatesConverter: TicketUpdatesConverter,
    ) {
        super(operationErrorConverter);
    }

    protected override convertAppliedResultParts(
        rpcData: TaquitoRpcTransferTicketResult,
        uid: string,
    ): TransferTicketResultParts {
        const balanceUpdates = this.balanceUpdateConverter.convertArrayProperty(rpcData, 'balance_updates', uid);
        const ticketUpdates = this.ticketUpdatesConverter.convertArrayProperty(rpcData, 'ticket_updates', uid);

        return {
            balanceUpdates,
            paidStorageSizeDiff: primitives.bigInteger.convertNullableProperty(rpcData, 'paid_storage_size_diff', uid),
            ticketUpdates,
        };
    }
}
