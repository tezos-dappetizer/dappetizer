import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';

import { TransferTicketResult } from './transfer-ticket-result';
import { Ticket } from '../../common/ticket/ticket';
import { TicketToken } from '../../common/ticket/ticket-token';
import { BaseMainOperationWithResult } from '../common/main-operation/base-main-operation-with-result';
import { OperationKind } from '../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcTransferTicketOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultTransferTicket>;

/** @category RPC Block Data */
export interface TransferTicketOperation extends BaseMainOperationWithResult<TaquitoRpcTransferTicketOperation> {
    // Narrowing properties:
    readonly kind: OperationKind.TransferTicket;

    // Specific properties:
    readonly destinationAddress: string;

    readonly entrypoint: string;
    readonly result: TransferTicketResult;

    /** @deprecated Use `ticketToken` and `ticketAmount` instead. */
    readonly ticket: Ticket; // eslint-disable-line deprecation/deprecation

    /** It is an integer greater than or equal to `1`. */
    readonly ticketAmount: BigNumber;

    readonly ticketToken: TicketToken;
}
