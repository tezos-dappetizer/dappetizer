import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../common/balance-update/balance-update';
import { TicketUpdate } from '../../common/ticket/ticket-update';
import { AppliedOperationResult, OperationResult } from '../common/operation-result/operation-results';

/** @category RPC Block Data */
export type TaquitoRpcTransferTicketResult = DeepReadonly<taquitoRpc.OperationResultTransferTicket>;

/** @category RPC Block Data */
export type TransferTicketResult = OperationResult<AppliedTransferTicketResult>;

/** @category RPC Block Data */
export interface AppliedTransferTicketResult extends AppliedOperationResult<TaquitoRpcTransferTicketResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly paidStorageSizeDiff: BigNumber | null;
    readonly ticketUpdates: readonly TicketUpdate[];
}
