import { singleton } from 'tsyringe';

import { BalanceUpdateConverter } from '../../common/balance-update/converters/balance-update-converter';
import { primitives } from '../../primitives';
import { RpcConvertOptions } from '../../rpc-convert-options';
import { RpcObjectConverter } from '../../rpc-converter';
import { ActivateAccountOperation, OperationKind, TaquitoRpcActivateAccountOperation } from '../../rpc-data-interfaces';

@singleton()
export class ActivateAccountOperationConverter
    extends RpcObjectConverter<TaquitoRpcActivateAccountOperation, ActivateAccountOperation, RpcConvertOptions> {
    constructor(
        private readonly balanceUpdateConverter: BalanceUpdateConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcActivateAccountOperation,
        uid: string,
        _options: RpcConvertOptions,
    ): ActivateAccountOperation {
        const balanceUpdates = this.balanceUpdateConverter.convertFromMetadataProperty(rpcData, uid);

        return Object.freeze<ActivateAccountOperation>({
            balanceUpdates,
            kind: OperationKind.ActivateAccount,
            pkh: primitives.nonWhiteSpaceString.convertProperty(rpcData, 'pkh', uid),
            rpcData,
            secret: primitives.string.convertProperty(rpcData, 'secret', uid),
            uid,
        });
    }
}
