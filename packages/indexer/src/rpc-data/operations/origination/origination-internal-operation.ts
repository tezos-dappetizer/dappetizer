// Separate file from main operation b/c it refers internal operations -> circle.
import { BigNumber } from 'bignumber.js';

import { OriginationResult } from './origination-result';
import { BaseInternalOperation } from '../common/internal-operation/base-internal-operation';
import { OperationKind } from '../operation-kind';

/**
 * {@link OriginationOperation} executed as an internal operation.
 * @category RPC Block Data
 */
export interface OriginationInternalOperation extends BaseInternalOperation {
    /** Usable for narrowing the type of this object from {@link InternalOperation}. */
    readonly kind: OperationKind.Origination;

    // Common origination properties:

    /** It is mutez - greater than or equal to `0`. */
    readonly balance: BigNumber;

    readonly delegateAddress: string | null;
    readonly result: OriginationResult;
}
