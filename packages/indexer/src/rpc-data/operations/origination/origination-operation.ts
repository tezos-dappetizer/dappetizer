import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';

import { OriginationResult } from './origination-result';
import { BaseMainOperationWithResult } from '../common/main-operation/base-main-operation-with-result';
import { OperationKind } from '../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcOriginationOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultOrigination>;

/**
 * It is used to {@link https://tezos.gitlab.io/alpha/glossary.html#origination | originate},
 * that is to deploy, smart contracts in the Tezos blockchain.
 * @category RPC Block Data
 */
export interface OriginationOperation extends BaseMainOperationWithResult<TaquitoRpcOriginationOperation> {
    readonly kind: OperationKind.Origination;

    // Common origination properties:

    /** It is mutez - greater than or equal to `0`. */
    readonly balance: BigNumber;

    readonly delegateAddress: string | null;
    readonly result: OriginationResult;
}
