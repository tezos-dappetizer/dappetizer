import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../common/balance-update/balance-update';
import { LazyContract } from '../../common/lazy-contract/lazy-contract';
import { LazyBigMapDiff, LazyMichelsonValue } from '../../rpc-data-interfaces';
import { AppliedOperationResult, OperationResult } from '../common/operation-result/operation-results';

/** @category RPC Block Data */
export type TaquitoRpcOriginationResult = DeepReadonly<taquitoRpc.OperationResultOrigination>;

/** @category RPC Block Data */
export type OriginationResult = OperationResult<AppliedOriginationResult>;

/** @category RPC Block Data */
export interface AppliedOriginationResult extends AppliedOperationResult<TaquitoRpcOriginationResult> {
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly bigMapDiffs: readonly LazyBigMapDiff[];
    readonly originatedContract: LazyContract;
    readonly storageSize: BigNumber | null;
    readonly paidStorageSizeDiff: BigNumber | null;

    getInitialStorage(): Promise<LazyMichelsonValue>;
}
