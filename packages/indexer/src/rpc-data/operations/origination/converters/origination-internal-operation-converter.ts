import * as taquitoRpc from '@taquito/rpc';
import { keyof } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { CommonOriginationOperationConverter } from './common-origination-operation-converter';
import { RpcConvertOptions } from '../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../rpc-converter';
import { OriginationInternalOperation, TaquitoRpcInternalOperation } from '../../../rpc-data-interfaces';
import {
    CommonInternalOperationConverter,
    guardKind,
} from '../../common/internal-operation/common-internal-operation-converter';

@singleton()
export class OriginationInternalOperationConverter
    extends RpcObjectConverter<TaquitoRpcInternalOperation, OriginationInternalOperation, RpcConvertOptions> {
    constructor(
        private readonly commonInternalOperationConverter: CommonInternalOperationConverter,
        private readonly commonOriginationOperationConverter: CommonOriginationOperationConverter,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcInternalOperation,
        uid: string,
        options: RpcConvertOptions,
    ): OriginationInternalOperation {
        guardKind(rpcData, taquitoRpc.OpKind.ORIGINATION, uid);

        const commonProperties = this.commonInternalOperationConverter.convert(rpcData, uid);
        const specificProperties = this.commonOriginationOperationConverter.convert(rpcData, uid, {
            ...options,
            rpcResult: rpcData.result,
            rpcResultProperty: [keyof<typeof rpcData>('result')],
        });

        return Object.freeze<OriginationInternalOperation>({ ...commonProperties, ...specificProperties });
    }
}
