import { keyof } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { OriginationResultConverter } from './origination-result-converter';
import { primitives } from '../../../primitives';
import { RpcConvertOptions } from '../../../rpc-convert-options';
import { guardRpcObject, joinUid } from '../../../rpc-converter';
import {
    Michelson,
    OriginationInternalOperation,
    OriginationOperation,
    TaquitoRpcInternalOperation,
    TaquitoRpcOriginationOperation,
    TaquitoRpcOriginationResult,
} from '../../../rpc-data-interfaces';
import { OperationKind } from '../../operation-kind';

export type CommonOriginationOperation =
    Pick<OriginationOperation | OriginationInternalOperation,
    'balance' | 'delegateAddress' | 'kind' | 'result'>;

export interface CommonOriginationConvertOptions extends RpcConvertOptions {
    readonly rpcResult: TaquitoRpcOriginationResult;
    readonly rpcResultProperty: readonly string[];
}

@singleton()
export class CommonOriginationOperationConverter {
    constructor(private readonly resultConverter: OriginationResultConverter) {}

    convert(
        rpcData: TaquitoRpcOriginationOperation | TaquitoRpcInternalOperation,
        uid: string,
        options: CommonOriginationConvertOptions,
    ): CommonOriginationOperation {
        const storageMichelson = this.convertStorage(rpcData, joinUid(uid, keyof<typeof rpcData>('script')));
        const result = this.resultConverter.convert(options.rpcResult, joinUid(uid, ...options.rpcResultProperty), { ...options, storageMichelson });

        return {
            balance: primitives.bigInteger.convertProperty(rpcData, 'balance', uid, { min: 0 }),
            delegateAddress: primitives.address.convertNullableProperty(rpcData, 'delegate', uid),
            kind: OperationKind.Origination,
            result,
        };
    }

    private convertStorage(rpcData: TaquitoRpcOriginationOperation | TaquitoRpcInternalOperation, rpcScriptUid: string): Michelson {
        const rpcScript = guardRpcObject(rpcData.script, rpcScriptUid);
        return primitives.michelson.convertProperty(rpcScript, 'storage', rpcScriptUid);
    }
}
