import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../common/balance-update/balance-update';
import { EntityToIndex } from '../../entity-to-index';
import { OperationKind } from '../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcSeedNonceRevelationOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultRevelation>;

/**
 * It allows a baker to anonymously reveal the nonce seed for the commitment it had included in a previously baked block (in the previous cycle).
 * @category RPC Block Data
 */
export interface SeedNonceRevelationOperation extends EntityToIndex<TaquitoRpcSeedNonceRevelationOperation> {
    // Narrowing properties:
    readonly kind: OperationKind.SeedNonceRevelation;

    // Specific properties:
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly level: number;
    readonly nonce: string;
}
