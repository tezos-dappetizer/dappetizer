import { singleton } from 'tsyringe';

import { TaquitoRpcSetDepositsLimitResult } from '../../../rpc-data-interfaces';
import { SimpleOperationResultConverter } from '../../common/operation-result/simple-operation-result-converter';

@singleton()
export class SetDepositsLimitResultConverter extends SimpleOperationResultConverter<TaquitoRpcSetDepositsLimitResult> {}
