import * as taquitoRpc from '@taquito/rpc';
import { BigNumber } from 'bignumber.js';
import { DeepReadonly } from 'ts-essentials';

import { SetDepositsLimitResult } from './set-deposits-limit-result';
import { BaseMainOperationWithResult } from '../common/main-operation/base-main-operation-with-result';
import { OperationKind } from '../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcSetDepositsLimitOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultSetDepositsLimit>;

/**
 * It enables delegates to adjust the amount of stake
 * a delegate {@link https://tezos.gitlab.io/alpha/consensus.html#active-stake-alpha | has locked in bonds}.
 * @category RPC Block Data
 */
export interface SetDepositsLimitOperation extends BaseMainOperationWithResult<TaquitoRpcSetDepositsLimitOperation> {
    readonly kind: OperationKind.SetDepositsLimit;

    // Common set deposits limit properties:

    /** It is mutez - greater than or equal to `0`. */
    readonly limit: BigNumber | null;

    readonly result: SetDepositsLimitResult;
}
