import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { AppliedOperationResult, OperationResult } from '../common/operation-result/operation-results';

/** @category RPC Block Data */
export type TaquitoRpcSetDepositsLimitResult = DeepReadonly<taquitoRpc.OperationResultSetDepositsLimit>;

/** @category RPC Block Data */
export type SetDepositsLimitResult = OperationResult<AppliedSetDepositsLimitResult>;

/** @category RPC Block Data */
export interface AppliedSetDepositsLimitResult extends AppliedOperationResult<TaquitoRpcSetDepositsLimitResult> {}
