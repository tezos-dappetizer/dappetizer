import { MainOperationWithResult } from './main-operation-with-result';
import { InternalOperation } from '../internal-operation';

/**
 * Either a {@link MainOperationWithResult} or an {@link InternalOperation} as far as all of them have a result.
 * @category RPC Block Data
 */
export type OperationWithResult = MainOperationWithResult | InternalOperation;
