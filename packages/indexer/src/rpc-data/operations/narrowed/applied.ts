import { argGuard } from '@tezos-dappetizer/utils';

import { OperationWithResult } from './operation-with-result';
import { OperationResultStatus } from '../common/operation-result/operation-result-status';

/**
 * Limits operation only to the one with `result.status` being {@link OperationResultStatus.Applied}.
 * @category RPC Block Data
 */
export type Applied<TOperation extends OperationWithResult> = TOperation & {
    // eslint-disable-next-line @typescript-eslint/ban-types
    readonly result: Extract<TOperation['result'], { status: OperationResultStatus.Applied }>;
};

/** Narrows down the given operation if its `result.status` is {@link OperationResultStatus.Applied}. */
export function isApplied<TOperation extends OperationWithResult>(operation: TOperation): operation is Applied<TOperation> {
    argGuard.object(operation, 'operation');
    argGuard.object(operation.result, 'operation.result');
    argGuard.enum(operation.result.status, 'operation.result.status', OperationResultStatus);

    return operation.result.status === OperationResultStatus.Applied;
}
