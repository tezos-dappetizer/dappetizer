import { argGuard } from '@tezos-dappetizer/utils';

import { Applied, isApplied } from './applied';
import { InternalOperation } from '../internal-operation';
import { OperationKind } from '../operation-kind';
import { OriginationInternalOperation } from '../origination/origination-internal-operation';
import { TransactionInternalOperation } from '../transaction/transaction-internal-operation';

/**
 * An internal operation with balance updates on its `result`.
 * @category RPC Block Data
 */
export type InternalOperationWithBalanceUpdatesInResult = Applied<OriginationInternalOperation | TransactionInternalOperation>;

export function isInternalOperationWithBalanceUpdatesInResult(
    operation: InternalOperation,
): operation is InternalOperationWithBalanceUpdatesInResult {
    argGuard.object(operation, 'operation');
    argGuard.enum(operation.kind, 'operation.kind', OperationKind);

    return isApplied(operation)
        && (operation.kind === OperationKind.Origination || operation.kind === OperationKind.Transaction);
}
