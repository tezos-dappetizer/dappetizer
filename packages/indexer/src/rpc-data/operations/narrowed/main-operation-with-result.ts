import { argGuard } from '@tezos-dappetizer/utils';

import { DelegationOperation } from '../delegation/delegation-operation';
import { IncreasePaidStorageOperation } from '../increase-paid-storage/increase-paid-storage-operation';
import { MainOperation } from '../main-operation';
import { OperationKind } from '../operation-kind';
import { OriginationOperation } from '../origination/origination-operation';
import { RegisterGlobalConstantOperation } from '../register-global-constant/register-global-constant-operation';
import { RevealOperation } from '../reveal/reveal-operation';
import { SetDepositsLimitOperation } from '../set-deposits-limit/set-deposits-limit-operation';
import { SmartRollupAddMessagesOperation } from '../smart-rollup/add-messages/smart-rollup-add-messages-operation';
import { SmartRollupCementOperation } from '../smart-rollup/cement/smart-rollup-cement-operation';
import {
    SmartRollupExecuteOutboxMessageOperation,
} from '../smart-rollup/execute-outbox-message/smart-rollup-execute-outbox-message-operation';
import { SmartRollupOriginateOperation } from '../smart-rollup/originate/smart-rollup-originate-operation';
import { SmartRollupPublishOperation } from '../smart-rollup/publish/smart-rollup-publish-operation';
import { SmartRollupRecoverBondOperation } from '../smart-rollup/recover-bond/smart-rollup-recover-bond-operation';
import { SmartRollupRefuteOperation } from '../smart-rollup/refute/smart-rollup-refute-operation';
import { SmartRollupTimeoutOperation } from '../smart-rollup/timeout/smart-rollup-timeout-operation';
import { TransactionOperation } from '../transaction/transaction-operation';
import { TransferTicketOperation } from '../transfer-ticket/transfer-ticket-operation';
import { UpdateConsensusKeyOperation } from '../update-consensus-key/update-consensus-key-operation';

/**
 * The main (top-level, directly in operation groups) operation which has a result and also internal operations with their results.
 * It is the discriminated union which can be narrowed by `kind`.
 * @category RPC Block Data
 */
export type MainOperationWithResult =
    | DelegationOperation
    | IncreasePaidStorageOperation
    | OriginationOperation
    | RegisterGlobalConstantOperation
    | RevealOperation
    | SetDepositsLimitOperation
    | SmartRollupAddMessagesOperation
    | SmartRollupCementOperation
    | SmartRollupExecuteOutboxMessageOperation
    | SmartRollupOriginateOperation
    | SmartRollupPublishOperation
    | SmartRollupRecoverBondOperation
    | SmartRollupRefuteOperation
    | SmartRollupTimeoutOperation
    | TransactionOperation
    | TransferTicketOperation
    | UpdateConsensusKeyOperation;

const narrowedKinds = new Set([
    OperationKind.Delegation,
    OperationKind.IncreasePaidStorage,
    OperationKind.Origination,
    OperationKind.RegisterGlobalConstant,
    OperationKind.Reveal,
    OperationKind.SetDepositsLimit,
    OperationKind.SmartRollupAddMessages,
    OperationKind.SmartRollupCement,
    OperationKind.SmartRollupExecuteOutboxMessage,
    OperationKind.SmartRollupOriginate,
    OperationKind.SmartRollupPublish,
    OperationKind.SmartRollupRecoverBond,
    OperationKind.SmartRollupRefute,
    OperationKind.SmartRollupTimeout,
    OperationKind.Transaction,
    OperationKind.TransferTicket,
    OperationKind.UpdateConsensusKey,
]);

export function isMainOperationWithResult(operation: MainOperation): operation is MainOperationWithResult {
    argGuard.object(operation, 'operation');
    argGuard.enum(operation.kind, 'operation.kind', OperationKind);

    return narrowedKinds.has(operation.kind);
}
