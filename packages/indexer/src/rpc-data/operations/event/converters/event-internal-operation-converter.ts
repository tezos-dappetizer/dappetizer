import * as taquitoRpc from '@taquito/rpc';
import { singleton } from 'tsyringe';

import { EventResultConverter } from './event-result-converter';
import { LazyMichelsonValueFactory } from '../../../common/michelson/lazy-michelson-value-factory';
import { primitives } from '../../../primitives';
import { RpcConvertOptions } from '../../../rpc-convert-options';
import { RpcObjectConverter } from '../../../rpc-converter';
import { EventInternalOperation, OperationKind, TaquitoRpcInternalOperation } from '../../../rpc-data-interfaces';
import {
    CommonInternalOperationConverter,
    guardKind,
} from '../../common/internal-operation/common-internal-operation-converter';

@singleton()
export class EventInternalOperationConverter
    extends RpcObjectConverter<TaquitoRpcInternalOperation, EventInternalOperation, RpcConvertOptions> {
    constructor(
        private readonly commonInternalOperationConverter: CommonInternalOperationConverter,
        private readonly resultConverter: EventResultConverter,
        private readonly lazyValueFactory: LazyMichelsonValueFactory,
    ) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcInternalOperation,
        uid: string,
        _options: RpcConvertOptions,
    ): EventInternalOperation {
        guardKind(rpcData, taquitoRpc.OpKind.EVENT, uid);

        const commonProperties = this.commonInternalOperationConverter.convert(rpcData, uid);
        const result = this.resultConverter.convertProperty(rpcData, 'result', uid);
        const typeSchema = primitives.michelsonSchema.convertProperty(rpcData, 'type', uid);
        const payloadMichelson = primitives.michelson.convertNullableProperty(rpcData, 'payload', uid);
        const payload = payloadMichelson ? this.lazyValueFactory.create(payloadMichelson, typeSchema) : null;

        return Object.freeze<EventInternalOperation>({
            ...commonProperties,
            kind: OperationKind.Event,
            payload,
            result,
            tag: primitives.nonWhiteSpaceString.convertNullableProperty(rpcData, 'tag', uid),
            typeSchema,
        });
    }
}
