import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { AppliedOperationResult, OperationResult } from '../common/operation-result/operation-results';

/** @category RPC Block Data */
export type TaquitoRpcEventResult = DeepReadonly<taquitoRpc.OperationResultEvent>;

/** @category RPC Block Data */
export type EventResult = OperationResult<AppliedEventResult>;

/** @category RPC Block Data */
export interface AppliedEventResult extends AppliedOperationResult<TaquitoRpcEventResult> {}
