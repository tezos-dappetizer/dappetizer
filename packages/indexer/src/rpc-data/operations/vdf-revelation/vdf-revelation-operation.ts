import * as taquitoRpc from '@taquito/rpc';
import { DeepReadonly } from 'ts-essentials';

import { BalanceUpdate } from '../../common/balance-update/balance-update';
import { EntityToIndex } from '../../entity-to-index';
import { OperationKind } from '../operation-kind';

/** @category RPC Block Data */
export type TaquitoRpcVdfRevelationOperation = DeepReadonly<taquitoRpc.OperationContentsAndResultVdfRevelation>;

/**
 * It allows the submission of a solution to, and a proof of correctness of, the VDF challenge
 * corresponding to the VDF revelation period of the randomness generation protocol.
 * @category RPC Block Data
 */
export interface VdfRevelationOperation extends EntityToIndex<TaquitoRpcVdfRevelationOperation> {
    readonly kind: OperationKind.VdfRevelation;

    // Specific properties:
    readonly balanceUpdates: readonly BalanceUpdate[];
    readonly solution: readonly string[];
}
