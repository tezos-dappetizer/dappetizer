import { singleton } from 'tsyringe';

import { BalanceUpdateConverter } from '../../common/balance-update/converters/balance-update-converter';
import { primitives } from '../../primitives';
import { RpcConvertOptions } from '../../rpc-convert-options';
import { RpcObjectConverter } from '../../rpc-converter';
import { OperationKind, TaquitoRpcVdfRevelationOperation, VdfRevelationOperation } from '../../rpc-data-interfaces';

@singleton()
export class VdfRevelationOperationConverter extends RpcObjectConverter<TaquitoRpcVdfRevelationOperation, VdfRevelationOperation, RpcConvertOptions> {
    constructor(private readonly balanceUpdateConverter: BalanceUpdateConverter) {
        super();
    }

    protected override convertObject(
        rpcData: TaquitoRpcVdfRevelationOperation,
        uid: string,
        _options: RpcConvertOptions,
    ): VdfRevelationOperation {
        const balanceUpdates = this.balanceUpdateConverter.convertFromMetadataProperty(rpcData, uid);

        return Object.freeze<VdfRevelationOperation>({
            balanceUpdates,
            kind: OperationKind.VdfRevelation,
            rpcData,
            solution: primitives.nonWhiteSpaceString.convertArrayProperty(rpcData, 'solution', uid),
            uid,
        });
    }
}
