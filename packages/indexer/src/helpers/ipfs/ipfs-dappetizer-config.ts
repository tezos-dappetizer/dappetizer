import { NonEmptyArray } from 'ts-essentials';

/**
 * Configures connections to {@link https://ipfs.io/ | IPFS} for metadata retrieval.
 * This is used as a part of {@link "@tezos-dappetizer/database".DappetizerConfigUsingDb}.
 * @example Sample usage (all properties specified explicitly) within `dappetizer.config.ts` file:
 * ```typescript
 * import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';
 *
 * const config: DappetizerConfigUsingDb = {
 *     ipfs: {
 *         gateways: ['https://ipfs.io/', 'https://cloudflare-ipfs.com/'],
 *         timeoutMillis: 100_000,
 *     },
 *     ... // Other config parts.
 * };
 *
 * export default config;
 * ```
 * @category Dappetizer Config
 */
export interface IpfsDappetizerConfig {
    /**
     * The IPFS gateways that are requested at the same time. Then the fastest response is used.
     * @limits A non-empty array of absolute URLs.
     * @default `['https://ipfs.io/', 'https://cloudflare-ipfs.com/']`
     */
    gateways?: NonEmptyArray<string>;

    /**
     * The timeout for IPFS requests in milliseconds.
     * @limits An integer with minimum `1`.
     * @default `100_000` (100 seconds)
     */
    timeoutMillis?: number;
}
