import { ConfigObjectElement, ConfigWithDiagnostics, ROOT_CONFIG_ELEMENT_DI_TOKEN } from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

@singleton()
export class IpfsConfig implements ConfigWithDiagnostics {
    static readonly ROOT_NAME = 'ipfs';

    readonly gateways: readonly string[];
    readonly timeoutMillis: number;

    constructor(@inject(ROOT_CONFIG_ELEMENT_DI_TOKEN) rootElement: ConfigObjectElement) {
        const thisElement = rootElement.getOptional(IpfsConfig.ROOT_NAME)?.asObject<keyof IpfsConfig>();

        this.gateways = thisElement?.getOptional('gateways')?.asArray('NotEmpty')?.map(e => e.asAbsoluteUrl())
            ?? ['https://ipfs.io/', 'https://cloudflare-ipfs.com/'];
        this.timeoutMillis = thisElement?.getOptional('timeoutMillis')?.asInteger({ min: 1 })
            ?? 100_000;
    }

    getDiagnostics(): [string, unknown] {
        return [IpfsConfig.ROOT_NAME, this];
    }
}
