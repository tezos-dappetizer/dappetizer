import { asRequiredLabels } from '@tezos-dappetizer/utils';
import { Counter, Histogram } from 'prom-client';
import { singleton } from 'tsyringe';

@singleton()
export class IpfsMetrics {
    readonly requestCount = new Counter({
        name: 'ai_ipfs_requests_total',
        help: 'The total number of IPFS requests. Each request is executed on all configured gateways.',
    });

    readonly executionDuration = new Histogram({
        name: 'ai_ipfs_execution_duration',
        help: 'How long it took to execute IPFS request in milliseconds - it equals to the time of the gateway that won.',
        buckets: [50, 100, 500, 1000, 5000, 10000, 30000],
    });

    readonly responseCount = asRequiredLabels(new Counter({
        name: 'ai_ipfs_responses',
        help: 'The number of IPFS responses with particular status.',
        labelNames: ['status'],
    }));

    readonly gatewayWinCount = asRequiredLabels(new Counter({
        name: 'ai_ipfs_gateway_wins',
        help: 'The number of times when the response from particular gateway was used because it was the fastest and preferably 200 OK.',
        labelNames: ['gateway'],
    }));

    readonly gatewayFailureCount = asRequiredLabels(new Counter({
        name: 'ai_ipfs_gateway_failures',
        help: 'The number of times when the response from particular gateway was the fastest but not 200 OK'
            + ' and some other gateway responsed with 200 OK. Therefore this indicates some issue on the gateway.',
        labelNames: ['gateway'],
    }));
}
