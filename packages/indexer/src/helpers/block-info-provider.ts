import { NonNullish } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { TaquitoRpcBlock } from '../rpc-data/rpc-data-interfaces';

@singleton()
export class BlockInfoProvider {
    getInfo(block: TaquitoRpcBlock): NonNullish {
        return {
            hash: block.hash,
            level: block.header.level,
            predecessor: block.header.predecessor,
            timestamp: block.header.timestamp,
        };
    }
}
