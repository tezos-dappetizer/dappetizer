import { InjectionToken } from 'tsyringe';

/** @internal Intended only for Dappetizer itself. */
export interface IndexerPackageJson {
    readonly version: string;
    readonly dependencies: IndexerPackageJsonDependencies;
}

/** @internal Intended only for Dappetizer itself. */
export interface IndexerPackageJsonDependencies {
    readonly bignumber: string;
    readonly taquito: string;
    readonly taquitoMichelsonEncoder: string;
}

/** @internal Intended only for Dappetizer itself. */
export const INDEXER_PACKAGE_JSON_DI_TOKEN: InjectionToken<IndexerPackageJson> = 'Dappetizer:Indexer:IndexerPackageJson';
