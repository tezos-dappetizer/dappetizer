import { freezeResult } from '@tezos-dappetizer/utils';
import { StrictExtract } from 'ts-essentials';
import { singleton } from 'tsyringe';

import { IndexingConfig } from '../config/indexing/indexing-config';

export interface ResolvedSelectiveIndexingConfig {
    readonly toBeIndexed: boolean;
    readonly name: string | null;
}

@singleton()
export class SelectiveIndexingConfigResolver {
    constructor(
        private readonly config: IndexingConfig,
    ) {}

    @freezeResult()
    resolve(
        address: string,
        configProperty: StrictExtract<keyof IndexingConfig, 'contracts' | 'smartRollups'>,
    ): ResolvedSelectiveIndexingConfig {
        const itemConfigs = this.config[configProperty];

        if (!itemConfigs) {
            return { toBeIndexed: true, name: null };
        }

        const properties = itemConfigs.find(c => c.addresses.includes(address));
        if (!properties) {
            return { toBeIndexed: false, name: null };
        }

        return {
            toBeIndexed: true,
            name: properties.name,
        };
    }
}
