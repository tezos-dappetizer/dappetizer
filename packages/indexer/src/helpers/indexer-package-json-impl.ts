import {
    locatePackageJsonInCurrentPackage,
    npmModules,
    PACKAGE_JSON_LOADER_DI_TOKEN,
    PackageJsonLoader,
} from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

import { IndexerPackageJson, IndexerPackageJsonDependencies } from './indexer-package-json';

@singleton()
export class IndexerPackageJsonImpl implements IndexerPackageJson {
    readonly version: string;
    readonly dependencies: IndexerPackageJsonDependencies;

    constructor(@inject(PACKAGE_JSON_LOADER_DI_TOKEN) loader: PackageJsonLoader) {
        const packageJson = loader.load(locatePackageJsonInCurrentPackage(__filename));

        this.version = packageJson.getVersion();
        this.dependencies = Object.freeze<IndexerPackageJsonDependencies>({
            bignumber: packageJson.getDependencyVersion(npmModules.BIGNUMBER),
            taquito: packageJson.getDependencyVersion(npmModules.taquito.TAQUITO),
            taquitoMichelsonEncoder: packageJson.getDependencyVersion(npmModules.taquito.MICHELSON_ENCODER),
        });
    }
}
