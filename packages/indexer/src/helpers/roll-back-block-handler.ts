import { dumpToString, errorToString, injectLogger, Logger } from '@tezos-dappetizer/utils';
import { reverse } from 'iter-tools';
import { singleton } from 'tsyringe';

import { tryRollBackTransaction } from './database-handler-helpers';
import { scopedLogKeys } from './scoped-log-keys';
import { IndexedBlock } from '../client-indexers/indexed-block';
import { IndexerDatabaseHandlerWrapper } from '../client-indexers/wrappers/indexer-database-handler-wrapper';
import { IndexerModuleCollection } from '../client-indexers/wrappers/indexer-module-collection';
import { IndexerModuleWrapper } from '../client-indexers/wrappers/indexer-module-wrapper';

@singleton()
export class RollBackBlockHandler<TDbContext = unknown> {
    constructor(
        private readonly databaseHandler: IndexerDatabaseHandlerWrapper<TDbContext>,
        private readonly indexerModules: IndexerModuleCollection<TDbContext>,
        @injectLogger(RollBackBlockHandler) private readonly logger: Logger,
    ) {}

    async rollBack(blockToRollBack: IndexedBlock): Promise<void> {
        const dbContext = await this.databaseHandler.startTransaction();
        try {
            // Reverse() so that dependencies (e.g. db relations) can be deleted in proper order.
            for (const module of reverse(this.indexerModules.modules)) {
                await this.executeModule(module, blockToRollBack, dbContext);
            }

            await this.databaseHandler.deleteBlock(blockToRollBack, dbContext);
            await this.databaseHandler.commitTransaction(dbContext);
        } catch (error) {
            await tryRollBackTransaction(this.databaseHandler, this.logger, dbContext);
            throw new Error(`Failed to rollback block ${dumpToString({ ...blockToRollBack })}. ${errorToString(error)}`);
        }
    }

    private async executeModule<TContextData>(
        module: IndexerModuleWrapper<TDbContext, TContextData>,
        blockToRollBack: IndexedBlock,
        dbContext: TDbContext,
    ): Promise<void> {
        await this.logger.runWithData({ [scopedLogKeys.INDEXER_MODULE]: module.name }, async () => {
            this.logger.logDebug(`Rolling back the block on rerog by {${scopedLogKeys.INDEXER_MODULE}}.`);

            await module.indexingCycleHandler.rollBackOnReorganization(blockToRollBack, dbContext);
            this.logger.logDebug(`Successfully rolled back the block on rerog by {${scopedLogKeys.INDEXER_MODULE}}.`);
        });
    }
}
