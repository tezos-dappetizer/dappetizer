import { Gauge } from 'prom-client';
import { singleton } from 'tsyringe';

/** Container with Prometheus metric objects. */
@singleton()
export class IndexerMetrics {
    readonly blockLevel = new Gauge({
        name: 'ai_indexer_block_level',
        help: 'Last sucessfully indexed block level.',
    });
}
