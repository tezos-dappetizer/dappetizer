import * as taquitoRpc from '@taquito/rpc';
import { injectLogger, Logger, runInPerformanceSection, TAQUITO_RPC_CLIENT_DI_TOKEN } from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

import { TaquitoRpcBlock } from '../rpc-data/rpc-data-interfaces';

@singleton()
export class BlockDownloader {
    constructor(
        @inject(TAQUITO_RPC_CLIENT_DI_TOKEN) private readonly rpcClient: taquitoRpc.RpcClient,
        @injectLogger(BlockDownloader) private readonly logger: Logger,
    ) {}

    async download(level: number): Promise<TaquitoRpcBlock> {
        return runInPerformanceSection(
            this.logger,
            async () => this.rpcClient.getBlock({ block: level.toString() }),
        );
    }
}
