import { registerSingleton } from '@tezos-dappetizer/utils';
import { AsyncOrSync } from 'ts-essentials';
import { DependencyContainer, Provider } from 'tsyringe';

import { BLOCK_LEVEL_INDEXING_LISTENERS_DI_TOKEN } from './block-level-indexing-listener-di-token';

export interface BlockLevelIndexingListener {
    onToBlockLevelReached(): AsyncOrSync<void>;
}

/**
 * Registers a listener called when particular block levels are indexed by Dappetizer.
 * It is registered as **singleton**.
 */
export function registerBlockLevelIndexingListener(diContainer: DependencyContainer, listenerProvider: Provider<BlockLevelIndexingListener>): void {
    registerSingleton(diContainer, BLOCK_LEVEL_INDEXING_LISTENERS_DI_TOKEN, listenerProvider);
}
