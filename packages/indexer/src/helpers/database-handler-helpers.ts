import { Logger } from '@tezos-dappetizer/utils';

import { IndexerDatabaseHandlerWrapper } from '../client-indexers/wrappers/indexer-database-handler-wrapper';

export async function tryRollBackTransaction<TDbContext>(
    databaseHandler: IndexerDatabaseHandlerWrapper<TDbContext>,
    logger: Logger,
    dbContext: TDbContext,
): Promise<void> {
    try {
        await databaseHandler.rollBackTransaction(dbContext);
    } catch (rollbackError) {
        logger.logError('Failed to roll back transaction after a previous error (will be thrown). {rollbackError}', { rollbackError });
    }
}
