import * as taquito from '@taquito/taquito';
import { InjectionToken } from 'tsyringe';

export const TAQUITO_TEZOS_TOOLKIT_DI_TOKEN: InjectionToken<taquito.TezosToolkit> = 'Dappetizer:Indexer:taquito.TezosToolkit';
