export const scopedLogKeys = Object.freeze({
    INDEXED_BLOCK: 'indexedBlock',
    INDEXED_CONTRACT_ADDRESS: 'indexedContractAddress',
    INDEXED_OPERATION_GROUP_HASH: 'indexedOperationGroupHash',
    INDEXER_MODULE: 'indexerModule',
} as const);
