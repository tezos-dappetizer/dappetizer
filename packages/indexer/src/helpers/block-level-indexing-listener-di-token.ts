import { InjectionToken } from 'tsyringe';

import { BlockLevelIndexingListener } from './block-level-indexing-listener';

export const BLOCK_LEVEL_INDEXING_LISTENERS_DI_TOKEN: InjectionToken<BlockLevelIndexingListener> = 'Dappetizer:Indexer:BlockLevelIndexingListeners';
