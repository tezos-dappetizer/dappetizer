import { singleton } from 'tsyringe';

import { SmartRollup } from './smart-rollup';
import { SmartRollupProvider } from './smart-rollup-provider';
import { SelectiveIndexingConfigResolver } from '../helpers/selective-indexing-config-resolver';

@singleton()
export class SmartRollupProviderImpl implements SmartRollupProvider {
    constructor(
        private readonly configResolver: SelectiveIndexingConfigResolver,
    ) {}

    getRollup(address: string): SmartRollup {
        const config = this.configResolver.resolve(address, 'smartRollups');

        return Object.freeze<SmartRollup>({ type: 'SmartRollup', address, config });
    }
}
