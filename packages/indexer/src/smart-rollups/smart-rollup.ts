/**
 * Basic info about smart rollup.
 * @experimental
 */
export interface SmartRollup {
    /** Usable for narrowing the type of this object. */
    readonly type: 'SmartRollup';

    /** Smart rollup `sr1` address. */
    readonly address: string;

    readonly config: SmartRollupConfig;
}

/**
 * Properties of the smart rollup from the local config file.
 * @experimental
 */
export interface SmartRollupConfig {
    /** Indicates selective indexing of this smart rollup. It is also `true` if no selective indexing is configured. */
    readonly toBeIndexed: boolean;

    /** Configured name. It is `null` if no `name` is configured or selective is not configured. */
    readonly name: string | null;
}
