import { singleton } from 'tsyringe';

import { BoundSmartRollupIndexer, UnboundRollupData } from './bound-smart-rollup-indexer';
import { TrustedSmartRollupIndexer } from '../../client-indexers/wrappers/trusted-indexer-interfaces';

@singleton()
export class BoundSmartRollupIndexerFactory<TDbContext> {
    create<TContextData, TRollupData>(
        indexer: TrustedSmartRollupIndexer<TDbContext, TContextData, TRollupData>,
        rollupData: TRollupData,
    ): BoundSmartRollupIndexer<TDbContext, TContextData> {
        return {
            indexCement: adaptContext(indexer.indexCement.bind(indexer), rollupData),
            indexExecuteOutboxMessage: adaptContext(indexer.indexExecuteOutboxMessage.bind(indexer), rollupData),
            indexOriginate: adaptContext(indexer.indexOriginate.bind(indexer), rollupData),
            indexPublish: adaptContext(indexer.indexPublish.bind(indexer), rollupData),
            indexRecoverBond: adaptContext(indexer.indexRecoverBond.bind(indexer), rollupData),
            indexRefute: adaptContext(indexer.indexRefute.bind(indexer), rollupData),
            indexTimeout: adaptContext(indexer.indexTimeout.bind(indexer), rollupData),
        };
    }
}

function adaptContext<TOperation, TDbContext, TIndexingContext extends { readonly rollupData: TRollupData }, TRollupData>(
    method: (operation: TOperation, dbContext: TDbContext, indexingContext: TIndexingContext) => void | PromiseLike<void>,
    rollupData: TRollupData,
): (operation: TOperation, dbContext: TDbContext, indexingContext: TIndexingContext & { rollupData: UnboundRollupData }) => Promise<void> {
    return async (operation, dbContext, indexingContext) => {
        // This is the final indexingContext -> it is served to user -> freeze it.
        await method(operation, dbContext, Object.freeze({ ...indexingContext, rollupData }));
    };
}
