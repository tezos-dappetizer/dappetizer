import { InjectionToken } from 'tsyringe';

import { SmartRollup } from './smart-rollup';

/** @experimental */
export interface SmartRollupProvider {
    getRollup(address: string): SmartRollup;
}

/** @experimental */
export const SMART_ROLLUP_PROVIDER_DI_TOKEN: InjectionToken<SmartRollupProvider> = 'Dappetizer:Indexer:SmartRollupProvider';
