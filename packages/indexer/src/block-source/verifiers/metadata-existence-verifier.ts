import { isNullish, keyof } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { BlockVerifier } from './block-verifier';
import { DappetizerConfig } from '../../config/dappetizer-config';
import { IndexingDappetizerConfig } from '../../config/indexing/indexing-dappetizer-config';
import { TaquitoRpcBlock } from '../../rpc-data/rpc-data-interfaces';

@singleton()
export class MetadataExistenceVerifier implements BlockVerifier {
    verify(block: TaquitoRpcBlock, _previousBlock: TaquitoRpcBlock | null): void {
        if (isNullish(block.metadata)) {
            throw new Error(`Missing block and operation ${keyof<typeof block>('metadata')} therefore it cannot be indexed.`
                + ` Try raising ${keyof<IndexingDappetizerConfig>('fromBlockLevel')} if the data interesting for you exist there.`
                + ` Or configure ${keyof<DappetizerConfig['networks'][string]>('tezosNode')} which has 'history_mode: archive'.`);
        }
    }
}
