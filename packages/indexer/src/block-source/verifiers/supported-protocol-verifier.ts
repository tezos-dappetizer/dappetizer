import { keyof } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { BlockVerifier } from './block-verifier';
import { IndexingDappetizerConfig } from '../../config/indexing/indexing-dappetizer-config';
import { TaquitoRpcBlock } from '../../rpc-data/rpc-data-interfaces';

const unsupportedProtocols: ReadonlySet<string> = Object.freeze(new Set([
    'PtCJ7pwoxe8JasnHY8YonnLYjcVHmhiARPJvqcC6VfHT5s8k8sY',
    'PsYLVpVvgbLhAhoqAkMFUo6gudkJ9weNXhUYCiLDzcUpFpkk8Wt',
    'PsddFKi32cMJ2qPjf43Qv5GDWLDPZb3T3bF6fLKiF5HtvHNU7aP',
    'Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd',
    'PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS',
]));

@singleton()
export class SupportedProtocolVerifier implements BlockVerifier {
    verify(block: TaquitoRpcBlock, _previousBlock: TaquitoRpcBlock | null): void {
        if (unsupportedProtocols.has(block.protocol)) {
            throw new Error(`Protocol hash '${block.protocol}' is not supported.`
                + ` Have you configured indexer '${keyof<IndexingDappetizerConfig>('fromBlockLevel')}' too far in the past?`);
        }
    }
}
