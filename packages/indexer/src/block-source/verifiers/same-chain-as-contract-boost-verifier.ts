import { keyof } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { BlockVerifier } from './block-verifier';
import { IndexingDappetizerConfig } from '../../config/indexing/indexing-dappetizer-config';
import { TaquitoRpcBlock } from '../../rpc-data/rpc-data-interfaces';
import { ContractIndexingBoostFactory } from '../contract-boost/contract-indexing-boost-factory';

@singleton()
export class SameChainAsContractBoost implements BlockVerifier {
    constructor(private readonly boostFactory: ContractIndexingBoostFactory) {}

    async verify(block: TaquitoRpcBlock, previousBlock: TaquitoRpcBlock | null): Promise<void> {
        if (previousBlock) {
            return; // Enough to verify it once
        }

        const boost = this.boostFactory.create();
        if (!boost) {
            return;
        }

        const boostChainId = await boost.getChainId();
        if (block.chain_id !== boostChainId) {
            throw new Error(`Configured ${keyof<IndexingDappetizerConfig>('contractBoost')} '${boost.name}' uses chain '${boostChainId}'`
                + ` but a new block is from chain '${block.chain_id}'. You may need to configure proper `
                + `${keyof<NonNullable<IndexingDappetizerConfig['contractBoost']>>('apiUrl')} for the boost.`);
        }
    }
}
