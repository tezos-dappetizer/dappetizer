import { AsyncOrSync } from 'ts-essentials';

import { TaquitoRpcBlock } from '../../rpc-data/rpc-data-interfaces';

export interface BlockVerifier {
    verify(block: TaquitoRpcBlock, previousBlock: TaquitoRpcBlock | null): AsyncOrSync<void>;
}
