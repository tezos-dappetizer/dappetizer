import * as taquitoRpc from '@taquito/rpc';
import {
    appendUrl,
    AsyncIterableProvider,
    dappetizerAssert,
    injectExplicit,
    isNullish,
    isWhiteSpace,
    TAQUITO_RPC_CLIENT_DI_TOKEN,
} from '@tezos-dappetizer/utils';
import { inject, injectable } from 'tsyringe';

import { BlockIndexingHealthState } from '../block-processing/health/block-indexing-health-state';
import { IndexingConfig } from '../config/indexing/indexing-config';
import { BlockDownloader } from '../helpers/block-downloader';
import { TaquitoRpcBlock } from '../rpc-data/rpc-data-interfaces';

/**
 * Downloads blocks from RPC by given level.
 * Multiple downloads (of subsequent blocks) are in progress according to config.
 *
 * Main algorithm:
 * - Producer independently listens for new blocks and creates new inactive tasks for them.
 * - Scheduler keeps activating configured number of tasks on various events - new task, task completes etc.
 * - We keep yielding first task from the queue. So the source order must be kept.
 * - Also errors must be propagated from all sources - source iterable, block download etc.
 */
@injectable()
export class DownloadBlockByLevelProvider implements AsyncIterableProvider<TaquitoRpcBlock> {
    constructor(
        @injectExplicit() private readonly nextProvider: AsyncIterableProvider<number>,
        private readonly blockDownloader: BlockDownloader,
        private readonly config: IndexingConfig,
        @inject(TAQUITO_RPC_CLIENT_DI_TOKEN) private readonly rpcClient: taquitoRpc.RpcClient,
        private readonly healthState: BlockIndexingHealthState,
    ) {}

    async *iterate(): AsyncIterableIterator<TaquitoRpcBlock> {
        // Wraps promises to functions -> captures errors b/c failed promises are considered unhandled by Node.js -> fail entire app.
        const queue = new ObservableQueue<Promise<BlockResult>>();

        void this.runBlockLevelProducer(queue);

        while (!queue.isClosed || queue.size > 0) {
            let blockPromise: Promise<BlockResult> | null = null;

            while (!isNullish(blockPromise = queue.dequeue())) {
                // Yield one-by-one to get the error as late as possible -> former blocks indexed correctly.
                const blockResult = await blockPromise;

                this.healthState.blockBeingProcessed = !isNullish(blockResult.level) ? { level: blockResult.level } : null;
                if (blockResult.hasError) {
                    throw blockResult.error;
                }
                this.verifyWellFormedBlock(blockResult.block, blockResult.level);
                this.healthState.blockBeingProcessed = { hash: blockResult.block.hash, level: blockResult.level };
                yield blockResult.block;
            }

            if (!queue.isClosed) {
                await queue.enqueuedEvent.wait();
            }
        }
    }

    private async runBlockLevelProducer(queue: ObservableQueue<Promise<BlockResult>>): Promise<void> {
        try {
            for await (const level of this.nextProvider.iterate()) {
                if (queue.size >= this.config.blockQueueSize) {
                    await queue.dequeuedEvent.wait(); // Avoids flooding the queue.
                }
                queue.enqueue(this.downloadBlock(level));
            }
            queue.close();
        } catch (error) {
            queue.enqueue(Promise.resolve({ hasError: true, error, level: null }));
        }
    }

    private async downloadBlock(level: number): Promise<BlockResult> {
        try {
            const block = await this.blockDownloader.download(level);
            return { hasError: false, block, level };
        } catch (error) {
            return { hasError: true, error, level };
        }
    }

    private verifyWellFormedBlock(block: TaquitoRpcBlock, level: number): void {
        const isWellFormed = typeof block === 'object'
            && typeof block.hash === 'string'
            && !isWhiteSpace(block.hash)
            && typeof block.header === 'object'
            && typeof block.header.level === 'number'
            && block.header.level >= 0;

        if (!isWellFormed) {
            const blockUrl = appendUrl(this.rpcClient.getRpcUrl(), `chains/main/blocks/${level}`);
            throw new Error(`The data do not represent a well-formed block with hash, level, chain_id etc.`
                + ` Check the response from RPC at: ${blockUrl}`
                + ` Received data: ${JSON.stringify(block)}`);
        }
    }
}

type BlockResult =
    | Readonly<{ hasError: true; error: unknown; level: number | null }>
    | Readonly<{ hasError: false; block: TaquitoRpcBlock; level: number }>;

class ObservableQueue<T> {
    readonly enqueuedEvent = new AsyncEvent();
    readonly dequeuedEvent = new AsyncEvent();

    private readonly items: T[] = [];
    private isClosedFlag = false;

    get isClosed(): boolean {
        return this.isClosedFlag;
    }

    get size(): number {
        return this.items.length;
    }

    enqueue(item: T): void {
        dappetizerAssert(!this.isClosed, 'The queue is closed. So no new items are expected.');

        this.items.push(item);
        this.enqueuedEvent.fire();
    }

    dequeue(): T | null {
        const item = this.items.shift() ?? null;

        if (item) {
            this.dequeuedEvent.fire();
        }
        return item;
    }

    close(): void {
        this.isClosedFlag = true;
        this.enqueuedEvent.fire(); // Nothing more will be enqueued -> free awaiters.
    }
}

class AsyncEvent {
    private readonly callbacks: (() => void)[] = [];

    async wait(): Promise<void> {
        return new Promise<void>(resolve => {
            this.callbacks.push(resolve);
        });
    }

    fire(): void {
        this.callbacks.forEach(c => c());
        this.callbacks.length = 0;
    }
}
