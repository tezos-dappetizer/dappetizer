import { InjectionToken } from 'tsyringe';

import { ExplicitBlocksToIndex } from './block-range';

export const EXPLICIT_BLOCKS_TO_INDEX_DI_TOKEN: InjectionToken<ExplicitBlocksToIndex> = 'Dappetizer:Indexer:ExplicitBlocksToIndex';
