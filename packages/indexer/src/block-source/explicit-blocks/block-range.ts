import { NonEmptyArray } from 'ts-essentials';

export type ExplicitBlocksToIndex = Readonly<NonEmptyArray<BlockRange>>;

export interface BlockRange {
    readonly fromLevel: number;
    readonly toLevel: number;
}
