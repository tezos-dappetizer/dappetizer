import { argGuard, registerSingleton } from '@tezos-dappetizer/utils';
import { DependencyContainer, Provider } from 'tsyringe';

import { ExplicitBlocksToIndex } from './block-range';
import { EXPLICIT_BLOCKS_TO_INDEX_DI_TOKEN } from './explicit-blocks-to-index-di-token';

/**
 * Registers explicit block levels to be indexed by Dappetizer indexer app instead of using standard config.
 * It is registered as **singleton** and can be registered only **once** to make it clear which one is used.
 */
export function registerExplicitBlocksToIndex(
    diContainer: DependencyContainer,
    explicitBlocksToIndexProvider: Provider<ExplicitBlocksToIndex>,
): void {
    argGuard.object(diContainer, 'diContainer');

    if (diContainer.isRegistered(EXPLICIT_BLOCKS_TO_INDEX_DI_TOKEN)) {
        throw new Error('IndexerDatabaseHandler is already registered. There can be only one to make it clear which one is used.');
    }
    registerSingleton(diContainer, EXPLICIT_BLOCKS_TO_INDEX_DI_TOKEN, explicitBlocksToIndexProvider);
}
