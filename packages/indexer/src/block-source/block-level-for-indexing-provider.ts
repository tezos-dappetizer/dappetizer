import * as taquitoRpc from '@taquito/rpc';
import {
    AsyncIterableProvider,
    RPC_BLOCK_MONITOR_DI_TOKEN,
    RpcBlockMonitor,
    TAQUITO_RPC_CLIENT_DI_TOKEN,
} from '@tezos-dappetizer/utils';
import { range } from 'lodash';
import { inject, singleton } from 'tsyringe';

import { StartIndexingBlockLevel } from './start-indexing-block-level';

/** Main logic which provides levels of blocks to be indexed - historical blocks firstly, then new blocks from Tezos monitor. */
@singleton()
export class BlockLevelForIndexingProvider implements AsyncIterableProvider<number> {
    constructor(
        private readonly startIndexingBlockLevel: StartIndexingBlockLevel,
        @inject(TAQUITO_RPC_CLIENT_DI_TOKEN) private readonly rpcClient: taquitoRpc.RpcClient,
        @inject(RPC_BLOCK_MONITOR_DI_TOKEN) private readonly blockMonitor: RpcBlockMonitor,
    ) {}

    async *iterate(): AsyncIterableIterator<number> {
        const headHeader = await this.rpcClient.getBlockHeader();
        const currentHeadLevel = headHeader.level;
        let nextLevelToIndex = await this.startIndexingBlockLevel.resolveLevel();

        if (nextLevelToIndex > currentHeadLevel + 1) {
            throw new Error(`Start block level to index '${nextLevelToIndex}' is the future`
                + ` (greater than head level '${currentHeadLevel}') so it cannot be indexed.`);
        }

        yield* advanceTo(currentHeadLevel);

        for await (const rpcMonitorHeader of this.blockMonitor.iterate()) {
            yield* advanceTo(rpcMonitorHeader.level);
        }

        function *advanceTo(headLevel: number): Iterable<number> {
            if (headLevel < nextLevelToIndex) {
                return; // Most likely reorg.
            }

            const levels = range(nextLevelToIndex, headLevel + 1);
            nextLevelToIndex = headLevel + 1;
            yield* levels;
        }
    }
}
