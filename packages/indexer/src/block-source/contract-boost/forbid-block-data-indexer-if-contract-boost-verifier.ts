import { keyof } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { IndexerModule } from '../../client-indexers/indexer-module';
import { PartialIndexerModuleVerifier } from '../../client-indexers/verification/partial-indexer-module-verifier';
import { IndexingConfig } from '../../config/indexing/indexing-config';

@singleton()
export class ForbidBlockDataIndexerIfContractBoostVerifier implements PartialIndexerModuleVerifier {
    constructor(
        private readonly config: IndexingConfig,
    ) {}

    verify(module: IndexerModule<unknown>): void {
        if (this.config.contractBoost && (module.blockDataIndexers?.length ?? 0) > 0) {
            throw new Error(`If contract boost is enabled (currently '${this.config.contractBoost.type}'),`
                + ` then indexer modules cannot define ${keyof<typeof module>('blockDataIndexers')}`
                + ` because some blocks (without configured contracts) would be skipped.`);
        }
    }
}
