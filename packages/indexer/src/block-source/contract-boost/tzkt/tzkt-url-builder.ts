import { appendUrl, isReadOnlyArray } from '@tezos-dappetizer/utils';
import { ArrayOrSingle, DeepReadonly } from 'ts-essentials';

export const MAX_LIMIT = 10_000;

export function buildTzktUrl(
    apiUrl: string,
    path: string,
    query: DeepReadonly<Record<string, ArrayOrSingle<string | number>>> = {},
): string {
    const queryStr = Object.entries(query)
        .map(([name, valueOrValues]) => {
            const values = isReadOnlyArray(valueOrValues) ? valueOrValues : [valueOrValues];
            return `${name}=${values.map(v => v.toString()).join(',')}`;
        }).join('&');

    return appendUrl(apiUrl, queryStr ? `${path}?${queryStr}` : path);
}
