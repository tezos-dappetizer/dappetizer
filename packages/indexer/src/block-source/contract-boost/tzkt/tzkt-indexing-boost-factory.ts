import { JSON_FETCHER_DI_TOKEN, JsonFetcher } from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

import { TzktIndexingBoost } from './tzkt-indexing-boost';
import { TzktContractsBoostConfig } from '../../../config/indexing/indexing-config';

@singleton()
export class TzktIndexingBoostFactory {
    constructor(
        @inject(JSON_FETCHER_DI_TOKEN) private readonly fetcher: JsonFetcher,
    ) {}

    create(config: TzktContractsBoostConfig, contractAddresses: readonly string[]): TzktIndexingBoost {
        return new TzktIndexingBoost(this.fetcher, config, contractAddresses);
    }
}
