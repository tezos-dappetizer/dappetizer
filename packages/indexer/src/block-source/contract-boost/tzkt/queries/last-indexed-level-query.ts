import { isObjectLiteral, isWhiteSpace, JsonFetchCommand, keyof } from '@tezos-dappetizer/utils';

import { buildTzktUrl } from '../tzkt-url-builder';

export interface TzktBlock {
    level: number;
    chainId: string;
}

export class LastIndexedLevelQuery implements JsonFetchCommand<TzktBlock> {
    readonly description = 'get TzKT last indexed block';
    readonly url: string;

    constructor(
        readonly apiUrl: string,
    ) {
        this.url = buildTzktUrl(this.apiUrl, '/v1/head');
    }

    transformResponse(rawResponse: TzktBlock): TzktBlock {
        if (!isObjectLiteral(rawResponse)) {
            throw new Error('It must be an object.');
        }
        if (typeof rawResponse.level !== 'number' || rawResponse.level < 0) {
            throw new Error(`Invalid ${keyof<typeof rawResponse>('level')}.`);
        }
        if (typeof rawResponse.chainId !== 'string' || isWhiteSpace(rawResponse.chainId)) {
            throw new Error(`Invalid ${keyof<typeof rawResponse>('chainId')}.`);
        }
        return rawResponse;
    }
}
