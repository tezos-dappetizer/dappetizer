import { JsonFetchCommand } from '@tezos-dappetizer/utils';

import { BoostLevelsOptions } from '../../contract-indexing-boost';
import { buildTzktUrl, MAX_LIMIT } from '../tzkt-url-builder';

export interface TransactionBlockLevels {
    levels: number[];
    hasMore: boolean;
}

export class TransactionBlockLevelsQuery implements JsonFetchCommand<TransactionBlockLevels> {
    readonly description: string;
    readonly url: string;

    constructor(
        readonly apiUrl: string,
        readonly contractAddresses: readonly string[],
        readonly options: BoostLevelsOptions,
        readonly limit = MAX_LIMIT,
    ) {
        this.description = `get TzKT transaction block levels from ${options.fromBlockLevel}`;
        this.url = buildTzktUrl(apiUrl, '/v1/operations/transactions', {
            ['level.ge']: options.fromBlockLevel,
            ['select.values']: 'level',
            ['limit']: limit,
            ['target.in']: contractAddresses,
            ['sort.asc']: 'level',
        });
    }

    transformResponse(rawResponse: unknown): TransactionBlockLevels {
        if (!Array.isArray(rawResponse)) {
            throw new Error('It must be an array.');
        }
        const levels = rawResponse.map(level => {
            if (typeof level !== 'number' || level < 0) {
                throw new Error(`Invalid level: ${JSON.stringify(level)}.`);
            }
            return level;
        });

        const hasMore = levels.length >= this.limit;
        return { levels, hasMore };
    }
}
