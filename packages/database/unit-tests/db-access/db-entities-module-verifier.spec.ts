import { Writable } from 'ts-essentials';
import * as typeorm from 'typeorm';

import { expectToThrow } from '../../../../test-utilities/mocks';
import { DbEntitiesModuleVerifier } from '../../src/db-access/db-entities-module-verifier';
import { IndexerModuleUsingDb } from '../../src/indexer-module-using-db';

class FooEntity {}

class EntitySchema {}

describe(DbEntitiesModuleVerifier.name, () => {
    let target: DbEntitiesModuleVerifier;
    let module: Writable<IndexerModuleUsingDb>;

    const act = () => target.verify(module);

    beforeEach(() => {
        target = new DbEntitiesModuleVerifier();
        module = {
            name: 'ignored',
            dbEntities: [FooEntity, 'bar_table', new typeorm.EntitySchema(null!)],
        };
    });

    it('should pass if correct db module', act);

    it('should pass if module is not using db', () => {
        delete (module as any).dbEntities;

        act();
    });

    it('should throw if dbEntities are not an array', () => {
        module.dbEntities = 'wtf' as any;

        const error = expectToThrow(act);

        expect(error.message).toIncludeMultiple([`'dbEntities'`, `must be an array of entities but it is 'string'`]);
    });

    it('should throw if invalid entity', () => {
        module.dbEntities = [FooEntity, 123 as any];

        const error = expectToThrow(act);

        expect(error.message).toIncludeMultiple([`'dbEntities[1]'`, `instance of 'typeorm.EntitySchema'`, `it is 'number'`]);
    });

    it('should throw if EntitySchema from different typeorm', () => {
        module.dbEntities = [FooEntity, 'bar_table', new EntitySchema() as any];

        const error = expectToThrow(act);

        expect(error.message).toIncludeMultiple([`'dbEntities[2]'`, `'typeorm.EntitySchema'`, `different 'typeorm'`]);
    });
});
