import { instance, mock, verify, when } from 'ts-mockito';
import { DataSource } from 'typeorm';

import { describeMemberFactory, expectToThrowAsync } from '../../../../test-utilities/mocks';
import { DbDataSourceConnector } from '../../src/db-access/db-data-source-connector';
import { DbDataSourceManager } from '../../src/db-access/db-data-source-manager';

describe(DbDataSourceManager.name, () => {
    let target: DbDataSourceManager;
    let connector: DbDataSourceConnector;
    let connectedDataSource: DataSource;

    beforeEach(() => {
        connector = mock();
        target = new DbDataSourceManager(instance(connector));

        connectedDataSource = mock(DataSource);
        when(connector.connect()).thenResolve(instance(connectedDataSource));
    });

    const describeMember = describeMemberFactory<typeof target>();

    describeMember('getDataSource', () => {
        it('should connect', async () => runSuccessTest());

        it('should get existing data source if already connected', async () => {
            await target.getDataSource();
            await runSuccessTest();
        });

        it('should get existing data source if worker has been started', async () => {
            await target.start();
            await runSuccessTest();
        });

        async function runSuccessTest() {
            const dataSource = await target.getDataSource();

            expect(dataSource).toBe(instance(connectedDataSource));
            verify(connector.connect()).once();
        }

        it('should throw if worker already stopped', async () => {
            await target.stop();
            await runThrowTest();
        });

        it('should throw if worker started and stopped', async () => {
            await target.start();
            await target.stop();
            await runThrowTest();
        });

        async function runThrowTest() {
            const error = await expectToThrowAsync(async () => target.getDataSource());

            expect(error.message).toContain('already destroyed');
        }
    });

    describeMember('start', () => {
        it('should connect', async () => {
            await target.start();

            verify(connector.connect()).once();
        });
    });

    describeMember('stop', () => {
        it('should destroy the data source', async () => {
            await target.start();

            await target.stop();

            verify(connectedDataSource.destroy()).once();
        });

        it('should do nothing if not connected', async () => {
            await target.stop();

            verify(connector.connect()).never();
        });
    });
});
