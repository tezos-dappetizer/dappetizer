import { DataSourceOptions } from 'typeorm';

import { describeMember, expectToThrow } from '../../../../test-utilities/mocks';
import { ConfigObjectElement } from '../../../utils/src/public-api';
import { DbConfig } from '../../src/db-access/db-config';
import { DappetizerConfigUsingDb } from '../../src/helpers/dappetizer-config-using-db';

describe(DbConfig.name, () => {
    const getTarget = (database: any) => new DbConfig(new ConfigObjectElement({ database }, 'config.json', 'prop.path'));

    describe('constructor', () => {
        describe('should apply common default correctly', () => {
            it.each([
                ['no', {}, true],
                ['undefined', { synchronize: undefined }, true],
                ['null', { synchronize: null }, true],
                ['true', { synchronize: true }, true],
                ['false', { synchronize: false }, false],
            ])('if %s synchronize', (_desc, inputOptions, expectedSynchronize) => {
                const target = getTarget({
                    file: 'C:/indexer.db',
                    ...inputOptions,
                });

                expect(target.dataSourceOptions).toEqual({
                    file: 'C:/indexer.db',
                    synchronize: expectedSynchronize,
                });
            });
        });

        describe('should apply postgres default correctly', () => {
            it.each([
                ['no', {}, 10_000],
                ['undefined', { connectTimeoutMS: undefined }, 10_000],
                ['null', { connectTimeoutMS: null! }, 10_000],
                ['explicit', { connectTimeoutMS: 666 }, 666],
            ])('if %s connectTimeoutMS', (_desc, inputOptions, expectedConnectTimeoutMS) => {
                const target = getTarget({
                    type: 'postgres',
                    host: 'indexer.db',
                    ...inputOptions,
                });

                expect(target.dataSourceOptions).toEqual<DataSourceOptions>({
                    type: 'postgres',
                    host: 'indexer.db',
                    connectTimeoutMS: expectedConnectTimeoutMS,
                    synchronize: true,
                });
            });
        });

        it('should throw if not object', () => {
            const error = expectToThrow(() => getTarget('WTF'));

            expect(error.message).toIncludeMultiple(['object', 'WTF', 'config.json', 'prop.path']);
        });
    });

    describeMember<DbConfig>('getDiagnostics', () => {
        it('should sanitize password', () => {
            const options: DataSourceOptions = {
                type: 'postgres',
                password: 'secret',
                connectTimeoutMS: 66,
                synchronize: true,
            };
            const target = getTarget(options);

            const diagnostics = target.getDiagnostics();

            expect(diagnostics[0]).toBe(DbConfig.ROOT_NAME);
            expect(diagnostics[1]).toEqual<DataSourceOptions>({
                type: 'postgres',
                password: '***',
                connectTimeoutMS: 66,
                synchronize: true,
            });
        });

        it('should just pass if no password', () => {
            const options: DataSourceOptions = {
                type: 'sqlite',
                synchronize: true,
                database: 'C:/indexer.db',
            };
            const target = getTarget(options);

            const diagnostics = target.getDiagnostics();

            expect(diagnostics[0]).toBe(DbConfig.ROOT_NAME);
            expect(diagnostics[1]).toEqual(options);
        });
    });

    it('should expose its properties in DappetizerConfig', () => {
        expect(DbConfig.ROOT_NAME).toBe<keyof DappetizerConfigUsingDb>('database');
    });
});
