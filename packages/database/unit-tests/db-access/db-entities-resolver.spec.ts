import { Writable } from 'ts-essentials';
import { EntitySchema } from 'typeorm';

import { TestLogger } from '../../../../test-utilities/mocks';
import { DbEntitiesResolver } from '../../src/db-access/db-entities-resolver';
import { DAPPETIZER_DB_ENTITIES } from '../../src/entities/dappetizer-db-entities';
import { IndexerModuleUsingDb } from '../../src/indexer-module-using-db';

class FooEntity1 {}
class FooEntity2 {}
class BarEntity {}

describe(DbEntitiesResolver.name, () => {
    let target: DbEntitiesResolver;
    let fooModule: Writable<IndexerModuleUsingDb>;
    let barModule: Writable<IndexerModuleUsingDb>;
    let logger: TestLogger;

    beforeEach(() => {
        fooModule = { name: 'Foo', dbEntities: [FooEntity1, FooEntity2] } as IndexerModuleUsingDb;
        barModule = { name: 'Bar', dbEntities: [BarEntity] } as IndexerModuleUsingDb;
        logger = new TestLogger();
        target = new DbEntitiesResolver([fooModule, barModule], logger);
    });

    const loggedDappetizerEntities = { '@tezos-dappetizer': DAPPETIZER_DB_ENTITIES.map(e => e.name) };

    it('should collect entities from all modules', () => {
        const entities = target.resolve();

        expect(entities).toIncludeSameMembers([...DAPPETIZER_DB_ENTITIES, FooEntity1, FooEntity2, BarEntity]);
        logger.loggedSingle().verify('Information', {
            dbEntities: {
                ...loggedDappetizerEntities,
                Foo: [FooEntity1.name, FooEntity2.name],
                Bar: [BarEntity.name],
            },
        });
    });

    it('should support other entity types', () => {
        const entitySchema = { options: { name: 'SchemaEntity' } } as EntitySchema;
        fooModule.dbEntities = [entitySchema];
        barModule.dbEntities = ['StrEntity'];

        const entities = target.resolve();

        expect(entities).toIncludeSameMembers([...DAPPETIZER_DB_ENTITIES, entitySchema, 'StrEntity']);
        logger.loggedSingle().verify('Information', {
            dbEntities: {
                ...loggedDappetizerEntities,
                Foo: ['SchemaEntity'],
                Bar: ['StrEntity'],
            },
        });
    });
});
