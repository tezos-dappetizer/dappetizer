import { assert, IsExact } from 'conditional-type-checks';
import { DataSourceOptions, MixedList } from 'typeorm';

import { DbEntity } from '../../src/db-access/db-entity';

assert<IsExact<MixedList<DbEntity> | undefined, DataSourceOptions['entities']>>(true);
