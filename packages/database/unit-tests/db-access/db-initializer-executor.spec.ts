import { anything, instance, mock, verify, when } from 'ts-mockito';
import { DataSource } from 'typeorm';

import { describeMember, expectToThrowAsync, TestLogger, verifyCalled } from '../../../../test-utilities/mocks';
import { LongExecutionHelper, LongExecutionOperation } from '../../../utils/src/public-api';
import { DbInitializer } from '../../src/db-access/db-initializer';
import { DbInitializerExecutor } from '../../src/db-access/db-initializer-executor';

describe(DbInitializerExecutor.name, () => {
    let target: DbInitializerExecutor;
    let longExecutionHelper: LongExecutionHelper;
    let logger: TestLogger;
    let initializer1: DbInitializer;
    let initializer2: DbInitializer;
    let initializer3: DbInitializer;

    let dataSource: DataSource;

    beforeEach(() => {
        longExecutionHelper = mock();
        logger = new TestLogger();
        [initializer1, initializer2, initializer3] = [mock(), mock(), mock()];

        when(initializer1.order).thenReturn(3);
        when(initializer2.order).thenReturn(undefined);
        when(initializer3.order).thenReturn(-1);

        target = new DbInitializerExecutor(
            instance(longExecutionHelper),
            logger,
            [instance(initializer1), instance(initializer2), instance(initializer3)],
        );

        dataSource = 'mockedDataSource' as any;

        when(initializer1.name).thenReturn('first');
        when(initializer2.name).thenReturn('second');
        when(initializer3.name).thenReturn('third');

        when(initializer2.beforeSynchronization).thenReturn(undefined);
        when(initializer3.afterSynchronization).thenReturn(undefined);

        when(longExecutionHelper.warn(anything())).thenCall(o => o.execute());
    });

    describeMember<typeof target>('beforeSynchronization', () => {
        const act = async () => target.beforeSynchronization(dataSource);

        it('should execute all initializers in order', async () => {
            await act();

            verify(initializer3.beforeSynchronization!(dataSource))
                .calledBefore(initializer1.beforeSynchronization!(dataSource));
            verifyCalled<LongExecutionOperation>(longExecutionHelper.warn)
                .with({ description: 'beforeSynchronization(...) of DbInitializer: third' } as LongExecutionOperation)
                .thenWith({ description: 'beforeSynchronization(...) of DbInitializer: first' } as LongExecutionOperation)
                .thenNoMore();

            logger.logged(0).verify('Information', { initializers: ['third', 'second', 'first'] });
            logger.logged(1).verify('Debug', { initializer: 'third', method: 'beforeSynchronization' }).verifyMessage('Starting');
            logger.logged(2).verify('Debug', { initializer: 'third', method: 'beforeSynchronization' }).verifyMessage('finished');
            logger.logged(3).verify('Debug', { initializer: 'second', method: 'beforeSynchronization' }).verifyMessage('nothing');
            logger.logged(4).verify('Debug', { initializer: 'first', method: 'beforeSynchronization' }).verifyMessage('Starting');
            logger.logged(5).verify('Debug', { initializer: 'first', method: 'beforeSynchronization' }).verifyMessage('finished');
            logger.verifyLoggedCount(6);
        });

        it('should wrap errors', async () => {
            when(initializer3.beforeSynchronization!(dataSource)).thenReject(new Error('oups'));

            const error = await expectToThrowAsync(act);

            expect(error.message).toIncludeMultiple([`'third'`, 'beforeSynchronization(...)', 'oups']);
        });
    });

    describeMember<typeof target>('afterSynchronization', () => {
        const act = async () => target.afterSynchronization(dataSource);

        it('should execute all initializers in order', async () => {
            await act();

            verify(initializer2.afterSynchronization!(dataSource))
                .calledBefore(initializer1.afterSynchronization!(dataSource));
            verifyCalled<LongExecutionOperation>(longExecutionHelper.warn)
                .with({ description: 'afterSynchronization(...) of DbInitializer: second' } as LongExecutionOperation)
                .thenWith({ description: 'afterSynchronization(...) of DbInitializer: first' } as LongExecutionOperation)
                .thenNoMore();

            logger.logged(0).verify('Debug', { initializer: 'third', method: 'afterSynchronization' }).verifyMessage('nothing');
            logger.logged(1).verify('Debug', { initializer: 'second', method: 'afterSynchronization' }).verifyMessage('Starting');
            logger.logged(2).verify('Debug', { initializer: 'second', method: 'afterSynchronization' }).verifyMessage('finished');
            logger.logged(3).verify('Debug', { initializer: 'first', method: 'afterSynchronization' }).verifyMessage('Starting');
            logger.logged(4).verify('Debug', { initializer: 'first', method: 'afterSynchronization' }).verifyMessage('finished');
            logger.verifyLoggedCount(5);
        });

        it('should wrap errors', async () => {
            when(initializer2.afterSynchronization!(dataSource)).thenReject(new Error('oups'));

            const error = await expectToThrowAsync(act);

            expect(error.message).toIncludeMultiple([`'second'`, 'afterSynchronization(...)', 'oups']);
        });
    });
});
