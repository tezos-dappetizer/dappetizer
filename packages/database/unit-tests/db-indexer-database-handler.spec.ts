import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';
import { DataSource, QueryRunner } from 'typeorm';

import { describeMemberFactory, fixedMock } from '../../../test-utilities/mocks';
import { Block } from '../../indexer/src/rpc-data/rpc-data-interfaces';
import { DbCommand } from '../src/commands/db-command';
import { DeleteBlockByHashCommand } from '../src/commands/delete-block-by-hash-command';
import { FindBlockByLevelCommand } from '../src/commands/find-block-by-level-command';
import { FindLatestBlockCommand } from '../src/commands/find-latest-block-command';
import { InsertCommand } from '../src/commands/insert-command';
import { DbDataSourceProvider } from '../src/db-access/db-data-source-provider';
import { DbContext } from '../src/db-context';
import { DbIndexerDatabaseHandler } from '../src/db-indexer-database-handler';
import { DbBlock } from '../src/entities/db-block';
import { DbDappetizerSettingsManager } from '../src/helpers/db-dappetizer-settings-manager';
import { DbExecutorImpl } from '../src/helpers/db-executor-impl';

describe(DbIndexerDatabaseHandler.name, () => {
    let target: DbIndexerDatabaseHandler;
    let dbConnectionProvider: DbDataSourceProvider;
    let dbExecutor: DbExecutorImpl;
    let dbSettingsManager: DbDappetizerSettingsManager;

    beforeEach(() => {
        [dbConnectionProvider, dbExecutor, dbSettingsManager] = [mock(), mock(), mock()];
        target = new DbIndexerDatabaseHandler(instance(dbConnectionProvider), instance(dbExecutor), instance(dbSettingsManager));
    });

    const describeMember = describeMemberFactory<typeof target>();

    describeMember('startTransaction', () => {
        it('should create connected db context with transaction', async () => {
            const [dbConnection, dbRunner] = [fixedMock<DataSource>(), fixedMock<QueryRunner>()];
            when(dbConnectionProvider.getDataSource()).thenResolve(instance(dbConnection));
            when(dbConnection.createQueryRunner()).thenReturn(instance(dbRunner));
            when(dbRunner.manager).thenReturn('mockedManager' as any);

            const dbContext = await target.startTransaction();

            expect(dbContext.globalDataSource).toBe(instance(dbConnection));
            expect(dbContext.transaction).toBe('mockedManager');
            expect(dbContext.transactionRunner).toBe(instance(dbRunner));
            expect(dbContext).toBeFrozen();
        });
    });

    describeMember('commitTransaction', () => {
        it('should commit and release', async () => {
            const { dbContext, dbRunner } = mockDbContext();

            await target.commitTransaction(dbContext);

            verify(dbRunner.commitTransaction()).once();
            verify(dbRunner.release()).once();
        });
    });

    describeMember('rollBackTransaction', () => {
        it('should roll back and release', async () => {
            const { dbContext, dbRunner } = mockDbContext();

            await target.rollBackTransaction(dbContext);

            verify(dbRunner.rollbackTransaction()).once();
            verify(dbRunner.release()).once();
        });
    });

    describeMember('insertBlock', () => {
        it('should insert specified block', async () => {
            const { dbContext } = mockDbContext();
            const block = {
                chainId: 'ccc',
                hash: 'haha',
                level: 66,
                predecessorBlockHash: 'pred',
                timestamp: new Date(123456),
            } as Block;

            await target.insertBlock(block, dbContext);

            verify(dbSettingsManager.setChainId('ccc')).once();
            verifyCmdExecuted(dbContext, new InsertCommand(DbBlock, {
                hash: 'haha',
                level: 66,
                predecessor: 'pred',
                timestamp: new Date(123456),
            }));
        });
    });

    describeMember('deleteBlock', () => {
        it('should delete block by specified hash', async () => {
            const { dbContext } = mockDbContext();

            await target.deleteBlock({ hash: 'haha', level: 123 }, dbContext);

            verifyCmdExecuted(dbContext, new DeleteBlockByHashCommand('haha'));
        });
    });

    describeMember('getIndexedChainId', () => {
        it('should get chain id from db settings', async () => {
            when(dbSettingsManager.getSettings()).thenResolve({ chainId: 'ccc' });

            expect(await target.getIndexedChainId()).toBe('ccc');
        });

        it('should get null if no settings saved yet', async () => {
            expect(await target.getIndexedChainId()).toBeNull();
        });
    });

    describeMember('getLastIndexedBlock', () => {
        it('should get last indexed block', async () => {
            when(dbExecutor.executeNoTransaction(deepEqual(new FindLatestBlockCommand()))).thenResolve('mockedLatestBlock' as any);

            const block = await target.getLastIndexedBlock();

            expect(block).toBe('mockedLatestBlock');
        });
    });

    describeMember('getIndexedBlock', () => {
        it('should get indexed block by level', async () => {
            when(dbExecutor.executeNoTransaction(deepEqual(new FindBlockByLevelCommand(123)))).thenResolve('mockedBlock123' as any);

            const block = await target.getIndexedBlock(123);

            expect(block).toBe('mockedBlock123');
        });
    });

    function mockDbContext() {
        const dbRunner = mock<QueryRunner>();
        const dbContext = {
            transactionRunner: instance(dbRunner),
            transaction: 'mockedTransaction' as any,
        } as DbContext;
        return { dbContext, dbRunner };
    }

    function verifyCmdExecuted(dbContext: DbContext, expectedCmd: DbCommand<unknown>) {
        verify(dbExecutor.execute(anything(), anything())).once();
        verify(dbExecutor.execute(dbContext.transaction, deepEqual(expectedCmd))).once();
    }
});
