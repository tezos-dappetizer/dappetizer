import { container as globalContainer, DependencyContainer } from 'tsyringe';

import {
    shouldResolveImplsForDappetizerFeatures,
    shouldResolvePublicApi,
    throwShouldNotBeCalled,
} from '../../../test-utilities/mocks';
import { INDEXER_DATABASE_HANDLER_DI_TOKEN } from '../../indexer/src/client-indexers/indexer-database-handler-di-token';
import { CONTRACT_PROVIDER_DI_TOKEN, INDEXER_MODULES_DI_TOKEN } from '../../indexer/src/public-api';
import { registerExplicitConfigFilePath, RPC_BLOCK_MONITOR_DI_TOKEN } from '../../utils/src/public-api';
import { DbConfig } from '../src/db-access/db-config';
import { DbEntitiesModuleVerifier } from '../src/db-access/db-entities-module-verifier';
import { DbSchemaInitializer } from '../src/db-access/db-schema-initializer';
import { registerDappetizerWithDatabase } from '../src/dependency-injection';
import { HasuraConfig } from '../src/hasura/config/hasura-config';
import { HasuraInitializer } from '../src/hasura/hasura-initializer';
import { DB_EXECUTOR_DI_TOKEN } from '../src/helpers/db-executor';
import { DATABASE_PACKAGE_JSON_DI_TOKEN, DB_DATA_SOURCE_PROVIDER_DI_TOKEN } from '../src/public-api';

describe('dependency injection', () => {
    let diContainer: DependencyContainer;

    beforeAll(() => {
        diContainer = globalContainer.createChildContainer();
        registerDappetizerWithDatabase(diContainer);
        registerExplicitConfigFilePath(diContainer, { useValue: '../../dappetizer.config.ts' });

        diContainer.registerInstance(INDEXER_MODULES_DI_TOKEN, {
            name: 'Dummy',
            indexingCycleHandler: { createContextData: throwShouldNotBeCalled },
            blockDataIndexers: [{ indexBlock: throwShouldNotBeCalled }],
        });
    });

    shouldResolvePublicApi(() => diContainer, [
        [DATABASE_PACKAGE_JSON_DI_TOKEN, 'frozen'],
        [DB_DATA_SOURCE_PROVIDER_DI_TOKEN, 'frozen'],
        [DB_EXECUTOR_DI_TOKEN, 'frozen'],
        INDEXER_DATABASE_HANDLER_DI_TOKEN,

        CONTRACT_PROVIDER_DI_TOKEN, // Should register Dappetizer Indexer too.
        RPC_BLOCK_MONITOR_DI_TOKEN, // Should register Dappetizer Utils too.
    ]);

    shouldResolveImplsForDappetizerFeatures(() => diContainer, {
        backgroundWorkerNames: ['DbConnection'],
        configsWithDiagnostics: [DbConfig, HasuraConfig],
        dbInitializers: [DbSchemaInitializer, HasuraInitializer],
        partialIndexerModuleVerifiers: [DbEntitiesModuleVerifier],
    });
});
