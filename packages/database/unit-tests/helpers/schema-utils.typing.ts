import { assert, IsExact } from 'conditional-type-checks';
import { DataSourceOptions } from 'typeorm';

import { DataSourceOptionsWithSchema } from '../../src/helpers/schema-utils';

// eslint-disable-next-line @typescript-eslint/ban-types
assert<IsExact<DataSourceOptionsWithSchema, Extract<DataSourceOptions, { schema?: string }>>>(true);
