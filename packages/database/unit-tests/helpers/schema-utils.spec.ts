import { DataSourceOptions } from 'typeorm';

import { expectToThrow } from '../../../../test-utilities/mocks';
import { canContainSchema, DataSourceOptionsWithSchema, resolveSchema } from '../../src/helpers/schema-utils';

describe(canContainSchema.name, () => {
    const withSchemaTestCases: Record<DataSourceOptionsWithSchema['type'], true> = {
        cockroachdb: true,
        mssql: true,
        oracle: true,
        postgres: true,
        sap: true,
        spanner: true,
    };
    const withoutSchemaTestCases: Record<Exclude<DataSourceOptions, DataSourceOptionsWithSchema>['type'], false> = {
        'aurora-mysql': false,
        'aurora-postgres': false,
        'better-sqlite3': false,
        'react-native': false,
        capacitor: false,
        cordova: false,
        expo: false,
        mariadb: false,
        mongodb: false,
        mysql: false,
        nativescript: false,
        sqlite: false,
        sqljs: false,
    };

    it.each([
        ...Object.entries(withSchemaTestCases),
        ...Object.entries(withoutSchemaTestCases),
    ])('should evaluate db %s to %s', (type, expected) => {
        const options = { type, synchronize: true } as DataSourceOptions;

        expect(canContainSchema(options)).toBe(expected);
    });
});

describe(resolveSchema.name, () => {
    it.each([
        ['schema property', 'sss', { type: 'postgres', schema: 'sss' } as DataSourceOptions],
        ['default if no schema specified', 'public', { type: 'postgres' } as DataSourceOptions],
        ['default if null schema specified', 'public', { type: 'postgres', schema: null! } as DataSourceOptions],
        ['default if empty schema specified', 'public', { type: 'postgres', schema: '' } as DataSourceOptions],
        ['null if schema not supported', null, { type: 'sqlite' } as DataSourceOptions],
        ['null if schema not supported ignoring schema property', null, { type: 'sqlite', schema: 'ignored' } as any],
    ])('should get %s', (_desc, expected, options) => {
        expect(resolveSchema(options)).toBe(expected);
    });

    it('should throw if white-space schema', () => {
        const options = { type: 'postgres', schema: '  ' } as DataSourceOptions;

        const error = expectToThrow(() => resolveSchema(options));

        expect(error.message).toIncludeMultiple([`schema`, `'postgres'`, `'  '`]);
    });
});
