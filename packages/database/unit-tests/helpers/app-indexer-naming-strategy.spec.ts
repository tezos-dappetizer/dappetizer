import { describeMemberFactory } from '../../../../test-utilities/mocks';
import { DappetizerNamingStrategy } from '../../src/helpers/dappetizer-naming-strategy';

describe(DappetizerNamingStrategy.name, () => {
    const target = new DappetizerNamingStrategy();

    const describeMember = describeMemberFactory<typeof target>();

    describeMember('tableName', () => {
        it.each([
            ['basic class', 'FooBar', 'foo_bar'],
            ['db prefix', 'DbOmgLol', 'omg_lol'],
            ['fake prefix', 'Dbwtf', 'dbwtf'],
            ['only prefix', 'Db', 'db'],
        ])('should get correct name from class name if %s', (_desc, className, expected) => {
            const name = target.tableName(className, '');

            expect(name).toBe(expected);
        });

        it.each([undefined, null, ''])('should handle %s custom name', customName => {
            const name = target.tableName('ClassName', customName!);

            expect(name).toBe('class_name');
        });

        it('should honor custom name if specified', () => {
            const name = target.tableName('ClassName', 'CustomName');

            expect(name).toBe('CustomName');
        });
    });

    describeMember('primaryKeyName', () => {
        it('should return primary key name', () => {
            const columnNames = ['Jee', 'Dee', 'Bee'];

            const key = target.primaryKeyName('TableName', columnNames);

            expect(key).toBe('PK_table_name_bee_dee_jee');
        });

        it('should return trimmed primary key name', () => {
            const columnNames = ['TheBirdIsTheWord', 'Another-Very-Long-Column-Name', 'NoTVAndNoBeerMakeHomerGoCrazy'];

            const key = target.primaryKeyName('SchemaName.TableName', columnNames);

            expect(key).toBe('PK_schema_name_table_name_another_very_long_column_name_no_tv_a');
        });
    });

    describeMember('foreignKeyName', () => {
        it('should return foreig key name', () => {
            const columnNames = ['Jee', 'Dee', 'Bee'];

            const key = target.foreignKeyName('TableName', columnNames);

            expect(key).toBe('FK_table_name_bee_dee_jee');
        });

        it('should return trimmed foreign key name', () => {
            const columnNames = ['TheBirdIsTheWord', 'Another-Very-Long-Column-Name', 'NoTVAndNoBeerMakeHomerGoCrazy'];

            const key = target.foreignKeyName('SchemaName.TableName', columnNames);

            expect(key).toBe('FK_schema_name_table_name_another_very_long_column_name_no_tv_a');
        });
    });
});
