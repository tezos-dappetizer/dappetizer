import { Writable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';
import { DataSource, DataSourceOptions } from 'typeorm';

import { expectToThrowAsync, TestLogger } from '../../../../test-utilities/mocks';
import { EnabledHasuraConfig, HasuraConfig } from '../../src/hasura/config/hasura-config';
import { HasuraHealthApi } from '../../src/hasura/hasura-health-api';
import { HasuraInitializer } from '../../src/hasura/hasura-initializer';
import { HasuraTableMapping } from '../../src/hasura/hasura-mappings';
import { HasuraMetadataApi } from '../../src/hasura/hasura-metadata-api';
import { IndexerEntitiesApi } from '../../src/hasura/indexer-entities-api';

describe(HasuraInitializer.name, () => {
    let target: HasuraInitializer;
    let config: Writable<HasuraConfig>;
    let indexerEntitiesApi: IndexerEntitiesApi;
    let healthApi: HasuraHealthApi;
    let metadataApi: HasuraMetadataApi;
    let logger: TestLogger;

    let dataSource: DataSource & { options: DataSourceOptions };
    let tablesWithRelationships: HasuraTableMapping[];

    const act = async () => target.afterSynchronization(dataSource);

    beforeEach(() => {
        [indexerEntitiesApi, healthApi, metadataApi] = [mock(), mock(), mock()];
        config = {} as HasuraConfig;
        logger = new TestLogger();
        target = new HasuraInitializer(config, instance(indexerEntitiesApi), instance(healthApi), instance(metadataApi), logger);

        config.value = {
            url: 'http://hasura',
            dropExistingTracking: true,
        } as EnabledHasuraConfig;
        dataSource = {
            options: {
                type: 'postgres',
                schema: 'sss',
            },
        } as DataSource;
        tablesWithRelationships = 'mockedTables' as any;

        when(indexerEntitiesApi.getIndexerMappings(anything(), anything())).thenReturn(tablesWithRelationships);
    });

    it('should initialize Hasura correctly', async () => {
        await act();

        verify(metadataApi.clearMetadata(config.value!))
            .calledBefore(metadataApi.configureTables(config.value!, 'sss', tablesWithRelationships));
        verify(indexerEntitiesApi.getIndexerMappings(dataSource, config.value!)).once();
        verify(healthApi.ensureHealthy('http://hasura')).once();

        logger.logged(0).verify('Information').verifyMessage('Starting');
        logger.logged(1).verify('Information').verifyMessage('finished');
        logger.verifyLoggedCount(2);
    });

    it('should not clear existing metadata if the drop is disabled in config', async () => {
        config.value = { ...config.value!, dropExistingTracking: false };

        await act();

        verify(metadataApi.configureTables(config.value, 'sss', tablesWithRelationships)).once();
        verify(metadataApi.clearMetadata(anything())).never();
    });

    shouldHandleErrorsIfFailed('clear existing metadata', () => metadataApi.clearMetadata(anything()) as unknown);
    shouldHandleErrorsIfFailed('get mappings', () => indexerEntitiesApi.getIndexerMappings(anything(), anything()));
    shouldHandleErrorsIfFailed('configure tables', () => metadataApi.configureTables(anything(), anything(), anything()) as unknown);

    function shouldHandleErrorsIfFailed(desc: string, methodToThrow: () => unknown) {
        it(`should handle error when failed to ${desc}`, async () => {
            const error = new Error('oups');
            when(methodToThrow()).thenThrow(error);

            await act();

            logger.logged(0).verify('Information').verifyMessage('Starting');
            logger.logged(1).verify('Error', { error }).verifyMessage('Failed');
            logger.verifyLoggedCount(2);
        });
    }

    it('should skip initialization if Hasura is unhealthy', async () => {
        const error = new Error('oups');
        when(healthApi.ensureHealthy('http://hasura')).thenReject(error);

        await act();

        verifyNothingInitialized();
        logger.loggedSingle().verify('Error', { error }).verifyMessage('unhealthy');
    });

    it('should skip initialization if disabled config', async () => {
        config.value = null;

        await act();

        verifyNothingInitialized();
        verify(healthApi.ensureHealthy(anything())).never();
        logger.verifyNothingLogged();
    });

    it('should throw if not postgres database', async () => {
        dataSource.options = { type: 'mssql' } as DataSourceOptions;

        const error = await expectToThrowAsync(act);

        expect(error.message).toIncludeMultiple(['Hasura', `'postgres'`, `'mssql'`]);
        verifyNothingInitialized();
        verify(healthApi.ensureHealthy(anything())).never();
        logger.verifyNothingLogged();
    });

    function verifyNothingInitialized() {
        verify(indexerEntitiesApi.getIndexerMappings(anything(), anything())).never();
        verify(metadataApi.clearMetadata(anything())).never();
        verify(metadataApi.configureTables(anything(), anything(), anything())).never();
    }
});
