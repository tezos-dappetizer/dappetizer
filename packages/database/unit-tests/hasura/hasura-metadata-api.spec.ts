import { Writable } from 'ts-essentials';
import { anything, deepEqual, instance, mock, objectContaining, verify, when } from 'ts-mockito';

import { describeMember, expectToThrowAsync, TestLogger } from '../../../../test-utilities/mocks';
import { asReadonly } from '../../../utils/src/basics/public-api';
import { ClearMetadataCommand } from '../../src/hasura/commands/clear-metadata-command';
import { CreateArrayRelationshipCommand } from '../../src/hasura/commands/create-array-relationship-command';
import { CreateObjectRelationshipCommand } from '../../src/hasura/commands/create-object-relationship-command';
import { ExportMetadataCommand, HasuraMetadata } from '../../src/hasura/commands/export-metadata-command';
import { PermitCommand } from '../../src/hasura/commands/permit-command';
import { TrackCommand } from '../../src/hasura/commands/track-command';
import { EnabledHasuraConfig, HasuraRelationshipType } from '../../src/hasura/config/hasura-config';
import { HasuraCommandExecutor } from '../../src/hasura/hasura-command-executor';
import { HasuraRelationshipMapping, HasuraTableMapping } from '../../src/hasura/hasura-mappings';
import { HasuraMetadataApi } from '../../src/hasura/hasura-metadata-api';

describe(HasuraMetadataApi.name, () => {
    let target: HasuraMetadataApi;
    let config: Writable<EnabledHasuraConfig>;
    let executor: HasuraCommandExecutor;
    let logger: TestLogger;

    beforeEach(() => {
        config = {} as typeof config;
        executor = mock();
        logger = new TestLogger();
        target = new HasuraMetadataApi(instance(executor), logger);
    });

    describeMember<typeof target>('clearMetadata', () => {
        it('should execute correct command', async () => {
            await target.clearMetadata(config);

            verify(executor.execute(anything(), anything())).once();
            verify(executor.execute(config, deepEqual(new ClearMetadataCommand()))).once();
            logger.loggedSingle().verify('Information').verifyMessage('cleared');
        });
    });

    describeMember<typeof target>('configureTables', () => {
        const act = async (m: HasuraTableMapping[] = []) => target.configureTables(config, 'testSchema', m);

        beforeEach(() => {
            config.source = 'testSrc';
            config.selectLimit = 66;
            config.allowAggregations = 'bool' as any;

            when(executor.execute<HasuraMetadata>(config, deepEqual(new ExportMetadataCommand()))).thenResolve({
                sources: [
                    { name: 'testSrc', tables: [] },
                    { name: 'otherSrc', tables: [] },
                ],
            });
        });

        it('should throw if current metadata does not contain configured source', async () => {
            config.source = 'wtfSrc';

            const error = await expectToThrowAsync(act);

            expect(error.message).toIncludeMultiple([`'wtfSrc'`, `does not exist in Hasura`, `['testSrc', 'otherSrc']`]);
            verify(executor.execute(anything(), anything())).once();
            logger.verifyNothingLogged();
        });

        it('should configure tables with their relationships', async () => {
            const tableMappings = [
                {
                    tableName: 'my_table_1',
                    graphQLNames: {
                        type: 'MyTable1',
                        singleByKeyField: 'myTable1',
                        selectField: 'myTables1',
                        aggregateField: 'myTablesAggregate1',
                    },
                    relationships: asReadonly([
                        { graphQLName: 'rel1_1', relationshipType: HasuraRelationshipType.ManyToOne },
                        { graphQLName: 'rel1_2', relationshipType: HasuraRelationshipType.OneToMany },
                    ]),
                } as HasuraTableMapping,
                {
                    tableName: 'my_table_2',
                    graphQLNames: {
                        type: 'MyTable2',
                        singleByKeyField: 'myTable2',
                        selectField: 'myTables2',
                        aggregateField: 'myTablesAggregate2',
                    },
                    relationships: asReadonly([
                        { graphQLName: 'rel2', relationshipType: HasuraRelationshipType.ManyToOne },
                    ]),
                } as HasuraTableMapping,
            ];

            await act(tableMappings);

            const args = { dbSchema: 'testSchema', hasuraSource: 'testSrc' };
            const expectedCmds = [
                new ExportMetadataCommand(),
                new TrackCommand({ ...args, table: tableMappings[0]! }),
                new PermitCommand({ ...args, table: tableMappings[0]!, selectLimit: 66, allowAggregations: 'bool' as any }),
                new CreateObjectRelationshipCommand({ ...args, tableName: 'my_table_1', relationship: tableMappings[0]!.relationships[0]! }),
                new CreateArrayRelationshipCommand({ ...args, tableName: 'my_table_1', relationship: tableMappings[0]!.relationships[1]! }),
                new TrackCommand({ ...args, table: tableMappings[1]! }),
                new PermitCommand({ ...args, table: tableMappings[1]!, selectLimit: 66, allowAggregations: 'bool' as any }),
                new CreateObjectRelationshipCommand({ ...args, tableName: 'my_table_2', relationship: tableMappings[1]!.relationships[0]! }),
            ];
            verify(executor.execute(anything(), anything())).times(expectedCmds.length);
            expectedCmds.forEach(c => verify(executor.execute(config, objectContaining(c))).once());

            logger.verifyLoggedCount(5);
            logger.logged(0).verify('Information', {
                table: 'my_table_1',
                graphQLType: 'MyTable1',
                graphQLFields: ['myTable1', 'myTables1', 'myTablesAggregate1'],
            });
            logger.logged(1).verify('Information', {
                table: 'my_table_2',
                graphQLType: 'MyTable2',
                graphQLFields: ['myTable2', 'myTables2', 'myTablesAggregate2'],
            });
            logger.logged(2).verify('Information', { table: 'my_table_1', graphQLType: 'MyTable1', graphQLRelationship: 'rel1_1' });
            logger.logged(3).verify('Information', { table: 'my_table_1', graphQLType: 'MyTable1', graphQLRelationship: 'rel1_2' });
            logger.logged(4).verify('Information', { table: 'my_table_2', graphQLType: 'MyTable2', graphQLRelationship: 'rel2' });
        });

        it('should skip table if already exists in Hasura', async () => {
            when(executor.execute<HasuraMetadata>(config, deepEqual(new ExportMetadataCommand()))).thenResolve({
                sources: [{
                    name: 'testSrc',
                    tables: [
                        { table: { name: 'existing_1' } },
                        { table: { name: 'existing_2' } },
                    ],
                }],
            });
            const tableMappings = [
                {
                    tableName: 'existing_1',
                    relationships: asReadonly([
                        { graphQLName: 'rel1_1', relationshipType: HasuraRelationshipType.ManyToOne },
                        { graphQLName: 'rel1_2', relationshipType: HasuraRelationshipType.OneToMany },
                    ]),
                } as HasuraTableMapping,
                {
                    tableName: 'existing_2',
                    relationships: asReadonly<HasuraRelationshipMapping>([]),
                } as HasuraTableMapping,
            ];

            await act(tableMappings);

            verify(executor.execute(anything(), anything())).once();
            logger.loggedSingle()
                .verify('Information', { tables: ['existing_1', 'existing_2'], configProperty: 'hasura.dropExistingTracking' })
                .verifyMessage('skipping');
        });
    });
});
