import { Response } from 'node-fetch';
import { anything, instance, mock, when } from 'ts-mockito';

import { expectToThrowAsync, verifyCalled } from '../../../../test-utilities/mocks';
import { httpStatusCodes } from '../../../utils/src/http-fetch/http-constants';
import { HttpFetcher } from '../../../utils/src/http-fetch/http-fetcher';
import { HasuraHealthApi } from '../../src/hasura/hasura-health-api';

describe(HasuraHealthApi.name, () => {
    let target: HasuraHealthApi;
    let httpFetcher: HttpFetcher;

    const hasuraUrl = 'http://hasura/path';
    const expectedUrl = 'http://hasura/path/healthz';

    beforeEach(() => {
        httpFetcher = mock();
        target = new HasuraHealthApi(instance(httpFetcher));
    });

    it('should pass if OK HTTP response from health endpoint', async () => {
        when(httpFetcher.fetch(anything())).thenResolve({ status: httpStatusCodes.OK } as Response);

        await target.ensureHealthy(hasuraUrl);

        verifyCalled(httpFetcher.fetch).onceWith(expectedUrl);
    });

    it('should throw if HTTP fetch throws', async () => {
        when(httpFetcher.fetch(anything())).thenReject(new Error('oups'));

        const error = await expectToThrowAsync(async () => target.ensureHealthy(hasuraUrl));

        expect(error.message).toIncludeMultiple(['Hasura', expectedUrl, 'oups']);
    });

    it('should throw if HTTP response is not OK', async () => {
        when(httpFetcher.fetch(anything())).thenResolve({
            status: httpStatusCodes.INTERNAL_SERVER_ERROR,
            statusText: 'WTF',
            text: async () => Promise.resolve('bodyTxt'),
        } as Response);

        const error = await expectToThrowAsync(async () => target.ensureHealthy(hasuraUrl));

        expect(error.message).toIncludeMultiple(['Hasura', expectedUrl, '500', 'WTF', 'bodyTxt']);
    });
});
