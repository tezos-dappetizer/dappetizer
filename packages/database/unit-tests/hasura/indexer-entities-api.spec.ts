import { Writable } from 'ts-essentials';
import { instance, mock, when } from 'ts-mockito';
import { DataSource, EntityMetadata } from 'typeorm';
import { RelationMetadata } from 'typeorm/metadata/RelationMetadata';

import {
    EnabledHasuraConfig,
    HasuraAutotrackedMappingConfig,
    HasuraCustomMappingConfig,
    HasuraNamingStyle,
    HasuraRelationshipType,
} from '../../src/hasura/config/hasura-config';
import { HasuraTableMapping } from '../../src/hasura/hasura-mappings';
import { HasuraNamingHelper } from '../../src/hasura/hasura-naming-helper';
import { IndexerEntitiesApi } from '../../src/hasura/indexer-entities-api';

describe(IndexerEntitiesApi.name, () => {
    let target: IndexerEntitiesApi;
    let config: Writable<EnabledHasuraConfig>;
    let namingHelper: HasuraNamingHelper;
    let dataSource: DataSource;

    beforeEach(() => {
        config = {} as EnabledHasuraConfig;
        namingHelper = mock();
        target = new IndexerEntitiesApi(instance(namingHelper));

        dataSource = mock();
        config.autotrackEntities = true;
        config.customMappings = [];
        config.autotrackedMappings = [];
    });

    function runTestExpectingMappings(expectedMappings: HasuraTableMapping[]) {
        const mappings = target.getIndexerMappings(instance(dataSource), config);

        expect(mappings).toEqual(expectedMappings);
    }

    it('should return empty array if entities exist but autotrack entities is off', () => {
        when(dataSource.entityMetadatas).thenReturn([{ name: 'entity' }] as EntityMetadata[]);
        config.autotrackEntities = false;

        runTestExpectingMappings([]);
    });

    it.each([false, true])(
        'should return correct tables based on connection entity metadatas for no transformation naming style with nameOverride %s',
        overrideName => {
            config.autotrackedMappings = overrideName ? getAutotrackedMappings() : [];
            setupEntityMetadatas(dataSource);
            setupNamingHelper(HasuraNamingStyle.NoTransformation);

            runTestExpectingMappings([
                {
                    tableName: 'block',
                    graphQLNames: {
                        type: 'Block',
                        singleByKeyField: 'block',
                        selectField: 'blocks',
                        aggregateField: 'blocksAggregate',
                    },
                    permitAllColumns: true,
                    columns: [
                        { columnName: 'hash', graphQLName: 'hash' },
                    ],
                    relationships: [],
                },
                {
                    tableName: 'my_entity',
                    graphQLNames: {
                        type: overrideName ? 'MyEntityOverriden' : 'MyEntity',
                        singleByKeyField: overrideName ? 'myEntityOverriden' : 'myEntity',
                        selectField: overrideName ? 'myEntitiesOverriden' : 'myEntities',
                        aggregateField: overrideName ? 'myEntitiesAggregateOverriden' : 'myEntitiesAggregate',
                    },
                    permitAllColumns: true,
                    columns: [
                        { columnName: 'some_id', graphQLName: overrideName ? 'myCustomSomeId' : 'someId' },
                        { columnName: 'dummy_column', graphQLName: 'dummyColumn' },
                    ],
                    relationships: [
                        {
                            isCustom: false,
                            foreignTableName: 'referenced_entity',
                            graphQLName: 'relationName',
                            relationshipType: HasuraRelationshipType.ManyToOne,
                            sourceToForeignColumns: { this_id: 'that_id' },
                        },
                    ],
                },
            ]);
        },
    );

    it.each([false, true])(
        'should return correct tables based on connection entity metadatas for snake case naming style with nameOverride %s',
        overrideName => {
            config.autotrackedMappings = overrideName ? getAutotrackedMappings() : [];
            setupEntityMetadatas(dataSource);
            setupNamingHelper(HasuraNamingStyle.SnakeCase);

            runTestExpectingMappings([
                {
                    tableName: 'block',
                    graphQLNames: {
                        type: 'Block',
                        singleByKeyField: 'block',
                        selectField: 'blocks',
                        aggregateField: 'blocks_aggregate',
                    },
                    permitAllColumns: true,
                    columns: [
                        { columnName: 'hash', graphQLName: 'hash' },
                    ],
                    relationships: [],
                },
                {
                    tableName: 'my_entity',
                    graphQLNames: {
                        type: overrideName ? 'MyEntityOverriden' : 'MyEntity',
                        singleByKeyField: overrideName ? 'myEntityOverriden' : 'my_entity',
                        selectField: overrideName ? 'myEntitiesOverriden' : 'my_entities',
                        aggregateField: overrideName ? 'myEntitiesAggregateOverriden' : 'my_entities_aggregate',
                    },
                    permitAllColumns: true,
                    columns: [
                        { columnName: 'some_id', graphQLName: overrideName ? 'myCustomSomeId' : 'some_id' },
                        { columnName: 'dummy_column', graphQLName: overrideName ? 'dummyColumn' : 'dummy_column' },
                    ],
                    relationships: [
                        {
                            isCustom: false,
                            foreignTableName: 'referenced_entity',
                            graphQLName: 'relation_name',
                            relationshipType: HasuraRelationshipType.ManyToOne,
                            sourceToForeignColumns: { this_id: 'that_id' },
                        },
                    ],
                },
            ]);
        },
    );

    it.each([false, true])(
        'should return correct tables based on connection entity metadatas with configured entity to be skipped=%s',
        skipEntity => {
            config.autotrackedMappings = getAutotrackedMappings(skipEntity);
            setupEntityMetadatas(dataSource);
            setupNamingHelper(HasuraNamingStyle.NoTransformation);

            const expectedMappings: HasuraTableMapping[] = [
                {
                    tableName: 'block',
                    graphQLNames: {
                        type: 'Block',
                        singleByKeyField: 'block',
                        selectField: 'blocks',
                        aggregateField: 'blocksAggregate',
                    },
                    permitAllColumns: true,
                    columns: [
                        { columnName: 'hash', graphQLName: 'hash' },
                    ],
                    relationships: [],
                },
                {
                    tableName: 'my_entity',
                    graphQLNames: {
                        type: 'MyEntityOverriden',
                        singleByKeyField: 'myEntityOverriden',
                        selectField: 'myEntitiesOverriden',
                        aggregateField: 'myEntitiesAggregateOverriden',
                    },
                    permitAllColumns: true,
                    columns: [
                        { columnName: 'some_id', graphQLName: 'myCustomSomeId' },
                        { columnName: 'dummy_column', graphQLName: 'dummyColumn' },
                    ],
                    relationships: [
                        {
                            isCustom: false,
                            foreignTableName: 'referenced_entity',
                            graphQLName: 'relationName',
                            relationshipType: HasuraRelationshipType.ManyToOne,
                            sourceToForeignColumns: { this_id: 'that_id' },
                        },
                    ],
                },
            ];

            runTestExpectingMappings(expectedMappings.slice(0, skipEntity ? 1 : 2));
        },
    );

    it.each([false, true])(
        'should return tables with correct columns based on connection entity metadatas with configured column to be hidden=%s',
        setColumnHidden => {
            config.autotrackedMappings = getAutotrackedMappings(false, setColumnHidden);
            setupEntityMetadatas(dataSource);
            setupNamingHelper(HasuraNamingStyle.NoTransformation);

            runTestExpectingMappings([
                {
                    tableName: 'block',
                    graphQLNames: {
                        type: 'Block',
                        singleByKeyField: 'block',
                        selectField: 'blocks',
                        aggregateField: 'blocksAggregate',
                    },
                    permitAllColumns: true,
                    columns: [
                        { columnName: 'hash', graphQLName: 'hash' },
                    ],
                    relationships: [],
                },
                {
                    tableName: 'my_entity',
                    graphQLNames: {
                        type: 'MyEntityOverriden',
                        singleByKeyField: 'myEntityOverriden',
                        selectField: 'myEntitiesOverriden',
                        aggregateField: 'myEntitiesAggregateOverriden',
                    },
                    permitAllColumns: !setColumnHidden,
                    columns: [
                        { columnName: 'some_id', graphQLName: 'myCustomSomeId' },
                        ...setColumnHidden ? [] : [{ columnName: 'dummy_column', graphQLName: 'dummyColumn' }],
                    ],
                    relationships: [
                        {
                            isCustom: false,
                            foreignTableName: 'referenced_entity',
                            graphQLName: 'relationName',
                            relationshipType: HasuraRelationshipType.ManyToOne,
                            sourceToForeignColumns: { this_id: 'that_id' },
                        },
                    ],
                },
            ]);
        },
    );

    it('should return correct tables based on metadatas and custom mappings', () => {
        setupEntityMetadatas(dataSource);
        config.customMappings = getCustomMappings();
        setupNamingHelper(HasuraNamingStyle.NoTransformation);

        runTestExpectingMappings([
            {
                tableName: 'block',
                graphQLNames: {
                    type: 'Block',
                    singleByKeyField: 'block',
                    selectField: 'blocks',
                    aggregateField: 'blocksAggregate',
                },
                permitAllColumns: true,
                columns: [
                    { columnName: 'hash', graphQLName: 'hash' },
                ],
                relationships: [],
            },
            {
                tableName: 'my_entity',
                graphQLNames: {
                    type: 'MyEntity',
                    singleByKeyField: 'myEntity',
                    selectField: 'myEntities',
                    aggregateField: 'myEntitiesAggregate',
                },
                permitAllColumns: true,
                columns: [
                    { columnName: 'that_id', graphQLName: 'thatId' },
                ],
                relationships: [
                    {
                        isCustom: true,
                        foreignTableName: 'referenced_entity',
                        graphQLName: 'referencedEntity',
                        relationshipType: HasuraRelationshipType.ManyToOne,
                        sourceToForeignColumns: { that_id: 'referenced_id' },
                    },
                ],
            },
        ]);
    });

    it('should return correct tables based on metadatas and added custom relationship', () => {
        setupEntityMetadatas(dataSource);
        config.autotrackedMappings = getAutotrackedMappingRelationships();
        setupNamingHelper(HasuraNamingStyle.NoTransformation);

        runTestExpectingMappings([
            {
                tableName: 'block',
                graphQLNames: {
                    type: 'Block',
                    singleByKeyField: 'block',
                    selectField: 'blocks',
                    aggregateField: 'blocksAggregate',
                },
                permitAllColumns: true,
                columns: [
                    { columnName: 'hash', graphQLName: 'hash' },
                ],
                relationships: [],
            },
            {
                tableName: 'my_entity',
                graphQLNames: {
                    type: 'MyEntity',
                    singleByKeyField: 'myEntity',
                    selectField: 'myEntities',
                    aggregateField: 'myEntitiesAggregate',
                },
                permitAllColumns: true,
                columns: [
                    { columnName: 'some_id', graphQLName: 'someId' },
                    { columnName: 'dummy_column', graphQLName: 'dummyColumn' },
                ],
                relationships: [
                    {
                        isCustom: false,
                        foreignTableName: 'referenced_entity',
                        graphQLName: 'relationName',
                        relationshipType: HasuraRelationshipType.ManyToOne,
                        sourceToForeignColumns: { this_id: 'that_id' },
                    },
                    {
                        isCustom: true,
                        foreignTableName: 'referenced_entity',
                        graphQLName: 'referencedEntityX',
                        relationshipType: HasuraRelationshipType.OneToMany,
                        sourceToForeignColumns: { this_id: 'referenced_id' },
                    },
                ],
            },
        ]);
    });

    function setupNamingHelper(namingStyle: HasuraNamingStyle) {
        if (namingStyle === HasuraNamingStyle.NoTransformation) {
            setupFieldNames([
                ['hash', 'hash'],
                ['someId', 'someId'],
                ['thisId', 'thisId'],
                ['thatId', 'thatId'],
                ['dummyColumn', 'dummyColumn'],
            ]);
            when(namingHelper.applyNamingOnRoot('Block', config)).thenReturn('block');
            when(namingHelper.applyNamingOnRoot('MyEntity', config)).thenReturn('myEntity');
            when(namingHelper.applyNaming('relationName', config)).thenReturn('relationName');
            when(namingHelper.getAggregateSuffix(config)).thenReturn('Aggregate');
        }

        if (namingStyle === HasuraNamingStyle.SnakeCase) {
            setupFieldNames([
                ['MyEntityOverriden', 'my_entity_overriden'],
                ['hash', 'hash'],
                ['someId', 'some_id'],
                ['thisId', 'this_id'],
                ['dummyColumn', 'dummy_column'],
            ]);
            when(namingHelper.applyNamingOnRoot('Block', config)).thenReturn('block');
            when(namingHelper.applyNamingOnRoot('MyEntity', config)).thenReturn('my_entity');
            when(namingHelper.applyNaming('relationName', config)).thenReturn('relation_name');
            when(namingHelper.getAggregateSuffix(config)).thenReturn('_aggregate');
        }
    }

    function setupFieldNames(maps: [string, string][]) {
        for (const map of maps) {
            when(namingHelper.applyNaming(map[0], config)).thenReturn(map[1]);
        }
    }
});

function setupEntityMetadatas(dataSource: DataSource) {
    when(dataSource.entityMetadatas).thenReturn([
        {
            name: 'DbBlock',
            tableName: 'block',
            ownColumns: [
                { databaseName: 'hash', propertyName: 'hash' },
            ],
            relations: [] as RelationMetadata[],
        } as EntityMetadata,
        {
            name: 'MyEntity',
            tableName: 'my_entity',
            ownColumns: [
                { databaseName: 'some_id', propertyName: 'someId' },
                { databaseName: 'dummy_column', propertyName: 'dummyColumn' },
            ],
            relations: [
                {
                    propertyName: 'relationName',
                    foreignKeys: [
                        {
                            referencedEntityMetadata: {
                                name: 'ReferencedEntity',
                                tableName: 'referenced_entity',
                            },
                            columns: [{ propertyName: 'thisId' }],
                            columnNames: ['this_id'],
                            referencedColumns: [{ propertyName: 'thatId' }],
                            referencedColumnNames: ['that_id'],
                        },
                    ],
                    isManyToOne: true,
                    isOneToMany: false,
                },
            ],
        } as EntityMetadata,
    ]);
}

function getCustomMappings(): HasuraCustomMappingConfig[] {
    return [{
        tableName: 'my_entity',
        graphQLTypeName: 'MyEntity',
        graphQLFieldName: 'myEntity',
        graphQLPluralFieldName: null,
        graphQLAggregateFieldName: null,
        columns: [
            { columnName: 'that_id', graphQLName: 'thatId', hidden: false },
        ],
        relationships: [
            {
                name: 'referencedEntity',
                type: HasuraRelationshipType.ManyToOne,
                columns: ['that_id'],
                referencedTableName: 'referenced_entity',
                referencedColumns: ['referenced_id'],
            },
        ],
    }];
}

function getAutotrackedMappings(setEntitySkipped = false, setColumnHidden = false): HasuraAutotrackedMappingConfig[] {
    return [
        {
            entityName: 'MyEntity',
            skipped: setEntitySkipped,
            graphQLTypeName: 'MyEntityOverriden',
            graphQLFieldName: 'myEntityOverriden',
            graphQLPluralFieldName: 'myEntitiesOverriden',
            graphQLAggregateFieldName: 'myEntitiesAggregateOverriden',
            columns: [
                { propertyName: 'someId', graphQLName: 'myCustomSomeId', hidden: false },
                { propertyName: 'dummyColumn', graphQLName: 'dummyColumn', hidden: setColumnHidden },
            ],
            relationships: [],
        },
        {
            entityName: 'ReferencedEntity',
            skipped: false,
            graphQLTypeName: 'ReferencedEntityOverriden',
            graphQLFieldName: 'referencedEntityOverriden',
            graphQLPluralFieldName: null,
            graphQLAggregateFieldName: null,
            columns: [],
            relationships: [],
        },
    ];
}

function getAutotrackedMappingRelationships(): HasuraAutotrackedMappingConfig[] {
    return [{
        entityName: 'MyEntity',
        graphQLFieldName: null,
        graphQLTypeName: null,
        graphQLPluralFieldName: null,
        graphQLAggregateFieldName: null,
        skipped: false,
        columns: [],
        relationships: [{
            name: 'referencedEntityX',
            type: HasuraRelationshipType.OneToMany,
            columns: ['this_id'],
            referencedTableName: 'referenced_entity',
            referencedColumns: ['referenced_id'],
        }],
    }];
}
