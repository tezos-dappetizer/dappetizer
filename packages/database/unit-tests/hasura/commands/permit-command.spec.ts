import { describeMember } from '../../../../../test-utilities/mocks';
import { asReadonly } from '../../../../utils/src/basics/public-api';
import { PermitCommand } from '../../../src/hasura/commands/permit-command';
import { HasuraTableMapping } from '../../../src/hasura/hasura-mappings';

describe(PermitCommand.name, () => {
    const getTarget = (permitAllColumns = true) => new PermitCommand({
        hasuraSource: 'mySource',
        dbSchema: 'my_db',
        table: {
            tableName: 'foo_bar',
            permitAllColumns,
            columns: asReadonly([
                { columnName: 'first_col', graphQLName: 'firstCol' },
                { columnName: 'second_col', graphQLName: 'secondCol' },
            ]),
        } as HasuraTableMapping,
        selectLimit: 23,
        allowAggregations: 'mockedBoolean' as any,
    });

    describeMember<PermitCommand>('description', () => {
        it('should be descriptive enough', () => {
            expect(getTarget().description).toIncludeMultiple([`permissions`, `'foo_bar'`]);
        });
    });

    describeMember<PermitCommand>('createPayload', () => {
        it.each([
            [true, '*'],
            [false, ['first_col', 'second_col']],
        ])('should create correct payload if permitAllColumns %s', (permitAllColumns, expectedColumns) => {
            const payload = getTarget(permitAllColumns).createPayload();

            expect(payload).toEqual({
                type: 'pg_create_select_permission',
                args: {
                    source: 'mySource',
                    table: {
                        schema: 'my_db',
                        name: 'foo_bar',
                    },
                    role: 'public',
                    permission: {
                        columns: expectedColumns,
                        filter: {},
                        limit: 23,
                        allow_aggregations: 'mockedBoolean',
                    },
                },
            });
        });
    });
});
