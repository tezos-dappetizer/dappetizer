import { describeMember } from '../../../../../test-utilities/mocks';
import { CreateArrayRelationshipCommand } from '../../../src/hasura/commands/create-array-relationship-command';
import { HasuraRelationshipType } from '../../../src/hasura/config/hasura-config';

describe(CreateArrayRelationshipCommand.name, () => {
    const target = new CreateArrayRelationshipCommand({
        hasuraSource: 'mySource',
        dbSchema: 'my_db',
        tableName: 'src_entity',
        relationship: {
            relationshipType: HasuraRelationshipType.OneToMany,
            isCustom: false,
            graphQLName: 'fooBars',
            foreignTableName: 'foo_bar',
            sourceToForeignColumns: {
                pk_first: 'fk_first',
                pk_second: 'fk_second',
            },
        },
    });

    describeMember<typeof target>('description', () => {
        it('should be descriptive enough', () => {
            expect(target.description).toIncludeMultiple([`GraphQL array relationship`, `'fooBars'`, `'src_entity'`]);
        });
    });

    describeMember<typeof target>('createPayload', () => {
        it('should create correct payload', () => {
            const payload = target.createPayload();

            expect(payload).toEqual({
                type: 'pg_create_array_relationship',
                args: {
                    source: 'mySource',
                    name: 'fooBars',
                    table: {
                        schema: 'my_db',
                        name: 'src_entity',
                    },
                    using: {
                        manual_configuration: {
                            remote_table: {
                                schema: 'my_db',
                                name: 'foo_bar',
                            },
                            column_mapping: {
                                pk_first: 'fk_first',
                                pk_second: 'fk_second',
                            },
                        },
                    },
                },
            });
        });
    });
});
