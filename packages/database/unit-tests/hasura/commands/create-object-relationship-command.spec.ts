import { Writable } from 'ts-essentials';

import { describeMember } from '../../../../../test-utilities/mocks';
import { CreateObjectRelationshipCommand } from '../../../src/hasura/commands/create-object-relationship-command';
import { HasuraRelationshipType } from '../../../src/hasura/config/hasura-config';
import { HasuraRelationshipMapping } from '../../../src/hasura/hasura-mappings';

describe(CreateObjectRelationshipCommand.name, () => {
    let relationshipMapping: Writable<HasuraRelationshipMapping>;

    const getTarget = () => new CreateObjectRelationshipCommand({
        hasuraSource: 'mySource',
        dbSchema: 'my_db',
        tableName: 'src_entity',
        relationship: relationshipMapping,
    });

    beforeEach(() => {
        relationshipMapping = {
            relationshipType: HasuraRelationshipType.ManyToOne,
            isCustom: false,
            graphQLName: 'fooBar',
            foreignTableName: 'foo_bar',
            sourceToForeignColumns: { pk_col: 'fk_col' },
        };
    });

    describeMember<CreateObjectRelationshipCommand>('description', () => {
        it('should be descriptive enough', () => {
            expect(getTarget().description).toIncludeMultiple([`GraphQL object relationship`, `'fooBar'`, `'src_entity'`]);
        });
    });

    describeMember<CreateObjectRelationshipCommand>('createPayload', () => {
        it('should create correct payload', () => {
            const payload = getTarget().createPayload();

            expect(payload).toEqual({
                type: 'pg_create_object_relationship',
                args: {
                    source: 'mySource',
                    name: 'fooBar',
                    table: {
                        schema: 'my_db',
                        name: 'src_entity',
                    },
                    using: { foreign_key_constraint_on: ['pk_col'] },
                },
            });
        });

        it('should use manual configuration if mapping is custom', () => {
            relationshipMapping.isCustom = true;

            const payload: any = getTarget().createPayload();

            expect(payload.args.using).toEqual({
                manual_configuration: {
                    remote_table: {
                        schema: 'my_db',
                        name: 'foo_bar',
                    },
                    column_mapping: { pk_col: 'fk_col' },
                },
            });
        });

        it('should use manual configuration if compound key', () => {
            relationshipMapping.sourceToForeignColumns = {
                pk_first: 'fk_first',
                pk_second: 'fk_second',
            };

            const payload: any = getTarget().createPayload();

            expect(payload.args.using).toEqual({
                manual_configuration: {
                    remote_table: {
                        schema: 'my_db',
                        name: 'foo_bar',
                    },
                    column_mapping: {
                        pk_first: 'fk_first',
                        pk_second: 'fk_second',
                    },
                },
            });
        });
    });
});
