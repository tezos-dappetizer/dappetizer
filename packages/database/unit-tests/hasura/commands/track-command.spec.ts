import { describeMember } from '../../../../../test-utilities/mocks';
import { asReadonly } from '../../../../utils/src/basics/public-api';
import { TrackCommand } from '../../../src/hasura/commands/track-command';
import { HasuraTableMapping } from '../../../src/hasura/hasura-mappings';

describe(TrackCommand.name, () => {
    const target = new TrackCommand({
        hasuraSource: 'mySource',
        dbSchema: 'my_db',
        table: {
            tableName: 'foo_bar',
            graphQLNames: {
                type: 'FooBar',
                singleByKeyField: 'fooBar',
                selectField: 'fooBars',
                aggregateField: 'fooBarAggregate',
            },
            columns: asReadonly([
                { columnName: 'first_col', graphQLName: 'firstCol' },
                { columnName: 'second_col', graphQLName: 'secondCol' },
            ]),
        } as HasuraTableMapping,
    });

    describeMember<typeof target>('description', () => {
        it('should be descriptive enough', () => {
            expect(target.description).toIncludeMultiple([`track`, `'foo_bar'`, `'FooBar'`]);
        });
    });

    describeMember<typeof target>('createPayload', () => {
        it('should create correct payload', () => {
            const payload = target.createPayload();

            expect(payload).toEqual({
                type: 'pg_track_table',
                args: {
                    source: 'mySource',
                    table: {
                        schema: 'my_db',
                        name: 'foo_bar',
                    },
                    configuration: {
                        custom_name: 'FooBar',
                        custom_root_fields: {
                            select: 'fooBars',
                            select_by_pk: 'fooBar',
                            select_aggregate: 'fooBarAggregate',
                        },
                        column_config: {
                            first_col: { custom_name: 'firstCol' },
                            second_col: { custom_name: 'secondCol' },
                        },
                        custom_column_names: {
                            first_col: 'firstCol',
                            second_col: 'secondCol',
                        },
                    },
                },
            });
        });
    });
});
