import { DataSource } from 'typeorm';

import { deleteFileOrDir, getRandomString } from '../../../test-utilities/mocks';
import { DAPPETIZER_DB_ENTITIES } from '../src/entities/dappetizer-db-entities';
import { dappetizerNamingStrategy } from '../src/helpers/naming-helper';

export interface TestDatabase {
    dataSource: DataSource;
    destroy(): Promise<void>;
}

export async function createTestDatabase(): Promise<TestDatabase> {
    const dbFile = `test-${getRandomString()}.sqlite`;
    const dataSource = new DataSource({
        name: 'testConnection',
        type: 'sqlite',
        database: dbFile,
        entities: DAPPETIZER_DB_ENTITIES.slice(),
        synchronize: true,
        namingStrategy: dappetizerNamingStrategy,
    });

    await dataSource.initialize();
    return {
        dataSource,
        async destroy(): Promise<void> {
            await dataSource.destroy();
            deleteFileOrDir(dbFile);
        },
    };
}
