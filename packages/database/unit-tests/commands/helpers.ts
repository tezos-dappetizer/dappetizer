import { DbCommand } from '../../src/commands/db-command';
import { DbBlock } from '../../src/entities/db-block';
import { TestDatabase } from '../test-database';

export async function executeCmd<T>(db: TestDatabase, cmd: DbCommand<T>): Promise<T> {
    return cmd.execute(db.dataSource.manager);
}

export function createTestBlock(level = 66, hash?: string): DbBlock {
    return Object.freeze({
        level,
        hash: hash ?? `haha-${level}`,
        predecessor: `pred-${level}`,
        timestamp: new Date(`${2000 + level}-01-02T12:34:56Z`),
    });
}

export async function sqlInsert(db: TestDatabase, blocks: readonly DbBlock[]) {
    for (const block of blocks) {
        await db.dataSource.query(`INSERT INTO block (hash, level, predecessor, timestamp)`
            + ` VALUES ('${block.hash}', ${block.level}, '${block.predecessor}', '${block.timestamp.toISOString()}')`);
    }
}

export async function verifyBlocksInDb(db: TestDatabase, expectedBlocks: DbBlock[]) {
    const dbData = await db.dataSource.query('SELECT hash, level, predecessor, timestamp FROM block');
    expect(dbData.map((d: any) => ({ ...d, timestamp: parseDate(d.timestamp) }))).toEqual(expectedBlocks);
}

export function parseDate(str: string) {
    return new Date(str.endsWith('Z') ? str : `${str}Z`);
}
