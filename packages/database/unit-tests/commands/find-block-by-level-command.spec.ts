import { createTestBlock, executeCmd, sqlInsert } from './helpers';
import { FindBlockByLevelCommand } from '../../src/commands/find-block-by-level-command';
import { createTestDatabase, TestDatabase } from '../test-database';

describe(FindBlockByLevelCommand.name, () => {
    let db: TestDatabase;

    const act = async (l: number) => executeCmd(db, new FindBlockByLevelCommand(l));

    beforeEach(async () => db = await createTestDatabase());
    afterEach(async () => db.destroy());

    it('should get block with specified level', async () => {
        const [block65, block66] = [createTestBlock(65), createTestBlock(66)];
        await sqlInsert(db, [block65, block66]);

        const block = await act(65);

        expect(block).toEqual(block65);
    });

    it('should get nullish if block not found', async () => {
        await sqlInsert(db, [createTestBlock(65), createTestBlock(66)]);

        const block = await act(64);

        expect(block).toBeNull();
    });
});
