import { createTestBlock, executeCmd, sqlInsert, verifyBlocksInDb } from './helpers';
import { describeMember, expectToThrowAsync } from '../../../../test-utilities/mocks';
import { keyof } from '../../../utils/src/public-api';
import { InsertCommand } from '../../src/commands/insert-command';
import { DbBlock } from '../../src/entities/db-block';
import { createTestDatabase, TestDatabase } from '../test-database';

describe(InsertCommand.name, () => {
    describe('constructor', () => {
        let block: DbBlock;

        beforeEach(() => block = createTestBlock());

        it('should be created correctly', () => {
            const blocks = [block];

            const target = new InsertCommand(DbBlock, blocks);

            expect(target).toBeFrozen();
            expect(target.entities).toEqual(blocks);
            expect(target.entities).not.toBe(blocks);
            expect(target.entities).toBeFrozen();
            expect(target.ignoreIfExists).toBeFalse();
            expect(target.description).toBe(`insert 1 of 'DbBlock' with ignoreIfExists false`);
        });

        it('should accept single entity', () => {
            const target = new InsertCommand(DbBlock, block);

            expect(target.entities).toEqual([block]);
            expect(target.entities).toBeFrozen();
        });

        it.each([true, false])(`should accept ${keyof<InsertCommand<{}>>('ignoreIfExists')} %s`, ignoreIfExists => {
            const target = new InsertCommand(DbBlock, [block], { ignoreIfExists });

            expect(target.ignoreIfExists).toBe(ignoreIfExists);
        });
    });

    describeMember<InsertCommand<{}>>('execute', () => {
        let db: TestDatabase;

        async function act(blocks: DbBlock | DbBlock[], ignoreIfExists = false) {
            await executeCmd(db, new InsertCommand(DbBlock, blocks, { ignoreIfExists }));
        }

        beforeEach(async () => db = await createTestDatabase());
        afterEach(async () => db.destroy());

        it('should insert entity', async () => {
            const block = createTestBlock();

            await act(block);

            await verifyBlocksInDb(db, [block]);
        });

        it('should ignore insert if entity already exists and flag specified', async () => {
            const originalBlock = createTestBlock(50);
            const conflictBlock = createTestBlock(66, originalBlock.hash);
            await sqlInsert(db, [originalBlock]);

            await act(conflictBlock, true);

            await verifyBlocksInDb(db, [originalBlock]);
        });

        it('should fail if entity already exists and no flag specified', async () => {
            const originalBlock = createTestBlock(50);
            const conflictBlock = createTestBlock(66, originalBlock.hash);
            await sqlInsert(db, [originalBlock]);

            await expectToThrowAsync(async () => act(conflictBlock));

            await verifyBlocksInDb(db, [originalBlock]);
        });

        it('should do nothing if no entities', async () => {
            await act([]);

            await verifyBlocksInDb(db, []);
        });
    });
});
