import { createTestBlock, executeCmd, sqlInsert } from './helpers';
import { FindLatestBlockCommand } from '../../src/commands/find-latest-block-command';
import { createTestDatabase, TestDatabase } from '../test-database';

describe(FindLatestBlockCommand.name, () => {
    let db: TestDatabase;

    const act = async () => executeCmd(db, new FindLatestBlockCommand());

    beforeEach(async () => db = await createTestDatabase());
    afterEach(async () => db.destroy());

    it('should get block with the highest level', async () => {
        const [block65, block66] = [createTestBlock(65), createTestBlock(66)];
        await sqlInsert(db, [block65, block66]);

        const block = await act();

        expect(block).toEqual(block66);
    });

    it('should get nullish if no block', async () => {
        const block = await act();

        expect(block).toBeNull();
    });
});
