import { executeCmd } from './helpers';
import { FindAllDappetizerSettingsCommand } from '../../src/commands/find-all-dappetizer-settings-command';
import { createTestDatabase, TestDatabase } from '../test-database';

describe(FindAllDappetizerSettingsCommand.name, () => {
    let db: TestDatabase;

    beforeEach(async () => db = await createTestDatabase());
    afterEach(async () => db.destroy());

    it('should load all indexer settings', async () => {
        await db.dataSource.query(`INSERT INTO dappetizer_settings (chain_id) VALUES ('aa'), ('bb')`);

        const settings = await executeCmd(db, new FindAllDappetizerSettingsCommand());

        expect(settings.map(s => s.chainId)).toEqual(['aa', 'bb']);
    });
});
