import path from 'path';
import { register } from 'ts-node';
import { DataSource } from 'typeorm';

import { deleteFileOrDir, runDappetizer } from '../../../test-utilities/mocks';

describe.each(['postgres'])('integration tests for %s', dbType => {
    let db: DbHandler;

    beforeAll(async () => {
        const configFile = `${__dirname}/dappetizer.${dbType}.config.ts`;
        db = await connectDb(configFile, dbType);

        await runDappetizer(['start', configFile], {
            expectedExitCode: 1,
            cwd: __dirname,
        });
    });

    afterAll(async () => {
        await db.cleanUp();
    });

    it('should index blocks', async () => {
        const blocks: any[] = await db.dataSource.query(`
            SELECT *
            FROM ${db.schemaPrefix}block
            ORDER BY level`);

        expect(blocks.map(b => ({ ...b, timestamp: sanitizeDate(b.timestamp) }))).toEqual([
            {
                level: 1796556,
                hash: 'BMeCZiGp6fHESMMj8UyMPNsJujZmSJ2JEVcxFxg7Zv8y8y93Zwj',
                predecessor: 'BMA7Uw5aG54Sb23KqYbR9srJqKDVJdykknS9EZYx6nWrmzj352L',
                timestamp: new Date('2021-10-21T17:54:30.000Z'),
            },
            {
                level: 1796557,
                hash: 'BM379sywdFpYsnqUUQfyeKAEcgJaY6xD3Sygd77mK79571on1Am',
                predecessor: 'BMeCZiGp6fHESMMj8UyMPNsJujZmSJ2JEVcxFxg7Zv8y8y93Zwj',
                timestamp: new Date('2021-10-21T17:55:00.000Z'),
            },
        ]);
    });

    it('should index consumer entities', async () => {
        const foos = await db.dataSource.query(`
            SELECT *
            FROM ${db.schemaPrefix}foo_entity
            ORDER BY value`);

        expect(foos).toEqual([
            { value: 'bar-1796556', block_hash: 'BMeCZiGp6fHESMMj8UyMPNsJujZmSJ2JEVcxFxg7Zv8y8y93Zwj' },
            { value: 'bar-1796557', block_hash: 'BM379sywdFpYsnqUUQfyeKAEcgJaY6xD3Sygd77mK79571on1Am' },
        ]);
    });
});

interface DbHandler {
    dataSource: DataSource;
    schemaPrefix: string;
    cleanUp(): Promise<void>;
}

async function connectDb(configFile: string, dbType: string): Promise<DbHandler> {
    register({ compilerOptions: { module: 'CommonJS', paths: {} } }).enabled(true);
    const dappetizerConfig = require(path.resolve(configFile)).default; // eslint-disable-line
    const dbConfig = dappetizerConfig.database;

    if (dbConfig.type !== dbType) {
        throw new Error(`Unexpected db type '${dbConfig.type as string}' in: ${configFile}`);
    }

    const dataSource = new DataSource(dbConfig);
    switch (dbConfig.type) {
        case 'sqlite': {
            const dbFile = `${__dirname}/${dbConfig.database as string}`;
            deleteFileOrDir(dbFile);
            dbConfig.database = dbFile;
            await dataSource.initialize();

            return {
                dataSource,
                schemaPrefix: '',

                cleanUp: async () => {
                    await dataSource.destroy();
                    deleteFileOrDir(dbFile);
                },
            };
        }
        default: {
            const schema = dbConfig.schema as string;
            await dataSource.initialize();
            await ensureNoSchema(dataSource, schema);

            return {
                dataSource,
                schemaPrefix: `${schema}.`,
                cleanUp: async () => {
                    await ensureNoSchema(dataSource, schema);
                    await dataSource.destroy();
                },
            };
        }
    }
}

async function ensureNoSchema(dataSource: DataSource, dbSchema: string) {
    await dataSource.query(`DROP SCHEMA IF EXISTS ${dbSchema} CASCADE`);
}

function sanitizeDate(value: unknown) {
    return typeof value === 'string' ? new Date(`${value}Z`) : value;
}
