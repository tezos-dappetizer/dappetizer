import { DappetizerConfig } from '@tezos-dappetizer/indexer';

import { tezosNodeUrlForIntegrationTests } from '../../../test-utilities/int-tezos-node';

export const configBase: DappetizerConfig = {
    modules: [
        { id: './indexer/dist' },
    ],
    networks: {
        mainnet: {
            indexing: {
                fromBlockLevel: 1796556,
                toBlockLevel: 1796558,
                retryDelaysMillis: [],
                retryIndefinitely: false,
            },
            tezosNode: {
                url: tezosNodeUrlForIntegrationTests,
            },
        },
    },
    usageStatistics: {
        enabled: false,
    },
};
