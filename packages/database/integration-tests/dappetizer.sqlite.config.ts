import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';

import { configBase } from './dappetizer.base.config';

const config: DappetizerConfigUsingDb = {
    ...configBase,
    database: {
        type: 'sqlite',
        database: '.tmp-sqlite/integration-tests.sqlite',
    },
};

export default config;
