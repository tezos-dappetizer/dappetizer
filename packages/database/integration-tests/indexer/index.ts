import { IndexerModuleUsingDb } from '@tezos-dappetizer/database';

import { FooEntity } from './foo-entity';
import { IntBlockDataIndexerUsingDb } from './int-block-data-indexer-using-db';

export const indexerModule: IndexerModuleUsingDb = {
    name: 'IntegrationTests',
    dbEntities: [FooEntity],
    blockDataIndexers: [new IntBlockDataIndexerUsingDb()],
};
