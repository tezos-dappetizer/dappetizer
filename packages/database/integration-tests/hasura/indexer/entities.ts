import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm';

@Entity()
export class Foo {
    @PrimaryColumn()
        key!: number;

    @Column()
        fooValue!: number;
}

@Entity()
export class Bar {
    @PrimaryColumn({ type: 'varchar', length: 32 })
        key!: string;

    @Column()
        barValue!: number;

    @ManyToOne(() => Foo)
        foo!: Foo;

    @Column()
        fooKey!: number;
}
