import { IndexerModuleUsingDb } from '@tezos-dappetizer/database';

import { Foo, Bar } from './entities';

export const indexerModule: IndexerModuleUsingDb = {
    name: 'HasuraIntegrationTests',
    dbEntities: [Bar, Foo],
    blockDataIndexers: [{ indexBlock() {} }], // Dummy indexer.
};
