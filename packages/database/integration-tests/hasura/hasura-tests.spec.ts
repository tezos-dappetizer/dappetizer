import { DB_DATA_SOURCE_PROVIDER_DI_TOKEN, registerDappetizerWithDatabase } from '@tezos-dappetizer/database';
import { INDEXER_MODULES_DI_TOKEN } from '@tezos-dappetizer/indexer';
import { contentTypes, httpHeaders, registerExplicitConfigFilePath } from '@tezos-dappetizer/utils';
import { buildClientSchema, getIntrospectionQuery, printSchema } from 'graphql/utilities';
import fetch from 'node-fetch';
import path from 'path';
import { container as globalContainer, DependencyContainer } from 'tsyringe';

import { readFileLines, splitToLines, writeFileText } from '../../../../test-utilities/mocks';

describe('Hasura', () => {
    let diContainer: DependencyContainer;

    beforeAll(() => {
        diContainer = globalContainer.createChildContainer();
        registerDappetizerWithDatabase(diContainer);
        registerExplicitConfigFilePath(diContainer, { useValue: getLocalPath('dappetizer.hasura.config.ts') });

        const indexerModule = require(getLocalPath('indexer/dist')).indexerModule; // eslint-disable-line
        diContainer.registerInstance(INDEXER_MODULES_DI_TOKEN, indexerModule);
    });

    it('should initialize dappetizer mappings', async () => {
        const dbSourceProvider = diContainer.resolve(DB_DATA_SOURCE_PROVIDER_DI_TOKEN);
        await dbSourceProvider.getDataSource();

        const schemaSdl = await fetchHasuraSchemaSdl();
        writeFileText(getLocalPath('.tmp-actual-graphql-schema.sdl'), schemaSdl.join('\n'));

        const expectedSchemaSdl = readFileLines(getLocalPath('expected-graphql-schema.sdl'));
        expect(schemaSdl).toEqual(expectedSchemaSdl);
    });
});

async function fetchHasuraSchemaSdl(): Promise<string[]> {
    const url = `http://${process.env.HASURA_HOST ? process.env.HASURA_HOST : 'localhost'}:8080/v1/graphql`;
    const response = await fetch(url, {
        method: 'POST',
        headers: { [httpHeaders.CONTENT_TYPE]: contentTypes.JSON },
        body: JSON.stringify({ query: getIntrospectionQuery({ descriptions: false }) }),
    });
    if (!response.ok) {
        throw new Error(`HTTP response status: ${response.status}`);
    }

    const json = await response.json();
    expect(json.errors).toBeUndefined();
    return splitToLines(printSchema(buildClientSchema(json.data)));
}

function getLocalPath(relPath: string): string {
    return path.resolve(`${__dirname}/${relPath}`);
}
