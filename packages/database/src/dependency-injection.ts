import {
    registerDappetizerIndexer,
    registerIndexerDatabaseHandler,
    registerPartialIndexerModuleVerifier,
} from '@tezos-dappetizer/indexer';
import {
    registerBackgroundWorker,
    registerConfigWithDiagnostics,
    registerOnce,
    registerPublicApi,
} from '@tezos-dappetizer/utils';
import { DependencyContainer } from 'tsyringe';

import { DbConfig } from './db-access/db-config';
import { DbDataSourceManager } from './db-access/db-data-source-manager';
import { DB_DATA_SOURCE_PROVIDER_DI_TOKEN } from './db-access/db-data-source-provider';
import { DbEntitiesModuleVerifier } from './db-access/db-entities-module-verifier';
import { registerDbInitializer } from './db-access/db-initializer';
import { DbSchemaInitializer } from './db-access/db-schema-initializer';
import { DbIndexerDatabaseHandler } from './db-indexer-database-handler';
import { HasuraConfig } from './hasura/config/hasura-config';
import { HasuraInitializer } from './hasura/hasura-initializer';
import { DATABASE_PACKAGE_JSON_DI_TOKEN } from './helpers/database-package-json';
import { DatabasePackageJsonImpl } from './helpers/database-package-json-impl';
import { DB_EXECUTOR_DI_TOKEN } from './helpers/db-executor';
import { DbExecutorImpl } from './helpers/db-executor-impl';

export function registerDappetizerWithDatabase(diContainer: DependencyContainer): void {
    registerOnce(diContainer, 'Dappetizer:Database', () => {
        // Dependencies:
        registerDappetizerIndexer(diContainer);

        // Public API:
        registerPublicApi(diContainer, DB_DATA_SOURCE_PROVIDER_DI_TOKEN, {
            useInternalToken: DbDataSourceManager,
            createProxy: internal => ({
                getDataSource: internal.getDataSource.bind(internal),
            }),
        });
        registerPublicApi(diContainer, DB_EXECUTOR_DI_TOKEN, {
            useInternalToken: DbExecutorImpl,
            createProxy: internal => ({
                execute: internal.execute.bind(internal),
            }),
        });
        registerPublicApi(diContainer, DATABASE_PACKAGE_JSON_DI_TOKEN, {
            useInternalToken: DatabasePackageJsonImpl,
            createProxy: internal => ({
                dependencies: internal.dependencies,
            }),
        });

        // Inject to Dappetizer features:
        registerConfigWithDiagnostics(diContainer, { useToken: DbConfig });
        registerConfigWithDiagnostics(diContainer, { useToken: HasuraConfig });
        registerBackgroundWorker(diContainer, { useToken: DbDataSourceManager });
        registerDbInitializer(diContainer, { useToken: DbSchemaInitializer });
        registerDbInitializer(diContainer, { useToken: HasuraInitializer });
        registerPartialIndexerModuleVerifier(diContainer, { useToken: DbEntitiesModuleVerifier });
        registerIndexerDatabaseHandler(diContainer, { useToken: DbIndexerDatabaseHandler });
    });
}
