import { EntityManager } from 'typeorm';

import { DbCommand } from './db-command';
import { DbBlock } from '../entities/db-block';

export class FindBlockByLevelCommand implements DbCommand<DbBlock | null> {
    readonly description: string;

    constructor(readonly level: number) {
        this.description = `find block by level ${level}`;
        Object.freeze(this);
    }

    async execute(dbManager: EntityManager): Promise<DbBlock | null> {
        return dbManager.getRepository(DbBlock)
            .findOne({ where: { level: this.level } });
    }
}
