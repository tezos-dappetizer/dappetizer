import { EntityManager } from 'typeorm';

import { DbCommand } from './db-command';
import { DbDappetizerSettings } from '../entities/db-dappetizer-settings';

export class FindAllDappetizerSettingsCommand implements DbCommand<DbDappetizerSettings[]> {
    readonly description = 'get all dappetizer settings';

    constructor() {
        Object.freeze(this);
    }

    async execute(dbManager: EntityManager): Promise<DbDappetizerSettings[]> {
        return dbManager.getRepository(DbDappetizerSettings).find();
    }
}
