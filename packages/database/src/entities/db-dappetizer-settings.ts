import { Entity, PrimaryColumn } from 'typeorm';

import { commonDbColumns } from '../helpers/common-db-columns';

@Entity()
export class DbDappetizerSettings {
    @PrimaryColumn(commonDbColumns.chainId)
    readonly chainId!: string;
}
