import { Constructor } from '@tezos-dappetizer/utils';

import { DbBlock } from './db-block';
import { DbDappetizerSettings } from './db-dappetizer-settings';

export const DAPPETIZER_DB_ENTITIES: readonly Constructor[] = Object.freeze([
    DbBlock,
    DbDappetizerSettings,
]);
