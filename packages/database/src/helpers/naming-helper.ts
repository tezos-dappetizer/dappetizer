import { ArgError, argGuard, Constructor } from '@tezos-dappetizer/utils';
import { NamingStrategyInterface } from 'typeorm';

import { DappetizerNamingStrategy } from './dappetizer-naming-strategy';
import { DbEntity } from '../db-access/db-entity';

export const dappetizerNamingStrategy: NamingStrategyInterface = new DappetizerNamingStrategy();

export function dbColumnName<TEntity>(column: keyof TEntity & string): string {
    argGuard.nonWhiteSpaceString(column, 'column');

    return dappetizerNamingStrategy.columnName(column, '', []);
}

export function dbTableName(entityType: Constructor): string {
    argGuard.function(entityType, 'entityType');

    return dappetizerNamingStrategy.tableName(entityType.name, '');
}

export function getEntityName(dbEntity: DbEntity): string {
    switch (typeof dbEntity) {
        case 'function':
            return dbEntity.name;
        case 'string':
            return dbEntity;
        case 'object':
            argGuard.object(dbEntity.options, 'dbEntity.options');
            argGuard.nonWhiteSpaceString(dbEntity.options.name, 'dbEntity.options.name');

            return dbEntity.options.name;
        default:
            throw new ArgError({
                argName: 'dbEntity',
                specifiedValue: dbEntity,
                details: `It must be either a constructor function, string or EntitySchema but it is ${typeof dbEntity}.`,
            });
    }
}
