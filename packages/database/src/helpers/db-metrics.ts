import { asRequiredLabels } from '@tezos-dappetizer/utils';
import { Histogram } from 'prom-client';
import { singleton } from 'tsyringe';

@singleton()
export class DbMetrics {
    readonly callDuration = asRequiredLabels(new Histogram({
        name: 'ai_database_call_duration',
        help: 'How long it took to execute database call (ms).',
        labelNames: ['command'] as const,
        buckets: [10, 20, 50, 100, 500, 1000],
    }));
}
