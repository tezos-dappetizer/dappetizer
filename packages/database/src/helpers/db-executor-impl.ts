import {
    argGuard,
    Clock,
    CLOCK_DI_TOKEN,
    ENABLE_TRACE,
    errorToString,
    getConstructorName,
    injectLogger,
    Logger,
    LONG_EXECUTION_HELPER_DI_TOKEN,
    LongExecutionHelper,
    safeJsonStringify,
    Stopwatch,
} from '@tezos-dappetizer/utils';
import { delay, inject, singleton } from 'tsyringe';
import { EntityManager } from 'typeorm';

import { DbExecutor } from './db-executor';
import { DbMetrics } from './db-metrics';
import { DbCommand } from '../commands/db-command';
import { DbDataSourceManager } from '../db-access/db-data-source-manager';
import { DbDataSourceProvider } from '../db-access/db-data-source-provider';

@singleton()
export class DbExecutorImpl implements DbExecutor {
    constructor(
        @inject(LONG_EXECUTION_HELPER_DI_TOKEN) private readonly longExecutionHelper: LongExecutionHelper,
        private readonly metrics: DbMetrics,
        @inject(CLOCK_DI_TOKEN) private readonly clock: Clock,
        @inject(delay(() => DbDataSourceManager)) private readonly dbConnectionProvider: DbDataSourceProvider,
        @injectLogger(DbExecutorImpl) private readonly logger: Logger,
    ) {}

    async executeNoTransaction<TResult>(command: DbCommand<TResult>): Promise<TResult> {
        const dbConnection = await this.dbConnectionProvider.getDataSource();
        return this.execute(dbConnection.manager, command);
    }

    async execute<TResult>(dbManager: EntityManager, command: DbCommand<TResult>): Promise<TResult> {
        argGuard.object(dbManager, 'dbManager');
        argGuard.object(command, 'command');

        const stopwatch = new Stopwatch(this.clock);
        const { description, ...args } = command;
        try {
            this.logger.logDebug('Executing database {command}.', {
                command: command.description,
                commandArgs: this.logger.isTraceEnabled ? args : ENABLE_TRACE,
            });

            const result = await this.longExecutionHelper.warn({
                description: `Database command: ${command.description}`,
                execute: async () => command.execute(dbManager),
            });

            this.metrics.callDuration.observe({ command: getConstructorName(command) }, stopwatch.elapsedMillis);

            this.logger.logDebug('Successfully executed database {command} after {durationMillis}.', {
                command: command.description,
                durationMillis: stopwatch.elapsedMillis,
                result: this.logger.isTraceEnabled ? result : ENABLE_TRACE,
            });
            return result;
        } catch (error) {
            this.logger.logDebug('Failed to execute database {command} after {durationMillis} because of {error}.', {
                command: description,
                error,
                durationMillis: stopwatch.elapsedMillis,
            });

            throw new Error(`Failed database command '${description}' after ${stopwatch.elapsedMillis} millis.\n`
                + `Arguments: ${safeJsonStringify(args)}\n`
                + `${errorToString(error)}`);
        }
    }
}
