import { argGuard, NumberLimits, StringLimits } from '@tezos-dappetizer/utils';
import { ElementOf } from 'ts-essentials';

export function fluentGuard<T>(value: T, argName: string): FluentGuard<T> {
    return new FluentArgGuardImpl(value, argName) as unknown as FluentGuard<T>;
}

export type FluentGuard<T> =
    T extends (null | undefined)
        ? NullishFluentGuard<T>
        : T extends number
            ? NumberFluentGuard
            : T extends string
                ? StringFluentGuard
                : T extends readonly unknown[]
                    ? ArrayFluentGuard<T>
                    : T extends object
                        ? ObjectFluentGuard<T>
                        : never;

export interface NullishFluentGuard<T> {
    undefinedOr(): FluentGuard<NonNullable<T>>;
    nullOr(): FluentGuard<NonNullable<T>>;
    nullishOr(): FluentGuard<NonNullable<T>>;
}

export interface NumberFluentGuard {
    number(limits?: NumberLimits): void;
}

export interface StringFluentGuard {
    string(limits?: StringLimits): void;
}

export interface ArrayFluentGuard<TArray extends readonly unknown[]> {
    array(validateItem: (guard: FluentGuard<ElementOf<TArray>>) => void): void;
}

export interface ObjectFluentGuard<TObject extends object> {
    object(validateProperties?: (guard: this) => void): void;

    property<TProperty extends keyof TObject & string>(propertyName: TProperty): FluentGuard<TObject[TProperty]>;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export class FluentArgGuardImpl implements NumberFluentGuard, StringFluentGuard, ArrayFluentGuard<unknown[]>, ObjectFluentGuard<any> {
    constructor(
        readonly value: unknown,
        readonly argName: string,
    ) {}

    number(limits?: NumberLimits | undefined): void {
        argGuard.number(this.value as number, this.argName, limits);
    }

    string(limits?: StringLimits): void {
        argGuard.string(this.value as string, this.argName, limits);
    }

    array(validateItem: Function): void {
        argGuard.array(this.value as unknown[], this.argName).forEach((item, index) => {
            validateItem(new FluentArgGuardImpl(item, `${this.argName}[${index}]`));
        });
    }

    object(validateProperties?: ((guard: this) => void) | undefined): void {
        argGuard.object(this.value as object, this.argName);
        validateProperties?.(this);
    }

    property(propertyName: string): FluentArgGuardImpl {
        this.object();
        const propertyValue = (this.value as Record<string, unknown>)[propertyName];
        return new FluentArgGuardImpl(propertyValue, `${this.argName}.${propertyName}`);
    }
}
