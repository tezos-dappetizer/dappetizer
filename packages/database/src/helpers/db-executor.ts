import { InjectionToken } from 'tsyringe';
import { EntityManager } from 'typeorm';

import { DbCommand } from '../commands/db-command';

/** Useful for mocking actual DB execution. Also warns about long execution. */
export interface DbExecutor {
    execute<TResult>(dbManager: EntityManager, command: DbCommand<TResult>): Promise<TResult>;
}

export const DB_EXECUTOR_DI_TOKEN: InjectionToken<DbExecutor> = 'Dappetizer:Database:DbExecutor';
