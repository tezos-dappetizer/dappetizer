import {
    locatePackageJsonInCurrentPackage,
    npmModules,
    PACKAGE_JSON_LOADER_DI_TOKEN,
    PackageJsonLoader,
} from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

import { DatabasePackageJson, DatabasePackageJsonDependencies } from './database-package-json';

@singleton()
export class DatabasePackageJsonImpl implements DatabasePackageJson {
    readonly dependencies: DatabasePackageJsonDependencies;

    constructor(@inject(PACKAGE_JSON_LOADER_DI_TOKEN) loader: PackageJsonLoader) {
        const packageJson = loader.load(locatePackageJsonInCurrentPackage(__filename));

        this.dependencies = Object.freeze<DatabasePackageJsonDependencies>({
            typeorm: packageJson.getDependencyVersion(npmModules.TYPEORM),
        });
    }
}
