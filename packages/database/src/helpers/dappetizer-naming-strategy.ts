import { isWhiteSpace } from '@tezos-dappetizer/utils';
import { snakeCase } from 'lodash';
import { Table } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

/**
 * https://www.postgresql.org/docs/9.3/runtime-config-preset.html
 * TODO this should be injected from db specific package.
 */
const MAX_IDENTIFIER_LENGTH = 63;

export class DappetizerNamingStrategy extends SnakeNamingStrategy {
    override tableName(className: string, customName: string): string {
        const prefix = 'Db';
        const nextChar = className[prefix.length];

        const hasDbPrefix = isWhiteSpace(customName)
            && className.startsWith(prefix)
            && nextChar !== undefined
            && nextChar.toUpperCase() === nextChar;

        return hasDbPrefix
            ? super.tableName(className.substring(prefix.length), customName)
            : super.tableName(className, customName);
    }

    override primaryKeyName(tableOrName: Table | string, columnNames: string[]): string {
        return prepareKeyName('PK', tableOrName, columnNames);
    }

    override foreignKeyName(
        tableOrName: Table | string,
        columnNames: string[],
        _referencedTablePath?: string,
        _referencedColumnNames?: string[],
    ): string {
        return prepareKeyName('FK', tableOrName, columnNames);
    }
}

function prepareKeyName(prefix: string, tableOrName: Table | string, columnNames: string[]): string {
    const tableNameSegment = prepareName(tableOrName instanceof Table ? tableOrName.name : tableOrName);

    // Sort incoming column names to avoid issue when ["id", "name"] and ["name", "id"] arrays.
    const columnsNamesSegment = [...columnNames].sort().map(prepareName).join('_');

    return `${prefix}_${tableNameSegment}_${columnsNamesSegment}`.substring(0, MAX_IDENTIFIER_LENGTH);
}

function prepareName(name: string): string {
    return snakeCase(name).replace(/\.|-/gu, '');
}
