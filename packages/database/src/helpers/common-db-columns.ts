import { StrictOmit } from 'ts-essentials';
import { PrimaryColumnOptions } from 'typeorm';

import { bigNumberDbTransformer } from './big-number-db-transformer';

export type DbColumnOptions = Readonly<StrictOmit<PrimaryColumnOptions, 'nullable'>>;

export const commonDbColumns = Object.freeze({
    blockHash: asOptions({
        type: 'character',
        length: 51,
    }),
    chainId: asOptions({
        type: 'character',
        length: 15,
    }),
    operationGroupHash: asOptions({
        type: 'character',
        length: 51,
    }),
    address: asOptions({
        type: 'character',
        length: 36,
    }),
    bigNumber: asOptions({
        type: 'decimal',
        transformer: bigNumberDbTransformer,
    }),
});

function asOptions(options: DbColumnOptions): DbColumnOptions {
    return Object.freeze(options);
}
