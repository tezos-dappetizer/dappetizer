import { isNullish } from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';

import { DbValueTransformer } from './db-value-transformer';

export const bigNumberDbTransformer: DbValueTransformer<BigNumber, string> = {
    to: value => {
        return !isNullish(value) ? value.toFixed() : null;
    },
    from: dbValue => {
        return !isNullish(dbValue) ? new BigNumber(dbValue) : null;
    },
};
