import { DappetizerConfig } from '@tezos-dappetizer/indexer';
import { StrictOmit } from 'ts-essentials';
import { DataSourceOptions } from 'typeorm';

import { HasuraDappetizerConfig } from '../hasura/config/hasura-dappetizer-config';

/**
 * Entire config for an indexer app built using Dappetizer with standard Dappetizer {@link DbContext}.
 * It corresponds to the value exported from a config file, see {@link "@tezos-dappetizer/utils".candidateConfigFileNames}.
 *
 * Compared to {@link "@tezos-dappetizer/indexer".DappetizerConfig}, it adds {@link database} property
 * which specifies {@link https://github.com/typeorm/typeorm/blob/master/docs/connection-options.md | TypeORM connection}.
 * @example Sample `dappetizer.config.ts` file with only mandatory values specified:
 * ```typescript
 * import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';
 *
 * const config: DappetizerConfigUsingDb = {
 *     modules: [
 *         {
 *             id: 'your-node-module-or-relative-path',
 *             config: 'module-config',
 *         },
 *     ],
 *     networks: {
 *         mainnet: {
 *             indexing: {
 *                 fromBlockLevel: 1796556,
 *             },
 *             tezosNode: {
 *                 url: 'https://mainnet-tezos.giganode.io',
 *             },
 *         },
 *         testnet: {
 *             indexing: {
 *                 fromBlockLevel: 856123,
 *             },
 *             tezosNode: {
 *                 url: 'https://testnet-tezos.giganode.io',
 *             },
 *         },
 *     },
 *     database: {
 *         type: 'sqlite',
 *         database: 'data/db.sqlite',
 *     },
 * };
 *
 * export default config;
 * ```
 * @category Dappetizer Config
 */
export interface DappetizerConfigUsingDb extends DappetizerConfig {
    /**
     * Configures database connection.
     * @limits Regular {@link https://typeorm.io/data-source-options | TypeORM connection}.
     * @example Sample value if you want to use SQLite:
     * ```typescript
     * {
     *     type: 'sqlite',
     *     database: 'data/db.sqlite',
     * }
     * ```
     * @example Sample value if you want to use PostgreSQL:
     * ```typescript
     * {
     *     type: 'postgres',
     *     host: 'localhost',
     *     port: 5432,
     *     username: 'postgres',
     *     password: 'postgrespassword',
     *     database: 'postgres',
     *     schema: 'indexer',
     * }
     * ```
     */
    database: StrictOmit<DataSourceOptions, 'entities'>;

    /** Configures Hasura for the indexer app. */
    hasura?: HasuraDappetizerConfig;
}
