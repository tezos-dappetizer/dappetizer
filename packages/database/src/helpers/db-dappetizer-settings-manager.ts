import { dappetizerAssert, safeJsonStringify } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { DbExecutorImpl } from './db-executor-impl';
import { FindAllDappetizerSettingsCommand } from '../commands/find-all-dappetizer-settings-command';
import { InsertCommand } from '../commands/insert-command';
import { DbDappetizerSettings } from '../entities/db-dappetizer-settings';

@singleton()
export class DbDappetizerSettingsManager {
    private cachedSettings: DbDappetizerSettings | null | 'NotLoadedYet' = 'NotLoadedYet';

    constructor(private readonly dbExecutor: DbExecutorImpl) {}

    async getSettings(): Promise<DbDappetizerSettings | null> {
        if (this.cachedSettings === 'NotLoadedYet') {
            this.cachedSettings = await this.load();
        }
        return this.cachedSettings;
    }

    async setChainId(chainId: string): Promise<void> {
        const settings = await this.getSettings();

        if (!settings) {
            this.cachedSettings = { chainId };
            await this.dbExecutor.executeNoTransaction(new InsertCommand(DbDappetizerSettings, this.cachedSettings));
        } else {
            dappetizerAssert(settings.chainId === chainId, 'Conflicting chain must be checked upfront in the indexer.', {
                existingDbChainId: settings.chainId,
                newIndexedChainId: chainId,
            });
        }
    }

    private async load(): Promise<DbDappetizerSettings | null> {
        const allSettings = await this.dbExecutor.executeNoTransaction(new FindAllDappetizerSettingsCommand());

        if (allSettings.length > 1) {
            throw new Error(`Found multiple conflicting indexer settings in the database: ${safeJsonStringify(allSettings)}`);
        }
        return allSettings[0] ?? null;
    }
}
