import { InjectionToken } from 'tsyringe';

/** @internal Intended only for Dappetizer itself. */
export interface DatabasePackageJson {
    readonly dependencies: DatabasePackageJsonDependencies;
}

/** @internal Intended only for Dappetizer itself. */
export interface DatabasePackageJsonDependencies {
    readonly typeorm: string;
}

/** @internal Intended only for Dappetizer itself. */
export const DATABASE_PACKAGE_JSON_DI_TOKEN: InjectionToken<DatabasePackageJson> = 'Dappetizer:Database:DatabasePackageJson';
