import { InjectionToken } from 'tsyringe';

import { DbInitializer } from './db-initializer';

export const DB_INITIALIZERS_DI_TOKEN: InjectionToken<DbInitializer> = 'Dappetizer:Database:DbInitializer';
