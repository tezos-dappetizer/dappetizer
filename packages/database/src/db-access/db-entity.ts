import { EntitySchema } from 'typeorm';

export type DbEntity = Function | string | EntitySchema;
