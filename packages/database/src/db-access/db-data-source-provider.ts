import { InjectionToken } from 'tsyringe';
import { DataSource } from 'typeorm';

export interface DbDataSourceProvider {
    /** Gets initialized app-wise `DataSource` according to Dappetizer config. */
    getDataSource(): Promise<DataSource>;
}

export const DB_DATA_SOURCE_PROVIDER_DI_TOKEN: InjectionToken<DbDataSourceProvider> = 'Dappetizer:Database:DbDataSourceProvider';
