import { BackgroundWorker } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';
import { DataSource } from 'typeorm';

import { DbDataSourceConnector } from './db-data-source-connector';
import { DbDataSourceProvider } from './db-data-source-provider';

@singleton()
export class DbDataSourceManager implements DbDataSourceProvider, BackgroundWorker {
    readonly name = 'DbConnection';
    private dataSource?: Promise<DataSource> | 'closed';

    constructor(
        private readonly connector: DbDataSourceConnector,
    ) {}

    async getDataSource(): Promise<DataSource> {
        if (this.dataSource === 'closed') {
            throw new Error('Database data source (connection) was already destroyed (closed) so it cannot be used anymore.');
        }
        if (!this.dataSource) {
            this.dataSource = this.connector.connect();
        }
        return this.dataSource;
    }

    async start(): Promise<void> {
        await this.getDataSource();
    }

    async stop(): Promise<void> {
        if (typeof this.dataSource === 'object') {
            const dataSource = await this.dataSource;
            await dataSource.destroy();
        }
        this.dataSource = 'closed';
    }
}
