import { IndexerModule, INDEXER_MODULES_DI_TOKEN } from '@tezos-dappetizer/indexer';
import { injectLogger, Logger } from '@tezos-dappetizer/utils';
import { mapValues } from 'lodash';
import { injectAll, singleton } from 'tsyringe';

import { isModuleUsingDb } from './db-entities-module-verifier';
import { DbEntity } from './db-entity';
import { DAPPETIZER_DB_ENTITIES } from '../entities/dappetizer-db-entities';
import { getEntityName } from '../helpers/naming-helper';

@singleton()
export class DbEntitiesResolver {
    constructor(
        @injectAll(INDEXER_MODULES_DI_TOKEN) private readonly modules: readonly IndexerModule<unknown>[],
        @injectLogger(DbEntitiesResolver) private readonly logger: Logger,
    ) {}

    resolve(): DbEntity[] {
        const dbEntities: Record<string, readonly DbEntity[]> = {
            '@tezos-dappetizer': DAPPETIZER_DB_ENTITIES,
        };

        for (const module of this.modules) {
            dbEntities[module.name] = isModuleUsingDb(module) ? module.dbEntities : [];
        }

        this.logger.logInformation('Using (from respective modules) these {dbEntities} .', {
            dbEntities: mapValues(dbEntities, v => v.map(getEntityName)),
        });
        return Object.values(dbEntities).flatMap(e => e);
    }
}
