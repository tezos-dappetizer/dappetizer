import { getConstructorName, injectLogger, injectValue, Logger } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';
import { DataSource } from 'typeorm';

import { DbInitializer } from './db-initializer';
import { canContainSchema, resolveSchema } from '../helpers/schema-utils';
import { TypeormAbstraction, typeormAbstraction } from '../helpers/typeorm-abstraction';

@singleton()
export class DbSchemaInitializer implements DbInitializer {
    readonly name = getConstructorName(this);
    readonly order = Number.NEGATIVE_INFINITY; // Execute first.

    constructor(
        @injectValue(typeormAbstraction) private readonly typeorm: TypeormAbstraction,
        @injectLogger(DbSchemaInitializer) private readonly logger: Logger,
    ) {}

    async beforeSynchronization(dataSource: DataSource): Promise<void> {
        if (!canContainSchema(dataSource.options)) {
            this.logger.logInformation('The data source of {type} does not support schema. So nothing to initialize.', {
                type: dataSource.options.type,
            });
            return;
        }

        const schema = resolveSchema(dataSource.options);
        this.logger.logInformation('Creating db {schema} if it does not exist yet.', { schema });
        const dataSourceWithoutSchema = this.typeorm.newDataSource({
            ...dataSource.options,
            schema: undefined,
        });
        await dataSourceWithoutSchema.initialize();

        try {
            await dataSourceWithoutSchema.query(`CREATE SCHEMA IF NOT EXISTS "${schema}"`);
            this.logger.logDebug('Created db {schema} if it did not exist yet.', { schema });
        } finally {
            await dataSourceWithoutSchema.destroy();
        }
    }
}
