import { IndexerModule, PartialIndexerModuleVerifier } from '@tezos-dappetizer/indexer';
import { hasConstructor, isObject, isReadOnlyArray, withIndex } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';
import { EntitySchema } from 'typeorm';

import { DbEntity } from './db-entity';
import { IndexerModuleUsingDb } from '../indexer-module-using-db';

const dbEntitiesKey: keyof IndexerModuleUsingDb = 'dbEntities';

@singleton()
export class DbEntitiesModuleVerifier implements PartialIndexerModuleVerifier {
    verify(module: IndexerModule<unknown>): void {
        if (!isModuleUsingDb(module)) {
            return;
        }
        if (!isReadOnlyArray(module.dbEntities)) {
            throw new Error(`Property '${dbEntitiesKey}' must be an array of entities but it is '${typeof module.dbEntities}'.`);
        }

        for (const [entity, index] of withIndex(module.dbEntities)) {
            if (!isCorrectDbEntity(entity)) {
                throw new Error(`Property '${dbEntitiesKey}[${index}]' must be either`
                    + ` a string, a constructor function or an instance of 'typeorm.${EntitySchema.name}' but it is '${typeof entity}'.`);
            }
            if (isFromOtherTypeOrmModuleInstance(entity)) {
                throw new Error(`Property '${dbEntitiesKey}[${index}]' specifies 'typeorm.${EntitySchema.name}'`
                    + ` but it the class from a different 'typeorm' NPM module (in nested 'node_modules' directory)`
                    + ` than the one used by Dappetizer. Hense it is unusable by Dappetizer.`
                    + ` Make sure that there is a single (the same version) 'typeorm' NPM module installed in the root directory of your project.`);
            }
        }
    }
}

function isCorrectDbEntity(entity: DbEntity): boolean {
    switch (typeof entity) {
        case 'string':
        case 'function':
            return true;
        case 'object':
            return hasConstructor(entity, EntitySchema);
        default:
            return false;
    }
}

function isFromOtherTypeOrmModuleInstance(entity: DbEntity): boolean {
    return typeof entity === 'object' && !(entity instanceof EntitySchema);
}

export function isModuleUsingDb(module: IndexerModule<unknown>): module is IndexerModuleUsingDb {
    return isObject(module) && module[dbEntitiesKey] !== undefined;
}
