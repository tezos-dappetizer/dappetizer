import {
    ConfigObjectElement,
    ConfigWithDiagnostics,
    isNullish,
    ROOT_CONFIG_ELEMENT_DI_TOKEN,
} from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';
import { DataSourceOptions } from 'typeorm';

@singleton()
export class DbConfig implements ConfigWithDiagnostics {
    static readonly ROOT_NAME = 'database';
    readonly dataSourceOptions: DataSourceOptions;

    constructor(@inject(ROOT_CONFIG_ELEMENT_DI_TOKEN) rootElement: ConfigObjectElement) {
        const thisElement = rootElement.get(DbConfig.ROOT_NAME).asObject();
        this.dataSourceOptions = applyDefaults(thisElement.value as unknown as DataSourceOptions);
    }

    getDiagnostics(): [string, unknown] {
        return [DbConfig.ROOT_NAME, sanitizeConfidential(this.dataSourceOptions)];
    }
}

function applyDefaults(options: DataSourceOptions): DataSourceOptions {
    options = { // eslint-disable-line no-param-reassign
        ...options,
        synchronize: !isNullish(options.synchronize) ? options.synchronize : true,
    };

    switch (options.type) {
        case 'postgres':
            return {
                ...options,
                connectTimeoutMS: !isNullish(options.connectTimeoutMS) ? options.connectTimeoutMS : 10_000,
            };
        default:
            return options;
    }
}

function sanitizeConfidential(options: DataSourceOptions): DataSourceOptions {
    return 'password' in options
        ? { ...options, password: '***' }
        : options;
}
