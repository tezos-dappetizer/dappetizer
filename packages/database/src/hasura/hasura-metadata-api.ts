import { dumpToString, injectLogger, keyof, Logger } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { ClearMetadataCommand } from './commands/clear-metadata-command';
import { CreateArrayRelationshipCommand } from './commands/create-array-relationship-command';
import { CreateObjectRelationshipCommand } from './commands/create-object-relationship-command';
import { ExportMetadataCommand, HasuraMetadata } from './commands/export-metadata-command';
import { PermitCommand } from './commands/permit-command';
import { TrackCommand } from './commands/track-command';
import { EnabledHasuraConfig, HasuraConfig, HasuraRelationshipType } from './config/hasura-config';
import { HasuraDappetizerConfig } from './config/hasura-dappetizer-config';
import { HasuraCommandExecutor } from './hasura-command-executor';
import { HasuraRelationshipMapping, HasuraTableMapping } from './hasura-mappings';

@singleton()
export class HasuraMetadataApi {
    constructor(
        private readonly executor: HasuraCommandExecutor,
        @injectLogger(HasuraMetadataApi) private readonly logger: Logger,
    ) {}

    async clearMetadata(config: EnabledHasuraConfig): Promise<void> {
        await this.executor.execute(config, new ClearMetadataCommand());

        this.logger.logInformation('Hasura metadata were cleared.');
    }

    async configureTables(config: EnabledHasuraConfig, dbSchema: string, allTables: HasuraTableMapping[]): Promise<void> {
        const tableNamesInHasura = await this.getTableNamesExistingInHasura(config);
        const tablesToConfigure = allTables.filter(m => !tableNamesInHasura.has(m.tableName));

        if (tablesToConfigure.length !== allTables.length) {
            this.logger.logInformation('Hasura has {tables} already configured so skipping their tracking, permissions and relationships.'
                + ' If you want to overwrite the tracking, then set Dappetizer {configProperty} to true.', {
                tables: allTables.map(m => m.tableName).filter(n => tableNamesInHasura.has(n)),
                configProperty: `${HasuraConfig.ROOT_NAME}.${keyof<HasuraDappetizerConfig>('dropExistingTracking')}`,
            });
        }

        // Relationships reference tables -> configure all tables first.
        await Promise.all(tablesToConfigure.map(async t => this.configureTable(config, dbSchema, t)));
        await Promise.all(tablesToConfigure.flatMap(t => t.relationships.map(async r => this.configureRelationship(config, dbSchema, t, r))));
    }

    private async configureTable(config: EnabledHasuraConfig, dbSchema: string, table: HasuraTableMapping): Promise<void> {
        const commandArgs = {
            ...config,
            hasuraSource: config.source,
            dbSchema,
            table,
        };
        await this.executor.execute(config, new TrackCommand(commandArgs));
        await this.executor.execute(config, new PermitCommand(commandArgs));

        this.logger.logInformation('Successfully configured Hasura tracking and permissions for {table} to {graphQLType} and {graphQLFields}.', {
            table: table.tableName,
            graphQLType: table.graphQLNames.type,
            graphQLFields: [table.graphQLNames.singleByKeyField, table.graphQLNames.selectField, table.graphQLNames.aggregateField],
        });
    }

    private async configureRelationship(
        config: EnabledHasuraConfig,
        dbSchema: string,
        table: HasuraTableMapping,
        relationship: HasuraRelationshipMapping,
    ): Promise<void> {
        const commandArgs = {
            hasuraSource: config.source,
            dbSchema,
            tableName: table.tableName,
            relationship,
        };
        switch (relationship.relationshipType) {
            case HasuraRelationshipType.ManyToOne:
                await this.executor.execute(config, new CreateObjectRelationshipCommand(commandArgs));
                break;
            case HasuraRelationshipType.OneToMany:
                await this.executor.execute(config, new CreateArrayRelationshipCommand(commandArgs));
                break;
        }

        this.logger.logInformation('Successfully configured Hasura {graphQLRelationship} on {graphQLType} (corresponding to {table}).', {
            graphQLRelationship: relationship.graphQLName,
            graphQLType: table.graphQLNames.type,
            table: table.tableName,
        });
    }

    private async getTableNamesExistingInHasura(config: EnabledHasuraConfig): Promise<Set<string>> {
        const metadata = await this.executor.execute<HasuraMetadata>(config, new ExportMetadataCommand());

        const source = metadata.sources.find(s => s.name === config.source);
        if (!source) {
            throw new Error(`Data source '${config.source}' does not exist in Hasura.`
                + ` Configure it in Hasura or change Dappetizer Hasura config to an existing source.`
                + ` Existing sources in Hasura are ${dumpToString(metadata.sources.map(s => s.name))}.`);
        }
        return new Set(source.tables.map(t => t.table.name));
    }
}
