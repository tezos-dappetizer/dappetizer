import { ConfigObjectElement, ConfigWithDiagnostics, ROOT_CONFIG_ELEMENT_DI_TOKEN } from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

import { HasuraDappetizerConfig } from './hasura-dappetizer-config';

export interface EnabledHasuraConfig {
    readonly url: string;
    readonly dropExistingTracking: boolean;
    readonly autotrackEntities: boolean;
    readonly adminSecret: string | null;
    readonly source: string;
    readonly selectLimit: number;
    readonly allowAggregations: boolean;
    readonly namingStyle: HasuraNamingStyle;
    readonly customMappings: readonly HasuraCustomMappingConfig[];
    readonly autotrackedMappings: readonly HasuraAutotrackedMappingConfig[];
}

export interface HasuraMappingConfigBase {
    readonly graphQLPluralFieldName: string | null;
    readonly graphQLAggregateFieldName: string | null;
    readonly relationships: readonly HasuraRelationshipConfig[];
}

export interface HasuraCustomMappingConfig extends HasuraMappingConfigBase {
    readonly tableName: string;
    readonly graphQLTypeName: string;
    readonly graphQLFieldName: string;
    readonly columns: readonly HasuraCustomColumnConfig[];
}

export interface HasuraCustomColumnConfig {
    readonly columnName: string;
    readonly graphQLName: string | null;
    readonly hidden: boolean;
}

export interface HasuraAutotrackedMappingConfig extends HasuraMappingConfigBase {
    readonly entityName: string;
    readonly graphQLTypeName: string | null;
    readonly graphQLFieldName: string | null;
    readonly skipped: boolean;
    readonly columns: readonly HasuraAutotrackedColumnConfig[];
}

export interface HasuraRelationshipConfig {
    readonly name: string;
    readonly type: HasuraRelationshipType;
    readonly columns: readonly string[];
    readonly referencedTableName: string;
    readonly referencedColumns: readonly string[];
}

export interface HasuraAutotrackedColumnConfig {
    readonly propertyName: string;
    readonly graphQLName: string | null;
    readonly hidden: boolean;
}

export enum HasuraNamingStyle {
    NoTransformation = 'noTransformation',
    SnakeCase = 'snakeCase',
}

export enum HasuraRelationshipType {
    OneToMany = 'oneToMany',
    ManyToOne = 'manyToOne',
}

export const URL_PROPERTY = 'url';

@singleton()
export class HasuraConfig implements ConfigWithDiagnostics {
    static readonly ROOT_NAME = 'hasura';

    readonly value: EnabledHasuraConfig | null;

    constructor(@inject(ROOT_CONFIG_ELEMENT_DI_TOKEN) rootElement: ConfigObjectElement) {
        const thisElement = rootElement.getOptional(HasuraConfig.ROOT_NAME)?.asObject<keyof HasuraDappetizerConfig>();
        const url = thisElement?.getOptional('url')?.asAbsoluteUrl() ?? null;
        this.value = thisElement && url
            ? {
                url,
                dropExistingTracking: thisElement.getOptional('dropExistingTracking')?.asBoolean()
                    ?? false,
                autotrackEntities: thisElement.getOptional('autotrackEntities')?.asBoolean()
                    ?? true,
                adminSecret: thisElement.getOptional('adminSecret')?.asString('NotWhiteSpace')
                    ?? null,
                source: thisElement.getOptional('source')?.asString('NotWhiteSpace')
                    ?? 'default',
                selectLimit: thisElement.getOptional('selectLimit')?.asInteger({ min: 1 })
                    ?? 100,
                allowAggregations: thisElement.getOptional('allowAggregations')?.asBoolean()
                    ?? true,
                namingStyle: thisElement.getOptional('namingStyle')?.asEnum(HasuraNamingStyle)
                    ?? HasuraNamingStyle.NoTransformation,
                customMappings: thisElement.getOptional('customMappings')?.asArray()
                    .map(e => getCustomMappingConfig(e.asObject()))
                    ?? [],
                autotrackedMappings: thisElement.getOptional('autotrackedMappings')?.asArray()
                    .map(e => getAutotrackedMappingConfig(e.asObject()))
                    ?? [],
            }
            : null;
    }

    getDiagnostics(): [string, unknown] {
        const config = this.value ? sanitizeConfidential(this.value) : 'disabled';
        return [HasuraConfig.ROOT_NAME, config];
    }
}

type DappetizerCustomMappingConfig = NonNullable<HasuraDappetizerConfig['customMappings']>[number];

function getCustomMappingConfig(element: ConfigObjectElement<keyof DappetizerCustomMappingConfig>): HasuraCustomMappingConfig {
    return {
        ...getMappingConfigBase(element),
        tableName: element.get('tableName').asString('NotWhiteSpace'),
        graphQLTypeName: element.get('graphQLTypeName').asString('NotWhiteSpace'),
        graphQLFieldName: element.get('graphQLFieldName').asString('NotWhiteSpace'),
        columns: element.getOptional('columns')?.asArray()
            .map(e => getColumnConfig(e.asObject()))
            ?? [],
    };
}

type DappetizerCustomColumnConfig = NonNullable<DappetizerCustomMappingConfig['columns']>[number];

function getColumnConfig(element: ConfigObjectElement<keyof DappetizerCustomColumnConfig>): HasuraCustomColumnConfig {
    return {
        columnName: element.get('columnName').asString('NotWhiteSpace'),
        graphQLName: element.getOptional('graphQLName')?.asString('NotWhiteSpace')
            ?? null,
        hidden: element.getOptional('hidden')?.asBoolean()
            ?? false,
    };
}

type DappetizerAutotrackedMappingConfig = NonNullable<HasuraDappetizerConfig['autotrackedMappings']>[number];

function getAutotrackedMappingConfig(element: ConfigObjectElement<keyof DappetizerAutotrackedMappingConfig>): HasuraAutotrackedMappingConfig {
    return {
        ...getMappingConfigBase(element),
        entityName: element.get('entityName').asString('NotWhiteSpace'),
        skipped: element.getOptional('skipped')?.asBoolean()
            ?? false,
        graphQLTypeName: element.getOptional('graphQLTypeName')?.asString('NotWhiteSpace')
            ?? null,
        graphQLFieldName: element.getOptional('graphQLFieldName')?.asString('NotWhiteSpace')
            ?? null,
        columns: element.getOptional('columns')?.asArray()
            .map(e => getAutotrackedColumnConfig(e.asObject()))
            ?? [],
    };
}

type DappetizerAutotrackedColumnConfig = NonNullable<DappetizerAutotrackedMappingConfig['columns']>[number];

function getAutotrackedColumnConfig(element: ConfigObjectElement<keyof DappetizerAutotrackedColumnConfig>): HasuraAutotrackedColumnConfig {
    return {
        propertyName: element.get('propertyName').asString('NotWhiteSpace'),
        graphQLName: element.getOptional('graphQLName')?.asString('NotWhiteSpace')
            ?? null,
        hidden: element.getOptional('hidden')?.asBoolean()
            ?? false,
    };
}

function getMappingConfigBase(
    element: ConfigObjectElement<keyof (DappetizerCustomMappingConfig | DappetizerAutotrackedMappingConfig)>,
): HasuraMappingConfigBase {
    return {
        graphQLPluralFieldName: element.getOptional('graphQLPluralFieldName')?.asString('NotWhiteSpace')
            ?? null,
        graphQLAggregateFieldName: element.getOptional('graphQLAggregateFieldName')?.asString('NotWhiteSpace')
            ?? null,
        relationships: element.getOptional('relationships')?.asArray()
            .map(e => getRelationshipConfig(e.asObject()))
            ?? [],
    };
}

type DappetizerRelationshipConfig =
    | NonNullable<DappetizerCustomMappingConfig['relationships']>[number]
    | NonNullable<DappetizerAutotrackedMappingConfig['relationships']>[number];

function getRelationshipConfig(element: ConfigObjectElement<keyof DappetizerRelationshipConfig>): HasuraRelationshipConfig {
    return {
        name: element.get('name').asString('NotWhiteSpace'),
        type: element.getOptional('type')?.asEnum(HasuraRelationshipType)
            ?? HasuraRelationshipType.ManyToOne,
        columns: element.get('columns').asArray('NotEmpty').map(c => c.asString('NotWhiteSpace')),
        referencedTableName: element.get('referencedTableName').asString('NotWhiteSpace'),
        referencedColumns: element.get('referencedColumns').asArray('NotEmpty').map(c => c.asString('NotWhiteSpace')),
    };
}

function sanitizeConfidential(config: EnabledHasuraConfig): unknown {
    return 'adminSecret' in config
        ? { ...config, adminSecret: '***' }
        : config;
}
