import { HasuraRelationshipType } from './config/hasura-config';

export interface HasuraTableMapping {
    readonly tableName: string;
    readonly graphQLNames: GraphQLNames;
    readonly permitAllColumns: boolean;
    readonly columns: readonly HasuraColumnMapping[];
    readonly relationships: readonly HasuraRelationshipMapping[];
}

export interface GraphQLNames {
    readonly type: string;
    readonly singleByKeyField: string;
    readonly selectField: string;
    readonly aggregateField: string;
}

export interface HasuraColumnMapping {
    readonly columnName: string;
    readonly graphQLName: string;
}

export interface HasuraRelationshipMapping {
    readonly graphQLName: string;
    readonly relationshipType: HasuraRelationshipType;
    readonly isCustom: boolean;
    readonly foreignTableName: string;
    readonly sourceToForeignColumns: Readonly<Record<string, string>>;
}
