import { isNotNullish, safeJsonStringify, typed } from '@tezos-dappetizer/utils';
import { fromPairs } from 'lodash';
import pluralize from 'pluralize';
import { singleton } from 'tsyringe';
import { DataSource, EntityMetadata } from 'typeorm';
import { RelationMetadata } from 'typeorm/metadata/RelationMetadata';

import { EnabledHasuraConfig, HasuraRelationshipType } from './config/hasura-config';
import { HasuraRelationshipMapping, HasuraTableMapping } from './hasura-mappings';
import { HasuraNamingHelper } from './hasura-naming-helper';
import { DbDappetizerSettings } from '../entities/db-dappetizer-settings';

@singleton()
export class IndexerEntitiesApi {
    constructor(private readonly namingHelper: HasuraNamingHelper) {}

    getIndexerMappings(dataSource: DataSource, config: EnabledHasuraConfig): HasuraTableMapping[] {
        const builder = new IndexerMappingBuilder(this.namingHelper, config);
        const metadatasMappings = Array.from(builder.getMappingsFromMetadatas(dataSource));
        const customMappings = Array.from(builder.getCustomMappings());

        return [
            ...metadatasMappings.filter(m => !customMappings.find(cm => cm.tableName === m.tableName)),
            ...customMappings,
        ];
    }
}

class IndexerMappingBuilder {
    private readonly entityNamesToSkipTracking: readonly string[] = [DbDappetizerSettings.name];
    private readonly aggregateSuffix: string;

    constructor(
        private readonly namingHelper: HasuraNamingHelper,
        private readonly config: EnabledHasuraConfig,
    ) {
        this.aggregateSuffix = this.namingHelper.getAggregateSuffix(this.config);
    }

    *getMappingsFromMetadatas(dataSource: DataSource): Iterable<HasuraTableMapping> {
        if (!this.config.autotrackEntities) {
            return;
        }

        for (const entityMetadata of dataSource.entityMetadatas) {
            const entityConfig = this.config.autotrackedMappings.find(am => am.entityName === entityMetadata.name);
            if (entityConfig?.skipped === true || this.entityNamesToSkipTracking.includes(entityMetadata.name)) {
                continue;
            }

            const defaultTypeName = hasDbPrefix(entityMetadata.name) ? entityMetadata.name.substring(DB_PREFIX.length) : entityMetadata.name;
            const singleFieldName = entityConfig?.graphQLFieldName ?? this.namingHelper.applyNamingOnRoot(defaultTypeName, this.config);
            const multipleFieldName = entityConfig?.graphQLPluralFieldName ?? pluralize(singleFieldName);

            const columns = entityMetadata.ownColumns
                .map(column => {
                    const columnConfig = entityConfig?.columns.find(c => c.propertyName === column.propertyName);
                    return columnConfig?.hidden !== true
                        ? {
                            columnName: column.databaseName,
                            graphQLName: columnConfig?.graphQLName ?? this.namingHelper.applyNaming(column.propertyName, this.config),
                        }
                        : null;
                })
                .filter(isNotNullish);

            yield {
                tableName: entityMetadata.tableName,
                graphQLNames: {
                    type: entityConfig?.graphQLTypeName ?? defaultTypeName,
                    singleByKeyField: singleFieldName,
                    selectField: multipleFieldName,
                    aggregateField: entityConfig?.graphQLAggregateFieldName ?? `${multipleFieldName}${this.aggregateSuffix}`,
                },
                columns,
                permitAllColumns: columns.length === entityMetadata.ownColumns.length,
                relationships: this.getRelationships(entityMetadata),
            };
        }
    }

    *getCustomMappings(): Iterable<HasuraTableMapping> {
        for (const mappingConfig of this.config.customMappings) {
            const multipleFieldName = mappingConfig.graphQLPluralFieldName ?? pluralize(mappingConfig.graphQLFieldName);
            yield {
                tableName: mappingConfig.tableName,
                graphQLNames: {
                    type: mappingConfig.graphQLTypeName,
                    singleByKeyField: mappingConfig.graphQLFieldName,
                    selectField: multipleFieldName,
                    aggregateField: mappingConfig.graphQLAggregateFieldName ?? `${multipleFieldName}${this.aggregateSuffix}`,
                },
                permitAllColumns: !mappingConfig.columns.some(c => c.hidden),
                columns: mappingConfig.columns
                    .filter(c => !c.hidden)
                    .map(column => ({
                        columnName: column.columnName,
                        graphQLName: column.graphQLName ?? this.namingHelper.applyNaming(column.columnName, this.config),
                    })),
                relationships: mappingConfig.relationships.map(relationship => {
                    if (relationship.columns.length !== relationship.referencedColumns.length) {
                        throw new Error('Source columns count does not match referenced columns count.');
                    }
                    return typed<HasuraRelationshipMapping>({
                        graphQLName: relationship.name,
                        relationshipType: relationship.type,
                        isCustom: true,
                        foreignTableName: relationship.referencedTableName,
                        sourceToForeignColumns: fromPairs(relationship.columns
                            .map((sourceName, index) => [sourceName, relationship.referencedColumns[index] ?? ''])),
                    });
                }),
            };
        }
    }

    private getRelationships(metadata: EntityMetadata): HasuraRelationshipMapping[] {
        const relationships = metadata.relations.map(r => {
            const foreignKeyMetadata = r.foreignKeys[0];
            if (!foreignKeyMetadata) {
                throw new Error(`Relation does not contain foreign key: '${safeJsonStringify(r)}'.`);
            }

            return typed<HasuraRelationshipMapping>({
                graphQLName: this.namingHelper.applyNaming(r.propertyName, this.config),
                relationshipType: this.getRelationshipType(r),
                isCustom: false,
                foreignTableName: foreignKeyMetadata.referencedEntityMetadata.tableName,
                sourceToForeignColumns: fromPairs(foreignKeyMetadata.columnNames
                    .map((sourceName, index) => [sourceName, foreignKeyMetadata.referencedColumnNames[index] ?? ''])),
            });
        });

        const customMappings = this.config.autotrackedMappings
            .find(am => am.relationships.length > 0 && am.entityName === metadata.name);

        for (const relationship of customMappings?.relationships ?? []) {
            if (!relationships.some(r => r.graphQLName === relationship.name)) {
                if (relationship.columns.length !== relationship.referencedColumns.length) {
                    throw new Error('Source columns count does not match referenced columns count.');
                }
                relationships.push({
                    graphQLName: relationship.name,
                    relationshipType: relationship.type,
                    isCustom: true,
                    foreignTableName: relationship.referencedTableName,
                    sourceToForeignColumns: fromPairs(relationship.columns
                        .map((sourceName, index) => [sourceName, relationship.referencedColumns[index] ?? ''])),
                });
            }
        }

        return relationships;
    }

    private getRelationshipType(metadata: RelationMetadata): HasuraRelationshipType {
        if (metadata.isManyToOne) {
            return HasuraRelationshipType.ManyToOne;
        }
        if (metadata.isOneToMany) {
            return HasuraRelationshipType.OneToMany;
        }
        throw new Error(`Unsupported relationship type: ${safeJsonStringify(metadata)}.`);
    }
}

const DB_PREFIX = 'Db';

function hasDbPrefix(name: string): boolean {
    return name.startsWith(DB_PREFIX)
        && name.length > DB_PREFIX.length
        && name.charAt(DB_PREFIX.length).toUpperCase() === name.charAt(DB_PREFIX.length);
}
