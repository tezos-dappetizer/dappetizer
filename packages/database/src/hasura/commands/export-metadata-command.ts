import { HasuraCommand } from './hasura-command';
import { fluentGuard } from '../../helpers/fluent-guard';

export class ExportMetadataCommand implements HasuraCommand<HasuraMetadata> {
    readonly description = 'export metadata';

    createPayload(): object {
        return {
            type: 'export_metadata',
            version: 1,
            args: {},
        };
    }

    transformResponse(response: HasuraMetadata): HasuraMetadata {
        fluentGuard(response, '$').object(metadata => {
            metadata.property('sources').array(source => {
                source.property('name').string('NotWhiteSpace');
                source.property('tables').array(table => {
                    table.property('table').object(innerTable => {
                        innerTable.property('name').string('NotWhiteSpace');
                    });
                });
            });
        });
        return response;
    }
}

export interface HasuraMetadata {
    sources: {
        name: string;
        tables: {
            table: {
                name: string;
            };
        }[];
    }[];
}
