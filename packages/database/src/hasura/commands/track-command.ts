import { fromPairs } from 'lodash';

import { HasuraCommand } from './hasura-command';
import { HasuraTableMapping } from '../hasura-mappings';

export class TrackCommand implements HasuraCommand {
    readonly description: string;

    constructor(readonly args: Readonly<{
        hasuraSource: string;
        dbSchema: string;
        table: HasuraTableMapping;
    }>) {
        this.description = `track table '${args.table.tableName}' to GraphQL type '${args.table.graphQLNames.type}' + related fields`;
    }

    createPayload(): object {
        const column_config = fromPairs(this.args.table.columns.map(c => [c.columnName, { custom_name: c.graphQLName }]));
        // This is deprecated field: https://hasura.io/docs/latest/api-reference/syntax-defs/#customcolumnnames
        const custom_column_names = fromPairs(this.args.table.columns.map(c => [c.columnName, c.graphQLName]));

        return {
            type: 'pg_track_table',
            args: {
                source: this.args.hasuraSource,
                table: {
                    schema: this.args.dbSchema,
                    name: this.args.table.tableName,
                },
                configuration: {
                    custom_name: this.args.table.graphQLNames.type,
                    custom_root_fields: {
                        select: this.args.table.graphQLNames.selectField,
                        select_by_pk: this.args.table.graphQLNames.singleByKeyField,
                        select_aggregate: this.args.table.graphQLNames.aggregateField,
                    },
                    column_config,
                    custom_column_names,
                },
            },
        };
    }
}
