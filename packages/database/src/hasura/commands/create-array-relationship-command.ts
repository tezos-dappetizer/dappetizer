import { HasuraCommand } from './hasura-command';
import { HasuraRelationshipMapping } from '../hasura-mappings';

export class CreateArrayRelationshipCommand implements HasuraCommand {
    readonly description: string;

    constructor(readonly args: Readonly<{
        hasuraSource: string;
        dbSchema: string;
        tableName: string;
        relationship: HasuraRelationshipMapping;
    }>) {
        this.description = `create GraphQL array relationship '${this.args.relationship.graphQLName}' on table '${this.args.tableName}'`;
    }

    createPayload(): object {
        return {
            type: 'pg_create_array_relationship',
            args: {
                source: this.args.hasuraSource,
                name: this.args.relationship.graphQLName,
                table: {
                    schema: this.args.dbSchema,
                    name: this.args.tableName,
                },
                using: {
                    manual_configuration: {
                        remote_table: {
                            schema: this.args.dbSchema,
                            name: this.args.relationship.foreignTableName,
                        },
                        column_mapping: this.args.relationship.sourceToForeignColumns,
                    },
                },
            },
        };
    }
}
