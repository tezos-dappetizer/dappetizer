import { HasuraCommand } from './hasura-command';
import { HasuraTableMapping } from '../hasura-mappings';

export class PermitCommand implements HasuraCommand {
    readonly description: string;

    constructor(readonly args: Readonly<{
        hasuraSource: string;
        dbSchema: string;
        table: HasuraTableMapping;
        selectLimit: number;
        allowAggregations: boolean;
    }>) {
        this.description = `permissions for table '${args.table.tableName}'`;
    }

    createPayload(): object {
        const columns = this.args.table.permitAllColumns
            ? '*'
            : this.args.table.columns.map(c => c.columnName);

        return {
            type: 'pg_create_select_permission',
            args: {
                source: this.args.hasuraSource,
                table: {
                    schema: this.args.dbSchema,
                    name: this.args.table.tableName,
                },
                role: 'public',
                permission: {
                    columns,
                    filter: {},
                    limit: this.args.selectLimit,
                    allow_aggregations: this.args.allowAggregations,
                },
            },
        };
    }
}
