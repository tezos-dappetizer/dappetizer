export interface HasuraCommand<TResponse = void> {
    readonly description: string;

    createPayload(): object;

    transformResponse?(rawResponse: unknown): TResponse;
}
