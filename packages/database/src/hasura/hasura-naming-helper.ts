import { singleton } from 'tsyringe';

import { EnabledHasuraConfig, HasuraNamingStyle } from './config/hasura-config';

@singleton()
export class HasuraNamingHelper {
    applyNamingOnRoot(name: string, config: EnabledHasuraConfig): string {
        return this.applyNaming(`${name.charAt(0).toLowerCase()}${name.substring(1)}`, config);
    }

    applyNaming(name: string, config: EnabledHasuraConfig): string {
        switch (config.namingStyle) {
            case HasuraNamingStyle.NoTransformation:
                return name;
            case HasuraNamingStyle.SnakeCase:
                return toSnakeCase(name);
        }
    }

    getAggregateSuffix(config: EnabledHasuraConfig): string {
        switch (config.namingStyle) {
            case HasuraNamingStyle.NoTransformation:
                return 'Aggregate';
            case HasuraNamingStyle.SnakeCase:
                return '_aggregate';
        }
    }
}

function toSnakeCase(input: string): string {
    const result = input.replace(/[A-Z]/gu, (letter: string) => `_${letter.toLowerCase()}`);
    return result.startsWith('_') ? result.substring(1) : result;
}
