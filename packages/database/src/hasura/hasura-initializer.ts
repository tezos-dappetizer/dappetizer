import { getConstructorName, injectLogger, Logger } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';
import { DataSource } from 'typeorm';

import { HasuraConfig } from './config/hasura-config';
import { HasuraHealthApi } from './hasura-health-api';
import { HasuraMetadataApi } from './hasura-metadata-api';
import { IndexerEntitiesApi } from './indexer-entities-api';
import { DbInitializer } from '../db-access/db-initializer';
import { resolveSchema } from '../helpers/schema-utils';

const FEATURE = 'Hasura metadata initialization';

@singleton()
export class HasuraInitializer implements DbInitializer {
    readonly name = getConstructorName(this);
    readonly order = 100; // Execute last to be sure all db objects are created.

    constructor(
        private readonly config: HasuraConfig,
        private readonly indexerEntitiesApi: IndexerEntitiesApi,
        private readonly healthApi: HasuraHealthApi,
        private readonly metadataApi: HasuraMetadataApi,
        @injectLogger(HasuraInitializer) private readonly logger: Logger,
    ) {}

    async afterSynchronization(dataSource: DataSource): Promise<void> {
        if (!this.config.value) {
            return; // Hasura initialization disabled.
        }
        if (dataSource.options.type !== 'postgres') {
            throw new Error(`${FEATURE} can be enabled in Dappetizer config only for database type 'postgres'`
                + ` but the currently configured database is '${dataSource.options.type}'.`);
        }

        try {
            await this.healthApi.ensureHealthy(this.config.value.url);
        } catch (error) {
            this.logger.logError(`Skipping ${FEATURE} because Hasura is unhealthy: {error}`, { error });
            return;
        }

        try {
            this.logger.logInformation(`Starting ${FEATURE}.`);
            if (this.config.value.dropExistingTracking) {
                await this.metadataApi.clearMetadata(this.config.value);
            }

            const schema = resolveSchema(dataSource.options);
            const tablesWithRelationships = this.indexerEntitiesApi.getIndexerMappings(dataSource, this.config.value);
            await this.metadataApi.configureTables(this.config.value, schema, tablesWithRelationships);

            this.logger.logInformation(`Successfully finished ${FEATURE}.`);
        } catch (error) {
            this.logger.logError(`Failed ${FEATURE}. {error}`, { error });
        }
    }
}
