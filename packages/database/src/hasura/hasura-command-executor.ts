import { appendUrl, injectLogger, JSON_FETCHER_DI_TOKEN, JsonFetcher, Logger } from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

import { HasuraCommand } from './commands/hasura-command';
import { EnabledHasuraConfig } from './config/hasura-config';

const METADATA_URL_PATH = '/v1/metadata';
export const ADMIN_SECRET_HTTP_HEADER = 'X-Hasura-Admin-Secret';

@singleton()
export class HasuraCommandExecutor {
    constructor(
        @inject(JSON_FETCHER_DI_TOKEN) private readonly jsonFetcher: JsonFetcher,
        @injectLogger(HasuraCommandExecutor) private readonly logger: Logger,
    ) {}

    async execute<TResponse>(
        config: EnabledHasuraConfig,
        command: HasuraCommand<TResponse>,
    ): Promise<TResponse> {
        this.logger.logDebug('Executing {command}.', { command: command.description });

        const payload = command.createPayload();
        const response = await this.jsonFetcher.execute({
            url: appendUrl(config.url, METADATA_URL_PATH),
            description: `Hasura ${command.description}`,
            requestOptions: {
                method: 'post',
                body: JSON.stringify(payload),
                headers: config.adminSecret ? { [ADMIN_SECRET_HTTP_HEADER]: config.adminSecret } : undefined,
            },
            transformResponse: (r: TResponse) => command.transformResponse?.(r) ?? r,
        });

        this.logger.logDebug('Successfully executed {command}.', { command: command.description });
        return response;
    }
}
