import { Block, IndexedBlock, IndexerDatabaseHandler } from '@tezos-dappetizer/indexer';
import { inject, singleton } from 'tsyringe';

import { DeleteBlockByHashCommand } from './commands/delete-block-by-hash-command';
import { FindBlockByLevelCommand } from './commands/find-block-by-level-command';
import { FindLatestBlockCommand } from './commands/find-latest-block-command';
import { InsertCommand } from './commands/insert-command';
import { DB_DATA_SOURCE_PROVIDER_DI_TOKEN, DbDataSourceProvider } from './db-access/db-data-source-provider';
import { DbContext } from './db-context';
import { DbBlock } from './entities/db-block';
import { DbDappetizerSettingsManager } from './helpers/db-dappetizer-settings-manager';
import { DbExecutorImpl } from './helpers/db-executor-impl';
import { commitTransaction, rollbackTransaction, startTransaction } from './helpers/db-helpers';

@singleton()
export class DbIndexerDatabaseHandler implements IndexerDatabaseHandler<DbContext> {
    constructor(
        @inject(DB_DATA_SOURCE_PROVIDER_DI_TOKEN) private readonly dbConnectionProvider: DbDataSourceProvider,
        private readonly dbExecutor: DbExecutorImpl,
        private readonly dbSettingsManager: DbDappetizerSettingsManager,
    ) {}

    async startTransaction(): Promise<DbContext> {
        const dbConnection = await this.dbConnectionProvider.getDataSource();
        const dbRunner = dbConnection.createQueryRunner();
        await startTransaction(dbRunner);

        return Object.freeze<DbContext>({
            transaction: dbRunner.manager,
            transactionRunner: dbRunner,
            globalDataSource: dbConnection,
        });
    }

    async commitTransaction(dbContext: DbContext): Promise<void> {
        await commitTransaction(dbContext.transactionRunner);
    }

    async rollBackTransaction(dbContext: DbContext): Promise<void> {
        await rollbackTransaction(dbContext.transactionRunner);
    }

    async insertBlock(block: Block, dbContext: DbContext): Promise<void> {
        const dbBlock: DbBlock = {
            hash: block.hash,
            level: block.level,
            predecessor: block.predecessorBlockHash,
            timestamp: block.timestamp,
        };

        await Promise.all([
            this.dbSettingsManager.setChainId(block.chainId),
            this.dbExecutor.execute(dbContext.transaction, new InsertCommand(DbBlock, dbBlock)),
        ]);
    }

    async deleteBlock(block: IndexedBlock, dbContext: DbContext): Promise<void> {
        await this.dbExecutor.execute(dbContext.transaction, new DeleteBlockByHashCommand(block.hash));
    }

    async getIndexedChainId(): Promise<string | null> {
        const dbIndexerSettings = await this.dbSettingsManager.getSettings();
        return dbIndexerSettings?.chainId ?? null;
    }

    async getLastIndexedBlock(): Promise<IndexedBlock | null> {
        return this.dbExecutor.executeNoTransaction(new FindLatestBlockCommand());
    }

    async getIndexedBlock(level: number): Promise<IndexedBlock | null> {
        return this.dbExecutor.executeNoTransaction(new FindBlockByLevelCommand(level));
    }
}
