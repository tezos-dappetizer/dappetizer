import {
    BlockDataIndexer,
    ContractIndexer,
    IndexerModule,
    IndexerModuleFactory,
    IndexingCycleHandler,
    SmartRollupIndexer,
} from '@tezos-dappetizer/indexer';

import { DbEntity } from './db-access/db-entity';
import { DbContext } from './db-context';

/**
 * {@link "@tezos-dappetizer/indexer".IndexerModule} with standard Dappetizer {@link DbContext}.
 * @typeParam TContextData See the same parameter on {@link "@tezos-dappetizer/indexer".IndexerModule}.
 * @category Client Indexer API
 */
export interface IndexerModuleUsingDb<TContextData = unknown> extends IndexerModule<DbContext, TContextData> {
    /** Database TypeORM entities from this module that should be registered with TypeORM data source. */
    readonly dbEntities: readonly DbEntity[];
}

/**
 * {@link "@tezos-dappetizer/indexer".IndexerModuleFactory} with standard Dappetizer {@link DbContext}.
 * @typeParam TContextData See the same parameter on {@link "@tezos-dappetizer/indexer".IndexerModule}.
 * @category Client Indexer API
 */
export type IndexerModuleUsingDbFactory<TContextData = unknown> =
    IndexerModuleFactory<DbContext, TContextData>;

/**
 * {@link "@tezos-dappetizer/indexer".IndexingCycleHandler} with standard Dappetizer {@link DbContext}.
 * @typeParam TContextData See the same parameter on {@link "@tezos-dappetizer/indexer".IndexerModule}.
 * @category Client Indexer API
 */
export type IndexingCycleHandlerUsingDb<TContextData = unknown> =
    IndexingCycleHandler<DbContext, TContextData>;

/**
 * {@link "@tezos-dappetizer/indexer".BlockDataIndexer} with standard Dappetizer {@link DbContext}.
 * @typeParam TContextData See the same parameter on {@link "@tezos-dappetizer/indexer".IndexerModule}.
 * @category Client Indexer API
 */
export type BlockDataIndexerUsingDb<TContextData = unknown> =
    BlockDataIndexer<DbContext, TContextData>;

/**
 * {@link "@tezos-dappetizer/indexer".ContractIndexer} with standard Dappetizer {@link DbContext}.
 * @typeParam TContextData See the same parameter on {@link "@tezos-dappetizer/indexer".IndexerModule}.
 * @typeParam TContractData See the same parameter on {@link "@tezos-dappetizer/indexer".ContractIndexer}.
 * @category Client Indexer API
 */
export type ContractIndexerUsingDb<TContextData = unknown, TContractData = unknown> =
    ContractIndexer<DbContext, TContextData, TContractData>;

/**
 * {@link "@tezos-dappetizer/indexer".SmartRollupIndexer} with standard Dappetizer {@link DbContext}.
 * @typeParam TContextData See the same parameter on {@link "@tezos-dappetizer/indexer".IndexerModule}.
 * @typeParam TRollupData See the same parameter on {@link "@tezos-dappetizer/indexer".SmartRollupIndexer}.
 * @category Client Indexer API
 * @experimental
 */
export type SmartRollupIndexerUsingDb<TContextData = unknown, TRollupData = unknown> =
    SmartRollupIndexer<DbContext, TContextData, TRollupData>;
