import { isNullish, listAllPropertyNames } from '@tezos-dappetizer/utils';

import { DecoratorAccessor } from './decorator-accessors';
import { MethodDecoratorFactory } from './decorator-factories';

export interface DecoratedMethod<TMethod extends Function, TDecoratorOptions = undefined> {
    readonly name: string;
    readonly execute: TMethod;
    readonly decoratorOptions: TDecoratorOptions;
}

export class DecoratedMethodDiscovery {
    constructor(private readonly accessor = new DecoratorAccessor()) {}

    getMethods<TMethod extends Function, TDecoratorOptions = undefined>(
        decoratedIndexer: object,
        decorator: MethodDecoratorFactory<[TDecoratorOptions]>,
    ): readonly DecoratedMethod<TMethod, TDecoratorOptions>[] {
        const methods: DecoratedMethod<TMethod, TDecoratorOptions>[] = [];

        for (const name of listAllPropertyNames(decoratedIndexer)) {
            const decoratorWrapper = this.accessor.getFromMethod(decorator, decoratedIndexer, name);
            const unboundMethod = (decoratedIndexer as Record<string, unknown>)[name];

            if (!isNullish(decoratorWrapper) && typeof unboundMethod === 'function') {
                methods.push({
                    name,
                    execute: unboundMethod.bind(decoratedIndexer) as TMethod,
                    decoratorOptions: decoratorWrapper.options,
                });
            }
        }
        return methods;
    }

    getSingleMethod<TMethod extends Function, TDecoratorOptions = undefined>(
        decoratedIndexer: object,
        decorator: MethodDecoratorFactory<[TDecoratorOptions]>,
    ): DecoratedMethod<TMethod, TDecoratorOptions> | null {
        const methods = this.getMethods<TMethod, TDecoratorOptions>(decoratedIndexer, decorator);

        if (methods.length > 1) {
            throw new Error(`Multiple methods have @${decorator.name}(...) decorator but there can be only one:`
                + ` ${methods.map(m => `${m.name}(...)`).join(' vs. ')}.`);
        }
        return methods[0] ?? null;
    }
}
