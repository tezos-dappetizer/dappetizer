import { dumpToString } from '@tezos-dappetizer/utils';

import { DecoratedMethodDiscovery } from './decorated-method-discovery';
import { DecoratorAccessor } from './decorator-accessors';
import { ClassDecoratorFactory, MethodDecoratorFactory } from './decorator-factories';

export class NoDecoratorsGuard {
    constructor(
        private readonly decoratedMethodDiscovery = new DecoratedMethodDiscovery(),
        private readonly decoratorAccessor = new DecoratorAccessor(),
    ) {}

    guardNotDecoratedWith(
        decoratedIndexer: object,
        options: {
            methodDecorators: readonly MethodDecoratorFactory[];
            classDecorators: readonly ClassDecoratorFactory[];
            conflictingTypeDescription: string;
        },
    ): void {
        for (const decorator of options.methodDecorators) {
            const methods = this.decoratedMethodDiscovery.getMethods(decoratedIndexer, decorator);
            if (methods.length > 0) {
                throw new Error(`The indexer has methods ${dumpToString(methods.map(m => m.name))} decodated with @${decorator.name}(...)`
                    + ` but that can be used only on ${options.conflictingTypeDescription}.`);
            }
        }

        for (const decorator of options.classDecorators) {
            const result = this.decoratorAccessor.getFromClass(decorator, decoratedIndexer);
            if (result) {
                throw new Error(`The indexer class is decorated with @${decorator.name}(...)`
                    + ` but that can be used only on ${options.conflictingTypeDescription}.`);
            }
        }
    }
}
