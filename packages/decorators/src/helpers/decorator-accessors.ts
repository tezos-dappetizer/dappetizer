import 'reflect-metadata';

import { NonNullish } from '@tezos-dappetizer/utils';

import { ClassDecoratorFactory, MethodDecoratorFactory } from './decorator-factories';

export interface DecoratorWrapper<TOptions> {
    readonly options: TOptions;
}

export function createDecorator<TOptions>(
    decorator: MethodDecoratorFactory<[TOptions]>,
    options?: TOptions,
): MethodDecorator;
export function createDecorator<TOptions>(
    decorator: ClassDecoratorFactory<[TOptions]>,
    options?: TOptions,
): ClassDecorator;
export function createDecorator<TOptions>(
    decorator: (options: TOptions) => unknown,
    options?: TOptions,
): unknown {
    const wrapper: DecoratorWrapper<unknown> = Object.freeze({ options });
    return Reflect.metadata(decorator.name, wrapper);
}

export class DecoratorAccessor {
    getFromMethod<TIndexer extends object, TOptions>(
        decorator: MethodDecoratorFactory<[TOptions]>,
        indexer: TIndexer,
        methodName: keyof TIndexer & string,
    ): DecoratorWrapper<TOptions> | null {
        return (Reflect.getMetadata(decorator.name, indexer, methodName) ?? null) as DecoratorWrapper<TOptions> | null;
    }

    getFromClass<TOptions extends NonNullish>(
        decorator: ClassDecoratorFactory<[TOptions]>,
        indexer: object,
    ): DecoratorWrapper<TOptions> | null {
        return (Reflect.getMetadata(decorator.name, indexer.constructor) ?? null) as DecoratorWrapper<TOptions> | null;
    }
}
