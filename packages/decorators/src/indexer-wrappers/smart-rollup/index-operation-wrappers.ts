import {
    SmartRollupAddMessagesOperation,
    SmartRollupCementOperation,
    SmartRollupExecuteOutboxMessageOperation,
    SmartRollupOriginateOperation,
    SmartRollupPublishOperation,
    SmartRollupRecoverBondOperation,
    SmartRollupRefuteOperation,
    SmartRollupTimeoutOperation,
} from '@tezos-dappetizer/indexer';

import { IndexOperationWrapperBase } from './index-operation-wrapper-base';
import { indexAddMessages } from '../../decorators/smart-rollup/index-add-messages-decorator';
import { indexCement } from '../../decorators/smart-rollup/index-cement-decorator';
import { indexExecuteOutboxMessage } from '../../decorators/smart-rollup/index-execute-outbox-message-decorator';
import { indexOriginate } from '../../decorators/smart-rollup/index-originate-decorator';
import { indexPublish } from '../../decorators/smart-rollup/index-publish-decorator';
import { indexRecoverBond } from '../../decorators/smart-rollup/index-recover-bond-decorator';
import { indexRefute } from '../../decorators/smart-rollup/index-refute-decorator';
import { indexTimeout } from '../../decorators/smart-rollup/index-timeout-decorator';

export class IndexAddMessagesWrapper extends IndexOperationWrapperBase<SmartRollupAddMessagesOperation> {
    protected override decorator = indexAddMessages;
}

export class IndexCementWrapper extends IndexOperationWrapperBase<SmartRollupCementOperation> {
    protected override decorator = indexCement;
}

export class IndexExecuteOutboxMessageWrapper extends IndexOperationWrapperBase<SmartRollupExecuteOutboxMessageOperation> {
    protected override decorator = indexExecuteOutboxMessage;
}

export class IndexOriginateWrapper extends IndexOperationWrapperBase<SmartRollupOriginateOperation> {
    protected override decorator = indexOriginate;
}

export class IndexPublishWrapper extends IndexOperationWrapperBase<SmartRollupPublishOperation> {
    protected override decorator = indexPublish;
}

export class IndexRecoverBondWrapper extends IndexOperationWrapperBase<SmartRollupRecoverBondOperation> {
    protected override decorator = indexRecoverBond;
}

export class IndexRefuteWrapper extends IndexOperationWrapperBase<SmartRollupRefuteOperation> {
    protected override decorator = indexRefute;
}

export class IndexTimeoutWrapper extends IndexOperationWrapperBase<SmartRollupTimeoutOperation> {
    protected override decorator = indexTimeout;
}
