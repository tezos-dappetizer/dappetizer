import { SmartRollupIndexer } from '@tezos-dappetizer/indexer';

import {
    IndexAddMessagesWrapper,
    IndexCementWrapper,
    IndexExecuteOutboxMessageWrapper,
    IndexOriginateWrapper,
    IndexPublishWrapper,
    IndexRecoverBondWrapper,
    IndexRefuteWrapper,
    IndexTimeoutWrapper,
} from './index-operation-wrappers';
import { contractFilter } from '../../decorators/contract/contract-filter-decorator';
import { indexBigMapDiff } from '../../decorators/contract/index-big-map-diff-decorator';
import { indexBigMapUpdate } from '../../decorators/contract/index-big-map-update-decorator';
import { indexEntrypoint } from '../../decorators/contract/index-entrypoint-decorator';
import { indexOrigination } from '../../decorators/contract/index-origination-decorator';
import { indexStorageChange } from '../../decorators/contract/index-storage-change-decorator';
import { indexTransaction } from '../../decorators/contract/index-transaction-decorator';
import { smartRollupFilter } from '../../decorators/smart-rollup/smart-rollup-filter-decorator';
import { NoDecoratorsGuard } from '../../helpers/no-decorators-guard';
import { MethodsWrapper } from '../indexer-wrapper-factory';
import { ShouldIndexWrapper } from '../should-index-wrapper';

export class SmartRollupIndexerWrapper<TDbContext, TContextData, TRollupData>
implements MethodsWrapper<SmartRollupIndexer<TDbContext, TContextData, TRollupData>> {
    // eslint-disable-next-line max-params
    constructor(
        private readonly noDecoratorsGuard = new NoDecoratorsGuard(),
        private readonly indexAddMessagesWrapper = new IndexAddMessagesWrapper(),
        private readonly indexCementWrapper = new IndexCementWrapper(),
        private readonly indexExecuteOutboxMessageWrapper = new IndexExecuteOutboxMessageWrapper(),
        private readonly indexOriginateWrapper = new IndexOriginateWrapper(),
        private readonly indexPublishWrapper = new IndexPublishWrapper(),
        private readonly indexRecoverBondWrapper = new IndexRecoverBondWrapper(),
        private readonly indexRefuteWrapper = new IndexRefuteWrapper(),
        private readonly indexTimeoutWrapper = new IndexTimeoutWrapper(),
        private readonly shouldIndexWrapper = new ShouldIndexWrapper(),
    ) {}

    wrapMethods(decoratedIndexer: object): SmartRollupIndexer<TDbContext, TContextData, TRollupData> {
        this.noDecoratorsGuard.guardNotDecoratedWith(decoratedIndexer, {
            conflictingTypeDescription: 'contract indexer',
            classDecorators: [contractFilter],
            methodDecorators: [indexBigMapDiff, indexBigMapUpdate, indexEntrypoint, indexOrigination, indexStorageChange, indexTransaction],
        });

        return {
            indexAddMessages: this.indexAddMessagesWrapper.wrapMethod(decoratedIndexer),
            indexCement: this.indexCementWrapper.wrapMethod(decoratedIndexer),
            indexExecuteOutboxMessage: this.indexExecuteOutboxMessageWrapper.wrapMethod(decoratedIndexer),
            indexOriginate: this.indexOriginateWrapper.wrapMethod(decoratedIndexer),
            indexPublish: this.indexPublishWrapper.wrapMethod(decoratedIndexer),
            indexRecoverBond: this.indexRecoverBondWrapper.wrapMethod(decoratedIndexer),
            indexRefute: this.indexRefuteWrapper.wrapMethod(decoratedIndexer),
            indexTimeout: this.indexTimeoutWrapper.wrapMethod(decoratedIndexer),
            shouldIndex: this.shouldIndexWrapper.wrapMethod(decoratedIndexer, smartRollupFilter),
        };
    }
}
