import { Applied, SmartRollupOperation } from '@tezos-dappetizer/indexer';
import { AsyncOrSync } from 'ts-essentials';

import { DecoratedMethod } from '../../helpers/decorated-method-discovery';
import { MethodWrapperBase } from '../method-wrapper-base';

export type IndexOperation<TOperation extends SmartRollupOperation, TDbContext, TIndexingContext> = (
    operation: Applied<TOperation>,
    dbContext: TDbContext,
    indexingContext: TIndexingContext,
) => AsyncOrSync<void>;

export abstract class IndexOperationWrapperBase<TOperation extends SmartRollupOperation> extends MethodWrapperBase<Applied<TOperation>> {
    protected override async executeDecorated<TDbContext, TIndexingContext>(
        decoratedMethod: DecoratedMethod<IndexOperation<TOperation, TDbContext, TIndexingContext>>,
        operation: Applied<TOperation>,
        dbContext: TDbContext,
        indexingContext: TIndexingContext,
    ): Promise<void> {
        await decoratedMethod.execute(operation, dbContext, indexingContext);
    }
}
