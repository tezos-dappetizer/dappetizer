import { StorageChange } from '@tezos-dappetizer/indexer';
import { AsyncOrSync } from 'ts-essentials';

import { indexStorageChange } from '../../decorators/contract/index-storage-change-decorator';
import { DecoratedMethod } from '../../helpers/decorated-method-discovery';
import { MethodWrapperBase } from '../method-wrapper-base';

export type IndexStorageChange<TDbContext, TIndexingContext> = (
    newStorageValue: unknown,
    dbContext: TDbContext,
    indexingContext: TIndexingContext,
) => AsyncOrSync<void>;

export class IndexStorageChangeWrapper extends MethodWrapperBase<StorageChange> {
    protected override decorator = indexStorageChange;

    protected override async executeDecorated<TDbContext, TIndexingContext>(
        decoratedMethod: DecoratedMethod<IndexStorageChange<TDbContext, TIndexingContext>>,
        storageChange: StorageChange,
        dbContext: TDbContext,
        indexingContext: TIndexingContext,
    ): Promise<void> {
        const newStorageValue = storageChange.newValue.convert();

        await decoratedMethod.execute(newStorageValue, dbContext, indexingContext);
    }
}
