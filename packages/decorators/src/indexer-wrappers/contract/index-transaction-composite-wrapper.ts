import { ContractIndexer } from '@tezos-dappetizer/indexer';

import { IndexEntrypointWrapper } from './index-entrypoint-wrapper';
import { IndexTransactionsWrapper } from './index-transactions-wrapper';

export class IndexTransactionCompositeWrapper {
    constructor(
        private readonly indexAllTransactionsWrapper = new IndexTransactionsWrapper(),
        private readonly indexEntrypointWrapper = new IndexEntrypointWrapper(),
    ) {}

    wrapMethod<TDbContext, TContextData, TContractData>(
        decoratedIndexer: object,
    ): ContractIndexer<TDbContext, TContextData, TContractData>['indexTransaction'] {
        const indexAllTransactions = this.indexAllTransactionsWrapper.wrapMethod(decoratedIndexer);
        const indexEntrypoint = this.indexEntrypointWrapper.wrapMethod(decoratedIndexer);

        if (!indexAllTransactions && !indexEntrypoint) {
            return undefined;
        }
        if (!indexAllTransactions || !indexEntrypoint) {
            return indexAllTransactions ?? indexEntrypoint;
        }
        return async (transactionParameter, dbContext, indexingContext) => {
            await indexAllTransactions(transactionParameter, dbContext, indexingContext);
            await indexEntrypoint(transactionParameter, dbContext, indexingContext);
        };
    }
}
