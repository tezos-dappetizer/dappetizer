import { BigMapDiff, BigMapInfo } from '@tezos-dappetizer/indexer';
import { AsyncOrSync } from 'ts-essentials';

import { isMatched } from './index-big-map-update-wrapper';
import { indexBigMapDiff } from '../../decorators/contract/index-big-map-diff-decorator';
import { DecoratedMethod } from '../../helpers/decorated-method-discovery';
import { MethodWrapperBase } from '../method-wrapper-base';

export type IndexBigMapDiff<TDbContext, TIndexingContext> = (
    diff: BigMapDiff,
    dbContext: TDbContext,
    indexingContext: TIndexingContext,
) => AsyncOrSync<void>;

export type DecoratorOptions = Parameters<typeof indexBigMapDiff>[0];

export class IndexBigMapDiffWrapper extends MethodWrapperBase<BigMapDiff, DecoratorOptions> {
    protected override decorator = indexBigMapDiff;

    protected override async executeDecorated<TDbContext, TIndexingContext>(
        decoratedMethod: DecoratedMethod<IndexBigMapDiff<TDbContext, TIndexingContext>, DecoratorOptions>,
        diff: BigMapDiff,
        dbContext: TDbContext,
        indexingContext: TIndexingContext,
    ): Promise<void> {
        if (isAnyMatched(decoratedMethod.decoratorOptions, [diff.bigMap, diff.sourceBigMap, diff.destinationBigMap])) {
            await decoratedMethod.execute(diff, dbContext, indexingContext);
        }
    }
}

function isAnyMatched(options: DecoratorOptions, bigMaps: (BigMapInfo | null)[]): boolean {
    return !options ? true : bigMaps.some(m => m && isMatched(options, m));
}
