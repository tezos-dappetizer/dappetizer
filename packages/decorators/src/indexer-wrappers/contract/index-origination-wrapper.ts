import { ContractOrigination } from '@tezos-dappetizer/indexer';
import { AsyncOrSync } from 'ts-essentials';

import { indexOrigination } from '../../decorators/contract/index-origination-decorator';
import { DecoratedMethod } from '../../helpers/decorated-method-discovery';
import { MethodWrapperBase } from '../method-wrapper-base';

export type IndexOrigination<TDbContext, TIndexingContext> = (
    initialStorage: unknown,
    dbContext: TDbContext,
    indexingContext: TIndexingContext,
) => AsyncOrSync<void>;

export class IndexOriginationWrapper extends MethodWrapperBase<ContractOrigination> {
    protected override decorator = indexOrigination;

    protected override async executeDecorated<TDbContext, TIndexingContext>(
        decoratedMethod: DecoratedMethod<IndexOrigination<TDbContext, TIndexingContext>>,
        origination: ContractOrigination,
        dbContext: TDbContext,
        indexingContext: TIndexingContext,
    ): Promise<void> {
        const lazyInitialStorage = await origination.result.getInitialStorage();
        const initialStorage = lazyInitialStorage.convert();

        await decoratedMethod.execute(initialStorage, dbContext, indexingContext);
    }
}
