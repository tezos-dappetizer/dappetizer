import { BigMapInfo, BigMapUpdate } from '@tezos-dappetizer/indexer';
import { dappetizerAssert, DappetizerBug } from '@tezos-dappetizer/utils';
import { isEqual } from 'lodash';
import { AsyncOrSync } from 'ts-essentials';

import { indexBigMapUpdate } from '../../decorators/contract/index-big-map-update-decorator';
import { DecoratedMethod } from '../../helpers/decorated-method-discovery';
import { MethodWrapperBase } from '../method-wrapper-base';

export type IndexBigMapUpdate<TDbContext, TIndexingContext> = (
    key: unknown,
    value: unknown,
    dbContext: TDbContext,
    indexingContext: TIndexingContext,
) => AsyncOrSync<void>;

export type DecoratorOptions = Parameters<typeof indexBigMapUpdate>[0];

export class IndexBigMapUpdateWrapper extends MethodWrapperBase<BigMapUpdate, DecoratorOptions> {
    protected override decorator = indexBigMapUpdate;

    protected override async executeDecorated<TDbContext, TIndexingContext>(
        decoratedMethod: DecoratedMethod<IndexBigMapUpdate<TDbContext, TIndexingContext>, DecoratorOptions>,
        update: BigMapUpdate,
        dbContext: TDbContext,
        indexingContext: TIndexingContext,
    ): Promise<void> {
        if (!update.bigMap) {
            return; // Temporary big map.
        }
        dappetizerAssert(update.key !== null, 'Key cannot be null if big map exists.', { update });

        if (isMatched(decoratedMethod.decoratorOptions, update.bigMap)) {
            const key = update.key.convert();
            const value = update.value ? update.value.convert() : null;

            await decoratedMethod.execute(key, value, dbContext, indexingContext);
        }
    }
}

export function isMatched(options: DecoratorOptions, bigMap: BigMapInfo): boolean {
    if (options.name) {
        return options.name === bigMap.name;
    }
    if (options.path) {
        return isEqual(options.path, bigMap.path);
    }
    throw new DappetizerBug('This should be validated upfront.');
}
