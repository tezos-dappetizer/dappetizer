import { ContractIndexer } from '@tezos-dappetizer/indexer';

import { IndexBigMapDiffWrapper } from './index-big-map-diff-wrapper';
import { IndexBigMapUpdateWrapper } from './index-big-map-update-wrapper';
import { IndexEventWrapper } from './index-event-wrapper';
import { IndexOriginationWrapper } from './index-origination-wrapper';
import { IndexStorageChangeWrapper } from './index-storage-change-wrapper';
import { IndexTransactionCompositeWrapper } from './index-transaction-composite-wrapper';
import { contractFilter } from '../../decorators/contract/contract-filter-decorator';
import { indexAddMessages } from '../../decorators/smart-rollup/index-add-messages-decorator';
import { indexCement } from '../../decorators/smart-rollup/index-cement-decorator';
import { indexExecuteOutboxMessage } from '../../decorators/smart-rollup/index-execute-outbox-message-decorator';
import { indexOriginate } from '../../decorators/smart-rollup/index-originate-decorator';
import { indexPublish } from '../../decorators/smart-rollup/index-publish-decorator';
import { indexRecoverBond } from '../../decorators/smart-rollup/index-recover-bond-decorator';
import { indexRefute } from '../../decorators/smart-rollup/index-refute-decorator';
import { indexTimeout } from '../../decorators/smart-rollup/index-timeout-decorator';
import { smartRollupFilter } from '../../decorators/smart-rollup/smart-rollup-filter-decorator';
import { NoDecoratorsGuard } from '../../helpers/no-decorators-guard';
import { MethodsWrapper } from '../indexer-wrapper-factory';
import { ShouldIndexWrapper } from '../should-index-wrapper';

export class ContractIndexerWrapper<TDbContext, TContextData, TContractData>
implements MethodsWrapper<ContractIndexer<TDbContext, TContextData, TContractData>> {
    // eslint-disable-next-line max-params
    constructor(
        private readonly noDecoratorsGuard = new NoDecoratorsGuard(),
        private readonly indexBigMapDiffWrapper = new IndexBigMapDiffWrapper(),
        private readonly indexBigMapUpdateWrapper = new IndexBigMapUpdateWrapper(),
        private readonly indexEventWrapper = new IndexEventWrapper(),
        private readonly indexOriginationWrapper = new IndexOriginationWrapper(),
        private readonly indexStorageChangeWrapper = new IndexStorageChangeWrapper(),
        private readonly indexTransactionWrapper = new IndexTransactionCompositeWrapper(),
        private readonly shouldIndexWrapper = new ShouldIndexWrapper(),
    ) {}

    wrapMethods(decoratedIndexer: object): ContractIndexer<TDbContext, TContextData, TContractData> {
        this.noDecoratorsGuard.guardNotDecoratedWith(decoratedIndexer, {
            conflictingTypeDescription: 'contract indexer',
            classDecorators: [smartRollupFilter],
            methodDecorators: [
                indexAddMessages, indexCement, indexExecuteOutboxMessage, indexOriginate, indexPublish, indexRecoverBond, indexRefute, indexTimeout,
            ],
        });

        return {
            indexBigMapDiff: this.indexBigMapDiffWrapper.wrapMethod(decoratedIndexer),
            indexBigMapUpdate: this.indexBigMapUpdateWrapper.wrapMethod(decoratedIndexer),
            indexEvent: this.indexEventWrapper.wrapMethod(decoratedIndexer),
            indexOrigination: this.indexOriginationWrapper.wrapMethod(decoratedIndexer),
            indexStorageChange: this.indexStorageChangeWrapper.wrapMethod(decoratedIndexer),
            indexTransaction: this.indexTransactionWrapper.wrapMethod(decoratedIndexer),
            shouldIndex: this.shouldIndexWrapper.wrapMethod(decoratedIndexer, contractFilter),
        };
    }
}
