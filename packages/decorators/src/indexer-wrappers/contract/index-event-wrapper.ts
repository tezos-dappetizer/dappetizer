import { ContractEvent } from '@tezos-dappetizer/indexer';
import { AsyncOrSync } from 'ts-essentials';

import { indexEvent } from '../../decorators/contract/index-event-decorator';
import { DecoratedMethod } from '../../helpers/decorated-method-discovery';
import { MethodWrapperBase } from '../method-wrapper-base';

export type IndexEvent<TDbContext, TIndexingContext> = (
    eventPayload: unknown,
    dbContext: TDbContext,
    indexingContext: TIndexingContext,
) => AsyncOrSync<void>;

export type DecoratorOptions = Parameters<typeof indexEvent>[0];

export class IndexEventWrapper extends MethodWrapperBase<ContractEvent, DecoratorOptions> {
    protected override decorator = indexEvent;

    protected override async executeDecorated<TDbContext, TIndexingContext>(
        decoratedMethod: DecoratedMethod<IndexEvent<TDbContext, TIndexingContext>, DecoratorOptions>,
        event: ContractEvent,
        dbContext: TDbContext,
        indexingContext: TIndexingContext,
    ): Promise<void> {
        if (event.tag === decoratedMethod.decoratorOptions) {
            const eventPayload = event.payload?.convert() ?? null;

            await decoratedMethod.execute(eventPayload, dbContext, indexingContext);
        }
    }
}
