import { TransactionParameter } from '@tezos-dappetizer/indexer';
import { AsyncOrSync } from 'ts-essentials';

import { indexEntrypoint } from '../../decorators/contract/index-entrypoint-decorator';
import { DecoratedMethod } from '../../helpers/decorated-method-discovery';
import { MethodWrapperBase } from '../method-wrapper-base';

export type IndexTransaction<TDbContext, TIndexingContext> = (
    parameterValue: unknown,
    dbContext: TDbContext,
    indexingContext: TIndexingContext,
) => AsyncOrSync<void>;

export type DecoratorOptions = Parameters<typeof indexEntrypoint>[0];

export class IndexEntrypointWrapper extends MethodWrapperBase<TransactionParameter, DecoratorOptions> {
    protected override decorator = indexEntrypoint;

    protected override async executeDecorated<TDbContext, TIndexingContext>(
        decoratedMethod: DecoratedMethod<IndexTransaction<TDbContext, TIndexingContext>, DecoratorOptions>,
        transactionParameter: TransactionParameter,
        dbContext: TDbContext,
        indexingContext: TIndexingContext,
    ): Promise<void> {
        if (transactionParameter.entrypoint === decoratedMethod.decoratorOptions) {
            const parameterValue = transactionParameter.value.convert();

            await decoratedMethod.execute(parameterValue, dbContext, indexingContext);
        }
    }
}
