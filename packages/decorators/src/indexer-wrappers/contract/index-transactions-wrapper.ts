import { TransactionParameter } from '@tezos-dappetizer/indexer';
import { AsyncOrSync } from 'ts-essentials';

import { indexTransaction } from '../../decorators/contract/index-transaction-decorator';
import { DecoratedMethod } from '../../helpers/decorated-method-discovery';
import { MethodWrapperBase } from '../method-wrapper-base';

export type IndexAnyTransaction<TDbContext, TIndexingContext> = (
    parameter: { entrypoint: string; value: unknown },
    dbContext: TDbContext,
    indexingContext: TIndexingContext,
) => AsyncOrSync<void>;

export class IndexTransactionsWrapper extends MethodWrapperBase<TransactionParameter> {
    protected override decorator = indexTransaction;

    protected override async executeDecorated<TDbContext, TIndexingContext>(
        decoratedMethod: DecoratedMethod<IndexAnyTransaction<TDbContext, TIndexingContext>>,
        transactionParameter: TransactionParameter,
        dbContext: TDbContext,
        indexingContext: TIndexingContext,
    ): Promise<void> {
        const parameter: Parameters<IndexAnyTransaction<TDbContext, TIndexingContext>>[0] = {
            entrypoint: transactionParameter.entrypoint,
            value: transactionParameter.value.convert(),
        };

        await decoratedMethod.execute(parameter, dbContext, indexingContext);
    }
}
