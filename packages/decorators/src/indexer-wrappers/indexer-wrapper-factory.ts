import { argGuard, errorToString, getConstructorName, isNullish, isWhiteSpace } from '@tezos-dappetizer/utils';

export interface MethodsWrapper<TIndexer extends NamedIndexer> {
    wrapMethods(decoratedIndexer: object): TIndexer;
}

export interface NamedIndexer {
    readonly name?: string;
}

export class IndexerWrapperFactory<TIndexer extends NamedIndexer> {
    constructor(private readonly wrapper: MethodsWrapper<TIndexer>) {}

    create(
        decoratedIndexer: object,
        indexerTypeDescription: string,
    ): TIndexer {
        argGuard.object(decoratedIndexer, 'decoratedIndexer');
        try {
            const wrappedMethods = this.wrapper.wrapMethods(decoratedIndexer);

            if (Object.values(wrappedMethods).every(isNullish)) {
                throw new Error('No decorated index methods discovered.');
            }

            const name = resolveName(decoratedIndexer);
            return Object.freeze({ ...wrappedMethods, name });
        } catch (error) {
            const indexerName = getConstructorName(decoratedIndexer);
            throw new Error(`Failed to create ${indexerTypeDescription} from ${indexerName} class based on its decorators. ${errorToString(error)}`);
        }
    }
}

function resolveName(decoratedIndexer: object): string {
    const explicitName = (decoratedIndexer as Record<string, unknown>).name;
    if (typeof explicitName === 'string' && !isWhiteSpace(explicitName)) {
        return explicitName;
    }

    const ctorName = getConstructorName(decoratedIndexer);
    if (ctorName !== Object.name) {
        return ctorName;
    }

    throw new Error(`There is no valid 'name' property nor valid constructor. So specify valid 'name' property explicitly.`);
}
