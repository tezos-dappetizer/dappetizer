import { Contract, SmartRollup } from '@tezos-dappetizer/indexer';
import { Nullish } from '@tezos-dappetizer/utils';
import { AsyncOrSync } from 'ts-essentials';

import { SelectiveIndexingOptions } from '../decorators/selective-filter-options-validator';
import { shouldIndex } from '../decorators/should-index-decorator';
import { DecoratedMethodDiscovery } from '../helpers/decorated-method-discovery';
import { DecoratorAccessor } from '../helpers/decorator-accessors';
import { ClassDecoratorFactory } from '../helpers/decorator-factories';

export type SelectiveIndexingItem = Pick<Contract | SmartRollup, 'address' | 'config'>;

export type ShouldIndex<TItem extends SelectiveIndexingItem, TDbContext, TItemData> = (
    item: TItem,
    dbContext: TDbContext,
) => AsyncOrSync<TItemData | false>;

export class ShouldIndexWrapper {
    constructor(
        private readonly decoratedMethodDiscovery = new DecoratedMethodDiscovery(),
        private readonly decoratorAccessor = new DecoratorAccessor(),
    ) {}

    wrapMethod<TItem extends SelectiveIndexingItem, TDbContext, TItemData>(
        decoratedIndexer: object,
        decorator: ClassDecoratorFactory<[SelectiveIndexingOptions]>,
    ): ShouldIndex<TItem, TDbContext, TItemData> | undefined {
        const shouldIndexMethod = this.decoratedMethodDiscovery
            .getSingleMethod<ShouldIndex<TItem, TDbContext, TItemData>>(decoratedIndexer, shouldIndex);
        const contractFilterOptions = this.decoratorAccessor.getFromClass(decorator, decoratedIndexer)?.options;

        return shouldIndexMethod || contractFilterOptions
            ? async (item, dbContext) => {
                if (!isFilterMatched(contractFilterOptions, item)) {
                    return false;
                }
                if (shouldIndexMethod) {
                    return shouldIndexMethod.execute(item, dbContext);
                }
                return undefined as TItemData;
            }
            : undefined;
    }
}

function isFilterMatched(
    options: Nullish<SelectiveIndexingOptions>,
    item: SelectiveIndexingItem,
): boolean {
    if (options?.name) {
        return options.name === item.config.name;
    }
    if (options?.anyOfAddresses) {
        return options.anyOfAddresses.includes(item.address);
    }
    return true;
}
