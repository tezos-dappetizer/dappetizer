import { createDecorator } from '../../helpers/decorator-accessors';

/**
 * The decorator for a method of a contract indexer to index its transactions with the specified entrypoint.
 * @example Demonstrates the method signature:
 * ```typescript
 * import { DbContext } from '@tezos-dappetizer/database';
 * import { indexPublish } from '@tezos-dappetizer/decorators';
 * import { Applied, SmartRollupPublishIndexingContext } from '@tezos-dappetizer/indexer';
 *
 * class YourSmartRollupIndexer {
 *     @indexPublish()
 *     indexMyRollupPublished(
 *         operation: Applied<SmartRollupPublishOperation>,
 *         dbContext: TDbContext,
 *         indexingContext: SmartRollupPublishIndexingContext<TContextData, TRollupData>,
 *     ): void | PromiseLike<void> {
 *         // Your indexing code.
 *     }
 * }
 * ```
 * @remark This is based on {@link "@tezos-dappetizer/indexer".SmartRollupIndexer.indexPublish | SmartRollupIndexer.indexPublish(...)}.
 * @experimental
 */
export function indexPublish(): MethodDecorator {
    return createDecorator(indexPublish);
}
