import { createDecorator } from '../../helpers/decorator-accessors';

/**
 * The decorator for a method of a contract indexer to index its transactions with the specified entrypoint.
 * @example Demonstrates the method signature:
 * ```typescript
 * import { DbContext } from '@tezos-dappetizer/database';
 * import { indexRefute } from '@tezos-dappetizer/decorators';
 * import { Applied, SmartRollupRefuteIndexingContext } from '@tezos-dappetizer/indexer';
 *
 * class YourSmartRollupIndexer {
 *     @indexRefute()
 *     indexMyRollupRefuteed(
 *         operation: Applied<SmartRollupRefuteOperation>,
 *         dbContext: TDbContext,
 *         indexingContext: SmartRollupRefuteIndexingContext<TContextData, TRollupData>,
 *     ): void | PromiseLike<void> {
 *         // Your indexing code.
 *     }
 * }
 * ```
 * @remark This is based on {@link "@tezos-dappetizer/indexer".SmartRollupIndexer.indexRefute | SmartRollupIndexer.indexRefute(...)}.
 * @experimental
 */
export function indexRefute(): MethodDecorator {
    return createDecorator(indexRefute);
}
