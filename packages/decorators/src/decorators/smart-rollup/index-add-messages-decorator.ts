import { createDecorator } from '../../helpers/decorator-accessors';

/**
 * The decorator for a method of a contract indexer to index its transactions with the specified entrypoint.
 * @example Demonstrates the method signature:
 * ```typescript
 * import { DbContext } from '@tezos-dappetizer/database';
 * import { indexAddMessages } from '@tezos-dappetizer/decorators';
 * import { Applied, SmartRollupAddMessagesOperation } from '@tezos-dappetizer/indexer';
 *
 * class YourSmartRollupIndexer {
 *     @indexAddMessages()
 *     indexMyMessages(
 *         operation: Applied<SmartRollupAddMessagesOperation>,
 *         dbContext: TDbContext,
 *         indexingContext: SmartRollupAddMessagesIndexingContext<TContextData>,
 *     ): void | PromiseLike<void> {
 *         // Your indexing code.
 *     }
 * }
 * ```
 * @remark This is based on {@link "@tezos-dappetizer/indexer".SmartRollupIndexer.indexAddMessages | SmartRollupIndexer.indexAddMessages(...)}.
 * @experimental
 */
export function indexAddMessages(): MethodDecorator {
    return createDecorator(indexAddMessages);
}
