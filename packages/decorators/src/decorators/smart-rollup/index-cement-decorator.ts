import { createDecorator } from '../../helpers/decorator-accessors';

/**
 * The decorator for a method of a contract indexer to index its transactions with the specified entrypoint.
 * @example Demonstrates the method signature:
 * ```typescript
 * import { DbContext } from '@tezos-dappetizer/database';
 * import { indexCement } from '@tezos-dappetizer/decorators';
 * import { Applied, SmartRollupCementIndexingContext } from '@tezos-dappetizer/indexer';
 *
 * class YourSmartRollupIndexer {
 *     @indexCement()
 *     indexMyRollupCemented(
 *         operation: Applied<SmartRollupCementOperation>,
 *         dbContext: TDbContext,
 *         indexingContext: SmartRollupCementIndexingContext<TContextData, TRollupData>,
 *     ): void | PromiseLike<void> {
 *         // Your indexing code.
 *     }
 * }
 * ```
 * @remark This is based on {@link "@tezos-dappetizer/indexer".SmartRollupIndexer.indexCement | SmartRollupIndexer.indexCement(...)}.
 * @experimental
 */
export function indexCement(): MethodDecorator {
    return createDecorator(indexCement);
}
