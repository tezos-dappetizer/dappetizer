import { createDecorator } from '../../helpers/decorator-accessors';

/**
 * The decorator for a method of a contract indexer to index its transactions with the specified entrypoint.
 * @example Demonstrates the method signature:
 * ```typescript
 * import { DbContext } from '@tezos-dappetizer/database';
 * import { indexRecoverBond } from '@tezos-dappetizer/decorators';
 * import { Applied, SmartRollupRecoverBondIndexingContext } from '@tezos-dappetizer/indexer';
 *
 * class YourSmartRollupIndexer {
 *     @indexRecoverBond()
 *     indexMyRollupRecoverBonded(
 *         operation: Applied<SmartRollupRecoverBondOperation>,
 *         dbContext: TDbContext,
 *         indexingContext: SmartRollupRecoverBondIndexingContext<TContextData, TRollupData>,
 *     ): void | PromiseLike<void> {
 *         // Your indexing code.
 *     }
 * }
 * ```
 * @remark This is based on {@link "@tezos-dappetizer/indexer".SmartRollupIndexer.indexRecoverBond | SmartRollupIndexer.indexRecoverBond(...)}.
 * @experimental
 */
export function indexRecoverBond(): MethodDecorator {
    return createDecorator(indexRecoverBond);
}
