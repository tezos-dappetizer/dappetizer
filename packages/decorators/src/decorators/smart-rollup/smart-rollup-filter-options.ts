/* eslint-disable @typescript-eslint/no-unnecessary-condition */
import { NonEmptyArray } from 'ts-essentials';

/**
 * Options for matching the smart rollup to index. Used by {@link smartRollupFilter} decorator.
 * @property `name` Matches smart rollup by its `name` which comes from {@link "@tezos-dappetizer/indexer".IndexingDappetizerConfig.smartRollups}.
 * @property `anyOfAddresses` Matches smart rollup if its {@link "@tezos-dappetizer/indexer".SmartRollup.address} is one of these addresses.
 *      It must be a non-empty array.
 * @experimental
 */
export type SmartRollupFilterOptions =
    | {
        /** Matches smart rollup by its `name` which comes from {@link "@tezos-dappetizer/indexer".IndexingDappetizerConfig.smartRollups}. */
        readonly name: string;
        anyOfAddresses?: undefined;
    }
    | {
        /**
         * Matches smart rollup if its {@link "@tezos-dappetizer/indexer".SmartRollup.address} is one of these addresses.
         * It must be a non-empty array.
         */
        readonly anyOfAddresses: Readonly<NonEmptyArray<string>>;
        name?: undefined;
    };
