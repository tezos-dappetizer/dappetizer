import { IndexBigMapOptions } from './index-big-map-options';
import { validateBigMapOptions } from './index-big-map-options-validator';
import { createDecorator } from '../../helpers/decorator-accessors';

/** See {@link IndexBigMapOptions}. */
export type IndexBigMapUpdateOptions = IndexBigMapOptions;

/**
 * The decorator for a method of a contract indexer to index updates of the specified big map.
 * @example Demonstrates the method signature:
 * ```typescript
 * import { DbContext } from '@tezos-dappetizer/database';
 * import { indexBigMapUpdate } from '@tezos-dappetizer/decorators';
 * import { BigMapUpdateIndexingContext } from '@tezos-dappetizer/indexer';
 *
 * class YourContractIndexer {
 *     @indexBigMapUpdate({ name: 'ledger' })
 *     indexLedgerUpdate(
 *         key: StronglyTypedBigMapKey,
 *         value: StronglyTypedBigMapValue | null, // Null if entry removal.
 *         dbContext: DbContext,
 *         indexingContext: BigMapUpdateIndexingContext<TContextData, TContractData>,
 *     ): void | PromiseLike<void> {
 *         // Your indexing code.
 *     }
 * }
 * ```
 * @remark This is based on {@link "@tezos-dappetizer/indexer".ContractIndexer.indexBigMapUpdate | ContractIndexer.indexBigMapUpdate(...)}.
 */
export function indexBigMapUpdate(options: IndexBigMapUpdateOptions): MethodDecorator {
    const validatedOptions = validateBigMapOptions(options, indexBigMapUpdate);
    return createDecorator(indexBigMapUpdate, validatedOptions);
}
