/* eslint-disable @typescript-eslint/no-unnecessary-condition */
import { ArgError, argGuard, keyof } from '@tezos-dappetizer/utils';

import { IndexBigMapOptions } from './index-big-map-options';

export function validateBigMapOptions(options: IndexBigMapOptions, decorator: Function): IndexBigMapOptions {
    const NAME = keyof<typeof options>('name');
    const PATH = keyof<typeof options>('path');
    const baseArgName = `@${decorator.name}.options`;

    argGuard.object(options, baseArgName);

    if (options.name !== undefined && options.path !== undefined) {
        throw createOptionsError(`Both '${NAME}' and '${PATH}' cannot be specified.`);
    }
    if (options.name !== undefined) {
        argGuard.nonWhiteSpaceString(options.name, `${baseArgName}.${NAME}`);

        return { name: options.name }; // Copy to prevent further changes.
    }
    if (options.path !== undefined) {
        argGuard.array(options.path, `${baseArgName}.${PATH}`)
            .forEach((a, i) => argGuard.nonWhiteSpaceString(a, `${baseArgName}.${PATH}[${i}]`));

        return { path: [...options.path] }; // Copy to prevent further changes.
    }
    throw createOptionsError(`Either '${NAME}' or '${PATH}' must be specified.`);

    function createOptionsError(details: string): Error {
        return new ArgError({ argName: baseArgName, specifiedValue: options, details });
    }
}
