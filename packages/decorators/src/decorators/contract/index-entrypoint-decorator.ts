import { argGuard } from '@tezos-dappetizer/utils';

import { createDecorator } from '../../helpers/decorator-accessors';

/**
 * The decorator for a method of a contract indexer to index its transactions with the specified entrypoint.
 * @example Demonstrates the method signature:
 * ```typescript
 * import { DbContext } from '@tezos-dappetizer/database';
 * import { indexEntrypoint } from '@tezos-dappetizer/decorators';
 * import { TransactionIndexingContext } from '@tezos-dappetizer/indexer';
 *
 * class YourContractIndexer {
 *     @indexEntrypoint('foo_entrypoint')
 *     indexFoo(
 *         parameter: StronglyTypedFooParameter,
 *         dbContext: DbContext,
 *         indexingContext: TransactionIndexingContext<TContextData, TContractData>,
 *     ): void | PromiseLike<void> {
 *         // Your indexing code.
 *     }
 * }
 * ```
 * @remark If you want to index all entrypoints with a single method, then see {@link indexTransaction} decorator.
 * @remark This is based on {@link "@tezos-dappetizer/indexer".ContractIndexer.indexTransaction | ContractIndexer.indexTransaction(...)}.
 */
export function indexEntrypoint(entrypoint: string): MethodDecorator {
    argGuard.nonWhiteSpaceString(entrypoint, 'entrypoint');

    return createDecorator(indexEntrypoint, entrypoint);
}
