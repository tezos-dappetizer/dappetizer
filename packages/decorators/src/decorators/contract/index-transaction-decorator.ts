import { createDecorator } from '../../helpers/decorator-accessors';

/**
 * The decorator for a method of a contract indexer to index all its transactions regardless of entrypoint.
 * @example Demonstrates the method signature:
 * ```typescript
 * import { DbContext } from '@tezos-dappetizer/database';
 * import { indexTransaction } from '@tezos-dappetizer/decorators';
 * import { TransactionIndexingContext } from '@tezos-dappetizer/indexer';
 *
 * class YourContractIndexer {
 *     @indexTransaction()
 *     indexMyContractTransaction(
 *         // You may want a union with narrowing on the entrypoint.
 *         parameter: { entrypoint: string; value: unknown },
 *         dbContext: DbContext,
 *         indexingContext: TransactionIndexingContext<TContextData, TContractData>,
 *     ): void | PromiseLike<void> {
 *         // Your indexing code.
 *     }
 * }
 * ```
 * @remark If you want a method to index only a specific entrypoint, then see {@link indexEntrypoint} decorator.
 * @remark This is based on {@link "@tezos-dappetizer/indexer".ContractIndexer.indexTransaction | ContractIndexer.indexTransaction(...)}.
 */
export function indexTransaction(): MethodDecorator {
    return createDecorator(indexTransaction);
}
