/**
 * Options for matching the big map to index its diffs.
 * @property `path` Matches big map by its full {@link "@tezos-dappetizer/indexer".BigMapInfo.path} based on Michelson `annots`.
 *      This is more exact than `name`.
 * @property `name` Matches big map by its {@link "@tezos-dappetizer/indexer".BigMapInfo.name} based on Michelson `annots`.
 */
export type IndexBigMapOptions =
    | {
        /**
         * Matches big map by its full {@link "@tezos-dappetizer/indexer".BigMapInfo.path} based on Michelson `annots`.
         * This is more exact than `name`.
         */
        readonly path: readonly string[];
        name?: undefined;
    }
    | {
        /** Matches big map by its {@link "@tezos-dappetizer/indexer".BigMapInfo.name} based on Michelson `annots`. */
        readonly name: string;
        path?: undefined;
    };
