import { ContractFilterOptions } from './contract-filter-options';
import { createDecorator } from '../../helpers/decorator-accessors';
import { validateSelectiveFilterOptions } from '../selective-filter-options-validator';

/**
 * The decorator for a contract indexer class to select a contract for indexing based on specified filter options.
 * @example Demonstrates the usage:
 * ```typescript
 * import { contractFilter } from '@tezos-dappetizer/decorators';
 *
 * @contractFilter({ name: 'TezosDomains' })
 * class YourContractIndexerByName {
 *     ...
 * }
 *
 * @contractFilter({ anyOfAddresses: ['KT191reDVKrLxU9rjTSxg53wRqj6zh8pnHgr'] })
 * class YourContractIndexerByAddress {
 *     ...
 * }
 * ```
 * @remark This is based on {@link "@tezos-dappetizer/indexer".ContractIndexer.shouldIndex | ContractIndexer.shouldIndex(...)}.
 */
export function contractFilter(options: ContractFilterOptions): ClassDecorator {
    const validatedOptions = validateSelectiveFilterOptions(options, contractFilter, 'KT1');
    return createDecorator(contractFilter, validatedOptions);
}
