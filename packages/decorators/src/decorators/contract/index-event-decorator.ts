import { argGuard } from '@tezos-dappetizer/utils';

import { createDecorator } from '../../helpers/decorator-accessors';

/**
 * The decorator for a method of a contract indexer to index its transactions with the specified entrypoint.
 * @example Demonstrates the method signature:
 * ```typescript
 * import { DbContext } from '@tezos-dappetizer/database';
 * import { indexEvent } from '@tezos-dappetizer/decorators';
 * import { EventIndexingContext } from '@tezos-dappetizer/indexer';
 *
 * class YourContractIndexer {
 *     @indexEvent('product_ordered')
 *     indexProductOrdered(
 *         payload: StronglyTypedProductOrderedPayload | null,
 *         dbContext: DbContext,
 *         indexingContext: EventIndexingContext<TContextData, TContractData>,
 *     ): void | PromiseLike<void> {
 *         // Your indexing code.
 *     }
 * }
 * ```
 * @remark This is based on {@link "@tezos-dappetizer/indexer".ContractIndexer.indexEvent | ContractIndexer.indexEvent(...)}.
 */
export function indexEvent(tag: string | null): MethodDecorator {
    if (tag !== null) {
        argGuard.nonWhiteSpaceString(tag, 'tag');
    }

    return createDecorator(indexEvent, tag);
}
