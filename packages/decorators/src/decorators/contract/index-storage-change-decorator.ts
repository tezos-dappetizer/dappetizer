import { createDecorator } from '../../helpers/decorator-accessors';

/**
 * The decorator for a method of a contract indexer to index changes of its storage.
 * @example Demonstrates the method signature:
 * ```typescript
 * import { DbContext } from '@tezos-dappetizer/database';
 * import { indexStorageChange } from '@tezos-dappetizer/decorators';
 * import { StorageChangeIndexingContext } from '@tezos-dappetizer/indexer';
 *
 * class YourContractIndexer {
 *     @indexStorageChange()
 *     indexMyContractStorageChange(
 *         newStorage: StronglyTypedChangedStorage,
 *         dbContext: DbContext,
 *         indexingContext: StorageChangeIndexingContext<TContextData, TContractData>,
 *     ): void | PromiseLike<void> {
 *         // Your indexing code.
 *     }
 * }
 * ```
 * @remark This is based on {@link "@tezos-dappetizer/indexer".ContractIndexer.indexStorageChange | ContractIndexer.indexStorageChange(...)}.
 */
export function indexStorageChange(): MethodDecorator {
    return createDecorator(indexStorageChange);
}
