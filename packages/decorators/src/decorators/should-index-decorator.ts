import { createDecorator } from '../helpers/decorator-accessors';

/**
 * The decorator for a method of a contract indexer or smart rollup indexer to dynamically select a contract or a smart rollup for indexing.
 * @example Demonstrates the method signature:
 * ```typescript
 * import { shouldIndex } from '@tezos-dappetizer/decorators';
 * import { LazyContract, SmartRollup } from '@tezos-dappetizer/indexer';
 *
 * class YourContractIndexer {
 *     @shouldIndex()
 *     shouldIndexMyContracts(
 *         lazyContract: LazyContract,
 *         dbContext: DbContext,
 *     ): TContractData | false | PromiseLike<TContractData | false> {
 *         // Your code to return false or create your context data for the contract.
 *         return true; // Just return true, if you do not need context data.
 *     }
 * }
 *
 * class YourSmartRollupIndexer {
 *     @shouldIndex()
 *     shouldIndexMySmartRollups(
 *         rollup: SmartRollup,
 *         dbContext: DbContext,
 *     ): TRolupData | false | PromiseLike<TRolupData | false> {
 *         // Your code to return false or create your context data for the smart rollup.
 *         return true; // Just return true, if you do not need context data.
 *     }
 * }
 * ```
 * @remark This is based on {@link "@tezos-dappetizer/indexer".ContractIndexer.shouldIndex | ContractIndexer.shouldIndex(...)}.
 */
export function shouldIndex(): MethodDecorator {
    return createDecorator(shouldIndex);
}
