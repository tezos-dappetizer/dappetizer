import { SmartRollupIndexer } from '@tezos-dappetizer/indexer';

import { IndexerWrapperFactory } from './indexer-wrappers/indexer-wrapper-factory';
import { SmartRollupIndexerWrapper } from './indexer-wrappers/smart-rollup/smart-rollup-indexer-wrapper';

/**
 * Creates {@link "@tezos-dappetizer/indexer".SmartRollupIndexer} from an arbitrary object with at least one of following decorators.
 * - {@link indexAddMessages | @indexAddMessages(...)}
 * - {@link indexCement | @indexCement(...)}
 * - {@link indexExecuteOutboxMessage | @indexExecuteOutboxMessage(...)}
 * - {@link indexOriginate | @indexOriginate(...)}
 * - {@link indexPublish | @indexPublish(...)}
 * - {@link indexRecoverBond | @indexRecoverBond(...)}
 * - {@link indexRefute | @indexRefute(...)}
 * - {@link indexTimeout | @indexTimeout(...)}
 * - {@link shouldIndex | @shouldIndex(...)}
 * - {@link smartRollupFilter | @smartRollupFilter(...)}
 *
 * Additionally, the passed object can have a `name` property returning a string that identifies the indexer (for example in error messages).
 * @typeParam TDbContext The type of database context. Usually {@link "@tezos-dappetizer/database".DbContext},
 *      if used with standard {@link @tezos-dappetizer/database}.
 * @typeParam TContextData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link "@tezos-dappetizer/indexer".IndexingCycleHandler.createContextData} for more details.
 * @typeParam TRollupData The type of your custom data associated with the smart rollup that flows in `indexingContext` through this indexer.
 *      See {@link "@tezos-dappetizer/indexer".SmartRollupIndexer.shouldIndex} method for more details.
 * @experimental
 */
export function createSmartRollupIndexerFromDecorators<TDbContext, TContextData = undefined, TRollupData = boolean>(
    decoratedIndexer: object,
): SmartRollupIndexer<TDbContext, TContextData, TRollupData> {
    const factory = new IndexerWrapperFactory(new SmartRollupIndexerWrapper<TDbContext, TContextData, TRollupData>());
    return factory.create(decoratedIndexer, 'smart rollup indexer');
}
