import { ContractIndexer } from '@tezos-dappetizer/indexer';

import { ContractIndexerWrapper } from './indexer-wrappers/contract/contract-indexer-wrapper';
import { IndexerWrapperFactory } from './indexer-wrappers/indexer-wrapper-factory';

/**
 * Creates {@link "@tezos-dappetizer/indexer".ContractIndexer} from an arbitrary object with at least one of following decorators.
 * - {@link contractFilter | @contractFilter(...)}
 * - {@link indexBigMapDiff | @indexBigMapDiff(...)}
 * - {@link indexBigMapUpdate | @indexBigMapUpdate(...)}
 * - {@link indexEntrypoint | @indexEntrypoint(...)}
 * - {@link indexEvent | @indexEvent(...)}
 * - {@link indexOrigination | @indexOrigination(...)}
 * - {@link indexStorageChange | @indexStorageChange(...)}
 * - {@link indexTransaction | @indexTransaction(...)}
 * - {@link shouldIndex | @shouldIndex(...)}
 *
 * Additionally, the passed object can have a `name` property returning a string that identifies the indexer (for example in error messages).
 * @typeParam TDbContext The type of database context. Usually {@link "@tezos-dappetizer/database".DbContext},
 *      if used with standard {@link @tezos-dappetizer/database}.
 * @typeParam TContextData The type of your custom data that flows in `indexingContext` through all indexers for a block.
 *      See {@link "@tezos-dappetizer/indexer".IndexingCycleHandler.createContextData} for more details.
 * @typeParam TContractData The type of your custom data associated with the contract that flows in `indexingContext` through this indexer.
 *      See {@link "@tezos-dappetizer/indexer".ContractIndexer.shouldIndex} method for more details.
 */
export function createContractIndexerFromDecorators<TDbContext, TContextData = undefined, TContractData = boolean>(
    decoratedIndexer: object,
): ContractIndexer<TDbContext, TContextData, TContractData> {
    const factory = new IndexerWrapperFactory(new ContractIndexerWrapper<TDbContext, TContextData, TContractData>());
    return factory.create(decoratedIndexer, 'contract indexer');
}
