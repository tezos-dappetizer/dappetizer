import { Writable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { DbContext } from '../../../../test-utilities/mocks';
import { LazyContract } from '../../../indexer/src/public-api';
import { SelectiveIndexingOptions } from '../../src/decorators/selective-filter-options-validator';
import { shouldIndex } from '../../src/decorators/should-index-decorator';
import { DecoratedMethod, DecoratedMethodDiscovery } from '../../src/helpers/decorated-method-discovery';
import { DecoratorAccessor } from '../../src/helpers/decorator-accessors';
import { SelectiveIndexingItem, ShouldIndex, ShouldIndexWrapper } from '../../src/indexer-wrappers/should-index-wrapper';

function testFilter(_o: SelectiveIndexingOptions): ClassDecorator {
    return null!;
}

describe(ShouldIndexWrapper.name, () => {
    let target: ShouldIndexWrapper;
    let decoratedMethodDiscovery: DecoratedMethodDiscovery;
    let decoratorAccessor: DecoratorAccessor;

    let decoratedIndexer: object;
    let decoratedMethod: DecoratedMethod<ShouldIndex<SelectiveIndexingItem, DbContext, string>>;
    let contract: Writable<LazyContract>;
    let db: DbContext;

    const act = () => target.wrapMethod(decoratedIndexer, testFilter);

    beforeEach(() => {
        target = new ShouldIndexWrapper(
            instance(decoratedMethodDiscovery = mock()),
            instance(decoratorAccessor = mock()),
        );

        contract = {
            address: 'KT191reDVKrLxU9rjTSxg53wRqj6zh8pnHgr',
            config: { name: 'TezosDomains' },
        } as LazyContract;
        db = 'mockedDb' as any;
        decoratedIndexer = { whatever: 123 };
        decoratedMethod = mock();
    });

    describe('filter decorator', () => {
        it.each<[string, SelectiveIndexingOptions, unknown]>([
            ['equal name', { name: 'TezosDomains' }, undefined],
            ['different name', { name: 'QuipuSwap' }, false],
            ['one of addresses', { anyOfAddresses: ['KT1KEa8z6vWXDJrVqtMrAeDVzsvxat3kHaCE', 'KT191reDVKrLxU9rjTSxg53wRqj6zh8pnHgr'] }, undefined],
            ['none of addresses', { anyOfAddresses: ['KT1KEa8z6vWXDJrVqtMrAeDVzsvxat3kHaCE'] }, false],
        ])(`should evalute decorator %s correctly`, async (_desc, options, expected) => {
            setupClassDecorator(options);

            await wrapAndExecute({ expected });
        });
    });

    describe(shouldIndex.name, () => {
        it('should delegate to decorated method', async () => {
            setupMethodDecorator('mockedCustomData');

            await wrapAndExecute({ expected: 'mockedCustomData' });
        });
    });

    describe('both decorators', () => {
        it('should delegate to decorated method if class decorator passed', async () => {
            setupMethodDecorator('mockedCustomData');
            setupClassDecorator({ name: 'TezosDomains' });

            await wrapAndExecute({ expected: 'mockedCustomData' });
        });

        it('should not pass to decorated method if class decorator not passed', async () => {
            setupMethodDecorator('mockedCustomData');
            setupClassDecorator({ name: 'QuipuSwap' });

            await wrapAndExecute({ expected: false });

            verify(decoratedMethod.execute(anything(), anything())).never();
        });
    });

    it('should not wrap if no decorated methods', () => {
        const method = act();

        expect(method).toBeUndefined();
    });

    async function wrapAndExecute(testCase: { expected: unknown }) {
        const method = act();

        const contractData = await method!(contract, db);
        expect(contractData).toBe(testCase.expected);
    }

    function setupClassDecorator(options: SelectiveIndexingOptions) {
        when(decoratorAccessor.getFromClass(testFilter, decoratedIndexer)).thenReturn({ options });
    }

    function setupMethodDecorator(contractData: string | false) {
        when(decoratedMethodDiscovery.getSingleMethod(decoratedIndexer, shouldIndex)).thenReturn(instance(decoratedMethod));
        when(decoratedMethod.execute(contract, db)).thenReturn(Promise.resolve(contractData));
    }
});
