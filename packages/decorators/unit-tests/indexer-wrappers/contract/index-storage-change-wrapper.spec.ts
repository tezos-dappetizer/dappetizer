import { instance, mock, verify } from 'ts-mockito';

import { DbContext, IndexingContext } from '../../../../../test-utilities/mocks';
import { StorageChange } from '../../../../indexer/src/public-api';
import { DecoratedMethod } from '../../../src/helpers/decorated-method-discovery';
import {
    IndexStorageChange,
    IndexStorageChangeWrapper,
} from '../../../src/indexer-wrappers/contract/index-storage-change-wrapper';
import { executeDecorated } from '../method-wrapper-base-helpers';

describe(IndexStorageChangeWrapper.name, () => {
    let target: IndexStorageChangeWrapper;
    let decoratedMethod: DecoratedMethod<IndexStorageChange<DbContext, IndexingContext>>;
    let db: DbContext;
    let indexingContext: IndexingContext;

    beforeEach(() => {
        target = new IndexStorageChangeWrapper();
        decoratedMethod = mock();
        db = 'mockedDb' as any;
        indexingContext = 'mockedIndexingContext' as any;
    });

    it('should index storage change', async () => {
        const storageChange = {
            newValue: {
                convert: () => 'convertedNewValue',
            },
        } as StorageChange;

        await executeDecorated(target, instance(decoratedMethod), storageChange, db, indexingContext);

        verify(decoratedMethod.execute('convertedNewValue', db, indexingContext)).once();
    });
});
