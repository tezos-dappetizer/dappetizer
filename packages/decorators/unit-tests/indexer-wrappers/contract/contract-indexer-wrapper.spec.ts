import { anything, instance, mock, verify, when } from 'ts-mockito';

import { ContextData, ContractData, DbContext } from '../../../../../test-utilities/mocks';
import { ContractIndexer } from '../../../../indexer/src/public-api';
import { contractFilter } from '../../../src/decorators/contract/contract-filter-decorator';
import { NoDecoratorsGuard } from '../../../src/helpers/no-decorators-guard';
import { ContractIndexerWrapper } from '../../../src/indexer-wrappers/contract/contract-indexer-wrapper';
import { IndexBigMapDiffWrapper } from '../../../src/indexer-wrappers/contract/index-big-map-diff-wrapper';
import { IndexBigMapUpdateWrapper } from '../../../src/indexer-wrappers/contract/index-big-map-update-wrapper';
import { IndexEventWrapper } from '../../../src/indexer-wrappers/contract/index-event-wrapper';
import { IndexOriginationWrapper } from '../../../src/indexer-wrappers/contract/index-origination-wrapper';
import { IndexStorageChangeWrapper } from '../../../src/indexer-wrappers/contract/index-storage-change-wrapper';
import {
    IndexTransactionCompositeWrapper,
} from '../../../src/indexer-wrappers/contract/index-transaction-composite-wrapper';
import { ShouldIndexWrapper } from '../../../src/indexer-wrappers/should-index-wrapper';

describe(ContractIndexerWrapper.name, () => {
    let target: ContractIndexerWrapper<DbContext, ContextData, ContractData>;
    let noDecoratorsGuard: NoDecoratorsGuard;
    let indexBigMapDiffWrapper: IndexBigMapDiffWrapper;
    let indexBigMapUpdateWrapper: IndexBigMapUpdateWrapper;
    let indexEventWrapper: IndexEventWrapper;
    let indexOriginationWrapper: IndexOriginationWrapper;
    let indexStorageChangeWrapper: IndexStorageChangeWrapper;
    let indexTransactionWrapper: IndexTransactionCompositeWrapper;
    let shouldIndexWrapper: ShouldIndexWrapper;
    let indexer: object;

    const act = () => target.wrapMethods(indexer);

    beforeEach(() => {
        target = new ContractIndexerWrapper(
            instance(noDecoratorsGuard = mock()),
            instance(indexBigMapDiffWrapper = mock()),
            instance(indexBigMapUpdateWrapper = mock()),
            instance(indexEventWrapper = mock()),
            instance(indexOriginationWrapper = mock()),
            instance(indexStorageChangeWrapper = mock()),
            instance(indexTransactionWrapper = mock()),
            instance(shouldIndexWrapper = mock()),
        );
        indexer = 'decoratedIndexer' as any;
    });

    it('should delegate methods', () => {
        type ResultIndexer = ContractIndexer<DbContext, ContextData, ContractData>;

        const indexBigMapDiff: ResultIndexer['indexBigMapDiff'] = 'mockedIndexBigMapDiff' as any;
        const indexBigMapUpdate: ResultIndexer['indexBigMapUpdate'] = 'mockedIndexBigMapUpdate' as any;
        const indexEvent: ResultIndexer['indexEvent'] = 'mockedIndexEvent' as any;
        const indexOrigination: ResultIndexer['indexOrigination'] = 'mockedIndexOrigination' as any;
        const indexStorageChange: ResultIndexer['indexStorageChange'] = 'mockedIndexStorage' as any;
        const indexTransaction: ResultIndexer['indexTransaction'] = 'mockedIndexTransaction' as any;
        const shouldIndex: ResultIndexer['shouldIndex'] = 'mockedIndexShouldIndex' as any;

        when(indexBigMapDiffWrapper.wrapMethod<any, any>(indexer)).thenReturn(indexBigMapDiff);
        when(indexBigMapUpdateWrapper.wrapMethod<any, any>(indexer)).thenReturn(indexBigMapUpdate);
        when(indexEventWrapper.wrapMethod<any, any>(indexer)).thenReturn(indexEvent);
        when(indexOriginationWrapper.wrapMethod<any, any>(indexer)).thenReturn(indexOrigination);
        when(indexStorageChangeWrapper.wrapMethod<any, any>(indexer)).thenReturn(indexStorageChange);
        when(indexTransactionWrapper.wrapMethod(indexer)).thenReturn(indexTransaction);
        when(shouldIndexWrapper.wrapMethod<any, any, any>(indexer, contractFilter)).thenReturn(shouldIndex);

        const wrappedIndexer = act();

        expect(wrappedIndexer).toEqual<ResultIndexer>({
            indexBigMapDiff,
            indexBigMapUpdate,
            indexEvent,
            indexOrigination,
            indexStorageChange,
            indexTransaction,
            shouldIndex,
        });
        verify(noDecoratorsGuard.guardNotDecoratedWith(indexer, anything())).once();
    });
});
