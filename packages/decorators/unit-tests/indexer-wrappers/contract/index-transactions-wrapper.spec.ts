import { deepEqual, instance, mock, verify } from 'ts-mockito';

import { DbContext, IndexingContext } from '../../../../../test-utilities/mocks';
import { TransactionParameter } from '../../../../indexer/src/public-api';
import { DecoratedMethod } from '../../../src/helpers/decorated-method-discovery';
import {
    IndexAnyTransaction,
    IndexTransactionsWrapper,
} from '../../../src/indexer-wrappers/contract/index-transactions-wrapper';
import { executeDecorated } from '../method-wrapper-base-helpers';

describe(IndexTransactionsWrapper.name, () => {
    let target: IndexTransactionsWrapper;
    let decoratedMethod: DecoratedMethod<IndexAnyTransaction<DbContext, IndexingContext>>;
    let db: DbContext;
    let indexingContext: IndexingContext;

    beforeEach(() => {
        target = new IndexTransactionsWrapper();
        decoratedMethod = mock();
        db = 'mockedDb' as any;
        indexingContext = 'mockedIndexingContext' as any;
    });

    it('should index storage change', async () => {
        const transactionParameter = {
            entrypoint: 'eee',
            value: { convert: () => 'convertedValue' },
        } as TransactionParameter;

        await executeDecorated(target, instance(decoratedMethod), transactionParameter, db, indexingContext);

        verify(decoratedMethod.execute(deepEqual({ entrypoint: 'eee', value: 'convertedValue' }), db, indexingContext)).once();
    });
});
