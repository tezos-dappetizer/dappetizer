import { Writable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { DbContext, IndexingContext } from '../../../../../test-utilities/mocks';
import { ContractEvent } from '../../../../indexer/src/public-api';
import { DecoratedMethod } from '../../../src/helpers/decorated-method-discovery';
import { DecoratorOptions, IndexEvent, IndexEventWrapper } from '../../../src/indexer-wrappers/contract/index-event-wrapper';
import { executeDecorated } from '../method-wrapper-base-helpers';

describe(IndexEventWrapper.name, () => {
    let target: IndexEventWrapper;
    let decoratedMethod: DecoratedMethod<IndexEvent<DbContext, IndexingContext>, DecoratorOptions>;
    let db: DbContext;
    let indexingContext: IndexingContext;
    let event: Writable<ContractEvent>;

    const act = async () => executeDecorated(target, instance(decoratedMethod), event, db, indexingContext);

    beforeEach(() => {
        target = new IndexEventWrapper();
        decoratedMethod = mock();
        db = 'mockedDb' as any;
        indexingContext = 'mockedIndexingContext' as any;
        event = { payload: { convert: () => 'convertedPayload' } } as ContractEvent;
    });

    it.each([
        ['same', 'ttt', 'ttt'],
        ['nulls', null, null],
    ])('should index event if decorator and event tags are %s', async (_desc, decoratorTag, eventTag) => {
        setupTags(decoratorTag, eventTag);

        await act();

        verify(decoratedMethod.execute('convertedPayload', db, indexingContext)).once();
    });

    it.each([
        ['different', 'xxx', 'yyy'],
        ['null and string', null, 'ttt'],
        ['string and null', 'ttt', null],
    ])('should NOT index event if decorator and event tags are %s', async (_desc, decoratorTag, eventTag) => {
        setupTags(decoratorTag, eventTag);

        await act();

        verify(decoratedMethod.execute(anything(), anything(), anything())).never();
    });

    it('should pass null if no payload', async () => {
        setupTags('ttt', 'ttt');
        event.payload = null;

        await act();

        verify(decoratedMethod.execute(null, db, indexingContext)).once();
    });

    function setupTags(decoratorTag: string | null, eventTag: string | null) {
        when(decoratedMethod.decoratorOptions).thenReturn(decoratorTag);
        event.tag = eventTag;
    }
});
