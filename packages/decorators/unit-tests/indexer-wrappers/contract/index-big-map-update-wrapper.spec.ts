import { Writable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { DbContext, IndexingContext } from '../../../../../test-utilities/mocks';
import { BigMapUpdate } from '../../../../indexer/src/public-api';
import { asReadonly } from '../../../../utils/src/public-api';
import { DecoratedMethod } from '../../../src/helpers/decorated-method-discovery';
import {
    DecoratorOptions,
    IndexBigMapUpdate,
    IndexBigMapUpdateWrapper,
} from '../../../src/indexer-wrappers/contract/index-big-map-update-wrapper';
import { executeDecorated } from '../method-wrapper-base-helpers';

describe(IndexBigMapUpdateWrapper.name, () => {
    let target: IndexBigMapUpdateWrapper;
    let decoratedMethod: DecoratedMethod<IndexBigMapUpdate<DbContext, IndexingContext>, DecoratorOptions>;
    let db: DbContext;
    let indexingContext: IndexingContext;
    let update: Writable<BigMapUpdate>;

    const act = async () => executeDecorated(target, instance(decoratedMethod), update, db, indexingContext);

    beforeEach(() => {
        target = new IndexBigMapUpdateWrapper();
        decoratedMethod = mock();
        db = 'mockedDb' as any;
        indexingContext = 'mockedIndexingContext' as any;

        update = {
            bigMap: {
                name: 'nnn',
                path: asReadonly(['p1', 'p2']),
            },
            key: { convert: () => 'convertedKey' },
            value: { convert: () => 'convertedValue' },
        } as BigMapUpdate;
    });

    it.each<[string, DecoratorOptions]>([
        ['name', { name: 'nnn' }],
        ['path', { path: ['p1', 'p2'] }],
    ])('should index big map update matched by %s', async (_desc, decoratorOptions) => {
        when(decoratedMethod.decoratorOptions).thenReturn(decoratorOptions);

        await act();

        verify(decoratedMethod.execute('convertedKey', 'convertedValue', db, indexingContext)).once();
    });

    it.each<[string, DecoratorOptions]>([
        ['name', { name: 'wtf' }],
        ['path (same prefix)', { path: ['p1', 'wtf'] }],
        ['path (same suffix)', { path: ['wtf', 'p2'] }],
        ['path (same as name)', { path: ['nnn'] }],
    ])('should NOT index big map diff if NOT matched by %s', async (_desc, decoratorOptions) => {
        when(decoratedMethod.decoratorOptions).thenReturn(decoratorOptions);

        await act();

        verify(decoratedMethod.execute(anything(), anything(), anything(), anything())).never();
    });

    it('should handle no value', async () => {
        when(decoratedMethod.decoratorOptions).thenReturn({ name: 'nnn' });
        update.value = null;

        await act();

        verify(decoratedMethod.execute('convertedKey', null, db, indexingContext)).once();
    });
});
