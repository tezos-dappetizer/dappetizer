import { Writable } from 'ts-essentials';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { DbContext, IndexingContext } from '../../../../../test-utilities/mocks';
import { TransactionParameter } from '../../../../indexer/src/public-api';
import { DecoratedMethod } from '../../../src/helpers/decorated-method-discovery';
import {
    DecoratorOptions,
    IndexEntrypointWrapper,
    IndexTransaction,
} from '../../../src/indexer-wrappers/contract/index-entrypoint-wrapper';
import { executeDecorated } from '../method-wrapper-base-helpers';

describe(IndexEntrypointWrapper.name, () => {
    let target: IndexEntrypointWrapper;
    let decoratedMethod: DecoratedMethod<IndexTransaction<DbContext, IndexingContext>, DecoratorOptions>;
    let db: DbContext;
    let indexingContext: IndexingContext;
    let transactionParameter: Writable<TransactionParameter>;

    const act = async () => executeDecorated(target, instance(decoratedMethod), transactionParameter, db, indexingContext);

    beforeEach(() => {
        target = new IndexEntrypointWrapper();
        decoratedMethod = mock();
        db = 'mockedDb' as any;
        indexingContext = 'mockedIndexingContext' as any;
        transactionParameter = {
            entrypoint: 'eee',
            value: { convert: () => 'convertedValue' },
        } as TransactionParameter;
    });

    it('should index event if entrypoint matched', async () => {
        when(decoratedMethod.decoratorOptions).thenReturn('eee');

        await act();

        verify(decoratedMethod.execute('convertedValue', db, indexingContext)).once();
    });

    it('should NOT index event if entrypoint NOT matched', async () => {
        when(decoratedMethod.decoratorOptions).thenReturn('xxx');

        await act();

        verify(decoratedMethod.execute(anything(), anything(), anything())).never();
    });
});
