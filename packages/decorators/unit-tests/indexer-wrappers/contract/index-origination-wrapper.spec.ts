import { instance, mock, verify } from 'ts-mockito';

import { DbContext, IndexingContext } from '../../../../../test-utilities/mocks';
import { ContractOrigination } from '../../../../indexer/src/public-api';
import { DecoratedMethod } from '../../../src/helpers/decorated-method-discovery';
import { IndexOrigination, IndexOriginationWrapper } from '../../../src/indexer-wrappers/contract/index-origination-wrapper';
import { executeDecorated } from '../method-wrapper-base-helpers';

describe(IndexOriginationWrapper.name, () => {
    let target: IndexOriginationWrapper;
    let decoratedMethod: DecoratedMethod<IndexOrigination<DbContext, IndexingContext>>;
    let db: DbContext;
    let indexingContext: IndexingContext;

    beforeEach(() => {
        target = new IndexOriginationWrapper();
        decoratedMethod = mock();
        db = 'mockedDb' as any;
        indexingContext = 'mockedIndexingContext' as any;
    });

    it('should index storage change', async () => {
        const origination = {
            result: {
                getInitialStorage: async () => Promise.resolve({
                    convert: () => 'convertedStorage',
                }),
            },
        } as ContractOrigination;

        await executeDecorated(target, instance(decoratedMethod), origination, db, indexingContext);

        verify(decoratedMethod.execute('convertedStorage', db, indexingContext)).once();
    });
});
