import { anything, instance, mock, verify, when } from 'ts-mockito';

import { ContextData, DbContext, RollupData } from '../../../../../test-utilities/mocks';
import { SmartRollupIndexer } from '../../../../indexer/src/public-api';
import { smartRollupFilter } from '../../../src/decorators/smart-rollup/smart-rollup-filter-decorator';
import { NoDecoratorsGuard } from '../../../src/helpers/no-decorators-guard';
import { ShouldIndexWrapper } from '../../../src/indexer-wrappers/should-index-wrapper';
import {
    IndexAddMessagesWrapper,
    IndexCementWrapper,
    IndexExecuteOutboxMessageWrapper,
    IndexOriginateWrapper,
    IndexPublishWrapper,
    IndexRecoverBondWrapper,
    IndexRefuteWrapper,
    IndexTimeoutWrapper,
} from '../../../src/indexer-wrappers/smart-rollup/index-operation-wrappers';
import { SmartRollupIndexerWrapper } from '../../../src/indexer-wrappers/smart-rollup/smart-rollup-indexer-wrapper';

describe(SmartRollupIndexerWrapper.name, () => {
    let target: SmartRollupIndexerWrapper<DbContext, ContextData, RollupData>;
    let noDecoratorsGuard: NoDecoratorsGuard;
    let indexAddMessagesWrapper: IndexAddMessagesWrapper;
    let indexCementWrapper: IndexCementWrapper;
    let indexExecuteOutboxMessageWrapper: IndexExecuteOutboxMessageWrapper;
    let indexOriginateWrapper: IndexOriginateWrapper;
    let indexPublishWrapper: IndexPublishWrapper;
    let indexRecoverBondWrapper: IndexRecoverBondWrapper;
    let indexRefuteWrapper: IndexRefuteWrapper;
    let indexTimeoutWrapper: IndexTimeoutWrapper;
    let shouldIndexWrapper: ShouldIndexWrapper;
    let indexer: object;

    const act = () => target.wrapMethods(indexer);

    beforeEach(() => {
        target = new SmartRollupIndexerWrapper(
            instance(noDecoratorsGuard = mock()),
            instance(indexAddMessagesWrapper = mock()),
            instance(indexCementWrapper = mock()),
            instance(indexExecuteOutboxMessageWrapper = mock()),
            instance(indexOriginateWrapper = mock()),
            instance(indexPublishWrapper = mock()),
            instance(indexRecoverBondWrapper = mock()),
            instance(indexRefuteWrapper = mock()),
            instance(indexTimeoutWrapper = mock()),
            instance(shouldIndexWrapper = mock()),
        );
        indexer = 'decoratedIndexer' as any;
    });

    it('should delegate methods', () => {
        type ResultIndexer = SmartRollupIndexer<DbContext, ContextData, RollupData>;

        const indexAddMessages: ResultIndexer['indexAddMessages'] = 'mockedIndexAddMessages' as any;
        const indexCement: ResultIndexer['indexCement'] = 'mockedIndexCement' as any;
        const indexExecuteOutboxMessage: ResultIndexer['indexExecuteOutboxMessage'] = 'mockedIndexExecuteOutboxMessage' as any;
        const indexOriginate: ResultIndexer['indexOriginate'] = 'mockedIndexOriginate' as any;
        const indexPublish: ResultIndexer['indexPublish'] = 'mockedIndexPublish' as any;
        const indexRecoverBond: ResultIndexer['indexRecoverBond'] = 'mockedIndexRecoverBond' as any;
        const indexRefute: ResultIndexer['indexRefute'] = 'mockedIndexRefute' as any;
        const indexTimeout: ResultIndexer['indexTimeout'] = 'mockedIndexTimeout' as any;
        const shouldIndex: ResultIndexer['shouldIndex'] = 'mockedIndexShouldIndex' as any;

        when(indexAddMessagesWrapper.wrapMethod<any, any>(indexer)).thenReturn(indexAddMessages);
        when(indexCementWrapper.wrapMethod<any, any>(indexer)).thenReturn(indexCement);
        when(indexExecuteOutboxMessageWrapper.wrapMethod<any, any>(indexer)).thenReturn(indexExecuteOutboxMessage);
        when(indexOriginateWrapper.wrapMethod<any, any>(indexer)).thenReturn(indexOriginate);
        when(indexPublishWrapper.wrapMethod<any, any>(indexer)).thenReturn(indexPublish);
        when(indexRecoverBondWrapper.wrapMethod<any, any>(indexer)).thenReturn(indexRecoverBond);
        when(indexRefuteWrapper.wrapMethod<any, any>(indexer)).thenReturn(indexRefute);
        when(indexTimeoutWrapper.wrapMethod<any, any>(indexer)).thenReturn(indexTimeout);
        when(shouldIndexWrapper.wrapMethod<any, any, any>(indexer, smartRollupFilter)).thenReturn(shouldIndex);

        const wrappedIndexer = act();

        expect(wrappedIndexer).toEqual<ResultIndexer>({
            indexAddMessages,
            indexCement,
            indexExecuteOutboxMessage,
            indexOriginate,
            indexPublish,
            indexRecoverBond,
            indexRefute,
            indexTimeout,
            shouldIndex,
        });
        verify(noDecoratorsGuard.guardNotDecoratedWith(indexer, anything())).once();
    });
});
