import { instance, mock, verify } from 'ts-mockito';

import { DbContext, IndexingContext } from '../../../../../test-utilities/mocks';
import { Applied, SmartRollupOperation } from '../../../../indexer/src/rpc-data/rpc-data-interfaces';
import { DecoratedMethod } from '../../../src/helpers/decorated-method-discovery';
import {
    IndexOperation,
    IndexOperationWrapperBase,
} from '../../../src/indexer-wrappers/smart-rollup/index-operation-wrapper-base';
import { executeDecorated } from '../method-wrapper-base-helpers';

class TestWrapper extends IndexOperationWrapperBase<SmartRollupOperation> {
    protected override decorator = undefined!;
}

describe(IndexOperationWrapperBase.name, () => {
    let target: TestWrapper;
    let decoratedMethod: DecoratedMethod<IndexOperation<SmartRollupOperation, DbContext, IndexingContext>>;
    let db: DbContext;
    let indexingContext: IndexingContext;
    let operation: Applied<SmartRollupOperation>;

    const act = async () => executeDecorated(target, instance(decoratedMethod), operation, db, indexingContext);

    beforeEach(() => {
        target = new TestWrapper();
        decoratedMethod = mock();
        db = 'mockedDb' as any;
        indexingContext = 'mockedIndexingContext' as any;
        operation = 'mockedOp' as any;
    });

    it('should execute decorated method', async () => {
        await act();

        verify(decoratedMethod.execute(operation, db, indexingContext)).once();
    });
});
