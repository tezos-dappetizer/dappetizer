import { shouldIndex } from '../../src/decorators/should-index-decorator';

describe(shouldIndex.name, () => {
    it('should pass if correctly declared', () => {
        shouldIndex();
    });
});
