import { indexExecuteOutboxMessage } from '../../../src/decorators/smart-rollup/index-execute-outbox-message-decorator';

describe(indexExecuteOutboxMessage.name, () => {
    it('should pass if correctly declared', () => {
        indexExecuteOutboxMessage();
    });
});
