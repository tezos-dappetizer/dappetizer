import { indexOriginate } from '../../../src/decorators/smart-rollup/index-originate-decorator';

describe(indexOriginate.name, () => {
    it('should pass if correctly declared', () => {
        indexOriginate();
    });
});
