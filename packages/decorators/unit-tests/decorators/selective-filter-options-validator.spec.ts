import { expectToThrow } from '../../../../test-utilities/mocks';
import { ArgError } from '../../../utils/src/public-api';
import {
    SelectiveIndexingOptions,
    validateSelectiveFilterOptions,
} from '../../src/decorators/selective-filter-options-validator';

function indexFoo() {}

describe(validateSelectiveFilterOptions.name, () => {
    const act = (o: SelectiveIndexingOptions) => validateSelectiveFilterOptions(o, indexFoo, 'tz1');

    it.each<[string, SelectiveIndexingOptions]>([
        ['with name', { name: 'TezosDomains' }],
        ['with anyOfAddresses', { anyOfAddresses: ['tz1VQnqCCqX4K5sP3FNkVSNKTdCAMJDd3E1n'] }],
    ])('should pass if valid options with %s', (_desc, options) => {
        const validated = act(options);

        expect(validated).toEqual(options);
    });

    it.each([
        ['@indexFoo.options', 'undefined', undefined],
        ['@indexFoo.options', 'null', null],
        ['@indexFoo.options', 'not object', 'wtf'],
        ['@indexFoo.options', 'empty', {}],
        ['@indexFoo.options.name', 'null', { name: null }],
        ['@indexFoo.options.name', 'white-space', { name: '  ' }],
        ['@indexFoo.options.name', 'not string', { name: 123 }],
        ['@indexFoo.options.anyOfAddresses', 'null', { anyOfAddresses: null }],
        ['@indexFoo.options.anyOfAddresses', 'not array', { anyOfAddresses: 'wtf' }],
        ['@indexFoo.options.anyOfAddresses', 'is empty', { anyOfAddresses: [] }],
        ['@indexFoo.options.anyOfAddresses[0]', 'contains null', { anyOfAddresses: [null] }],
        ['@indexFoo.options.anyOfAddresses[0]', 'contains non-string', { anyOfAddresses: [123] }],
        ['@indexFoo.options.anyOfAddresses[0]', 'contains white-space string', { anyOfAddresses: ['  '] }],
        ['@indexFoo.options.anyOfAddresses[0]', 'contains non-address', { anyOfAddresses: ['wtf'] }],
    ])('should throw if %s is %s', (expectedArgName, _desc, options: any) => {
        const error = expectToThrow(() => act(options), ArgError);

        expect(error.argName).toBe(expectedArgName);
    });
});
