import { expectToThrow } from '../../../../../test-utilities/mocks';
import { contractFilter } from '../../../src/decorators/contract/contract-filter-decorator';

describe(contractFilter.name, () => {
    it('should pass if correctly declared', () => {
        contractFilter({ name: 'test' });
    });

    it('should throw if incorrectly declared', () => {
        expectToThrow(() => contractFilter('wtf' as any));
    });
});
