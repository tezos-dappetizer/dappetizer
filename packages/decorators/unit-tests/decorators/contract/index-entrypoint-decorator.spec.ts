import { ArgError } from '@tezos-dappetizer/utils';

import { expectToThrow } from '../../../../../test-utilities/mocks';
import { indexEntrypoint } from '../../../src/decorators/contract/index-entrypoint-decorator';

describe(indexEntrypoint.name, () => {
    it('should pass if correct entrypoint', () => {
        indexEntrypoint('lol');
    });

    it.each([null, undefined, '', '  \t', 123])('should throw if entrypoint is %s', entrypoint => {
        expectToThrow(() => indexEntrypoint(entrypoint as string), ArgError);
    });
});
