import { expectToThrow } from '../../../../../test-utilities/mocks';
import { indexBigMapDiff } from '../../../src/decorators/contract/index-big-map-diff-decorator';

describe(indexBigMapDiff.name, () => {
    it('should pass if correctly declared with options', () => {
        indexBigMapDiff({ name: 'lol' });
    });

    it('should pass if correctly declared without options', () => {
        indexBigMapDiff();
    });

    it('should throw if incorrectly declared', () => {
        expectToThrow(() => indexBigMapDiff('wtf' as any));
    });
});
