import { describeMember } from '../../../../test-utilities/mocks';
import { createDecorator, DecoratorAccessor } from '../../src/helpers/decorator-accessors';

function optionsDecorator(options: string): MethodDecorator {
    return createDecorator(optionsDecorator, options);
}

function flagDecorator(): MethodDecorator {
    return createDecorator(flagDecorator);
}

function classDecorator(options: string): ClassDecorator {
    return createDecorator(classDecorator, options);
}

@classDecorator('classFoo')
class Foo {
    @optionsDecorator('foo')
    optionsMethod(): void {}

    @flagDecorator()
    flaggedMethod(): void {}

    undecoratedMethod(): void {}
}

class UnannotatedClass {}

describe(DecoratorAccessor.name, () => {
    const target = new DecoratorAccessor();

    describeMember<typeof target>('getFromMethod', () => {
        describe('decorator with options', () => {
            it('should get options if decorated', () => {
                const decorator = target.getFromMethod(optionsDecorator, new Foo(), 'optionsMethod');

                expect(decorator).toEqual<typeof decorator>({ options: 'foo' });
            });

            it('should get null if not decorated', () => {
                expect(target.getFromMethod(optionsDecorator, new Foo(), 'flaggedMethod')).toBeNull();
                expect(target.getFromMethod(optionsDecorator, new Foo(), 'undecoratedMethod')).toBeNull();
            });
        });

        describe('flag decorator', () => {
            it('should get non-nullish if decorated', () => {
                const decorator = target.getFromMethod(flagDecorator, new Foo(), 'flaggedMethod');

                expect(decorator).toEqual<typeof decorator>({ options: undefined });
            });

            it('should get null if not decorated', () => {
                expect(target.getFromMethod(flagDecorator, new Foo(), 'optionsMethod')).toBeNull();
                expect(target.getFromMethod(flagDecorator, new Foo(), 'undecoratedMethod')).toBeNull();
            });
        });
    });

    describeMember<typeof target>('getFromClass', () => {
        it('should get options if decorated', () => {
            const decorator = target.getFromClass(classDecorator, new Foo());

            expect(decorator).toEqual<typeof decorator>({ options: 'classFoo' });
        });

        it('should get null if not decorated', () => {
            expect(target.getFromClass(classDecorator, new UnannotatedClass())).toBeNull();
        });
    });
});
