import { anything, instance, mock, spy, verify, when } from 'ts-mockito';

import { describeMember, expectToThrow } from '../../../../test-utilities/mocks';
import { DecoratedMethod, DecoratedMethodDiscovery } from '../../src/helpers/decorated-method-discovery';
import { DecoratorAccessor } from '../../src/helpers/decorator-accessors';

describe(DecoratedMethodDiscovery.name, () => {
    let target: DecoratedMethodDiscovery;
    let accessor: DecoratorAccessor;

    beforeEach(() => {
        target = new DecoratedMethodDiscovery(instance(accessor = mock()));
    });

    function testDecorator(_value: string): MethodDecorator {
        throw new Error('Should be mocked.');
    }

    describeMember<typeof target>('getMethods', () => {
        let indexer: TestIndexer;
        let indexerSpy: TestIndexer;

        const act = () => target.getMethods(indexer, testDecorator);

        beforeEach(() => {
            indexer = new TestIndexer();
            indexerSpy = spy(indexer);

            when(accessor.getFromMethod(testDecorator, indexer, 'fooMethod')).thenReturn({ options: 'fooDecorator' });
            when(accessor.getFromMethod(testDecorator, indexer, 'barMethod')).thenReturn({ options: 'barDecorator' });
        });

        class TestIndexer {
            fooMethod(_arg: string): void {}
            barMethod(): void {}
            wtfMethod(): void {}
        }

        it('should list all decorated methods', () => {
            const methods = act();

            expect(methods).toMatchObject<Partial<DecoratedMethod<Function, string>>[]>([
                { name: 'barMethod', decoratorOptions: 'barDecorator' },
                { name: 'fooMethod', decoratorOptions: 'fooDecorator' },
            ]);

            verify(indexerSpy.fooMethod(anything())).never();
            verify(indexerSpy.barMethod()).never();
            verify(indexerSpy.wtfMethod()).never();
        });

        it('should delegate methods correctly', () => {
            const methods = act();
            methods[1]!.execute('arg');

            verify(indexerSpy.fooMethod('arg')).once();
        });

        it('should not list if not function', () => {
            indexer.fooMethod = undefined as any;
            indexer.barMethod = 'wtf' as any;

            const methods = act();

            expect(methods).toBeEmpty();
        });

        it('should throw if not object', () => {
            indexer = 'wtf' as any;

            expectToThrow(act);
        });
    });

    describeMember<typeof target>('getSingleMethod', () => {
        let targetSpy: DecoratedMethodDiscovery;
        let indexer: object;

        const act = () => target.getSingleMethod(indexer, testDecorator);

        beforeEach(() => {
            targetSpy = spy(target);
            indexer = { whatever: 123 };
        });

        it('should return decorated method if only one exists', () => {
            const discoveredMethod: DecoratedMethod<Function, string> = 'mockedMethod' as any;
            when(targetSpy.getMethods(indexer, testDecorator)).thenReturn([discoveredMethod]);

            const method = act();

            expect(method).toBe(discoveredMethod);
        });

        it('should return null if no decorated method exists', () => {
            when(targetSpy.getMethods(indexer, testDecorator)).thenReturn([]);

            const method = act();

            expect(method).toBeNull();
        });

        it('should throw if more decorated methods exists', () => {
            when(targetSpy.getMethods(indexer, testDecorator)).thenReturn([
                { name: 'first' } as DecoratedMethod<Function, string>,
                { name: 'second' } as DecoratedMethod<Function, string>,
            ]);

            const error = expectToThrow(act);

            expect(error.message).toIncludeMultiple([
                '@testDecorator(...) decorator',
                'first(...) vs. second(...)',
            ]);
        });
    });
});
