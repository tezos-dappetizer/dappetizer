import { anything, instance, mock, verify, when } from 'ts-mockito';

import { expectToThrow } from '../../../../test-utilities/mocks';
import { DecoratedMethodDiscovery } from '../../src/helpers/decorated-method-discovery';
import { DecoratorAccessor } from '../../src/helpers/decorator-accessors';
import { ClassDecoratorFactory, MethodDecoratorFactory } from '../../src/helpers/decorator-factories';
import { NoDecoratorsGuard } from '../../src/helpers/no-decorators-guard';

describe(NoDecoratorsGuard.name, () => {
    let target: NoDecoratorsGuard;
    let decoratedMethodDiscovery: DecoratedMethodDiscovery;
    let decoratorAccessor: DecoratorAccessor;

    let decoratedIndexer: object;
    let methodDecorator1: MethodDecoratorFactory;
    let methodDecorator2: MethodDecoratorFactory;
    let classDecorator1: ClassDecoratorFactory;
    let classDecorator2: ClassDecoratorFactory;

    const act = () => target.guardNotDecoratedWith(decoratedIndexer, {
        conflictingTypeDescription: 'test indexer',
        methodDecorators: [methodDecorator1, methodDecorator2],
        classDecorators: [classDecorator1, classDecorator2],
    });

    beforeEach(() => {
        target = new NoDecoratorsGuard(
            instance(decoratedMethodDiscovery = mock()),
            instance(decoratorAccessor = mock()),
        );

        decoratedIndexer = 'mockedDecoratedIndexer' as any;
        methodDecorator1 = { name: 'mockedMethodDecorator1' } as any;
        methodDecorator2 = { name: 'mockedMethodDecorator2' } as any;
        classDecorator1 = { name: 'mockedClassDecorator1' } as any;
        classDecorator2 = { name: 'mockedClassDecorator2' } as any;

        when(decoratedMethodDiscovery.getMethods(decoratedIndexer, anything())).thenReturn([]);
    });

    it('should pass if no decorators', () => {
        act();

        verify(decoratedMethodDiscovery.getMethods(decoratedIndexer, methodDecorator1)).once();
        verify(decoratedMethodDiscovery.getMethods(decoratedIndexer, methodDecorator2)).once();
        verify(decoratedMethodDiscovery.getMethods(anything(), anything())).times(2);
        verify(decoratorAccessor.getFromClass(classDecorator1, decoratedIndexer)).once();
        verify(decoratorAccessor.getFromClass(classDecorator2, decoratedIndexer)).once();
        verify(decoratorAccessor.getFromClass(anything(), anything())).times(2);
    });

    it('should throw if decorated method', () => {
        when(decoratedMethodDiscovery.getMethods(decoratedIndexer, methodDecorator1)).thenReturn([
            { name: 'mockedMethod1' } as any,
            { name: 'mockedMethod2' } as any,
        ]);

        const error = expectToThrow(act);

        expect(error.message).toIncludeMultiple([
            `methods ['mockedMethod1', 'mockedMethod2']`,
            `@mockedMethodDecorator1(...)`,
            `used only on test indexer`,
        ]);
    });

    it('should throw if decorated method', () => {
        when(decoratorAccessor.getFromClass(classDecorator1, decoratedIndexer)).thenReturn('mockedWrapper' as any);

        const error = expectToThrow(act);

        expect(error.message).toIncludeMultiple([
            `indexer class`,
            `@mockedClassDecorator1(...)`,
            `used only on test indexer`,
        ]);
    });
});
