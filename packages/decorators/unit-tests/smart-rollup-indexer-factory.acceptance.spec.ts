import {
    createSmartRollupIndexerFromDecorators,
    indexAddMessages,
    indexCement,
    indexExecuteOutboxMessage,
    indexOriginate,
    indexPublish,
    indexRecoverBond,
    indexRefute,
    indexTimeout,
    shouldIndex,
    smartRollupFilter,
} from '@tezos-dappetizer/decorators';
import {
    Applied,
    OperationWithResultIndexingContext,
    SmartRollup,
    SmartRollupIndexer,
    SmartRollupIndexingContext,
    SmartRollupOperation,
} from '@tezos-dappetizer/indexer';
import { entriesOf } from '@tezos-dappetizer/utils';
import { DeepWritable, StrictExclude } from 'ts-essentials';

import { describeMemberFactory } from '../../../test-utilities/mocks';

@smartRollupFilter({ anyOfAddresses: ['sr1THC2rKtEkE5NcnZoSAh43vA8ZfgJUjcHs', 'sr1R23ax3Gj8NDQFbQRfNnzuKEZhth5qvWVP'] })
class MyIndexer {
    readonly name = 'MyExplicitName';

    constructor(private readonly executed: string[]) {}

    @shouldIndex()
    async myShouldIndex(rollup: SmartRollup, db: string): Promise<false | string> {
        return Promise.resolve(`rollupData[${rollup.address}, ${db}]`);
    }

    @indexAddMessages()
    myIndexAddMessages(op: Applied<SmartRollupOperation>, db: string, ctx: OperationWithResultIndexingContext<string>): void {
        this.executed.push(`indexAddMessages(${op.sourceAddress}, ${db}, ${ctx.data})`);
    }

    @indexCement()
    myIndexCement(op: Applied<SmartRollupOperation>, db: string, ctx: SmartRollupIndexingContext<string>): void {
        this.executed.push(`indexCement(${op.sourceAddress}, ${db}, ${ctx.data})`);
    }

    @indexExecuteOutboxMessage()
    myIndexExecuteOutboxMessage(op: Applied<SmartRollupOperation>, db: string, ctx: SmartRollupIndexingContext<string>): void {
        this.executed.push(`indexExecuteOutboxMessage(${op.sourceAddress}, ${db}, ${ctx.data})`);
    }

    @indexOriginate()
    myIndexOriginate(op: Applied<SmartRollupOperation>, db: string, ctx: SmartRollupIndexingContext<string>): void {
        this.executed.push(`indexOriginate(${op.sourceAddress}, ${db}, ${ctx.data})`);
    }

    @indexPublish()
    myIndexPublish(op: Applied<SmartRollupOperation>, db: string, ctx: SmartRollupIndexingContext<string>): void {
        this.executed.push(`indexPublish(${op.sourceAddress}, ${db}, ${ctx.data})`);
    }

    @indexRecoverBond()
    myIndexRecoverBond(op: Applied<SmartRollupOperation>, db: string, ctx: SmartRollupIndexingContext<string>): void {
        this.executed.push(`indexRecoverBond(${op.sourceAddress}, ${db}, ${ctx.data})`);
    }

    @indexRefute()
    myIndexRefute(op: Applied<SmartRollupOperation>, db: string, ctx: SmartRollupIndexingContext<string>): void {
        this.executed.push(`indexRefute(${op.sourceAddress}, ${db}, ${ctx.data})`);
    }

    @indexTimeout()
    myIndexTimeout(op: Applied<SmartRollupOperation>, db: string, ctx: SmartRollupIndexingContext<string>): void {
        this.executed.push(`indexTimeout(${op.sourceAddress}, ${db}, ${ctx.data})`);
    }
}

describe('SmartRollupIndexer acceptance tests', () => {
    let wrappedIndexer: SmartRollupIndexer<string, string, string>;
    let executed: string[];
    let context: any;

    beforeEach(() => {
        executed = [];
        const decoratedIndexer = new MyIndexer(executed);
        wrappedIndexer = createSmartRollupIndexerFromDecorators(decoratedIndexer);

        context = { data: 'myCtx' } as OperationWithResultIndexingContext<string>;
    });

    const describeMember = describeMemberFactory<typeof wrappedIndexer>();

    describeMember('name', () => {
        it(`should be read from 'name' property`, () => {
            expect(wrappedIndexer.name).toBe('MyExplicitName');
        });
    });

    describeMember('shouldIndex', () => {
        let rollup: DeepWritable<SmartRollup>;

        const act = async () => wrappedIndexer.shouldIndex!(rollup, 'myDb');

        beforeEach(() => {
            rollup = {
                address: 'sr1R23ax3Gj8NDQFbQRfNnzuKEZhth5qvWVP',
                config: { name: 'MyRollup' },
            } as SmartRollup;
        });

        it('should delegate to decorated method correctly', async () => {
            const data = await act();

            expect(data).toBe('rollupData[sr1R23ax3Gj8NDQFbQRfNnzuKEZhth5qvWVP, myDb]');
        });

        it('should not delegate if class filter not passed', async () => {
            rollup.address = 'sr1H6pEjm9nLpvXfuYWGH2Ja29PdA2884YTh';

            const data = await act();

            expect(data).toBeFalse();
        });
    });

    const methodsToTest: Record<StrictExclude<keyof typeof wrappedIndexer, 'name' | 'shouldIndex'>, string> = {
        indexAddMessages: 'indexAddMessages(tz1SrcAddr, myDb, myCtx)',
        indexCement: 'indexCement(tz1SrcAddr, myDb, myCtx)',
        indexExecuteOutboxMessage: 'indexExecuteOutboxMessage(tz1SrcAddr, myDb, myCtx)',
        indexOriginate: 'indexOriginate(tz1SrcAddr, myDb, myCtx)',
        indexPublish: 'indexPublish(tz1SrcAddr, myDb, myCtx)',
        indexRecoverBond: 'indexRecoverBond(tz1SrcAddr, myDb, myCtx)',
        indexRefute: 'indexRefute(tz1SrcAddr, myDb, myCtx)',
        indexTimeout: 'indexTimeout(tz1SrcAddr, myDb, myCtx)',
    };
    describe.each(entriesOf(methodsToTest))('%s', (methodName, expectedExecuted) => {
        it('should delegate to decorated method correctly', async () => {
            const op = { sourceAddress: 'tz1SrcAddr' } as Applied<SmartRollupOperation>;

            await wrappedIndexer[methodName]!(op as any, 'myDb', context);

            expect(executed).toEqual([expectedExecuted]);
        });
    });
});
