export class CommandBuilder {
    commandParts: string[] = [];
    readonly options: Record<string, string> = {};

    setCommand(...parts: readonly string[]): this {
        this.commandParts = [...parts];
        return this;
    }

    setOption(name: string, value: string): this {
        this.options[name] = value;
        return this;
    }

    build(): string {
        return [
            ...this.commandParts.map(escape),
            ...Object.entries(this.options).map(o => `--${o[0]} ${escape(o[1])}`),
        ].join(' ');
    }
}

// Inspired by: https://github.com/xxorax/node-shell-escape/blob/master/shell-escape.js
function escape(value: string): string {
    if (/[^A-Za-z0-9_/:=-]/u.test(value)) {
        return `'${value.replace(/'/gu, `'\\''`)}'`
            .replace(/^(?:'')+/gu, '') // Unduplicate single-quote at the beginning.
            .replace(/\\'''/gu, `\\'`); // Remove non-escaped single-quote if there are enclosed between 2 escaped.
    }
    return value;
}
