import { getCurrentEnvironmentConfig, getNetworkConfig, getSandboxConfig } from '@taqueria/node-sdk';
import { hasLength } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { quotedOption, TaqueriaConsoleArgs } from './taqueria-console-args';

export interface TezosNodeDetails {
    readonly url: string;
    readonly network: string;
}

@singleton()
export class TezosNodeResolver {
    resolve(args: TaqueriaConsoleArgs): TezosNodeDetails {
        // Firstly: specified by user.
        if (args.network) {
            return resolveDetails(args.network, 'network', args, `you specified in ${quotedOption('network')} option`);
        }
        if (args.sandbox) {
            return resolveDetails(args.sandbox, 'sandbox', args, `you specified in ${quotedOption('sandbox')} option`);
        }

        // Lastly: from Taqueria config.
        const envConfig = getCurrentEnvironmentConfig(args);
        if (!envConfig) {
            throw new Error(`There is no environment called '${args.env}' in your Taqueria config.`);
        }

        if (hasLength(envConfig.networks, 1) && !envConfig.sandboxes.length) {
            return resolveDetails(envConfig.networks[0], 'network', args, `comes from your Taqueria config for environment '${args.env}'`);
        }
        if (hasLength(envConfig.sandboxes, 1) && !envConfig.networks.length) {
            return resolveDetails(envConfig.sandboxes[0], 'sandbox', args, `comes from your Taqueria config for environment '${args.env}'`);
        }
        throw new Error(`The current environment '${args.env}' in your Taqueria config can have only one target to execute it automatically,`
            + ` but there are networks [${envConfig.networks.join(', ')}] and sanboxes [${envConfig.sandboxes.join(', ')}].`
            + ` So specify ${quotedOption('network')} or ${quotedOption('sandbox')} option with a single value explicitly.`);
    }
}

function resolveDetails(
    name: string,
    type: 'network' | 'sandbox',
    args: TaqueriaConsoleArgs,
    sourceDescription: string,
): TezosNodeDetails {
    const accessor = type === 'network' ? getNetworkConfig : getSandboxConfig;
    const config = accessor(args)(name);

    if (!config) {
        throw new Error(`Your Taqeuria config does not contain ${type} '${name}' which ${sourceDescription}.`);
    }
    return {
        url: config.rpcUrl,
        network: name,
    };
}
