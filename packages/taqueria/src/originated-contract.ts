export interface OriginatedContract {
    readonly address: string;
    readonly name: string;
}
