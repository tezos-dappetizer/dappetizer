import { errorToString, FILE_SYSTEM_DI_TOKEN, FileSystem, isWhiteSpace, typeValidator } from '@tezos-dappetizer/utils';
import path from 'path';
import { inject, singleton } from 'tsyringe';

const CONFIG_REL_PATH = './.taq/config.json';
const DEFAULT_DIR = 'indexer';

@singleton()
export class IndexerDirResolver {
    constructor(
        @inject(FILE_SYSTEM_DI_TOKEN) private readonly fileSystem: FileSystem,
    ) {}

    async resolvePath(projectDirPath: string): Promise<string> {
        const configPath = path.resolve(projectDirPath, CONFIG_REL_PATH);
        const config = await this.readJson(configPath) ?? {};

        if (isWhiteSpace(config.indexerDir)) {
            config.indexerDir = DEFAULT_DIR;
            const configText = JSON.stringify(config, undefined, 4);
            await this.fileSystem.writeFileText(configPath, configText);
        }
        return path.resolve(projectDirPath, config.indexerDir);
    }

    private async readJson(configFile: string): Promise<ConfigJson | null> {
        try {
            const text = await this.fileSystem.readFileTextIfExists(configFile);
            if (isWhiteSpace(text)) {
                return null;
            }

            const config = JSON.parse(text) as ConfigJson;
            typeValidator.validate(config, 'object');
            return config;
        } catch (error) {
            throw new Error(`Failed reading '${configFile}'. ${errorToString(error)}`);
        }
    }
}

interface ConfigJson {
    indexerDir?: string;
}
