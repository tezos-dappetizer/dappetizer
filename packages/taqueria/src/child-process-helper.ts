import { injectValue, isNullish } from '@tezos-dappetizer/utils';
import nodeChildProcess, { ExecOptions } from 'child_process';
import { singleton } from 'tsyringe';

@singleton()
export class ChildProcessHelper {
    constructor(
        @injectValue(nodeChildProcess) private readonly childProcess: typeof nodeChildProcess,
        @injectValue(global.process) private readonly process: NodeJS.Process,
    ) {}

    async exec(command: string, options: ExecOptions = {}): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            const child = this.childProcess.exec(command, options);

            child.stdout?.pipe(this.process.stdout);
            child.stderr?.pipe(this.process.stderr);

            child.on('exit', code => {
                if (isNullish(code) || code === 0) {
                    resolve();
                } else {
                    reject(new Error(`Received exit code ${code} from command: ${command}`));
                }
            });
        });
    }
}
