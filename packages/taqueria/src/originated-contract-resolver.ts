import { getCurrentEnvironmentConfig } from '@taqueria/node-sdk';
import { addressValidator, dumpToString, isNonEmpty, isObjectLiteral, isWhiteSpace } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { OriginatedContract } from './originated-contract';
import { quotedOption, TaqueriaConsoleArgs } from './taqueria-console-args';

const TAQ_SOURCE = `according to environment aliases in file '.taq/config.json'`;

@singleton()
export class OriginatedContractResolver {
    resolve(args: TaqueriaConsoleArgs): OriginatedContract {
        if (!isWhiteSpace(args.contractAddress) || !isWhiteSpace(args.contractName)) {
            if (isWhiteSpace(args.contractName)) {
                throw new Error(`If ${quotedOption('contractAddress')} is specified, then ${quotedOption('contractName')} must be specified too.`);
            }
            if (isWhiteSpace(args.contractAddress)) {
                throw new Error(`If ${quotedOption('contractName')} is specified, then ${quotedOption('contractAddress')} must be specified too.`);
            }
            return {
                address: args.contractAddress,
                name: args.contractName,
            };
        }

        const contracts = Array.from(this.resolveFromTaqueriaConfig(args));

        if (!isNonEmpty(contracts)) {
            throw new Error(`No contract was recently originated (${TAQ_SOURCE})`
                + ` nor any contract was explicitly specified in the console`
                + ` (${quotedOption('contractAddress')} and ${quotedOption('contractName')} options).`
                + ` So it does not make sense to generate any indexer.`);
        }
        if (contracts.length > 1) {
            throw new Error(`Currently only one contract is supported but multiple contracts were originated recently (${TAQ_SOURCE}).`
                + ` Specify only a single contract using ${quotedOption('contractAddress')} and ${quotedOption('contractName')} console options.`
                + ` Originated contracts: ${dumpToString(contracts.map(c => ({ ...c })))}`);
        }
        return contracts[0];
    }

    private *resolveFromTaqueriaConfig(args: TaqueriaConsoleArgs): Iterable<OriginatedContract> {
        const envConfig = getCurrentEnvironmentConfig(args);
        for (const [name, alias] of Object.entries(envConfig?.aliases ?? [])) {
            if (isContractAlias(alias)) {
                addressValidator.validate(alias.address, ['KT1']);
                yield { address: alias.address, name };
            }
        }
    }
}

function isContractAlias(value: unknown): value is { address: string } {
    return isObjectLiteral(value) && typeof value.address === 'string';
}
