import { ProxyTaskArgs } from '@taqueria/node-sdk/types';
import { kebabCase } from 'lodash';

export interface TaqueriaConsoleArgs extends ProxyTaskArgs.t {
    readonly contractAddress?: string;
    readonly contractName?: string;
    readonly sandbox?: string;
    readonly network?: string;
}

export function optionFlag(name: keyof TaqueriaConsoleArgs): string {
    return kebabCase(name);
}

export function quotedOption(name: keyof TaqueriaConsoleArgs): string {
    return `'--${optionFlag(name)}'`;
}

export const taqueriaCommands = Object.freeze({
    BUILD_INDEXER: 'build-indexer',
    CREATE_INDEXER: 'indexer',
    START_INDEXER: 'start-indexer',
    UPDATE_INDEXER: 'update-indexer',
});
