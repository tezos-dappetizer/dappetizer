import { CommandBuilder } from '../src/command-builder';

describe(CommandBuilder.name, () => {
    let target: CommandBuilder;

    beforeEach(() => {
        target = new CommandBuilder();
    });

    it('should build correctly', () => {
        target.setCommand('npx', 'dappetizer', 'init')
            .setOption('address', 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5')
            .setOption('name', 'Foo "Bar');

        const cmd = target.build();

        expect(cmd).toBe(`npx dappetizer init --address KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5 --name 'Foo "Bar'`);
    });
});
