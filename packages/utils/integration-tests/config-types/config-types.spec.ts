import { registerDappetizerUtils, registerExplicitConfigFilePath } from '@tezos-dappetizer/utils';
import { container as globalContainer } from 'tsyringe';

import { TimeConfig } from '../../src/time/time-config';

describe('config types', () => {
    it.each(['.json', '.js', '.ts'])('should load config with %s extension', ext => {
        const diContainer = globalContainer.createChildContainer();
        registerDappetizerUtils(diContainer);
        registerExplicitConfigFilePath(diContainer, { useValue: `${__dirname}/foo-config${ext}` });

        const timeConfig = diContainer.resolve(TimeConfig);

        expect(timeConfig.longExecutionWarningMillis).toBe(123);
    });
});
