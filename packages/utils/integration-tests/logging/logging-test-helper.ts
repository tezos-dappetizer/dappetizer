import { DappetizerConfig } from '@tezos-dappetizer/indexer';
import {
    ConfigObjectElement,
    registerDappetizerUtils,
    resolveLogger,
    ROOT_CONFIG_ELEMENT_DI_TOKEN,
    ROOT_LOGGER_DI_TOKEN,
} from '@tezos-dappetizer/utils';
import { container as globalContainer, DependencyContainer } from 'tsyringe';

import { deleteFileOrDir, readFileText } from '../../../../test-utilities/mocks';

// `describe.each()` fails b/c winston has some static state -> separate test files -> jest loads everything from scratch.
export function runLoggingTests(format: 'messages' | 'json') {
    describe(`logging with format ${format}`, () => {
        let diContainer: DependencyContainer;
        let loggedDir: string;

        beforeEach(() => {
            loggedDir = `${__dirname}/.tmp-logging-${format}`;
            deleteFileOrDir(loggedDir);

            diContainer = globalContainer.createChildContainer();
            registerDappetizerUtils(diContainer);

            const config: Pick<DappetizerConfig, 'logging'> = {
                logging: {
                    console: {
                        minLevel: 'off',
                    },
                    file: {
                        minLevel: 'warning',
                        format,
                        path: `${loggedDir}/test.log`,
                    },
                    globalData: 'ggg',
                },
            };
            diContainer.registerInstance(ROOT_CONFIG_ELEMENT_DI_TOKEN, new ConfigObjectElement(config, 'embedded', '$'));
        });

        it('should write log entries correctly', async () => {
            const logger = resolveLogger(diContainer, 'FooCategory');
            const rootLogger = diContainer.resolve(ROOT_LOGGER_DI_TOKEN);

            expect(logger.isCriticalEnabled).toBeTrue();
            expect(logger.isErrorEnabled).toBeTrue();
            expect(logger.isWarningEnabled).toBeTrue();
            expect(logger.isInformationEnabled).toBeFalse();
            expect(logger.isDebugEnabled).toBeFalse();
            expect(logger.isTraceEnabled).toBeFalse();

            logger.logDebug('Skipped');
            logger.logWarning('Hello {emoji}.', { emoji: 'LOL' });
            await rootLogger.close();

            const today = new Date().toISOString().substring(0, 10);
            const loggedText = readFileText(`${loggedDir}/test.${today}.log`);
            expect(loggedText).toMatch(/\w*/u);
            expect(loggedText).not.toInclude('Skipped');
            expect(loggedText).not.toInclude('Debug');

            switch (format) {
                case 'messages':
                    expect(loggedText).toInclude('warning [FooCategory] Hello emoji "LOL".');
                    break;
                case 'json':
                    expect(loggedText).toIncludeMultiple([
                        '"timestamp":"20',
                        '"category":"FooCategory"',
                        '"level":"warning"',
                        '"message":"Hello {emoji}."',
                        '"data":{"emoji":"LOL"}',
                        '"globalData":"ggg"',
                        '"packages":{"dappetizer":{"commitHash":',
                        '"formattedMessage":"Hello emoji \\"LOL\\"."',
                    ]);
                    break;
            }
        });
    });
}
