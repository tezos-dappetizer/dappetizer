import { singleton } from 'tsyringe';

import { ConfigDiagnosticsAggregator } from '../configuration/config-diagnostics-aggregator';
import { injectLogger, Logger } from '../logging/public-api';
import { PackageInfoResolver } from '../packages-versions/package-info-resolver';
import { versionInfo } from '../version';

export const UNKNOWN = '(unknown)';

@singleton()
export class AppStartupReporter {
    constructor(
        private readonly configAggregator: ConfigDiagnosticsAggregator,
        private readonly packageInfoResolver: PackageInfoResolver,
        @injectLogger('App') private readonly logger: Logger,
    ) {}

    async logOnAppStartup(indexerModuleIds: readonly string[]): Promise<void> {
        const dappetizerConfig = this.configAggregator.aggregateConfigs();
        const indexerModules = await Promise.all(indexerModuleIds.map(async id => this.resolveInfo(id)));

        this.logger.logInformation('Starting {dappetizerVersion} with {indexerModules} and {dappetizerConfig}.', {
            dappetizerVersion: versionInfo.VERSION,
            dappetizerConfig,
            indexerModules: Object.fromEntries(indexerModules),
        });
    }

    private async resolveInfo(indexerModuleId: string): Promise<[string, unknown]> {
        try {
            const info = await this.packageInfoResolver.resolveInfo(indexerModuleId);

            return [indexerModuleId, { // Do not log properties if no data.
                file: info.filePath,
                ...info.name ? { name: info.name } : null,
                ...info.version ? { version: info.version } : null,
            }];
        } catch (error) {
            this.logger.logWarning(`Using ${UNKNOWN} as info of {indexerModuleId} because of {error}`, { indexerModuleId, error });
            return [indexerModuleId, UNKNOWN];
        }
    }
}
