import { singleton } from 'tsyringe';

import { addAbortListener } from './abortion';

@singleton()
export class AbortControllerFactory {
    createLinked(...signals: readonly AbortSignal[]): AbortController {
        const controller = new AbortController();

        if (!signals.length) {
            return controller;
        }
        if (signals.some(s => s.aborted)) {
            controller.abort();
            return controller;
        }

        const unlinks: (() => void)[] = [];
        const result: AbortController = {
            signal: controller.signal,
            abort: (): void => {
                controller.abort();
                unlinks.forEach(u => u()); // Avoids memory leak if parent signal has longer lifetime.
            },
        };

        for (const signal of signals) {
            unlinks.push(addAbortListener(signal, () => result.abort()));
        }
        return result;
    }
}
