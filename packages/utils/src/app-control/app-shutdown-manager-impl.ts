import { DependencyContainer, inject, singleton } from 'tsyringe';

import { AppShutdownManager } from './app-shutdown-manager';
import { BackgroundWorkerExecutor } from './background-worker-executor';
import { GlobalErrorHandler } from './global-error-handler';
import { DI_CONTAINER_DI_TOKEN, injectValue } from '../dependency-injection/public-api';
import { injectLogger } from '../logging/inject-logger-decorator';
import { Logger } from '../logging/logger';
import { ROOT_LOGGER_DI_TOKEN, RootLogger } from '../logging/public-api';

@singleton()
export class AppShutdownManagerImpl implements AppShutdownManager {
    readonly controller = new AbortController();

    constructor(
        globalErrorHandler: GlobalErrorHandler,
        @injectValue(global.process) private readonly process: NodeJS.Process,
        @injectLogger(AppShutdownManagerImpl) private readonly logger: Logger,
        @inject(ROOT_LOGGER_DI_TOKEN) private readonly rootLogger: RootLogger,
        @inject(DI_CONTAINER_DI_TOKEN) private readonly diContainer: DependencyContainer,
    ) {
        globalErrorHandler.register(this);
    }

    get signal(): AbortSignal {
        return this.controller.signal;
    }

    async shutdown(options?: { exitCode: number }): Promise<void> {
        const exit = (): void => this.process.exit(options?.exitCode);

        setTimeout(exit, 5_000); // Just in case if cleanup is stuck.
        await this.cleanUp();
        exit();
    }

    private async cleanUp(): Promise<void> {
        try {
            this.controller.abort();

            // Resolve just-in-time to avoid circular reference, delay() also fails.
            await this.diContainer.resolve(BackgroundWorkerExecutor).stop();

            this.logger.logInformation('The app shuts down.');
            await this.rootLogger.close();
        } catch {
            // Nothing to do.
        }
    }
}
