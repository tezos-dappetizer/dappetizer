import { InjectionToken } from 'tsyringe';

export interface AppShutdownManager {
    readonly signal: AbortSignal;

    shutdown(options?: { exitCode: number }): Promise<void>;
}

export const APP_SHUTDOWN_MANAGER_DI_TOKEN: InjectionToken<AppShutdownManager> = 'Dappetizer:Utils:AppShutdownManager';
