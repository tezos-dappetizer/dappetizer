import { singleton } from 'tsyringe';

import { AppShutdownManager } from './app-shutdown-manager';
import { injectValue } from '../dependency-injection/public-api';
import { injectLogger, Logger } from '../logging/public-api';

@singleton()
export class GlobalErrorHandler {
    constructor(
        @injectValue(global.process) private readonly process: NodeJS.Process,
        @injectLogger(GlobalErrorHandler) private readonly logger: Logger,
    ) {}

    register(appShutdownManager: AppShutdownManager): void {
        const onAppSignal = (signal: NodeJS.Signals): void => {
            this.logger.logInformation('Received {signal} therefore the app will shut down.', { signal });
            void appShutdownManager.shutdown({ exitCode: 0 });
        };

        this.process.on('SIGINT', onAppSignal);
        this.process.on('SIGTERM', onAppSignal);

        const onUnhandledError = (error: unknown): void => {
            this.logger.logCritical('Received unhandled {error}. The app will shut down to avoid unpredicted behavior.', { error });
            void appShutdownManager.shutdown({ exitCode: 1 });
        };

        this.process.on('unhandledRejection', onUnhandledError);
        this.process.on('uncaughtException', onUnhandledError);
    }
}
