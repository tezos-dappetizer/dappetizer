import { InjectionToken } from 'tsyringe';

import { BackgroundWorker } from './background-worker';

// It should not be part of public API as far as it should be consumed only by Dappetizer.
export const BACKGROUND_WORKERS_DI_TOKEN: InjectionToken<BackgroundWorker> = 'Dappetizer:Utils:BackgroundWorkers';
