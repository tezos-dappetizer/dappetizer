import express from 'express';
import { Registry } from 'prom-client';
import { singleton } from 'tsyringe';

import { MetricsCollector } from './metrics-collector';
import { httpHeaders } from '../http-fetch/http-constants';
import { HttpHandler } from '../http-server/public-api';

/** Exposes Prometheus metrics as a web page at particular HTTP path. */
@singleton()
export class MetricsHttpHandler implements HttpHandler {
    readonly urlPath = '/metrics';

    constructor(
        private readonly prometheusRegistry: Registry,
        private readonly collector: MetricsCollector,
    ) { }

    async handle(_request: express.Request, response: express.Response): Promise<void> {
        const metrics = await this.collector.collectMetrics();

        response.setHeader(httpHeaders.CONTENT_TYPE, this.prometheusRegistry.contentType);
        response.send(metrics);
    }
}
