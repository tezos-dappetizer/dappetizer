import { Registry } from 'prom-client';
import { inject, injectAll, singleton } from 'tsyringe';

import { MetricsUpdater } from './metrics-updater';
import { METRICS_UPDATERS_DI_TOKEN } from './metrics-updater-di-token';
import { Clock, CLOCK_DI_TOKEN } from '../time/public-api';

const METRICS_UPDATE_MILLIS = 1_000;

/** Collects all up-to-date metrics. */
@singleton()
export class MetricsCollector {
    private lastUpdateTimestamp = 0;

    constructor(
        private readonly prometheusRegistry: Registry,
        @injectAll(METRICS_UPDATERS_DI_TOKEN) private readonly updaters: readonly MetricsUpdater[],
        @inject(CLOCK_DI_TOKEN) private readonly clock: Clock,
    ) {}

    async collectMetrics(): Promise<string> {
        if (this.lastUpdateTimestamp + METRICS_UPDATE_MILLIS < this.clock.getNowTimestamp()) {
            await Promise.all(this.updaters.map(u => u.update()));
            this.lastUpdateTimestamp = this.clock.getNowTimestamp();
        }

        return this.prometheusRegistry.metrics();
    }
}
