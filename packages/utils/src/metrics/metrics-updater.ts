import { AsyncOrSync } from 'ts-essentials';
import { DependencyContainer, Provider } from 'tsyringe';

import { METRICS_UPDATERS_DI_TOKEN } from './metrics-updater-di-token';
import { registerSingleton } from '../dependency-injection/public-api';

export interface MetricsUpdater {
    update(): AsyncOrSync<void>;
}

/**
 * Registers a metrics updater to be used for evaluation of Prometheus metrics available at `/metrics` endpoint.
 * It is registered as **singleton**.
 */
export function registerMetricsUpdater(diContainer: DependencyContainer, metricsUpdaterProvider: Provider<MetricsUpdater>): void {
    registerSingleton(diContainer, METRICS_UPDATERS_DI_TOKEN, metricsUpdaterProvider);
}
