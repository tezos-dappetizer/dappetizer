import client, { Registry } from 'prom-client';
import { DependencyContainer } from 'tsyringe';

import { MetricsHttpHandler } from './metrics-http-handler';
import { registerHttpHandler } from '../http-server/public-api';

export function registerMetrics(diContainer: DependencyContainer): void {
    // Inject to Dappetizer features:
    registerHttpHandler(diContainer, { useToken: MetricsHttpHandler });

    // Internal:
    diContainer.registerInstance(Registry, client.register);
}
