import { DependencyContainer } from 'tsyringe';

import { PACKAGE_JSON_LOADER_DI_TOKEN } from './package-json-loader';
import { PackageJsonLoaderImpl } from './package-json-loader-impl';
import { registerPublicApi } from '../dependency-injection/public-api';

export function registerPackagesJson(diContainer: DependencyContainer): void {
    // Public API:
    registerPublicApi(diContainer, PACKAGE_JSON_LOADER_DI_TOKEN, {
        useInternalToken: PackageJsonLoaderImpl,
        createProxy: internal => ({
            load: internal.load.bind(internal),
        }),
    });
}
