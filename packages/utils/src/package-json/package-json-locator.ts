import path from 'path';

import { argGuard } from '../basics/public-api';

export function locatePackageJsonInCurrentPackage(pathWithinPackage: string): string {
    argGuard.nonWhiteSpaceString(pathWithinPackage, 'pathWithinPackage');

    return locateInternal(pathWithinPackage, pathWithinPackage);
}

function locateInternal(pathWithinPackage: string, originalPath: string): string {
    const parts = path.parse(pathWithinPackage);

    if (!parts.base) {
        throw new Error(`Failed to locate package.json from a path within package: ${originalPath}`);
    }
    return parts.base === 'src' || parts.base === 'dist'
        ? path.resolve(parts.dir, 'package.json')
        : locateInternal(parts.dir, originalPath);
}
