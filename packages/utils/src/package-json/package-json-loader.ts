import { InjectionToken } from 'tsyringe';

export interface PackageJsonLoader {
    load(filePath: string): PackageJson;
}

export interface PackageJson {
    getVersion(): string;

    getDependencyVersion(dependencyName: string): string;

    getDevDependencyVersion(dependencyName: string): string;
}

export const PACKAGE_JSON_LOADER_DI_TOKEN: InjectionToken<PackageJsonLoader> = 'Dappetizer:Utils:PackageJsonLoader';
