import nodeFS from 'fs';
import path from 'path';
import { singleton } from 'tsyringe';

import { PackageJsonImpl } from './package-json-impl';
import { PackageJson, PackageJsonLoader } from './package-json-loader';
import { argGuard, errorToString, isObjectLiteral, stringValidator } from '../basics/public-api';
import { injectValue } from '../dependency-injection/public-api';

@singleton()
export class PackageJsonLoaderImpl implements PackageJsonLoader {
    constructor(
        @injectValue(nodeFS) private readonly fs: typeof nodeFS,
    ) {}

    load(filePath: string): PackageJson {
        argGuard.nonWhiteSpaceString(filePath, 'filePath');

        const absFilePath = path.resolve(filePath);
        let contents = '';

        try {
            contents = this.fs.readFileSync(absFilePath).toString();
            const json: unknown = JSON.parse(contents);

            if (!isObjectLiteral(json)) {
                throw new Error('The file content is not a JSON object.');
            }
            return new PackageJsonImpl(absFilePath, json, stringValidator);
        } catch (error) {
            throw new Error(`Failed to load file '${absFilePath}'. ${errorToString(error)} File contents: ${contents}`);
        }
    }
}
