import { JSONSchemaForNPMPackageJsonFiles } from '@schemastore/package';

import { PackageJson } from './package-json-loader';
import { errorToString, keyof, StringValidator } from '../basics/public-api';

export class PackageJsonImpl implements PackageJson {
    #stringValidator: StringValidator;

    constructor(
        readonly filePath: string,
        readonly json: JSONSchemaForNPMPackageJsonFiles,
        stringValidator: StringValidator,
    ) {
        Object.freeze(this);
        this.#stringValidator = stringValidator;
    }

    getVersion(): string {
        return this.getString(
            keyof<JSONSchemaForNPMPackageJsonFiles>('version'),
            () => this.json.version,
        );
    }

    getDependencyVersion(dependencyName: string): string {
        return this.getString(
            `${keyof<JSONSchemaForNPMPackageJsonFiles>('dependencies')}['${dependencyName}']`,
            () => this.json.dependencies?.[dependencyName],
        );
    }

    getDevDependencyVersion(dependencyName: string): string {
        return this.getString(
            `${keyof<JSONSchemaForNPMPackageJsonFiles>('devDependencies')}['${dependencyName}']`,
            () => this.json.devDependencies?.[dependencyName],
        );
    }

    private getString(valueInfo: string, getRawStr: () => string | undefined): string {
        try {
            const rawValue = getRawStr();
            return this.#stringValidator.validate(rawValue, 'NotWhiteSpace');
        } catch (error) {
            throw new Error(`Failed to get ${valueInfo} from file '${this.filePath}'.`
                + ` ${errorToString(error)} File contents: ${JSON.stringify(this.json)}`);
        }
    }
}
