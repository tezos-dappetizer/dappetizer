import { container as globalDIContainer, DependencyContainer } from 'tsyringe';

import { registerAppControl } from './app-control/dependency-injection';
import { registerConfiguration } from './configuration/dependency-injection';
import { registerDependencyInjection } from './dependency-injection/dependency-injection';
import { registerOnce } from './dependency-injection/registration-utils';
import { registerFileSystem } from './file-system/dependency-injection';
import { registerHealth } from './health/dependency-injection';
import { registerHttpFetch } from './http-fetch/dependency-injection';
import { registerHttpServer } from './http-server/dependency-injection';
import { registerLogging } from './logging/dependency-injection';
import { registerMetrics } from './metrics/dependency-injection';
import { registerPackagesJson } from './package-json/dependency-injection';
import { registerPackagesVersions } from './packages-versions/dependency-injection';
import { registerTezosRpc } from './tezos-rpc/dependency-injection';
import { registerTime } from './time/dependency-injection';
import { registerUsageStatistics } from './usage-statistics/dependency-injection';

export function registerDappetizerUtils(diContainer: DependencyContainer = globalDIContainer): void {
    registerOnce(diContainer, 'Dappetizer:Utils', () => {
        registerAppControl(diContainer);
        registerConfiguration(diContainer);
        registerDependencyInjection(diContainer);
        registerFileSystem(diContainer);
        registerHealth(diContainer);
        registerHttpFetch(diContainer);
        registerHttpServer(diContainer);
        registerLogging(diContainer);
        registerMetrics(diContainer);
        registerUsageStatistics(diContainer);
        registerPackagesJson(diContainer);
        registerPackagesVersions(diContainer);
        registerTezosRpc(diContainer);
        registerTime(diContainer);
        registerUsageStatistics(diContainer);
    });
}
