import { DependencyContainer } from 'tsyringe';

import { FILE_SYSTEM_DI_TOKEN } from './file-system';
import { FileSystemImpl } from './file-system-impl';
import { registerPublicApi } from '../dependency-injection/public-api';

export function registerFileSystem(diContainer: DependencyContainer): void {
    // Public API:
    registerPublicApi(diContainer, FILE_SYSTEM_DI_TOKEN, {
        useInternalToken: FileSystemImpl,
        createProxy: internal => ({
            existsAsFile: internal.existsAsFile.bind(internal),
            readFileTextIfExists: internal.readFileTextIfExists.bind(internal),
            writeFileText: internal.writeFileText.bind(internal),
        }),
    });
}
