import { argGuard, NonNullish } from '../basics/public-api';

/**
 * Fixed round-robin queue - after it's full it removes (overwrites) firsly added items.
 * Useful when you want to cache few last items.
 */
export class FixedQueue<T extends NonNullish> {
    private readonly wrappers: ({ readonly value: T } | undefined)[];
    private index = 0;

    constructor(size: number) {
        argGuard.number(size, 'size', { integer: true, min: 1 });

        this.wrappers = Array<{ value: T } | undefined>(size);
    }

    add(value: T): void {
        this.wrappers[this.index] = { value };
        this.index = this.index + 1 < this.wrappers.length ? this.index + 1 : 0;
    }

    has(value: T): boolean {
        return this.wrappers.some(w => w && w.value === value);
    }
}
