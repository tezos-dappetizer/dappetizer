import { NonNullish } from '../basics/public-api';

export function getOrCreate<TKey, TValue extends NonNullish>(
    map: Map<TKey, TValue>,
    key: TKey,
    factory: (key: TKey) => TValue,
): TValue {
    let value = map.get(key);

    if (value === undefined) {
        value = factory(key);
        map.set(key, value);
    }
    return value;
}
