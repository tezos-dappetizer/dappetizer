import { BigNumber } from 'bignumber.js';
import { mapValues } from 'lodash';

import { errorObjToString } from './conversion';
import { hasToJSON, hasConstructor } from './reflection-utils';

export function safeJsonStringify(value: unknown, options?: { indent?: string | number }): string {
    const sanitizedValue = sanitizeForJson(value);
    return JSON.stringify(sanitizedValue, undefined, options?.indent);
}

export function sanitizeForJson(value: unknown, stringSanitizer: StringSanitizer = emptyStringSanitizer): unknown {
    return new Sanitizer(stringSanitizer).sanitize(value, '');
}

export interface StringSanitizer {
    sanitizeString(str: string): string;
}

export const emptyStringSanitizer: StringSanitizer = Object.freeze<StringSanitizer>({
    sanitizeString: s => s,
});

class Sanitizer {
    readonly sanitizedObjsAtCurrentPath = new Set<object>();

    constructor(
        private readonly stringSanitizer: StringSanitizer,
    ) {}

    sanitize(value: unknown, parentIndex: string): unknown {
        switch (typeof value) {
            case 'undefined':
                return null;
            case 'string':
                return this.stringSanitizer.sanitizeString(value);
            case 'bigint':
                return value.toString();
            case 'object': {
                if (value === null || value instanceof Date) {
                    return value;
                }
                if (value instanceof Error) {
                    return errorObjToString(value);
                }
                if (hasConstructor(value, BigNumber)) {
                    return value.toFixed();
                }
                if (this.sanitizedObjsAtCurrentPath.has(value)) {
                    return '(circular reference)';
                }

                this.sanitizedObjsAtCurrentPath.add(value);
                try {
                    if (Array.isArray(value)) {
                        return value.map((item, index) => this.sanitize(item, index.toString()));
                    }
                    if (hasToJSON(value)) {
                        const jsonValue = value.toJSON(parentIndex);
                        return this.sanitize(jsonValue, parentIndex);
                    }

                    return mapValues(value, (propertyValue, propertyName) => this.sanitize(propertyValue, propertyName));
                } finally {
                    this.sanitizedObjsAtCurrentPath.delete(value);
                }
            }
            default:
                return value;
        }
    }
}
