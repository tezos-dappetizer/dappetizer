import { argGuard } from './arg-guard';

/** Helper for lazy initialization of a singleton value. */
export function cacheFuncResult<T>(factory: () => T): () => T {
    argGuard.function(factory, 'factory');

    let cached: { value: T } | null = null;

    return (): T => {
        if (!cached) {
            cached = { value: factory() };
            // eslint-disable-next-line no-param-reassign, @typescript-eslint/no-non-null-assertion
            factory = undefined!; // Allows garbage collection.
        }
        return cached.value;
    };
}
