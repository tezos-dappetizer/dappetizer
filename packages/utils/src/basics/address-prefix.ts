export type AddressPrefix =
    | 'tz'
    | 'tz1'
    | 'tz2'
    | 'tz3'
    | 'KT1'
    | 'txr1'
    | 'sr1';

/** @deprecated Simply use `tz` of `AddressPrefix`. */
export const tzAddressPrefixes = Object.freeze<AddressPrefix[]>(['tz1', 'tz2', 'tz3']);

/**
 * @deprecated Because of forward compatiblity, it is better NOT to restrict all possible prefixes.
 *      If you still need this array, feel free to add it in your code.
 */
export const allAddressPrefixes = Object.freeze<AddressPrefix[]>(['tz1', 'tz2', 'tz3', 'KT1', 'txr1', 'sr1']);
