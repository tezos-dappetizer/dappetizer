import { AsyncIterableProvider } from './async-iterable-provider';
import { FixedQueue } from '../../collections/fixed-queue';

/** Removes duplicates by ID when processing an `AsyncIterableIterator`. */
export class DeduplicateItemsProvider<TItem> implements AsyncIterableProvider<TItem> {
    private readonly lastIds: FixedQueue<string>;
    private readonly getItemId: (item: TItem) => string;

    constructor(
        options: {
            numberOfLastItemsToDeduplicate: number;
            getItemId: (item: TItem) => string;
        },
        private readonly nextProvider: AsyncIterableProvider<TItem>,
    ) {
        this.lastIds = new FixedQueue(options.numberOfLastItemsToDeduplicate);
        this.getItemId = options.getItemId;
        this.nextProvider = nextProvider;
    }

    async *iterate(): AsyncIterableIterator<TItem> {
        for await (const item of this.nextProvider.iterate()) {
            const itemId = this.getItemId(item);

            if (!this.lastIds.has(itemId)) {
                this.lastIds.add(itemId);
                yield item;
            }
        }
    }
}
