export interface AsyncIterableProvider<TItem> {
    iterate(): AsyncIterableIterator<TItem>;
}
