/** An error indicating that an argument passed to Dappetizer public API was invalid. */
export class ArgError extends Error {
    readonly argName: string;
    readonly specifiedValue: unknown;
    readonly details: string;

    constructor(options: {
        readonly argName: string;
        readonly specifiedValue: unknown;
        readonly details: string;
    }) {
        super(`Argument '${options.argName}' is invalid. ${options.details} The specified value: ${JSON.stringify(options.specifiedValue)}.`);

        this.argName = options.argName;
        this.specifiedValue = options.specifiedValue;
        this.details = options.details;

        Object.freeze(this);
    }
}
