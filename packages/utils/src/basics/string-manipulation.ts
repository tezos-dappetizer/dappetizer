import { camelCase } from 'lodash';

import { ArgError } from './arg-error';
import { argGuard } from './arg-guard';
import { Nullish } from './reflection-types';
import { ONLY_WHITE_SPACES_REGEX } from './validators/string-validator-impl';

export function equalsIgnoreCase(str1: string, str2: string): boolean {
    argGuard.string(str1, 'str1');
    argGuard.string(str2, 'str2');

    return str1.localeCompare(str2, undefined, { sensitivity: 'accent' }) === 0;
}

export function removeRequiredPrefix(str: string, prefixToRemove: string): string {
    argGuard.string(str, 'str');
    argGuard.string(prefixToRemove, 'prefixToRemove');

    if (!str.startsWith(prefixToRemove)) {
        throw new ArgError({
            argName: 'str',
            specifiedValue: str,
            details: `It does not start with prefix '${prefixToRemove}' which should be removed.`,
        });
    }
    return str.substring(prefixToRemove.length);
}

export function removeRequiredSuffix(str: string, suffixToRemove: string): string {
    argGuard.string(str, 'str');
    argGuard.string(suffixToRemove, 'suffixToRemove');

    if (!str.endsWith(suffixToRemove)) {
        throw new ArgError({
            argName: 'str',
            specifiedValue: str,
            details: `It does not end with suffix '${suffixToRemove}' which should be removed.`,
        });
    }
    return str.substring(0, str.length - suffixToRemove.length);
}

export function isWhiteSpace(str: Nullish<string>): str is null | undefined | '' {
    argGuard.ifNotNullish(str, s => argGuard.string(s, 'str'));

    return !str || ONLY_WHITE_SPACES_REGEX.test(str);
}

export function replaceAll(str: string, search: string, replacement: string): string {
    argGuard.string(str, 'str');
    argGuard.string(search, 'search');
    argGuard.string(replacement, 'replacement');

    return str.split(search).join(replacement);
}

export function pascalCase(str: string): string {
    argGuard.string(str, 'str');

    const camelStr = camelCase(str);
    return `${camelStr[0]?.toUpperCase() ?? ''}${camelStr.substring(1)}`;
}
