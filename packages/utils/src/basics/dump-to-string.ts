import { BigNumber } from 'bignumber.js';

import { ArgError } from './arg-error';

export type ValueToDump =
    | undefined
    | null
    | BigNumber
    | boolean
    | Date
    | number
    | string
    | readonly ValueToDump[]
    | RecordToDump;

export type RecordToDump = {
    readonly [key: string]: ValueToDump;
};

/** Produces debug friendly dump of the given value, not a valid JSON. */
export function dumpToString(value: ValueToDump): string {
    switch (typeof value) {
        case 'undefined':
            return 'undefined';
        case 'boolean':
        case 'number':
            return value.toString();
        case 'string':
            return `'${value}'`;
        case 'object': {
            if (value === null) {
                return 'null';
            }
            if (value instanceof Date) {
                return value.toISOString();
            }
            if (value instanceof BigNumber) {
                return value.toFixed(); // B/c toString() returns exponential format for large number.
            }

            if (Array.isArray(value)) {
                return `[${value.map(dumpToString).join(', ')}]`;
            }

            const dumpedProperties = Object.entries(value)
                .map(([propertyName, propertyValue]) => `${propertyName}: ${dumpToString(propertyValue)}`);
            return dumpedProperties.length > 0 ? `{ ${dumpedProperties.join(', ')} }` : '{}';
        }
        default:
            throw new ArgError({
                argName: 'value',
                specifiedValue: value,
                details: `The value type ${typeof value} is unsupported.`,
            });
    }
}
