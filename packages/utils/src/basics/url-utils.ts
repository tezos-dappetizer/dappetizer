import { trimEnd, trimStart } from 'lodash';

import { argGuard } from './arg-guard';

/** Resolves trailing/leading/missing slashes. */
export function appendUrl(baseUrl: string, relativeUrl: string): string {
    argGuard.nonWhiteSpaceString(baseUrl, 'baseUrl');
    argGuard.string(relativeUrl, 'relativeUrl');

    return `${trimEnd(baseUrl, '/')}/${trimStart(relativeUrl, '/')}`;
}
