import { OneOfSupportedValidator } from './one-of-supported-validator';
import { StringValidator } from './string-validator';
import { ArgError } from '../arg-error';
import { dumpToString } from '../dump-to-string';

export class OneOfSupportedValidatorImpl implements OneOfSupportedValidator {
    constructor(private readonly stringValidator: StringValidator) {}

    validate<TValue extends string>(rawValue: unknown, supportedValues: readonly TValue[]): TValue {
        guardSupportedValues(supportedValues);

        const value = this.stringValidator.validate(rawValue);

        for (const supportedValue of supportedValues) {
            if (value === supportedValue) {
                return supportedValue;
            }
        }
        throw new Error('It is NOT one of supported strings.');
    }

    getExpectedValueDescription(supportedValues: readonly string[]): string {
        guardSupportedValues(supportedValues);

        return `one of supported strings: ${dumpToString(supportedValues)}`;
    }
}

function guardSupportedValues(values: readonly string[]): void {
    if (!Array.isArray(values) || values.length === 0 || values.some(p => typeof p !== 'string')) {
        throw new ArgError({
            argName: 'supportedValues',
            specifiedValue: values,
            details: 'It must be a non-empty array of strings.',
        });
    }
}
