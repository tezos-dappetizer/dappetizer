import { StringValidatorImpl } from './string-validator-impl';
import { typeValidator } from './type-validator';
import { Validator } from './validator';
import { protectAsPublicApi } from './validator-utils';

export type StringLimits = 'NotEmpty' | 'NotWhiteSpace';

/** Low-level validation of a `string`. */
export interface StringValidator extends Validator<string, StringLimits | void> {}

/** Static instance of {@link StringValidator}. */
export const stringValidator: StringValidator = protectAsPublicApi(new StringValidatorImpl(typeValidator));
