import { TypeValidator } from './type-validator';
import { TypeOfName } from '../reflection-types';

export class TypeValidatorImpl implements TypeValidator {
    validate(value: unknown, expectedType: TypeOfName): any { // eslint-disable-line @typescript-eslint/no-explicit-any
        if (value === undefined) {
            if (expectedType === 'undefined') {
                return value;
            }
            throw new Error('It cannot be undefined.');
        }
        if (value === null) {
            throw new Error('It cannot be null.');
        }
        if (typeof value !== expectedType) {
            throw new Error(`It cannot be ${typeof value}.`);
        }
        return value;
    }

    getExpectedValueDescription(expectedType: TypeOfName): string {
        switch (expectedType) {
            case 'undefined':
                return 'undefined';
            case 'object':
                return 'an object';
            default:
                return `a ${expectedType}`;
        }
    }
}
