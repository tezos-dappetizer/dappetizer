import { ObjectValidator } from './object-validator';
import { TypeValidator } from './type-validator';
import { Constructor } from '../reflection-types';

export class ObjectValidatorImpl implements ObjectValidator {
    constructor(private readonly typeValidator: TypeValidator) {}

    validate<TObject extends object>(rawValue: unknown, constructor?: Constructor<TObject>): TObject {
        const value = this.typeValidator.validate(rawValue, 'object');

        if (constructor && !(value instanceof constructor)) {
            const constructorName = typeof value.constructor === 'function' && value.constructor.name ? value.constructor.name : Object.name;
            throw new Error(`It is an instance of '${constructorName}'.`);
        }
        return value as TObject;
    }

    getExpectedValueDescription(constructor?: Constructor): string {
        return constructor
            ? `an instance of '${constructor.name}'`
            : 'an object';
    }
}
