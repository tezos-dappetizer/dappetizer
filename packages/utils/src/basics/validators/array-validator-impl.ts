import { ArrayLimits, ArrayValidator } from './array-validator';
import { TypeValidator } from './type-validator';
import { ArgError } from '../arg-error';
import { dumpToString } from '../dump-to-string';

export class ArrayValidatorImpl implements ArrayValidator {
    constructor(private readonly typeValidator: TypeValidator) {}

    validate(value: unknown, limits?: ArrayLimits): readonly unknown[] {
        const obj = this.typeValidator.validate(value, 'object');

        if (!Array.isArray(obj)) {
            throw new Error(`It is not an array but ${(obj.constructor as Function | undefined)?.name ?? 'undefined'}.`);
        }
        switch (limits) {
            case undefined:
                break;
            case 'NotEmpty':
                if (obj.length === 0) {
                    throw new Error('Is cannot be an empty array.');
                }
                break;
            default:
                throwInvalidLimits(limits);
        }
        return obj;
    }

    getExpectedValueDescription(limits?: ArrayLimits): string {
        switch (limits) {
            case undefined:
                return 'an array';
            case 'NotEmpty':
                return 'a not-empty array';
            default:
                throwInvalidLimits(limits);
        }
    }
}

function throwInvalidLimits(limits?: ArrayLimits): never {
    const supportedLimits: (typeof limits)[] = [undefined, 'NotEmpty'];
    throw new ArgError({
        argName: 'limits',
        specifiedValue: limits,
        details: `It must be one of values: ${dumpToString(supportedLimits)}.`,
    });
}
