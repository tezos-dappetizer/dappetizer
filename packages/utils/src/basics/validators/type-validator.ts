import { TypeValidatorImpl } from './type-validator-impl';
import { Validator } from './validator';
import { protectAsPublicApi } from './validator-utils';
import { TypeOfName } from '../reflection-types';

/** Low-level validation of a value type. */
export interface TypeValidator extends Validator<unknown, TypeOfName> {
    validate(value: unknown, expectedType: 'bigint'): bigint;
    validate(value: unknown, expectedType: 'boolean'): boolean;
    validate(value: unknown, expectedType: 'function'): Function;
    validate(value: unknown, expectedType: 'number'): number;
    validate(value: unknown, expectedType: 'object'): object;
    validate(value: unknown, expectedType: 'string'): string;
    validate(value: unknown, expectedType: 'symbol'): symbol;
    validate(value: unknown, expectedType: 'undefined'): undefined;
    validate(value: unknown, expectedType: TypeOfName): unknown;
}

/** Static instance of {@link TypeValidator}. */
export const typeValidator: TypeValidator = protectAsPublicApi(new TypeValidatorImpl());
