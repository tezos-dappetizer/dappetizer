import { OneOfSupportedValidatorImpl } from './one-of-supported-validator-impl';
import { stringValidator } from './string-validator';
import { Validator } from './validator';
import { protectAsPublicApi } from './validator-utils';

/** Low-level validation of a `string` within supported `string`-s. The matching is case-insensitive. */
export interface OneOfSupportedValidator extends Validator<string, readonly string[]> {
    /**
     * Validates value to be one of the specified supported values. This can be easily used with an enum.
     * @return The specified value if it is valid.
     * @throws `Error` with explanatory message without actual value if it is invalid.
     */
    validate<TValue extends string>(value: unknown, supportedValues: readonly TValue[]): TValue;
}

/** Static instance of {@link OneOfSupportedValidator}. */
export const oneOfSupportedValidator: OneOfSupportedValidator
    = protectAsPublicApi(new OneOfSupportedValidatorImpl(stringValidator)) as OneOfSupportedValidator;
