import * as taquitoUtils from '@taquito/utils';

import { AddressValidator } from './address-validator';
import { StringValidator } from './string-validator';
import { AddressPrefix } from '../address-prefix';
import { ArgError } from '../arg-error';
import { dumpToString } from '../dump-to-string';

export class AddressValidatorImpl implements AddressValidator {
    constructor(private readonly stringValidator: StringValidator) {}

    validate(rawValue: unknown, prefixes?: readonly AddressPrefix[]): string {
        guardPrefixesArg(prefixes);

        const value = this.stringValidator.validate(rawValue, 'NotWhiteSpace');

        if (!isCorrectlyBase58Encoded(value)) {
            throw new Error(`It is NOT correctly base58 encoded address.`);
        }
        if (prefixes) {
            const prefix = prefixes.find(p => value.startsWith(p));
            if (!prefix) {
                throw new Error(`It does not start with any of supported prefixes: ${dumpToString(prefixes)}.`);
            }
        }
        return value;
    }

    getExpectedValueDescription(prefixes?: readonly AddressPrefix[]): string {
        guardPrefixesArg(prefixes);

        const basicDescription = 'a valid Tezos address';
        return prefixes
            ? `${basicDescription} with one of prefixes: ${dumpToString(prefixes)}`
            : basicDescription;
    }
}

function isCorrectlyBase58Encoded(str: string): boolean {
    try {
        taquitoUtils.b58decode(str);
        return true;
    } catch {
        return false;
    }
}

function guardPrefixesArg(prefixes: readonly AddressPrefix[] | undefined): void {
    if (prefixes === undefined) {
        return;
    }
    if (!Array.isArray(prefixes) || prefixes.length === 0 || prefixes.some(p => typeof p !== 'string' || !p.trim().length)) {
        throw new ArgError({
            argName: 'prefixes',
            specifiedValue: prefixes,
            details: 'It is not a non-empty array of valid address prefixes.',
        });
    }
}
