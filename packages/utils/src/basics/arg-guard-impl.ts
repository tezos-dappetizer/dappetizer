import { AddressPrefix } from './address-prefix';
import { ArgError } from './arg-error';
import { ArgGuard } from './arg-guard';
import { errorToString } from './conversion';
import { Constructor, Nullish } from './reflection-types';
import { AddressValidator } from './validators/address-validator';
import { ArrayValidator } from './validators/array-validator';
import { NumberLimits, NumberValidator } from './validators/number-validator';
import { ObjectValidator } from './validators/object-validator';
import { OneOfSupportedValidator } from './validators/one-of-supported-validator';
import { StringLimits, StringValidator } from './validators/string-validator';
import { TypeValidator } from './validators/type-validator';
import { Validator } from './validators/validator';

export class ArgGuardImpl implements ArgGuard {
    constructor(
        private readonly addressGuard: ArgGuardFor<AddressValidator>,
        private readonly arrayGuard: ArgGuardFor<ArrayValidator>,
        private readonly numberGuard: ArgGuardFor<NumberValidator>,
        private readonly objectGuard: ArgGuardFor<ObjectValidator>,
        private readonly oneOfSupportedGuard: ArgGuardFor<OneOfSupportedValidator>,
        private readonly stringGuard: ArgGuardFor<StringValidator>,
        private readonly typeGuard: ArgGuardFor<TypeValidator>,
    ) {}

    address(value: string, argName: string, prefixes?: readonly AddressPrefix[]): string {
        return this.addressGuard.guard(value, argName, prefixes);
    }

    array<T extends readonly unknown[]>(value: T, argName: string): T {
        return this.arrayGuard.guard(value, argName) as T;
    }

    enum<TEnum extends string>(value: TEnum, argName: string, enumType: Readonly<Record<string, TEnum>>): TEnum {
        return this.oneOf(value, argName, Object.values(enumType));
    }

    function<T extends Function>(value: Nullish<T>, argName: string): T {
        return this.typeGuard.guard(value, argName, 'function') as T;
    }

    ifNotNullish<T>(value: Nullish<T>, validate: (value: T) => T): Nullish<T> {
        return value !== undefined && value !== null ? validate(value) : value;
    }

    nonEmptyArray<T extends readonly unknown[]>(value: T, argName: string): T {
        return this.arrayGuard.guard(value, argName, 'NotEmpty') as T;
    }

    nonEmptyString(value: string, argName: string): string {
        return this.stringGuard.guard(value, argName, 'NotEmpty');
    }

    nonWhiteSpaceString(value: string, argName: string): string {
        return this.stringGuard.guard(value, argName, 'NotWhiteSpace');
    }

    number(value: number, argName: string, limits?: NumberLimits): number {
        return this.numberGuard.guard(value, argName, limits);
    }

    object<T extends object>(value: T, argName: string, constructor?: Constructor<T>): T {
        return this.objectGuard.guard(value, argName, constructor) as T;
    }

    oneOf<TSupported extends string, TValue extends TSupported>(value: TValue, argName: string, supportedValues: readonly TSupported[]): TValue {
        return this.oneOfSupportedGuard.guard(value, argName, supportedValues) as TValue;
    }

    string(value: string, argName: string, limits?: StringLimits): string {
        return this.stringGuard.guard(value, argName, limits);
    }
}

export type ArgGuardFor<TValidator> = TValidator extends Validator<infer TValue, infer TLimits>
    ? ArgGuardHelper<TValue, TLimits>
    : never;

export class ArgGuardHelper<TValue, TLimits> {
    constructor(
        private readonly validator: Validator<TValue, TLimits>,
    ) {}

    guard(specifiedValue: TValue, argName: string, limits: TLimits): TValue {
        try {
            return this.validator.validate(specifiedValue, limits);
        } catch (error) {
            const expectedDescription = this.validator.getExpectedValueDescription(limits);
            const details = `The value must be ${expectedDescription}. ${errorToString(error, { onlyMessage: true })}`;
            throw new ArgError({ argName, specifiedValue, details });
        }
    }
}
