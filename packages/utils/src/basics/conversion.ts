import { DeepReadonly } from 'ts-essentials';

export function asReadonly<T>(items: T[]): readonly T[];
export function asReadonly<T>(obj: T): Readonly<T>;
export function asReadonly<T>(obj: T[] | T): readonly T[] | Readonly<T> {
    return obj;
}

export function errorToString(error: unknown, options?: { onlyMessage?: boolean }): string {
    if (error instanceof Error) {
        return options?.onlyMessage === true
            ? error.message
            : errorObjToString(error);
    }
    switch (typeof error) {
        case 'string':
            return error;
        case 'undefined':
            return 'undefined';
        default:
            return JSON.stringify(error);
    }
}

export function errorObjToString(error: Error): string {
    return error.stack ?? error.toString();
}

/** Deeply freezes an object even if it has circular references. */
export function deepFreeze<T>(obj: T): DeepReadonly<T> {
    deepFreezeInternal(obj as unknown as object, new Set<unknown>());
    return obj as DeepReadonly<T>;
}

function deepFreezeInternal(obj: unknown, alreadyFrozen: Set<unknown>): void {
    if (alreadyFrozen.has(obj)) {
        return;
    }

    Object.freeze(obj);
    alreadyFrozen.add(obj);

    for (const propertyName of Object.getOwnPropertyNames(obj)) {
        const value = (obj as Record<string, unknown>)[propertyName];

        if (typeof value === 'object' && value !== null) {
            deepFreezeInternal(value, alreadyFrozen);
        } else if (typeof value === 'function') {
            Object.freeze(value);
        }
    }
}
