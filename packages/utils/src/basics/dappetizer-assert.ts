/** @internal Intended only for Dappetizer itself. */
export function dappetizerAssert(
    condition: boolean,
    message: string,
    dataToDump?: unknown,
): asserts condition {
    if (!condition) {
        throw new DappetizerBug(`${message} Data: ${JSON.stringify(dataToDump)}`);
    }
}

/** @internal Intended only for Dappetizer itself. */
export class DappetizerBug extends Error {}
