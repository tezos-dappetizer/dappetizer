import { Gauge } from 'prom-client';
import { delay, inject, singleton } from 'tsyringe';

import { HttpServerWrapper } from './http-server-wrapper';
import { MetricsUpdater } from '../metrics/public-api';

@singleton()
export class HttpServerMetrics {
    readonly connectionCount = new Gauge({
        name: 'ai_http_server_connections_active',
        help: 'Number of currently active http server connections.',
    });
}

@singleton()
export class HttpServerMetricsUpdater implements MetricsUpdater {
    constructor(
        private readonly metrics: HttpServerMetrics,
        @inject(delay(() => HttpServerWrapper)) private readonly httpServer: HttpServerWrapper,
    ) {}

    async update(): Promise<void> {
        const count = await this.httpServer.getConnections();
        this.metrics.connectionCount.set(count);
    }
}
