import http from 'http';
import { DependencyContainer } from 'tsyringe';

import { EXPRESS_APP_DI_TOKEN } from './express-app-di-token';
import { ExpressAppFactory } from './express-app-factory';
import { HttpServerConfig } from './http-server-config';
import { HttpServerMetricsUpdater } from './http-server-metrics';
import { HttpServerWorker } from './http-server-worker';
import { registerBackgroundWorker } from '../app-control/public-api';
import { registerConfigWithDiagnostics } from '../configuration/public-api';
import { registerSingleton } from '../dependency-injection/public-api';
import { registerMetricsUpdater } from '../metrics/public-api';

export function registerHttpServer(diContainer: DependencyContainer): void {
    // Public API:
    registerSingleton(diContainer, EXPRESS_APP_DI_TOKEN, {
        useFactory: c => c.resolve(ExpressAppFactory).create(),
    });
    registerSingleton(diContainer, http.Server, {
        useFactory: c => http.createServer(c.resolve(EXPRESS_APP_DI_TOKEN)),
    });

    // Inject to Dappetizer features:
    registerBackgroundWorker(diContainer, { useToken: HttpServerWorker });
    registerConfigWithDiagnostics(diContainer, { useToken: HttpServerConfig });
    registerMetricsUpdater(diContainer, { useToken: HttpServerMetricsUpdater });
}
