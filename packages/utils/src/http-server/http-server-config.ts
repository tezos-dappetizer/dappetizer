import { inject, singleton } from 'tsyringe';

import { ConfigObjectElement, ConfigWithDiagnostics, ROOT_CONFIG_ELEMENT_DI_TOKEN } from '../configuration/public-api';

export interface EnabledHttpServerConfig {
    readonly host: string;
    readonly port: number;
}

export const ENABLED_PROPERTY = 'enabled';

@singleton()
export class HttpServerConfig implements ConfigWithDiagnostics {
    static readonly ROOT_NAME = 'httpServer';

    readonly value: EnabledHttpServerConfig | null;

    constructor(@inject(ROOT_CONFIG_ELEMENT_DI_TOKEN) rootElement: ConfigObjectElement) {
        type Keys = (keyof EnabledHttpServerConfig) | typeof ENABLED_PROPERTY;
        const thisElement = rootElement.getOptional(HttpServerConfig.ROOT_NAME)?.asObject<Keys>();

        this.value = thisElement?.getOptional(ENABLED_PROPERTY)?.asBoolean() === true
            ? {
                host: thisElement.getOptional('host')?.asString('NotWhiteSpace')
                    ?? '0.0.0.0',
                port: thisElement.getOptional('port')?.asInteger({ min: 0, max: 65535 })
                    ?? 3600,
            }
            : null;
    }

    getDiagnostics(): [string, unknown] {
        const config = this.value ?? 'disabled';
        return [HttpServerConfig.ROOT_NAME, config];
    }
}
