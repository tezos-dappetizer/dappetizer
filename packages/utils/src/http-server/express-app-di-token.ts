import { Application as ExpressApplication } from 'express';
import { InjectionToken } from 'tsyringe';

export const EXPRESS_APP_DI_TOKEN: InjectionToken<ExpressApplication> = 'Dappetizer:Utils:ExpressApplication';
