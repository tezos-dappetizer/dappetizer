/**
 * Configures HTTP server of this indexer app mainly for monitoring it.
 * This is used as a part of {@link "@tezos-dappetizer/database".DappetizerConfigUsingDb}.
 * @example Sample usage (all properties specified explicitly) within `dappetizer.config.ts` file:
 * ```typescript
 * import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';
 *
 * const config: DappetizerConfigUsingDb = {
 *     httpServer: {
 *         enabled: false,
 *         host: '0.0.0.0',
 *         port: 3600,
 *     },
 *     ... // Other config parts.
 * };
 *
 * export default config;
 * ```
 * @category Dappetizer Config
 */
export interface HttpServerDappetizerConfig {
    /**
     * Boolean indicating if HTTP server should be enabled.
     * @default `false`
     */
    enabled?: boolean;

    /**
     * The hostname or IP address on which HTTP server should listen.
     * @default `'0.0.0.0'` (listening on all IP addresses)
     */
    host?: string;

    /**
     * The port on which HTTP server should listen.
     * @limits An integer between `0` (which means random free port) and `65_535`.
     * @default `3600`
     */
    port?: number;
}
