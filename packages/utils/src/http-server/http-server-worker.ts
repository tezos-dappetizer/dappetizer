import { singleton } from 'tsyringe';

import { HttpServerConfig } from './http-server-config';
import { HttpServerWrapper } from './http-server-wrapper';
import { BackgroundWorker } from '../app-control/public-api';
import { injectLogger, Logger } from '../logging/public-api';

@singleton()
export class HttpServerWorker implements BackgroundWorker {
    readonly name = 'HttpServer';

    constructor(
        private readonly httpServer: HttpServerWrapper,
        private readonly config: HttpServerConfig,
        @injectLogger(HttpServerWorker) private readonly logger: Logger,
    ) {}

    async start(): Promise<void> {
        const config = this.config.value;
        if (!config) {
            this.logger.logInformation('Http server is disabled by configuration. To change this behavior, please set corresponding http config.');
            return;
        }

        const url = `http://${config.host}:${config.port}`;
        this.logger.logInformation('Http server starting at {url}.', { url });

        await this.httpServer.listen(config.port, config.host);
    }

    async stop(): Promise<void> {
        if (!this.config.value) {
            return;
        }

        await this.httpServer.close();
    }
}
