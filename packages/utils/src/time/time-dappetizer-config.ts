/**
 * Configures generic time related features.
 * This is used as a part of {@link "@tezos-dappetizer/database".DappetizerConfigUsingDb}.
 * @example Sample usage (all properties specified explicitly) within `dappetizer.config.ts` file:
 * ```typescript
 * import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';
 *
 * const config: DappetizerConfigUsingDb = {
 *     time: {
 *         longExecutionWarningMillis: 1_000,
 *     },
 *     ... // Other config parts.
 * };
 *
 * export default config;
 * ```
 * @category Dappetizer Config
 */
export interface TimeDappetizerConfig {
    /**
     * The time in milliseconds after which a warning is logged for an operation that takes loo long to execute.
     * @limits An integer greater than `1`.
     * @default `3_000` (3 seconds)
     */
    longExecutionWarningMillis?: number;
}
