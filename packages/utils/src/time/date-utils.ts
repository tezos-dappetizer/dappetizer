import { argGuard } from '../basics/public-api';

export function addMillis(date: Date, millis: number): Date {
    argGuard.object(date, 'date', Date);
    argGuard.number(millis, 'millis');

    return new Date(date.getTime() + millis);
}
