import { inject, singleton } from 'tsyringe';

import { LongExecutionHelper, LongExecutionOperation } from './long-execution-helper';
import { TimeConfig } from './time-config';
import { TIMER_HELPER_DI_TOKEN, TimerHelper } from './timer-helper';
import { argGuard } from '../basics/public-api';
import { injectLogger, Logger } from '../logging/public-api';

@singleton()
export class LongExecutionHelperImpl implements LongExecutionHelper {
    constructor(
        private readonly config: TimeConfig,
        @inject(TIMER_HELPER_DI_TOKEN) private readonly timerHelper: TimerHelper,
        @injectLogger(LongExecutionHelperImpl) private readonly logger: Logger,
    ) {}

    async warn<TResult>(operation: LongExecutionOperation<TResult>): Promise<TResult> {
        argGuard.object(operation, 'operation');
        argGuard.nonWhiteSpaceString(operation.description, 'operation.description');
        argGuard.function(operation.execute, 'operation.execute'); // eslint-disable-line @typescript-eslint/unbound-method

        const millis = this.config.longExecutionWarningMillis;

        const timeout = this.timerHelper.setTimeout(millis, () => {
            this.logger.logWarning('Execution of {operation} is taking longer than {millis}.', {
                operation: operation.description,
                millis,
            });
        });

        try {
            return await operation.execute();
        } finally {
            timeout.clear();
        }
    }
}
