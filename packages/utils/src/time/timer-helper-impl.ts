import { inject, singleton } from 'tsyringe';

import { Timeout, TimerHelper } from './timer-helper';
import { addAbortListener, APP_SHUTDOWN_MANAGER_DI_TOKEN, AppShutdownManager } from '../app-control/public-api';
import { argGuard } from '../basics/public-api';

@singleton()
export class TimerHelperImpl implements TimerHelper {
    constructor(
        @inject(APP_SHUTDOWN_MANAGER_DI_TOKEN) private readonly appShutdownManager: AppShutdownManager,
    ) {}

    setTimeout(delayMillis: number, callback: () => void): Timeout {
        argGuard.number(delayMillis, 'delayMillis');
        argGuard.function(callback, 'callback');

        // Avoids memory leak of timeoutId locked in abortion closure.
        let unlinkAbortion: (() => void) | null = null;

        const timeoutId = setTimeout(
            () => {
                unlinkAbortion?.();
                callback();
            },
            delayMillis,
        );

        unlinkAbortion = addAbortListener(this.appShutdownManager.signal, () => clearTimeout(timeoutId));

        return {
            clear() {
                unlinkAbortion?.();
                clearTimeout(timeoutId);
            },
        };
    }
}
