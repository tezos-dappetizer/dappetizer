import { InjectionToken } from 'tsyringe';

export interface Timeout {
    clear(): void;
}

export interface TimerHelper {
    setTimeout(delayMillis: number, callback: () => void): Timeout;
}

export const TIMER_HELPER_DI_TOKEN: InjectionToken<TimerHelper> = 'Dappetizer:Utils:TimerHelper';
