import { InjectionToken } from 'tsyringe';

/** Usable mainly for delays for given amount of milliseconds.  */
export interface SleepHelper {
    sleep(sleepMillis: number): Promise<void>;
}

export const SLEEP_HELPER_DI_TOKEN: InjectionToken<SleepHelper> = 'Dappetizer:Utils:SleepHelper';
