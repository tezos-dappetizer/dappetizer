import { inject, singleton } from 'tsyringe';

import { ConfigObjectElement, ConfigWithDiagnostics, ROOT_CONFIG_ELEMENT_DI_TOKEN } from '../configuration/public-api';

@singleton()
export class TimeConfig implements ConfigWithDiagnostics {
    static readonly ROOT_NAME = 'time';

    readonly longExecutionWarningMillis: number;

    constructor(@inject(ROOT_CONFIG_ELEMENT_DI_TOKEN) rootElement: ConfigObjectElement) {
        const thisElement = rootElement.getOptional(TimeConfig.ROOT_NAME)?.asObject<keyof TimeConfig>();

        this.longExecutionWarningMillis = thisElement?.getOptional('longExecutionWarningMillis')?.asInteger({ min: 1 })
            ?? 3_000;
    }

    getDiagnostics(): [string, unknown] {
        return [TimeConfig.ROOT_NAME, this];
    }
}
