import { inject, singleton } from 'tsyringe';

import { SleepHelper } from './sleep-helper';
import { TIMER_HELPER_DI_TOKEN, TimerHelper } from './timer-helper';
import { argGuard } from '../basics/public-api';

@singleton()
export class SleepHelperImpl implements SleepHelper {
    constructor(
        @inject(TIMER_HELPER_DI_TOKEN) private readonly timerHelper: TimerHelper,
    ) {}

    async sleep(sleepMillis: number): Promise<void> {
        argGuard.number(sleepMillis, 'sleepMillis');

        if (sleepMillis > 0) {
            await new Promise<void>(resolve => {
                this.timerHelper.setTimeout(
                    sleepMillis,
                    () => resolve(),
                );
            });
        }
    }
}
