import { singleton } from 'tsyringe';

import { Clock } from './clock';

@singleton()
export class ClockImpl implements Clock {
    getNowDate(): Date {
        return new Date();
    }

    getNowTimestamp(): number {
        return Date.now();
    }
}
