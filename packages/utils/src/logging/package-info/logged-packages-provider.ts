import { StrictOmit } from 'ts-essentials';
import { injectAll, singleton } from 'tsyringe';

import { LoggedPackageInfo } from './logged-package-info';
import { LOGGED_PACKAGE_INFOS_DI_TOKEN } from './logged-package-info-di-token';

@singleton()
export class LoggedPackagesProvider {
    readonly packages: Readonly<Record<string, StrictOmit<LoggedPackageInfo, 'name'>>>;

    constructor(@injectAll(LOGGED_PACKAGE_INFOS_DI_TOKEN) packageInfos: readonly LoggedPackageInfo[]) {
        const result: Record<string, StrictOmit<LoggedPackageInfo, 'name'>> = {};
        this.packages = result;

        for (const info of packageInfos) {
            const conflict = result[info.name];
            if (conflict) {
                throw new Error(`LoggedPackageInfo cannot be registered multiple times but there are:`
                    + ` ${JSON.stringify(info)} vs ${JSON.stringify({ ...info, ...conflict })}`);
            }

            result[info.name] = {
                commitHash: info.commitHash,
                version: info.version,
            };
        }
    }
}
