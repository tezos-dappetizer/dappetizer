import { InjectionToken } from 'tsyringe';

import { LoggedPackageInfo } from './logged-package-info';

export const LOGGED_PACKAGE_INFOS_DI_TOKEN: InjectionToken<LoggedPackageInfo> = 'Dappetizer:Utils:LoggedPackageInfos';
