import { DependencyContainer, Provider } from 'tsyringe';

import { LOGGED_PACKAGE_INFOS_DI_TOKEN } from './logged-package-info-di-token';
import { registerSingleton } from '../../dependency-injection/public-api';

export interface LoggedPackageInfo {
    readonly name: string;
    readonly commitHash: string;
    readonly version: string;
}

/**
 * Registers a version info of a package to be logged by Dappetizer app.
 * It is registered as **singleton**.
 */
export function registerLoggedPackageInfo(diContainer: DependencyContainer, versionInfoProvider: Provider<LoggedPackageInfo>): void {
    registerSingleton(diContainer, LOGGED_PACKAGE_INFOS_DI_TOKEN, versionInfoProvider);
}
