import { injectWithTransform, singleton } from 'tsyringe';
import Transform from 'tsyringe/dist/typings/types/transform'; // eslint-disable-line import/no-unresolved

import { Logger } from './logger';
import { LoggerCategory, LoggerFactory, LOGGER_FACTORY_DI_TOKEN } from './logger-factory';

@singleton()
class LoggerTransformer implements Transform<LoggerFactory, Logger> {
    transform(loggerFactory: LoggerFactory, category: LoggerCategory): Logger {
        return loggerFactory.getLogger(category);
    }
}

/** Used on constructor parameter for dependency injection of a logger for the specified category. */
export function injectLogger(category: LoggerCategory): ReturnType<typeof injectWithTransform> {
    return injectWithTransform(LOGGER_FACTORY_DI_TOKEN, LoggerTransformer, category);
}
