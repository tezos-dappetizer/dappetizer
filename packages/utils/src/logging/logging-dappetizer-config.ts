/**
 * Configures logging of app execution.
 * This is used as a part of {@link "@tezos-dappetizer/database".DappetizerConfigUsingDb}.
 * @example Sample usage (all properties specified explicitly) within `dappetizer.config.ts` file:
 * ```typescript
 * import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';
 *
 * const config: DappetizerConfigUsingDb = {
 *     logging: {
 *         console: {
 *             minLevel: 'information',
 *             format: 'coloredMessages',
 *         },
 *         file: {
 *             minLevel: 'off',
 *             path: 'logs/log.jsonl',
 *             format: 'json',
 *             maxSize: '100m',
 *             maxFiles: '7d',
 *         },
 *         globalData: {
 *             env: 'prod',
 *             machine: 'skynet',
 *         },
 *         maxStringLength: 100_000,
 *     },
 *     ... // Other config parts.
 * };
 *
 * export default config;
 * ```
 * @category Dappetizer Config
 */
export interface LoggingDappetizerConfig {
    /** Logging to the console. */
    console?: {
        /**
         * The minimum level of log entries written to the console.
         * @default `information`
         */
        minLevel?: 'off' | 'critical' | 'error' | 'warning' | 'information' | 'debug' | 'trace';

        /**
         * Defines how log entries are serialized.
         * @default `coloredMessages`
         */
        format?: 'json' | 'messages' | 'coloredMessages' | 'coloredSimpleMessages';
    };

    /** Logging to the specified file. */
    file?: {
        /**
         * The minimum level of log entries written to the console.
         * @default `off`
         */
        minLevel?: 'off' | 'critical' | 'error' | 'warning' | 'information' | 'debug' | 'trace';

        /**
         * The path to the file where log entries are written.
         * If {@link minLevel} is not `off`, then it must be specified explicitly.
         * @limits An absolute file path.
         */
        path?: string;

        /**
         * Defines how log entries are serialized.
         * @default `json`
         */
        format?: 'json' | 'messages' | 'coloredMessages' | 'coloredSimpleMessages';

        /**
         * Maximum file size after which it {@link https://www.npmjs.com/package/winston-daily-rotate-file | gets rotated}.
         * This can be a string containing a number with suffix `k` for kilobytes, `m` for megabytes or `g` for gigabytes.
         * @default `100m`
         */
        maxSize?: string;

        /**
         * Maximum number of log files to keep.
         * This can be a string with number of files or number of days with `d` suffix.
         * @default `7d`
         */
        maxFiles?: string;
    };

    /**
     * Global data appended to all log entries.
     * Useful for example for environment or server name.
     * @default `undefined`
     */
    globalData?: unknown;

    /**
     * Maximum logged length of logged string in `data` values. Longer strings get truncated.
     * @limits An integer with minimum `1`.
     * @default `9007199254740991` (maximum safe integer)
     */
    maxStringLength?: number;
}
