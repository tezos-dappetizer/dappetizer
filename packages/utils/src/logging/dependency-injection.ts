import { DependencyContainer } from 'tsyringe';

import { LoggerFactoryImpl } from './impl/logger-factory-impl';
import { Logger } from './logger';
import { LOGGER_FACTORY_DI_TOKEN, LoggerFactory } from './logger-factory';
import { LoggingConfig } from './logging-config';
import { registerLoggedPackageInfo } from './package-info/logged-package-info';
import { ROOT_LOGGER_DI_TOKEN, ROOT_LOGGER_FACTORY_DI_TOKEN, RootLogger, RootLoggerFactory } from './root-logger';
import { WinstonRootLoggerFactory } from './winston/winston-root-logger-factory';
import { registerConfigWithDiagnostics } from '../configuration/public-api';
import { registerPublicApi, registerSingleton } from '../dependency-injection/public-api';
import { versionInfo } from '../version';

export function registerLogging(diContainer: DependencyContainer): void {
    // Public API:
    registerSingleton(diContainer, ROOT_LOGGER_DI_TOKEN, {
        useFactory: c => c.resolve(ROOT_LOGGER_FACTORY_DI_TOKEN).create(),
    });
    registerPublicApi(diContainer, ROOT_LOGGER_FACTORY_DI_TOKEN, {
        useInternalToken: WinstonRootLoggerFactory,
        createProxy: protectRootLoggerFactoryPublicApi,
    });
    registerPublicApi(diContainer, LOGGER_FACTORY_DI_TOKEN, {
        useInternalToken: LoggerFactoryImpl,
        createProxy: protectLoggerFactoryPublicApi,
    });

    // Inject to Dappetizer features:
    registerConfigWithDiagnostics(diContainer, { useToken: LoggingConfig });
    registerLoggedPackageInfo(diContainer, {
        useValue: {
            name: 'dappetizer',
            version: versionInfo.VERSION,
            commitHash: versionInfo.COMMIT_HASH,
        },
    });
}

function protectRootLoggerFactoryPublicApi(factory: RootLoggerFactory): RootLoggerFactory {
    return {
        create: () => {
            const logger = factory.create();
            return Object.freeze<RootLogger>({
                close: logger.close.bind(logger),
                isEnabled: logger.isEnabled.bind(logger),
                log: logger.log.bind(logger),
            });
        },
    };
}

function protectLoggerFactoryPublicApi(factory: LoggerFactory): LoggerFactory {
    return {
        getLogger: category => {
            const logger = factory.getLogger(category);
            return Object.freeze<Logger>({
                category: logger.category,
                isCriticalEnabled: logger.isCriticalEnabled,
                isErrorEnabled: logger.isErrorEnabled,
                isWarningEnabled: logger.isWarningEnabled,
                isInformationEnabled: logger.isInformationEnabled,
                isDebugEnabled: logger.isDebugEnabled,
                isTraceEnabled: logger.isTraceEnabled,
                isEnabled: logger.isEnabled.bind(logger),
                log: logger.log.bind(logger),
                runWithData: logger.runWithData.bind(logger),
                logCritical: logger.logCritical.bind(logger),
                logError: logger.logError.bind(logger),
                logWarning: logger.logWarning.bind(logger),
                logInformation: logger.logInformation.bind(logger),
                logDebug: logger.logDebug.bind(logger),
                logTrace: logger.logTrace.bind(logger),
            });
        },
    };
}
