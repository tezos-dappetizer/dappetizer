import { AsyncLocalStorage } from 'async_hooks';
import { singleton } from 'tsyringe';

import { LogData } from './log-entry';

/**
 * Stores additional data that flow through given scope and get appended to all log entries inside.
 * There must be only one scope instance so that scoped data flow accross loggers of various app components.
 */
@singleton()
export class LoggerScope {
    // Private fields are JS private (using #) & instance is frozen b/c the class is exposed as public API.
    readonly #storage = new AsyncLocalStorage<LogData>();

    constructor() {
        Object.freeze(this);
    }

    runWithData<TResult>(data: LogData, scopeFunc: () => TResult): TResult {
        const parentData = this.#storage.getStore();
        const fullData = { ...parentData, ...data };

        return this.#storage.run(fullData, scopeFunc);
    }

    enrichData(data: LogData): LogData {
        const scopeData = this.#storage.getStore();
        return scopeData ? { ...scopeData, ...data } : data;
    }
}
