import { argGuard } from '../../basics/arg-guard';
import { LogData, LogLevel } from '../log-entry';
import { Logger } from '../logger';
import { LoggerScope } from '../logger-scope';

/** Common logic for {@link Logger} implementations. */
export abstract class BaseLogger implements Logger {
    readonly category: string;
    readonly #scope = new LoggerScope(); // JS private fields (using #) b/c the class is exposed as public API.

    constructor(category: string, scope = new LoggerScope()) {
        this.category = argGuard.nonWhiteSpaceString(category, 'category');
        this.#scope = argGuard.object(scope, 'scope');
    }

    get isCriticalEnabled(): boolean {
        return this.isEnabled(LogLevel.Critical);
    }

    get isErrorEnabled(): boolean {
        return this.isEnabled(LogLevel.Error);
    }

    get isWarningEnabled(): boolean {
        return this.isEnabled(LogLevel.Warning);
    }

    get isInformationEnabled(): boolean {
        return this.isEnabled(LogLevel.Information);
    }

    get isDebugEnabled(): boolean {
        return this.isEnabled(LogLevel.Debug);
    }

    get isTraceEnabled(): boolean {
        return this.isEnabled(LogLevel.Trace);
    }

    log(level: LogLevel, message: string, data: LogData = {}): void {
        argGuard.enum(level, 'level', LogLevel);
        argGuard.string(message, 'message');
        argGuard.object(data, 'data');

        if (!this.isEnabled(level)) {
            return;
        }

        const completeData = this.#scope.enrichData(data);
        this.writeToLog(level, message, completeData);
    }

    runWithData<TResult>(data: LogData, scopeFunc: () => TResult): TResult {
        return this.#scope.runWithData(data, scopeFunc);
    }

    logCritical(message: string, data?: LogData): void {
        this.log(LogLevel.Critical, message, data);
    }

    logError(message: string, data?: LogData): void {
        this.log(LogLevel.Error, message, data);
    }

    logWarning(message: string, data?: LogData): void {
        this.log(LogLevel.Warning, message, data);
    }

    logInformation(message: string, data?: LogData): void {
        this.log(LogLevel.Information, message, data);
    }

    logDebug(message: string, data?: LogData): void {
        this.log(LogLevel.Debug, message, data);
    }

    logTrace(message: string, data?: LogData): void {
        this.log(LogLevel.Trace, message, data);
    }

    abstract isEnabled(level: LogLevel): boolean;
    protected abstract writeToLog(level: LogLevel, message: string, data: LogData): void;
}
