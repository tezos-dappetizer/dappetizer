import { singleton } from 'tsyringe';

import { StringSanitizer } from '../../basics/public-api';
import { LoggingConfig } from '../logging-config';

@singleton()
export class LoggedStringLimiter implements StringSanitizer {
    constructor(
        private readonly config: LoggingConfig,
    ) {}

    sanitizeString(str: string): string {
        return str.length > this.config.maxStringLength
            ? `${str.substring(0, this.config.maxStringLength)}... (${str.length} chars in total)`
            : str;
    }
}
