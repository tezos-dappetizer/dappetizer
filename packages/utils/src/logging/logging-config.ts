import { inject, singleton } from 'tsyringe';

import { LogLevel } from './log-entry';
import { getEnumValues } from '../basics/public-api';
import { ConfigObjectElement, ConfigWithDiagnostics, ROOT_CONFIG_ELEMENT_DI_TOKEN } from '../configuration/public-api';

export const OFF_LOG_LEVEL = 'off';

export enum LogFormat {
    Json = 'json',
    Messages = 'messages',
    ColoredMessages = 'coloredMessages',
    ColoredSimpleMessages = 'coloredSimpleMessages',
}

export interface ConsoleLoggingConfig {
    readonly minLevel: LogLevel;
    readonly format: LogFormat;
}

export interface FileLoggingConfig {
    readonly minLevel: LogLevel;
    readonly format: LogFormat;
    readonly path: string;
    readonly maxSize: string;
    readonly maxFiles: string;
}

@singleton()
export class LoggingConfig implements ConfigWithDiagnostics {
    static readonly ROOT_NAME = 'logging';

    readonly console: ConsoleLoggingConfig | null;
    readonly file: FileLoggingConfig | null;
    readonly globalData: unknown;
    readonly maxStringLength: number;

    constructor(@inject(ROOT_CONFIG_ELEMENT_DI_TOKEN) rootElement: ConfigObjectElement) {
        const thisElement = rootElement.getOptional(LoggingConfig.ROOT_NAME)?.asObject<keyof LoggingConfig>();

        this.console = getConsoleConfig(thisElement);
        this.file = getFileConfig(thisElement);
        this.globalData = thisElement?.getOptional('globalData')?.value;
        this.maxStringLength = thisElement?.getOptional('maxStringLength')?.asInteger({ min: 1 })
            ?? Number.MAX_SAFE_INTEGER;
    }

    getDiagnostics(): [string, unknown] {
        return [LoggingConfig.ROOT_NAME, this];
    }
}

const minLogLevels: (LogLevel | 'off')[] = [OFF_LOG_LEVEL, ...getEnumValues(LogLevel)];

function getConsoleConfig(loggingElement: ConfigObjectElement<keyof LoggingConfig> | undefined): ConsoleLoggingConfig | null {
    const consoleElement = loggingElement?.getOptional('console')?.asObject<keyof ConsoleLoggingConfig>();
    const consoleMinLogLevel = consoleElement?.getOptional('minLevel')?.asOneOf(minLogLevels)
        ?? LogLevel.Information;

    return consoleMinLogLevel !== OFF_LOG_LEVEL
        ? {
            minLevel: consoleMinLogLevel,
            format: consoleElement?.getOptional('format')?.asEnum(LogFormat)
                ?? LogFormat.ColoredMessages,
        }
        : null;
}

function getFileConfig(loggingElement: ConfigObjectElement<keyof LoggingConfig> | undefined): FileLoggingConfig | null {
    const fileElement = loggingElement?.getOptional('file')?.asObject<keyof FileLoggingConfig>();
    const fileMinLogLevel = fileElement?.getOptional('minLevel')?.asOneOf(minLogLevels)
        ?? OFF_LOG_LEVEL;

    return fileElement && fileMinLogLevel !== OFF_LOG_LEVEL
        ? {
            minLevel: fileMinLogLevel,
            format: fileElement.getOptional('format')?.asEnum(LogFormat)
                ?? LogFormat.Json,
            path: fileElement.get('path').asString('NotWhiteSpace'),
            maxSize: fileElement.getOptional('maxSize')?.asString('NotWhiteSpace')
                ?? '100m',
            maxFiles: fileElement.getOptional('maxFiles')?.asString('NotWhiteSpace')
                ?? '7d',
        }
        : null;
}
