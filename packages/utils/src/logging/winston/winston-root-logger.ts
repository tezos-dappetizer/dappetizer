import { Logger } from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';
import TransportStream from 'winston-transport';

import { argGuard, keyof } from '../../basics/public-api';
import { LogEntry, LogLevel } from '../log-entry';
import { RootLogger } from '../root-logger';

export class WinstonRootLogger implements RootLogger {
    constructor(
        private readonly winstonLogger: Logger,
    ) {}

    isEnabled(level: LogLevel): boolean {
        argGuard.enum(level, 'level', LogLevel);

        return this.winstonLogger.isLevelEnabled(level);
    }

    log(entry: LogEntry): void {
        argGuard.object(entry, 'entry');

        this.winstonLogger.log(entry);
    }

    async close(): Promise<void> {
        // `winstonLogger.on('finish')` is called before all transports finish -> register on them.
        const transportsFinishedPromise = Promise.all(this.winstonLogger.transports.map(
            async transport => new Promise<void>(resolve => {
                if (hasLogStream(transport)) {
                    transport.logStream.on('finish', resolve);
                } else {
                    transport.on('finish', resolve);
                }
            }),
        ));

        this.winstonLogger.end();
        await transportsFinishedPromise;
        this.winstonLogger.close();
    }
}

function hasLogStream(transport: TransportStream): transport is (TransportStream & Pick<DailyRotateFile, 'logStream'>) {
    return keyof<DailyRotateFile>('logStream') in transport;
}
