/* istanbul ignore file */
import path from 'path';
import { singleton } from 'tsyringe';
import * as winston from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';

import { FormatEntryOptions, formatEntryToString, formatMessage } from './winston-logger-formatter';
import { WinstonRootLogger } from './winston-root-logger';
import { errorToString, sanitizeForJson } from '../../basics/public-api';
import { LoggedStringLimiter } from '../impl/logged-string-limiter';
import { NullRootLogger } from '../impl/null-root-logger';
import { LogEntry, LogLevel } from '../log-entry';
import { LogFormat, LoggingConfig } from '../logging-config';
import { RootLogger, RootLoggerFactory } from '../root-logger';

/** Initializes Winston root logger according to env config. */
@singleton()
export class WinstonRootLoggerFactory implements RootLoggerFactory {
    constructor(
        private readonly loggingConfig: LoggingConfig,
        private readonly stringLimiter: LoggedStringLimiter,
    ) {}

    create(): RootLogger {
        const transports = Array.from(this.determineTransports());
        if (transports.length === 0) {
            return new NullRootLogger();
        }

        const levelNumbers: Record<LogLevel, number> = {
            critical: 1,
            error: 2,
            warning: 3,
            information: 4,
            debug: 5,
            trace: 6,
        };
        const logger = winston.createLogger({
            levels: { off: 0, ...levelNumbers },
            level: 'off', // Must be defined for isLevelEnabled() to work properly.
            format: winston.format((e) => e)(), // Root format is not a final one -> just a pass-through.
        });

        transports.forEach(t => logger.add(t));
        return new WinstonRootLogger(logger);
    }

    private *determineTransports(): Iterable<winston.transport> {
        const consoleConfig = this.loggingConfig.console;
        if (consoleConfig) {
            yield new winston.transports.Console({
                level: consoleConfig.minLevel,
                format: this.getFormat(consoleConfig.format),
                stderrLevels: [LogLevel.Critical, LogLevel.Error, LogLevel.Warning],
            });
        }

        const fileConfig = this.loggingConfig.file;
        if (fileConfig) {
            const file = path.parse(path.resolve(fileConfig.path));
            yield new DailyRotateFile({
                level: fileConfig.minLevel,
                filename: `${file.dir}/${file.name}.%DATE%${file.ext}`,
                utc: true,
                zippedArchive: true,
                maxSize: fileConfig.maxSize,
                maxFiles: fileConfig.maxFiles,
                format: this.getFormat(fileConfig.format),
                createSymlink: true,
                symlinkName: `${file.name}${file.ext}`,
            });
        }
    }

    private getFormat(format: LogFormat): winston.Logform.Format {
        switch (format) {
            case LogFormat.Json:
                return createFormat(entry => {
                    try {
                        const formattedMessage = formatMessage(entry);
                        const jsonEntry = sanitizeForJson({ ...entry, formattedMessage }, this.stringLimiter);
                        return JSON.stringify(jsonEntry);
                    } catch (error) {
                        throw new Error(`Failed to serialize log entry with message '${entry.message}'. ${errorToString(error)}`);
                    }
                });
            case LogFormat.Messages:
                return this.createMessagesFormat();
            case LogFormat.ColoredMessages:
                return this.createMessagesFormat({ colorize: true });
            case LogFormat.ColoredSimpleMessages:
                return this.createMessagesFormat({ colorize: true, simplify: true });
        }
    }

    private createMessagesFormat(options?: FormatEntryOptions): winston.Logform.Format {
        return createFormat(entry => this.stringLimiter.sanitizeString(formatEntryToString(entry, options)));
    }
}

function createFormat(templateFunc: (entry: LogEntry) => string): winston.Logform.Format {
    return winston.format.printf(e => templateFunc(e as LogEntry));
}
