import { InjectionToken } from 'tsyringe';

import { LogEntry, LogLevel } from './log-entry';

/** Root logger API from Winston that we intend to use. */
export interface RootLogger {
    isEnabled(level: LogLevel): boolean;
    log(entry: LogEntry): void;
    close(): Promise<void>;
}

export interface RootLoggerFactory {
    create(): RootLogger;
}

export const ROOT_LOGGER_DI_TOKEN: InjectionToken<RootLogger> = 'Dappetizer:Utils:RootLogger';

export const ROOT_LOGGER_FACTORY_DI_TOKEN: InjectionToken<RootLoggerFactory> = 'Dappetizer:Utils:RootLoggerFactory';
