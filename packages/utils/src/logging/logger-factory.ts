import { DependencyContainer, InjectionToken } from 'tsyringe';

import { Logger } from './logger';
import { Constructor } from '../basics/public-api';

export type LoggerCategory = string | Constructor;

/** Creates a Logger with given category. */
export interface LoggerFactory {
    getLogger(category: LoggerCategory): Logger;
}

export const LOGGER_FACTORY_DI_TOKEN: InjectionToken<LoggerFactory> = 'Dappetizer:Utils:LoggerFactory';

export function resolveLogger(diContainer: DependencyContainer, category: LoggerCategory): Logger {
    const factory = diContainer.resolve(LOGGER_FACTORY_DI_TOKEN);
    return factory.getLogger(category);
}
