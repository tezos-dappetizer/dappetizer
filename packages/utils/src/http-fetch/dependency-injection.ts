import fetch from 'node-fetch';
import { DependencyContainer } from 'tsyringe';

import { HTTP_FETCHER_DI_TOKEN, HttpFetcher } from './http-fetcher';
import { JSON_FETCHER_DI_TOKEN, JsonFetcher } from './json-fetcher';
import { JsonFetcherImpl } from './json-fetcher-impl';
import { LoggingJsonFetcher } from './logging-json-fetcher';
import { registerSingleton, resolveWithExplicitArgs } from '../dependency-injection/public-api';

export function registerHttpFetch(diContainer: DependencyContainer): void {
    // Public API:
    registerSingleton(diContainer, JSON_FETCHER_DI_TOKEN, {
        useFactory: c => {
            const fetcher = resolveWithExplicitArgs(c, LoggingJsonFetcher, [c.resolve(JsonFetcherImpl)]);
            return Object.freeze<JsonFetcher>({
                execute: fetcher.execute.bind(fetcher),
            });
        },
    });
    diContainer.registerInstance(HTTP_FETCHER_DI_TOKEN, Object.freeze({ fetch } as unknown as HttpFetcher));
}
