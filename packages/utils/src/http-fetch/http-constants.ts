export const contentTypes = Object.freeze({
    JSON: 'application/json',
    TEXT: 'text/plain',
});

/** Usually compression algorithms. */
export const contentEncodings = Object.freeze({
    DEFLATE: 'deflate',
    GZIP: 'gzip',
});

export const httpHeaders = Object.freeze({
    ACCEPT: 'Accept',
    ACCEPT_ENCODING: 'Accept-Encoding',
    CONTENT_TYPE: 'Content-Type',
});

export const httpMethods = Object.freeze({
    DELETE: 'DELETE',
    GET: 'GET',
    HEAD: 'HEAD',
    OPTIONS: 'OPTIONS',
    PATCH: 'PATCH',
    POST: 'POST',
    PUT: 'PUT',
});

export const httpStatusCodes = Object.freeze({
    INTERNAL_SERVER_ERROR: 500,
    OK: 200,
});
