import { inject, singleton } from 'tsyringe';

import { contentTypes, httpHeaders } from './http-constants';
import { HTTP_FETCHER_DI_TOKEN, HttpFetcher, RequestOptions } from './http-fetcher';
import { JsonFetchCommand, JsonFetcher } from './json-fetcher';
import { APP_SHUTDOWN_MANAGER_DI_TOKEN, AppShutdownManager } from '../app-control/public-api';
import { errorToString } from '../basics/public-api';

@singleton()
export class JsonFetcherImpl implements JsonFetcher {
    constructor(
        @inject(HTTP_FETCHER_DI_TOKEN) private readonly httpFetcher: HttpFetcher,
        @inject(APP_SHUTDOWN_MANAGER_DI_TOKEN) private readonly appShutdownManager?: AppShutdownManager,
    ) {}

    async execute<TResponse>(command: JsonFetchCommand<TResponse>): Promise<TResponse> {
        const requestHeaders: RequestOptions['headers'] = {
            [httpHeaders.ACCEPT]: contentTypes.JSON,
            ...command.requestOptions?.headers,
        };
        try {
            const httpResponse = await this.httpFetcher.fetch(command.url, {
                signal: this.appShutdownManager?.signal,
                ...command.requestOptions,
                headers: requestHeaders,
            });

            if (!httpResponse.ok) {
                throw new Error(`The response status is ${httpResponse.status} ${httpResponse.statusText}.`);
            }

            const rawData: unknown = await httpResponse.json();
            try {
                const data = command.transformResponse(rawData);
                return data;
            } catch (error) {
                throw new Error(`Failed to transform the response: ${errorToString(error, { onlyMessage: true })}\n`
                    + `Response body: ${JSON.stringify(rawData)}`);
            }
        } catch (error) {
            throw new Error(`Failed HTTP request '${command.description}' to  ${command.url}.\n`
                + `Error: ${errorToString(error, { onlyMessage: true })}\n`
                + `Request headers: ${JSON.stringify(requestHeaders)}\n`
                + `Request body: ${command.requestOptions?.body?.toString() ?? 'null'}`);
        }
    }
}
