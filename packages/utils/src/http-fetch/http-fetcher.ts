import { RequestInfo, RequestInit, Response } from 'node-fetch';
import { StrictOmit } from 'ts-essentials';
import { InjectionToken } from 'tsyringe';

/** Simple wrapper around `fetch` for easier mocking in tests. */
export interface HttpFetcher {
    fetch(
        url: RequestInfo,
        init?: RequestOptions,
    ): Promise<Response>;
}

export type RequestOptions = StrictOmit<RequestInit, 'signal'> & {
    signal?: AbortSignal | null;
};

export const HTTP_FETCHER_DI_TOKEN: InjectionToken<HttpFetcher> = 'Dappetizer:Utils:HttpFetcher';
