import { inject, injectable } from 'tsyringe';

import { httpMethods } from './http-constants';
import { JsonFetchCommand, JsonFetcher } from './json-fetcher';
import { injectExplicit } from '../dependency-injection/public-api';
import { ENABLE_TRACE, injectLogger, LogData, Logger } from '../logging/public-api';
import { Clock, CLOCK_DI_TOKEN, LONG_EXECUTION_HELPER_DI_TOKEN, LongExecutionHelper, Stopwatch } from '../time/public-api';

@injectable()
export class LoggingJsonFetcher implements JsonFetcher {
    constructor(
        @injectExplicit() private readonly nextFetcher: JsonFetcher,
        @inject(CLOCK_DI_TOKEN) private readonly clock: Clock,
        @injectLogger('JsonFetcher') private readonly logger: Logger,
        @inject(LONG_EXECUTION_HELPER_DI_TOKEN) private readonly longExecutionHelper: LongExecutionHelper,
    ) {}

    async execute<TResponse>(command: JsonFetchCommand<TResponse>): Promise<TResponse> {
        const httpMethod = command.requestOptions?.method ?? httpMethods.GET;
        const commonLogData: LogData = {
            command: command.description,
            method: httpMethod,
            url: command.url,
        };
        this.logger.logDebug('Executing {command} to {method} {url}.', commonLogData);

        const stopwatch = new Stopwatch(this.clock);
        try {
            const result = await this.longExecutionHelper.warn({
                description: `HTTP JSON fetch - ${command.description}`,
                execute: async () => this.nextFetcher.execute(command),
            });

            this.logger.logDebug('Succeeded {command} to {method} {url} after {durationMillis}.', {
                ...commonLogData,
                durationMillis: stopwatch.elapsedMillis,
                result: this.logger.isTraceEnabled ? result : ENABLE_TRACE,
            });
            return result;
        } catch (error) {
            this.logger.logDebug('Failed {command} to {method} {url} after {durationMillis}. {error}.', {
                ...commonLogData,
                error,
                durationMillis: stopwatch.elapsedMillis,
            });
            throw error;
        }
    }
}
