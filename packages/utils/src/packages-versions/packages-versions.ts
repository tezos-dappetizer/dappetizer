import fs from 'fs';
import glob from 'glob';
import { singleton } from 'tsyringe';

const PACKAGES_SELECT_PATTERN = 'node_modules/@tezos-dappetizer/*/package.json';

@singleton()
export class PackagesVersionsProvider {
    readonly versions: Readonly<Record<string, object>>;

    constructor() {
        const fileNames = glob.sync(PACKAGES_SELECT_PATTERN, {});
        const versions: Record<string, object> = {};

        for (const fileName of fileNames) {
            const text = fs.readFileSync(fileName).toString();
            versions[fileName] = JSON.parse(text) as object;
        }

        this.versions = Object.freeze(versions);
        Object.freeze(this);
    }
}
