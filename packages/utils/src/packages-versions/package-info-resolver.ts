import { JSONSchemaForNPMPackageJsonFiles } from '@schemastore/package';
import path from 'path';
import { inject, singleton } from 'tsyringe';

import { injectValue } from '../dependency-injection/public-api';
import { FILE_SYSTEM_DI_TOKEN, FileSystem } from '../file-system/public-api';

export const PACKAGE_JSON = 'package.json';

export interface PackageInfo {
    readonly filePath: string;
    readonly version: string | null;
    readonly name: string | null;
}

/** Best effort algoritm to determine version of an NPM package based on its ID. */
@singleton()
export class PackageInfoResolver {
    constructor(
        @injectValue(require) private readonly nodeJSRequire: NodeJS.Require,
        @inject(FILE_SYSTEM_DI_TOKEN) private readonly fileSystem: FileSystem,
    ) {}

    async resolveInfo(npmModuleId: string): Promise<PackageInfo> {
        const filePath = this.nodeJSRequire.resolve(npmModuleId);
        let dirPath = filePath;

        do {
            dirPath = path.dirname(dirPath);
            const packageJsonText = await this.fileSystem.readFileTextIfExists(path.resolve(dirPath, PACKAGE_JSON));

            if (packageJsonText !== null) {
                const packageJson = JSON.parse(packageJsonText) as JSONSchemaForNPMPackageJsonFiles;
                return {
                    filePath,
                    name: packageJson.name !== npmModuleId ? packageJson.name ?? null : null,
                    version: packageJson.version ?? null,
                };
            }
        } while (dirPath !== path.parse(dirPath).root);

        return { filePath, name: null, version: null };
    }
}
