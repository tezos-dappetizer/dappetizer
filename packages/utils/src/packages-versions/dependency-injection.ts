import { DependencyContainer } from 'tsyringe';

import { PackagesVersionsHttpHandler } from './http-handlers/packages-versions-http-handler';
import { registerHttpHandler } from '../http-server/public-api';

export function registerPackagesVersions(diContainer: DependencyContainer): void {
    // Inject to Dappetizer features:
    registerHttpHandler(diContainer, { useToken: PackagesVersionsHttpHandler });
}
