import * as taquitoRpc from '@taquito/rpc';
import { inject, singleton } from 'tsyringe';

import { TezosNodeConfig } from './tezos-node-config';
import { injectExplicit } from '../dependency-injection/public-api';
import { HealthCheck, HealthCheckResult, HealthStatus } from '../health/public-api';
import { Clock, CLOCK_DI_TOKEN } from '../time/public-api';

@singleton()
export class RpcHealthWatcher implements HealthCheck {
    readonly name = 'TezosRpcWatcher';

    private lastHealthyBlock?: {
        readonly [k: string]: unknown;
        readonly receivedFromRpcOn: Date;
    };

    constructor(
        @injectExplicit() private readonly rpcClient: taquitoRpc.RpcClient,
        @inject(CLOCK_DI_TOKEN) private readonly clock: Clock,
        private readonly config: TezosNodeConfig,
    ) {}

    async checkHealth(): Promise<HealthCheckResult> {
        try {
            const block = await this.rpcClient.getBlockHeader();

            this.lastHealthyBlock = {
                hash: block.hash,
                level: block.level,
                chainId: block.chain_id,
                timestamp: block.timestamp,
                receivedFromRpcOn: this.clock.getNowDate(),
            };
            return {
                status: HealthStatus.Healthy,
                data: {
                    headBlock: this.lastHealthyBlock,
                },
            };
        } catch (error) {
            const healthyBeforeMillis = this.clock.getNowTimestamp() - (this.lastHealthyBlock?.receivedFromRpcOn.getTime() ?? 0);
            const status = healthyBeforeMillis > this.config.health.unhealthyAfterMillis ? HealthStatus.Unhealthy : HealthStatus.Degraded;
            return {
                status,
                data: {
                    error,
                    lastHealthyHeadBlock: this.lastHealthyBlock,
                    healthyBeforeMillis,
                },
            };
        }
    }
}
