/**
 * Configures connection to Tezos node to be used to fetch blockchain data.
 * This is used as a part of {@link "@tezos-dappetizer/database".DappetizerConfigUsingDb}.
 * @example Sample usage (all properties specified explicitly) within `dappetizer.config.ts` file:
 * ```typescript
 * import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';
 *
 * const config: DappetizerConfigUsingDb = {
 *     networks: {
 *         mainnet: {
 *             tezosNode: {
 *                 url: 'https://mainnet-tezos.giganode.io',
 *                 retryDelaysMillis: [50, 500, 1_000, 2_000, 4_000],
 *                 monitor: {
 *                     reconnectIfStuckMillis: 180_000,
 *                     chunkCount: {
 *                         warn: 100,
 *                         fail: 1_000,
 *                     },
 *                 },
 *             },
 *             ... // Other config parts.
 *         },
 *         testnet: {
 *             tezosNode: {
 *                 url: 'https://testnet-tezos.giganode.io',
 *                 retryDelaysMillis: [50, 500, 1_000, 2_000, 4_000],
 *                 monitor: {
 *                     reconnectIfStuckMillis: 180_000,
 *                     chunkCount: {
 *                         warn: 100,
 *                         fail: 1_000,
 *                     },
 *                 },
 *             },
 *             ... // Other config parts.
 *         },
 *     },
 *     ... // Other config parts.
 * };
 *
 * export default config;
 * ```
 * @category Dappetizer Config
 */
export interface TezosNodeDappetizerConfig {
    /** The URL of Tezos node RPC to which the indexer should connect. It must be an absolute URL string. */
    url: string;

    /**
     * The array which configures how many times an RPC request is retried when it fails.
     * An array item is a sleep delay is milliseconds until the retry.
     * @default `[50, 500, 1_000, 2_000, 4_000]`
     */
    retryDelaysMillis?: number[];

    /** Configures connection to Tezos node monitor. */
    monitor?: {
        /**
         * If Tezos node monitor does not send a new item (block or mempool operation group respectively) for specified number of milliseconds,
         * then the connection is considered as stuck and it is restarted.
         * @limits An integer with minimum `1`.
         * @default `180_000` (3 minutes)
         */
        reconnectIfStuckMillis?: number;

        /** Tezos node monitor responds with chunks of incomplete JSON. So they need to be joined. */
        chunkCount?: {
            /**
             * The number of chunks from Tezos node monitor when a warning is logged.
             * @limits An integer with minimum `1`.
             * @default `100`
             */
            warn?: number;

            /**
             * The number of chunks from Tezos node monitor when it fails and the connection is reestablished.
             * @limits An integer with minimum `1`.
             * @default `1_000`
             */
            fail?: number;
        };
    };

    /**
     * Configures reporting of health status of the Tezos node connection.
     * Tezos node watcher gets HEAD block header and reports respective error if any.
     */
    health?: {
        /** @deprecated Use {@link resultCacheMillis} instead. */
        watcherCacheMillis?: number;

        /**
         * The time in milliseconds for how long the health result (of all {@link "@tezos-dappetizer/utils".HealthStatus}-es) is cached.
         * @limits An integer with minimum `0`.
         * @default `5_000`
         */
        resultCacheMillis?: number;

        /**
         * The time in milliseconds to wait before a health RPC request times out.
         * @limits An integer with minimum `1`.
         * @default `1_000`
         */
        timeoutMillis?: number;

        /**
         * The time in milliseconds after which the check reports {@link "@tezos-dappetizer/utils".HealthStatus.Unhealthy}
         * if health RPC requests keep failing subsequently.
         * Before that, the check reports {@link "@tezos-dappetizer/utils".HealthStatus.Degraded}.
         * A single successful health RPC request turns the check back to {@link "@tezos-dappetizer/utils".HealthStatus.Healthy}.
         * @limits An integer with minimum `0`.
         * @default `70_000`
         */
        unhealthyAfterMillis?: number;
    };

    /**
     * The time in milliseconds to wait before an RPC request times out.
     * Some requests may override it explicitly in the code.
     * @limits An integer with minimum `1`.
     * @default `30_000`
     */
    timeoutMillis?: number;
}
