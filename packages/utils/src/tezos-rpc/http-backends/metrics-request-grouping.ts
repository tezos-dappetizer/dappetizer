import { singleton } from 'tsyringe';
import { URL } from 'url';

import { injectLogger, Logger } from '../../logging/public-api';

export const DEFAULT_GROUP_NAME = 'default';

export const metricGroups = Object.freeze([
    '/chains/main/blocks/head/context/contracts',
    '/chains/main/blocks/head/context/big_maps',
    '/chains/main/blocks/head/helpers/scripts',
    '/chains/main/blocks/head/header',
    '/chains/main/blocks',
    '/chains/main/chain_id',
] as const);

export interface EndpointGroup {
    readonly name: string;
    readonly host: string;
}

@singleton()
export class MetricsRequestGrouping {
    constructor(
        @injectLogger(MetricsRequestGrouping) private readonly logger: Logger,
    ) {}

    getGroup(url: string): EndpointGroup {
        const typedUrl = new URL(url);
        for (const group of metricGroups) {
            if (typedUrl.pathname.includes(group)) {
                return {
                    name: `${group}${typedUrl.pathname !== group ? '/*' : ''}`,
                    host: typedUrl.host,
                };
            }
        }

        this.logger.logWarning('Endpoint group was not recognized for {url}, using {defaultGroupName}.', {
            url,
            defaultGroupName: DEFAULT_GROUP_NAME,
        });
        return {
            name: DEFAULT_GROUP_NAME,
            host: typedUrl.host,
        };
    }
}
