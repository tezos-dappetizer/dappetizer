import { inject, injectable } from 'tsyringe';

import { AbortControllerFactory } from '../../app-control/abort-controller-factory';
import { APP_SHUTDOWN_MANAGER_DI_TOKEN, AppShutdownManager } from '../../app-control/public-api';
import { injectExplicit } from '../../dependency-injection/public-api';
import { TaquitoHttpBackend } from '../http-infrastructure/taquito-http-backend';
import { TaquitoHttpRequest } from '../http-infrastructure/taquito-http-request';

@injectable()
export class AppShutdownHttpBackend extends TaquitoHttpBackend {
    constructor(
        @injectExplicit() private readonly nextBackend: TaquitoHttpBackend,
        @inject(APP_SHUTDOWN_MANAGER_DI_TOKEN) private readonly appShutdownManager: AppShutdownManager,
        private readonly abortControllerFactory: AbortControllerFactory,
    ) {
        super();
    }

    override async execute(request: TaquitoHttpRequest): Promise<unknown> {
        if (!request.signal) {
            return this.nextBackend.execute({
                ...request,
                signal: this.appShutdownManager.signal,
            });
        }

        const controller = this.abortControllerFactory.createLinked(request.signal, this.appShutdownManager.signal);
        return this.nextBackend.execute({
            ...request,
            signal: controller.signal,
        });
    }
}
