import { injectable } from 'tsyringe';

import { isNullish } from '../../basics/public-api';
import { injectExplicit } from '../../dependency-injection/public-api';
import { TaquitoHttpBackend } from '../http-infrastructure/taquito-http-backend';
import { TaquitoHttpRequest } from '../http-infrastructure/taquito-http-request';

/** Decorator that sets the explicit timeout to HTTP requests. It honors previously set timeout. */
@injectable()
export class TimeoutHttpBackend extends TaquitoHttpBackend {
    constructor(
        private readonly timeoutMillis: number,
        @injectExplicit() private readonly nextBackend: TaquitoHttpBackend,
    ) {
        super();
        Object.freeze(this);
    }

    override async execute(request: TaquitoHttpRequest): Promise<unknown> {
        const requestWithTimeout: TaquitoHttpRequest = isNullish(request.timeout)
            ? { ...request, timeout: this.timeoutMillis }
            : request;

        return this.nextBackend.execute(requestWithTimeout);
    }
}
