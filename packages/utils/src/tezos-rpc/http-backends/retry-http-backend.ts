import * as taquitoHttp from '@taquito/http-utils';
import { inject, injectable } from 'tsyringe';

import { MetricsRequestGrouping } from './metrics-request-grouping';
import { isNullish } from '../../basics/public-api';
import { injectExplicit } from '../../dependency-injection/public-api';
import { injectLogger, Logger } from '../../logging/public-api';
import { SLEEP_HELPER_DI_TOKEN, SleepHelper } from '../../time/public-api';
import { TaquitoHttpBackend } from '../http-infrastructure/taquito-http-backend';
import { TaquitoHttpRequest } from '../http-infrastructure/taquito-http-request';
import { getRequestDescription } from '../http-infrastructure/taquito-http-utils';
import { RpcMetrics } from '../rpc-metrics';
import { TezosNodeConfig } from '../tezos-node-config';

const blockByLevelRegex = /\/chains\/main\/blocks\/\d+$/u;

@injectable()
export class RetryHttpBackend extends TaquitoHttpBackend {
    constructor(
        @injectExplicit() private readonly nextBackend: TaquitoHttpBackend,
        private readonly config: TezosNodeConfig,
        @inject(SLEEP_HELPER_DI_TOKEN) private readonly sleepHelper: SleepHelper,
        private readonly metrics: RpcMetrics,
        private readonly metricsRequestGrouping: MetricsRequestGrouping,
        @injectLogger(TaquitoHttpBackend) private readonly logger: Logger,
    ) {
        super();
    }

    override async execute(request: TaquitoHttpRequest, delayIndex = 0): Promise<unknown> {
        try {
            return await this.nextBackend.execute(request);
        } catch (error) {
            // GET block by level should be retried.
            if (isHttpNotFoundResponse(error) && !blockByLevelRegex.test(request.url)) {
                this.logger.logDebug('The {request} is not retried because it is 404 Not Found.', {
                    request: getRequestDescription(request),
                });
                throw error;
            }

            const delayMillis = this.config.retryDelaysMillis[delayIndex];
            if (isNullish(delayMillis)) {
                this.logger.logDebug('Failed {request} again after {retries} hence failing for real.', {
                    request: getRequestDescription(request),
                    retries: delayIndex,
                });
                throw error;
            }

            this.logger.logWarning('Retrying {request} for {index} (+1). time with {delayMillis}.'
                + ' If this happens often, consider using different Tezos Node. {error}', {
                request: getRequestDescription(request),
                index: delayIndex,
                delayMillis,
                error,
            });

            this.metrics.rpcClientRetryCount.inc({
                ...this.metricsRequestGrouping.getGroup(request.url),
                delay: delayMillis,
                index: delayIndex,
            });

            await this.sleepHelper.sleep(delayMillis);
            return await this.execute(request, delayIndex + 1);
        }
    }
}

function isHttpNotFoundResponse(error: unknown): boolean {
    return error instanceof taquitoHttp.HttpResponseError
        && error.status === taquitoHttp.STATUS_CODE.NOT_FOUND;
}
