import { inject, injectable } from 'tsyringe';

import { MetricsRequestGrouping } from './metrics-request-grouping';
import { injectExplicit } from '../../dependency-injection/public-api';
import { Clock, CLOCK_DI_TOKEN } from '../../time/public-api';
import { TaquitoHttpBackend } from '../http-infrastructure/taquito-http-backend';
import { TaquitoHttpRequest } from '../http-infrastructure/taquito-http-request';
import { RpcMetrics } from '../rpc-metrics';

@injectable()
export class MetricsHttpBackend extends TaquitoHttpBackend {
    constructor(
        @injectExplicit() private readonly nextBackend: TaquitoHttpBackend,
        private readonly metrics: RpcMetrics,
        @inject(CLOCK_DI_TOKEN) private readonly clock: Clock,
        private readonly metricsRequestGrouping: MetricsRequestGrouping,
    ) {
        super();
    }

    override async execute(request: TaquitoHttpRequest): Promise<unknown> {
        const startTimestamp = this.clock.getNowTimestamp();

        const result = await this.nextBackend.execute(request);

        const duration = this.clock.getNowTimestamp() - startTimestamp;
        const group = this.metricsRequestGrouping.getGroup(request.url);
        this.metrics.rpcClientCallDuration.observe(group, duration);

        return result;
    }
}
