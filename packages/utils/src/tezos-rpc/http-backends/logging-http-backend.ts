import { inject, injectable } from 'tsyringe';

import { injectExplicit } from '../../dependency-injection/public-api';
import { ENABLE_TRACE, injectLogger, Logger } from '../../logging/public-api';
import { LONG_EXECUTION_HELPER_DI_TOKEN, LongExecutionHelper } from '../../time/long-execution-helper';
import { Clock, CLOCK_DI_TOKEN, Stopwatch } from '../../time/public-api';
import { TaquitoHttpBackend } from '../http-infrastructure/taquito-http-backend';
import { TaquitoHttpRequest } from '../http-infrastructure/taquito-http-request';
import { getRequestDescription } from '../http-infrastructure/taquito-http-utils';

@injectable()
export class LoggingHttpBackend extends TaquitoHttpBackend {
    constructor(
        @injectExplicit() private readonly nextBackend: TaquitoHttpBackend,
        @inject(CLOCK_DI_TOKEN) private readonly clock: Clock,
        @injectLogger(TaquitoHttpBackend) private readonly logger: Logger,
        @inject(LONG_EXECUTION_HELPER_DI_TOKEN) private readonly longExecutionHelper: LongExecutionHelper,
    ) {
        super();
    }

    override async execute(request: TaquitoHttpRequest): Promise<unknown> {
        this.logger.logDebug('Executing {request}.', {
            request: getRequestDescription(request),
            requestData: this.logger.isTraceEnabled ? request.data : ENABLE_TRACE,
        });

        const stopwatch = new Stopwatch(this.clock);
        try {
            const result = await this.longExecutionHelper.warn({
                description: getRequestDescription(request),
                execute: async () => this.nextBackend.execute(request),
            });

            this.logger.logDebug('Succeeded {request} after {durationMillis}.', {
                request: getRequestDescription(request),
                durationMillis: stopwatch.elapsedMillis,
                result: this.logger.isTraceEnabled ? result : ENABLE_TRACE,
            });
            return result;
        } catch (error) {
            this.logger.logDebug('Failed {request} after {durationMillis}. {error}.', {
                request: getRequestDescription(request),
                error,
                durationMillis: stopwatch.elapsedMillis,
            });
            throw error;
        }
    }
}
