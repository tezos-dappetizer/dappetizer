import { inject, singleton } from 'tsyringe';

import {
    ConfigObjectElement,
    ConfigWithDiagnostics,
    NETWORK_SPECIFIC_CONFIG_ELEMENT_DI_TOKEN,
} from '../configuration/public-api';

export interface TezosMonitorConfig {
    readonly chunkCount: TezosMonitorChunkCountConfig;
    readonly reconnectIfStuckMillis: number;
}

export interface TezosMonitorChunkCountConfig {
    readonly warn: number;
    readonly fail: number;
}

export interface TezosNodeHealthConfig {
    readonly resultCacheMillis: number;
    readonly timeoutMillis: number;
    readonly unhealthyAfterMillis: number;
}

@singleton()
export class TezosNodeConfig implements ConfigWithDiagnostics {
    static readonly ROOT_NAME = 'tezosNode';

    readonly url: string;
    readonly retryDelaysMillis: readonly number[];
    readonly monitor: TezosMonitorConfig;
    readonly health: TezosNodeHealthConfig;
    readonly timeoutMillis: number;

    constructor(@inject(NETWORK_SPECIFIC_CONFIG_ELEMENT_DI_TOKEN) rootElement: ConfigObjectElement) {
        const thisElement = rootElement.get(TezosNodeConfig.ROOT_NAME).asObject<keyof TezosNodeConfig>();

        this.url = thisElement.get('url').asAbsoluteUrl();
        this.retryDelaysMillis = thisElement.getOptional('retryDelaysMillis')?.asArray().map(c => c.asInteger({ min: 0 }))
            ?? [50, 500, 1_000, 2_000, 4_000];
        this.timeoutMillis = thisElement.getOptional('timeoutMillis')?.asInteger({ min: 1 })
            ?? 30_000;

        const monitorElement = thisElement.getOptional('monitor')?.asObject<keyof TezosMonitorConfig>();
        const monitorChunkCountElement = monitorElement?.getOptional('chunkCount')?.asObject<keyof TezosMonitorChunkCountConfig>();
        this.monitor = {
            chunkCount: {
                warn: monitorChunkCountElement?.getOptional('warn')?.asInteger({ min: 1 })
                    ?? 100,
                fail: monitorChunkCountElement?.getOptional('fail')?.asInteger({ min: 1 })
                    ?? 1_000,
            },
            reconnectIfStuckMillis: monitorElement?.getOptional('reconnectIfStuckMillis')?.asInteger({ min: 1 })
                ?? 3 * 60_000,
        };

        const healthElement = thisElement.getOptional('health')?.asObject<keyof TezosNodeHealthConfig>();
        this.health = {
            resultCacheMillis: healthElement?.getOptional('resultCacheMillis')?.asInteger({ min: 0 })
                ?? (healthElement as ConfigObjectElement | null)?.getOptional('watcherCacheMillis')?.asInteger({ min: 0 })
                ?? 5_000,
            timeoutMillis: healthElement?.getOptional('timeoutMillis')?.asInteger({ min: 1 })
                ?? 1_000,
            unhealthyAfterMillis: healthElement?.getOptional('unhealthyAfterMillis')?.asInteger({ min: 0 })
                ?? 70_000,
        };
    }

    getDiagnostics(): [string, unknown] {
        return [TezosNodeConfig.ROOT_NAME, this];
    }
}
