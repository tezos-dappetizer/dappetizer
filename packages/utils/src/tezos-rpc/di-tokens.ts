import * as taquitoRpc from '@taquito/rpc';
import { InjectionToken } from 'tsyringe';

import { TaquitoHttpBackend } from './http-infrastructure/taquito-http-backend';

export const TAQUITO_RPC_CLIENT_DI_TOKEN: InjectionToken<taquitoRpc.RpcClient> = 'Dappetizer:Utils:taquitoRpc.RpcClient';

export const TAQUITO_HTTP_BACKEND_DI_TOKEN: InjectionToken<TaquitoHttpBackend> = 'Dappetizer:Utils:TaquitoHttpBackend';
