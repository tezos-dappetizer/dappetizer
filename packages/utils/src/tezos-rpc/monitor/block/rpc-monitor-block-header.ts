/** Data downloaded from Tezos node block monitor. */
export interface RpcMonitorBlockHeader {
    hash: string;
    level: number;
    proto: number;
    predecessor: string;
    timestamp: string;
    validation_pass: number;
    operations_hash: string;
    fitness: string[];
    context: string;
    protocol_data: string;
}
