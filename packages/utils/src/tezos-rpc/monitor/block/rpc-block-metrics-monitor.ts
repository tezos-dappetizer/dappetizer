import { injectable } from 'tsyringe';

import { RpcMonitorBlockHeader } from './rpc-monitor-block-header';
import { AsyncIterableProvider } from '../../../basics/public-api';
import { injectExplicit } from '../../../dependency-injection/public-api';
import { RpcMetrics } from '../../rpc-metrics';

@injectable()
export class RpcBlockMetricsMonitor implements AsyncIterableProvider<RpcMonitorBlockHeader> {
    constructor(
        @injectExplicit() private readonly nextProvider: AsyncIterableProvider<RpcMonitorBlockHeader>,
        private readonly metrics: RpcMetrics,
    ) {}

    async *iterate(): AsyncIterableIterator<RpcMonitorBlockHeader> {
        for await (const item of this.nextProvider.iterate()) {
            this.metrics.monitorBlockLevel.set(item.level);
            yield item;
        }
    }
}
