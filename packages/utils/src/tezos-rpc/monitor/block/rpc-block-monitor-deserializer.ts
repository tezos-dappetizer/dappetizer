import { singleton } from 'tsyringe';

import { RpcMonitorBlockHeader } from './rpc-monitor-block-header';
import { isObjectLiteral, isWhiteSpace } from '../../../basics/public-api';
import { ItemsDeserializer } from '../generic/deserialize-data-monitor';

/** Downloads blocks and casts them strictly. */
@singleton()
export class RpcBlockMonitorDeserializer implements ItemsDeserializer<RpcMonitorBlockHeader> {
    deserialize(data: unknown): RpcMonitorBlockHeader[] {
        if (!isObjectLiteral(data)) {
            throw new Error('It must be an object.');
        }
        if (typeof data.hash !== 'string' || isWhiteSpace(data.hash)) {
            throw new Error(`Missing non-empty 'hash' property.`);
        }
        return [data as unknown as RpcMonitorBlockHeader];
    }
}
