import { singleton } from 'tsyringe';

import { RpcMempoolOperationGroup } from './rpc-mempool-operation-group';
import { isObjectLiteral, isWhiteSpace, withIndex } from '../../../basics/public-api';
import { ItemsDeserializer } from '../generic/deserialize-data-monitor';

/** Downloads mempool operation groups and casts them strictly. */
@singleton()
export class RpcMempoolMonitorDeserializer implements ItemsDeserializer<RpcMempoolOperationGroup> {
    deserialize(data: unknown): RpcMempoolOperationGroup[] {
        if (!Array.isArray(data)) {
            throw new Error('It must be an array.');
        }
        for (const [item, index] of withIndex(data)) {
            if (!isObjectLiteral(item)) {
                throw new Error(`All items in the array must be objects but ${index}. item is not.`);
            }
            if (typeof item.signature !== 'string' || isWhiteSpace(item.signature)) {
                throw new Error(`All items in the array must have non-empty 'signature' property objects but ${index}. item does not.`);
            }
        }
        return data as RpcMempoolOperationGroup[];
    }
}
