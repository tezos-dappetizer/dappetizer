import { InjectionToken } from 'tsyringe';

import { RpcMempoolOperationGroup } from './rpc-mempool-operation-group';
import { AsyncIterableProvider } from '../../../basics/public-api';

export type RpcMempoolMonitor = AsyncIterableProvider<RpcMempoolOperationGroup>;

export const RPC_MEMPOOL_MONITOR_DI_TOKEN: InjectionToken<RpcMempoolMonitor> = 'Dappetizer:Utils:RpcMempoolMonitor';
