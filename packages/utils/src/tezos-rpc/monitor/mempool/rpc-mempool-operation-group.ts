import * as taquitoRpc from '@taquito/rpc';

/** Data downloaded from Tezos node mempool monitor. */
export interface RpcMempoolOperationGroup {
    protocol: string;
    branch: string;
    signature: string;

    // Slightly adapted to make it compatible with block operation groups -> common handling is easier.
    contents: (taquitoRpc.OperationContents | taquitoRpc.OperationContentsAndResult)[];
    chain_id?: undefined;
    hash?: undefined;
}
