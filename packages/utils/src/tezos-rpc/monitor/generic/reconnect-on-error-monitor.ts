import { inject, injectable } from 'tsyringe';

import { AsyncIterableProvider } from '../../../basics/public-api';
import { injectExplicit } from '../../../dependency-injection/public-api';
import { ComponentHealthState } from '../../../health/checks/component-health-state';
import { HealthStatus } from '../../../health/public-api';
import { Logger, LogLevel } from '../../../logging/public-api';
import { SLEEP_HELPER_DI_TOKEN, SleepHelper } from '../../../time/public-api';
import { RpcMetrics } from '../../rpc-metrics';
import { TezosNodeConfig } from '../../tezos-node-config';

@injectable()
export class ReconnectOnErrorMonitor<TItem> implements AsyncIterableProvider<TItem> {
    private delayIndex = -1;

    constructor(
        @injectExplicit(0) private readonly healthState: ComponentHealthState,
        @injectExplicit(1) private readonly logger: Logger,
        @injectExplicit(2) private readonly nextProvider: AsyncIterableProvider<TItem>,
        private readonly config: TezosNodeConfig,
        @inject(SLEEP_HELPER_DI_TOKEN) private readonly sleepHelper: SleepHelper,
        private readonly metrics: RpcMetrics,
    ) {}

    async *iterate(): AsyncIterableIterator<TItem> {
        let hasEnded = false;
        while (!hasEnded) {
            try {
                for await (const item of this.nextProvider.iterate()) {
                    this.setHealthy();
                    yield item;
                }
                hasEnded = true;
            } catch (error) {
                this.delayIndex++;
                let delay = this.config.retryDelaysMillis[this.delayIndex];

                if (delay === undefined) {
                    this.delayIndex--;
                    delay = this.config.retryDelaysMillis[this.delayIndex] ?? 0;
                    this.setError(HealthStatus.Unhealthy, error, LogLevel.Error, healthMessages.persistentFailure);
                    this.metrics.monitorInfiniteLoopCount.inc();
                } else {
                    this.setError(HealthStatus.Degraded, error, LogLevel.Debug, healthMessages.temporaryFailure);
                    this.metrics.monitorReconnectCount.inc();
                }

                this.metrics.monitorRetryCount.inc({
                    name: this.healthState.name,
                    host: this.config.url,
                    delay,
                    index: this.delayIndex,
                });

                await this.sleepHelper.sleep(delay);
            }
        }
    }

    private setHealthy(): void {
        this.healthState.set(HealthStatus.Healthy, healthMessages.healthy);
        this.delayIndex = -1;
    }

    private setError(status: HealthStatus, error: unknown, logLevel: LogLevel, reason: string): void {
        this.logger.log(logLevel, `${reason} {error}`, { error });
        this.healthState.set(status, { reason, error });
    }
}

export const healthMessages = {
    healthy: 'Everything is fine.',
    temporaryFailure: 'Monitoring temporarily failed. It will reconnect.',
    persistentFailure: 'Monitoring failed multiple times. Now trying with the last delay indefinitely.',
} as const;
