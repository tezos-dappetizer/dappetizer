import { inject, injectable } from 'tsyringe';

import { isAbortError } from '../../../app-control/public-api';
import { appendUrl, errorToString } from '../../../basics/public-api';
import { injectExplicit } from '../../../dependency-injection/public-api';
import { HTTP_FETCHER_DI_TOKEN, HttpFetcher, httpStatusCodes } from '../../../http-fetch/public-api';
import { Logger } from '../../../logging/public-api';
import { TezosNodeConfig } from '../../tezos-node-config';

/** Dowloands raw JSON from RPC monitor of a Tezos node.  */
@injectable()
export class DownloadDataMonitor {
    private readonly url: string;

    constructor(
        @injectExplicit(0) private readonly logger: Logger,
        @injectExplicit(1) relativeUrl: string,
        private readonly config: TezosNodeConfig,
        @inject(HTTP_FETCHER_DI_TOKEN) private readonly httpFetcher: HttpFetcher,
    ) {
        this.url = appendUrl(config.url, relativeUrl);
    }

    async *iterate(abortSignal: AbortSignal): AsyncIterableIterator<unknown> {
        try {
            const responseBody = await this.connect(abortSignal);

            // Tezos node splits long JSON (esp. mempool) to smaller chunks -> invalid JSON -> concat and try to parse it.
            let incompleteChunkCount = 0;
            let dataStr = '';

            for await (const chunk of responseBody) {
                try {
                    const chunkStr = typeof chunk !== 'string' ? chunk.toString() : chunk;
                    dataStr = dataStr.length ? dataStr + chunkStr : chunkStr; // Avoids string copy for '' + 'abc'.

                    const item: unknown = JSON.parse(dataStr);

                    this.onChunksCompleted(incompleteChunkCount + 1);
                    dataStr = '';
                    incompleteChunkCount = 0;

                    yield item;
                } catch (error) {
                    incompleteChunkCount++;
                    this.processError(error, incompleteChunkCount);
                }
            }
            throw new Error('Connection ended which should not happen for a monitor.');
        } catch (error) {
            if (isAbortError(error)) {
                this.logger.logDebug('Monitoring aborted.');
                return;
            }

            throw error;
        }
    }

    private async connect(abortSignal: AbortSignal): Promise<NodeJS.ReadableStream> {
        this.logger.logInformation('Connecting to {url}.', { url: this.url });
        const response = await this.httpFetcher.fetch(this.url, { signal: abortSignal });

        if (response.status !== httpStatusCodes.OK) {
            throw new Error(`Failed to connect to ${this.url} because of ${response.status} ${response.statusText}`);
        }

        this.logger.logInformation('Connected to {url}. Monitoring it for new chunks.', { url: this.url });
        return response.body;
    }

    private onChunksCompleted(chunkCount: number): void {
        if (chunkCount >= this.config.monitor.chunkCount.warn) {
            this.logger.logWarning('Finally received a valid JSON from {url} within {chunkCount}.', {
                url: this.url,
                chunkCount,
            });
        }
    }

    private processError(error: unknown, incompleteChunkCount: number): void {
        if (incompleteChunkCount >= this.config.monitor.chunkCount.fail) {
            throw new Error(
                `Received too many ${incompleteChunkCount} chunks from ${this.url}`
                + ` which still don't produce a valid JSON. ${errorToString(error)}`,
            );
        }
    }
}
