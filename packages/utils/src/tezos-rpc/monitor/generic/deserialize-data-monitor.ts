import { AsyncIterableProvider, NonNullish } from '../../../basics/public-api';
import { Logger } from '../../../logging/public-api';

export interface ItemsDeserializer<TItem extends NonNullish> {
    deserialize(data: unknown): TItem[];
}

/** Deserializes (casts) downloaded data strictly. */
export class DeserializeDataMonitor<TItem extends NonNullish> implements AsyncIterableProvider<TItem> {
    constructor(
        private readonly logger: Logger,
        private readonly deserializer: ItemsDeserializer<TItem>,
        private readonly nextProvider: AsyncIterableProvider<unknown>,
    ) {}

    async *iterate(): AsyncIterableIterator<TItem> {
        for await (const data of this.nextProvider.iterate()) {
            try {
                const items = this.deserializer.deserialize(data);
                yield* items;
            } catch (error) {
                this.logger.logError('Downloaded data is invalid. Related processing will be skipped. {error} {data}', { error, data });
            }
        }
    }
}
