import { TaquitoHttpRequest } from './taquito-http-request';

export function getRequestDescription(request: TaquitoHttpRequest): string {
    return `${request.method} ${request.url}`;
}
