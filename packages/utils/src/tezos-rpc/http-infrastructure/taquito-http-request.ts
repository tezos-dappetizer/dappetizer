// Breaking:
// - make readonly
// - json -> isJson
// - signal -> abortSignal?
// - timeout -> timeoutMillis
export interface TaquitoHttpRequest {
    data: object | string | undefined;
    headers: Record<string, string>;
    json: boolean;
    method: 'GET' | 'POST';
    signal: AbortSignal | undefined;
    timeout: number | undefined;
    url: string;
}
