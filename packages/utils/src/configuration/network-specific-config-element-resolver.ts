import { inject, singleton } from 'tsyringe';

import { ConfigObjectElement } from './config-element';
import { DAPPETIZER_CONFIG_FROM } from './config-utils';
import { CONFIG_NETWORK_DI_TOKEN, ROOT_CONFIG_ELEMENT_DI_TOKEN } from './di-tokens';
import { dumpToString, dumpType, getVariableName, isNullish, isWhiteSpace } from '../basics/public-api';
import { CLI_SCRIPT, cliCommands, fullCliOption } from '../cli/public-api';

export const ROOT_NAME = 'networks';

@singleton()
export class NetworkSpecificConfigElementResolver {
    constructor(
        @inject(ROOT_CONFIG_ELEMENT_DI_TOKEN) private readonly rootElement: ConfigObjectElement,
        @inject(CONFIG_NETWORK_DI_TOKEN) private readonly network: string,
    ) {
        if (typeof network !== 'string' || isWhiteSpace(network)) {
            throw new Error(`Network name injected via ${getVariableName({ CONFIG_NETWORK_DI_TOKEN })} must be a string`
                + ` but it is ${JSON.stringify(network)} of type ${dumpType(network)}.`);
        }
    }

    resolve(): ConfigObjectElement {
        const allNetworksElement = this.rootElement.get(ROOT_NAME).asObject();
        const networkElement = allNetworksElement.get(this.network);

        if (isNullish(networkElement.value)) {
            const configuredNetworks = Object.entries(allNetworksElement.value)
                .filter(([_, config]) => !isNullish(config))
                .map(([name]) => name);

            throw new Error(`${DAPPETIZER_CONFIG_FROM} '${networkElement.filePath}' is missing section for network '${this.network}'`
                + ` (property path '${networkElement.propertyPath}'). Configured networks are: ${dumpToString(configuredNetworks)}.`
                + ` It can be specified using '${fullCliOption('NETWORK')}' option for '${CLI_SCRIPT} ${cliCommands.START}' command.`);
        }
        return networkElement.asObject();
    }
}
