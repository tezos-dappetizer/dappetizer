import { StrictOmit } from 'ts-essentials';
import { URL } from 'url';

import { ConfigError } from './config-error';
import {
    AddressPrefix,
    addressValidator,
    argGuard,
    ArrayLimits,
    arrayValidator,
    errorToString,
    getConstructorName,
    getEnumValues,
    isNullish,
    isObjectLiteral,
    NumberLimits,
    numberValidator,
    oneOfSupportedValidator,
    StringLimits,
    stringValidator,
    Validator,
} from '../basics/public-api';
import { typeValidator } from '../basics/validators/type-validator';

/** Common logic for {@link ConfigElement} and {@link ConfigObjectElement}. */
export abstract class BaseConfigElement<TValue = unknown> {
    constructor(
        readonly value: TValue,
        readonly filePath: string,
        readonly propertyPath: string,
    ) {
        argGuard.nonWhiteSpaceString(filePath, 'filePath');
        argGuard.nonWhiteSpaceString(propertyPath, 'propertyPath');

        Object.freeze(this);
    }
}

/**
 * Generic object element of the configuration from JSON file.
 * DI injects the root config element.
 * @template TProperty The helper for strongly-typed access to config properties according to target class.
 */
export class ConfigObjectElement<TProperty extends string = string> extends BaseConfigElement<Readonly<Record<string, unknown>>> {
    constructor(value: Readonly<Record<string, unknown>>, filePath: string, propertyPath: string) {
        super(argGuard.object(value, 'value'), filePath, propertyPath);
    }

    get(propertyName: TProperty): ConfigElement {
        argGuard.nonWhiteSpaceString(propertyName, 'propertyName');

        const propertyValue = this.value[propertyName];
        return new ConfigElement(propertyValue, this.filePath, `${this.propertyPath}.${propertyName}`);
    }

    getOptional(propertyName: TProperty): ConfigElement | null {
        const property = this.get(propertyName);
        return !isNullish(property.value) ? property : null;
    }
}

const configValidators = {
    address: addressValidator,
    array: arrayValidator,
    number: numberValidator,
    oneOfSupported: oneOfSupportedValidator,
    string: stringValidator,
    type: typeValidator,
};

/** Generic untyped element of the configuration from JSON file. */
export class ConfigElement extends BaseConfigElement {
    readonly #validators: typeof configValidators; // JS private fields (using #) b/c the class is exposed as public API.

    /** @param validators Keep it as it is - intended only for **internal** use. */
    constructor(value: unknown, filePath: string, propertyPath: string, validators = configValidators) {
        super(value, filePath, propertyPath);
        this.#validators = validators;
    }

    asAbsoluteUrl(): string {
        try {
            const str = this.#validators.string.validate(this.value, 'NotWhiteSpace');
            try {
                // Return raw string b/c URL is mutable, cannot be frozen and string is good enough.
                return new URL(str).toString();
            } catch {
                throw new Error('It is NOT an absolute URL.');
            }
        } catch (error) {
            this.#throwError({ error, mustBe: 'an absolute URL string' });
        }
    }

    asAddress(prefixes?: readonly AddressPrefix[]): string {
        return this.#validateUsing(this.#validators.address, prefixes);
    }

    asArray(limits?: ArrayLimits): ConfigElement[] {
        const array = this.#validateUsing(this.#validators.array, limits);
        return array.map((item, index) => new ConfigElement(item, this.filePath, `${this.propertyPath}[${index}]`));
    }

    asBoolean(): boolean {
        return this.#validateUsing(this.#validators.type, 'boolean') as boolean;
    }

    asEnum<T extends string>(enumType: Record<string, T>): T {
        return this.asOneOf(getEnumValues(enumType));
    }

    asInteger(limits: StrictOmit<NumberLimits, 'integer'> = {}): number {
        return this.#validateUsing(this.#validators.number, { ...limits, integer: true });
    }

    asObject<TKey extends string>(): ConfigObjectElement<TKey> {
        try {
            const value = this.#validators.type.validate(this.value, 'object');

            if (!isObjectLiteral(value)) {
                throw new Error(`It cannot be custom type ${getConstructorName(value)}.`);
            }
            return new ConfigObjectElement(value, this.filePath, this.propertyPath);
        } catch (error) {
            this.#throwError({ error, mustBe: 'an object literal' });
        }
    }

    asOneOf<T extends string>(supportedValues: readonly T[]): T {
        return this.#validateUsing(this.#validators.oneOfSupported, supportedValues) as T;
    }

    asString(limits?: StringLimits): string {
        return this.#validateUsing(this.#validators.string, limits);
    }

    #validateUsing<TValue, TLimits>(validator: Validator<TValue, TLimits>, limits: TLimits): TValue {
        try {
            return validator.validate(this.value, limits);
        } catch (error) {
            this.#throwError({ error, mustBe: validator.getExpectedValueDescription(limits) });
        }
    }

    #throwError(options: { error: unknown; mustBe: string }): never {
        const reason = `The value must be ${options.mustBe}. ${errorToString(options.error, { onlyMessage: true })}`;
        throw new ConfigError({ ...this, reason });
    }
}
