import { DAPPETIZER_CONFIG_FROM } from './config-utils';
import { argGuard, dumpType } from '../basics/public-api';

/**
 * An error in the app configuration which exposes all details of originating {@link ConfigElement} or {@link ConfigObjectElement}.
 * @example Throw an error using values from originating config element:
 * ```typescript
 * throw new ConfigError({
 *     ...configElement,
 *     reason: `The value must be 'Foo' or 'Bar' but it is 'WTF'`,
 * });
 * ```
 */
export class ConfigError extends Error {
    readonly value: unknown;
    readonly filePath: string;
    readonly propertyPath: string;
    readonly reason: string;

    /**
     * @example Creation from {@link ConfigElement} directly:
     * ```typescript
     * throw new ConfigError({ ...element, reason: 'It is foo bar.' });
     * ```
     */
    constructor(options: {
        readonly value: unknown;
        readonly filePath: string;
        readonly propertyPath: string;
        readonly reason: string;
    }) {
        argGuard.object(options, 'options');
        argGuard.nonWhiteSpaceString(options.filePath, 'options.filePath');
        argGuard.nonWhiteSpaceString(options.propertyPath, 'options.propertyPath');
        argGuard.nonWhiteSpaceString(options.reason, 'options.reason');

        super(`${DAPPETIZER_CONFIG_FROM} '${options.filePath}' at property path '${options.propertyPath}'`
            + ` has invalid value: ${JSON.stringify(options.value)} of type ${dumpType(options.value)}.`
            + ` ${options.reason}`);

        this.value = options.value;
        this.filePath = options.filePath;
        this.propertyPath = options.propertyPath;
        this.reason = options.reason;

        Object.freeze(this);
    }
}
