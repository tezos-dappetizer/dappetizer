import { DependencyContainer, Provider } from 'tsyringe';

import { CONFIGS_WITH_DIAGNOSTIC_DI_TOKEN } from './config-diagnostics-di-token';
import { registerSingleton } from '../dependency-injection/public-api';

export interface ConfigWithDiagnostics {
    /** Should return config name usually corresponding to its name within the root config JSON and the actual config value. */
    getDiagnostics(): [string, unknown];
}

/**
 * Registers a config to exposed actually configured values at `/config` endpoint.
 * It is registered as **singleton**.
 */
export function registerConfigWithDiagnostics(diContainer: DependencyContainer, configProvider: Provider<ConfigWithDiagnostics>): void {
    registerSingleton(diContainer, CONFIGS_WITH_DIAGNOSTIC_DI_TOKEN, configProvider);
}
