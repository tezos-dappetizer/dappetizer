import { orderBy } from 'lodash';
import { injectAll, singleton } from 'tsyringe';

import { ConfigWithDiagnostics } from './config-diagnostics';
import { CONFIGS_WITH_DIAGNOSTIC_DI_TOKEN } from './config-diagnostics-di-token';

@singleton()
export class ConfigDiagnosticsAggregator {
    constructor(
        @injectAll(CONFIGS_WITH_DIAGNOSTIC_DI_TOKEN) private readonly configs: readonly ConfigWithDiagnostics[],
    ) {}

    aggregateConfigs(): Readonly<Record<string, unknown>> {
        const diagnostics = this.configs.map(c => c.getDiagnostics());
        return Object.fromEntries(orderBy(diagnostics, d => d[0]));
    }
}
