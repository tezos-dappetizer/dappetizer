import { DependencyContainer, Provider } from 'tsyringe';

import { EXPLICIT_CONFIG_FILE_PATH_DI_TOKEN } from './explicit-config-file-path-di-token';
import { argGuard } from '../../basics/public-api';
import { registerSingleton } from '../../dependency-injection/public-api';

/**
 * If config file path is not expecified explicitly, then these candidates are checked:
 * - `dappetizer.config.ts`
 * - `dappetizer.config.js`
 * - `dappetizer.config.json`
 */
export const candidateConfigFileNames = Object.freeze([
    'dappetizer.config.ts',
    'dappetizer.config.js',
    'dappetizer.config.json',
]);

/**
 * Registers a config file path to be used by Dappetizer indexer app.
 * It is registered as **singleton** and can be registered only **once** to make it clear which one is used.
 */
export function registerExplicitConfigFilePath(diContainer: DependencyContainer, configFilePathProvider: Provider<string>): void {
    argGuard.object(diContainer, 'diContainer');

    if (diContainer.isRegistered(EXPLICIT_CONFIG_FILE_PATH_DI_TOKEN)) {
        throw new Error('ExplicitConfigFilePath is already registered. There can be only one to make it clear which one is used.');
    }
    registerSingleton(diContainer, EXPLICIT_CONFIG_FILE_PATH_DI_TOKEN, configFilePathProvider);
}
