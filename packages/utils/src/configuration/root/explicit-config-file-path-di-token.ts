import { InjectionToken } from 'tsyringe';

export const EXPLICIT_CONFIG_FILE_PATH_DI_TOKEN: InjectionToken<string> = 'Dappetizer:Utils:ExplicitConfigFilePath';
