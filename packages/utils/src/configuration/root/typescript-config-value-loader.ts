import * as npmTSNode from 'ts-node';
import { singleton } from 'tsyringe';

import { RootConfigValueLoader } from './root-config-value-loader';
import { isObject } from '../../basics/public-api';
import { injectValue } from '../../dependency-injection/public-api';

@singleton()
export class TypeScriptConfigValueLoader implements RootConfigValueLoader {
    private isTSNodeInitialized = false;

    constructor(
        @injectValue(npmTSNode) private readonly tsNode: typeof npmTSNode,
        @injectValue({ require }) private readonly nodeJS: { readonly require: NodeJS.Require },
    ) {}

    loadValue(filePath: string): unknown {
        if (!this.isTSNodeInitialized) {
            const service = this.tsNode.register({ compilerOptions: { module: 'CommonJS' } });
            service.enabled(true);

            this.isTSNodeInitialized = true;
        }

        const value = this.nodeJS.require(filePath) as unknown;

        return isObject(value) && value.__esModule === true // eslint-disable-line no-underscore-dangle
            ? value.default
            : value;
    }
}
