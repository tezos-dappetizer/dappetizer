export interface RootConfigValueLoader {
    loadValue(filePath: string): unknown;
}
