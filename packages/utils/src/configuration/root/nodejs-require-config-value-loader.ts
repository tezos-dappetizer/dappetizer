import { injectable } from 'tsyringe';

import { RootConfigValueLoader } from './root-config-value-loader';
import { injectValue } from '../../dependency-injection/public-api';

@injectable()
export class NodeJSRequireConfigValueLoader implements RootConfigValueLoader {
    constructor(
        @injectValue({ require }) private readonly nodeJS: { readonly require: NodeJS.Require },
    ) {}

    loadValue(filePath: string): unknown {
        return this.nodeJS.require(filePath);
    }
}
