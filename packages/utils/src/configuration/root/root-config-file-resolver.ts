import nodeFS from 'fs';
import path from 'path';
import { singleton } from 'tsyringe';

import { EXPLICIT_CONFIG_FILE_PATH_DI_TOKEN } from './explicit-config-file-path-di-token';
import { candidateConfigFileNames } from './root-config-file-utils';
import { dumpToString } from '../../basics/public-api';
import { injectOptional, injectValue } from '../../dependency-injection/public-api';

/**
 * Resolves path of dappetizer config file.
 * It's Absolute b/c that is more understandable for users if included in errors.
 */
@singleton()
export class RootConfigFileResolver {
    constructor(
        @injectOptional(EXPLICIT_CONFIG_FILE_PATH_DI_TOKEN) private readonly explicitConfigFilePath: string | undefined,
        @injectValue(nodeFS) private readonly fs: typeof nodeFS,
        @injectValue(global.process) private readonly process: NodeJS.Process,
        @injectValue(candidateConfigFileNames) private readonly candidateFileNames: readonly string[],
    ) {}

    resolveAbsPath(): string {
        return this.explicitConfigFilePath
            ? this.resolveExplicitFromUser(this.explicitConfigFilePath)
            : this.resolveFirstExistingCandidate();
    }

    private resolveExplicitFromUser(relFilePath: string): string {
        const filePath = this.toAbsolute(relFilePath);

        if (!this.fs.existsSync(filePath)) {
            throw new Error(`Explicitly specified config file does not exist: '${relFilePath}' corresponding to: '${filePath}'.`);
        }
        return filePath;
    }

    private resolveFirstExistingCandidate(): string {
        for (const fileName of this.candidateFileNames) {
            const filePath = this.toAbsolute(fileName);

            if (this.fs.existsSync(filePath)) {
                return filePath;
            }
        }
        throw new Error(`Failed to find a configuration file.`
            + ` Supported files: ${dumpToString(this.candidateFileNames)} in directory: '${this.process.cwd()}'.`);
    }

    private toAbsolute(relPath: string): string {
        return path.resolve(this.process.cwd(), relPath);
    }
}
