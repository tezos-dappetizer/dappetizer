import { errorToString } from '../basics/public-api';

export const DAPPETIZER_CONFIG_FROM = 'Dappetizer config from file';

/** Trims the prefix b/c it is misleading for users. */
export function trimTsyringeInjectionPrefix(error: unknown): string {
    const str = errorToString(error);
    const index = str.indexOf(DAPPETIZER_CONFIG_FROM);
    return index >= 0 ? str.substring(index) : str;
}
