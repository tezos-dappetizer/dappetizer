import { InjectionToken } from 'tsyringe';

import { ConfigObjectElement } from './config-element';

export const MAINNET = 'mainnet';

export const ROOT_CONFIG_ELEMENT_DI_TOKEN: InjectionToken<ConfigObjectElement> = 'Dappetizer:Utils:RootConfigElement';

export const CONFIG_NETWORK_DI_TOKEN: InjectionToken<string> = 'Dappetizer:Utils:ConfigNetwork';

export const NETWORK_SPECIFIC_CONFIG_ELEMENT_DI_TOKEN: InjectionToken<ConfigObjectElement> = 'Dappetizer:Utils:NetworkSpecificConfigElement';
