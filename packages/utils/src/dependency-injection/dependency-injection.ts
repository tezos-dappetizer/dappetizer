import { DependencyContainer } from 'tsyringe';

import { DI_CONTAINER_DI_TOKEN } from './token-utils';

export function registerDependencyInjection(diContainer: DependencyContainer): void {
    diContainer.registerInstance(DI_CONTAINER_DI_TOKEN, diContainer);
}
