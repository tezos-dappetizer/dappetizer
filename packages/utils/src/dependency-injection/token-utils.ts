import { DependencyContainer, InjectionToken } from 'tsyringe';

export const DI_CONTAINER_DI_TOKEN: InjectionToken<DependencyContainer> = 'Dappetizer:Utils:DependencyContainer';
