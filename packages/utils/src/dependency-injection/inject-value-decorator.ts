import { injectWithTransform, singleton } from 'tsyringe';
import Transform from 'tsyringe/dist/typings/types/transform'; // eslint-disable-line import/no-unresolved

@singleton()
class InjectValueDummy {} // eslint-disable-line @typescript-eslint/no-extraneous-class

@singleton()
class InjectValueTransformer implements Transform<InjectValueDummy, unknown> {
    transform(_dummy: InjectValueDummy, value: unknown): unknown {
        return value;
    }
}

/** Used on constructor parameter for dependency injection of a logger for the specified category. */
export function injectValue(value: unknown): ReturnType<typeof injectWithTransform> {
    return injectWithTransform(InjectValueDummy, InjectValueTransformer, value);
}
