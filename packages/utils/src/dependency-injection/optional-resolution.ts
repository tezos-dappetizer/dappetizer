import { DependencyContainer, InjectionToken, injectWithTransform, singleton } from 'tsyringe';
import Transform from 'tsyringe/dist/typings/types/transform'; // eslint-disable-line import/no-unresolved

import { DI_CONTAINER_DI_TOKEN } from './token-utils';
import { argGuard, NonNullish } from '../basics/public-api';

export function resolveOptional<T extends NonNullish>(
    diContainer: DependencyContainer,
    token: InjectionToken<T>,
): T | undefined {
    argGuard.object(diContainer, 'diContainer');

    return diContainer.isRegistered(token, true) ? diContainer.resolve(token) : undefined;
}

/** Fixes @inject() which fails if nothing is registered. This injects undefined instead. */
export function injectOptional<T>(token: InjectionToken<T>): ReturnType<typeof injectWithTransform> {
    return injectWithTransform(DI_CONTAINER_DI_TOKEN, InjectOptionalTransformer, token);
}

@singleton()
class InjectOptionalTransformer<T extends NonNullish> implements Transform<DependencyContainer, T | undefined> {
    transform = resolveOptional;
}

export function resolveAllOptional<T extends NonNullish>(
    diContainer: DependencyContainer,
    token: InjectionToken<T>,
): T[] {
    argGuard.object(diContainer, 'diContainer');

    return diContainer.isRegistered(token, true) ? diContainer.resolveAll(token) : [];
}

/** Fixes @injectAll() which fails if nothing is registered. This injects empty array instead. */
export function injectAllOptional<T>(token: InjectionToken<T>): ReturnType<typeof injectWithTransform> {
    return injectWithTransform(DI_CONTAINER_DI_TOKEN, InjectAllOptionalTransformer, token);
}

@singleton()
class InjectAllOptionalTransformer<T extends NonNullish> implements Transform<DependencyContainer, T[]> {
    transform = resolveAllOptional;
}
