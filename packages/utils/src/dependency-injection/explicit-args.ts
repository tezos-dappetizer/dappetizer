import { DependencyContainer, inject, InjectionToken } from 'tsyringe';

import { argGuard, Constructor, NonNullish, withIndex } from '../basics/public-api';

export function resolveWithExplicitArgs<TResult extends NonNullish, TExplicitArgs extends unknown[]>(
    diContainer: DependencyContainer,
    resultType: new(...args: [...TExplicitArgs, ...any]) => TResult, // eslint-disable-line @typescript-eslint/no-explicit-any
    explicitArgs: [...TExplicitArgs],
): TResult {
    argGuard.object(diContainer, 'diContainer');
    argGuard.function(resultType, 'resultType');
    argGuard.nonEmptyArray(explicitArgs, 'explicitArgs');

    const childDIContainer = diContainer.createChildContainer();

    for (const [explicitArg, argIndex] of withIndex(explicitArgs)) {
        childDIContainer.registerInstance(getDIToken(argIndex), explicitArg);
    }

    return childDIContainer.resolve(resultType as Constructor<TResult>);
}

export function injectExplicit(argIndex = 0): ReturnType<typeof inject> {
    argGuard.number(argIndex, 'argIndex', { integer: true, min: 0, max: 20 });

    return inject(getDIToken(argIndex));
}

function getDIToken(argIndex: number): InjectionToken<unknown> {
    return `ExplicitArg-${argIndex}`;
}
