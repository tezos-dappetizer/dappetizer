import { DependencyContainer } from 'tsyringe';

import { HealthMetricsUpdater } from './health-metrics';
import { HealthCheckHttpHandler } from './http-handlers/health-check-http-handler';
import { HealthStatusHttpHandler } from './http-handlers/health-status-http-handler';
import { registerHttpHandler } from '../http-server/public-api';
import { registerMetricsUpdater } from '../metrics/public-api';

export function registerHealth(diContainer: DependencyContainer): void {
    // Inject to Dappetizer features:
    registerHttpHandler(diContainer, { useToken: HealthStatusHttpHandler });
    registerHttpHandler(diContainer, { useToken: HealthCheckHttpHandler });
    registerMetricsUpdater(diContainer, { useToken: HealthMetricsUpdater });
}
