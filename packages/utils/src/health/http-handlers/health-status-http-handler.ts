import express from 'express';
import { singleton } from 'tsyringe';

import { entriesOf, safeJsonStringify } from '../../basics/public-api';
import { contentTypes, httpHeaders } from '../../http-fetch/public-api';
import { HttpHandler } from '../../http-server/public-api';
import { injectLogger, Logger } from '../../logging/public-api';
import { HealthStatus } from '../health-check';
import { HealthProvider, HealthReport } from '../health-provider';

@singleton()
export class HealthStatusHttpHandler implements HttpHandler {
    readonly urlPath = '/health';

    constructor(
        private readonly healthProvider: HealthProvider,
        @injectLogger(HealthStatusHttpHandler) private readonly logger: Logger,
    ) {}

    async handle(_request: express.Request, response: express.Response): Promise<void> {
        const report = await this.healthProvider.generateHealthReport();

        const unhealthyChecks = findCheckNames(report, HealthStatus.Unhealthy);
        const degradedChecks = findCheckNames(report, HealthStatus.Degraded);

        if (unhealthyChecks.length > 0) {
            this.logger.logError(
                `${this.urlPath} responded with the report with {unhealthyChecks} and {degradedChecks}.`,
                { report, unhealthyChecks, degradedChecks },
            );
        } else if (degradedChecks.length > 0) {
            this.logger.logWarning(`${this.urlPath} responded with the report with {degradedChecks}.`, { report, degradedChecks });
        } else {
            this.logger.logInformation(`${this.urlPath} responded with the healthy report.`, { report });
        }

        response.setHeader(httpHeaders.CONTENT_TYPE, contentTypes.JSON);
        response.send(safeJsonStringify(report, { indent: 2 }));
    }
}

export function findCheckNames(report: HealthReport, status: HealthStatus): string[] {
    return entriesOf(report.results).filter(r => r[1].status === status).map(r => r[0]);
}
