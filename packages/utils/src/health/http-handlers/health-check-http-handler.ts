import express from 'express';
import { singleton } from 'tsyringe';

import { findCheckNames } from './health-status-http-handler';
import { httpStatusCodes } from '../../http-fetch/public-api';
import { HttpHandler } from '../../http-server/public-api';
import { injectLogger, Logger } from '../../logging/public-api';
import { HealthStatus } from '../health-check';
import { HealthProvider } from '../health-provider';

@singleton()
export class HealthCheckHttpHandler implements HttpHandler {
    readonly urlPath = '/check';

    constructor(
        private readonly healthProvider: HealthProvider,
        @injectLogger(HealthCheckHttpHandler) private readonly logger: Logger,
    ) {}

    async handle(_request: express.Request, response: express.Response): Promise<void> {
        const report = await this.healthProvider.generateHealthReport();

        if (report.isHealthy) {
            this.logger.logInformation(`${this.urlPath} responded with 200 OK based on the report.`, { report });
            response.send('OK');
        } else {
            this.logger.logError(`${this.urlPath} responded with 500 Internal Server Error because of {unhealthyChecks} from the report.`, {
                report,
                unhealthyChecks: findCheckNames(report, HealthStatus.Unhealthy),
            });
            response.sendStatus(httpStatusCodes.INTERNAL_SERVER_ERROR);
        }
    }
}
