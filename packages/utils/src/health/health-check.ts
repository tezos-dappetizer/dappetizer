import { AsyncOrSync } from 'ts-essentials';
import { DependencyContainer, Provider } from 'tsyringe';

import { HEALTH_CHECKS_DI_TOKEN } from './health-check-di-token';
import { registerSingleton } from '../dependency-injection/public-api';

export enum HealthStatus {
    Degraded = 'Degraded',
    Healthy = 'Healthy',
    Unhealthy = 'Unhealthy',
}

export interface HealthCheckResult {
    readonly status: HealthStatus;
    readonly data: unknown;
    readonly evaluatedTime?: Date;
}

/** Common top level interface for health checks. */
export interface HealthCheck {
    readonly name: string;
    checkHealth(): AsyncOrSync<HealthCheckResult>;
}

/**
 * Registers a health check to be used for evaluation of health status of the app available at `/check` and `/health` endpoints.
 * It is registered as **singleton**.
 */
export function registerHealthCheck(diContainer: DependencyContainer, healthCheckProvider: Provider<HealthCheck>): void {
    registerSingleton(diContainer, HEALTH_CHECKS_DI_TOKEN, healthCheckProvider);
}
