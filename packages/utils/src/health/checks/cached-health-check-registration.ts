import { DependencyContainer } from 'tsyringe';

import { CachedHealthCheck } from './cached-health-check';
import { CLOCK_DI_TOKEN } from '../../time/public-api';
import { HealthCheck, registerHealthCheck } from '../health-check';

export function registerCachedHealthCheck(
    diContainer: DependencyContainer,
    resolveHealthCheckDetails: (diContainer: DependencyContainer) => {
        healthCheckToCache: HealthCheck;
        cacheTimeMillis: number;
    },
): void {
    registerHealthCheck(diContainer, {
        useFactory: c => {
            const details = resolveHealthCheckDetails(c);

            return details.cacheTimeMillis > 0
                ? new CachedHealthCheck(details.healthCheckToCache, details.cacheTimeMillis, c.resolve(CLOCK_DI_TOKEN))
                : details.healthCheckToCache;
        },
    });
}
