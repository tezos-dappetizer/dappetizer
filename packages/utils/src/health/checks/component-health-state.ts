import { inject, injectable } from 'tsyringe';

import { injectExplicit } from '../../dependency-injection/public-api';
import { Clock, CLOCK_DI_TOKEN } from '../../time/public-api';
import { HealthCheck, HealthCheckResult, HealthStatus } from '../health-check';

/** Statically stores a health of some component. The component is specified in the inherited class. */
@injectable()
export class ComponentHealthState implements HealthCheck {
    private result: HealthCheckResult = {
        status: HealthStatus.Healthy,
        data: `No health state available because the component hasn't been executed yet.`,
    };

    constructor(
        @injectExplicit() readonly name: string,
        @inject(CLOCK_DI_TOKEN) private readonly clock: Clock,
    ) {}

    set(status: HealthStatus, data: unknown): void {
        const evaluatedOn = this.clock.getNowDate();
        this.result = { status, data, evaluatedTime: evaluatedOn };
    }

    checkHealth(): HealthCheckResult {
        return this.result;
    }
}
