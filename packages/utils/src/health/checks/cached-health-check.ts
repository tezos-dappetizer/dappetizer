import { addMillis, Clock } from '../../time/public-api';
import { HealthCheck, HealthCheckResult, HealthStatus } from '../health-check';

/** Decorator which caches health state for configured time. */
export class CachedHealthCheck implements HealthCheck {
    private cached?: Readonly<{ result: HealthCheckResult; expiresTime: Date }>;

    constructor(
        private readonly checkToCache: HealthCheck,
        private readonly cacheTimeMillis: number,
        private readonly clock: Clock,
    ) {}

    get name(): string {
        return this.checkToCache.name;
    }

    async checkHealth(): Promise<HealthCheckResult> {
        const nowTime = this.clock.getNowDate();

        if (!this.cached || nowTime > this.cached.expiresTime) {
            const result = await this.checkHealthFresh();

            this.cached = {
                // Honor existing evaluatedTime b/c it may be older.
                result: result.evaluatedTime ? result : { ...result, evaluatedTime: nowTime },
                expiresTime: addMillis(nowTime, this.cacheTimeMillis),
            };
        }
        return this.cached.result;
    }

    private async checkHealthFresh(): Promise<HealthCheckResult> {
        try {
            return await this.checkToCache.checkHealth();
        } catch (error) {
            return {
                status: HealthStatus.Unhealthy,
                data: error,
            };
        }
    }
}
