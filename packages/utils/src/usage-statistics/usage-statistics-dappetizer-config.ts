/**
 * Configures if the usage statistics are enabled.
 * This is used as a part of {@link "@tezos-dappetizer/database".DappetizerConfigUsingDb}.
 * @example Sample usage (all properties specified explicitly) within `dappetizer.config.ts` file:
 * ```typescript
 * import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';
 *
 * const config: DappetizerConfigUsingDb = {
 *     usageStatistics: {
 *         enabled: false,
 *     },
 *     ... // Other config parts.
 * };
 *
 * export default config;
 * ```
 * @category Dappetizer Config
 */
export interface UsageStatisticsDappetizerConfig {
    /**
     * Indicates if usage statistics should be enabled.
     * @default `true`
     */
    enabled?: boolean;
}
