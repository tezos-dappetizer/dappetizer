import { JWTInput } from 'google-auth-library';

import { isObjectLiteral, keyof } from '../basics/reflection-utils';
import { isWhiteSpace } from '../basics/string-manipulation';
import { JsonFetchCommand } from '../http-fetch/json-fetcher';

/** Downloads Dappetizer API key for Google Trace Cloud. */
export class GoogleCloudApiKeyQuery implements JsonFetchCommand<JWTInput> {
    readonly description = 'get Google Cloud Trace API access key';
    readonly url = 'https://telemetry.dappetizer.dev/google-cloud-key.json';

    transformResponse(jwt: JWTInput): JWTInput {
        if (!isObjectLiteral(jwt)) {
            throw new Error('It must be an object.');
        }
        if (typeof jwt.client_email !== 'string' || isWhiteSpace(jwt.client_email)) {
            throw new Error(`Invalid ${keyof<typeof jwt>('client_email')}.`);
        }
        if (typeof jwt.private_key !== 'string' || isWhiteSpace(jwt.private_key)) {
            throw new Error(`Invalid ${keyof<typeof jwt>('private_key')}.`);
        }
        if (typeof jwt.private_key_id !== 'string' || isWhiteSpace(jwt.private_key_id)) {
            throw new Error(`Invalid ${keyof<typeof jwt>('private_key_id')}.`);
        }
        if (typeof jwt.project_id !== 'string' || isWhiteSpace(jwt.project_id)) {
            throw new Error(`Invalid ${keyof<typeof jwt>('project_id')}.`);
        }
        return jwt;
    }
}
