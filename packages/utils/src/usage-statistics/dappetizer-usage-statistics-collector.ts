import { Attributes } from '@opentelemetry/api';
import { machineIdSync } from 'node-machine-id';
import { singleton } from 'tsyringe';

import { UsageStatisticsController } from './usage-statistics-controller';
import { injectValue } from '../dependency-injection/public-api';
import { versionInfo } from '../version';

/** Collects usage statistics reported by Dappetizer. */
@singleton()
export class DappetizerUsageStatisticsCollector {
    constructor(
        private readonly statisticsProvider: UsageStatisticsController,
        @injectValue(process) private readonly process: NodeJS.Process,
        @injectValue(versionInfo) private readonly dappetizerVersion: typeof versionInfo,
    ) {}

    collect(consoleCommand: string, additionalStats: Attributes): void {
        this.statisticsProvider.tracer
            ?.startSpan(consoleCommand)
            .setAttributes({
                uuid: machineIdSync(false), // Hashed machine id value (sha-256)
                architecture: this.process.arch,
                platform: this.process.platform,
                nodeVersion: this.process.version,
                dappetizerVersion: this.dappetizerVersion.VERSION,
                ...additionalStats,
            })
            .end();
    }
}
