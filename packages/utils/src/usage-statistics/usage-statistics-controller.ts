import { Tracer } from '@opentelemetry/api';
import { singleton } from 'tsyringe';

import { UsageStatistics, UsageStatisticsFactory } from './usage-statistics-factory';
import { injectLogger, Logger } from '../logging/public-api';

/** Controls lifetime of OpenTelemetry services used for usage statistics. */
@singleton()
export class UsageStatisticsController {
    private state = ControllerState.NotStarted;
    private statistics: UsageStatistics | null = null;

    constructor(
        private readonly statisticsFactory: UsageStatisticsFactory,
        @injectLogger(UsageStatisticsController) private readonly logger: Logger,
    ) {}

    get tracer(): Tracer | null {
        if (this.state !== ControllerState.Started) {
            throw new Error(`Cannot get tracer because usage statistics are ${this.state}.`);
        }
        return this.statistics?.tracer ?? null;
    }

    async start(): Promise<void> {
        if (this.state !== ControllerState.NotStarted) {
            throw new Error(`Cannot start usage statistics because they are ${this.state}.`);
        }
        this.state = ControllerState.Starting;

        try {
            this.statistics = await this.statisticsFactory.create();
        } catch (error) {
            this.logger.logError('Failed to start usage statistics. They will not be reported. {error}', { error });
        }
        this.state = ControllerState.Started;
    }

    async stop(): Promise<void> {
        if (this.state !== ControllerState.Started) {
            throw new Error(`Cannot stop usage statistics because they are ${this.state}.`);
        }
        this.state = ControllerState.Stopping;

        try {
            await this.statistics?.exporter.shutdown();
            this.statistics = null; // Clears memory.
        } catch (error) {
            this.logger.logError('Failed to stop usage statistics. {error}', { error });
        }
        this.state = ControllerState.Stopped;
    }
}

enum ControllerState {
    NotStarted = 'not started yet',
    Starting = 'starting in progress',
    Started = 'already started',
    Stopping = 'stopping in progress',
    Stopped = 'already stopped',
}
