import { Tracer } from '@opentelemetry/api';
import { BatchSpanProcessor, SpanExporter } from '@opentelemetry/sdk-trace-base';
import { SEMRESATTRS_SERVICE_NAME } from '@opentelemetry/semantic-conventions';
import { inject, singleton } from 'tsyringe';

import { GoogleCloudApiKeyQuery } from './google-cloud-api-key-query';
import { OpenTelemetryAbstraction } from './open-telemetry-abstraction';
import { UsageStatisticsConfig } from './usage-statistics-config';
import { JSON_FETCHER_DI_TOKEN, JsonFetcher } from '../http-fetch/public-api';

export interface UsageStatistics {
    readonly tracer: Tracer;
    readonly exporter: SpanExporter;
}

/** Creates and initializes OpenTelemetry services used for usage statistics. */
@singleton()
export class UsageStatisticsFactory {
    constructor(
        private readonly config: UsageStatisticsConfig,
        @inject(JSON_FETCHER_DI_TOKEN) private readonly jsonFetcher: JsonFetcher,
        private readonly openTelemetry: OpenTelemetryAbstraction,
    ) {}

    async create(): Promise<UsageStatistics | null> {
        if (!this.config.enabled) {
            return null;
        }

        let exporter: SpanExporter | null = null;
        try {
            const apiKey = await this.jsonFetcher.execute(new GoogleCloudApiKeyQuery());
            exporter = this.openTelemetry.createSpanExporter(apiKey);

            this.openTelemetry.registerInstrumentations();

            const provider = this.openTelemetry.createNodeTracerProvider({
                [SEMRESATTRS_SERVICE_NAME]: 'dappetizer',
            });
            provider.addSpanProcessor(new BatchSpanProcessor(exporter));
            provider.register();

            const tracer = this.openTelemetry.getTracer('dappetizer-trace');
            return { exporter, tracer };
        } catch (error) {
            try {
                await exporter?.shutdown();
            } catch {
                // Nothing to do b/c this is just cleanup after another error.
            }
            throw error;
        }
    }
}
