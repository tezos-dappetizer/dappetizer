import { inject, singleton } from 'tsyringe';

import { ConfigObjectElement, ConfigWithDiagnostics, ROOT_CONFIG_ELEMENT_DI_TOKEN } from '../configuration/public-api';

@singleton()
export class UsageStatisticsConfig implements ConfigWithDiagnostics {
    static readonly ROOT_NAME = 'usageStatistics';

    readonly enabled: boolean;

    constructor(@inject(ROOT_CONFIG_ELEMENT_DI_TOKEN) rootElement: ConfigObjectElement) {
        const thisElement = rootElement.getOptional(UsageStatisticsConfig.ROOT_NAME)?.asObject<keyof UsageStatisticsConfig>();

        this.enabled = thisElement?.getOptional('enabled')?.asBoolean()
            ?? true;
    }

    getDiagnostics(): [string, unknown] {
        return [UsageStatisticsConfig.ROOT_NAME, this];
    }
}
