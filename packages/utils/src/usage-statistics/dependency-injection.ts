import { DependencyContainer } from 'tsyringe';

import { UsageStatisticsConfig } from './usage-statistics-config';
import { registerConfigWithDiagnostics } from '../configuration/config-diagnostics';

export function registerUsageStatistics(diContainer: DependencyContainer): void {
    registerConfigWithDiagnostics(diContainer, { useToken: UsageStatisticsConfig });
}
