import { TraceExporter } from '@google-cloud/opentelemetry-cloud-trace-exporter';
import { diag, trace, Tracer } from '@opentelemetry/api';
import { registerInstrumentations } from '@opentelemetry/instrumentation';
import { Resource, ResourceAttributes } from '@opentelemetry/resources';
import { SpanExporter } from '@opentelemetry/sdk-trace-base';
import { NodeTracerProvider } from '@opentelemetry/sdk-trace-node';
import { GoogleAuth, JWT, JWTInput } from 'google-auth-library';
import { singleton } from 'tsyringe';

/** All stuff tightly coupled with OpenTelemetry libraries. */
@singleton()
export class OpenTelemetryAbstraction {
    /**
     * Original google trace exporter accepts options = {} as a constructor parameter where it's possible to provide either credentials
     * (not suitable for service account authorization) or service accout key filepath which requires physical key json file on disk.
     * We have changed the constructor the way that it accepts object created from json (fetched from dappetizer server)
     * which is used for creating GoogleAuth client instance to provide signing of the google cloud trace api requests.
     */
    createSpanExporter(key: JWTInput): SpanExporter {
        const client = new JWT({
            scopes: [
                'https://www.googleapis.com/auth/cloud-platform',
                'https://www.googleapis.com/auth/trace.readonly',
            ],
        });
        client.fromJSON(key);

        const googleAuth = new GoogleAuth({ authClient: client });
        const exporter = {
            _apiEndpoint: 'cloudtrace.googleapis.com:443',
            _auth: googleAuth,
            _projectId: googleAuth.getProjectId().catch((err: string) => {
                diag.error(err);
            }),
        };

        Object.setPrototypeOf(exporter, TraceExporter.prototype);
        return exporter as unknown as SpanExporter;
    }

    registerInstrumentations(): void {
        registerInstrumentations({
            instrumentations: [],
        });
    }

    createNodeTracerProvider(attributes: ResourceAttributes): NodeTracerProvider {
        const resource = Resource.default().merge(new Resource(attributes));
        return new NodeTracerProvider({ resource });
    }

    getTracer(name: string): Tracer {
        return trace.getTracer(name);
    }
}
