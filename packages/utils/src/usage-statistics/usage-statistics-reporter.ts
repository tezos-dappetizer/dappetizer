import { Attributes } from '@opentelemetry/api';
import { DependencyContainer } from 'tsyringe';

import { DappetizerUsageStatisticsCollector } from './dappetizer-usage-statistics-collector';
import { UsageStatisticsController } from './usage-statistics-controller';

/** @internal Intended only for Dappetizer itself. */
export async function reportUsageStatistics(
    diContainer: DependencyContainer,
    consoleCommand: string,
    additionalStats: Attributes = {},
): Promise<void> {
    const controller = diContainer.resolve(UsageStatisticsController);
    const dappetizerStatistics = diContainer.resolve(DappetizerUsageStatisticsCollector);

    await controller.start();
    dappetizerStatistics.collect(consoleCommand, additionalStats);
    await controller.stop();
}
