/** Application version and git information. */
export interface AppVersion {
    readonly name: string | null;
    readonly gitSha: string | null;
    readonly gitTag: string | null;
    readonly releaseNotesUrl: string | null;
}
