import { singleton } from 'tsyringe';

import { AppVersion } from './app-version';
import { isWhiteSpace } from '../basics/public-api';
import { injectValue } from '../dependency-injection/public-api';

export const envVariables = {
    APP_NAME: 'APP_NAME',
    GIT_SHA: 'GIT_SHA',
    GIT_TAG: 'GIT_TAG',
    APP_URL: 'APP_URL',
};

@singleton()
export class AppVersionProvider {
    readonly version: AppVersion;

    constructor(@injectValue(global.process) process: NodeJS.Process) {
        const getValue = (name: string): string | null => process.env[name] ?? null;

        const gitTag = getValue(envVariables.GIT_TAG);
        const appUrl = getValue(envVariables.APP_URL);

        this.version = {
            name: getValue(envVariables.APP_NAME),
            gitSha: getValue(envVariables.GIT_SHA),
            gitTag,
            releaseNotesUrl: !isWhiteSpace(appUrl) ? `${appUrl}/-/releases/${gitTag ?? ''}` : null,
        };
    }
}
