import { container as globalDIContainer, DependencyContainer } from 'tsyringe';

import { AppStartupReporter } from './app-control/app-startup-reporter';
import { BackgroundWorkerExecutor } from './app-control/background-worker-executor';
import { argGuard } from './basics/public-api';
import { trimTsyringeInjectionPrefix } from './configuration/config-utils';
import { LoggingConfig } from './logging/logging-config';
import { LoggedPackagesProvider } from './logging/package-info/logged-packages-provider';
import { LogLevel, ROOT_LOGGER_DI_TOKEN } from './logging/public-api';

export async function runApp(diContainer: DependencyContainer = globalDIContainer, indexerModuleIds: readonly string[] = []): Promise<void> {
    argGuard.object(diContainer, 'diContainer');
    try {
        const startupReporter = diContainer.resolve(AppStartupReporter);
        const backgroundWorkers = diContainer.resolve(BackgroundWorkerExecutor);

        await startupReporter.logOnAppStartup(indexerModuleIds);
        await backgroundWorkers.start();
    } catch (error) {
        logCritical(diContainer, error);
        process.exit(1);
    }
}

function logCritical(diContainer: DependencyContainer, rawError: unknown): void {
    try {
        const error = trimTsyringeInjectionPrefix(rawError);
        const logger = diContainer.resolve(ROOT_LOGGER_DI_TOKEN);
        const loggingConfig = diContainer.resolve(LoggingConfig);
        const packagesProvider = diContainer.resolve(LoggedPackagesProvider);

        logger.log({
            message: 'Failed to start the app. {error}',
            data: { error },
            category: 'App',
            level: LogLevel.Critical,
            timestamp: new Date(),
            globalData: loggingConfig.globalData,
            packages: packagesProvider.packages,
        });
    } catch {
        console.log('Failed to start the app.', rawError); // eslint-disable-line no-console
    }
}
