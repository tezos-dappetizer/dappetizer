import { TestClock } from '../../../../test-utilities/mocks';
import { Stopwatch } from '../../src/time/stopwatch';

describe(Stopwatch.name, () => {
    let target: Stopwatch;
    let clock: TestClock;

    beforeEach(() => {
        clock = new TestClock();
        target = new Stopwatch(clock);
    });

    it('should start measurement on creation', () => {
        expect(target.startTimestamp).toBe(clock.nowDate.getTime());
    });

    it('should measure duration correctly', () => {
        expect(target.elapsedMillis).toBe(0);

        clock.tick(66);

        expect(target.elapsedMillis).toBe(66);
    });
});
