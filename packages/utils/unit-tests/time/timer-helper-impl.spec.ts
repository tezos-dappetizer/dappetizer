import { instance, mock, verify, when } from 'ts-mockito';

import { AppShutdownManager } from '../../src/app-control/public-api';
import { TimerHelper } from '../../src/time/timer-helper';
import { TimerHelperImpl } from '../../src/time/timer-helper-impl';

interface CallbackHost {
    callback(): void;
}

describe(TimerHelperImpl.name, () => {
    let target: TimerHelper;
    let appShutdownController: AbortController;

    let callbackHost: CallbackHost;

    beforeEach(() => {
        appShutdownController = new AbortController();
        const appShutdownManager = { signal: appShutdownController.signal } as AppShutdownManager;
        target = new TimerHelperImpl(appShutdownManager);

        callbackHost = mock();
    });

    function targetSetTimeout() {
        return target.setTimeout(500, () => instance(callbackHost).callback());
    }

    it('should call given func after given delay', async () => {
        const startTime = Date.now();
        let endTime = 0;
        when(callbackHost.callback()).thenCall(() => {
            endTime = Date.now();
        });

        targetSetTimeout();

        await sleep(1_000);
        verify(callbackHost.callback()).once();

        const duration = endTime - startTime;
        expect(duration).toBeGreaterThanOrEqual(500);
        expect(duration).toBeLessThanOrEqual(1_000);
    });

    it('should cancel already started timeout', async () => {
        const timeout = targetSetTimeout();

        timeout.clear();

        await sleep(1_000);
        verify(callbackHost.callback()).never();
    });

    it('should cancel timeout on app shutdown', async () => {
        targetSetTimeout();

        appShutdownController.abort();

        await sleep(1_000);
        verify(callbackHost.callback()).never();
    });
});

async function sleep(delay: number) {
    return new Promise<void>(resolve => {
        setTimeout(resolve, delay);
    });
}
