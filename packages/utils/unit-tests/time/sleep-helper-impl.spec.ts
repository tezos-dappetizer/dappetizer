import { anything, instance, mock, verify, when } from 'ts-mockito';

import { SleepHelper } from '../../src/time/public-api';
import { SleepHelperImpl } from '../../src/time/sleep-helper-impl';
import { TimerHelper } from '../../src/time/timer-helper';

describe(SleepHelperImpl.name, () => {
    let target: SleepHelper;
    let timerHelper: TimerHelper;

    beforeEach(() => {
        timerHelper = mock();
        target = new SleepHelperImpl(instance(timerHelper));
    });

    it('should wake after given time', async () => {
        when(timerHelper.setTimeout(anything(), anything())).thenCall((_delay, callback) => {
            callback();
        });

        await target.sleep(666);

        verify(timerHelper.setTimeout(anything(), anything())).once();
        verify(timerHelper.setTimeout(666, anything())).once();
    });

    it('should not sleep if zero millis', async () => {
        await target.sleep(0);

        verify(timerHelper.setTimeout(anything(), anything())).never();
    });
});
