import { anything, instance, mock, verify, when } from 'ts-mockito';

import { expectToThrowAsync, TestLogger } from '../../../../test-utilities/mocks';
import { LongExecutionHelper, LongExecutionOperation } from '../../src/time/long-execution-helper';
import { LongExecutionHelperImpl } from '../../src/time/long-execution-helper-impl';
import { TimeConfig } from '../../src/time/time-config';
import { Timeout, TimerHelper } from '../../src/time/timer-helper';

describe(LongExecutionHelperImpl.name, () => {
    let target: LongExecutionHelper;
    let timerHelper: TimerHelper;
    let logger: TestLogger;
    let operation: LongExecutionOperation<string>;
    let config: TimeConfig;
    let timeout: Timeout;

    const act = async () => target.warn(instance(operation));

    beforeEach(() => {
        timerHelper = mock();
        logger = new TestLogger();
        config = { longExecutionWarningMillis: 1_000 } as TimeConfig;
        target = new LongExecutionHelperImpl(config, instance(timerHelper), logger);

        [timeout, operation] = [mock(), mock()];
        when(operation.description).thenReturn('op desc');
    });

    it('should execute given operation if successful', async () => {
        when(operation.execute()).thenResolve('abc');
        whenSetTimeout().thenReturn(instance(timeout));

        const result = await act();

        expect(result).toBe('abc');
        logger.verifyNothingLogged();
        verify(timeout.clear()).once();
    });

    it('should execute given operation if failed', async () => {
        const opError = new Error('oups');
        when(operation.execute()).thenReject(opError);
        whenSetTimeout().thenReturn(instance(timeout));

        const error = await expectToThrowAsync(act);

        expect(error).toBe(opError);
        logger.verifyNothingLogged();
        verify(timeout.clear()).once();
    });

    it('should log warning if execution takes too long', async () => {
        whenSetTimeout().thenCall((_delay, callback) => {
            callback();
            return instance(timeout);
        });

        await act();

        logger.loggedSingle().verify('Warning', {
            operation: 'op desc',
            millis: 1_000,
        });
        verify(timeout.clear()).once();
    });

    function whenSetTimeout() {
        return when(timerHelper.setTimeout(config.longExecutionWarningMillis, anything()));
    }
});
