import { describeMember } from '../../../../test-utilities/mocks';
import { ClockImpl } from '../../src/time/clock-impl';
import { Clock } from '../../src/time/public-api';

describe(ClockImpl.name, () => {
    let target: Clock;
    let startTimestamp: number;

    beforeEach(() => {
        target = new ClockImpl();
        startTimestamp = Date.now();
    });

    describeMember<typeof target>('getNowDate', () => {
        it('should get current Date', () => {
            const date = target.getNowDate();

            verifyNow(date.getTime());
        });
    });

    describeMember<typeof target>('getNowTimestamp', () => {
        it('should get current Date', () => {
            const timestamp = target.getNowTimestamp();

            verifyNow(timestamp);
        });
    });

    function verifyNow(timestamp: number) {
        expect(timestamp).toBeGreaterThanOrEqual(startTimestamp);
        expect(timestamp).toBeLessThanOrEqual(Date.now());
    }
});
