import { range } from 'lodash';
import { instance, mock, verify } from 'ts-mockito';

import { expectToThrow } from '../../../../test-utilities/mocks';
import { AbortError, addAbortListener, isAbortError, throwIfAborted } from '../../src/app-control/abortion';

describe('Abortion utils', () => {
    let controller: AbortController;

    beforeEach(() => {
        controller = new AbortController();
    });

    describe(isAbortError.name, () => {
        it.each([
            [true, 'test AbortError', { name: 'AbortError' }],
            [true, 'our own AbortError', new AbortError()],
            [false, 'other Error', new Error()],
            [false, 'other type', 'foo'],
        ])('should return %s if %s', (expected: boolean, _desc: string, error: unknown) => {
            expect(isAbortError(error)).toBe(expected);
        });
    });

    interface Listener {
        action(): void;
    }

    describe(addAbortListener.name, () => {
        let listener: Listener;

        beforeEach(() => {
            listener = mock();
        });

        it('should register listener', () => {
            addAbortListener(controller.signal, () => instance(listener).action());
            verify(listener.action()).never();

            controller.abort();
            verify(listener.action()).once();
        });

        it('should allow unsubscribe', () => {
            const unsubscribe = addAbortListener(controller.signal, () => instance(listener).action());
            unsubscribe();

            controller.abort();
            verify(listener.action()).never();
        });

        it('should automatically unsubscribe once fired', () => {
            addAbortListener(controller.signal, () => instance(listener).action());

            range(5).forEach(() => controller.abort());

            verify(listener.action()).once();
        });
    });

    describe(throwIfAborted.name, () => {
        it('should throw if aborted', () => {
            controller.abort();

            expectToThrow(() => throwIfAborted(controller.signal));
        });

        it('should pass if not aborted', () => {
            throwIfAborted(controller.signal);
        });
    });
});
