import { anything, instance, mock, when } from 'ts-mockito';

import { TestLogger, verifyCalled } from '../../../../test-utilities/mocks';
import { AppShutdownManager } from '../../src/app-control/app-shutdown-manager';
import { GlobalErrorHandler } from '../../src/app-control/global-error-handler';

describe(GlobalErrorHandler.name, () => {
    let process: NodeJS.Process;
    let logger: TestLogger;
    let appShutdownManager: AppShutdownManager;
    let registeredHandlers: Record<string, (a: any) => void>;

    beforeEach(() => {
        [process, appShutdownManager] = [mock(), mock()];
        logger = new TestLogger();
        registeredHandlers = {};

        when(process.on(anything(), anything())).thenCall((event, handler) => {
            registeredHandlers[event] = handler;
        });

        const target = new GlobalErrorHandler(instance(process), logger);
        target.register(instance(appShutdownManager));
    });

    it.each<NodeJS.Signals>(['SIGINT', 'SIGTERM'])('should shut down on %s', signal => {
        registeredHandlers[signal]!(signal);

        logger.loggedSingle().verify('Information', { signal });
        verifyCalled(appShutdownManager.shutdown).onceWith({ exitCode: 0 });
    });

    it.each(['unhandledRejection', 'uncaughtException'])('should shut down on %s', event => {
        const error = new Error('oups');

        registeredHandlers[event]!(error);

        logger.loggedSingle().verify('Critical', { error });
        verifyCalled(appShutdownManager.shutdown).onceWith({ exitCode: 1 });
    });
});
