import { instance, mock, when } from 'ts-mockito';

import { verifyCalled } from '../../../../test-utilities/mocks';
import { AppUptimeMetrics, AppUptimeMetricsUpdater } from '../../src/app-control/app-uptime-metrics';

describe(AppUptimeMetricsUpdater.name, () => {
    let target: AppUptimeMetricsUpdater;
    let nodeProcess: NodeJS.Process;
    let uptimeGauge: AppUptimeMetrics['uptime'];

    beforeEach(() => {
        uptimeGauge = mock();
        const metrics = { uptime: instance(uptimeGauge) } as AppUptimeMetrics;

        nodeProcess = mock();
        target = new AppUptimeMetricsUpdater(metrics, instance(nodeProcess));
    });

    it('should report process uptime', () => {
        when(nodeProcess.uptime()).thenReturn(123.987);

        target.update();

        verifyCalled(uptimeGauge.set).onceWith(123);
    });
});
