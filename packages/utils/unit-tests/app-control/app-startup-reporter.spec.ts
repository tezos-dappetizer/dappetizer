import { anyString, instance, mock, when } from 'ts-mockito';

import { TestLogger } from '../../../../test-utilities/mocks';
import { AppStartupReporter, UNKNOWN } from '../../src/app-control/app-startup-reporter';
import { ConfigDiagnosticsAggregator } from '../../src/configuration/config-diagnostics-aggregator';
import { PackageInfoResolver } from '../../src/packages-versions/package-info-resolver';

describe(AppStartupReporter.name, () => {
    let target: AppStartupReporter;
    let configAggregator: ConfigDiagnosticsAggregator;
    let versionResolver: PackageInfoResolver;
    let logger: TestLogger;

    beforeEach(() => {
        [configAggregator, versionResolver] = [mock(), mock()];
        logger = new TestLogger();
        target = new AppStartupReporter(instance(configAggregator), instance(versionResolver), logger);
    });

    it('should log app config', async () => {
        const dappetizerConfig = { foo: 'bar' };
        const infoError = new Error('oups');
        when(configAggregator.aggregateConfigs()).thenReturn(dappetizerConfig);
        when(versionResolver.resolveInfo('moduleXX')).thenResolve({
            filePath: '/dir/file-xx.js',
            name: '@mod/xx',
            version: null,
        });
        when(versionResolver.resolveInfo('moduleYY')).thenReject(infoError);
        when(versionResolver.resolveInfo('moduleZZ')).thenResolve({
            filePath: '/dir/file-zz.js',
            name: null,
            version: '1.2.3',
        });

        await target.logOnAppStartup(['moduleXX', 'moduleYY', 'moduleZZ']);

        logger.verifyLoggedCount(2);
        logger.logged(0).verify('Warning', { indexerModuleId: 'moduleYY', error: infoError });
        logger.logged(1).verify('Information', {
            dappetizerConfig,
            dappetizerVersion: anyString(),
            indexerModules: {
                moduleXX: { file: '/dir/file-xx.js', name: '@mod/xx' },
                moduleYY: UNKNOWN,
                moduleZZ: { file: '/dir/file-zz.js', version: '1.2.3' },
            },
        });
    });
});
