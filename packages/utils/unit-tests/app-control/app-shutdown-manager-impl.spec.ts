import { instance, mock, verify, when } from 'ts-mockito';
import { DependencyContainer } from 'tsyringe';

import { describeMember, TestLogger, verifyCalled } from '../../../../test-utilities/mocks';
import { AppShutdownManager } from '../../src/app-control/app-shutdown-manager';
import { AppShutdownManagerImpl } from '../../src/app-control/app-shutdown-manager-impl';
import { BackgroundWorkerExecutor } from '../../src/app-control/background-worker-executor';
import { GlobalErrorHandler } from '../../src/app-control/global-error-handler';
import { RootLogger } from '../../src/logging/public-api';

describe(AppShutdownManagerImpl.name, () => {
    let target: AppShutdownManager;
    let process: NodeJS.Process;
    let logger: TestLogger;
    let rootLogger: RootLogger;
    let diContainer: DependencyContainer;
    let globalErrorHandler: GlobalErrorHandler;
    let backgroundWorkerExecutor: BackgroundWorkerExecutor;

    beforeEach(() => {
        [process, rootLogger, diContainer, globalErrorHandler, backgroundWorkerExecutor] = [mock(), mock(), mock(), mock(), mock()];
        logger = new TestLogger();
        target = new AppShutdownManagerImpl(instance(globalErrorHandler), instance(process), logger, instance(rootLogger), instance(diContainer));

        when(diContainer.resolve(BackgroundWorkerExecutor)).thenReturn(instance(backgroundWorkerExecutor));
    });

    describe('constructor', () => {
        it('should not be aborted', () => {
            expect(target.signal.aborted).toBeFalse();
        });

        it('should register global error handling', () => {
            verify(globalErrorHandler.register(target)).once();
        });
    });

    describeMember<AppShutdownManagerImpl>('shutdown', () => {
        it('should clean up and exit', async () => {
            await target.shutdown({ exitCode: 66 });

            expect(target.signal.aborted).toBeTrue();
            logger.loggedSingle().verify('Information').verifyMessage('shuts down');
            verify(backgroundWorkerExecutor.stop()).once();
            verify(rootLogger.close()).once();
            verifyCalled(process.exit).onceWith(66);
        });

        it('should use default (zero) exit code if not provided', async () => {
            await target.shutdown();

            verifyCalled(process.exit).onceWith(undefined);
        });
    });
});
