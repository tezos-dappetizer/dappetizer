import path from 'path';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { FileSystem } from '../../src/file-system/public-api';
import { PACKAGE_JSON, PackageInfo, PackageInfoResolver } from '../../src/packages-versions/package-info-resolver';

describe(PackageInfoResolver.name, () => {
    let target: PackageInfoResolver;
    let nodeJSRequire: NodeJS.Require;
    let fileSystem: FileSystem;

    beforeEach(() => {
        [nodeJSRequire, fileSystem] = [mock(), mock()];
        target = new PackageInfoResolver(instance(nodeJSRequire), instance(fileSystem));

        when(nodeJSRequire.resolve('testId')).thenReturn(toAbs('dir1/dir2/dir3/index.js'));
    });

    it('should resolve version from package.json in direct directory', async () => {
        setupFile(`dir1/dir2/dir3/${PACKAGE_JSON}`, '{"name":"foo","version":"1.2.3"}');

        await runTest({ expected: { name: 'foo', version: '1.2.3' } });
    });

    it('should resolve version from package.json in parent directory', async () => {
        setupFile(`dir1/${PACKAGE_JSON}`, '{"name":"foo","version":"1.2.3"}');

        await runTest({
            expected: { name: 'foo', version: '1.2.3' },
            expectedFileReadCount: 3,
        });

        verify(fileSystem.readFileTextIfExists(toAbs(`dir1/dir2/dir3/${PACKAGE_JSON}`)))
            .calledBefore(fileSystem.readFileTextIfExists(toAbs(`dir1/dir2/${PACKAGE_JSON}`)));
    });

    it('should return empty values if no package.json found', async () => {
        await runTest({
            expected: { name: null, version: null },
            expectedFileReadCount: 4,
        });
    });

    it('should omit name if same as module id', async () => {
        setupFile(`dir1/dir2/dir3/${PACKAGE_JSON}`, '{"name":"testId","version":"1.2.3"}');

        await runTest({ expected: { name: null, version: '1.2.3' } });
    });

    async function runTest(options: { expected: Pick<PackageInfo, 'name' | 'version'>; expectedFileReadCount?: number }) {
        const info = await target.resolveInfo('testId');

        expect(info).toEqual<typeof info>({
            filePath: toAbs('dir1/dir2/dir3/index.js'),
            name: options.expected.name,
            version: options.expected.version,
        });
        verify(fileSystem.readFileTextIfExists(anything())).times(options.expectedFileReadCount ?? 1);
    }

    function setupFile(filePath: string, contents: string) {
        when(fileSystem.readFileTextIfExists(toAbs(filePath))).thenResolve(contents);
    }

    function toAbs(relPath: string) {
        return path.resolve(path.parse(__filename).root, relPath);
    }
});
