import { Tracer } from '@opentelemetry/api';
import { BatchSpanProcessor, SpanExporter } from '@opentelemetry/sdk-trace-base';
import { NodeTracerProvider } from '@opentelemetry/sdk-trace-node';
import { JWTInput } from 'google-auth-library';
import { Writable } from 'ts-essentials';
import { anyOfClass, anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { expectToThrowAsync, fixedMock } from '../../../../test-utilities/mocks';
import { JsonFetcher } from '../../src/http-fetch/public-api';
import { GoogleCloudApiKeyQuery } from '../../src/usage-statistics/google-cloud-api-key-query';
import { OpenTelemetryAbstraction } from '../../src/usage-statistics/open-telemetry-abstraction';
import { UsageStatisticsConfig } from '../../src/usage-statistics/usage-statistics-config';
import { UsageStatisticsFactory } from '../../src/usage-statistics/usage-statistics-factory';

describe(UsageStatisticsFactory.name, () => {
    let target: UsageStatisticsFactory;
    let config: Writable<UsageStatisticsConfig>;
    let jsonFetcher: JsonFetcher;
    let openTelemetry: OpenTelemetryAbstraction;

    let apiKey: JWTInput;
    let exporter: SpanExporter;
    let tracer: Tracer;
    let provider: NodeTracerProvider;

    beforeEach(() => {
        config = { enabled: true } as UsageStatisticsConfig;
        jsonFetcher = mock();
        openTelemetry = mock();
        tracer = 'mockedTracer' as any;
        target = new UsageStatisticsFactory(config, instance(jsonFetcher), instance(openTelemetry));

        exporter = fixedMock();
        apiKey = 'mockedApiKey' as any;
        provider = mock();

        when(jsonFetcher.execute(anyOfClass(GoogleCloudApiKeyQuery))).thenResolve(apiKey);
        when(openTelemetry.createSpanExporter(apiKey)).thenReturn(instance(exporter));
        when(openTelemetry.createNodeTracerProvider(anything())).thenReturn(instance(provider));
        when(openTelemetry.getTracer('dappetizer-trace')).thenReturn(tracer);
    });

    it('should create statistics correctly', async () => {
        const statistics = await target.create();

        expect(statistics?.exporter).toBe(instance(exporter));
        expect(statistics?.tracer).toBe(tracer);
        verify(openTelemetry.createNodeTracerProvider(deepEqual({ 'service.name': 'dappetizer' }))).once();
        verify(openTelemetry.registerInstrumentations()).once();
        verify(provider.addSpanProcessor(anyOfClass(BatchSpanProcessor))).once();
        verify(provider.register()).once();
        verify(exporter.shutdown()).never();
    });

    it('should not create statistics if disabled', async () => {
        config.enabled = false;

        const statistics = await target.create();

        expect(statistics).toBeNull();
        verify(jsonFetcher.execute(anything())).never();
        verify(openTelemetry.createSpanExporter(anything())).never();
    });

    it('should shutdown exporter if some other error', async () => {
        const otherError = new Error('oups');
        when(openTelemetry.registerInstrumentations()).thenThrow(otherError);

        const error = await expectToThrowAsync(async () => target.create());

        expect(error).toBe(otherError);
        verify(exporter.shutdown()).once();
    });
});
