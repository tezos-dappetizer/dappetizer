import { JWTInput } from 'google-auth-library';
import { URL } from 'url';

import { describeMember, expectToThrow } from '../../../../test-utilities/mocks';
import { GoogleCloudApiKeyQuery } from '../../src/usage-statistics/google-cloud-api-key-query';

describe(GoogleCloudApiKeyQuery.name, () => {
    let target: GoogleCloudApiKeyQuery;

    beforeEach(() => {
        target = new GoogleCloudApiKeyQuery();
    });

    it('should have correct properties', () => {
        expect(target.description).not.toBeEmpty();
        expect(() => new URL(target.url)).not.toThrow();
    });

    describeMember<typeof target>('transformResponse', () => {
        let inputKey: JWTInput;

        beforeEach(() => {
            inputKey = {
                client_email: 'sdf@sdfd',
                private_key: 'fsdf',
                private_key_id: 'aa',
                project_id: 'ccc',
            };
        });

        it('should pass if valid access key', () => {
            const key = target.transformResponse(inputKey);

            expect(key).toBe(inputKey);
        });

        it.each([
            ['undefined', undefined],
            ['null', null],
            ['not object', 'wtf'],
        ])('should throw if %s', (_desc, invalidKey: any) => {
            expectToThrow(() => target.transformResponse(invalidKey));
        });

        describe.each([
            'client_email',
            'private_key',
            'private_key_id',
            'project_id',
        ] as const)('%s', property => {
            it.each([
                ['undefined', undefined],
                ['null', null],
                ['not string', 123],
                ['white-space', '  '],
            ])('should throw if %s', (_desc, invalidValue: any) => {
                inputKey[property] = invalidValue;

                expectToThrow(() => target.transformResponse(inputKey));
            });
        });
    });
});
