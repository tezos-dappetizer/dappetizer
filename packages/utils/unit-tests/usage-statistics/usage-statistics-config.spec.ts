import { StrictOmit } from 'ts-essentials';

import { verifyPropertyNames } from '../../../../test-utilities/mocks';
import { DappetizerConfig } from '../../../indexer/src/config/dappetizer-config';
import { ConfigObjectElement } from '../../src/configuration/public-api';
import { UsageStatisticsConfig } from '../../src/usage-statistics/usage-statistics-config';
import { UsageStatisticsDappetizerConfig } from '../../src/usage-statistics/usage-statistics-dappetizer-config';

type UsageStatisticsConfigProperties = StrictOmit<UsageStatisticsConfig, 'getDiagnostics'>;

describe(UsageStatisticsConfig.name, () => {
    function act(thisJson: DappetizerConfig['usageStatistics']) {
        const rootJson = { [UsageStatisticsConfig.ROOT_NAME]: thisJson };
        return new UsageStatisticsConfig(new ConfigObjectElement(rootJson, 'file', '$'));
    }

    it.each([true, false])('should be created if all values specified esp. enabled %s', enabled => {
        const thisJson = { enabled };

        const config = act(thisJson);

        expect(config).toEqual<UsageStatisticsConfigProperties>({ enabled });
    });

    it.each([
        ['no root element', undefined],
        ['empty root element', {}],
    ])('should be created with defaults if %s', (_desc, thisJson) => {
        const config = act(thisJson);

        expect(config).toEqual<UsageStatisticsConfigProperties>({ enabled: true });
    });

    it('should expose its properties in DappetizerConfig', () => {
        expect(UsageStatisticsConfig.ROOT_NAME).toBe<keyof DappetizerConfig>('usageStatistics');

        const config = act({});
        verifyPropertyNames<UsageStatisticsDappetizerConfig>(config, ['enabled']);
    });
});
