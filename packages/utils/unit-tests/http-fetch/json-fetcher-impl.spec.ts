import { Response } from 'node-fetch';
import { anything, instance, mock, when } from 'ts-mockito';

import { expectToThrowAsync, verifyCalled } from '../../../../test-utilities/mocks';
import { AppShutdownManager } from '../../src/app-control/app-shutdown-manager';
import { contentTypes, httpHeaders } from '../../src/http-fetch/http-constants';
import { HttpFetcher, RequestOptions } from '../../src/http-fetch/http-fetcher';
import { JsonFetchCommand, JsonFetcher } from '../../src/http-fetch/json-fetcher';
import { JsonFetcherImpl } from '../../src/http-fetch/json-fetcher-impl';

describe(JsonFetcherImpl.name, () => {
    let target: JsonFetcher;
    let httpFetcher: HttpFetcher;
    let appShutdownManager: AppShutdownManager;

    let command: JsonFetchCommand<string>;
    let httpResponse: Response;

    const act = async () => target.execute(instance(command));

    beforeEach(() => {
        httpFetcher = mock();
        appShutdownManager = { signal: 'mockedSignal' as any } as AppShutdownManager;
        target = new JsonFetcherImpl(instance(httpFetcher), appShutdownManager);

        command = mock();
        httpResponse = {} as Response;
        const rawResponse = { foo: 'bar' };

        when(command.url).thenReturn('http://test/api');
        when(command.requestOptions).thenReturn({
            headers: { foo: 'bar' },
            timeout: 999,
        });
        when(httpFetcher.fetch(anything(), anything())).thenResolve(httpResponse);
        httpResponse.ok = true;
        httpResponse.json = async () => Promise.resolve(rawResponse);
        when(command.transformResponse(rawResponse)).thenReturn('testResult');
    });

    it('should get response data correctly', async () => {
        const data = await act();

        expect(data).toBe('testResult');
        verifyCalled<string, RequestOptions>(httpFetcher.fetch).onceWith('http://test/api', {
            headers: {
                [httpHeaders.ACCEPT]: contentTypes.JSON,
                foo: 'bar',
            },
            timeout: 999,
            signal: appShutdownManager.signal,
        });
    });

    it('should support override of request options', async () => {
        when(command.requestOptions).thenReturn({
            signal: 'customSignal' as any,
            headers: { [httpHeaders.ACCEPT]: 'customContentType' },
        });

        await act();

        verifyCalled<string, RequestOptions>(httpFetcher.fetch).onceWith('http://test/api', {
            signal: 'customSignal' as any,
            headers: { [httpHeaders.ACCEPT]: 'customContentType' },
        });
    });

    it('should throw if not ok response', async () => {
        httpResponse.ok = false;
        httpResponse.status = 666;
        httpResponse.statusText = 'wtf';

        await runThrowTest({ expectedError: ['666 wtf'] });
    });

    it('should wrap network error', async () => {
        when(httpFetcher.fetch(anything(), anything())).thenReject(new Error('oups'));

        await runThrowTest({ expectedError: ['oups'] });
    });

    it('should wrap json read error', async () => {
        httpResponse.json = async () => Promise.reject(new Error('oups'));

        await runThrowTest({ expectedError: ['oups'] });
    });

    it('should wrap response transform error', async () => {
        when(command.transformResponse(anything())).thenThrow(new Error('oups'));

        await runThrowTest({ expectedError: ['transform', 'oups', '{"foo":"bar"}'] });
    });

    async function runThrowTest(options: { expectedError: string[] }) {
        const error = await expectToThrowAsync(act);

        expect(error.message).toIncludeMultiple([
            'HTTP request',
            'http://test/api',
            ...options.expectedError,
        ]);
    }
});
