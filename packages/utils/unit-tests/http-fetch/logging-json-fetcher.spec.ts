import { Writable } from 'ts-essentials';
import { anything, capture, instance, mock, when } from 'ts-mockito';

import { expectToThrowAsync, TestClock, TestLogger } from '../../../../test-utilities/mocks';
import { ENABLE_TRACE } from '../../../utils/src/public-api';
import { httpMethods } from '../../src/http-fetch/http-constants';
import { JsonFetchCommand, JsonFetcher } from '../../src/http-fetch/json-fetcher';
import { LoggingJsonFetcher } from '../../src/http-fetch/logging-json-fetcher';
import { LongExecutionHelper, LongExecutionOperation } from '../../src/time/public-api';

describe(LoggingJsonFetcher.name, () => {
    let target: JsonFetcher;
    let nextFetcher: JsonFetcher;
    let clock: TestClock;
    let logger: TestLogger;
    let longExecutionHelper: LongExecutionHelper;

    let command: Writable<JsonFetchCommand<string>>;

    const act = async () => target.execute(command);

    beforeEach(() => {
        [nextFetcher, longExecutionHelper] = [mock(), mock()];
        clock = new TestClock();
        logger = new TestLogger();
        target = new LoggingJsonFetcher(instance(nextFetcher), clock, logger, instance(longExecutionHelper));

        command = {
            description: 'cmdDesc',
            url: 'http://test/api',
        } as JsonFetchCommand<string>;

        when(longExecutionHelper.warn(anything())).thenCall(o => o.execute());
    });

    afterEach(() => {
        const longExecOp = capture<LongExecutionOperation<string>>(longExecutionHelper.warn).first()[0];
        expect(longExecOp.description).toIncludeMultiple(['HTTP', 'fetch', 'cmdDesc']);
    });

    describe.each([true, false])('with logger tracing %s', isLoggerTracing => {
        beforeEach(() => logger.enableLevel('Trace', isLoggerTracing));

        it('should log details of successful request when tracing %s', async () => {
            when(nextFetcher.execute(command)).thenCall(async () => {
                clock.tick(66);
                return Promise.resolve('testResult');
            });

            const result = await act();

            expect(result).toBe('testResult');
            logger.verifyLoggedCount(2);
            logger.logged(0)
                .verify('Debug', {
                    command: 'cmdDesc',
                    method: httpMethods.GET,
                    url: 'http://test/api',
                })
                .verifyMessage('Executing');
            logger.logged(1)
                .verify('Debug', {
                    command: 'cmdDesc',
                    method: httpMethods.GET,
                    url: 'http://test/api',
                    durationMillis: 66,
                    result: isLoggerTracing ? 'testResult' : ENABLE_TRACE,
                })
                .verifyMessage('Succeeded');
        });

        it('should log details of failed request when tracing %s', async () => {
            const httpError = new Error('oups');
            when(nextFetcher.execute(command)).thenCall(async () => {
                clock.tick(77);
                return Promise.reject(httpError);
            });

            const error = await expectToThrowAsync(act);

            expect(error).toBe(httpError);
            logger.verifyLoggedCount(2);
            logger.logged(0)
                .verify('Debug', {
                    command: 'cmdDesc',
                    method: httpMethods.GET,
                    url: 'http://test/api',
                })
                .verifyMessage('Executing');
            logger.logged(1)
                .verify('Debug', {
                    command: 'cmdDesc',
                    method: httpMethods.GET,
                    url: 'http://test/api',
                    durationMillis: 77,
                    error,
                })
                .verifyMessage('Failed');
        });
    });
});
