import { AppVersion } from '../../src/app-version/app-version';
import { AppVersionProvider, envVariables } from '../../src/app-version/app-version-provider';

describe(AppVersionProvider.name, () => {
    let process: NodeJS.Process;

    beforeEach(() => {
        process = { env: {} } as NodeJS.Process;
    });

    it(`should determine version correctly`, () => {
        process.env = {
            [envVariables.APP_NAME]: 'dappetizer',
            [envVariables.GIT_SHA]: 'hahaha',
            [envVariables.GIT_TAG]: 'ttt',
            [envVariables.APP_URL]: 'https://gitlab/tezos-dappetizer',
        };

        const target = new AppVersionProvider(process);

        expect(target.version).toEqual<AppVersion>({
            name: 'dappetizer',
            gitSha: 'hahaha',
            gitTag: 'ttt',
            releaseNotesUrl: 'https://gitlab/tezos-dappetizer/-/releases/ttt',
        });
    });

    it(`should handle release notes url without git tag`, () => {
        process.env = {
            [envVariables.APP_URL]: 'https://gitlab/tezos-dappetizer',
        };

        const target = new AppVersionProvider(process);

        expect(target.version.releaseNotesUrl).toBe('https://gitlab/tezos-dappetizer/-/releases/');
    });

    it(`should return empty version`, () => {
        const target = new AppVersionProvider(process);

        expect(target.version).toEqual<AppVersion>({
            name: null,
            gitSha: null,
            gitTag: null,
            releaseNotesUrl: null,
        });
    });
});
