import { instance, mock, when } from 'ts-mockito';

import { verifyCalled } from '../../../../test-utilities/mocks';
import { HttpServerMetrics, HttpServerMetricsUpdater } from '../../src/http-server/http-server-metrics';
import { HttpServerWrapper } from '../../src/http-server/http-server-wrapper';

describe(HttpServerMetricsUpdater.name, () => {
    let target: HttpServerMetricsUpdater;
    let httpServer: HttpServerWrapper;
    let connectionCountGauge: HttpServerMetrics['connectionCount'];

    beforeEach(() => {
        connectionCountGauge = mock();
        const metrics = { connectionCount: instance(connectionCountGauge) } as HttpServerMetrics;

        httpServer = mock(HttpServerWrapper);
        target = new HttpServerMetricsUpdater(metrics, instance(httpServer));
    });

    it('should report connection count', async () => {
        when(httpServer.getConnections()).thenResolve(123);

        await target.update();

        verifyCalled(connectionCountGauge.set).onceWith(123);
    });
});
