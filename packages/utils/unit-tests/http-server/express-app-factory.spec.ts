import express from 'express';
import { anything, capture, instance, mock, spy, verify, when } from 'ts-mockito';

import { ExpressAppFactory } from '../../src/http-server/express-app-factory';
import { HttpHandler } from '../../src/http-server/http-handler';

describe(ExpressAppFactory.name, () => {
    let target: ExpressAppFactory;
    let httpHandler1: HttpHandler;
    let httpHandler2: HttpHandler;
    let mockedApp: express.Application;

    beforeEach(() => {
        [httpHandler1, httpHandler2] = [mock(), mock()];
        target = new ExpressAppFactory([instance(httpHandler1), instance(httpHandler2)]);

        mockedApp = mock();
        const targetSpy = spy(target);
        when(targetSpy.createRaw()).thenReturn(instance(mockedApp));

        when(httpHandler1.urlPath).thenReturn('/foo');
        when(httpHandler2.urlPath).thenReturn('/bar');
    });

    it('should create express app with all HTTP handlers registered', () => {
        const expressApp = target.create();

        expect(expressApp).toBe(instance(mockedApp));
        verify(httpHandler1.handle(anything(), anything())).never();
        verify(httpHandler2.handle(anything(), anything())).never();

        verify(mockedApp.get(anything(), anything())).times(2);
        verifyHandler(0, httpHandler1, '/foo');
        verifyHandler(1, httpHandler2, '/bar');
    });

    function verifyHandler(index: number, httpHandler: HttpHandler, exprestedPath: string) {
        const [path, handle] = capture(mockedApp.get).byCallIndex(index);
        expect(path).toBe(exprestedPath);

        const [req1, res1] = [{} as express.Request, {} as express.Response];
        handle(req1, res1);
        verify(httpHandler.handle(req1, res1)).once();
    }
});
