import { Writable } from 'ts-essentials';
import { instance, mock, verify } from 'ts-mockito';

import { describeMember, TestLogger } from '../../../../test-utilities/mocks';
import { HttpServerConfig } from '../../src/http-server/http-server-config';
import { HttpServerWorker } from '../../src/http-server/http-server-worker';
import { HttpServerWrapper } from '../../src/http-server/http-server-wrapper';

describe(HttpServerWorker.name, () => {
    let target: HttpServerWorker;
    let httpServer: HttpServerWrapper;
    let logger: TestLogger;
    let config: Writable<HttpServerConfig>;

    beforeEach(() => {
        httpServer = mock();
        logger = new TestLogger();
        config = { value: { host: 'indexer.host', port: 66 } } as HttpServerConfig;
        target = new HttpServerWorker(instance(httpServer), config, logger);
    });

    describeMember<HttpServerWorker>('start', () => {
        it('should start listening on configured host and port', async () => {
            await target.start();

            verify(httpServer.listen(66, 'indexer.host'));
            logger.loggedSingle().verify('Information', {
                url: 'http://indexer.host:66',
            });
        });

        it('should not start listening if disabled in config', async () => {
            config.value = null;

            await target.start();

            verify(httpServer.listen(66, 'indexer.host')).never();
        });
    });

    describeMember<HttpServerWorker>('stop', () => {
        it('should close the server', async () => {
            await target.stop();

            verify(httpServer.close()).once();
        });

        it('should close the server if disabled in config', async () => {
            config.value = null;

            await target.stop();

            verify(httpServer.close()).never();
        });
    });
});
