import { instance, mock, when } from 'ts-mockito';

import { TestClock, verifyCalled } from '../../../../../test-utilities/mocks';
import { MetricsHttpBackend } from '../../../src/tezos-rpc/http-backends/metrics-http-backend';
import { EndpointGroup, MetricsRequestGrouping } from '../../../src/tezos-rpc/http-backends/metrics-request-grouping';
import { TaquitoHttpBackend } from '../../../src/tezos-rpc/http-infrastructure/taquito-http-backend';
import { TaquitoHttpRequest } from '../../../src/tezos-rpc/http-infrastructure/taquito-http-request';
import { RpcMetrics } from '../../../src/tezos-rpc/rpc-metrics';

describe(MetricsHttpBackend, () => {
    let target: TaquitoHttpBackend;
    let nextBackend: TaquitoHttpBackend;
    let rpcClientCallDuration: RpcMetrics['rpcClientCallDuration'];
    let clock: TestClock;
    let metricsRequestGrouping: MetricsRequestGrouping;
    let request: TaquitoHttpRequest;

    const act = async () => target.execute(request);

    beforeEach(() => {
        rpcClientCallDuration = mock();
        const metrics = { rpcClientCallDuration: instance(rpcClientCallDuration) } as RpcMetrics;

        [nextBackend, metricsRequestGrouping] = [mock(), mock()];
        clock = new TestClock();
        target = new MetricsHttpBackend(instance(nextBackend), metrics, clock, instance(metricsRequestGrouping));

        request = { url: 'http://tezos.org' } as TaquitoHttpRequest;
    });

    it('should report call duration', async () => {
        when(nextBackend.execute(request)).thenCall(async () => {
            clock.tick(66);
            return Promise.resolve('foo');
        });
        const requestGroup: EndpointGroup = { name: 'gg', host: 'hh' };
        when(metricsRequestGrouping.getGroup(request.url)).thenReturn(requestGroup);

        const result = await act();

        expect(result).toBe('foo');
        verifyCalled(rpcClientCallDuration.observe).onceWith(requestGroup, 66);
    });
});
