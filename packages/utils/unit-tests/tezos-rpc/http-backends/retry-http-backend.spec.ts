import * as taquitoHttp from '@taquito/http-utils';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { expectToThrowAsync, TestLogger, verifyCalled } from '../../../../../test-utilities/mocks';
import { AbortError } from '../../../src/app-control/public-api';
import { asReadonly } from '../../../src/basics/public-api';
import { MetricsRequestGrouping } from '../../../src/tezos-rpc/http-backends/metrics-request-grouping';
import { RetryHttpBackend } from '../../../src/tezos-rpc/http-backends/retry-http-backend';
import { TaquitoHttpBackend } from '../../../src/tezos-rpc/http-infrastructure/taquito-http-backend';
import { TaquitoHttpRequest } from '../../../src/tezos-rpc/http-infrastructure/taquito-http-request';
import { RpcMetrics } from '../../../src/tezos-rpc/rpc-metrics';
import { TezosNodeConfig } from '../../../src/tezos-rpc/tezos-node-config';
import { SleepHelper } from '../../../src/time/public-api';

describe(RetryHttpBackend, () => {
    let target: TaquitoHttpBackend;
    let nextBackend: TaquitoHttpBackend;
    let sleepHelper: SleepHelper;
    let rpcClientRetryCounter: RpcMetrics['rpcClientRetryCount'];
    let metricsRequestGrouping: MetricsRequestGrouping;
    let logger: TestLogger;
    let request: TaquitoHttpRequest;

    const act = async () => target.execute(request);

    beforeEach(() => {
        rpcClientRetryCounter = mock();
        const metrics = { rpcClientRetryCount: instance(rpcClientRetryCounter) } as RpcMetrics;

        [nextBackend, sleepHelper, metricsRequestGrouping] = [mock(), mock(), mock()];
        const config = { retryDelaysMillis: asReadonly([10, 20]) } as TezosNodeConfig;
        logger = new TestLogger();
        target = new RetryHttpBackend(instance(nextBackend), config, instance(sleepHelper), metrics, instance(metricsRequestGrouping), logger);

        request = {
            method: 'POST',
            url: 'http://tezos/',
        } as TaquitoHttpRequest;

        when(metricsRequestGrouping.getGroup('http://tezos/')).thenReturn({ name: 'req-group', host: 'req-host' });
    });

    it('should just pass ok result', async () => {
        when(nextBackend.execute(request)).thenResolve('foo');

        const result = await act();

        expect(result).toBe('foo');
        verify(rpcClientRetryCounter.inc(anything())).never();
        verify(sleepHelper.sleep(anything())).never();
        logger.verifyNothingLogged();
    });

    it('should retry if failed request', async () => {
        const error1 = new Error('oups 1');
        const error2 = new Error('oups 2');
        when(nextBackend.execute(request)).thenReject(error1).thenReject(error2).thenResolve('foo');

        const result = await act();

        expect(result).toBe('foo');

        verifyCalled(rpcClientRetryCounter.inc)
            .with({ name: 'req-group', host: 'req-host', delay: 10, index: 0 })
            .thenWith({ name: 'req-group', host: 'req-host', delay: 20, index: 1 })
            .thenNoMore();

        verifyCalled(sleepHelper.sleep)
            .with(10)
            .thenWith(20)
            .thenNoMore();

        logger.verifyLoggedCount(2);
        logger.logged(0).verify('Warning', {
            request: 'POST http://tezos/',
            index: 0,
            delayMillis: 10,
            error: error1,
        });
        logger.logged(1).verify('Warning', {
            request: 'POST http://tezos/',
            index: 1,
            delayMillis: 20,
            error: error2,
        });
    });

    it('should throw if failed more than configured times', async () => {
        const finalError = new Error('final oups');
        when(nextBackend.execute(request)).thenReject(new Error('oups 1')).thenReject(new Error('oups 2')).thenReject(finalError);

        const error = await expectToThrowAsync(act);

        expect(error).toBe(finalError);
        logger.verifyLoggedCount(3);
        logger.logged(2).verify('Debug', {
            request: 'POST http://tezos/',
            retries: 2,
        });
    });

    it('should not retry if abort error', async () => {
        const abortError = new AbortError();
        when(nextBackend.execute(request)).thenReject(abortError);

        const error = await expectToThrowAsync(act);

        expect(error).toBe(abortError);
    });

    it('should not retry if not-found error', async () => {
        const notFoundError = new taquitoHttp.HttpResponseError('oups', taquitoHttp.STATUS_CODE.NOT_FOUND, 'txt', 'bdy', 'url');
        when(nextBackend.execute(request)).thenReject(notFoundError);

        await expect(act).rejects.toBe(notFoundError);

        logger.loggedSingle()
            .verify('Debug', { request: 'POST http://tezos/' })
            .verifyMessage('404');
    });

    it('should retry if not-found error when getting block', async () => {
        request.url = 'http://tezos/auth/chains/main/blocks/123';
        const notFoundError = new taquitoHttp.HttpResponseError('oups', taquitoHttp.STATUS_CODE.NOT_FOUND, 'txt', 'bdy', 'url');
        when(nextBackend.execute(request)).thenReject(notFoundError).thenResolve('block123');

        const result = await act();

        expect(result).toBe('block123');

        verify(sleepHelper.sleep(anything())).times(1);
    });
});
