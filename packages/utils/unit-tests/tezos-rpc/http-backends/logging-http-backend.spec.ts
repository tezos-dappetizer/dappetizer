import { anything, capture, instance, mock, when } from 'ts-mockito';

import { expectToThrowAsync, TestClock, TestLogger } from '../../../../../test-utilities/mocks';
import { ENABLE_TRACE } from '../../../../utils/src/public-api';
import { LoggingHttpBackend } from '../../../src/tezos-rpc/http-backends/logging-http-backend';
import { TaquitoHttpBackend } from '../../../src/tezos-rpc/http-infrastructure/taquito-http-backend';
import { TaquitoHttpRequest } from '../../../src/tezos-rpc/http-infrastructure/taquito-http-request';
import { LongExecutionHelper, LongExecutionOperation } from '../../../src/time/long-execution-helper';

describe(LoggingHttpBackend.name, () => {
    let target: TaquitoHttpBackend;
    let nextBackend: TaquitoHttpBackend;
    let clock: TestClock;
    let logger: TestLogger;
    let longExecutionHelper: LongExecutionHelper;
    let request: TaquitoHttpRequest;

    const act = async () => target.execute(request);

    beforeEach(() => {
        [nextBackend, longExecutionHelper] = [mock(), mock()];
        clock = new TestClock();
        logger = new TestLogger();
        target = new LoggingHttpBackend(instance(nextBackend), clock, logger, instance(longExecutionHelper));

        when(longExecutionHelper.warn(anything())).thenCall(async (o: LongExecutionOperation) => o.execute());

        request = {
            method: 'POST',
            url: 'http://tezos/',
        } as TaquitoHttpRequest;
    });

    afterEach(() => {
        const longExecCalls = capture<LongExecutionOperation<string>>(longExecutionHelper.warn).first();
        expect(longExecCalls).toHaveLength(1);
        expect(longExecCalls[0].description).toBe('POST http://tezos/');
    });

    describe.each([true, false])('with tracing %s', isLoggerTracing => {
        beforeEach(() => logger.enableLevel('Trace', isLoggerTracing));

        it('should log details of successful request when tracing %s', async () => {
            when(nextBackend.execute(request)).thenCall(async () => {
                clock.tick(66);
                return Promise.resolve('foo-result');
            });

            const result = await act();

            expect(result).toBe('foo-result');
            logger.verifyLoggedCount(2);
            logger.logged(0).verify('Debug', {
                request: 'POST http://tezos/',
                requestData: isLoggerTracing ? request.data : ENABLE_TRACE,
            });
            logger.logged(1).verify('Debug', {
                request: 'POST http://tezos/',
                durationMillis: 66,
                result: isLoggerTracing ? 'foo-result' : ENABLE_TRACE,
            });
        });

        it('should log details of failed request when tracing %s', async () => {
            const httpError = new Error('oups');
            when(nextBackend.execute(request)).thenCall(async () => {
                clock.tick(77);
                return Promise.reject(httpError);
            });

            const error = await expectToThrowAsync(act);

            expect(error).toBe(httpError);
            logger.verifyLoggedCount(2);
            logger.logged(0).verify('Debug', {
                request: 'POST http://tezos/',
                requestData: isLoggerTracing ? request.data : ENABLE_TRACE,
            });
            logger.logged(1).verify('Debug', {
                request: 'POST http://tezos/',
                durationMillis: 77,
                error,
            });
        });
    });
});
