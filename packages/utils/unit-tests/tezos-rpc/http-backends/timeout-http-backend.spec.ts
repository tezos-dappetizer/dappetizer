import { anything, instance, mock, when } from 'ts-mockito';

import { verifyCalled } from '../../../../../test-utilities/mocks';
import { TimeoutHttpBackend } from '../../../src/tezos-rpc/http-backends/timeout-http-backend';
import { TaquitoHttpBackend } from '../../../src/tezos-rpc/http-infrastructure/taquito-http-backend';
import { TaquitoHttpRequest } from '../../../src/tezos-rpc/http-infrastructure/taquito-http-request';

describe(TimeoutHttpBackend.name, () => {
    let target: TimeoutHttpBackend;
    let nextBackend: TaquitoHttpBackend;
    let request: TaquitoHttpRequest;

    const act = async () => target.execute(request);

    beforeEach(() => {
        nextBackend = mock();
        target = new TimeoutHttpBackend(123, instance(nextBackend));

        request = { url: 'http://tezos/blocks' } as TaquitoHttpRequest;

        when(nextBackend.execute(anything())).thenResolve('resultVal');
    });

    it('should set timeout if request has none', async () => {
        const result = await act();

        expect(result).toBe('resultVal');
        verifyCalled(nextBackend.execute).onceWith({
            url: 'http://tezos/blocks',
            timeout: 123,
        } as TaquitoHttpRequest);
        expect(request.timeout).toBeUndefined(); // Original request should be untouched.
    });

    it('should honor existing timeout', async () => {
        request.timeout = 456;

        const result = await act();

        expect(result).toBe('resultVal');
        verifyCalled(nextBackend.execute).onceWith({
            url: 'http://tezos/blocks',
            timeout: 456,
        } as TaquitoHttpRequest);
    });
});
