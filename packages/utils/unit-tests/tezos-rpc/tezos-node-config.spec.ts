import { StrictOmit } from 'ts-essentials';

import { verifyPropertyNames } from '../../../../test-utilities/mocks';
import { DappetizerConfig } from '../../../indexer/src/config/dappetizer-config';
import { ConfigObjectElement } from '../../src/configuration/public-api';
import { TezosNodeConfig } from '../../src/tezos-rpc/tezos-node-config';
import { TezosNodeDappetizerConfig } from '../../src/tezos-rpc/tezos-node-dappetizer-config';

type TezosNodeConfigValues = StrictOmit<TezosNodeConfig, 'getDiagnostics'>;

describe(TezosNodeConfig.name, () => {
    function act(thisJson: DappetizerConfig['networks'][string]['tezosNode']) {
        const rootJson = { [TezosNodeConfig.ROOT_NAME]: thisJson };
        return new TezosNodeConfig(new ConfigObjectElement(rootJson, 'file', '$'));
    }

    it('should be created with all values specified', () => {
        const inputJson = {
            url: 'https://api.tezos.org.ua/',
            retryDelaysMillis: [1, 2],
            monitor: {
                chunkCount: {
                    warn: 9,
                    fail: 10,
                },
                reconnectIfStuckMillis: 3_000,
            },
            health: {
                resultCacheMillis: 33,
                timeoutMillis: 666,
                unhealthyAfterMillis: 88_000,
            },
            timeoutMillis: 7_777,
        } satisfies TezosNodeDappetizerConfig;

        const config = act(inputJson);

        expect(config).toEqual<TezosNodeConfigValues>(inputJson);
    });

    it.each([
        ['only necessary', { url: 'https://api.tezos.org.ua/' }],
        ['empty monitor', { url: 'https://api.tezos.org.ua/', monitor: {} }],
        ['empty monitor.chunkCount', { url: 'https://api.tezos.org.ua/', monitor: { chunkCount: {} } }],
        ['empty health', { url: 'https://api.tezos.org.ua/', health: {} }],
    ])('should be created with defaults if %s', (_desc, inputJson) => {
        const config = act(inputJson);

        expect(config).toEqual<TezosNodeConfigValues>({
            url: 'https://api.tezos.org.ua/',
            retryDelaysMillis: [50, 500, 1_000, 2_000, 4_000],
            monitor: {
                chunkCount: {
                    warn: 100,
                    fail: 1_000,
                },
                reconnectIfStuckMillis: 180_000,
            },
            health: {
                resultCacheMillis: 5_000,
                timeoutMillis: 1_000,
                unhealthyAfterMillis: 70_000,
            },
            timeoutMillis: 30_000,
        });
    });

    it('should expose its properties in DappetizerConfig', () => {
        expect(TezosNodeConfig.ROOT_NAME).toBe<keyof DappetizerConfig['networks'][string]>('tezosNode');

        const config = act({ url: 'https://api.tezos.org.ua/' });
        verifyPropertyNames<TezosNodeDappetizerConfig>(config, ['url', 'retryDelaysMillis', 'monitor', 'health', 'timeoutMillis']);

        type MonitorConfig = NonNullable<TezosNodeDappetizerConfig['monitor']>;
        verifyPropertyNames<MonitorConfig>(config.monitor, ['chunkCount', 'reconnectIfStuckMillis']);
        verifyPropertyNames<MonitorConfig['chunkCount']>(config.monitor.chunkCount, ['fail', 'warn']);

        type HealthConfig = NonNullable<TezosNodeDappetizerConfig['health']>;
        verifyPropertyNames<HealthConfig>(config.health, ['resultCacheMillis', 'timeoutMillis', 'unhealthyAfterMillis']);
    });
});
