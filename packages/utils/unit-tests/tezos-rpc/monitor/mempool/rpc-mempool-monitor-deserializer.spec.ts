import { expectToThrow } from '../../../../../../test-utilities/mocks';
import { RpcMempoolMonitorDeserializer } from '../../../../src/tezos-rpc/monitor/mempool/rpc-mempool-monitor-deserializer';

describe(RpcMempoolMonitorDeserializer.name, () => {
    const target = new RpcMempoolMonitorDeserializer();

    it.each([
        ['valid', [{ signature: 'sig1' }, { signature: 'sig2' }]],
        ['empty', []],
    ])('should cast data if %s mempool operation groups', (_desc, data) => {
        const groups = target.deserialize(data);

        expect(groups).toBe(data);
    });

    it.each([
        ['undefined', undefined, ['an array']],
        ['null', null, ['an array']],
        ['not array', 'omg', ['an array']],
        ['item with missing signature', [{ signature: 's1' }, 'wtf'], ['objects', '1. item']],
        ['item with missing signature', [{ signature: 's1' }, {}], ['signature', '1. item']],
        ['item with mismatched signature', [{ signature: 123 }, { signature: 's2' }], ['signature', '0. item']],
        ['item with white-space signature', [{ signature: 's1' }, { signature: '  ' }], ['signature', '1. item']],
    ])('should throw if data is %s', (_desc, data, expectedError) => {
        const error = expectToThrow(() => target.deserialize(data));

        expect(error.message).toIncludeMultiple(expectedError);
    });
});
