import { asyncToArray, asyncWrap } from 'iter-tools';
import { Gauge } from 'prom-client';
import { instance, mock, when } from 'ts-mockito';

import { verifyCalled } from '../../../../../../test-utilities/mocks';
import { AsyncIterableProvider } from '../../../../src/basics/public-api';
import { RpcBlockMetricsMonitor } from '../../../../src/tezos-rpc/monitor/block/rpc-block-metrics-monitor';
import { RpcMonitorBlockHeader } from '../../../../src/tezos-rpc/public-api';
import { RpcMetrics } from '../../../../src/tezos-rpc/rpc-metrics';

describe(RpcBlockMetricsMonitor.name, () => {
    let target: RpcBlockMetricsMonitor;
    let nextProvider: AsyncIterableProvider<RpcMonitorBlockHeader>;
    let blockLevelGauge: Gauge;

    beforeEach(() => {
        blockLevelGauge = mock();
        const metrics = { monitorBlockLevel: instance(blockLevelGauge) } as RpcMetrics;

        nextProvider = mock();
        target = new RpcBlockMetricsMonitor(instance(nextProvider), metrics);
    });

    it('should call tezos monitor block level metrics for each iterated block', async () => {
        const nextBlocks = [
            { hash: 'hash123', level: 11 } as RpcMonitorBlockHeader,
            { hash: 'hash456', level: 12 } as RpcMonitorBlockHeader,
        ];
        when(nextProvider.iterate()).thenReturn(asyncWrap(nextBlocks));

        const blocks = await asyncToArray(target.iterate());

        expect(blocks).toEqual(nextBlocks);
        verifyCalled(blockLevelGauge.set)
            .with(11)
            .thenWith(12)
            .thenNoMore();
    });
});
