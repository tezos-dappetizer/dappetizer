import { asyncToArray, asyncWrap } from 'iter-tools';
import { range } from 'lodash';
import { Response } from 'node-fetch';
import { deepEqual, instance, mock, when } from 'ts-mockito';

import { expectToThrowAsync, TestLogger } from '../../../../../../test-utilities/mocks';
import { AbortError } from '../../../../src/app-control/public-api';
import { HttpFetcher } from '../../../../src/http-fetch/public-api';
import { DownloadDataMonitor } from '../../../../src/tezos-rpc/monitor/generic/download-data-monitor';
import { TezosNodeConfig } from '../../../../src/tezos-rpc/tezos-node-config';

describe(DownloadDataMonitor.name, () => {
    let target: DownloadDataMonitor;
    let httpFetcher: HttpFetcher;
    let abortSignal: AbortSignal;
    let logger: TestLogger;
    const url = 'http://tezos/acess-id/monitor';

    const act = async (options: { take: number }) => asyncToArray(asyncTakeFixed(options.take, target.iterate(abortSignal)));

    beforeEach(() => {
        const config = {
            url: 'http://tezos/acess-id',
            monitor: {
                chunkCount: { warn: 5, fail: 7 },
            },
        } as TezosNodeConfig;
        httpFetcher = mock();
        abortSignal = {} as AbortSignal;
        logger = new TestLogger();
        target = new DownloadDataMonitor(logger, '/monitor', config, instance(httpFetcher));
    });

    it('should continuously download data', async () => {
        setupFetch([
            '{ "id": "007", "name": "James Bond" }',
            { toString: () => '{ "id": "001", "name": "Johnny English" }' }, // Imitates Buffer.
        ]);

        const items = await act({ take: 2 });

        expect(items).toEqual([
            { id: '007', name: 'James Bond' },
            { id: '001', name: 'Johnny English' },
        ]);
        logger.verifyLoggedCount(2);
        logger.logged(0).verify('Information', { url }).verifyMessage('Connecting');
        logger.logged(1).verify('Information', { url }).verifyMessage('Connected');
    });

    it('should concat JSON split to multiple chunks', async () => {
        setupFetch(['{ "id":', ' "007", "na', 'me": "James Bond" }']);

        const items = await act({ take: 1 });

        expect(items).toEqual([{ id: '007', name: 'James Bond' }]);
        logger.verifyNotLogged('Error', 'Warning');
    });

    it('should warn if JSON split to more than configured number of chunks ', async () => {
        setupFetch(['{ ', '"id":', ' "007", "na', 'me":', ' "James ', 'Bond" }']);

        const items = await act({ take: 1 });

        expect(items).toEqual([{ id: '007', name: 'James Bond' }]);
        logger.loggedSingle('Warning').verifyData({ url, chunkCount: 6 });
        logger.verifyNotLogged('Error');
    });

    it('should fail if JSON split to more than configured number of chunks ', async () => {
        setupFetch(range(7).map(() => '{'));

        await runThrowTest({ expectedError: ['SyntaxError', '7 chunks', url] });
    });

    it('should fail if not OK response', async () => {
        whenFetch().thenResolve({ status: 512, statusText: 'WTF' } as Response);

        await runThrowTest({ expectedError: ['connect', '512', 'WTF', url] });
    });

    it('should fail if connection ended', async () => {
        setupFetch(['{ "id": "007", "name": "James Bond" }']);

        const error = await expectToThrowAsync(async () => act({ take: 2 }));

        expect(error.message).toInclude('Connection ended');
    });

    it('should end iteration if aborted on connect', async () => {
        whenFetch().thenReject(new AbortError());

        const items = await act({ take: 1 });

        expect(items).toBeEmpty();
        logger.loggedLast().verify('Debug').verifyMessage('aborted');
    });

    it('should end iteration if aborted on response reading', async () => {
        setupFetchStream(() => {
            throw new AbortError();
        });

        const items = await act({ take: 1 });

        expect(items).toBeEmpty();
        logger.loggedLast().verify('Debug').verifyMessage('aborted');
    });

    function whenFetch() {
        return when(httpFetcher.fetch(url, deepEqual({ signal: abortSignal })));
    }

    function setupFetch(jsons: (string | { toString(): string })[]) {
        setupFetchStream(() => asyncWrap(jsons));
    }

    function setupFetchStream(iterateStream: () => AsyncIterable<string | { toString(): string }>) {
        whenFetch().thenResolve({
            status: 200,
            body: { [Symbol.asyncIterator]: iterateStream } as unknown as NodeJS.ReadableStream,
        } as Response);
    }

    async function runThrowTest(options: { expectedError: string[] }) {
        const error = await expectToThrowAsync(async () => act({ take: 1 }));

        expect(error.message).toIncludeMultiple(options.expectedError);
    }
});

async function *asyncTakeFixed<T>(maxCount: number, iterable: AsyncIterableIterator<T>): AsyncIterableIterator<T> {
    let remainingCount = maxCount;
    for await (const item of iterable) {
        yield item;
        if (--remainingCount === 0) {
            break;
        }
    }
}
