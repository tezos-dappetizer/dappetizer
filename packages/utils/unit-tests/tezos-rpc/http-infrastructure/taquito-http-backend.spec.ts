import * as taquitoHttp from '@taquito/http-utils';
import axiosAbstraction, { AxiosError, RawAxiosRequestConfig, AxiosResponse } from 'axios';
import { anything, capture, instance, mock, spy, when } from 'ts-mockito';

import { describeMember, expectToThrowAsync, verifyCalled } from '../../../../../test-utilities/mocks';
import { contentTypes, httpHeaders } from '../../../src/http-fetch/public-api';
import { TaquitoHttpBackend } from '../../../src/tezos-rpc/http-infrastructure/taquito-http-backend';
import { TaquitoHttpRequest } from '../../../src/tezos-rpc/http-infrastructure/taquito-http-request';

describe(TaquitoHttpBackend.name, () => {
    let target: TaquitoHttpBackend;
    let axios: typeof axiosAbstraction;

    beforeEach(() => {
        axios = mock();
        target = new TaquitoHttpBackend(instance(axios));
    });

    describeMember<typeof target>('createRequest', () => {
        let targetSpy: TaquitoHttpBackend;
        let options: taquitoHttp.HttpRequestOptions;
        let receivedRequest: TaquitoHttpRequest | null;

        beforeEach(() => {
            targetSpy = spy(target);
            options = { url: 'http://tezos/blocks/123' };
            receivedRequest = null;

            when(targetSpy.execute(anything())).thenCall(async request => {
                receivedRequest = request;
                return Promise.resolve('resultVal');
            });
        });

        it('should convert explicit options to request correctly', async () => {
            options.headers = { UserAgent: 'James Bond' };
            options.json = true;
            options.method = 'POST';
            options.query = { q: 1, p: '2' };
            options.timeout = 100;

            const result = await target.createRequest(options, 'dataVal');

            expect(result).toBe('resultVal');
            verifyCalled(targetSpy.execute).onceWith({
                data: 'dataVal',
                headers: { UserAgent: 'James Bond' },
                json: true,
                method: 'POST',
                signal: undefined,
                timeout: 100,
                url: 'http://tezos/blocks/123?q=1&p=2',
            });
        });

        it.each([
            ['undefined', undefined, {}],
            ['empty', {}, {}],
            ['non-empty', { UserAgent: 'James Bond' }, { UserAgent: 'James Bond' }],
        ])('should convert %s headers correctly', async (_desc, inputHeaders, expectedHeaders) => {
            options.headers = inputHeaders;

            await target.createRequest(options);

            expect(receivedRequest!.headers).toEqual(expectedHeaders);
        });

        it.each([
            [undefined, true],
            [false, false],
            [true, true],
        ])('should convert json %s to %s', async (inputJson, expectedJson) => {
            options.json = inputJson;

            await target.createRequest(options);

            expect(receivedRequest!.json).toBe(expectedJson);
        });

        it.each([
            [undefined, 'GET'],
            ['GET', 'GET'],
            ['POST', 'POST'],
        ] as const)('should convert method %s to %s', async (inputMethod, expectedMethod) => {
            options.method = inputMethod;

            await target.createRequest(options);

            expect(receivedRequest!.method).toBe(expectedMethod);
        });

        it.each([undefined, 100])('should convert timeout %s to %s', async timeout => {
            options.timeout = timeout;

            await target.createRequest(options);

            expect(receivedRequest!.timeout).toBe(timeout);
        });
    });

    describeMember<typeof target>('execute', () => {
        let request: TaquitoHttpRequest;

        const act = async () => target.execute(request);

        beforeEach(() => {
            request = {
                data: 'reqData',
                headers: { UserAgent: 'James Bond' },
                json: false,
                method: 'POST',
                signal: 'mockedSignal' as any,
                timeout: 100,
                url: 'http://tezos/blocks',
            };
        });

        it.each([
            [true, 'json', contentTypes.JSON],
            [false, 'text', contentTypes.TEXT],
        ] as const)('should execute request correctly if json is %s', async (json, expectedType, expectedAccept) => {
            request.json = json;
            when(axios.request(anything())).thenResolve({ data: 'resData' } as AxiosResponse);

            const result = await act();

            expect(result).toBe('resData');
            const receivedRequest = capture<RawAxiosRequestConfig>(axios.request).first()[0];
            expect(receivedRequest).toEqual<RawAxiosRequestConfig>({
                data: 'reqData',
                headers: {
                    UserAgent: 'James Bond',
                    [httpHeaders.ACCEPT]: expectedAccept,
                    [httpHeaders.ACCEPT_ENCODING]: 'gzip, deflate',
                },
                method: 'POST',
                responseType: expectedType,
                signal: 'mockedSignal' as any,
                timeout: 100,
                url: 'http://tezos/blocks',
            });
        });

        it.each([
            ['json', { val: 123 }, '{"val":123}'],
            ['text', 'resData', 'resData'],
        ])('should throw with response details if axios error with %s response', async (_desc, responseData, expectedData) => {
            when(axios.request(anything())).thenReject(new AxiosError('oups', undefined, undefined, undefined, {
                data: responseData,
                status: 403,
                statusText: 'Go away!',
            } as AxiosResponse));

            const error = await expectToThrowAsync(act, taquitoHttp.HttpResponseError);

            expect(error.message).toIncludeMultiple(['POST http://tezos/blocks', '403', 'Go away!', expectedData]);
            expect(error.status).toBe(403);
            expect(error.statusText).toBe('Go away!');
            expect(error.body).toBe(expectedData);
            expect(error.url).toBe('http://tezos/blocks');
        });

        it.each([
            ['axious', new AxiosError('oups')],
            ['other', new Error('oups')],
        ])('should throw if %s error', async (_desc, requestError) => {
            when(axios.request(anything())).thenReject(requestError);

            const error = await expectToThrowAsync(act, taquitoHttp.HttpRequestFailed);

            expect(error.message).toIncludeMultiple(['POST http://tezos/blocks', 'oups']);
        });
    });
});
