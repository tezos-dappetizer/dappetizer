import { container as globalDIContainer, DependencyContainer } from 'tsyringe';

import { CachedHealthCheck } from '../../../src/health/checks/cached-health-check';
import { registerCachedHealthCheck } from '../../../src/health/checks/cached-health-check-registration';
import { HEALTH_CHECKS_DI_TOKEN } from '../../../src/health/health-check-di-token';
import { HealthCheck } from '../../../src/health/public-api';
import { Clock, CLOCK_DI_TOKEN } from '../../../src/time/public-api';

describe(registerCachedHealthCheck.name, () => {
    let diContainer: DependencyContainer;
    let healthCheckToCache: HealthCheck;
    let clock: Clock;

    beforeEach(() => {
        diContainer = globalDIContainer.createChildContainer();
        healthCheckToCache = {} as HealthCheck;
        clock = {} as Clock;

        diContainer.registerInstance(CLOCK_DI_TOKEN, clock);
    });

    it('should register cached health check', () => {
        registerCachedHealthCheck(diContainer, () => ({ cacheTimeMillis: 123, healthCheckToCache }));

        const checks = diContainer.resolveAll(HEALTH_CHECKS_DI_TOKEN);
        expect(checks).toHaveLength(1);
        expect(checks[0]).toBeInstanceOf(CachedHealthCheck);
        expect(checks[0]).toEqual({
            checkToCache: healthCheckToCache,
            cacheTimeMillis: 123,
            clock,
        });
    });

    it.each([0, -10])('should register original health check if %s cache time', cacheTimeMillis => {
        registerCachedHealthCheck(diContainer, () => ({ cacheTimeMillis, healthCheckToCache }));

        const checks = diContainer.resolveAll(HEALTH_CHECKS_DI_TOKEN);
        expect(checks).toHaveLength(1);
        expect(checks[0]).toBe(healthCheckToCache);
    });
});
