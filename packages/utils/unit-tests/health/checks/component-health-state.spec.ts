import { TestClock } from '../../../../../test-utilities/mocks';
import { ComponentHealthState } from '../../../src/health/checks/component-health-state';
import { HealthCheckResult, HealthStatus } from '../../../src/health/health-check';

describe(ComponentHealthState.name, () => {
    let target: ComponentHealthState;
    let clock: TestClock;

    beforeEach(() => {
        clock = new TestClock();
        target = new ComponentHealthState('Foo', clock);
    });

    it('should initially return healthy', () => {
        const result = target.checkHealth();

        expect(result.status).toBe(HealthStatus.Healthy);
        expect(typeof result.data).toBe('string');
        expect(result.evaluatedTime).toBeUndefined();
    });

    it('should return result which was set', () => {
        target.set(HealthStatus.Degraded, { value: 123 });
        const result = target.checkHealth();

        expect(result).toEqual<HealthCheckResult>({
            status: HealthStatus.Degraded,
            data: { value: 123 },
            evaluatedTime: clock.nowDate,
        });
    });
});
