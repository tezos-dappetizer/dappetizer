import { instance, mock, when } from 'ts-mockito';

import { verifyCalled } from '../../../../test-utilities/mocks';
import { AGGREGATED_HEALTH_NAME, HealthMetrics, HealthMetricsUpdater } from '../../src/health/health-metrics';
import { HealthProvider, HealthReport } from '../../src/health/health-provider';
import { HealthCheckResult, HealthStatus } from '../../src/health/public-api';

describe(HealthMetricsUpdater.name, () => {
    let target: HealthMetricsUpdater;
    let healthProvider: HealthProvider;
    let unhealthyGauge: HealthMetrics['unhealthyCount'];

    beforeEach(() => {
        unhealthyGauge = mock();
        const metrics = { unhealthyCount: instance(unhealthyGauge) } as HealthMetrics;

        healthProvider = mock(HealthProvider);
        target = new HealthMetricsUpdater(metrics, instance(healthProvider));
    });

    it.each([
        [true, 0],
        [false, 1],
    ])('should update health metrics if aggregated isHealthy is %s', async (aggregIsHealthy, expectedAggregated) => {
        when(healthProvider.generateHealthReport()).thenResolve({
            isHealthy: aggregIsHealthy,
            results: {
                good: { status: HealthStatus.Healthy } as HealthCheckResult,
                bad: { status: HealthStatus.Unhealthy } as HealthCheckResult,
                ugly: { status: HealthStatus.Degraded } as HealthCheckResult,
            } as Record<string, HealthCheckResult>,
        } as HealthReport);

        await target.update();

        verifyCalled(unhealthyGauge.set)
            .with({ name: AGGREGATED_HEALTH_NAME }, expectedAggregated)
            .thenWith({ name: 'good' }, 0)
            .thenWith({ name: 'bad' }, 1)
            .thenWith({ name: 'ugly' }, 0)
            .thenNoMore();
    });
});
