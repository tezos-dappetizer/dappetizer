import express from 'express';
import { Writable } from 'ts-essentials';
import { instance, mock, verify, when } from 'ts-mockito';

import { TestLogger } from '../../../../../test-utilities/mocks';
import { HealthCheckResult, HealthStatus } from '../../../src/health/health-check';
import { HealthProvider, HealthReport } from '../../../src/health/health-provider';
import { HealthStatusHttpHandler } from '../../../src/health/http-handlers/health-status-http-handler';
import { contentTypes, httpHeaders } from '../../../src/http-fetch/public-api';

describe(HealthStatusHttpHandler.name, () => {
    let target: HealthStatusHttpHandler;
    let healthProvider: HealthProvider;
    let logger: TestLogger;

    let response: express.Response;
    let report: Writable<HealthReport>;

    const act = async () => target.handle(null!, instance(response));

    beforeEach(() => {
        [healthProvider, response] = [mock(), mock()];
        logger = new TestLogger();
        target = new HealthStatusHttpHandler(instance(healthProvider), logger);

        report = {
            evaluatedOn: new Date(),
            results: { ['foo' as string]: { status: HealthStatus.Healthy } as HealthCheckResult },
        } as typeof report;
        when(healthProvider.generateHealthReport()).thenResolve(report);
    });

    it('should respond with full health report', async () => {
        await act();

        verify(response.setHeader(httpHeaders.CONTENT_TYPE, contentTypes.JSON)).once();
        verify(response.send(JSON.stringify(report, null, 2))).once();
        logger.loggedSingle().verify('Information', { report });
    });

    it('should log warning if some result is degraded', async () => {
        report.results = {
            ['foo' as string]: { status: HealthStatus.Healthy } as HealthCheckResult,
            ['omg' as string]: { status: HealthStatus.Degraded } as HealthCheckResult,
        };

        await act();

        logger.loggedSingle().verify('Warning', { report, degradedChecks: ['omg'] });
    });

    it('should log warning if some result is not healthy', async () => {
        report.results = {
            ['foo' as string]: { status: HealthStatus.Healthy } as HealthCheckResult,
            ['omg' as string]: { status: HealthStatus.Degraded } as HealthCheckResult,
            ['lol' as string]: { status: HealthStatus.Unhealthy } as HealthCheckResult,
        };

        await act();

        logger.loggedSingle().verify('Error', { report, degradedChecks: ['omg'], unhealthyChecks: ['lol'] });
    });
});
