import { expectToThrow } from '../../../../../test-utilities/mocks';
import { LoggedPackagesProvider } from '../../../src/logging/package-info/logged-packages-provider';

describe(LoggedPackagesProvider.name, () => {
    it('should construct object from package infos', () => {
        const target = new LoggedPackagesProvider([
            { name: 'dappetizer', version: '1.1', commitHash: 'haha' },
            { name: 'myIndexer', version: '2.2', commitHash: 'hehe' },
        ]);

        expect(target.packages).toEqual<LoggedPackagesProvider['packages']>({
            dappetizer: { version: '1.1', commitHash: 'haha' },
            myIndexer: { version: '2.2', commitHash: 'hehe' },
        });
    });

    it('should throw if package specified multiple times', () => {
        const error = expectToThrow(() => new LoggedPackagesProvider([
            { name: 'dappetizer', version: '1.1', commitHash: 'haha' },
            { name: 'dappetizer', version: '2.2', commitHash: 'hehe' },
        ]));

        expect(error.message).toIncludeMultiple([
            '{"name":"dappetizer","version":"1.1","commitHash":"haha"}',
            '{"name":"dappetizer","version":"2.2","commitHash":"hehe"}',
        ]);
    });
});
