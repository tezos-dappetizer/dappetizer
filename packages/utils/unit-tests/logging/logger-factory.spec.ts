import { instance, mock, when } from 'ts-mockito';
import { DependencyContainer } from 'tsyringe';

import { Logger, LOGGER_FACTORY_DI_TOKEN, LoggerFactory, resolveLogger } from '../../src/logging/public-api';

describe(resolveLogger.name, () => {
    it('should resolve from DI container', () => {
        const diContainer = mock<DependencyContainer>();
        const factory = mock<LoggerFactory>();
        const testLogger = {} as Logger;
        when(diContainer.resolve(LOGGER_FACTORY_DI_TOKEN)).thenReturn(instance(factory));
        when(factory.getLogger('testCat')).thenReturn(testLogger);

        const logger = resolveLogger(instance(diContainer), 'testCat');

        expect(logger).toBe(testLogger);
    });
});
