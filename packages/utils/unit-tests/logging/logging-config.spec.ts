import { StrictOmit } from 'ts-essentials';

import { verifyPropertyNames } from '../../../../test-utilities/mocks';
import { DappetizerConfig } from '../../../indexer/src/config/dappetizer-config';
import { getEnumValues } from '../../src/basics/public-api';
import { ConfigObjectElement } from '../../src/configuration/public-api';
import { LogLevel } from '../../src/logging/log-entry';
import { LogFormat, LoggingConfig, OFF_LOG_LEVEL } from '../../src/logging/logging-config';
import { LoggingDappetizerConfig } from '../../src/logging/logging-dappetizer-config';

type LoggingConfigValues = StrictOmit<LoggingConfig, 'getDiagnostics'>;

describe(LoggingConfig.name, () => {
    function act(thisJson: DappetizerConfig['logging']) {
        const rootJson = { [LoggingConfig.ROOT_NAME]: thisJson };
        return new LoggingConfig(new ConfigObjectElement(rootJson, 'file', '$'));
    }

    it('should be created with all values specified', () => {
        const inputJson = {
            console: {
                minLevel: LogLevel.Debug,
                format: LogFormat.Json,
            },
            file: {
                minLevel: LogLevel.Trace,
                format: LogFormat.ColoredMessages,
                path: 'logs/file.json',
                maxSize: '100m',
                maxFiles: '7d',
            },
            globalData: { globalKey: 'dataValue' },
            maxStringLength: 123,
        };

        const config = act(inputJson);

        expect(config).toEqual<LoggingConfigValues>(inputJson);
    });

    it('should be created disabled if min level is "off"', () => {
        const inputJson: LoggingDappetizerConfig = {
            console: {
                minLevel: OFF_LOG_LEVEL,
                format: LogFormat.Json,
            },
            file: {
                minLevel: OFF_LOG_LEVEL,
                format: LogFormat.ColoredMessages,
                path: 'logs/file.json',
                maxSize: '100m',
                maxFiles: '7d',
            },
        };

        const config = act(inputJson);

        expect(config).toEqual<LoggingConfigValues>({
            console: null,
            file: null,
            globalData: undefined,
            maxStringLength: Number.MAX_SAFE_INTEGER,
        });
    });

    it.each([
        ['no root element', undefined],
        ['empty root element', {}],
        ['empty nested elements', { console: {}, file: {} }],
    ])('should be created with defaults if %s', (_desc, inputJson) => {
        const config = act(inputJson);

        expect(config).toEqual<LoggingConfigValues>({
            console: {
                minLevel: LogLevel.Information,
                format: LogFormat.ColoredMessages,
            },
            file: null,
            globalData: undefined,
            maxStringLength: Number.MAX_SAFE_INTEGER,
        });
    });

    it('should expose its properties in DappetizerConfig', () => {
        expect(LoggingConfig.ROOT_NAME).toBe<keyof DappetizerConfig>('logging');

        const expectedLevels = [...getEnumValues(LogLevel), OFF_LOG_LEVEL];
        const expectedFormats = getEnumValues(LogFormat);
        const config = act({ file: { minLevel: 'debug', path: 'file.jsonl' } });
        verifyPropertyNames<LoggingDappetizerConfig>(config, ['console', 'file', 'globalData', 'maxStringLength']);

        type ConsoleConfig = NonNullable<LoggingDappetizerConfig['console']>;
        const consoleLevels: ConsoleConfig['minLevel'][] = ['off', 'critical', 'error', 'warning', 'information', 'debug', 'trace'];
        const consoleFormats: ConsoleConfig['format'][] = ['json', 'messages', 'coloredMessages', 'coloredSimpleMessages'];
        verifyPropertyNames<ConsoleConfig>(config.console!, ['format', 'minLevel']);
        expect(consoleLevels).toIncludeSameMembers(expectedLevels);
        expect(consoleFormats).toIncludeSameMembers(expectedFormats);

        type FileConfig = NonNullable<LoggingDappetizerConfig['file']>;
        const fileLevels: FileConfig['minLevel'][] = ['off', 'critical', 'error', 'warning', 'information', 'debug', 'trace'];
        const fileFormats: FileConfig['format'][] = ['json', 'messages', 'coloredMessages', 'coloredSimpleMessages'];
        verifyPropertyNames<FileConfig>(config.file!, ['format', 'minLevel', 'path', 'maxSize', 'maxFiles']);
        expect(fileLevels).toIncludeSameMembers(expectedLevels);
        expect(fileFormats).toIncludeSameMembers(expectedFormats);
    });
});
