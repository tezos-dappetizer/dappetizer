import { Writable } from 'ts-essentials';

import { LoggedStringLimiter } from '../../../src/logging/impl/logged-string-limiter';
import { NullRootLogger } from '../../../src/logging/impl/null-root-logger';
import { LoggingConfig } from '../../../src/logging/logging-config';
import { WinstonRootLoggerFactory } from '../../../src/logging/winston/winston-root-logger-factory';

describe(WinstonRootLoggerFactory.name, () => {
    let target: WinstonRootLoggerFactory;
    let loggingConfig: Writable<LoggingConfig>;
    let limiter: LoggedStringLimiter;

    beforeEach(() => {
        loggingConfig = {} as LoggingConfig;
        limiter = {} as LoggedStringLimiter;
        target = new WinstonRootLoggerFactory(loggingConfig, limiter);
    });

    it(`should return ${NullRootLogger.name} if no transports`, () => {
        loggingConfig.console = null;
        loggingConfig.file = null;

        const logger = target.create();

        expect(logger).toBeInstanceOf(NullRootLogger);
    });
});
