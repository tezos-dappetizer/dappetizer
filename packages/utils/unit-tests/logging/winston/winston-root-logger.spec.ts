import { anyFunction, instance, mock, verify, when } from 'ts-mockito';
import { Logger } from 'winston';
import DailyRotateFile from 'winston-daily-rotate-file';
import TransportStream from 'winston-transport';

import { describeMemberFactory } from '../../../../../test-utilities/mocks';
import { LogEntry, LogLevel } from '../../../src/logging/public-api';
import { RootLogger } from '../../../src/logging/root-logger';
import { WinstonRootLogger } from '../../../src/logging/winston/winston-root-logger';

describe(WinstonRootLogger.name, () => {
    let target: RootLogger;
    let winstonLogger: Logger;

    beforeEach(() => {
        winstonLogger = mock();
        target = new WinstonRootLogger(instance(winstonLogger));
    });

    const describeMember = describeMemberFactory<typeof target>();

    describeMember('isEnabled', () => {
        it.each([
            [LogLevel.Debug, true],
            [LogLevel.Debug, false],
            [LogLevel.Error, true],
            [LogLevel.Warning, false],
        ])('should be delegated if %s %s', (level, isEnabled) => {
            when(winstonLogger.isLevelEnabled(level)).thenReturn(isEnabled);

            const result = target.isEnabled(level);

            expect(result).toBe(isEnabled);
        });
    });

    describeMember('log', () => {
        it('should be delegated', () => {
            const entry = { message: 'test' } as LogEntry;

            target.log(entry);

            verify(winstonLogger.log(entry)).once();
        });
    });

    describeMember('close', () => {
        it('should wait for all transports to finish', async () => {
            const finishHandlers: (() => void)[] = [];
            const transport1Stream = mock<NodeJS.WritableStream>();
            const transport1 = { logStream: instance(transport1Stream) } as DailyRotateFile;
            const transport2 = mock(TransportStream);
            when(transport1Stream.on('finish', anyFunction())).thenCall((_, h) => finishHandlers.push(h));
            when(transport2.on('finish', anyFunction())).thenCall((_, h) => finishHandlers.push(h));
            when(winstonLogger.transports).thenReturn([transport1, instance(transport2)]);
            when(winstonLogger.end()).thenCall(() => finishHandlers.forEach(h => h()));

            await target.close();

            verify(winstonLogger.end())
                .calledBefore(winstonLogger.close());
        });
    });
});
