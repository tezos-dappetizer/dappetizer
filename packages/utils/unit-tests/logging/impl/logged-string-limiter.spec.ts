import { LoggedStringLimiter } from '../../../src/logging/impl/logged-string-limiter';
import { LoggingConfig } from '../../../src/logging/logging-config';

describe(LoggedStringLimiter.name, () => {
    let target: LoggedStringLimiter;

    beforeEach(() => {
        const config = { maxStringLength: 5 } as LoggingConfig;
        target = new LoggedStringLimiter(config);
    });

    it.each([
        ['', ''],
        ['abc', 'abc'],
        ['12345', '12345'],
        ['1234567', '12345... (7 chars in total)'],
    ])(`should limit '%s' to '%s'`, (input, expected) => {
        expect(target.sanitizeString(input)).toBe(expected);
    });
});
