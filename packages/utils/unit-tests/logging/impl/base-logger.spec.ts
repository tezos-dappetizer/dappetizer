import { anything, instance, mock, spy, verify, when } from 'ts-mockito';

import { describeMember } from '../../../../../test-utilities/mocks';
import { KeyOfType } from '../../../src/basics/public-api';
import { BaseLogger } from '../../../src/logging/impl/base-logger';
import { LogData, LogLevel } from '../../../src/logging/log-entry';
import { LoggerScope } from '../../../src/logging/logger-scope';

class TestLogger extends BaseLogger {
    isEnabled(_level: LogLevel): boolean {
        throw new Error('Should be mocked');
    }

    writeToLog(_level: LogLevel, _message: string, _data: LogData): void {
        /* Verified in tests. */
    }
}

describe(BaseLogger.name, () => {
    let target: TestLogger;
    let targetSpy: TestLogger;
    let scope: LoggerScope;

    beforeEach(() => {
        scope = mock();
        target = new TestLogger('TestCategory', instance(scope));
        targetSpy = spy(target);
    });

    describeMember<typeof target>('isEnabled', () => {
        testIsEnabled(t => t.isEnabled(LogLevel.Debug), LogLevel.Debug);
        testIsEnabled(t => t.isEnabled(LogLevel.Critical), LogLevel.Critical);
        testIsEnabled(t => t.isEnabled(LogLevel.Error), LogLevel.Error);
    });

    testIsEnabledProperty('isCriticalEnabled', LogLevel.Critical);
    testIsEnabledProperty('isErrorEnabled', LogLevel.Error);
    testIsEnabledProperty('isWarningEnabled', LogLevel.Warning);
    testIsEnabledProperty('isInformationEnabled', LogLevel.Information);
    testIsEnabledProperty('isDebugEnabled', LogLevel.Debug);
    testIsEnabledProperty('isTraceEnabled', LogLevel.Trace);

    function testIsEnabledProperty(property: KeyOfType<BaseLogger, boolean>, expectedLevel: LogLevel) {
        describe(property, () => {
            testIsEnabled(t => t[property], expectedLevel);
        });
    }

    function testIsEnabled(act: (l: BaseLogger) => boolean, expectedLevel: LogLevel) {
        it.each([true, false])(`should check ${expectedLevel} level and return %s`, expected => {
            when(targetSpy.isEnabled(expectedLevel)).thenReturn(expected);

            const actual = act(target);

            expect(actual).toBe(expected);
        });
    }

    describeMember<typeof target>('log', () => {
        testLog((t, m, d) => t.log(LogLevel.Information, m, d), LogLevel.Information);
        testLog((t, m, d) => t.log(LogLevel.Warning, m, d), LogLevel.Warning);
    });

    testLogMethod('logCritical', LogLevel.Critical);
    testLogMethod('logError', LogLevel.Error);
    testLogMethod('logWarning', LogLevel.Warning);
    testLogMethod('logInformation', LogLevel.Information);
    testLogMethod('logDebug', LogLevel.Debug);
    testLogMethod('logTrace', LogLevel.Trace);

    function testLogMethod(method: KeyOfType<BaseLogger, (m: string) => void>, expectedLevel: LogLevel) {
        describe(method, () => {
            testLog((t, m, d) => t[method](m, d), expectedLevel);
        });
    }

    function testLog(act: (t: BaseLogger, m: string, d: LogData) => void, expectedLevel: LogLevel) {
        it(`should log ${expectedLevel} level`, () => {
            const data: LogData = { name: 'Batman' };
            const enrichedData: LogData = { name: 'Joker' };
            when(targetSpy.isEnabled(expectedLevel)).thenReturn(true);
            when(scope.enrichData(data)).thenReturn(enrichedData);

            act(target, 'test msg', data);

            verify(targetSpy.writeToLog(expectedLevel, 'test msg', enrichedData)).once();
        });

        it(`should not log ${expectedLevel} level if not enabled`, () => {
            const data: LogData = { name: 'Batman' };
            when(targetSpy.isEnabled(expectedLevel)).thenReturn(false);

            act(target, 'test msg', data);

            verify(targetSpy.writeToLog(anything(), anything(), anything())).never();
        });
    }
});
