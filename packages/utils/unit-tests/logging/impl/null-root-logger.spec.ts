import { describeMemberFactory } from '../../../../../test-utilities/mocks';
import { NullRootLogger } from '../../../src/logging/impl/null-root-logger';

describe(NullRootLogger, () => {
    const target = new NullRootLogger();

    const describeMember = describeMemberFactory<typeof target>();

    it('should be created correctly', () => {
        expect(target).toBeFrozen();
    });

    describeMember('create', () => {
        it('should return this', () => {
            expect(target.create()).toBe(target);
        });
    });

    describeMember('isEnabled', () => {
        it('should always return false', () => {
            expect(target.isEnabled(null!)).toBe(false);
        });
    });

    describeMember('log', () => {
        it('should do nothing', () => {
            expect(() => target.log(null!)).not.toThrow();
        });
    });

    describeMember('close', () => {
        it('should do nothing', async () => {
            await target.close();
        });
    });
});
