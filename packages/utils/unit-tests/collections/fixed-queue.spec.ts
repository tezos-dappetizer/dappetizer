import { FixedQueue } from '../../src/collections/fixed-queue';

describe(FixedQueue.name, () => {
    let target: FixedQueue<string>;

    beforeEach(() => {
        target = new FixedQueue(3);
    });

    it('should be empty by default', () => {
        verifyContained(false, ['a', 'b', 'c', 'd', '']);
    });

    it('should store values correctly', () => {
        target.add('a');
        target.add('b');

        verifyContained(true, ['a', 'b']);
        verifyContained(false, ['c', 'd', '']);
    });

    it('should drop old values once full', () => {
        for (const value of ['a', 'b', 'c', 'd']) {
            target.add(value);
        }

        verifyContained(true, ['b', 'c', 'd']);
        verifyContained(false, ['a', '']);
    });

    function verifyContained(expected: boolean, values: string[]) {
        values.forEach((v) => expect(target.has(v)).toBe(expected));
    }
});
