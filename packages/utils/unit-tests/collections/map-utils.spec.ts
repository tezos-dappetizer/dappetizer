import { times } from 'lodash';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { getOrCreate } from '../../src/collections/map-utils';

interface TestFactory {
    create(key: string): Value;
}

interface Value {
    readonly num: number;
}

describe(getOrCreate.name, () => {
    let map: Map<string, Value>;
    let factory: TestFactory;

    beforeEach(() => {
        map = new Map();
        factory = mock();
    });

    it('should create and cache the value', () => {
        const value: Value = { num: 123 };
        when(factory.create(anything())).thenReturn(value);

        times(5, () => {
            const result = getOrCreate(map, 'foo', k => instance(factory).create(k));

            expect(result).toBe(value);
        });

        verify(factory.create('foo')).once();
        verify(factory.create(anything())).once();
    });

    it('should cache values by key', () => {
        const fooValue: Value = { num: 111 };
        const barValue: Value = { num: 222 };
        when(factory.create('foo')).thenReturn(fooValue);
        when(factory.create('bar')).thenReturn(barValue);

        times(5, () => {
            const fooResult = getOrCreate(map, 'foo', k => instance(factory).create(k));
            const barResult = getOrCreate(map, 'bar', k => instance(factory).create(k));

            expect(fooResult).toBe(fooValue);
            expect(barResult).toBe(barValue);
        });

        verify(factory.create(anything())).times(2);
    });
});
