import { fullCliOption } from '../../src/cli/cli-defaults';

describe(fullCliOption.name, () => {
    it('should get quoted option correctly', () => {
        const option = fullCliOption('INDEXER_MODULE');

        expect(option).toBe('--indexer-module');
    });

    it.each([
        ['FooBar', '--indexer-module=FooBar'],
        ['', '--indexer-module='],
    ])('should get quoted option with value correctly', (value, expectedOption) => {
        const option = fullCliOption('INDEXER_MODULE', value);

        expect(option).toBe(expectedOption);
    });
});
