import { ConfigError } from '../../src/configuration/config-error';

describe(ConfigError.name, () => {
    it('should expose specified details correctly', () => {
        const target = new ConfigError({
            filePath: 'C:/config.js',
            propertyPath: '$.root.prop',
            value: 123,
            reason: 'WTF',
        });

        expect(target).toBeFrozen();
        expect(target.filePath).toBe('C:/config.js');
        expect(target.propertyPath).toBe('$.root.prop');
        expect(target.value).toBe(123);
        expect(target.reason).toBe('WTF');
        expect(target.message).toIncludeMultiple([
            `'C:/config.js'`,
            `'$.root.prop'`,
            `123 of type 'number'`,
            `WTF`,
        ]);
    });

    describe('should correctly format value', () => {
        class Foo {
            constructor(readonly val: number) {}
        }

        it.each([
            ['null', null, `null of type 'object' (null)`],
            ['undefined', undefined, `undefined of type 'undefined'`],
            ['string', 'abc', `"abc" of type 'string'`],
            ['object literal', { val: 123 }, `{"val":123} of type 'object' (constructor Object)`],
            ['custom class', new Foo(123), `{"val":123} of type 'object' (constructor Foo)`],
        ])('%s', (_desc, value, expected) => {
            const target = new ConfigError({
                filePath: 'C:/config.js',
                propertyPath: '$.root.prop',
                value,
                reason: 'WTF',
            });

            expect(target.message).toContain(expected);
        });
    });
});
