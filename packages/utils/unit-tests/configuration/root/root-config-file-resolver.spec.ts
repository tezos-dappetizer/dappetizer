import * as nodeFS from 'fs';
import path from 'path';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { expectToThrow } from '../../../../../test-utilities/mocks';
import { RootConfigFileResolver } from '../../../src/configuration/root/root-config-file-resolver';

describe(RootConfigFileResolver.name, () => {
    let explicitConfig: string | undefined;
    let fs: typeof nodeFS;
    let process: NodeJS.Process;

    const getTarget = () => new RootConfigFileResolver(explicitConfig, instance(fs), instance(process), ['first.ts', 'second.js']);

    beforeEach(() => {
        explicitConfig = undefined;
        [fs, process] = [mock(), mock()];

        when(process.cwd()).thenReturn('C:/app');
    });

    describe('if explicitly specified config file', () => {
        let expectedPath: string;

        beforeEach(() => {
            explicitConfig = 'dir/config.json';
            expectedPath = path.resolve('C:/app/dir/config.json');
        });

        it('should resolve explicitly specified file if exists', () => {
            when(fs.existsSync(expectedPath)).thenReturn(true);

            const resultPath = getTarget().resolveAbsPath();

            expect(resultPath).toBe(expectedPath);
        });

        it('should throw if explicitly specified file does not exist', () => {
            const target = getTarget();

            const error = expectToThrow(() => target.resolveAbsPath());

            expect(error.message).toIncludeMultiple([
                `does not exist`,
                `'dir/config.json'`,
                `'${expectedPath}'`,
            ]);
            verify(fs.existsSync(expectedPath)).once();
        });
    });

    describe('if implicit config file', () => {
        it.each([
            ['first.ts', []],
            ['second.js', ['first.ts']],
        ])('should use implicit %s', (expectedName, otherTestedNames) => {
            const expectedPath = path.resolve('C:/app', expectedName);
            when(fs.existsSync(expectedPath)).thenReturn(true);

            const resultPath = getTarget().resolveAbsPath();

            expect(resultPath).toBe(expectedPath);
            fileExistsCalledOnce([expectedName, ...otherTestedNames]);
        });

        it('should throw if no implicit file exists', () => {
            const target = getTarget();

            const error = expectToThrow(() => target.resolveAbsPath());

            expect(error.message).toIncludeMultiple([
                'Failed',
                `'first.ts'`,
                `'second.js'`,
                `'C:/app'`,
            ]);
            fileExistsCalledOnce(['first.ts', 'second.js']);
        });

        function fileExistsCalledOnce(names: readonly string[]) {
            verify(fs.existsSync(anything())).times(names.length);
            names.forEach(n => verify(fs.existsSync(path.resolve('C:/app', n))).once());
        }
    });
});
