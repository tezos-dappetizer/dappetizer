import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';
import * as npmTSNode from 'ts-node';

import { TypeScriptConfigValueLoader } from '../../../src/configuration/root/typescript-config-value-loader';

describe(TypeScriptConfigValueLoader.name, () => {
    let target: TypeScriptConfigValueLoader;
    let tsNode: typeof npmTSNode;
    let nodeJS: { require: NodeJS.Require };

    let tsNodeService: npmTSNode.Service;

    beforeEach(() => {
        [tsNode, nodeJS] = [mock(), mock()];
        target = new TypeScriptConfigValueLoader(instance(tsNode), instance(nodeJS));

        tsNodeService = mock();
        when(tsNode.register(anything())).thenReturn(instance(tsNodeService));
    });

    it('should initialize TS node and load value using require', () => {
        const loadedValue = { val: 123 };
        when(nodeJS.require('dir/file.js')).thenReturn(loadedValue);

        const value = target.loadValue('dir/file.js');

        expect(value).toBe(loadedValue);
        verify(tsNode.register(deepEqual({ compilerOptions: { module: 'CommonJS' } }))).once();
        verify(tsNodeService.enabled(true)).once();
    });

    it('should extract "default" property if ESM module', () => {
        const loadedValue = {
            __esModule: true,
            default: { val: 123 },
        };
        when(nodeJS.require('dir/file.js')).thenReturn(loadedValue);

        const value = target.loadValue('dir/file.js');

        expect(value).toBe(loadedValue.default);
    });

    it('should initialize TS node only once', () => {
        when(nodeJS.require('dir/file.js')).thenReturn({ val: 123 });

        target.loadValue('dir/file.js');
        target.loadValue('dir/file.js');

        verify(tsNode.register(anything())).once();
        verify(tsNodeService.enabled(anything())).once();
    });
});
