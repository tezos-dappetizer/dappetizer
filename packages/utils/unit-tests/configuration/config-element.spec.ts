import { deepEqual, instance, mock, when } from 'ts-mockito';

import { describeMemberFactory, expectToThrow } from '../../../../test-utilities/mocks';
import {
    AddressPrefix,
    AddressValidator,
    ArrayLimits,
    ArrayValidator,
    NumberValidator,
    OneOfSupportedValidator,
    StringValidator,
    TypeValidator,
    Validator,
} from '../../src/basics/public-api';
import { BaseConfigElement, ConfigElement, ConfigObjectElement } from '../../src/configuration/config-element';
import { ConfigError } from '../../src/configuration/config-error';

describe(ConfigElement.name, () => {
    let target: ConfigElement;
    let addressValidator: AddressValidator;
    let arrayValidator: ArrayValidator;
    let numberValidator: NumberValidator;
    let oneOfSupportedValidator: OneOfSupportedValidator;
    let stringValidator: StringValidator;
    let typeValidator: TypeValidator;

    beforeEach(() => {
        [addressValidator, arrayValidator, numberValidator, oneOfSupportedValidator, stringValidator, typeValidator]
            = [mock(), mock(), mock(), mock(), mock(), mock()];
        target = new ConfigElement('inputVal', 'C:/config.json', '$.testProp', {
            address: instance(addressValidator),
            array: instance(arrayValidator),
            number: instance(numberValidator),
            oneOfSupported: instance(oneOfSupportedValidator),
            string: instance(stringValidator),
            type: instance(typeValidator),
        });
    });

    const describeMember = describeMemberFactory<typeof target>();

    describeMember('asAbsoluteUrl', () => {
        const act = () => target.asAbsoluteUrl();

        it('should return underlying value if absolute url', () => {
            when(stringValidator.validate(target.value, 'NotWhiteSpace')).thenReturn('http://tezos.org/path?q=1');

            const result = act();

            expect(result).toBe('http://tezos.org/path?q=1');
        });

        it('should wrap error from validator', () => {
            when(stringValidator.validate(target.value, 'NotWhiteSpace')).thenThrow(new Error('oups'));

            runThrowTest({ act, expectedError: ['must be an absolute URL string', 'oups'] });
        });

        it('should throw if not absolute URL', () => {
            when(stringValidator.validate(target.value, 'NotWhiteSpace')).thenReturn('/wtf');

            runThrowTest({ act, expectedError: ['must be an absolute URL string.', 'NOT an absolute URL'] });
        });
    });

    shouldBeDelegatedToValidator({
        methodName: 'asAddress',
        act: () => target.asAddress(['tz2', 'KT1']),
        expectedLimits: () => deepEqual<AddressPrefix[]>(['tz2', 'KT1']),
        validator: () => addressValidator,
    });

    describeMember('asArray', () => {
        const limits: ArrayLimits = 'mockedArrayLimits' as any;

        const act = () => target.asArray(limits);

        it('should create child elements for array items if valid array', () => {
            when(arrayValidator.validate(target.value, limits)).thenReturn(['a', 'b', 333]);

            const elms = act();

            expect(elms).toEqual<BaseConfigElement[]>([
                { value: 'a', propertyPath: '$.testProp[0]', filePath: target.filePath },
                { value: 'b', propertyPath: '$.testProp[1]', filePath: target.filePath },
                { value: 333, propertyPath: '$.testProp[2]', filePath: target.filePath },
            ]);
            elms.forEach(e => expect(e).toBeInstanceOf(ConfigElement));
        });

        it('should wrap error from validator', () => {
            when(arrayValidator.validate(target.value, limits)).thenThrow(new Error('oups'));
            when(arrayValidator.getExpectedValueDescription(limits)).thenReturn('foo bar');

            runThrowTest({ act, expectedError: ['must be foo bar.', 'oups'] });
        });
    });

    shouldBeDelegatedToValidator({
        methodName: 'asBoolean',
        act: () => target.asBoolean(),
        expectedLimits: () => 'boolean',
        validator: () => typeValidator,
    });

    enum TestEnum {
        First = 'first',
        Second = 'second',
    }

    shouldBeDelegatedToValidator({
        methodName: 'asEnum',
        act: () => target.asEnum(TestEnum),
        expectedLimits: () => deepEqual([TestEnum.First, TestEnum.Second]),
        validator: () => oneOfSupportedValidator,
    });

    shouldBeDelegatedToValidator({
        methodName: 'asInteger',
        act: () => target.asInteger({ min: 10 }),
        expectedLimits: () => deepEqual({ integer: true, min: 10 }),
        validator: () => numberValidator,
    });

    describeMember('asObject', () => {
        const act = () => target.asObject();

        it('should create object element wrapping the value object', () => {
            const obj = { a: 1 };
            when(typeValidator.validate(target.value, 'object')).thenReturn(obj);

            const resultElm = act();

            expect(resultElm).toBeInstanceOf(ConfigObjectElement);
            expect(resultElm.value).toBe(obj);
            expect(resultElm.filePath).toBe(target.filePath);
            expect(resultElm.propertyPath).toBe(target.propertyPath);
        });

        it('should throw if not object literal', () => {
            when(typeValidator.validate(target.value, 'object')).thenReturn(new Date());

            runThrowTest({ act, expectedError: ['must be an object literal', 'custom type Date'] });
        });
    });

    shouldBeDelegatedToValidator({
        methodName: 'asOneOf',
        act: () => target.asOneOf(['omg', 'lol']),
        expectedLimits: () => deepEqual(['omg', 'lol']),
        validator: () => oneOfSupportedValidator,
    });

    shouldBeDelegatedToValidator({
        methodName: 'asString',
        act: () => target.asString('mockedLimits' as any),
        expectedLimits: () => 'mockedLimits' as any,
        validator: () => stringValidator,
    });

    function shouldBeDelegatedToValidator<TLimits>(testCase: {
        methodName: keyof typeof target;
        act: () => unknown;
        validator: () => Validator<unknown, TLimits>;
        expectedLimits: () => TLimits;
    }) {
        describe(testCase.methodName, () => {
            it('should return underlying value if valid', () => {
                when(testCase.validator().validate(target.value, testCase.expectedLimits())).thenReturn('abc');

                const result = testCase.act();

                expect(result).toBe('abc');
            });

            it('should wrap error from validator', () => {
                when(testCase.validator().validate(target.value, testCase.expectedLimits())).thenThrow(new Error('oups'));
                when(testCase.validator().getExpectedValueDescription(testCase.expectedLimits())).thenReturn('foo bar');

                runThrowTest({ ...testCase, expectedError: ['must be foo bar.', 'oups'] });
            });
        });
    }

    function runThrowTest(testCase: {
        act: () => unknown;
        expectedError: string[];
        value?: unknown;
    }) {
        const error = expectToThrow(testCase.act, ConfigError);

        expect(error.filePath).toBe(target.filePath);
        expect(error.propertyPath).toBe(target.propertyPath);
        expect(error.value).toBe(target.value);
        expect(error.reason).toIncludeMultiple(testCase.expectedError);
    }
});
