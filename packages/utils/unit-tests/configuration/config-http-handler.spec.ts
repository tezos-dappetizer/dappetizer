import express from 'express';
import { instance, mock, verify, when } from 'ts-mockito';

import { ConfigDiagnosticsAggregator } from '../../src/configuration/config-diagnostics-aggregator';
import { ConfigHttpHandler } from '../../src/configuration/config-http-handler';
import { contentTypes, httpHeaders } from '../../src/http-fetch/public-api';

describe(ConfigHttpHandler.name, () => {
    let target: ConfigHttpHandler;
    let configAggregator: ConfigDiagnosticsAggregator;

    beforeEach(() => {
        configAggregator = mock();
        target = new ConfigHttpHandler(instance(configAggregator));
    });

    it('should send JSON serialized config diagnostics', () => {
        const request = {} as express.Request;
        const response = mock<express.Response>();
        when(configAggregator.aggregateConfigs()).thenReturn({ foo: 'bar' });

        target.handle(request, response);

        verify(response.setHeader(httpHeaders.CONTENT_TYPE, contentTypes.JSON));
        verify(response.send(JSON.stringify({ foo: 'bar' }, undefined, 2)));
    });
});
