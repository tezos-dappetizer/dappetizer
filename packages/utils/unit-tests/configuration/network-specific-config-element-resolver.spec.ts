import { expectToThrow } from '../../../../test-utilities/mocks';
import { DappetizerConfig } from '../../../indexer/src/config/dappetizer-config';
import { ConfigObjectElement } from '../../src/configuration/config-element';
import { ConfigError } from '../../src/configuration/config-error';
import { DAPPETIZER_CONFIG_FROM } from '../../src/configuration/config-utils';
import { MAINNET } from '../../src/configuration/di-tokens';
import {
    NetworkSpecificConfigElementResolver,
    ROOT_NAME,
} from '../../src/configuration/network-specific-config-element-resolver';

describe(NetworkSpecificConfigElementResolver.name, () => {
    function getTarget(network: string, rootConfig: Record<string, unknown>) {
        const rootElement = new ConfigObjectElement(rootConfig, 'dir/config.json', '$.root');
        return new NetworkSpecificConfigElementResolver(rootElement, network);
    }

    it('should resolve object corresponding to given network', () => {
        const rootConfig = { [ROOT_NAME]: { foonet: { foo: 'bar' } } };
        const target = getTarget('foonet', rootConfig);

        const networkElement = target.resolve();

        expect(networkElement).toBeInstanceOf(ConfigObjectElement);
        expect(networkElement).toMatchObject<Partial<ConfigObjectElement>>({
            value: rootConfig.networks.foonet,
            filePath: 'dir/config.json',
            propertyPath: '$.root.networks.foonet',
        });
    });

    it.each([
        ['empty string', '', `"" of type 'string'`],
        ['white-space string', '  ', `"  " of type 'string'`],
        ['not string', 123, `123 of type 'number'`],
    ])('should throw constructor error if network is %s', (_desc, network: any, expectedError) => {
        const rootConfig = { [ROOT_NAME]: { [MAINNET]: { foo: 'bar' } } };

        const error = expectToThrow(() => getTarget(network, rootConfig), Error);

        expect(error.message).toIncludeMultiple([
            'CONFIG_NETWORK_DI_TOKEN',
            expectedError,
        ]);
    });

    it.each([null, undefined])('should throw if specified network is %s', networkConfig => {
        const target = getTarget('foonet', {
            [ROOT_NAME]: {
                foonet: networkConfig,
                aanet: {},
                bbnet: null,
                ccnet: {},
                ddnet: undefined,
            },
        });

        const error = expectToThrow(() => target.resolve(), Error);

        expect(error.message).toStartWith(DAPPETIZER_CONFIG_FROM);
        expect(error.message).toIncludeMultiple([
            `file 'dir/config.json'`,
            `property path '$.root.networks.foonet'`,
            `'foonet'`,
            `['aanet', 'ccnet']`,
            `'--network' option`,
            `'dappetizer start' command`,
        ]);
    });

    it.each([
        ['networks property is missing', {}],
        ['networks property is invalid', { [ROOT_NAME]: 'wtf' }],
        ['value for specific network is invalid', { [ROOT_NAME]: { foonet: 'wtf' } }],
    ])('should throw if %s', (_desc, rootConfig) => {
        const target = getTarget('foonet', rootConfig);

        expectToThrow(() => target.resolve(), ConfigError);
    });

    it('should expose its properties in DappetizerConfig', () => {
        expect(ROOT_NAME).toBe<keyof DappetizerConfig>('networks');
    });
});
