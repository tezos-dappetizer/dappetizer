import { describeMember, expectToThrow } from '../../../../../test-utilities/mocks';
import { TypeOfName } from '../../../src/basics/public-api';
import { TypeValidator } from '../../../src/basics/validators/type-validator';
import { TypeValidatorImpl } from '../../../src/basics/validators/type-validator-impl';

describe(TypeValidatorImpl.name, () => {
    const target: TypeValidator = new TypeValidatorImpl();

    describeMember<typeof target>('validate', () => {
        it('should pass if value of expected type', () => {
            const result = target.validate('abc', 'string');

            expect(result).toBe('abc');
        });

        it('should handle undefined', () => {
            const result = target.validate(undefined, 'undefined');

            expect(result).toBeUndefined();
        });

        it.each([
            ['undefined', undefined, 'undefined'],
            ['null', null, 'null'],
            ['unexpected type', 123, 'cannot be number'],
        ])('should throw if %s', (_desc, input, expectedError) => {
            const error = expectToThrow(() => target.validate(input, 'string'));

            expect(error.message).toContain(expectedError);
        });
    });

    describeMember<typeof target>('getExpectedValueDescription', () => {
        const testCases: Record<TypeOfName, string> = {
            bigint: 'a bigint',
            boolean: 'a boolean',
            function: 'a function',
            number: 'a number',
            object: 'an object',
            string: 'a string',
            symbol: 'a symbol',
            undefined: 'undefined',
        };

        it.each(Object.entries(testCases))('should construct correct value description for %s', (limits: any, expected) => {
            const desc = target.getExpectedValueDescription(limits);

            expect(desc).toBe(expected);
        });
    });
});
