import { instance, mock, when } from 'ts-mockito';

import { describeMember, expectToThrow } from '../../../../../test-utilities/mocks';
import { AddressPrefix } from '../../../src/basics/address-prefix';
import { ArgError } from '../../../src/basics/arg-error';
import { AddressValidator } from '../../../src/basics/validators/address-validator';
import { AddressValidatorImpl } from '../../../src/basics/validators/address-validator-impl';
import { StringValidator } from '../../../src/basics/validators/string-validator';

describe(AddressValidatorImpl.name, () => {
    let target: AddressValidator;
    let stringValidator: StringValidator;

    beforeEach(() => {
        stringValidator = mock();
        target = new AddressValidatorImpl(instance(stringValidator));
    });

    describeMember<typeof target>('validate', () => {
        const act = (p?: AddressPrefix[]) => target.validate('raw', p);

        const allAddressTestCases: Record<AddressPrefix, string> = {
            tz: 'tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj',
            tz1: 'tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj',
            tz2: 'tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL',
            tz3: 'tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm',
            KT1: 'KT1RKZcsTS7kLuRd6JygiVkC1RJ4pfwznwkf',
            txr1: 'txr1V16e1hXyVKndP4aE8cujRfryoHTiHK9fG',
            sr1: 'sr1NtrMuXaN6RoiJuxTE6uUbBEzfDmoGqccP',
            ['unsupported (b/c of forward compatibility)' as any]: 'scr1Ew52VCdi6nF1JuokRGMqfmSeiAEXymW2m',
        };
        it.each(Object.entries(allAddressTestCases))('should pass %s address', (_desc, value) => {
            setupStringValidatedTo(value);

            const result = act();

            expect(result).toBe(value);
        });

        // eslint-disable-next-line @typescript-eslint/ban-types
        const tzAddressTestCases: Record<Extract<AddressPrefix, `tz${number}${string}`>, string> = {
            tz1: 'tz1g1mFqhoTk2CHMonQX7FQ2wvnwGPUozZJj',
            tz2: 'tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL',
            tz3: 'tz3SYyWM9sq9eWTxiA8KHb36SAieVYQPeZZm',
        };
        it.each(Object.entries(tzAddressTestCases))('should pass %s address if only "tz" prefix is supported', (_desc, value) => {
            setupStringValidatedTo(value);

            const result = act(['tz']);

            expect(result).toBe(value);
        });

        it('should throw if address not correctly encoded', () => {
            setupStringValidatedTo('tz1WTF');

            const error = expectToThrow(act);

            expect(error.message).toBe('It is NOT correctly base58 encoded address.');
        });

        it('should throw if address does not start with any of specified prefixes', () => {
            setupStringValidatedTo('tz2DyzzHe44duaYiSRS9SeYaXHQXCUrYyQKL');

            const error = expectToThrow(() => act(['tz1', 'KT1']));

            expect(error.message).toBe(`It does not start with any of supported prefixes: ['tz1', 'KT1'].`);
        });

        shouldThrowIfInvalidPrefixes(act);

        function setupStringValidatedTo(value: string) {
            when(stringValidator.validate('raw', 'NotWhiteSpace')).thenReturn(value);
        }
    });

    describeMember<typeof target>('getExpectedValueDescription', () => {
        const act = (p?: AddressPrefix[]) => target.getExpectedValueDescription(p);

        it('should construct correct value description if no prefix specified', () => {
            const desc = act();

            expect(desc).toBe('a valid Tezos address');
        });

        it('should construct correct value description if %s specified', () => {
            const desc = act(['KT1', 'tz2']);

            expect(desc).toBe(`a valid Tezos address with one of prefixes: ['KT1', 'tz2']`);
        });

        shouldThrowIfInvalidPrefixes(act);
    });

    function shouldThrowIfInvalidPrefixes(act: (p: AddressPrefix[]) => unknown) {
        it.each([
            ['null', null],
            ['not array', {}],
            ['empty array', []],
            ['undefined items', [undefined]],
            ['null items', [null]],
            ['items but not strings', [1, 2]],
            ['items but contain empty string', ['abc', '']],
            ['items but contain white-space string', ['abc', '  ']],
        ])('should throw if prefixes are %s', (_desc, prefixes: any) => {
            const error = expectToThrow(() => act(prefixes), ArgError);

            expect(error.argName).toBe('prefixes');
            expect(error.specifiedValue).toBe(prefixes);
        });
    }
});
