import { instance, mock, when } from 'ts-mockito';

import { describeMember, expectToThrow } from '../../../../../test-utilities/mocks';
import { ArgError } from '../../../src/basics/arg-error';
import { ArrayLimits, ArrayValidator } from '../../../src/basics/validators/array-validator';
import { ArrayValidatorImpl } from '../../../src/basics/validators/array-validator-impl';
import { TypeValidator } from '../../../src/basics/validators/type-validator';

describe(ArrayValidatorImpl.name, () => {
    let target: ArrayValidator;
    let typeValidator: TypeValidator;

    beforeEach(() => {
        typeValidator = mock();
        target = new ArrayValidatorImpl(instance(typeValidator));
    });

    describeMember<typeof target>('validate', () => {
        const act = (l?: ArrayLimits) => target.validate('raw', l);

        it.each<[string, ArrayLimits | undefined, readonly unknown[]]>([
            ['empty array', undefined, []],
            ['regular array', undefined, [1, 2]],
            ['regular array', 'NotEmpty', [1, 2]],
            ['regular frozen array', undefined, Object.freeze([1, 2])],
        ])('should pass %s', (_desc, limits, value) => {
            when(typeValidator.validate('raw', 'object')).thenReturn(value);

            const result = act(limits);

            expect(result).toBe(value);
        });

        it.each<[string, object]>([
            ['not an array but Date', new Date()],
            ['an empty array', []],
        ])(`should throw if %s`, (expectedError, value) => {
            when(typeValidator.validate('raw', 'object')).thenReturn(value);

            const error = expectToThrow(() => act('NotEmpty'));

            expect(error.message).toContain(expectedError);
        });

        shouldThrowIfInvalidLimits({
            setup: () => when(typeValidator.validate('raw', 'object')).thenReturn([1, 2]),
            act,
        });
    });

    describeMember<typeof target>('getExpectedValueDescription', () => {
        const act = (l?: ArrayLimits) => target.getExpectedValueDescription(l);

        it.each<[ArrayLimits | undefined, string]>([
            [undefined, 'an array'],
            ['NotEmpty', 'a not-empty array'],
        ])('should construct correct value description for %s', (limits, expected) => {
            const desc = act(limits);

            expect(desc).toBe(expected);
        });

        shouldThrowIfInvalidLimits({ act });
    });

    function shouldThrowIfInvalidLimits(testCase: { act: (l?: ArrayLimits) => unknown; setup?: () => void }) {
        it.each([
            ['null', null],
            ['not string', 123],
            ['unsupported string', 'wtf'],
        ])('should throw if limits are %s', (_desc, limits: any) => {
            testCase.setup?.();

            const error = expectToThrow(() => testCase.act(limits), ArgError);

            expect(error.argName).toBe('limits');
            expect(error.specifiedValue).toBe(limits);
            expect(error.details).toInclude(`[undefined, 'NotEmpty']`);
        });
    }
});
