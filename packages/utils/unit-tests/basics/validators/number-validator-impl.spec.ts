import { instance, mock, when } from 'ts-mockito';

import { describeMember, expectToThrow } from '../../../../../test-utilities/mocks';
import { NumberLimits, NumberValidator } from '../../../src/basics/validators/number-validator';
import { NumberValidatorImpl } from '../../../src/basics/validators/number-validator-impl';
import { TypeValidator } from '../../../src/basics/validators/type-validator';

describe(NumberValidatorImpl.name, () => {
    let target: NumberValidator;
    let typeValidator: TypeValidator;

    beforeEach(() => {
        typeValidator = mock();
        target = new NumberValidatorImpl(instance(typeValidator));
    });

    describeMember<typeof target>('validate', () => {
        it.each<[string, NumberLimits | undefined, number]>([
            ['integer without limits', undefined, 123],
            ['float without limits', undefined, 1.23],
            ['integer with integer limit', { integer: true }, 123],
            ['integer without integer limit', { integer: false }, 1.23],
            ['equal to min', { min: 10 }, 10],
            ['greater than min', { min: 10 }, 15],
            ['less than max', { max: 20 }, 15],
            ['equal to max', { max: 20 }, 20],
            ['all limits at once', { integer: true, min: 10, max: 20 }, 15],
        ])('should pass %s', (_desc, limits, value) => {
            when(typeValidator.validate('raw', 'number')).thenReturn(value);

            const result = target.validate('raw', limits);

            expect(result).toBe(value);
        });

        it.each<[string, number]>([
            ['is NaN', NaN],
            ['NOT an integer', 1.23],
            ['less than minimum 10', 9],
            ['greater than maximum 20', 21],
        ])(`should throw: %s`, (expectedError, value) => {
            when(typeValidator.validate('raw', 'number')).thenReturn(value);

            const error = expectToThrow(() => target.validate('raw', { integer: true, min: 10, max: 20 }));

            expect(error.message).toContain(expectedError);
        });
    });

    describeMember<typeof target>('getExpectedValueDescription', () => {
        it.each<[NumberLimits | undefined, string]>([
            [undefined, 'a number (can be floating point)'],
            [{ integer: true }, 'an integer'],
            [{ integer: true, min: 10 }, 'an integer greater than or equal to 10'],
            [{ integer: true, max: 20 }, 'an integer less than or equal to 20'],
            [{ integer: true, min: 10, max: 20 }, 'an integer greater than or equal to 10 and less than or equal to 20'],
        ])('should construct correct value description for %s', (limits, expected) => {
            const desc = target.getExpectedValueDescription(limits);

            expect(desc).toBe(expected);
        });
    });
});
