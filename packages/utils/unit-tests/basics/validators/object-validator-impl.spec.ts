import { instance, mock, when } from 'ts-mockito';

import { describeMember, expectToThrow } from '../../../../../test-utilities/mocks';
import { Constructor } from '../../../src/basics/reflection-types';
import { ObjectValidator } from '../../../src/basics/validators/object-validator';
import { ObjectValidatorImpl } from '../../../src/basics/validators/object-validator-impl';
import { TypeValidator } from '../../../src/basics/validators/type-validator';

describe(ObjectValidatorImpl.name, () => {
    let target: ObjectValidator;
    let typeValidator: TypeValidator;

    beforeEach(() => {
        typeValidator = mock();
        target = new ObjectValidatorImpl(instance(typeValidator));
    });

    describeMember<typeof target>('validate', () => {
        it.each<[string, object, Constructor | undefined]>([
            ['an object literal without constructor', {}, undefined],
            ['an object literal with constructor', {}, Object],
            ['an object without constructor', new Date(), undefined],
            ['an object with constructor', new Date(), Date],
            ['an inherited object with constructor', new SyntaxError(), Error],
        ])('should pass %s check', (_desc, value, constructor) => {
            when(typeValidator.validate('raw', 'object')).thenReturn(value);

            const result = target.validate('raw', constructor);

            expect(result).toBe(value);
        });

        it('should throw if not instance of type', () => {
            when(typeValidator.validate('raw', 'object')).thenReturn(new Date());

            const error = expectToThrow(() => target.validate('raw', Error));

            expect(error.message).toContain(`an instance of 'Date'`);
        });
    });

    describeMember<typeof target>('getExpectedValueDescription', () => {
        it.each<[Constructor | undefined, string]>([
            [undefined, 'an object'],
            [Date, `an instance of 'Date'`],
        ])('should construct correct value description for %s', (limits, expected) => {
            const desc = target.getExpectedValueDescription(limits);

            expect(desc).toBe(expected);
        });
    });
});
