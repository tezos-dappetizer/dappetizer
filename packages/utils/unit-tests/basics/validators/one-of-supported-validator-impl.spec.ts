import { instance, mock, when } from 'ts-mockito';

import { describeMember, expectToThrow } from '../../../../../test-utilities/mocks';
import { ArgError } from '../../../src/basics/arg-error';
import { OneOfSupportedValidator } from '../../../src/basics/validators/one-of-supported-validator';
import { OneOfSupportedValidatorImpl } from '../../../src/basics/validators/one-of-supported-validator-impl';
import { StringValidator } from '../../../src/basics/validators/string-validator';

describe(OneOfSupportedValidatorImpl.name, () => {
    let target: OneOfSupportedValidator;
    let stringValidator: StringValidator;

    beforeEach(() => {
        stringValidator = mock();
        target = new OneOfSupportedValidatorImpl(instance(stringValidator));
    });

    describeMember<typeof target>('validate', () => {
        const act = (v: readonly string[] = ['lol', '', 'omg']) => target.validate('raw', v);

        it.each(['lol', '', 'omg'])(`should pass supported '%s'`, value => {
            when(stringValidator.validate('raw')).thenReturn(value);

            const result = act();

            expect(result).toBe(value);
        });

        it('should throw if not supported value', () => {
            when(stringValidator.validate('raw')).thenReturn('wtf');

            const error = expectToThrow(act);

            expect(error.message).toBe('It is NOT one of supported strings.');
        });

        shouldThrowIfInvalidSupportedValues(act);
    });

    describeMember<typeof target>('getExpectedValueDescription', () => {
        const act = (v: readonly string[]) => target.getExpectedValueDescription(v);

        it('should construct correct value description', () => {
            const desc = act(['lol', 'omg']);

            expect(desc).toBe(`one of supported strings: ['lol', 'omg']`);
        });

        shouldThrowIfInvalidSupportedValues(act);
    });

    function shouldThrowIfInvalidSupportedValues(act: (p: readonly string[]) => unknown) {
        it.each([
            ['null', null],
            ['not array', {}],
            ['empty array', []],
            ['undefined items', [undefined]],
            ['null items', [null]],
            ['items not string', [1, 2]],
        ])('should throw if prefixes are %s', (_desc, values: any) => {
            const error = expectToThrow(() => act(values), ArgError);

            expect(error.argName).toBe('supportedValues');
            expect(error.specifiedValue).toBe(values);
        });
    }
});
