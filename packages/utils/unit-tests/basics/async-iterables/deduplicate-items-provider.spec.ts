import { asyncToArray, asyncWrap } from 'iter-tools';
import { instance, mock, when } from 'ts-mockito';

import { AsyncIterableProvider } from '../../../src/basics/async-iterables/async-iterable-provider';
import { DeduplicateItemsProvider } from '../../../src/basics/async-iterables/deduplicate-items-provider';

interface Foo {
    id: string;
    value: string;
}

describe(DeduplicateItemsProvider.name, () => {
    let target: AsyncIterableProvider<Foo>;
    let nextProvider: AsyncIterableProvider<Foo>;

    beforeEach(() => {
        nextProvider = mock();
        target = new DeduplicateItemsProvider(
            {
                getItemId: f => f.id,
                numberOfLastItemsToDeduplicate: 100,
            },
            instance(nextProvider),
        );
    });

    it('should deduplicate items', async () => {
        const nextItems: Foo[] = [
            { id: '1', value: 'val.1.1' },
            { id: '2', value: 'val.2.1' },
            { id: '1', value: 'val.1.2' },
            { id: '2', value: 'val.2.2' },
            { id: '3', value: 'val.3' },
            { id: '2', value: 'val.2.3' },
        ];
        when(nextProvider.iterate()).thenReturn(asyncWrap(nextItems));

        const items = await asyncToArray(target.iterate());

        expect(items).toEqual([nextItems[0], nextItems[1], nextItems[4]]);
    });
});
