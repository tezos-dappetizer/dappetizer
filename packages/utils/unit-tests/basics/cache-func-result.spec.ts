import { instance, mock, verify, when } from 'ts-mockito';

import { cacheFuncResult } from '../..//src/basics/cache-func-result';

interface FooFactory {
    execute(): string;
}

describe(cacheFuncResult.name, () => {
    let factory: FooFactory;

    const getTargetFunc = () => cacheFuncResult(() => instance(factory).execute());

    beforeEach(() => {
        factory = mock();
        when(factory.execute()).thenReturn('foo');
    });

    it('should return value from factory and cache it', () => {
        const targetFunc = getTargetFunc();

        for (let i = 0; i < 5; i++) {
            expect(targetFunc()).toBe('foo');
        }

        verify(factory.execute()).once();
    });

    it('should not execute factory when only constructed', () => {
        getTargetFunc();

        verify(factory.execute()).never();
    });
});
