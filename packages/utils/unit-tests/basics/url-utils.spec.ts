import { appendUrl } from '../../src/basics/url-utils';

describe('url utils', () => {
    describe(appendUrl.name, () => {
        describe('should append url with correct slashes', () => {
            it.each([
                ['http://tezos/node', 'blocks/abc', 'http://tezos/node/blocks/abc'],
                ['http://tezos/node/', 'blocks/abc', 'http://tezos/node/blocks/abc'],
                ['http://tezos/node', '/blocks/abc', 'http://tezos/node/blocks/abc'],
                ['http://tezos/node/', '/blocks/abc', 'http://tezos/node/blocks/abc'],
            ])('%s + %s => %s', (baseUrl, relativeUrl, expectedUrl) => {
                const url = appendUrl(baseUrl, relativeUrl);

                expect(url).toBe(expectedUrl);
            });
        });
    });
});
