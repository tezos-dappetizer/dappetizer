import { assert, IsExact } from 'conditional-type-checks';

import { TypeOfName } from '../../src/basics/reflection-types';

const unknownDummy = typeof (null as unknown);
assert<IsExact<TypeOfName, typeof unknownDummy>>(true);
