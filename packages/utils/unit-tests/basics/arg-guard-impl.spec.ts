import { deepEqual, instance, mock, when } from 'ts-mockito';

import { describeMember, expectToThrow } from '../../../../test-utilities/mocks';
import { ArgError } from '../../src/basics/arg-error';
import { ArgGuard } from '../../src/basics/arg-guard';
import { ArgGuardFor, ArgGuardHelper, ArgGuardImpl } from '../../src/basics/arg-guard-impl';
import { AddressValidator } from '../../src/basics/validators/address-validator';
import { ArrayValidator } from '../../src/basics/validators/array-validator';
import { NumberValidator } from '../../src/basics/validators/number-validator';
import { ObjectValidator } from '../../src/basics/validators/object-validator';
import { OneOfSupportedValidator } from '../../src/basics/validators/one-of-supported-validator';
import { StringValidator } from '../../src/basics/validators/string-validator';
import { TypeValidator } from '../../src/basics/validators/type-validator';
import { Validator } from '../../src/basics/validators/validator';

describe(ArgGuardHelper.name, () => {
    let target: ArgGuardHelper<string, Limits>;
    let validator: Validator<string, Limits>;
    let limits: Limits;

    type Limits = { limit: string };

    beforeEach(() => {
        validator = mock();
        target = new ArgGuardHelper(instance(validator));

        limits = { limit: 'mocked' };
    });

    it('should pass valid value', () => {
        when(validator.validate('inputVal', limits)).thenReturn('outputVal');

        const result = target.guard('inputVal', 'fooArg', limits);

        expect(result).toBe('outputVal');
    });

    it('should wrap throw error if invalid value', () => {
        when(validator.validate('inputVal', limits)).thenThrow(new Error('Oups.'));
        when(validator.getExpectedValueDescription(limits)).thenReturn('foo bar');

        const error = expectToThrow(() => target.guard('inputVal', 'fooArg', limits), ArgError);

        expect(error.argName).toBe('fooArg');
        expect(error.details).toBe('The value must be foo bar. Oups.');
        expect(error.specifiedValue).toBe('inputVal');
    });
});

describe(ArgGuardImpl, () => {
    let target: ArgGuard;
    let addressGuard: ArgGuardFor<AddressValidator>;
    let arrayGuard: ArgGuardFor<ArrayValidator>;
    let numberGuard: ArgGuardFor<NumberValidator>;
    let objectGuard: ArgGuardFor<ObjectValidator>;
    let oneOfSupportedGuard: ArgGuardFor<OneOfSupportedValidator>;
    let stringGuard: ArgGuardFor<StringValidator>;
    let typeGuard: ArgGuardFor<TypeValidator>;

    beforeEach(() => {
        target = new ArgGuardImpl(
            instance(addressGuard = mock()),
            instance(arrayGuard = mock()),
            instance(numberGuard = mock()),
            instance(objectGuard = mock()),
            instance(oneOfSupportedGuard = mock()),
            instance(stringGuard = mock()),
            instance(typeGuard = mock()),
        );
    });

    shouldBeDelegated('address', {
        act: () => target.address('addr', 'fooArg', 'addrPrefixes' as any),
        delegatedTo: () => addressGuard.guard('addr', 'fooArg', 'addrPrefixes' as any),
    });

    shouldBeDelegated('array', {
        act: () => target.array('array' as any, 'fooArg'),
        delegatedTo: () => arrayGuard.guard('array' as any, 'fooArg'),
    });

    enum Foo {
        First = 'first',
        Second = 'second',
    }

    shouldBeDelegated('enum', {
        act: () => target.enum(Foo.First, 'fooArg', Foo),
        delegatedTo: () => oneOfSupportedGuard.guard(Foo.First, 'fooArg', deepEqual([Foo.First, Foo.Second])),
    });

    shouldBeDelegated('function', {
        act: () => target.function('func' as any, 'fooArg'),
        delegatedTo: () => typeGuard.guard('func' as any, 'fooArg', 'function'),
    });

    describeMember<typeof target>('ifNotNullish', () => {
        it.each([
            ['skip validation if undefined', undefined, undefined],
            ['skip validation if null', null, null],
            ['validate if not nullish', 'abc', 'validated:abc'],
            ['validate if falsy but not nullish', '', 'validated:'],
        ])('should %s', (_desc, input, expected) => {
            const result = target.ifNotNullish(input, x => `validated:${x}`);

            expect(result).toBe(expected);
        });
    });

    shouldBeDelegated('nonEmptyArray', {
        act: () => target.nonEmptyArray('array' as any, 'fooArg'),
        delegatedTo: () => arrayGuard.guard('array' as any, 'fooArg', 'NotEmpty'),
    });

    shouldBeDelegated('nonEmptyString', {
        act: () => target.nonEmptyString('str', 'fooArg'),
        delegatedTo: () => stringGuard.guard('str', 'fooArg', 'NotEmpty'),
    });

    shouldBeDelegated('nonWhiteSpaceString', {
        act: () => target.nonWhiteSpaceString('str', 'fooArg'),
        delegatedTo: () => stringGuard.guard('str', 'fooArg', 'NotWhiteSpace'),
    });

    shouldBeDelegated('number', {
        act: () => target.number(123, 'fooArg', { min: 10 }),
        delegatedTo: () => numberGuard.guard(123, 'fooArg', deepEqual({ min: 10 })),
    });

    shouldBeDelegated('object', {
        act: () => target.object('obj' as any, 'fooArg', Date),
        delegatedTo: () => objectGuard.guard('obj' as any, 'fooArg', Date),
    });

    shouldBeDelegated('oneOf', {
        act: () => target.oneOf('omg', 'fooArg', 'mockedSupportedValues' as any),
        delegatedTo: () => oneOfSupportedGuard.guard('omg', 'fooArg', 'mockedSupportedValues' as any),
    });

    shouldBeDelegated('string', {
        act: () => target.string('str', 'fooArg', 'mockedLimits' as any),
        delegatedTo: () => stringGuard.guard('str', 'fooArg', 'mockedLimits' as any),
    });

    function shouldBeDelegated(methodName: keyof typeof target, testCase: {
        act: () => unknown;
        delegatedTo: () => unknown;
    }) {
        describe(methodName, () => {
            it('should be delegated', () => {
                when(testCase.delegatedTo()).thenReturn('outputVal');

                const result = testCase.act();

                expect(result).toBe('outputVal');
            });
        });
    }
});
