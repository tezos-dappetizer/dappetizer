import { BigNumber } from 'bignumber.js';

import { sanitizeForJson, StringSanitizer } from '../../src/basics/safe-json-stringifier';

describe(sanitizeForJson.name, () => {
    describe('should sanitize primitive values correctly', () => {
        shouldSanitize('undefined', { input: undefined, expected: null });
        shouldSanitize('null', { input: null, expected: null });
        shouldSanitize('number', { input: 123, expected: 123 });
        shouldSanitize('string', { input: 'omg', expected: 'omg' });
        shouldSanitize('bigint', { input: BigInt(123), expected: '123' });
        shouldSanitize('BigNumber', { input: new BigNumber(123), expected: '123' });
        shouldSanitize('Date', { input: new Date(123), expected: new Date(123) });
    });

    describe('should sanitize complex values correctly', () => {
        shouldSanitize('object', {
            input: { prop1: 11, deep: { prop2: 22 } },
            expected: { prop1: 11, deep: { prop2: 22 } },
        });

        shouldSanitize('array', {
            input: [123, 'abc'],
            expected: [123, 'abc'],
        });

        it('Error', () => {
            const error = new Error('wtf');

            const str = sanitizeForJson(error);

            expect(str).toStartWith('Error: wtf');
            expect(str).toIncludeMultiple(['at', '<anonymous>']);
        });
    });

    describe('should call toJSON() before stringify', () => {
        const obj = {
            whatever: 456,
            toJSON: (index: string): unknown => `custom-${index}`,
        };

        shouldSanitize('on root', { input: obj, expected: 'custom-' });
        shouldSanitize('within object', { input: { foo: obj }, expected: { foo: 'custom-foo' } });
        shouldSanitize('within array', { input: [66, obj], expected: [66, 'custom-1'] });
    });

    it('should apply external sanitizer on strings', () => {
        const strSanitizer: StringSanitizer = {
            sanitizeString: s => `sanitized-${s}`,
        };

        const result = sanitizeForJson('foo', strSanitizer);

        expect(result).toBe('sanitized-foo');
    });

    describe('should replace circular references with a message', () => {
        const simple = {
            circle: null as any,
            other: 123,
        };
        simple.circle = simple;

        shouldSanitize('simple circular obj', {
            input: simple,
            expected: {
                circle: '(circular reference)',
                other: 123,
            },
        });

        const repeated = { value: 123 };

        shouldSanitize('repeated obj without circles', {
            input: {
                first: repeated,
                second: repeated,
                array: [repeated],
            },
            expected: {
                first: { value: 123 },
                second: { value: 123 },
                array: [{ value: 123 }],
            },
        });
    });

    function shouldSanitize(desc: string, testCase: { input: unknown; expected: unknown }) {
        it(desc, () => {
            const result = sanitizeForJson(testCase.input);

            expect(result).toEqual(testCase.expected);
        });
    }
});
