import { expectToThrow } from '../../../../test-utilities/mocks';
import { ArgError } from '../../src/basics/arg-error';
import {
    equalsIgnoreCase,
    isWhiteSpace,
    pascalCase,
    removeRequiredPrefix,
    removeRequiredSuffix,
    replaceAll,
} from '../../src/basics/string-manipulation';

describe('String manipulation utils', () => {
    describe(equalsIgnoreCase, () => {
        it.each([
            ['abc', 'abc', true],
            ['ABc', 'aBC', true],
            ['áčš', 'acs', false],
            ['abc', 'xyz', false],
        ])('%s and %s should be equal %s', (str1, str2, expected) => {
            expect(equalsIgnoreCase(str1, str2)).toBe(expected);
            expect(equalsIgnoreCase(str2, str1)).toBe(expected);
        });
    });

    describe(removeRequiredPrefix.name, () => {
        it('should remove prefix correctly', () => {
            const result = removeRequiredPrefix('Hello world', 'Hello ');

            expect(result).toBe('world');
        });

        it(`should throw if input doesn't start with the prefix`, () => {
            const error = expectToThrow(() => removeRequiredPrefix('Hello world', 'not prefix'), ArgError);

            expect(error.argName).toBe('str');
            expect(error.specifiedValue).toBe('Hello world');
            expect(error.details).toInclude(`with prefix 'not prefix'`);
        });
    });

    describe(removeRequiredSuffix.name, () => {
        it('should remove suffix correctly', () => {
            const result = removeRequiredSuffix('Hello world', ' world');

            expect(result).toBe('Hello');
        });

        it(`should throw if input doesn't end with the suffix`, () => {
            const error = expectToThrow(() => removeRequiredSuffix('Hello world', 'not suffix'), ArgError);

            expect(error.argName).toBe('str');
            expect(error.specifiedValue).toBe('Hello world');
            expect(error.details).toInclude(`with suffix 'not suffix'`);
        });
    });

    describe(isWhiteSpace.name, () => {
        it.each([
            ['undefined', true, undefined],
            ['null', true, null],
            ['empty', true, ''],
            ['spaces', true, '  '],
            ['tabs', true, '\t'],
            ['text', false, 'abc'],
            ['text with white-spaces', false, 'a b\tc'],
        ])('should take %s and return %s', (_desc, expected, input) => {
            expect(isWhiteSpace(input)).toBe(expected);
        });
    });

    describe(replaceAll.name, () => {
        it('should replace all occurrences', () => {
            const str = replaceAll('Hou, Hou, Hou! Merry Christmas!', 'Hou', 'Fuck');

            expect(str).toBe('Fuck, Fuck, Fuck! Merry Christmas!');
        });
    });

    describe(pascalCase.name, () => {
        it.each([
            'fooBar',
            ' foo bar  ',
            'foo_bar',
            'foo-bar',
        ])(`should convert '%s' to pascal caseed 'FooBar'`, str => {
            expect(pascalCase(str)).toBe('FooBar');
        });
    });
});
