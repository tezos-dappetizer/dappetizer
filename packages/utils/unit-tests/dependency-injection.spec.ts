import http from 'http';
import { container as globalContainer, DependencyContainer } from 'tsyringe';

import { shouldResolveImplsForDappetizerFeatures, shouldResolvePublicApi } from '../../../test-utilities/mocks';
import { AppUptimeMetricsUpdater } from '../src/app-control/app-uptime-metrics';
import { HealthMetricsUpdater } from '../src/health/health-metrics';
import { HttpServerConfig } from '../src/http-server/http-server-config';
import { HttpServerMetricsUpdater } from '../src/http-server/http-server-metrics';
import { LoggingConfig } from '../src/logging/logging-config';
import {
    APP_SHUTDOWN_MANAGER_DI_TOKEN,
    CLOCK_DI_TOKEN,
    CONFIG_NETWORK_DI_TOKEN,
    DI_CONTAINER_DI_TOKEN,
    EXPRESS_APP_DI_TOKEN,
    HTTP_FETCHER_DI_TOKEN,
    JSON_FETCHER_DI_TOKEN,
    LOGGER_FACTORY_DI_TOKEN,
    LONG_EXECUTION_HELPER_DI_TOKEN,
    MAINNET,
    NETWORK_SPECIFIC_CONFIG_ELEMENT_DI_TOKEN,
    PACKAGE_JSON_LOADER_DI_TOKEN,
    registerDappetizerUtils,
    registerExplicitConfigFilePath,
    ROOT_CONFIG_ELEMENT_DI_TOKEN,
    ROOT_LOGGER_DI_TOKEN,
    ROOT_LOGGER_FACTORY_DI_TOKEN,
    RPC_BLOCK_MONITOR_DI_TOKEN,
    RPC_MEMPOOL_MONITOR_DI_TOKEN,
    SLEEP_HELPER_DI_TOKEN,
    TAQUITO_HTTP_BACKEND_DI_TOKEN,
    TAQUITO_RPC_CLIENT_DI_TOKEN,
    TIMER_HELPER_DI_TOKEN,
} from '../src/public-api';
import { TezosNodeConfig } from '../src/tezos-rpc/tezos-node-config';
import { TimeConfig } from '../src/time/time-config';
import { UsageStatisticsConfig } from '../src/usage-statistics/usage-statistics-config';

describe('dependency injection', () => {
    let diContainer: DependencyContainer;

    beforeAll(() => {
        diContainer = globalContainer.createChildContainer();
        registerDappetizerUtils(diContainer);
        registerExplicitConfigFilePath(diContainer, { useValue: '../../dappetizer.config.ts' });
    });

    shouldResolvePublicApi(() => diContainer, [
        // App Control.
        [APP_SHUTDOWN_MANAGER_DI_TOKEN, 'frozen'],

        // Configuration.
        [ROOT_CONFIG_ELEMENT_DI_TOKEN, 'frozen'],
        [NETWORK_SPECIFIC_CONFIG_ELEMENT_DI_TOKEN, 'frozen'],

        // Dependency Injection.
        DI_CONTAINER_DI_TOKEN,

        // Http Fetch.
        [HTTP_FETCHER_DI_TOKEN, 'frozen'],
        [JSON_FETCHER_DI_TOKEN, 'frozen'],

        // Http Server.
        EXPRESS_APP_DI_TOKEN,
        http.Server,

        // Logging.
        [LOGGER_FACTORY_DI_TOKEN, 'frozen'],
        [ROOT_LOGGER_DI_TOKEN, 'frozen'],
        [ROOT_LOGGER_FACTORY_DI_TOKEN, 'frozen'],

        // Package.json.
        [PACKAGE_JSON_LOADER_DI_TOKEN, 'frozen'],

        // Tezos RPC.
        [RPC_BLOCK_MONITOR_DI_TOKEN, 'frozen'],
        [RPC_MEMPOOL_MONITOR_DI_TOKEN, 'frozen'],
        TAQUITO_RPC_CLIENT_DI_TOKEN,
        [TAQUITO_HTTP_BACKEND_DI_TOKEN, 'frozen'],

        // Time.
        [CLOCK_DI_TOKEN, 'frozen'],
        [LONG_EXECUTION_HELPER_DI_TOKEN, 'frozen'],
        [SLEEP_HELPER_DI_TOKEN, 'frozen'],
        [TIMER_HELPER_DI_TOKEN, 'frozen'],
    ]);

    describe('network', () => {
        it(`should be set to 'mainnet' by default`, () => {
            expect(diContainer.resolve(CONFIG_NETWORK_DI_TOKEN)).toBe(MAINNET);
        });

        it('should honor existing value', () => {
            const localDiContainer = globalContainer.createChildContainer();
            localDiContainer.registerInstance(CONFIG_NETWORK_DI_TOKEN, 'skynet');
            registerDappetizerUtils(localDiContainer);

            expect(localDiContainer.resolve(CONFIG_NETWORK_DI_TOKEN)).toBe('skynet');
        });
    });

    shouldResolveImplsForDappetizerFeatures(() => diContainer, {
        backgroundWorkerNames: ['HttpServer'],
        configsWithDiagnostics: [HttpServerConfig, LoggingConfig, TezosNodeConfig, TimeConfig, UsageStatisticsConfig],
        healthCheckNames: ['BlockTezosMonitor', 'MempoolTezosMonitor', 'TezosRpcWatcher'],
        httpHandlerUrlPaths: ['/check', '/config', '/health', '/metrics', '/versions'],
        metricsUpdaters: [AppUptimeMetricsUpdater, HealthMetricsUpdater, HttpServerMetricsUpdater],
    });
});
