import { JSONSchemaForNPMPackageJsonFiles } from '@schemastore/package';
import { instance, mock, when } from 'ts-mockito';

import { describeMember, expectToThrow } from '../../../../test-utilities/mocks';
import { StringValidator } from '../../src/basics/public-api';
import { PackageJsonImpl } from '../../src/package-json/package-json-impl';
import { PackageJson } from '../../src/package-json/package-json-loader';

describe(PackageJsonImpl.name, () => {
    let target: PackageJsonImpl;
    let json: JSONSchemaForNPMPackageJsonFiles;
    let stringValidator: StringValidator;

    beforeEach(() => {
        json = {};
        stringValidator = mock();
        target = new PackageJsonImpl('dir/file.json', json, instance(stringValidator));
    });

    it('should be frozen', () => {
        expect(target).toBeFrozen();
    });

    describeMember<PackageJson>('getVersion', () => {
        it('should get version correctly', () => {
            json.version = 'raw-1.2.3';
            whenValidated('raw-1.2.3').thenReturn('1.2.3');

            const version = target.getVersion();

            expect(version).toBe('1.2.3');
        });

        it('should throw if not valid string', () => {
            json.version = 'raw-1.2.3';
            whenValidated('raw-1.2.3').thenThrow(new Error('oups'));

            const error = expectToThrow(() => target.getVersion());

            expect(error.message).toIncludeMultiple([
                'oups',
                `version`,
                `'dir/file.json'`,
                JSON.stringify(target.json),
            ]);
        });
    });

    describeMember<PackageJson>('getDependencyVersion', () => {
        it('should get dependency version correctly', () => {
            json.dependencies = { lodash: 'raw-4.5.6' };
            whenValidated('raw-4.5.6').thenReturn('4.5.6');

            const version = target.getDependencyVersion('lodash');

            expect(version).toBe('4.5.6');
        });

        it.each([
            ['not a valid string', { lodash: 'raw' }, 'raw'],
            ['undefined', undefined, undefined],
        ])('should throw if not valid string', (_desc, dependencies, validatedStr) => {
            json.dependencies = dependencies;
            whenValidated(validatedStr).thenThrow(new Error('oups'));

            const error = expectToThrow(() => target.getDependencyVersion('lodash'));

            expect(error.message).toIncludeMultiple([
                'oups',
                `dependencies['lodash']`,
                `'dir/file.json'`,
                JSON.stringify(target.json),
            ]);
        });
    });

    describeMember<PackageJson>('getDevDependencyVersion', () => {
        it('should get dev dependency version correctly', () => {
            json.devDependencies = { typescript: 'raw-4.5.6' };
            whenValidated('raw-4.5.6').thenReturn('4.5.6');

            const version = target.getDevDependencyVersion('typescript');

            expect(version).toBe('4.5.6');
        });

        it.each([
            ['not a valid string', { typescript: 'raw' }, 'raw'],
            ['undefined', undefined, undefined],
        ])('should throw if not valid string', (_desc, dependencies, validatedStr) => {
            json.devDependencies = dependencies;
            whenValidated(validatedStr).thenThrow(new Error('oups'));

            const error = expectToThrow(() => target.getDevDependencyVersion('typescript'));

            expect(error.message).toIncludeMultiple([
                'oups',
                `devDependencies['typescript']`,
                `'dir/file.json'`,
                JSON.stringify(target.json),
            ]);
        });
    });

    function whenValidated(value: string | undefined) {
        return when(stringValidator.validate(value, 'NotWhiteSpace'));
    }
});
