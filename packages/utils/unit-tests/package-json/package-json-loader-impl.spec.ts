import nodeFS from 'fs';
import path from 'path';
import { instance, mock, when } from 'ts-mockito';

import { PackageJsonImpl } from '../../../utils/src/package-json/package-json-impl';
import { PackageJsonLoader } from '../../src/package-json/package-json-loader';
import { PackageJsonLoaderImpl } from '../../src/package-json/package-json-loader-impl';

describe(PackageJsonLoaderImpl.name, () => {
    let target: PackageJsonLoader;
    let fs: typeof nodeFS;

    beforeEach(() => {
        fs = mock();
        target = new PackageJsonLoaderImpl(instance(fs));
    });

    it('should load package.json correctly', () => {
        when(fs.readFileSync(path.resolve('dir/file.json'))).thenReturn(Buffer.from('{ "val": 123 }', 'utf-8'));

        const packageJson = target.load('dir/file.json') as PackageJsonImpl;

        expect(packageJson.json).toEqual({ val: 123 });
        expect(packageJson.filePath).toBe(path.resolve('dir/file.json'));
        expect(packageJson).toBeInstanceOf(PackageJsonImpl);
    });
});
