import { times } from 'lodash';
import { anything, instance, mock, verify, when } from 'ts-mockito';
import { container as globalContainer, DependencyContainer } from 'tsyringe';

import { registerOnce, registerSingleton } from '../../src/dependency-injection/registration-utils';

describe('registration utils', () => {
    let diContainer: DependencyContainer;

    beforeEach(() => {
        diContainer = globalContainer.createChildContainer();
    });

    describe(registerSingleton.name, () => {
        it('should register single instance from factory', () => {
            const factory = mock<{ create(c: DependencyContainer): object }>();
            const obj = {};
            when(factory.create(anything())).thenReturn(obj);

            registerSingleton(diContainer, 'test', {
                useFactory: c => instance(factory).create(c),
            });

            times(3, () => expect(diContainer.resolve('test')).toBe(obj));
            verify(factory.create(anything())).once();
            verify(factory.create(diContainer)).once();
        });
    });

    describe(registerOnce.name, () => {
        it('should execute registrations only once', () => {
            const registrations = mock<{ register(): object }>();

            times(3, () => registerOnce(diContainer, 'test', () => instance(registrations).register()));

            verify(registrations.register()).once();
        });
    });
});
