import { container as globalContainer, DependencyContainer, singleton } from 'tsyringe';

import { registerDependencyInjection } from '../../src/dependency-injection/dependency-injection';
import {
    injectAllOptional,
    injectOptional,
    resolveAllOptional,
    resolveOptional,
} from '../../src/dependency-injection/optional-resolution';

@singleton()
class RegisteredFoo {}

class UnknownFoo {}

@singleton()
class InjectRegisteredResult {
    constructor(@injectOptional(RegisteredFoo) readonly foo: RegisteredFoo | undefined) {}
}

@singleton()
class InjectUnknownResult {
    constructor(@injectOptional(UnknownFoo) readonly foo: UnknownFoo | undefined) {}
}

@singleton()
class InjectAllRegisteredResult {
    constructor(@injectAllOptional(RegisteredFoo) readonly foos: RegisteredFoo[]) {}
}

@singleton()
class InjectAllUnknownResult {
    constructor(@injectAllOptional(UnknownFoo) readonly foos: UnknownFoo[]) {}
}

describe('optional resolution', () => {
    let diContainer: DependencyContainer;
    let registeredFoo: RegisteredFoo;

    beforeEach(() => {
        diContainer = globalContainer.createChildContainer();
        registerDependencyInjection(diContainer);

        registeredFoo = diContainer.resolve(RegisteredFoo);
    });

    describe(resolveOptional.name, () => {
        it('should resolve instance if registered', () => {
            const foo = resolveOptional(diContainer, RegisteredFoo);

            expect(foo).toBe(registeredFoo);
        });

        it('should resolve undefined if not registered', () => {
            const foo = resolveOptional(diContainer, UnknownFoo);

            expect(foo).toBeUndefined();
        });
    });

    describe(injectOptional.name, () => {
        it('should inject instance if registered', () => {
            const result = diContainer.resolve(InjectRegisteredResult);

            expect(result.foo).toBe(registeredFoo);
        });

        it('should inject undefined if not registered', () => {
            const result = diContainer.resolve(InjectUnknownResult);

            expect(result.foo).toBeUndefined();
        });
    });

    describe(resolveAllOptional.name, () => {
        it('should resolve all registered instances', () => {
            const foos = resolveAllOptional(diContainer, RegisteredFoo);

            expect(foos).toEqual([registeredFoo]);
        });

        it('should resolve empty array if nothing registered', () => {
            const foos = resolveAllOptional(diContainer, UnknownFoo);

            expect(foos).toBeEmpty();
        });
    });

    describe(injectAllOptional.name, () => {
        it('should inject all registered instances', () => {
            const result = diContainer.resolve(InjectAllRegisteredResult);

            expect(result.foos).toEqual([registeredFoo]);
        });

        it('should inject empty array if nothing registered', () => {
            const result = diContainer.resolve(InjectAllUnknownResult);

            expect(result.foos).toBeEmpty();
        });
    });
});
