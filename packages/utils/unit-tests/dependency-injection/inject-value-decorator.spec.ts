import { container as globalContainer, singleton } from 'tsyringe';

import { injectValue } from '../../src/dependency-injection/inject-value-decorator';

@singleton()
class Bar {}

@singleton()
class Foo {
    constructor(
        readonly bar: Bar,
        @injectValue('Hello Foo') readonly message: string,
        @injectValue(66) readonly level: number,
    ) {}
}

describe(injectValue.name, () => {
    it('should inject explicty specified value', () => {
        const container = globalContainer.createChildContainer();
        const bar = container.resolve(Bar);

        const foo = container.resolve(Foo);

        expect(foo.bar).toBe(bar);
        expect(foo.message).toBe('Hello Foo');
        expect(foo.level).toBe(66);
    });
});
