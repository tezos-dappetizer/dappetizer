# Tezos Dappetizer CLI package

`@tezos-dappetizer/cli` provides developers a command-line interface for [Dappetizer](https://dappetizer.dev). 

See the [top-level](https://gitlab.com/tezos-dappetizer/dappetizer) README for details on reporting issues, contributing, and versioning.

## Documentation

See [documentation](https://docs.dappetizer.dev/reference/command-line) for more details.

## Disclaimer

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
