import { ExplicitBlocksToIndex } from '@tezos-dappetizer/indexer';

export type ConsoleArgs =
    | GenerateInitialAppArgs
    | StartIndexingArgs
    | UpdateContractIndexerClassArgs;

export enum ConsoleCommand {
    GenerateInitialApp = 'GenerateInitialApp',
    StartIndexing = 'StartIndexing',
    UpdateContractIndexerClass = 'UpdateContractIndexerClass',
}

export interface CommonGenerateArgs {
    readonly outDirPath: string;
    readonly tezosNodeUrl: string;
    readonly isTezosNodeUrlExplicit: boolean;
    readonly tezosNetwork: string;
}

export interface GenerateInitialAppArgs extends CommonGenerateArgs {
    readonly command: ConsoleCommand.GenerateInitialApp;
    readonly installNpmDependencies: boolean;
    readonly indexerModuleName: string | null;
    readonly contractAddress: string | null;
    readonly contractName: string | null;
    readonly enableUsageStatistics: boolean;
}

export interface StartIndexingArgs {
    readonly command: ConsoleCommand.StartIndexing;
    readonly configFilePath: string | null;
    readonly tezosNetwork: string;
    readonly blocks: ExplicitBlocksToIndex | null;
}

export interface UpdateContractIndexerClassArgs extends CommonGenerateArgs {
    readonly command: ConsoleCommand.UpdateContractIndexerClass;
    readonly contractAddress: string;
    readonly contractName: string;
}
