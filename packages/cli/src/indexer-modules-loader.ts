import {
    INDEXER_MODULE_EXPORT_NAME,
    INDEXER_MODULES_DI_TOKEN,
    IndexerModule,
    IndexerModuleFactory,
} from '@tezos-dappetizer/indexer';
import { errorToString, injectValue, isObject, resolveLogger } from '@tezos-dappetizer/utils';
import { DependencyContainer, singleton } from 'tsyringe';

import { IndexerModuleConfig, IndexerModulesConfig } from './indexer-modules-config';

@singleton()
export class IndexerModulesLoader {
    constructor(
        private readonly config: IndexerModulesConfig,
        @injectValue({ require }) private readonly nodeJS: { readonly require: NodeJS.Require },
    ) {}

    registerAll(diContainer: DependencyContainer): string[] {
        const registeredIds: string[] = [];
        for (const moduleConfig of this.config.modules) {
            try {
                const module = this.loadModule(moduleConfig, diContainer);
                diContainer.registerInstance(INDEXER_MODULES_DI_TOKEN, module);
                registeredIds.push(moduleConfig.id);
            } catch (error) {
                throw new Error(`Failed to load indexer module '${moduleConfig.id}'. ${errorToString(error)}`);
            }
        }
        return registeredIds.sort();
    }

    private loadModule(moduleConfig: IndexerModuleConfig, diContainer: DependencyContainer): IndexerModule<unknown> {
        const moduleExports = this.nodeJS.require(moduleConfig.id) as unknown;
        if (!isObject(moduleExports)) {
            throw new Error('Exported values are not an object.');
        }

        const module = moduleExports[INDEXER_MODULE_EXPORT_NAME];

        if (typeof module === 'object' && module !== null) {
            return module as IndexerModule<unknown>;
        }

        if (typeof module === 'function' && module.length === 1) {
            const moduleFactory = module as IndexerModuleFactory<unknown>;
            return moduleFactory({
                config: moduleConfig.config.value,
                configElement: moduleConfig.config,
                diContainer,
                getLogger: category => resolveLogger(diContainer, category),
            });
        }

        throw new Error(`The Node module export '${INDEXER_MODULE_EXPORT_NAME}' has unsupported type '${typeof module}'.`
            + ` It must be an indexer module object or a factory function accepting options as a single parameter.`);
    }
}
