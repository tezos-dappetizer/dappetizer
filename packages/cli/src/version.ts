/* eslint-disable */
// IMPORTANT: THIS FILE IS AUTO GENERATED! DO NOT MANUALLY EDIT OR CHECKIN!
const packageJson = require('../package.json');

export const versionInfo = Object.freeze({
    COMMIT_HASH: '',
    VERSION: packageJson.version as string,
} as const);
/* eslint-enable */
