import * as DappetizerDatabase from '@tezos-dappetizer/database';
import * as DappetizerGenerator from '@tezos-dappetizer/generator';
import * as DappetizerIndexer from '@tezos-dappetizer/indexer';
import {
    CONFIG_NETWORK_DI_TOKEN,
    registerExplicitConfigFilePath,
    reportUsageStatistics,
    ROOT_CONFIG_ELEMENT_DI_TOKEN,
    runApp,
} from '@tezos-dappetizer/utils';
import { container as globalDIContainer, DependencyContainer } from 'tsyringe';

import { ConsoleCommand, StartIndexingArgs } from './console-args';
import { parseConsoleArgs } from './console-args-parser';
import { IndexerModulesLoader } from './indexer-modules-loader';

declare function require<T>(moduleId: string): T;

export async function runCli(): Promise<void> {
    const args = parseConsoleArgs(process.argv);
    const diContainer = globalDIContainer.createChildContainer();

    switch (args.command) {
        case ConsoleCommand.GenerateInitialApp: {
            const emitter = loadGeneratorEmitter(diContainer, args.tezosNodeUrl);

            await Promise.all([
                reportUsageStatistics(diContainer, ConsoleCommand.GenerateInitialApp),
                emitter.emitInitialApp(args),
            ]);
            break;
        }
        case ConsoleCommand.StartIndexing: {
            const { registerDappetizerIndexer } = require<typeof DappetizerIndexer>('@tezos-dappetizer/indexer');
            registerDappetizerIndexer(diContainer);
            registerConfigFromConsole(diContainer, args);
            registerDatabase(diContainer);
            const indexerModules = registerClientIndexerModules(diContainer);

            await Promise.all([
                reportUsageStatistics(diContainer, ConsoleCommand.StartIndexing, { indexerModules }),
                runApp(diContainer, indexerModules),
            ]);
            break;
        }
        case ConsoleCommand.UpdateContractIndexerClass: {
            const emitter = loadGeneratorEmitter(diContainer, args.tezosNodeUrl);

            await Promise.all([
                reportUsageStatistics(diContainer, ConsoleCommand.UpdateContractIndexerClass),
                emitter.updateContractIndexerClass(args),
            ]);
            break;
        }
    }
}

function registerConfigFromConsole(diContainer: DependencyContainer, args: StartIndexingArgs): void {
    diContainer.registerInstance(CONFIG_NETWORK_DI_TOKEN, args.tezosNetwork);

    if (args.configFilePath) {
        registerExplicitConfigFilePath(diContainer, { useValue: args.configFilePath });
    }
    if (args.blocks) {
        const { registerExplicitBlocksToIndex } = require<typeof DappetizerIndexer>('@tezos-dappetizer/indexer');
        registerExplicitBlocksToIndex(diContainer, { useValue: args.blocks });
    }
}

function registerClientIndexerModules(diContainer: DependencyContainer): string[] {
    const loader = diContainer.resolve(IndexerModulesLoader);
    return loader.registerAll(diContainer);
}

function registerDatabase(diContainer: DependencyContainer): void {
    const rootConfigElement = diContainer.resolve(ROOT_CONFIG_ELEMENT_DI_TOKEN);
    const dbConfig = rootConfigElement.getOptional('database');

    if (dbConfig) {
        const { registerDappetizerWithDatabase } = require<typeof DappetizerDatabase>('@tezos-dappetizer/database');
        registerDappetizerWithDatabase(diContainer);
    }
}

function loadGeneratorEmitter(diContainer: DependencyContainer, tezosNodeUrl: string): DappetizerGenerator.IndexerEmitter {
    const { INDEXER_EMITTER_DI_TOKEN, registerDappetizerGenerator } = require<typeof DappetizerGenerator>('@tezos-dappetizer/generator');

    registerDappetizerGenerator(diContainer, tezosNodeUrl);
    return diContainer.resolve(INDEXER_EMITTER_DI_TOKEN);
}
