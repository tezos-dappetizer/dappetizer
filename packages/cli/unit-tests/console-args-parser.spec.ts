import { instance, mock, verify } from 'ts-mockito';

import { expectToThrow } from '../../../test-utilities/mocks';
import { generatorDefaults } from '../../generator/src/public-api';
import { cliCommands, cliOptions, MAINNET } from '../../utils/src/public-api';
import { ConsoleArgs, ConsoleCommand } from '../src/console-args';
import { parseConsoleArgs } from '../src/console-args-parser';

describe(parseConsoleArgs.name, () => {
    let mockedProcess: NodeJS.Process;
    let realProcessExit: NodeJS.Process['exit'];
    let realConsoleError: Console['error'];

    const act = (a: string[]) => parseConsoleArgs(['node', 'entrypoint.js', ...a]);

    beforeEach(() => {
        mockedProcess = mock();

        realProcessExit = process.exit.bind(process);
        realConsoleError = console.error.bind(console); // eslint-disable-line no-console

        process.exit = code => instance(mockedProcess).exit(code);
        console.error = () => { /* Nothing. */ }; // eslint-disable-line no-console
    });

    afterEach(() => {
        process.exit = realProcessExit;
        console.error = realConsoleError; // eslint-disable-line no-console
    });

    const networkTestCases = Object.entries(generatorDefaults.tezosNodeUrlsByNetwork);

    describe(ConsoleCommand.GenerateInitialApp, () => {
        it.each([
            [`--${cliOptions.NPM_INSTALL}=true`, true],
            [`--${cliOptions.NPM_INSTALL}=false`, false],
        ])(`should parse with explicit options esp. %s`, (npmInstallOption, expectedInstallNpm) => {
            runSuccessTest({
                input: [
                    cliCommands.INIT,
                    'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5',
                    `--${cliOptions.CONTRACT_NAME}=FooBar`,
                    npmInstallOption,
                    `--${cliOptions.INDEXER_MODULE}=LolModule`,
                    `--${cliOptions.OUT_DIR}=dir/output`,
                    `--${cliOptions.TEZOS_ENDPOINT}=https://tezos.url/`,
                    `--${cliOptions.NETWORK}=skynet`,
                    `--${cliOptions.USAGE_STATISTICS}=false`,
                ],
                expected: {
                    command: ConsoleCommand.GenerateInitialApp,
                    contractAddress: 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5',
                    contractName: 'FooBar',
                    enableUsageStatistics: false,
                    installNpmDependencies: expectedInstallNpm,
                    indexerModuleName: 'LolModule',
                    outDirPath: 'dir/output',
                    tezosNodeUrl: 'https://tezos.url/',
                    isTezosNodeUrlExplicit: true,
                    tezosNetwork: 'skynet',
                },
            });
        });

        it('should use defaults if no options', () => {
            runSuccessTest({
                input: [cliCommands.INIT],
                expected: {
                    command: ConsoleCommand.GenerateInitialApp,
                    contractAddress: null,
                    contractName: null,
                    enableUsageStatistics: true,
                    installNpmDependencies: true,
                    indexerModuleName: null,
                    outDirPath: generatorDefaults.OUT_DIR,
                    tezosNodeUrl: generatorDefaults.tezosNodeUrlsByNetwork.mainnet,
                    isTezosNodeUrlExplicit: false,
                    tezosNetwork: MAINNET,
                },
            });
        });

        it.each(networkTestCases)('should resolve tezos node url from --network=%s', (network, expectedTezosNodeUrl) => {
            runSuccessTest({
                input: [cliCommands.INIT, `--${cliOptions.NETWORK}=${network}`],
                expectedMatching: {
                    command: ConsoleCommand.GenerateInitialApp,
                    tezosNodeUrl: expectedTezosNodeUrl,
                    isTezosNodeUrlExplicit: false,
                    tezosNetwork: network,
                },
            });
        });

        it('should support named options instead of positional', () => {
            runSuccessTest({
                input: [cliCommands.INIT, `--${cliOptions.CONTRACT_ADDRESS}=KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5`],
                expectedMatching: {
                    command: ConsoleCommand.GenerateInitialApp,
                    contractAddress: 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5',
                },
            });
        });

        it.each([
            ['white-space', '"  "'],
            ['invalid', 'gibberish'],
        ])('should throw if %s contract address', (_desc, contractAddress) => {
            runThrowTest([cliCommands.INIT, contractAddress]);
        });

        it.each([
            `--${cliOptions.CONTRACT_NAME}="  "`,
            `--${cliOptions.INDEXER_MODULE}=""`,
            `--${cliOptions.OUT_DIR}="\t"`,
            `--${cliOptions.TEZOS_ENDPOINT}="  "`,
            `--${cliOptions.TEZOS_ENDPOINT}=not-abs-url`,
            `--${cliOptions.NETWORK}=""`,
            `--${cliOptions.NETWORK}=wtf`,
        ])('should throw if %s', option => {
            runThrowTest([cliCommands.INIT, 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5', option]);
        });

        it('should throw if contract name but missing contract address', () => {
            runThrowTest([cliCommands.INIT, `--${cliOptions.CONTRACT_NAME}=FooBar`]);
        });
    });

    describe(ConsoleCommand.StartIndexing, () => {
        it('should parse with explicit options', () => {
            runSuccessTest({
                input: [
                    cliCommands.START,
                    'my.config.js',
                    `--${cliOptions.NETWORK}=memenet`,
                    `--${cliOptions.BLOCKS}=4,8-12,17`,
                ],
                expected: {
                    command: ConsoleCommand.StartIndexing,
                    configFilePath: 'my.config.js',
                    tezosNetwork: 'memenet',
                    blocks: [
                        { fromLevel: 4, toLevel: 4 },
                        { fromLevel: 8, toLevel: 12 },
                        { fromLevel: 17, toLevel: 17 },
                    ],
                },
            });
        });

        it('should use defaults if no options', () => {
            runSuccessTest({
                input: [cliCommands.START],
                expected: {
                    command: ConsoleCommand.StartIndexing,
                    configFilePath: null,
                    tezosNetwork: MAINNET,
                    blocks: null,
                },
            });
        });

        it('should support named options instead of positional', () => {
            runSuccessTest({
                input: [cliCommands.START, `--${cliOptions.CONFIG_FILE}=my.config.js`],
                expected: {
                    command: ConsoleCommand.StartIndexing,
                    configFilePath: 'my.config.js',
                    tezosNetwork: MAINNET,
                    blocks: null,
                },
            });
        });

        it(`should throw if white-space ${cliOptions.CONFIG_FILE}`, () => {
            runThrowTest([cliCommands.START, '"  "']);
        });

        it(`should throw if white-space --${cliOptions.NETWORK}`, () => {
            runThrowTest([cliCommands.START, `--${cliOptions.NETWORK}=`]);
        });

        it.each([
            ['empty', '""'],
            ['white space', '"  "'],
            ['invalid level', '123wtf'],
            ['invalid levels separator', '1;2;3'],
            ['invalid range separator', '1,5=8'],
            ['missing from level', '1,-5'],
            ['invalid from level', '1,wtf-5'],
            ['missing to level', '1,5-'],
            ['invalid to level', '1,5-wtf'],
        ])(`should throw if --${cliOptions.BLOCKS} is %s`, (_desc, value) => {
            runThrowTest([cliCommands.START, `--${cliOptions.BLOCKS}=${value}`]);
        });
    });

    describe(ConsoleCommand.UpdateContractIndexerClass, () => {
        it('should parse with explicit options', () => {
            runSuccessTest({
                input: [
                    cliCommands.UPDATE,
                    'FooBar',
                    'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5',
                    `--${cliOptions.OUT_DIR}=dir/output`,
                    `--${cliOptions.TEZOS_ENDPOINT}=https://tezos.url/`,
                    `--${cliOptions.NETWORK}=skynet`,
                ],
                expected: {
                    command: ConsoleCommand.UpdateContractIndexerClass,
                    contractName: 'FooBar',
                    contractAddress: 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5',
                    outDirPath: 'dir/output',
                    tezosNodeUrl: 'https://tezos.url/',
                    isTezosNodeUrlExplicit: true,
                    tezosNetwork: 'skynet',
                },
            });
        });

        it('should use defaults if no options', () => {
            runSuccessTest({
                input: [cliCommands.UPDATE, 'FooBar', 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5'],
                expected: {
                    command: ConsoleCommand.UpdateContractIndexerClass,
                    contractName: 'FooBar',
                    contractAddress: 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5',
                    outDirPath: generatorDefaults.OUT_DIR,
                    tezosNodeUrl: generatorDefaults.tezosNodeUrlsByNetwork.mainnet,
                    isTezosNodeUrlExplicit: false,
                    tezosNetwork: MAINNET,
                },
            });
        });

        it.each(networkTestCases)('should resolve tezos node url from --network=%s', (network, expectedTezosNodeUrl) => {
            runSuccessTest({
                input: [
                    cliCommands.UPDATE,
                    'FooBar',
                    'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5',
                    `--${cliOptions.NETWORK}=${network}`,
                ],
                expectedMatching: {
                    command: ConsoleCommand.UpdateContractIndexerClass,
                    tezosNodeUrl: expectedTezosNodeUrl,
                    isTezosNodeUrlExplicit: false,
                    tezosNetwork: network,
                },
            });
        });

        it.each([
            ['no positionals', []],
            ['only name positional', ['FooBar']],
        ])('should throw if %s specified', (_desc, options) => {
            runThrowTest([cliCommands.UPDATE, ...options]);
        });

        it('should throw if white-space contract name', () => {
            runThrowTest([cliCommands.UPDATE, '"  "', 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5']);
        });

        it.each([
            ['white-space', '"  "'],
            ['invalid', 'gibberish'],
        ])('should throw if %s contract address', (_desc, contractAddress) => {
            runThrowTest([cliCommands.UPDATE, 'FooBar', contractAddress]);
        });

        it.each([
            `--${cliOptions.OUT_DIR}="\t"`,
            `--${cliOptions.TEZOS_ENDPOINT}="  "`,
            `--${cliOptions.TEZOS_ENDPOINT}=not-abs-url`,
            `--${cliOptions.NETWORK}=""`,
            `--${cliOptions.NETWORK}=wtf`,
        ])('should throw if %s', option => {
            runThrowTest([cliCommands.UPDATE, 'FooBar', 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5', option]);
        });
    });

    it.each([
        ['unsupported', ['wtf']],
        ['no', []],
    ])('should throw if %s command', (_desc, input) => {
        runThrowTest(input);
    });

    function runSuccessTest(testCase: { input: string[]; expected?: ConsoleArgs; expectedMatching?: Partial<ConsoleArgs> }) {
        const args = act(testCase.input);

        expect(args).toMatchObject(testCase.expected ?? testCase.expectedMatching ?? {});
    }

    function runThrowTest(input: string[]) {
        expectToThrow(() => act(input));

        verify(mockedProcess.exit(1)).once();
    }
});
