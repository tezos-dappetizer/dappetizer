import { verifyPropertyNames } from '../../../test-utilities/mocks';
import { DappetizerConfig } from '../../indexer/src/config/dappetizer-config';
import { ConfigElement, ConfigObjectElement } from '../../utils/src/configuration/public-api';
import { IndexerModuleConfig, IndexerModulesConfig } from '../src/indexer-modules-config';

describe(IndexerModulesConfig.name, () => {
    function act(thisJson: DappetizerConfig['modules']) {
        const rootJson = { [IndexerModulesConfig.ROOT_NAME]: thisJson };
        return new IndexerModulesConfig(new ConfigObjectElement(rootJson, 'config.json', '$'));
    }

    it('should be created with all values specified', () => {
        const module1 = { id: 'id0' };
        const module2 = { id: 'id1', config: 'config2' };

        const config = act([module1, module2]);

        expect(config.modules).toHaveLength(2);
        verifyConfig(config.modules[0]!, 0, undefined);
        verifyConfig(config.modules[1]!, 1, 'config2');

        function verifyConfig(module: IndexerModuleConfig, expectedIndex: number, expectedValue: unknown) {
            expect(module.id).toBe(`id${expectedIndex}`);
            expect(module.config).toBeInstanceOf(ConfigElement);
            expect(module.config.value).toBe(expectedValue);
            expect(module.config.filePath).toBe('config.json');
            expect(module.config.propertyPath).toBe(`$.modules[${expectedIndex}].config`);
        }
    });

    it('should expose its properties in DappetizerConfig', () => {
        expect(IndexerModulesConfig.ROOT_NAME).toBe<keyof DappetizerConfig>('modules');

        const config = act([{ id: 'id' }]);
        verifyPropertyNames<DappetizerConfig['modules'][number]>(config.modules[0]!, ['id', 'config']);
    });
});
