import { exec } from 'child_process';

import { verifyFileSystem } from './file-system-verifier';
import { deleteFileOrDir, runDappetizer } from '../../../test-utilities/mocks';
import { cliCommands, cliOptions } from '../../utils/src/cli/cli-defaults';
import { generatedPaths } from '../src/helpers/generated-paths';

describe('"init" command without contract tests', () => {
    const outDirPath = './integration-tests/.tmp-init-command-without-contract';

    beforeAll(() => {
        deleteFileOrDir(outDirPath);
    });

    it('should generate initial app with NPM packages installed', async () => {
        await runDappetizer([
            cliCommands.INIT,
            `--${cliOptions.OUT_DIR}='${outDirPath}'`,
            `--${cliOptions.NETWORK}=skynet`,
            `--${cliOptions.TEZOS_ENDPOINT}=http://tezos-node.wtf/`,
            `--no-${cliOptions.NPM_INSTALL}`,
        ]);

        verifyFileSystem(outDirPath, {
            [generatedPaths.DAPPETIZER_CONFIG_TS]: contents => {
                expect(contents).toIncludeMultiple([
                    `modules: [{`,
                    `networks: loadDappetizerNetworkConfigs(__dirname),`,
                    `usageStatistics: {`,
                ]);
            },
            ['dappetizer.skynet.config.json']: contents => {
                expect(contents).toIncludeMultiple([
                    `"fromBlockLevel": 0`,
                    `"tezosNode"`,
                    `"url": "http://tezos-node.wtf/"`,
                ]);
                expect(contents).not.toInclude('contracts');
            },
            [generatedPaths.DOCKERFILE]: contents => {
                expect(contents).toInclude('CMD ["./node_modules/.bin/dappetizer", "start"]');
            },
            [generatedPaths.PACKAGE_JSON]: contents => {
                expect(contents).toInclude('"build": "tsc');
            },
            [generatedPaths.TSCONFIG_JSON]: contents => {
                expect(contents).toInclude('"module": "CommonJS"');
            },
            [generatedPaths.SRC]: {
                [generatedPaths.INDEX_TS]: contents => {
                    expect(contents).toInclude('export const indexerModule');
                },
            },
        });

        await verifyDockerfileSyntax(outDirPath);
    });
});

async function verifyDockerfileSyntax(cwd: string) {
    return new Promise<void>((resolve, reject) => {
        exec(
            `docker run --rm -i hadolint/hadolint < Dockerfile`,
            { cwd },
            (error, stdout, stderr) => {
                if (!error) {
                    resolve();
                } else {
                    reject(new Error(`Failed Dockerfile validation executed in: ${cwd}.\n`
                        + `${(error as any).toString() as string}\n`
                        + `Console standard output: ${stdout}\n`
                        + `Console error output: ${stderr}`));
                }
            },
        );
    });
}
