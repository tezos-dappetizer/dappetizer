import fs from 'fs';
import path from 'path';

export type FileVerifier = (contents: string, filePath: string) => void;
export type DirectoryVerifier = { [name: string]: FileVerifier | DirectoryVerifier };

export function verifyFileSystem(dirPath: string, expectedDirContents: DirectoryVerifier) {
    const actualNames = fs.readdirSync(dirPath).sort();
    const expectedNames = Object.keys(expectedDirContents).sort();
    expect(actualNames).toEqual(expectedNames);

    for (const [name, verifier] of Object.entries(expectedDirContents)) {
        const itemPath = path.resolve(dirPath, name);

        if (typeof verifier === 'function') {
            const fileContents = fs.readFileSync(itemPath).toString();
            verifier(fileContents, itemPath);
        } else {
            verifyFileSystem(itemPath, verifier);
        }
    }
}
