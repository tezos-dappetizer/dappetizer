import { tezosNodeUrlForIntegrationTests } from '../../../test-utilities/int-tezos-node';
import { deleteFileOrDir, readFileText, runDappetizer, writeFileText } from '../../../test-utilities/mocks';
import { cliCommands, cliOptions } from '../../utils/src/cli/cli-defaults';
import { generatedPaths } from '../src/helpers/generated-paths';

describe('"update" command tests', () => {
    const outDirPath = './integration-tests/.tmp-update-command';
    const tezosDomainsAddress = 'KT191reDVKrLxU9rjTSxg53wRqj6zh8pnHgr';

    beforeAll(() => {
        deleteFileOrDir(outDirPath);
    });

    it('should write parameter types', async () => {
        await runDappetizer([
            cliCommands.INIT,
            tezosDomainsAddress,
            `--${cliOptions.CONTRACT_NAME}=TezosDomains`,
            `--${cliOptions.OUT_DIR}='${outDirPath}'`,
            `--no-${cliOptions.NPM_INSTALL}`, // Use local packages -> compile against current code.
            `--${cliOptions.TEZOS_ENDPOINT}=${tezosNodeUrlForIntegrationTests}`,
        ]);

        const parametersPath = `${outDirPath}/${generatedPaths.SRC}/tezos-domains-indexer-interfaces.generated.ts`;
        const expectedParametersCode = readFileText(parametersPath);
        expect(expectedParametersCode).toIncludeMultiple([
            'export interface TezosDomainsBuyParameter {',
            'export interface TezosDomainsCurrentStorage {',
        ]);

        writeFileText(parametersPath, 'gibberish');

        await runDappetizer([
            cliCommands.UPDATE,
            'TezosDomains',
            tezosDomainsAddress,
            `--${cliOptions.OUT_DIR}='${outDirPath}'`,
            `--${cliOptions.TEZOS_ENDPOINT}=${tezosNodeUrlForIntegrationTests}`,
        ]);

        const updatedParametersCode = readFileText(parametersPath);
        expect(updatedParametersCode).toBe(expectedParametersCode);
    });
});
