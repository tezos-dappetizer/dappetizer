import { exec } from 'child_process';
import { noop } from 'lodash';

import { verifyFileSystem } from './file-system-verifier';
import { tezosNodeUrlForIntegrationTests } from '../../../test-utilities/int-tezos-node';
import { deleteFileOrDir, readFileText, runDappetizer, writeFileText } from '../../../test-utilities/mocks';
import { DappetizerConfig } from '../../indexer/src/public-api';
import { cliCommands, cliOptions } from '../../utils/src/cli/cli-defaults';
import { generatedPaths } from '../src/helpers/generated-paths';

describe('"init" command with contract tests', () => {
    const outDirPath = './integration-tests/.tmp-init-command-with-contract';
    const tezosDomainsAddress = 'KT191reDVKrLxU9rjTSxg53wRqj6zh8pnHgr';

    beforeAll(() => {
        deleteFileOrDir(outDirPath);
    });

    it('should generate initial app that compiles with local packages and runs', async () => {
        await runDappetizer([
            cliCommands.INIT,
            tezosDomainsAddress,
            `--${cliOptions.CONTRACT_NAME}=TezosDomains`,
            `--${cliOptions.OUT_DIR}='${outDirPath}'`,
            `--no-${cliOptions.NPM_INSTALL}`, // Use local packages -> compile against current code.
            `--${cliOptions.TEZOS_ENDPOINT}=${tezosNodeUrlForIntegrationTests}`,
        ]);

        // Verify structure & important snippets.
        verifyFileSystem(outDirPath, {
            'dappetizer.mainnet.config.json': contents => {
                expect(contents).toIncludeMultiple([
                    `"fromBlockLevel": 851969`,
                    `"tezosNode"`,
                    `"TezosDomains"`,
                    `"KT191reDVKrLxU9rjTSxg53wRqj6zh8pnHgr"`,
                ]);
            },
            // Verified in init-command-without-contract-tests.spec.ts.
            [generatedPaths.DAPPETIZER_CONFIG_TS]: noop,
            [generatedPaths.DOCKERFILE]: noop,
            [generatedPaths.PACKAGE_JSON]: noop,
            [generatedPaths.TSCONFIG_JSON]: noop,
            [generatedPaths.SRC]: {
                [generatedPaths.INDEX_TS]: contents => {
                    expect(contents).toInclude('createContractIndexerFromDecorators(new TezosDomainsIndexer())');
                },
                'tezos-domains-indexer-interfaces.generated.ts': contents => {
                    expect(contents).toIncludeMultiple([
                        'export interface TezosDomainsBuyParameter {',
                        'export interface TezosDomainsCurrentStorage {',
                    ]);
                },
                'tezos-domains-indexer.ts': contents => {
                    expect(contents).toInclude('export class TezosDomainsIndexer {');
                },
            },
        });

        setBlockLevels(outDirPath, { from: 1966671, to: 1966672 });
        await runNpmScript('npm run build', outDirPath);
        await runNpmScript('npx dappetizer start', outDirPath);
    });
});

function setBlockLevels(outDirPath: string, blocks: { from: number; to: number }) {
    const networkConfigPath = `${outDirPath}/dappetizer.mainnet.config.json`;
    const networkConfig = JSON.parse(readFileText(networkConfigPath)) as DappetizerConfig['networks'][string];

    networkConfig.indexing.fromBlockLevel = blocks.from;
    networkConfig.indexing.toBlockLevel = blocks.to;
    writeFileText(networkConfigPath, JSON.stringify(networkConfig, undefined, 4));
}

async function runNpmScript(command: string, cwd: string) {
    return new Promise<void>((resolve, reject) => {
        exec(command, { cwd }, (error, stdout, stderr) => {
            if (!error) {
                resolve();
            } else {
                reject(new Error(`Failed command '${command}' executed in: ${cwd}.\n`
                    + `${(error as any).toString() as string}\n`
                    + `Console standard output: ${stdout}\n`
                    + `Console error output: ${stderr}`));
            }
        });
    });
}
