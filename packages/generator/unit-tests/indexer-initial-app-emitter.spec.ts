import { Writable } from 'ts-essentials';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { TestLogger } from '../../../test-utilities/mocks';
import { ContractFileGenerator } from '../src/file-generators/contract-indexer/contract-file-generator';
import { FileEmitter } from '../src/file-generators/file-emitter';
import { StructuralFileGenerator } from '../src/file-generators/structural/structural-file-generator';
import { ContractDetails, ContractDetailsResolver } from '../src/helpers/contract-details-resolver';
import { IndexerModuleNameResolver } from '../src/helpers/indexer-module-name-resolver';
import { NpmDappetizerInstaller } from '../src/helpers/npm-dappetizer-installer';
import { OutDirInitializer } from '../src/helpers/out-dir-initializer';
import { EmitInitialAppOptions } from '../src/indexer-emitter';
import { IndexerInitialAppEmitter } from '../src/indexer-initial-app-emitter';

describe(IndexerInitialAppEmitter.name, () => {
    let target: IndexerInitialAppEmitter;
    let npmDappetizerInstaller: NpmDappetizerInstaller;
    let fileEmitter: FileEmitter;
    let outDirInitializer: OutDirInitializer;
    let contractDetailsResolver: ContractDetailsResolver;
    let indexerModuleNameResolver: IndexerModuleNameResolver;
    let structuralGenerators: readonly StructuralFileGenerator[];
    let contractGenerators: readonly ContractFileGenerator[];
    let logger: TestLogger;

    let contractDetails: ContractDetails;
    let options: Writable<EmitInitialAppOptions>;

    beforeEach(() => {
        [npmDappetizerInstaller, fileEmitter, outDirInitializer, contractDetailsResolver, indexerModuleNameResolver]
            = [mock(), mock(), mock(), mock(), mock()];
        structuralGenerators = 'mockedStructuralGenerators' as any;
        contractGenerators = 'mockedContractGenerators' as any;
        logger = new TestLogger();
        target = new IndexerInitialAppEmitter(
            instance(npmDappetizerInstaller),
            instance(fileEmitter),
            instance(outDirInitializer),
            instance(contractDetailsResolver),
            instance(indexerModuleNameResolver),
            structuralGenerators,
            contractGenerators,
            logger,
        );

        contractDetails = 'mockedContractDetails' as any;
        options = {
            outDirPath: './out/dir',
            installNpmDependencies: true,
            enableUsageStatistics: true,
            contractAddress: 'KT1Ezht4PDKZri7aVppVGT4Jkw39sesaFnww',
            contractName: 'Foo Contract',
            indexerModuleName: 'ExplicitModule',
        } as EmitInitialAppOptions;

        when(contractDetailsResolver.resolve('KT1Ezht4PDKZri7aVppVGT4Jkw39sesaFnww', 'Foo Contract')).thenResolve(contractDetails);
        when(indexerModuleNameResolver.resolve('ExplicitModule', './out/dir')).thenResolve('ResolvedModule');
    });

    it('should install NPM packages and generate initial app for a contract', async () => {
        await target.emitInitialApp(options);

        logger.loggedSingle().verify('Information');
        verify(outDirInitializer.init('./out/dir', anything())).once();
        verify(npmDappetizerInstaller.installPackages('./out/dir')).once();
        verify(fileEmitter.emitFiles(
            './out/dir',
            deepEqual({ ...options, indexerModuleName: 'ResolvedModule', contractDetails }),
            structuralGenerators,
        )).once();
        verify(fileEmitter.emitFiles(
            './out/dir',
            deepEqual({ ...options, contractDetails }),
            contractGenerators,
        )).once();
    });

    it('should allow to skip installing NPM packages', async () => {
        options.installNpmDependencies = false;

        await target.emitInitialApp(options);

        verify(npmDappetizerInstaller.installPackages(anything())).never();
    });

    it('should not log info about usage statistics if not enabled', async () => {
        options.enableUsageStatistics = false;

        await target.emitInitialApp(options);

        logger.verifyNothingLogged();
    });

    it('should not generate contract indexer class if no contract specified', async () => {
        options.contractAddress = null;

        await target.emitInitialApp(options);

        verify(fileEmitter.emitFiles('./out/dir', anything(), anything())).once();
        verify(contractDetailsResolver.resolve(anything(), anything())).never();
    });
});
