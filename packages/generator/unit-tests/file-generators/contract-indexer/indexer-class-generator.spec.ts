import { EOL } from 'os';

import { describeMember } from '../../../../../test-utilities/mocks';
import { michelsonTypePrims } from '../../../../indexer/src/michelson/michelson-prims';
import { MichelsonSchema } from '../../../../indexer/src/michelson/michelson-schema';
import { asReadonly } from '../../../../utils/src/public-api';
import { ContractFileOptions } from '../../../src/file-generators/contract-indexer/contract-file-generator';
import { IndexerClassGenerator } from '../../../src/file-generators/contract-indexer/indexer-class-generator';

describe(IndexerClassGenerator.name, () => {
    let target: IndexerClassGenerator;
    let options: ContractFileOptions;

    beforeEach(() => {
        target = new IndexerClassGenerator();

        options = {
            contractDetails: {
                name: 'FooBar',
                contract: {
                    parameterSchemas: {
                        entrypoints: {
                            ['mint' as string]: {} as MichelsonSchema,
                            ['_mint' as string]: {} as MichelsonSchema,
                            ['transfer' as string]: {} as MichelsonSchema,
                        },
                    },
                    storageSchema: new MichelsonSchema({
                        prim: michelsonTypePrims.BIG_MAP,
                        args: [{ prim: michelsonTypePrims.STRING }, { prim: michelsonTypePrims.NAT }],
                    }),
                    bigMaps: asReadonly([
                        { path: asReadonly(['ledger']), pathStr: 'ledger' },
                        { path: asReadonly(['memes', 'lol']), pathStr: 'memes.lol' },
                    ]),
                    events: asReadonly([
                        { tag: 'product_ordered', schema: {} as MichelsonSchema },
                        { tag: null, schema: null },
                    ]),
                },
            },
        } as ContractFileOptions;
    });

    describeMember<typeof target>('getRelPath', () => {
        it('should be correct', () => {
            const relPath = target.getRelPath(options);

            expect(relPath).toBe('src/foo-bar-indexer.ts');
        });
    });

    describeMember<typeof target>('generateContents', () => {
        it('should generate indexer class with constructor if more entrypoints', () => {
            const contents = target.generateContents(options);

            expect(contents).toBe([
                `import { DbContext } from '@tezos-dappetizer/database';`,
                `import {`,
                `    contractFilter,`,
                `    indexBigMapUpdate,`,
                `    indexEntrypoint,`,
                `    indexEvent,`,
                `    indexOrigination,`,
                `    indexStorageChange,`,
                `} from '@tezos-dappetizer/decorators';`,
                `import {`,
                `    BigMapUpdateIndexingContext,`,
                `    EventIndexingContext,`,
                `    OriginationIndexingContext,`,
                `    StorageChangeIndexingContext,`,
                `    TransactionIndexingContext,`,
                `} from '@tezos-dappetizer/indexer';`,
                ``,
                `import {`,
                `    FooBarChangedStorage,`,
                `    FooBarInitialStorage,`,
                `    FooBarLedgerKey,`,
                `    FooBarLedgerValue,`,
                `    FooBarMemesLolKey,`,
                `    FooBarMemesLolValue,`,
                `    FooBarMint2Parameter,`,
                `    FooBarMintParameter,`,
                `    FooBarProductOrderedPayload,`,
                `    FooBarTransferParameter,`,
                `} from './foo-bar-indexer-interfaces.generated';`,
                ``,
                `@contractFilter({ name: 'FooBar' })`,
                `export class FooBarIndexer {`,
                `    @indexOrigination()`,
                `    async indexOrigination(`,
                `        initialStorage: FooBarInitialStorage,`,
                `        dbContext: DbContext,`,
                `        indexingContext: OriginationIndexingContext,`,
                `    ): Promise<void> {`,
                `        // Implement your indexing logic here or delete the method if not needed.`,
                `    }`,
                ``,
                `    @indexEntrypoint('mint')`,
                `    async indexMint(`,
                `        parameter: FooBarMintParameter,`,
                `        dbContext: DbContext,`,
                `        indexingContext: TransactionIndexingContext,`,
                `    ): Promise<void> {`,
                `        // Implement your indexing logic here or delete the method if not needed.`,
                `    }`,
                ``,
                `    @indexEntrypoint('_mint')`,
                `    async indexMint2(`,
                `        parameter: FooBarMint2Parameter,`,
                `        dbContext: DbContext,`,
                `        indexingContext: TransactionIndexingContext,`,
                `    ): Promise<void> {`,
                `        // Implement your indexing logic here or delete the method if not needed.`,
                `    }`,
                ``,
                `    @indexEntrypoint('transfer')`,
                `    async indexTransfer(`,
                `        parameter: FooBarTransferParameter,`,
                `        dbContext: DbContext,`,
                `        indexingContext: TransactionIndexingContext,`,
                `    ): Promise<void> {`,
                `        // Implement your indexing logic here or delete the method if not needed.`,
                `    }`,
                ``,
                `    @indexStorageChange()`,
                `    async indexStorageChange(`,
                `        newStorage: FooBarChangedStorage,`,
                `        dbContext: DbContext,`,
                `        indexingContext: StorageChangeIndexingContext,`,
                `    ): Promise<void> {`,
                `        // Implement your indexing logic here or delete the method if not needed.`,
                `    }`,
                ``,
                `    @indexBigMapUpdate({ path: ['ledger'] })`,
                `    async indexLedgerUpdate(`,
                `        key: FooBarLedgerKey,`,
                `        value: FooBarLedgerValue | undefined, // Undefined represents a removal.`,
                `        dbContext: DbContext,`,
                `        indexingContext: BigMapUpdateIndexingContext,`,
                `    ): Promise<void> {`,
                `        // Implement your indexing logic here or delete the method if not needed.`,
                `    }`,
                ``,
                `    @indexBigMapUpdate({ path: ['memes', 'lol'] })`,
                `    async indexMemesLolUpdate(`,
                `        key: FooBarMemesLolKey,`,
                `        value: FooBarMemesLolValue | undefined, // Undefined represents a removal.`,
                `        dbContext: DbContext,`,
                `        indexingContext: BigMapUpdateIndexingContext,`,
                `    ): Promise<void> {`,
                `        // Implement your indexing logic here or delete the method if not needed.`,
                `    }`,
                ``,
                `    @indexEvent('product_ordered')`,
                `    async indexProductOrderedEvent(`,
                `        payload: FooBarProductOrderedPayload,`,
                `        dbContext: DbContext,`,
                `        indexingContext: EventIndexingContext,`,
                `    ): Promise<void> {`,
                `        // Implement your indexing logic here or delete the method if not needed.`,
                `    }`,
                ``,
                `    @indexEvent(null)`,
                `    async indexUnnamedEvent(`,
                `        payload: any,`,
                `        dbContext: DbContext,`,
                `        indexingContext: EventIndexingContext,`,
                `    ): Promise<void> {`,
                `        // Implement your indexing logic here or delete the method if not needed.`,
                `    }`,
                `}`,
                ``,
            ].join(EOL));
        });
    });
});
