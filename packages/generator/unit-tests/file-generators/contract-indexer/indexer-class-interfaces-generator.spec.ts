import { EOL } from 'os';
import { Writable } from 'ts-essentials';
import { instance, mock, when } from 'ts-mockito';

import { describeMember } from '../../../../../test-utilities/mocks';
import { Contract, ContractEventInfo, MichelsonSchema, michelsonTypePrims } from '../../../../indexer/src/public-api';
import { asReadonly } from '../../../../utils/src/public-api';
import { ContractFileOptions } from '../../../src/file-generators/contract-indexer/contract-file-generator';
import {
    IndexerClassInterfacesGenerator,
} from '../../../src/file-generators/contract-indexer/indexer-class-interfaces-generator';
import { BigMapTyping } from '../../../src/type-from-michelson-schema/big-map-typing';
import {
    TypeFromMichelsonSchemaGenerator,
} from '../../../src/type-from-michelson-schema/type-from-michelson-schema-generator';

describe(IndexerClassInterfacesGenerator.name, () => {
    let target: IndexerClassInterfacesGenerator;
    let typeFromMichelsonSchemaGenerator: TypeFromMichelsonSchemaGenerator;

    let options: ContractFileOptions;
    let contract: Writable<Contract>;

    beforeEach(() => {
        typeFromMichelsonSchemaGenerator = mock();
        target = new IndexerClassInterfacesGenerator(instance(typeFromMichelsonSchemaGenerator));

        const mintSchema: MichelsonSchema = 'mockedMintSchema' as any;
        const mint2Schema: MichelsonSchema = 'mockedMint2Schema' as any;
        const transferSchema: MichelsonSchema = 'mockedTransferSchema' as any;
        const defaultEntrySchema: MichelsonSchema = 'mockedDefaultEntrySchema' as any;
        const storageSchema = new MichelsonSchema({ prim: michelsonTypePrims.STRING, annots: ['%sss'] }); // No big map.
        const keySchema: MichelsonSchema = 'mockedBigMapKeySchema' as any;
        const valueSchema: MichelsonSchema = 'mockedBigMapValueSchema' as any;

        contract = {
            address: 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5',
            parameterSchemas: {
                entrypoints: {
                    ['mint' as string]: mintSchema,
                    ['_mint' as string]: mint2Schema, // Conflict should be resolved.
                    ['transfer' as string]: transferSchema,
                },
                default: defaultEntrySchema,
            },
            storageSchema,
            bigMaps: asReadonly([{ pathStr: 'memes.lol', keySchema, valueSchema }]),
            events: [] as readonly ContractEventInfo[],
        } as Contract;
        options = {
            contractDetails: { name: 'Foo', contract },
            tezosNetwork: 'lolnet',
        } as ContractFileOptions;

        setupSchemaGenerator(mintSchema, 'FooMintParameter', BigMapTyping.InitialValues);
        setupSchemaGenerator(mint2Schema, 'FooMint2Parameter', BigMapTyping.InitialValues);
        setupSchemaGenerator(transferSchema, 'FooTransferParameter', BigMapTyping.InitialValues);
        setupSchemaGenerator(defaultEntrySchema, 'FooDefaultParameter', BigMapTyping.InitialValues);
        setupSchemaGenerator(storageSchema, 'FooStorage', BigMapTyping.TaquitoAbstraction);
        setupSchemaGenerator(keySchema, 'FooMemesLolKey', BigMapTyping.Unexpected);
        setupSchemaGenerator(valueSchema, 'FooMemesLolValue', BigMapTyping.Unexpected);
    });

    describeMember<typeof target>('getRelPath', () => {
        it('should be correct', () => {
            const relPath = target.getRelPath(options);

            expect(relPath).toBe('src/foo-indexer-interfaces.generated.ts');
        });
    });

    describeMember<typeof target>('generateContents', () => {
        it('should generate basic config', () => {
            runTest([
                `/* istanbul ignore next */`,
                `/* eslint-disable */`,
                ``,
                `// This file was generated.`,
                `// It should NOT be modified manually rather it should be regenerated.`,
                `// Contract: KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5`,
                `// Tezos network: lolnet`,
                ``,
                `import {`,
                `    FooMemesLolKeyDep,`,
                `    FooMemesLolValueDep,`,
                `    FooMint2ParameterDep,`,
                `    FooMintParameterDep,`,
                `    FooStorageDep,`,
                `    FooTransferParameterDep,`,
                `} from 'Dependencies';`,
                ``,
                `export type FooParameter =`,
                `    | { entrypoint: 'mint'; value: FooMintParameter }`,
                `    | { entrypoint: '_mint'; value: FooMint2Parameter }`,
                `    | { entrypoint: 'transfer'; value: FooTransferParameter };`,
                ``,
                `export type FooMintParameter = Mocked;`,
                `export type FooMint2Parameter = Mocked;`,
                `export type FooTransferParameter = Mocked;`,
                `export type FooStorage = Mocked;`,
                `export type FooMemesLolKey = Mocked;`,
                `export type FooMemesLolValue = Mocked;`,
            ]);
        });

        it('should generate multiple storage types if storage contains big map', () => {
            const storageSchema = new MichelsonSchema({
                prim: michelsonTypePrims.BIG_MAP,
                args: [{ prim: michelsonTypePrims.STRING }, { prim: michelsonTypePrims.NAT }],
            });
            contract.storageSchema = storageSchema;

            setupSchemaGenerator(storageSchema, 'FooCurrentStorage', BigMapTyping.TaquitoAbstraction);
            setupSchemaGenerator(storageSchema, 'FooInitialStorage', BigMapTyping.InitialValues);
            setupSchemaGenerator(storageSchema, 'FooChangedStorage', BigMapTyping.RawID);

            runTest([
                `/* istanbul ignore next */`,
                `/* eslint-disable */`,
                ``,
                `// This file was generated.`,
                `// It should NOT be modified manually rather it should be regenerated.`,
                `// Contract: KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5`,
                `// Tezos network: lolnet`,
                ``,
                `import {`,
                `    FooChangedStorageDep,`,
                `    FooCurrentStorageDep,`,
                `    FooInitialStorageDep,`,
                `    FooMemesLolKeyDep,`,
                `    FooMemesLolValueDep,`,
                `    FooMint2ParameterDep,`,
                `    FooMintParameterDep,`,
                `    FooTransferParameterDep,`,
                `} from 'Dependencies';`,
                ``,
                `export type FooParameter =`,
                `    | { entrypoint: 'mint'; value: FooMintParameter }`,
                `    | { entrypoint: '_mint'; value: FooMint2Parameter }`,
                `    | { entrypoint: 'transfer'; value: FooTransferParameter };`,
                ``,
                `export type FooMintParameter = Mocked;`,
                `export type FooMint2Parameter = Mocked;`,
                `export type FooTransferParameter = Mocked;`,
                `export type FooCurrentStorage = Mocked;`,
                `export type FooChangedStorage = Mocked;`,
                `export type FooInitialStorage = Mocked;`,
                `export type FooMemesLolKey = Mocked;`,
                `export type FooMemesLolValue = Mocked;`,
            ]);
        });

        it('should generate default entrypoint if no named entrypoints', () => {
            contract.parameterSchemas = {
                entrypoints: {},
                default: contract.parameterSchemas.default,
            };

            runTest([
                `/* istanbul ignore next */`,
                `/* eslint-disable */`,
                ``,
                `// This file was generated.`,
                `// It should NOT be modified manually rather it should be regenerated.`,
                `// Contract: KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5`,
                `// Tezos network: lolnet`,
                ``,
                `import {`,
                `    FooDefaultParameterDep,`,
                `    FooMemesLolKeyDep,`,
                `    FooMemesLolValueDep,`,
                `    FooStorageDep,`,
                `} from 'Dependencies';`,
                ``,
                `export type FooParameter =`,
                `    | { entrypoint: 'default'; value: FooDefaultParameter };`,
                ``,
                `export type FooDefaultParameter = Mocked;`,
                `export type FooStorage = Mocked;`,
                `export type FooMemesLolKey = Mocked;`,
                `export type FooMemesLolValue = Mocked;`,
            ]);
        });

        it('should generate types of event payloads', () => {
            const productOrderedSchema: MichelsonSchema = 'mockedProductOrderedSchema' as any;
            const shippedSchema1: MichelsonSchema = 'mockedShippedSchema1' as any;
            const shippedSchema2: MichelsonSchema = 'mockedShippedSchema2' as any;
            const unnamedSchema: MichelsonSchema = 'mockedUnnamedSchema' as any;
            contract.events = [
                { tag: 'product_ordered', schema: productOrderedSchema },
                { tag: 'shipped', schema: shippedSchema1 },
                { tag: 'shipped', schema: shippedSchema2 },
                { tag: null, schema: unnamedSchema },
                { tag: null, schema: null },
            ];
            setupSchemaGenerator(productOrderedSchema, 'FooProductOrderedPayload', BigMapTyping.Unexpected);
            setupSchemaGenerator(shippedSchema1, 'FooShippedPayload1', BigMapTyping.Unexpected);
            setupSchemaGenerator(shippedSchema2, 'FooShippedPayload2', BigMapTyping.Unexpected);
            setupSchemaGenerator(unnamedSchema, 'FooUnnamedPayload1', BigMapTyping.Unexpected);

            runTest([
                `/* istanbul ignore next */`,
                `/* eslint-disable */`,
                ``,
                `// This file was generated.`,
                `// It should NOT be modified manually rather it should be regenerated.`,
                `// Contract: KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5`,
                `// Tezos network: lolnet`,
                ``,
                `import {`,
                `    FooMemesLolKeyDep,`,
                `    FooMemesLolValueDep,`,
                `    FooMint2ParameterDep,`,
                `    FooMintParameterDep,`,
                `    FooProductOrderedPayloadDep,`,
                `    FooShippedPayload1Dep,`,
                `    FooShippedPayload2Dep,`,
                `    FooStorageDep,`,
                `    FooTransferParameterDep,`,
                `    FooUnnamedPayload1Dep,`,
                `} from 'Dependencies';`,
                ``,
                `export type FooParameter =`,
                `    | { entrypoint: 'mint'; value: FooMintParameter }`,
                `    | { entrypoint: '_mint'; value: FooMint2Parameter }`,
                `    | { entrypoint: 'transfer'; value: FooTransferParameter };`,
                ``,
                `export type FooMintParameter = Mocked;`,
                `export type FooMint2Parameter = Mocked;`,
                `export type FooTransferParameter = Mocked;`,
                `export type FooStorage = Mocked;`,
                `export type FooMemesLolKey = Mocked;`,
                `export type FooMemesLolValue = Mocked;`,
                `export type FooProductOrderedPayload = Mocked;`,
                `export type FooShippedPayload1 = Mocked;`,
                `export type FooShippedPayload2 = Mocked;`,
                `export type FooShippedPayload = FooShippedPayload1 | FooShippedPayload2;`,
                ``,
                `export type FooUnnamedPayload1 = Mocked;`,
            ]);
        });

        function runTest(expectedLines: string[]) {
            const contents = target.generateContents(options);

            expect(contents).toBe(expectedLines.join(EOL));
        }
    });

    function setupSchemaGenerator(schema: MichelsonSchema, typeName: string, bigMapTyping: BigMapTyping) {
        when(typeFromMichelsonSchemaGenerator.generate(schema, typeName, bigMapTyping))
            .thenReturn({
                codeLines: [`export type ${typeName} = Mocked;`],
                imports: [{ import: `${typeName}Dep`, from: 'Dependencies' }],
            });
    }
});
