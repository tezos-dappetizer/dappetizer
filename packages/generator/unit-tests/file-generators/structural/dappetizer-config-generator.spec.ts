import { EOL } from 'os';
import { Writable } from 'ts-essentials';

import { describeMember } from '../../../../../test-utilities/mocks';
import { DappetizerConfigGenerator } from '../../../src/file-generators/structural/dappetizer-config-generator';
import { StructuralFileOptions } from '../../../src/file-generators/structural/structural-file-generator';
import { generatedPaths } from '../../../src/helpers/generated-paths';

describe(DappetizerConfigGenerator.name, () => {
    let target: DappetizerConfigGenerator;
    let options: Writable<StructuralFileOptions>;

    beforeEach(() => {
        target = new DappetizerConfigGenerator();

        options = {
            tezosNodeUrl: 'https://tezos.node/',
            tezosNetwork: 'skynet',
            enableUsageStatistics: 'statsBoolean' as any,
        } as StructuralFileOptions;
    });

    describeMember<typeof target>('getRelPath', () => {
        it('should return static file', () => {
            const relPath = target.getRelPath(options);

            expect(relPath).toBe(generatedPaths.DAPPETIZER_CONFIG_TS);
        });
    });

    describeMember<typeof target>('generateContents', () => {
        it('should generate basic config without contract', () => {
            const contents = target.generateContents(options);

            expect(contents).toBe([
                `import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';`,
                `import { loadDappetizerNetworkConfigs } from '@tezos-dappetizer/indexer';`,
                ``,
                `const config: DappetizerConfigUsingDb = {`,
                `    modules: [{`,
                `        id: '.', // This project is the indexer module itself.`,
                `    }],`,
                `    networks: loadDappetizerNetworkConfigs(__dirname),`,
                `    database: {`,
                `        type: 'sqlite',`,
                `        database: 'database.sqlite',`,
                ``,
                `        // If you want to use PostgreSQL:`,
                `        // type: 'postgres',`,
                `        // host: 'localhost',`,
                `        // port: 5432,`,
                `        // username: 'postgres',`,
                `        // password: 'postgrespassword',`,
                `        // database: 'postgres',`,
                `        // schema: 'indexer',`,
                `    },`,
                `    usageStatistics: {`,
                `        enabled: statsBoolean,`,
                `    },`,
                `};`,
                ``,
                `export default config;`,
                ``,
            ].join(EOL));
        });
    });
});
