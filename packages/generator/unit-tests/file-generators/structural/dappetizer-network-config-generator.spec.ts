import { Writable } from 'ts-essentials';

import { describeMember, expectToThrow } from '../../../../../test-utilities/mocks';
import { MAINNET } from '../../../../utils/src/public-api';
import {
    DappetizerNetworkConfigGenerator,
    NetworkConfig,
} from '../../../src/file-generators/structural/dappetizer-network-config-generator';
import { StructuralFileOptions } from '../../../src/file-generators/structural/structural-file-generator';
import { ContractDetails } from '../../../src/helpers/contract-details-resolver';
import { Exposed } from '../exposed-json-file-generator';

describe(DappetizerNetworkConfigGenerator.name, () => {
    let target: Exposed<DappetizerNetworkConfigGenerator>;
    let options: Writable<StructuralFileOptions>;

    beforeEach(() => {
        target = new DappetizerNetworkConfigGenerator() as any;

        options = {
            tezosNetwork: 'skynet',
            tezosNodeUrl: 'http://tezos.node/',
            isTezosNodeUrlExplicit: false,
            contractDetails: null,
        } as StructuralFileOptions;
    });

    describeMember<typeof target>('getRelPath', () => {
        it('should be correct', () => {
            const relPath = target.getRelPath(options);

            expect(relPath).toBe('dappetizer.skynet.config.json');
        });
    });

    describeMember<typeof target>('validateExistingContents', () => {
        it.each([
            ['config', {
                indexing: {},
                tezosNode: {},
            } as NetworkConfig],
            ['config with contracts', {
                indexing: { contracts: [{}, {}] },
                tezosNode: {},
            } as NetworkConfig],
        ])('should pass if valid %s', (_desc, config) => {
            target.validateExistingContents(config);
        });

        it.each([
            ['$', 'wtf' as unknown as NetworkConfig],
            ['$.tezosNode', {
                indexing: {},
                tezosNode: 'wtf' as any,
            } as NetworkConfig],
            ['$.indexing', {
                indexing: 'wtf' as any,
                tezosNode: {},
            } as NetworkConfig],
            ['$.indexing.contracts', {
                indexing: { contracts: 'wtf' as any },
                tezosNode: {},
            } as NetworkConfig],
            ['$.indexing.contracts[1]', {
                indexing: { contracts: [{}, 'wtf' as any] },
                tezosNode: {},
            } as NetworkConfig],
        ])('should fail if invalid %s', (expectedMessage, config) => {
            const error = expectToThrow(() => target.validateExistingContents(config));

            expect(error.message).toInclude(expectedMessage);
        });
    });

    describeMember<typeof target>('generateContentsObject', () => {
        let existingConfig: NetworkConfig | null;

        const act = () => target.generateContentsObject(options, existingConfig);

        beforeEach(() => {
            existingConfig = {
                indexing: { fromBlockLevel: 66 },
                tezosNode: { url: 'http://existing.node/' },
            };
        });

        it('should write config if previous does not exist', () => {
            existingConfig = null;

            const config = act();

            expect(config).toEqual({
                indexing: { fromBlockLevel: 0 },
                tezosNode: { url: 'http://tezos.node/' },
            });
        });

        it('should keep config if previously exists', () => {
            const config = act();

            expect(config).toEqual({
                indexing: { fromBlockLevel: 66 },
                tezosNode: { url: 'http://existing.node/' },
            });
        });

        it('should overwrite Tezos node URL if specified explicitly', () => {
            options.isTezosNodeUrlExplicit = true;

            const config = act();

            expect(config.tezosNode.url).toBe('http://tezos.node/');
        });

        describe('contract address', () => {
            beforeEach(() => {
                options.contractDetails = {
                    name: 'FooBar',
                    contract: { address: 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5' },
                } as ContractDetails;
            });

            it('should add contracts if not exist', () => {
                const config = act();

                expect(config.indexing.contracts).toEqual([
                    { name: 'FooBar', addresses: ['KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5'] },
                ]);
            });

            it('should replace address if contract with name exists', () => {
                existingConfig!.indexing.contracts = [
                    { name: 'FooBar', addresses: ['KT1BNpJgbtyb2JedTyM9G1xPuA4EWbeamCp4', 'KT1WESG5zjPWpTJvupm1HbaGSZG1YpNNHrk9'] },
                    { name: 'OmgLol', addresses: ['KT1KZ6fhfN92ouCeSQcSdCvyqMpA8zUvpyFK'] },
                ];

                const config = act();

                expect(config.indexing.contracts).toEqual([
                    { name: 'FooBar', addresses: ['KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5'] },
                    { name: 'OmgLol', addresses: ['KT1KZ6fhfN92ouCeSQcSdCvyqMpA8zUvpyFK'] },
                ]);
            });
        });

        it('should generate contract boost and correct block level if mainnet', () => {
            options.tezosNetwork = MAINNET;
            existingConfig = null;

            const config = act();

            expect(config).toEqual({
                indexing: {
                    fromBlockLevel: 851969,
                    contractBoost: { type: 'tzkt' },
                },
                tezosNode: { url: 'http://tezos.node/' },
            });
        });
    });
});
