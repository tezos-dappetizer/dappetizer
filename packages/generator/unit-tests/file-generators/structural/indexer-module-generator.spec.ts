import { EOL } from 'os';
import { Writable } from 'ts-essentials';

import { describeMember } from '../../../../../test-utilities/mocks';
import { IndexerModuleGenerator } from '../../../src/file-generators/structural/indexer-module-generator';
import { StructuralFileOptions } from '../../../src/file-generators/structural/structural-file-generator';
import { ContractDetails } from '../../../src/helpers/contract-details-resolver';

describe(IndexerModuleGenerator.name, () => {
    let target: IndexerModuleGenerator;
    let options: Writable<StructuralFileOptions>;

    beforeEach(() => {
        target = new IndexerModuleGenerator();
        options = { indexerModuleName: 'FooModule' } as StructuralFileOptions;
    });

    describeMember<typeof target>('getRelPath', () => {
        it('should return static file', () => {
            const relPath = target.getRelPath(options);

            expect(relPath).toBe('src/index.ts');
        });
    });

    describeMember<typeof target>('generateContents', () => {
        it('should generate basic config without indexer class', () => {
            runTest({
                expectedImportLines: [],
                expectedContractIndexerLines: [
                    `        // new MyContractIndexer(),`,
                ],
            });
        });

        it('should generate basic config with indexer class', () => {
            const contractDetails = { name: 'LolMemes' } as ContractDetails;
            options.contractDetails = contractDetails;

            runTest({
                expectedImportLines: [
                    `import { createContractIndexerFromDecorators } from '@tezos-dappetizer/decorators';`,
                    ``,
                    `import { LolMemesIndexer } from './lol-memes-indexer';`,
                ],
                expectedContractIndexerLines: [
                    `        createContractIndexerFromDecorators(new LolMemesIndexer()),`,
                ],
            });
        });

        function runTest(testCase: { expectedImportLines: string[]; expectedContractIndexerLines: string[] }) {
            const contents = target.generateContents(options);

            expect(contents).toBe([
                `import { IndexerModuleUsingDb } from '@tezos-dappetizer/database';`,
                ...testCase.expectedImportLines,
                ``,
                `export const indexerModule: IndexerModuleUsingDb = {`,
                `    name: 'FooModule',`,
                `    dbEntities: [`,
                `        // Register your DB entity classes to TypeORM here:`,
                `        // MyDbEntity,`,
                `    ],`,
                `    contractIndexers: [`,
                `        // Create your contract indexers here:`,
                ...testCase.expectedContractIndexerLines,
                `    ],`,
                `    blockDataIndexers: [`,
                `        // Create your block data indexers here:`,
                `        // new MyBlockDataIndexer(),`,
                `    ],`,
                `    // Create your indexing cycle handler here:`,
                `    // indexingCycleHandler: new MyIndexingCycleHandler(),`,
                `};`,
                ``,
            ].join(EOL));
        }
    });
});
