import { EOL } from 'os';

import { describeMember } from '../../../../../test-utilities/mocks';
import { DockerfileGenerator } from '../../../src/file-generators/structural/dockerfile-generator';
import { StructuralFileOptions } from '../../../src/file-generators/structural/structural-file-generator';
import { generatedPaths } from '../../../src/helpers/generated-paths';

describe(DockerfileGenerator.name, () => {
    let target: DockerfileGenerator;
    let options: StructuralFileOptions;

    beforeEach(() => {
        target = new DockerfileGenerator();
        options = {} as StructuralFileOptions;
    });

    describeMember<typeof target>('getRelPath', () => {
        it('should return static file', () => {
            expect(target.getRelPath(options)).toBe(generatedPaths.DOCKERFILE);
        });
    });

    describeMember<typeof target>('generateContents', () => {
        it('should generate basic config', () => {
            const contents = target.generateContents(options);

            expect(contents).toBe([
                `# Prepare files and a temp image to build.`,
                `FROM node:16-alpine3.11 AS builder`,
                `COPY package.json package-lock.json dappetizer.config.ts tsconfig.json /app/`,
                `COPY src /app/src`,
                ``,
                `# Build the app.`,
                `WORKDIR /app`,
                `RUN npm install && npm run build`,
                ``,
                `# Publish files to a final image.`,
                `FROM node:16-alpine3.11`,
                `COPY --from=builder /app/package.json /app/dappetizer.config.ts /app/`,
                `COPY --from=builder /app/dist /app/dist`,
                `COPY --from=builder /app/node_modules /app/node_modules`,
                ``,
                `# Run the app.`,
                `WORKDIR /app`,
                `CMD ["./node_modules/.bin/dappetizer", "start"]`,
                ``,
            ].join(EOL));
        });
    });
});
