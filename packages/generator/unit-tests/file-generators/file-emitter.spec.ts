import { randomUUID } from 'crypto';
import path from 'path';
import { anything, instance, mock, when } from 'ts-mockito';

import { expectToThrowAsync, TestLogger, verifyCalled } from '../../../../test-utilities/mocks';
import { FileSystem } from '../../../utils/src/public-api';
import { FileEmitter } from '../../src/file-generators/file-emitter';
import { FileGenerator } from '../../src/file-generators/file-generator';

interface FooOptions { value: number }

describe(FileEmitter.name, () => {
    let target: FileEmitter;
    let fileSystem: FileSystem;
    let logger: TestLogger;
    let options: FooOptions;

    let generator1: FileGenerator<FooOptions>;
    let generator2: FileGenerator<FooOptions>;
    let expectedFilePath1: string;
    let expectedFilePath2: string;

    const act = async () => target.emitFiles('./out/path', options, [instance(generator1), instance(generator2)]);

    beforeEach(() => {
        fileSystem = mock();
        logger = new TestLogger();
        target = new FileEmitter(instance(fileSystem), logger);
        options = { value: 123 };

        [generator1, expectedFilePath1] = setupGenerator('file1.txt');
        [generator2, expectedFilePath2] = setupGenerator('dir/file2.txt');
    });

    it('should emit files from all generators', async () => {
        await act();

        verifyCalled(fileSystem.readFileTextIfExists)
            .with(expectedFilePath1)
            .thenWith(expectedFilePath2)
            .thenNoMore();
        verifyCalled(fileSystem.writeFileText)
            .with(expectedFilePath1, 'file1.txt-generated')
            .thenWith(expectedFilePath2, 'dir/file2.txt-generated')
            .thenNoMore();

        logger.logged(0).verify('Information', { file: 'file1.txt' });
        logger.logged(1).verify('Information', { file: 'dir/file2.txt' });
        logger.verifyLoggedCount(2);
    });

    function setupGenerator(fileRelPath: string): [FileGenerator<FooOptions>, string] {
        const generator = mock<FileGenerator<FooOptions>>();
        const existingContents = randomUUID();
        const filePath = path.resolve('./out/path', fileRelPath);

        when(fileSystem.readFileTextIfExists(filePath)).thenResolve(existingContents);
        when(generator.getRelPath(options)).thenReturn(fileRelPath);
        when(generator.generateContents(options, existingContents)).thenReturn(`${fileRelPath}-generated`);

        return [generator, filePath];
    }

    it.each(['reading', 'writing'] as const)('should throw if file %s failed', async desc => {
        if (desc === 'reading') {
            when(fileSystem.readFileTextIfExists(anything())).thenReject(new Error('oups'));
        } else {
            when(fileSystem.writeFileText(anything(), anything())).thenReject(new Error('oups'));
        }

        const error = await expectToThrowAsync(act);

        expect(error.message).toIncludeMultiple([expectedFilePath1, 'oups']);
    });
});
