import { container as globalContainer, DependencyContainer } from 'tsyringe';

import { shouldResolvePublicApi } from '../../../test-utilities/mocks';
import { CONTRACT_PROVIDER_DI_TOKEN } from '../../indexer/src/public-api';
import { RPC_BLOCK_MONITOR_DI_TOKEN } from '../../utils/src/public-api';
import { registerDappetizerGenerator } from '../src/dependency-injection';
import { INDEXER_EMITTER_DI_TOKEN } from '../src/public-api';

describe('dependency injection', () => {
    let diContainer: DependencyContainer;

    beforeAll(() => {
        diContainer = globalContainer.createChildContainer();
        registerDappetizerGenerator(diContainer, 'https://tezos.node/');
    });

    shouldResolvePublicApi(() => diContainer, [
        INDEXER_EMITTER_DI_TOKEN,

        CONTRACT_PROVIDER_DI_TOKEN, // Should register Dappetizer Indexer too.
        RPC_BLOCK_MONITOR_DI_TOKEN, // Should register Dappetizer Utils too.
    ]);
});
