import { instance, mock } from 'ts-mockito';

import { verifyCalled } from '../../../../test-utilities/mocks';
import { DatabasePackageJson } from '../../../database/src/helpers/database-package-json';
import { IndexerPackageJson } from '../../../indexer/src/helpers/indexer-package-json';
import { npmModules } from '../../../utils/src/package-json/npm-modules';
import { GeneratorPackageJson } from '../../src/helpers/generator-package-json';
import { NpmDappetizerInstaller } from '../../src/helpers/npm-dappetizer-installer';
import { NpmDependenciesInstaller } from '../../src/helpers/npm-dependencies-installer';

describe(NpmDappetizerInstaller.name, () => {
    let target: NpmDappetizerInstaller;
    let npmDependenciesInstaller: NpmDependenciesInstaller;

    beforeEach(() => {
        npmDependenciesInstaller = mock();
        const indexerPackageJson: IndexerPackageJson = {
            version: '1.1.1',
            dependencies: {
                bignumber: '^1.2.2',
                taquito: '^1.3.3',
                taquitoMichelsonEncoder: '^1.4.4',
            },
        };
        const databasePackageJson: DatabasePackageJson = {
            dependencies: {
                typeorm: '^2.1.1',
            },
        };
        const generatorPackageJson: GeneratorPackageJson = {
            devDependencies: {
                typescript: '^3.1.1',
            },
        };
        target = new NpmDappetizerInstaller(instance(npmDependenciesInstaller), indexerPackageJson, databasePackageJson, generatorPackageJson);
    });

    it('should install dappetizer with dependencies', async () => {
        await target.installPackages('C:/out/dir');

        verifyCalled(npmDependenciesInstaller.install).onceWith(
            'C:/out/dir',
            [
                { package: npmModules.dappetizer.CLI, version: '1.1.1' },
                { package: npmModules.dappetizer.DATABASE, version: '1.1.1' },
                { package: npmModules.dappetizer.DECORATORS, version: '1.1.1' },
                { package: npmModules.dappetizer.INDEXER, version: '1.1.1' },
                { package: npmModules.dappetizer.UTILS, version: '1.1.1' },

                { package: npmModules.taquito.TAQUITO, version: '^1.3.3' },
                { package: npmModules.taquito.MICHELSON_ENCODER, version: '^1.4.4' },
                { package: npmModules.BIGNUMBER, version: '^1.2.2' },
                { package: npmModules.TYPEORM, version: '^2.1.1' },
                { package: 'sqlite3' }, // Peer dependency of typeorm.

                { package: 'rimraf', isDev: true },
                { package: npmModules.TYPESCRIPT, isDev: true, version: '^3.1.1' },
            ],
        );
    });
});
