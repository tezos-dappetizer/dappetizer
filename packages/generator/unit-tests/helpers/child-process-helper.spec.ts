import nodeChildProcess, { ExecOptions } from 'child_process';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { expectToThrowAsync } from '../../../../test-utilities/mocks';
import { ChildProcessHelper } from '../../src/helpers/child-process-helper';

describe(ChildProcessHelper.name, () => {
    let target: ChildProcessHelper;
    let childProcess: typeof nodeChildProcess;
    let options: ExecOptions;

    beforeEach(() => {
        childProcess = mock();
        target = new ChildProcessHelper(instance(childProcess));
        options = { cwd: 'cw dir' };
    });

    it('should pass if executed without error', async () => {
        setupExecAbstraction({ error: null });

        await target.exec('foo bar', options);

        verify(childProcess.exec('foo bar', options));
    });

    it('should include all output if failed', async () => {
        setupExecAbstraction({ error: new Error('oups') });

        const error = await expectToThrowAsync(async () => target.exec('foo bar', options));

        expect(error.message).toIncludeMultiple([
            `command: foo bar`,
            'Working directory: cw dir',
            'Console standard output: std out',
            'Console error output: std err',
            'Error: oups',
        ]);
        verify(childProcess.exec('foo bar', options));
    });

    function setupExecAbstraction(result: { error: Error | null }) {
        when(childProcess.exec(anything(), anything(), anything()))
            .thenCall((_c, _o, c) => c(result.error, 'std out', 'std err'));
    }
});
