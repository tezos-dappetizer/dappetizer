import path from 'path';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { TestLogger } from '../../../../test-utilities/mocks';
import { FileSystem } from '../../../utils/src/public-api';
import { generatorDefaults } from '../../src/helpers/generator-defaults';
import { IndexerModuleNameResolver } from '../../src/helpers/indexer-module-name-resolver';

describe(IndexerModuleNameResolver.name, () => {
    let target: IndexerModuleNameResolver;
    let fileSystem: FileSystem;
    let logger: TestLogger;

    const act = async (n: string | null = null) => target.resolve(n, 'C:/out/dir');

    beforeEach(() => {
        fileSystem = mock();
        logger = new TestLogger();
        target = new IndexerModuleNameResolver(instance(fileSystem), logger);
    });

    it('should use explicit name if specified', async () => {
        const name = await act('FooBarExplicit');

        expect(name).toBe('FooBarExplicit');
        verify(fileSystem.readFileTextIfExists(anything())).never();
        logger.loggedSingle().verify('Information', {
            name: 'FooBarExplicit',
            cliOption: '--indexer-module',
        });
    });

    it('should use package name if package.json exists', async () => {
        const expectedFilePath = setupPackageJson('{ "name":"foo-bar-package" }');

        const name = await act();

        expect(name).toBe('FooBarPackage');
        logger.loggedSingle().verify('Information', {
            name: 'FooBarPackage',
            cliOption: '--indexer-module',
            filePath: expectedFilePath,
        });
    });

    it.each([
        ['empty string', ''],
        ['white-space string', '  '],
        ['not well-formed JSON', 'wtf'],
        ['null JSON', 'null'],
        ['not JSON object', '123'],
        ['name which is not a string', '{ "name": 123 }'],
    ])('should warn if package.json contains %s', async (_desc, fileContents) => {
        const expectedFile = setupPackageJson(fileContents);

        const name = await act();

        expect(name).toBe(generatorDefaults.INDEXER_MODULE_NAME);
        logger.verifyLoggedCount(2);
        logger.logged(0).verify('Warning', {
            filePath: expectedFile,
            error: anything(),
        });
    });

    it.each([
        ['does not exist', null],
        ['contains empty JSON object', '{}'],
        ['contains name which is null', '{ "name": null }'],
    ])('should fall back to default name if package.json %s', async (_desc, fileContents) => {
        setupPackageJson(fileContents);

        const name = await act();

        expect(name).toBe(generatorDefaults.INDEXER_MODULE_NAME);
        logger.loggedSingle().verify('Information', {
            defaultName: generatorDefaults.INDEXER_MODULE_NAME,
            cliOption: '--indexer-module',
        });
    });

    function setupPackageJson(contents: string | null) {
        const expectedFile = path.resolve('C:/out/dir/package.json');
        when(fileSystem.readFileTextIfExists(expectedFile)).thenResolve(contents);
        return expectedFile;
    }
});
