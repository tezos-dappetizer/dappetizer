import path from 'path';
import { instance, mock, when } from 'ts-mockito';

import { npmModules, PackageJson, PackageJsonLoader } from '../../../utils/src/public-api';
import { GeneratorPackageJson } from '../../src/helpers/generator-package-json';

describe(GeneratorPackageJson.name, () => {
    it('should resolve values correctly', () => {
        const loader = mock<PackageJsonLoader>();
        const packageJson = mock<PackageJson>();
        when(loader.load(path.resolve('package.json'))).thenReturn(instance(packageJson));
        when(packageJson.getDevDependencyVersion(npmModules.TYPESCRIPT)).thenReturn('^2.2.2');

        const target = new GeneratorPackageJson(instance(loader));

        expect(target).toEqual<GeneratorPackageJson>({
            devDependencies: { typescript: '^2.2.2' },
        });
        expect(target.devDependencies).toBeFrozen();
    });
});
