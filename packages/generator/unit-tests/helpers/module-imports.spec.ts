import { describeMember } from '../../../../test-utilities/mocks';
import { ImportsBuilder } from '../../src/helpers/module-imports';

describe(ImportsBuilder.name, () => {
    let target: ImportsBuilder;

    beforeEach(() => {
        target = new ImportsBuilder();
    });

    describeMember<typeof target>('imports', () => {
        it('should be empty by default', () => {
            expect(target.imports).toBeEmpty();
        });

        it('should store added imports sorted', () => {
            target.add({ import: 'Ccc', from: 'module2' })
                .add({ import: 'Bbb', from: 'module1' })
                .add({ import: 'Yyy', from: './local2' })
                .add({ import: 'Aaa', from: 'module1' })
                .add({ import: 'Xxx', from: '../local1' })
                .add({ import: 'Aaa', from: 'module1' })
                .add({ import: 'Ddd', from: 'module1' });

            expect(target.imports).toEqual([
                { import: 'Xxx', from: '../local1' },
                { import: 'Yyy', from: './local2' },
                { import: 'Aaa', from: 'module1' },
                { import: 'Bbb', from: 'module1' },
                { import: 'Ddd', from: 'module1' },
                { import: 'Ccc', from: 'module2' },
            ]);
        });

        it('should store unique imports', () => {
            target.add({ import: 'Aaa', from: 'sameModule' })
                .add({ import: 'Aaa', from: 'sameModule' });

            expect(target.imports).toEqual([
                { import: 'Aaa', from: 'sameModule' },
            ]);
        });
    });

    describeMember<typeof target>('getFinalLines', () => {
        it('should generate imports sorted in sections', () => {
            target.add({ import: 'Ccc', from: 'module2' })
                .add({ import: 'Bbb', from: 'module1' })
                .add({ import: 'Yyy', from: './local2' })
                .add({ import: 'Aaa', from: 'module1' })
                .add({ import: 'Xxx', from: '../local1' })
                .add({ import: 'Aaa', from: 'module1' })
                .add({ import: 'Ddd', from: 'module1' });

            expect(target.getFinalLines()).toEqual([
                `import { Aaa, Bbb, Ddd } from 'module1';`,
                `import { Ccc } from 'module2';`,
                ``,
                `import { Xxx } from '../local1';`,
                `import { Yyy } from './local2';`,
                ``,
            ]);
        });

        it('should generate only one section as needed', () => {
            target.add({ import: 'Bbb', from: 'module1' })
                .add({ import: 'Aaa', from: 'module2' });

            expect(target.getFinalLines()).toEqual([
                `import { Bbb } from 'module1';`,
                `import { Aaa } from 'module2';`,
                ``,
            ]);
        });

        it('should split values to multiple lines if more than 3', () => {
            target.add({ import: 'Ccc', from: 'moduleX' })
                .add({ import: 'Bbb', from: 'moduleX' })
                .add({ import: 'Aaa', from: 'moduleX' })
                .add({ import: 'Ddd', from: 'moduleX' });

            expect(target.getFinalLines()).toEqual([
                `import {`,
                `    Aaa,`,
                `    Bbb,`,
                `    Ccc,`,
                `    Ddd,`,
                `} from 'moduleX';`,
                ``,
            ]);
        });

        it('should generate empty by default', () => {
            expect(target.imports).toBeEmpty();
            expect(target.getFinalLines()).toBeEmpty();
        });
    });
});
