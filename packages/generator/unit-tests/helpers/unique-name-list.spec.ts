import { UniqueNameList } from '../../src/helpers/unique-name-list';

describe(UniqueNameList.name, () => {
    let target: UniqueNameList;

    beforeEach(() => {
        target = new UniqueNameList();
    });

    it('should just return names if unique', () => {
        expect(target.addAndGetUniqueFor('foo')).toBe('foo');
        expect(target.addAndGetUniqueFor('bar')).toBe('bar');
    });

    it('should append counter if name already exists', () => {
        expect(target.addAndGetUniqueFor('foo')).toBe('foo');
        expect(target.addAndGetUniqueFor('foo')).toBe('foo2');
        expect(target.addAndGetUniqueFor('foo')).toBe('foo3');
    });
});
