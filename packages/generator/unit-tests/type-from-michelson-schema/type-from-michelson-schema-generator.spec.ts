import { anything, instance, mock, when } from 'ts-mockito';

import { expectToThrow } from '../../../../test-utilities/mocks';
import { MichelsonSchema } from '../../../indexer/src/michelson/michelson-schema';
import { GeneratorRouter } from '../../src/type-from-michelson-schema/generators/generator-router';
import {
    GeneratedType,
    TypeFromMichelsonSchemaGenerator,
} from '../../src/type-from-michelson-schema/type-from-michelson-schema-generator';

describe(TypeFromMichelsonSchemaGenerator.name, () => {
    let target: TypeFromMichelsonSchemaGenerator;
    let generatorRouter: GeneratorRouter;

    let schema: MichelsonSchema;

    const act = () => target.generate(schema, 'FooBar', 'mockedBigMapTyping' as any);

    beforeEach(() => {
        generatorRouter = mock();
        target = new TypeFromMichelsonSchemaGenerator(instance(generatorRouter));

        schema = {
            generated: 'mockedGenerated' as any,
            michelson: 'mockedMichelson' as any,
        } as MichelsonSchema;
    });

    it('should output code if inner type is fully generated', () => {
        when(generatorRouter.generate(anything(), anything())).thenReturn({
            typeDeclaration: 'FooBar',
            commentLines: ['ignored'],
            relatedCodeLines: ['mockedRelatedCode1', 'mockedRelatedCode2'],
            imports: 'mockedImports' as any,
        });

        const result = act();

        expect(result).toEqual<GeneratedType>({
            codeLines: ['mockedRelatedCode1', 'mockedRelatedCode2'],
            imports: 'mockedImports' as any,
        });
    });

    it('should generate export if inner type is partially generated', () => {
        when(generatorRouter.generate(anything(), anything())).thenReturn({
            typeDeclaration: 'string[]',
            commentLines: ['Tezos array.'],
            relatedCodeLines: ['mockedRelatedCode1', 'mockedRelatedCode2'],
            imports: 'mockedImports' as any,
        });

        const result = act();

        expect(result).toEqual<GeneratedType>({
            codeLines: [
                '/** Tezos array. */',
                'export type FooBar = string[];',
                '',
                'mockedRelatedCode1',
                'mockedRelatedCode2',
            ],
            imports: 'mockedImports' as any,
        });
    });

    it('should include schema michelson if failed', () => {
        when(generatorRouter.generate(anything(), anything())).thenThrow(new Error('oups'));

        const error = expectToThrow(act);

        expect(error.message).toIncludeMultiple(['oups', '"mockedMichelson"']);
    });
});
