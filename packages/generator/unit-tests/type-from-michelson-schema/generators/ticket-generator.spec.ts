import { BigNumber } from 'bignumber.js';
import { anything, instance, mock, when } from 'ts-mockito';

import { verifyCalled } from '../../../../../test-utilities/mocks';
import { michelsonDataPrims, MichelsonSchema, michelsonTypePrims } from '../../../../indexer/src/public-api';
import { InterfaceGenerator } from '../../../src/type-from-michelson-schema/generators/interface-generator';
import { TicketGenerator, TicketSchema } from '../../../src/type-from-michelson-schema/generators/ticket-generator';

describe(TicketGenerator.name, () => {
    let target: TicketGenerator;
    let interfaceMapper: InterfaceGenerator;
    let schema: MichelsonSchema;

    beforeEach(() => {
        interfaceMapper = mock();
        target = new TicketGenerator(instance(interfaceMapper));

        schema = new MichelsonSchema({
            prim: michelsonTypePrims.TICKET,
            args: [{ prim: michelsonTypePrims.STRING }],
        });
    });

    it('should generate correctly', () => {
        when(interfaceMapper.generate(anything(), anything())).thenReturn('mockedType' as any);

        const result = target.generate(schema.generated as any, {
            typeNameToGenerate: 'FooBar',
            bigMapTyping: 'mockedBigMapTyping' as any,
        });

        expect(result).toBe('mockedType');
        verifyCalled(interfaceMapper.generate).onceWith(
            {
                __michelsonType: 'pair',
                schema: {
                    amount: { __michelsonType: 'nat', schema: 'nat' },
                    ticketer: {
                        __michelsonType: 'contract',
                        schema: { parameter: { __michelsonType: 'string', schema: 'string' } },
                    },
                    value: { __michelsonType: 'string', schema: 'string' },
                },
            },
            {
                typeNameToGenerate: 'FooBar',
                comment: 'Ticket.',
                bigMapTyping: 'mockedBigMapTyping' as any,
            },
        );
    });

    it('should use same generated schema as Taquito', () => {
        expect(schema.generated).toEqual<TicketSchema>({
            __michelsonType: 'ticket',
            schema: {
                value: { __michelsonType: 'string', schema: 'string' },
                ticketer: { __michelsonType: 'contract', schema: 'contract' },
                amount: { __michelsonType: 'int', schema: 'int' },
            },
        });
    });

    it('should match value deserialized by Taquito', () => {
        const value = schema.execute({
            prim: michelsonDataPrims.PAIR,
            args: [
                { string: 'KT1PVuv7af4VkPsZVZ8oZz9GSSdGnGBCbFWw' },
                { string: 'abc' },
                { int: '123' },
            ],
        });

        expect(value).toEqual({
            value: 'abc',
            ticketer: 'KT1PVuv7af4VkPsZVZ8oZz9GSSdGnGBCbFWw',
            amount: new BigNumber(123),
        });
    });
});
