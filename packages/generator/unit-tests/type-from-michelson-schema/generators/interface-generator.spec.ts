import { anything, instance, mock, when } from 'ts-mockito';

import { verifyCalled } from '../../../../../test-utilities/mocks';
import { Michelson, michelsonDataPrims, MichelsonSchema, michelsonTypePrims } from '../../../../indexer/src/public-api';
import { GeneratedTypeParts, GeneratorRouter } from '../../../src/type-from-michelson-schema/generators/generator-router';
import { InterfaceGenerator, InterfaceSchema } from '../../../src/type-from-michelson-schema/generators/interface-generator';

describe(InterfaceGenerator.name, () => {
    let target: InterfaceGenerator;
    let generatorRouter: GeneratorRouter;

    const act = (s: MichelsonSchema, c?: string) => target.generate(s.generated as any, {
        typeNameToGenerate: 'FooBar',
        bigMapTyping: 'mockedBigMapTyping' as any,
        comment: c,
    });

    beforeEach(() => {
        generatorRouter = mock();
        target = new InterfaceGenerator(instance(generatorRouter));

        when(generatorRouter.generate(anything(), anything())).thenReturn({
            typeDeclaration: 'MockedType',
            commentLines: [],
            imports: [],
            relatedCodeLines: [],
        });
    });

    it('should map pair schema correctly', () => {
        const schema = getSchema(michelsonTypePrims.PAIR);
        when(generatorRouter.generate(anything(), anything()))
            .thenReturn({
                typeDeclaration: 'MockedStr',
                commentLines: ['Mocked str comment.'],
                imports: ['mockedStrImport' as any],
                relatedCodeLines: ['mockedStrRelatedCodeLine1', 'mockedStrRelatedCodeLine2'],
            })
            .thenReturn({
                typeDeclaration: 'MockedAddr',
                commentLines: ['Mocked addr comment.'],
                imports: ['mockedAddrImport1' as any, 'mockedAddrImport2' as any],
                relatedCodeLines: ['mockedAddrRelatedCodeLines'],
            });

        const result = act(schema, 'Foo bar comment.');

        expect(result).toEqual<GeneratedTypeParts>({
            typeDeclaration: 'FooBar',
            commentLines: ['Foo bar comment.'],
            imports: [
                'mockedStrImport' as any,
                'mockedAddrImport1' as any,
                'mockedAddrImport2' as any,
            ],
            relatedCodeLines: [
                `/** Foo bar comment. */`,
                `export interface FooBar {`,
                `    /** Mocked str comment. */`,
                `    aaa: MockedStr;`,
                ``,
                `    /** Mocked addr comment. */`,
                `    bbb: MockedAddr;`,
                `}`,
                ``,
                `mockedStrRelatedCodeLine1`,
                `mockedStrRelatedCodeLine2`,
                `mockedAddrRelatedCodeLines`,
            ],
        });
        verifyCalled(generatorRouter.generate)
            .with(
                { __michelsonType: 'string', schema: 'string' },
                { typeNameToGenerate: 'FooBarAaa', bigMapTyping: 'mockedBigMapTyping' as any },
            )
            .thenWith(
                { __michelsonType: 'address', schema: 'address' },
                { typeNameToGenerate: 'FooBarBbb', bigMapTyping: 'mockedBigMapTyping' as any },
            )
            .thenNoMore();
    });

    it('should map or schema to optional properties', () => {
        const schema = getSchema(michelsonTypePrims.OR);

        const result = act(schema);

        expect(result.relatedCodeLines).toEqual([
            `export interface FooBar {`,
            `    aaa?: MockedType;`,
            ``,
            `    bbb?: MockedType;`,
            `}`,
            ``,
        ]);
    });

    it('should quote property names correctly', () => {
        const schema = new MichelsonSchema({
            prim: michelsonTypePrims.PAIR,
            args: [
                { prim: michelsonTypePrims.STRING, annots: ['%2'] },
                { prim: michelsonTypePrims.STRING, annots: [':3wtf'] },
                { prim: michelsonTypePrims.STRING, annots: ['%omg lol'] },
            ],
        });

        const result = act(schema);

        expect(result.relatedCodeLines).toEqual([
            `export interface FooBar {`,
            `    '2': MockedType;`,
            ``,
            `    '3wtf': MockedType;`,
            ``,
            `    'omg lol': MockedType;`,
            `}`,
            ``,
        ]);
    });

    it.each([
        michelsonTypePrims.PAIR,
        michelsonTypePrims.OR,
    ] as const)(`should use same generated '%s' schema as Taquito`, prim => {
        const schema = getSchema(prim);

        expect(schema.generated).toEqual<InterfaceSchema>({
            __michelsonType: prim,
            schema: {
                ['bbb' as string]: { __michelsonType: 'address', schema: 'address' },
                ['aaa' as string]: { __michelsonType: 'string', schema: 'string' },
            },
        });
    });

    it.each<[string, MichelsonSchema, Michelson, object]>([
        [
            'pair',
            getSchema(michelsonTypePrims.PAIR),
            { prim: michelsonDataPrims.PAIR, args: [{ string: 'bVal' }, { string: 'aVal' }] },
            { aaa: 'aVal', bbb: 'bVal' },
        ],
        [
            'Left or',
            getSchema(michelsonTypePrims.OR),
            { prim: michelsonDataPrims.LEFT, args: [{ string: 'bVal' }] },
            { bbb: 'bVal' },
        ],
        [
            'Right or',
            getSchema(michelsonTypePrims.OR),
            { prim: michelsonDataPrims.RIGHT, args: [{ string: 'aVal' }] },
            { aaa: 'aVal' },
        ],
    ])(`should match '%s' deserialized by Taquito`, (_desc, schema, michelsonValue, expectedValue) => {
        const value = schema.execute(michelsonValue);

        expect(value).toEqual(expectedValue);
    });

    function getSchema(prim: typeof michelsonTypePrims.PAIR | typeof michelsonTypePrims.OR) {
        return new MichelsonSchema({
            prim,
            args: [
                { prim: michelsonTypePrims.ADDRESS, annots: ['%bbb'] },
                { prim: michelsonTypePrims.STRING, annots: ['%aaa'] },
            ],
        });
    }
});
