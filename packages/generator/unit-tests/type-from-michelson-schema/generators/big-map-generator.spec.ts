import { anything, instance, mock, when } from 'ts-mockito';

import { expectToThrow, verifyCalled } from '../../../../../test-utilities/mocks';
import { npmModules } from '../../../../utils/src/public-api';
import { BigMapTyping } from '../../../src/type-from-michelson-schema/big-map-typing';
import { BigMapGenerator, BigMapSchema } from '../../../src/type-from-michelson-schema/generators/big-map-generator';
import { GeneratedTypeParts } from '../../../src/type-from-michelson-schema/generators/generator-router';
import { MapGenerator } from '../../../src/type-from-michelson-schema/generators/map-generator';
import { MapGeneratorHelper } from '../../../src/type-from-michelson-schema/generators/map-generator-helper';
import { libTypes, tsTypes } from '../../../src/type-from-michelson-schema/simple-types';

describe(BigMapGenerator.name, () => {
    let target: BigMapGenerator;
    let mapGenerator: MapGenerator;
    let helper: MapGeneratorHelper;
    let schema: BigMapSchema;

    const act = (t: BigMapTyping) => target.generate(schema, {
        typeNameToGenerate: 'FooBar',
        bigMapTyping: t,
    });

    beforeEach(() => {
        [mapGenerator, helper] = [mock(), mock()];
        target = new BigMapGenerator(instance(mapGenerator), instance(helper));

        schema = 'mockedSchema' as any;
    });

    it(`should generate regular map if ${BigMapTyping.InitialValues}`, () => {
        when(mapGenerator.generate(anything(), anything())).thenReturn('MockedType' as any);

        const result = act(BigMapTyping.InitialValues);

        expect(result).toBe('MockedType');
        verifyCalled(mapGenerator.generate).onceWith(schema, {
            typeNameToGenerate: 'FooBar',
            comment: 'Big map initial values.',
        });
    });

    it(`should generate string if ${BigMapTyping.RawID}`, () => {
        const result = act(BigMapTyping.RawID);

        expect(result).toEqual<GeneratedTypeParts>({
            typeDeclaration: tsTypes.STRING,
            commentLines: ['Big map ID - string with arbitrary big integer, negative if temporary.'],
            imports: [],
            relatedCodeLines: [],
        });
    });

    it(`should generate string if ${BigMapTyping.TaquitoAbstraction}`, () => {
        when(helper.generate(anything(), anything())).thenReturn({
            genericsSuffix: 'ignored',
            commentLines: 'mockedComments' as any,
            imports: ['mockedImport1' as any, 'mockedImport2' as any],
            relatedCodeLines: 'mockedCodeLines' as any,
        });

        const result = act(BigMapTyping.TaquitoAbstraction);

        expect(result).toMatchObject<GeneratedTypeParts>({
            typeDeclaration: libTypes.BIG_MAP_ABSTRACTION,
            commentLines: 'mockedComments' as any,
            imports: [
                'mockedImport1' as any,
                'mockedImport2' as any,
                { import: libTypes.BIG_MAP_ABSTRACTION, from: npmModules.taquito.TAQUITO },
            ],
            relatedCodeLines: 'mockedCodeLines' as any,
        });
        verifyCalled(helper.generate).onceWith(schema, {
            typeNameToGenerate: 'FooBar',
            comment: 'Big map.',
        });
    });

    it(`should throw if ${BigMapTyping.Unexpected}`, () => {
        const error = expectToThrow(() => act(BigMapTyping.Unexpected));

        expect(error.message).toContain('"mockedSchema"');
    });
});
