import * as taquitoMichelson from '@taquito/michelson-encoder';
import { BigNumber } from 'bignumber.js';

import { expectToThrow } from '../../../../../test-utilities/mocks';
import { Michelson, michelsonDataPrims, MichelsonSchema, michelsonTypePrims } from '../../../../indexer/src/public-api';
import { npmModules } from '../../../../utils/src/public-api';
import { ModuleImport } from '../../../src/helpers/module-imports';
import { GeneratedTypeParts } from '../../../src/type-from-michelson-schema/generators/generator-router';
import { PrimitiveGenerator, PrimitiveSchema } from '../../../src/type-from-michelson-schema/generators/primitive-generator';
import { libTypes, tsTypes } from '../../../src/type-from-michelson-schema/simple-types';

describe(PrimitiveGenerator.name, () => {
    const target = new PrimitiveGenerator();

    const untestedMichelsonTypes: Record<PrimitiveSchema['__michelsonType'], ''> = {
        address: '', bls12_381_fr: '', bls12_381_g1: '', bls12_381_g2: '', bool: '', bytes: '', chain_id: '', chest: '', chest_key: '',
        contract: '', int: '', key: '', key_hash: '', lambda: '', mutez: '', nat: '', never: '', operation: '', sapling_state: '',
        sapling_transaction: '', sapling_transaction_deprecated: '', signature: '', string: '', timestamp: '', unit: '',
    };

    describeMappedToString('address', 'Tezos address.');

    describeMappedToBytesString('bls12_381_fr', 'Bls12_381_fr bytes.');

    describeMappedToBytesString('bls12_381_g1', 'Bls12_381_g1 bytes.');

    describeMappedToBytesString('bls12_381_g2', 'Bls12_381_g2 bytes.');

    describeType('bool', () => {
        const schema = new MichelsonSchema({ prim: michelsonTypePrims.BOOL });

        shouldBeMappedTo(schema, {
            expectedType: tsTypes.BOOLEAN,
            expectedComment: 'Simple boolean.',
        });

        shouldUseSchemaAsTaquito(schema, {
            __michelsonType: 'bool',
            schema: 'bool',
        });

        it.each([
            [true, michelsonDataPrims.TRUE],
            [false, michelsonDataPrims.FALSE],
        ])('should match %s deserialized by Taquito', (expectedValue, prim) => {
            const value = schema.execute({ prim });

            expect(value).toEqual(expectedValue);
        });
    });

    describeMappedToBytesString('bytes', 'Bytes.');

    describeMappedToString('chain_id', 'Chain ID.');

    describeMappedToBytesString('chest', 'Chest.');

    describeMappedToBytesString('chest_key', 'Chest key.');

    describe('constant', () => {
        const schema = new MichelsonSchema({
            prim: michelsonTypePrims.CONSTANT,
            args: [{ string: 'haha' }],
        });

        it('should always throw when generating', () => {
            expectToThrow(() => target.generateConstant());
        });

        shouldUseSchemaAsTaquito(schema, {
            __michelsonType: 'constant',
            schema: { hash: 'haha' },
        } as any);

        it('should match error when deserialized by Taquito', () => {
            const error = expectToThrow(() => schema.execute({ string: 'ignored' }));

            expect(error.message).toInclude('an expanded script');
        });
    });

    describeType('contract', () => {
        const schema = new MichelsonSchema({
            prim: michelsonTypePrims.CONTRACT,
            args: [{ prim: michelsonTypePrims.STRING }],
        });

        shouldBeMappedTo(schema, {
            expectedType: tsTypes.STRING,
            expectedComment: 'Contract address.',
        });

        shouldUseSchemaAsTaquito(schema, {
            __michelsonType: 'contract',
            schema: { parameter: { __michelsonType: 'string', schema: 'string' } },
        });

        shouldMatchDeserializationByTaquito(schema, {
            valueMichelson: { string: 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5' },
            expectedValue: 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5',
        });
    });

    describeMappedToBigNumber('int', 'Integer from -2^31-1 to 2^31.');

    describeMappedToString('key', 'Key.');

    describeMappedToString('key_hash', 'Key hash.');

    describeType('lambda', () => {
        const schema = new MichelsonSchema({
            prim: michelsonTypePrims.LAMBDA,
            args: [
                { prim: michelsonTypePrims.STRING },
                { prim: michelsonTypePrims.NAT },
            ],
        });

        shouldBeMappedTo(schema, {
            expectedType: tsTypes.UNKNOWN,
            expectedComment: 'Lambda.',
        });

        shouldUseSchemaAsTaquito(schema, {
            __michelsonType: 'lambda',
            schema: {
                parameters: { __michelsonType: 'string', schema: 'string' },
                returns: { __michelsonType: 'nat', schema: 'nat' },
            },
        });
    });

    describeMappedToBigNumber('mutez', 'Mutez - arbitrary big integer >= 0.');

    describeMappedToBigNumber('nat', 'Nat - arbitrary big integer >= 0.');

    describeType('never', () => {
        const schema = new MichelsonSchema({ prim: michelsonTypePrims.NEVER });

        shouldBeMappedTo(schema, {
            expectedType: tsTypes.NEVER,
            expectedComment: 'Never.',
        });

        shouldUseSchemaAsTaquito(schema, {
            __michelsonType: 'never',
            schema: 'never',
        });

        it('should match error when deserialized by Taquito', () => {
            const error = expectToThrow(() => schema.execute({ string: 'ignored' }));

            expect(error.message).toInclude('There is no literal value for the type never.');
        });
    });

    describeType('operation', () => {
        const schema = new MichelsonSchema({ prim: michelsonTypePrims.OPERATION });

        shouldBeMappedTo(schema, {
            expectedType: tsTypes.UNKNOWN,
            expectedComment: 'Operation.',
        });
    });

    describeType('sapling_state', () => {
        const schema = new MichelsonSchema({
            prim: michelsonTypePrims.SAPLING_STATE,
            args: [{ int: '8' }],
        });

        shouldBeMappedTo(schema, {
            expectedType: tsTypes.STRING,
            expectedComment: 'Sapling state.',
        });

        shouldUseSchemaAsTaquito(schema, {
            __michelsonType: 'sapling_state',
            schema: { memoSize: '8' },
        });

        shouldMatchDeserializationByTaquito(schema, {
            valueMichelson: { int: 'abc' },
            expectedValue: 'abc',
        });
    });

    describeType('sapling_transaction', () => {
        const schema = new MichelsonSchema({
            prim: michelsonTypePrims.SAPLING_TRANSACTION,
            args: [{ prim: michelsonTypePrims.STRING }],
        });

        shouldBeMappedTo(schema, {
            expectedType: tsTypes.UNKNOWN,
            expectedComment: 'Sapling transaction.',
        });
    });

    describeType('sapling_transaction_deprecated', () => {
        const schema = new MichelsonSchema({
            prim: michelsonTypePrims.SAPLING_TRANSACTION_DEPRECATED,
            args: [{ prim: michelsonTypePrims.STRING }],
        });

        shouldBeMappedTo(schema, {
            expectedType: tsTypes.UNKNOWN,
            expectedComment: 'Deprecated sapling transaction.',
        });
    });

    describeMappedToString('signature', 'Signature.');

    describeMappedToString('string', 'Arbitrary string.');

    describeType('timestamp', () => {
        const schema = new MichelsonSchema({ prim: michelsonTypePrims.TIMESTAMP });

        shouldBeMappedTo(schema, {
            expectedType: tsTypes.STRING,
            expectedComment: 'Date ISO 8601 string.',
        });

        shouldUseSchemaAsTaquito(schema, {
            __michelsonType: 'timestamp',
            schema: 'timestamp',
        });

        shouldMatchDeserializationByTaquito(schema, {
            valueMichelson: { string: '2022-08-17T10:24:44Z' },
            expectedValue: '2022-08-17T10:24:44.000Z',
        });
    });

    describeType('unit', () => {
        const schema = new MichelsonSchema({ prim: michelsonTypePrims.UNIT });

        shouldBeMappedTo(schema, {
            expectedType: 'typeof UnitValue',
            expectedComment: 'An empty result.',
            expectedImports: [{ import: libTypes.UNIT_VALUE, from: npmModules.taquito.MICHELSON_ENCODER }],
        });

        shouldUseSchemaAsTaquito(schema, {
            __michelsonType: 'unit',
            schema: 'unit',
        });

        shouldMatchDeserializationByTaquito(schema, {
            valueMichelson: { prim: michelsonDataPrims.UNIT },
            expectedValue: taquitoMichelson.UnitValue,
        });
    });

    it('should test all primitive michelson types', () => {
        expect(untestedMichelsonTypes).toBeEmpty();
    });

    function describeMappedToString(michelsonType: PrimitiveSchema['__michelsonType'], expectedComment: string) {
        describeType(michelsonType, () => {
            const schema = new MichelsonSchema({ prim: michelsonType });

            shouldBeMappedTo(schema, { expectedType: tsTypes.STRING, expectedComment });

            shouldUseSchemaAsTaquito(schema, {
                __michelsonType: michelsonType as any,
                schema: michelsonType,
            });

            shouldMatchDeserializationByTaquito(schema, {
                valueMichelson: { string: 'abc' },
                expectedValue: 'abc',
            });
        });
    }

    function describeMappedToBytesString(michelsonType: PrimitiveSchema['__michelsonType'], expectedComment: string) {
        describeType(michelsonType, () => {
            const schema = new MichelsonSchema({ prim: michelsonType });

            shouldBeMappedTo(schema, { expectedType: tsTypes.STRING, expectedComment });

            shouldUseSchemaAsTaquito(schema, {
                __michelsonType: michelsonType as any,
                schema: michelsonType,
            });

            shouldMatchDeserializationByTaquito(schema, {
                valueMichelson: { bytes: 'bbb' },
                expectedValue: 'bbb',
            });
        });
    }

    function describeMappedToBigNumber(michelsonType: PrimitiveSchema['__michelsonType'], expectedComment: string) {
        describeType(michelsonType, () => {
            const schema = new MichelsonSchema({ prim: michelsonType });

            shouldBeMappedTo(schema, {
                expectedType: libTypes.BIG_NUMBER,
                expectedComment,
                expectedImports: [{ import: libTypes.BIG_NUMBER, from: npmModules.BIGNUMBER }],
            });

            shouldUseSchemaAsTaquito(schema, {
                __michelsonType: michelsonType as any,
                schema: michelsonType,
            });

            shouldMatchDeserializationByTaquito(schema, {
                valueMichelson: { int: '123' },
                expectedValue: new BigNumber(123),
            });
        });
    }

    function describeType(michelsonType: PrimitiveSchema['__michelsonType'], runTests: () => void) {
        delete untestedMichelsonTypes[michelsonType];
        describe(michelsonType, () => void runTests());
    }

    function shouldBeMappedTo(schema: MichelsonSchema, testCase: {
        expectedType: string;
        expectedComment: string;
        expectedImports?: ModuleImport[];
    }) {
        it(`should be mapped to ${testCase.expectedType}`, () => {
            const mapped = target.generate(schema.generated as any);

            expect(mapped).toEqual<GeneratedTypeParts>({
                typeDeclaration: testCase.expectedType,
                commentLines: [testCase.expectedComment],
                imports: testCase.expectedImports ?? [],
                relatedCodeLines: [],
            });
        });
    }

    function shouldUseSchemaAsTaquito(schema: MichelsonSchema, expectedSchema: PrimitiveSchema) {
        it('should use same generated schema as Taquito', () => {
            expect(schema.generated).toEqual(expectedSchema);
        });
    }

    function shouldMatchDeserializationByTaquito(schema: MichelsonSchema, testCase: {
        valueMichelson: Michelson;
        expectedValue: unknown;
    }) {
        it('should match value deserialized by Taquito', () => {
            const value = schema.execute(testCase.valueMichelson);

            expect(value).toEqual(testCase.expectedValue);
        });
    }
});
