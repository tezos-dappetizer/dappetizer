import { anything, instance, mock, when } from 'ts-mockito';

import { verifyCalled } from '../../../../../test-utilities/mocks';
import { MichelsonSchema, michelsonTypePrims } from '../../../../indexer/src/public-api';
import { ArrayGenerator, ArraySchema } from '../../../src/type-from-michelson-schema/generators/array-generator';
import { GeneratedTypeParts, GeneratorRouter } from '../../../src/type-from-michelson-schema/generators/generator-router';

describe(ArrayGenerator.name, () => {
    describe.each([michelsonTypePrims.LIST, michelsonTypePrims.SET])('%s', prim => {
        let target: ArrayGenerator;
        let generatorRouter: GeneratorRouter;
        let schema: MichelsonSchema;

        beforeEach(() => {
            generatorRouter = mock();
            target = new ArrayGenerator(instance(generatorRouter));

            schema = new MichelsonSchema({
                prim,
                args: [{ prim: michelsonTypePrims.ADDRESS }],
            });
        });

        it.each([
            ['no', [], []],
            ['single-line', ['mockedComment'], ['Array of: mockedComment']],
            ['multi-line', ['mockedComment1', 'mockedComment2'], ['Array of: mockedComment1', 'mockedComment2']],
        ])('should generate correctly with %s comment', (_desc, inputComments, expectedComments) => {
            when(generatorRouter.generate(anything(), anything())).thenReturn({
                typeDeclaration: 'MockedType',
                commentLines: inputComments,
                imports: 'mockedImports' as any,
                relatedCodeLines: 'mockedRelatedCodeLines' as any,
            });

            const result = target.generate(schema.generated as any, {
                typeNameToGenerate: 'FooBar',
                bigMapTyping: 'mockedBigMapTyping' as any,
            });

            expect(result).toEqual<GeneratedTypeParts>({
                typeDeclaration: 'MockedType[]',
                commentLines: expectedComments,
                imports: 'mockedImports' as any,
                relatedCodeLines: 'mockedRelatedCodeLines' as any,
            });
            verifyCalled(generatorRouter.generate).onceWith(
                { __michelsonType: 'address', schema: 'address' },
                { typeNameToGenerate: 'FooBarItem', bigMapTyping: 'mockedBigMapTyping' as any },
            );
        });

        it('should use same generated schema as Taquito', () => {
            expect(schema.generated).toEqual<ArraySchema>({
                __michelsonType: prim as any,
                schema: { __michelsonType: 'address', schema: 'address' },
            });
        });

        it('should match value deserialized by Taquito', () => {
            const value = schema.execute([{ string: 'aaa' }, { string: 'bbb' }]);

            expect(value).toEqual(['aaa', 'bbb']);
        });
    });
});
