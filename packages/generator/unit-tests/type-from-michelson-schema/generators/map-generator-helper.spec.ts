import * as taquitoMichelson from '@taquito/michelson-encoder';
import { BigNumber } from 'bignumber.js';
import { anything, instance, mock, when } from 'ts-mockito';

import { verifyCalled } from '../../../../../test-utilities/mocks';
import { michelsonDataPrims, MichelsonSchema, michelsonTypePrims } from '../../../../indexer/src/public-api';
import { BigMapTyping } from '../../../src/type-from-michelson-schema/big-map-typing';
import { GeneratorRouter } from '../../../src/type-from-michelson-schema/generators/generator-router';
import {
    GeneratedMapParts,
    MapGeneratorHelper,
    MapSchema,
} from '../../../src/type-from-michelson-schema/generators/map-generator-helper';

describe(MapGeneratorHelper.name, () => {
    describe.each([michelsonTypePrims.BIG_MAP, michelsonTypePrims.MAP])('%s', prim => {
        let target: MapGeneratorHelper;
        let generatorRouter: GeneratorRouter;
        let schema: MichelsonSchema;

        beforeEach(() => {
            generatorRouter = mock();
            target = new MapGeneratorHelper(instance(generatorRouter));

            schema = new MichelsonSchema({
                prim,
                args: [
                    { prim: michelsonTypePrims.ADDRESS },
                    { prim: michelsonTypePrims.NAT },
                ],
            });
        });

        it('should generate correctly', () => {
            when(generatorRouter.generate(anything(), anything()))
                .thenReturn({
                    typeDeclaration: 'MockedKey',
                    commentLines: ['mockedKeyComment'],
                    imports: ['mockedKeyImport1' as any, 'mockedKeyImport2' as any],
                    relatedCodeLines: ['mockedKeyCodeLine'],
                })
                .thenReturn({
                    typeDeclaration: 'MockedValue',
                    commentLines: ['mockedValueComment1', 'mockedValueComment2'],
                    imports: ['mockedValueImport' as any],
                    relatedCodeLines: ['mockedValueCodeLine1', 'mockedValueCodeLine2'],
                });

            const result = target.generate(schema.generated as any, {
                typeNameToGenerate: 'FooBar',
                comment: 'Google maps.',
            });

            expect(result).toEqual<GeneratedMapParts>({
                genericsSuffix: '<MockedKey, MockedValue>',
                commentLines: [
                    'Google maps.',
                    '',
                    'Key of `MockedKey`: mockedKeyComment',
                    '',
                    'Value of `MockedValue`:',
                    'mockedValueComment1',
                    'mockedValueComment2',
                ],
                imports: ['mockedKeyImport1' as any, 'mockedKeyImport2' as any, 'mockedValueImport' as any],
                relatedCodeLines: ['mockedKeyCodeLine', 'mockedValueCodeLine1', 'mockedValueCodeLine2'],
            });
            verifyCalled(generatorRouter.generate)
                .with(
                    { __michelsonType: 'address', schema: 'address' },
                    { typeNameToGenerate: 'FooBarKey', bigMapTyping: BigMapTyping.Unexpected },
                )
                .thenWith(
                    { __michelsonType: 'nat', schema: 'nat' },
                    { typeNameToGenerate: 'FooBarValue', bigMapTyping: BigMapTyping.Unexpected },
                )
                .thenNoMore();
        });

        it('should generate key/value comment even if empty', () => {
            when(generatorRouter.generate(anything(), anything()))
                .thenReturn({ typeDeclaration: 'MockedType', commentLines: [], imports: [], relatedCodeLines: [] });

            const result = target.generate(schema.generated as any, { typeNameToGenerate: 'Foo', comment: 'Map.' });

            expect(result.commentLines).toEqual([
                'Map.',
                '',
                'Key of `MockedType`.',
                '',
                'Value of `MockedType`.',
            ]);
        });

        it('should use same generated schema as Taquito', () => {
            expect(schema.generated).toEqual<MapSchema>({
                __michelsonType: prim as any,
                schema: {
                    key: { __michelsonType: 'address', schema: 'address' },
                    value: { __michelsonType: 'nat', schema: 'nat' },
                },
            });
        });

        it('should match value deserialized by Taquito', () => {
            const value = schema.execute<taquitoMichelson.MichelsonMap<string, BigNumber>>([
                { prim: michelsonDataPrims.ELT, args: [{ string: 'tz1XK9UXz2198eqYNB8sqH1gm7XRR4zMDaHR' }, { int: '111' }] },
                { prim: michelsonDataPrims.ELT, args: [{ string: 'tz1TL5mbC8x3T2nQLJrMTngrKmPn6dGmcPe3' }, { int: '222' }] },
            ]);

            expect(value).toBeInstanceOf(taquitoMichelson.MichelsonMap);
            expect(Array.from(value.entries())).toEqual([
                ['tz1XK9UXz2198eqYNB8sqH1gm7XRR4zMDaHR', new BigNumber(111)],
                ['tz1TL5mbC8x3T2nQLJrMTngrKmPn6dGmcPe3', new BigNumber(222)],
            ]);
        });
    });
});
