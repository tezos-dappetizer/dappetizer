/* eslint-disable no-underscore-dangle */
import * as taquitoMichelson from '@taquito/michelson-encoder';
import { instance, mock, objectContaining, when } from 'ts-mockito';

import { typed } from '../../../../utils/src/public-api';
import { ArrayGenerator } from '../../../src/type-from-michelson-schema/generators/array-generator';
import { BigMapGenerator } from '../../../src/type-from-michelson-schema/generators/big-map-generator';
import { GenerateOptions, GeneratorRouter } from '../../../src/type-from-michelson-schema/generators/generator-router';
import { GeneratorRouterImpl } from '../../../src/type-from-michelson-schema/generators/generator-router-impl';
import { InterfaceGenerator } from '../../../src/type-from-michelson-schema/generators/interface-generator';
import { MapGenerator } from '../../../src/type-from-michelson-schema/generators/map-generator';
import { MapGenerateOptions } from '../../../src/type-from-michelson-schema/generators/map-generator-helper';
import { NullableGenerator } from '../../../src/type-from-michelson-schema/generators/nullable-generator';
import { PrimitiveGenerator } from '../../../src/type-from-michelson-schema/generators/primitive-generator';
import { TicketGenerator } from '../../../src/type-from-michelson-schema/generators/ticket-generator';

describe(GeneratorRouterImpl.name, () => {
    let target: GeneratorRouter;
    let arrayGenerator: ArrayGenerator;
    let bigMapGenerator: BigMapGenerator;
    let interfaceGenerator: InterfaceGenerator;
    let mapGenerator: MapGenerator;
    let nullableGenerator: NullableGenerator;
    let primitiveGenerator: PrimitiveGenerator;
    let ticketGenerator: TicketGenerator;

    beforeEach(() => {
        [arrayGenerator, bigMapGenerator, interfaceGenerator, mapGenerator, nullableGenerator, primitiveGenerator, ticketGenerator]
            = [mock(), mock(), mock(), mock(), mock(), mock(), mock()];
        target = new GeneratorRouterImpl(
            instance(arrayGenerator),
            instance(bigMapGenerator),
            instance(interfaceGenerator),
            instance(mapGenerator),
            instance(nullableGenerator),
            instance(primitiveGenerator),
            instance(ticketGenerator),
        );
    });

    // Record used to cover testing of all types.
    type MichelsonType = taquitoMichelson.TokenSchema['__michelsonType'];
    const michelsonTypes: Record<MichelsonType, ''> = {
        address: '', big_map: '', bls12_381_fr: '', bls12_381_g1: '', bls12_381_g2: '', bool: '', bytes: '', chain_id: '', chest: '', chest_key: '',
        constant: '', contract: '', int: '', key: '', key_hash: '', lambda: '', list: '', map: '', mutez: '', nat: '', never: '', operation: '',
        option: '', or: '', pair: '', sapling_state: '', sapling_transaction: '', sapling_transaction_deprecated: '', set: '', signature: '',
        string: '', ticket: '', ticket_deprecated: '', timestamp: '', unit: '',
    };

    shouldBeDelegatedTo('big_map', (schema, options) => bigMapGenerator.generate(schema, options));

    shouldBeDelegatedTo('constant', () => primitiveGenerator.generateConstant());

    shouldBeDelegatedTo('list', (schema, options) => arrayGenerator.generate(schema, options));

    shouldBeDelegatedTo('map', schema => mapGenerator.generate(schema, objectContaining(typed<MapGenerateOptions>({
        typeNameToGenerate: 'FooBar',
        comment: 'In-memory map.',
    }))));

    shouldBeDelegatedTo('option', (schema, options) => nullableGenerator.generate(schema, options));

    shouldBeDelegatedTo('or', (schema, options) => interfaceGenerator.generate(schema, options));

    shouldBeDelegatedTo('pair', (schema, options) => interfaceGenerator.generate(schema, options));

    shouldBeDelegatedTo('set', (schema, options) => arrayGenerator.generate(schema, options));

    shouldBeDelegatedTo('ticket', (schema, options) => ticketGenerator.generate(schema, options));

    shouldBeDelegatedTo('ticket_deprecated', (schema, options) => ticketGenerator.generate(schema, options));

    Object.keys(michelsonTypes).forEach(michelsonType => {
        shouldBeDelegatedTo(michelsonType as MichelsonType, schema => primitiveGenerator.generate(schema));
    });

    function shouldBeDelegatedTo(michelsonType: MichelsonType, delegatedTo: (schema: any, options: GenerateOptions) => unknown) {
        delete michelsonTypes[michelsonType];

        it(`should delegate generation of '${michelsonType}'`, () => {
            const schema = {
                __michelsonType: michelsonType,
                schema: 'sss' as any,
            } as taquitoMichelson.TokenSchema;
            const options = {
                typeNameToGenerate: 'FooBar',
                bigMapTyping: 'mockedBigMapTyping' as any,
            } as GenerateOptions;
            when(delegatedTo(schema, options)).thenReturn('mockedType' as any);

            const generated = target.generate(schema, options);

            expect(generated).toBe('mockedType');
        });
    }
});
