export const tsTypes = Object.freeze({
    ANY: 'any',
    BOOLEAN: 'boolean',
    NEVER: 'never',
    NUMBER: 'number',
    OBJECT: 'object',
    STRING: 'string',
    UNKNOWN: 'unknown',
} as const);

export const libTypes = Object.freeze({
    BIG_MAP_ABSTRACTION: 'BigMapAbstraction',
    BIG_NUMBER: 'BigNumber',
    MICHELSON_MAP: 'MichelsonMap',
    UNIT_VALUE: 'UnitValue',
} as const);
