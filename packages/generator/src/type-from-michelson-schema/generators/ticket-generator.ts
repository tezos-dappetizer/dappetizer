import * as taquitoMichelson from '@taquito/michelson-encoder';
import { michelsonTypePrims } from '@tezos-dappetizer/indexer';
import { DeepReadonly } from 'ts-essentials';
import { singleton } from 'tsyringe';

import { GeneratedTypeParts, GenerateOptions } from './generator-router';
import { InterfaceGenerator } from './interface-generator';

export type TicketSchema = DeepReadonly<taquitoMichelson.TicketTokenSchema | taquitoMichelson.TicketDeprecatedTokenSchema>;

@singleton()
export class TicketGenerator {
    constructor(
        private readonly interfaceGenerator: InterfaceGenerator,
    ) {}

    generate(ticketSchema: TicketSchema, options: GenerateOptions): GeneratedTypeParts {
        const properties: Record<string, taquitoMichelson.TokenSchema> = {
            // Taquito incorrectly uses `int` for amount -> replace it with `nat`.
            amount: { __michelsonType: michelsonTypePrims.NAT, schema: michelsonTypePrims.NAT },
            ticketer: {
                __michelsonType: michelsonTypePrims.CONTRACT,
                schema: { parameter: { __michelsonType: michelsonTypePrims.STRING, schema: michelsonTypePrims.STRING } },
            },
            value: ticketSchema.schema.value,
        };

        return this.interfaceGenerator.generate(
            { __michelsonType: 'pair', schema: properties },
            { ...options, comment: 'Ticket.' },
        );
    }
}
