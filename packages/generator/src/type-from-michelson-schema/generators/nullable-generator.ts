import * as taquitoMichelson from '@taquito/michelson-encoder';
import { DeepReadonly } from 'ts-essentials';
import { inject, singleton } from 'tsyringe';

import { GeneratedTypeParts, GenerateOptions, GeneratorRouter, ROUTER_GENERATOR_DI_TOKEN } from './generator-router';

export type OptionSchema = DeepReadonly<taquitoMichelson.OptionTokenSchema>;

@singleton()
export class NullableGenerator {
    constructor(
        @inject(ROUTER_GENERATOR_DI_TOKEN) private readonly generatorRouter: GeneratorRouter,
    ) {}

    generate(optionSchema: OptionSchema, options: GenerateOptions): GeneratedTypeParts {
        const generatedOption = this.generatorRouter.generate(optionSchema.schema, {
            ...options,
            typeNameToGenerate: `${options.typeNameToGenerate}Option`,
        });

        return {
            ...generatedOption,
            typeDeclaration: `{ Some: ${generatedOption.typeDeclaration} } | null`,
        };
    }
}
