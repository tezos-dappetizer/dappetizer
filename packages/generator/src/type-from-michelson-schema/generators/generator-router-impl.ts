import { MichelsonSchema, michelsonTypePrims } from '@tezos-dappetizer/indexer';
import { delay, registry, singleton } from 'tsyringe';

import { ArrayGenerator } from './array-generator';
import { BigMapGenerator } from './big-map-generator';
import { GeneratedTypeParts, GenerateOptions, GeneratorRouter, ROUTER_GENERATOR_DI_TOKEN } from './generator-router';
import { InterfaceGenerator } from './interface-generator';
import { MapGenerator } from './map-generator';
import { NullableGenerator } from './nullable-generator';
import { PrimitiveGenerator } from './primitive-generator';
import { TicketGenerator } from './ticket-generator';

@singleton()
@registry([{
    token: ROUTER_GENERATOR_DI_TOKEN,
    useToken: delay(() => GeneratorRouterImpl),
}])
export class GeneratorRouterImpl implements GeneratorRouter {
    constructor(
        private readonly arrayGenerator: ArrayGenerator,
        private readonly bigMapGenerator: BigMapGenerator,
        private readonly interfaceGenerator: InterfaceGenerator,
        private readonly mapGenerator: MapGenerator,
        private readonly nullableGenerator: NullableGenerator,
        private readonly primitiveGenerator: PrimitiveGenerator,
        private readonly ticketGenerator: TicketGenerator,
    ) {}

    generate(schema: MichelsonSchema['generated'], options: GenerateOptions): GeneratedTypeParts {
        switch (schema.__michelsonType) { // eslint-disable-line no-underscore-dangle
            case michelsonTypePrims.BIG_MAP:
                return this.bigMapGenerator.generate(schema, options);
            case michelsonTypePrims.CONSTANT:
                return this.primitiveGenerator.generateConstant();
            case michelsonTypePrims.LIST:
                return this.arrayGenerator.generate(schema, options);
            case michelsonTypePrims.MAP:
                return this.mapGenerator.generate(schema, { ...options, comment: 'In-memory map.' });
            case michelsonTypePrims.OPTION:
                return this.nullableGenerator.generate(schema, options);
            case michelsonTypePrims.OR:
                return this.interfaceGenerator.generate(schema, options);
            case michelsonTypePrims.PAIR:
                return this.interfaceGenerator.generate(schema, options);
            case michelsonTypePrims.SET:
                return this.arrayGenerator.generate(schema, options);
            case michelsonTypePrims.TICKET:
            case michelsonTypePrims.TICKET_DEPRECATED:
                return this.ticketGenerator.generate(schema, options);
            default:
                return this.primitiveGenerator.generate(schema);
        }
    }
}
