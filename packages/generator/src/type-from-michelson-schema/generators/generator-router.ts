import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { InjectionToken } from 'tsyringe';

import { ModuleImport } from '../../helpers/module-imports';
import { BigMapTyping } from '../big-map-typing';

export interface GenerateOptions {
    readonly typeNameToGenerate: string;
    readonly bigMapTyping: BigMapTyping;
}

export interface GeneratedTypeParts {
    readonly typeDeclaration: string;
    readonly commentLines: readonly string[];
    readonly relatedCodeLines: readonly string[];
    readonly imports: readonly ModuleImport[];
}

export interface GeneratorRouter {
    generate(schema: MichelsonSchema['generated'], options: GenerateOptions): GeneratedTypeParts;
}

export const ROUTER_GENERATOR_DI_TOKEN: InjectionToken<GeneratorRouter> = 'Dappetizer:Generator:GeneratorRouter';
