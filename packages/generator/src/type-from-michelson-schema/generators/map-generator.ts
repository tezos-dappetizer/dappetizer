import * as taquitoMichelson from '@taquito/michelson-encoder';
import { npmModules } from '@tezos-dappetizer/utils';
import { DeepReadonly } from 'ts-essentials';
import { singleton } from 'tsyringe';

import { GeneratedTypeParts } from './generator-router';
import { MapGeneratorHelper, MapGenerateOptions } from './map-generator-helper';
import { libTypes } from '../simple-types';

export type MapSchema = DeepReadonly<taquitoMichelson.BigMapTokenSchema | taquitoMichelson.MapTokenSchema>;

@singleton()
export class MapGenerator {
    constructor(
        private readonly helper: MapGeneratorHelper,
    ) {}

    generate(mapSchema: MapSchema, options: MapGenerateOptions): GeneratedTypeParts {
        const generated = this.helper.generate(mapSchema, options);

        return {
            ...generated,
            typeDeclaration: libTypes.MICHELSON_MAP + generated.genericsSuffix,
            imports: [
                ...generated.imports,
                { import: libTypes.MICHELSON_MAP, from: npmModules.taquito.TAQUITO },
            ],
        };
    }
}
