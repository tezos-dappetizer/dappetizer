import * as taquitoMichelson from '@taquito/michelson-encoder';
import { DeepReadonly, StrictOmit } from 'ts-essentials';
import { inject, singleton } from 'tsyringe';

import { GeneratedTypeParts, GeneratorRouter, ROUTER_GENERATOR_DI_TOKEN } from './generator-router';
import { BigMapTyping } from '../big-map-typing';

export interface MapGenerateOptions {
    readonly typeNameToGenerate: string;
    readonly comment: string;
}

export type GeneratedMapParts = StrictOmit<GeneratedTypeParts, 'typeDeclaration'> & {
    readonly genericsSuffix: string;
};

export type MapSchema = DeepReadonly<taquitoMichelson.BigMapTokenSchema | taquitoMichelson.MapTokenSchema>;

@singleton()
export class MapGeneratorHelper {
    constructor(
        @inject(ROUTER_GENERATOR_DI_TOKEN) private readonly generatorRouter: GeneratorRouter,
    ) {}

    generate(mapSchema: MapSchema, options: MapGenerateOptions): GeneratedMapParts {
        const generatedKey = this.generatorRouter.generate(mapSchema.schema.key, {
            typeNameToGenerate: `${options.typeNameToGenerate}Key`,
            bigMapTyping: BigMapTyping.Unexpected,
        });
        const generatedValue = this.generatorRouter.generate(mapSchema.schema.value, {
            typeNameToGenerate: `${options.typeNameToGenerate}Value`,
            bigMapTyping: BigMapTyping.Unexpected,
        });

        return {
            genericsSuffix: `<${generatedKey.typeDeclaration}, ${generatedValue.typeDeclaration}>`,
            commentLines: [
                options.comment,
                '',
                ...prefixComment(`Key of \`${generatedKey.typeDeclaration}\``, generatedKey.commentLines),
                '',
                ...prefixComment(`Value of \`${generatedValue.typeDeclaration}\``, generatedValue.commentLines),
            ],
            relatedCodeLines: [...generatedKey.relatedCodeLines, ...generatedValue.relatedCodeLines],
            imports: [...generatedKey.imports, ...generatedValue.imports],
        };
    }
}

function prefixComment(prefix: string, commentLines: readonly string[]): string[] {
    switch (commentLines.length) {
        case 0: return [`${prefix}.`];
        case 1: return [`${prefix}: ${commentLines[0] ?? ''}`];
        default: return [`${prefix}:`, ...commentLines];
    }
}
