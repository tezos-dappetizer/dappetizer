import * as taquitoMichelson from '@taquito/michelson-encoder';
import { npmModules } from '@tezos-dappetizer/utils';
import { DeepReadonly } from 'ts-essentials';
import { singleton } from 'tsyringe';

import { GeneratedTypeParts, GenerateOptions } from './generator-router';
import { MapGenerator } from './map-generator';
import { MapGeneratorHelper } from './map-generator-helper';
import { BigMapTyping } from '../big-map-typing';
import { libTypes, tsTypes } from '../simple-types';

export type BigMapSchema = DeepReadonly<taquitoMichelson.BigMapTokenSchema>;

@singleton()
export class BigMapGenerator {
    constructor(
        private readonly mapGenerator: MapGenerator,
        private readonly helper: MapGeneratorHelper,
    ) {}

    generate(bigMapSchema: BigMapSchema, options: GenerateOptions): GeneratedTypeParts {
        switch (options.bigMapTyping) {
            case BigMapTyping.InitialValues:
                return this.mapGenerator.generate(bigMapSchema, { ...options, comment: 'Big map initial values.' });

            case BigMapTyping.RawID:
                return {
                    typeDeclaration: tsTypes.STRING,
                    commentLines: ['Big map ID - string with arbitrary big integer, negative if temporary.'],
                    imports: [],
                    relatedCodeLines: [],
                };

            case BigMapTyping.TaquitoAbstraction: {
                const mapped = this.helper.generate(bigMapSchema, { ...options, comment: 'Big map.' });

                return {
                    ...mapped,
                    typeDeclaration: libTypes.BIG_MAP_ABSTRACTION, // It isn't generic :-(
                    imports: [
                        ...mapped.imports,
                        { import: libTypes.BIG_MAP_ABSTRACTION, from: npmModules.taquito.TAQUITO },
                    ],
                };
            }

            case BigMapTyping.Unexpected:
                throw new Error(`Big map is NOT allowed within its parent value. Big map: ${JSON.stringify(bigMapSchema)}`);
        }
    }
}
