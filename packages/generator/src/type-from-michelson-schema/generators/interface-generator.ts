import * as taquitoMichelson from '@taquito/michelson-encoder';
import { pascalCase } from '@tezos-dappetizer/utils';
import { orderBy } from 'lodash';
import { DeepReadonly } from 'ts-essentials';
import { inject, singleton } from 'tsyringe';

import { GeneratedTypeParts, GenerateOptions, GeneratorRouter, ROUTER_GENERATOR_DI_TOKEN } from './generator-router';
import { CodeStringBuilder } from '../../helpers/code-string-builder';
import { ModuleImport } from '../../helpers/module-imports';
import { addTypeDocComment } from '../generator-utils';

export type InterfaceSchema = DeepReadonly<taquitoMichelson.PairTokenSchema | taquitoMichelson.OrTokenSchema>;

export interface InterfaceOptions extends GenerateOptions {
    readonly comment?: string;
}

@singleton()
export class InterfaceGenerator {
    constructor(
        @inject(ROUTER_GENERATOR_DI_TOKEN) private readonly generatorRouter: GeneratorRouter,
    ) {}

    generate(interfaceSchema: InterfaceSchema, options: InterfaceOptions): GeneratedTypeParts {
        const relatedCodeLines: string[] = [];
        const builder = new CodeStringBuilder();
        const imports: ModuleImport[] = [];
        const propertyNameSuffix = interfaceSchema.__michelsonType === 'or' ? '?' : ''; // eslint-disable-line no-underscore-dangle
        const commentLines = options.comment ? [options.comment] : [];

        addTypeDocComment(builder, commentLines)
            .addLine(`export interface ${options.typeNameToGenerate} {`)
            .indent();

        for (const [propertyName, propertySchema] of orderBy(Object.entries(interfaceSchema.schema), p => p[0])) {
            const safePropertyName = isSafeAsIdentifier(propertyName) ? propertyName : `'${propertyName}'`;
            const generatedProperty = this.generatorRouter.generate(propertySchema, {
                ...options,
                typeNameToGenerate: options.typeNameToGenerate + pascalCase(propertyName),
            });

            addTypeDocComment(builder, generatedProperty.commentLines)
                .addLine(`${safePropertyName}${propertyNameSuffix}: ${generatedProperty.typeDeclaration};`)
                .addEmptyLine();

            relatedCodeLines.push(...generatedProperty.relatedCodeLines);
            imports.push(...generatedProperty.imports);
        }

        builder.trimEnd()
            .unindent()
            .addLine('}')
            .addEmptyLine();

        return {
            typeDeclaration: options.typeNameToGenerate,
            commentLines,
            imports,
            relatedCodeLines: [...builder.lines, ...relatedCodeLines],
        };
    }
}

function isSafeAsIdentifier(str: string): boolean {
    return /^[a-z][a-z0-9_]*$/iu.test(str);
}
