import * as taquitoMichelson from '@taquito/michelson-encoder';
import { deepFreeze, npmModules } from '@tezos-dappetizer/utils';
import { DeepReadonly } from 'ts-essentials';
import { singleton } from 'tsyringe';

import { GeneratedTypeParts } from './generator-router';
import { ModuleImport } from '../../helpers/module-imports';
import { libTypes, tsTypes } from '../simple-types';

export type PrimitiveSchema =
    DeepReadonly<
    | taquitoMichelson.BaseTokenSchema
    | taquitoMichelson.ContractTokenSchema
    | taquitoMichelson.LambdaTokenSchema
    | taquitoMichelson.SaplingStateTokenSchema
    | taquitoMichelson.SaplingTransactionDeprecatedTokenSchema
    | taquitoMichelson.SaplingTransactionTokenSchema>;

@singleton()
export class PrimitiveGenerator {
    generate(primitiveSchema: PrimitiveSchema): GeneratedTypeParts {
        return mapping[primitiveSchema.__michelsonType]; // eslint-disable-line no-underscore-dangle
    }

    generateConstant(): GeneratedTypeParts {
        throw new Error('A global constant is not supported.'
            + ' It should be expanded upfront to actual Michelson so that Dappetizer can generate corresponding type.');
    }
}

const mapping = deepFreeze<Record<PrimitiveSchema['__michelsonType'], GeneratedTypeParts>>({
    address: mapString('Tezos address.'),
    bls12_381_fr: mapString('Bls12_381_fr bytes.'),
    bls12_381_g1: mapString('Bls12_381_g1 bytes.'),
    bls12_381_g2: mapString('Bls12_381_g2 bytes.'),
    bool: mapTo(tsTypes.BOOLEAN, 'Simple boolean.'),
    bytes: mapString('Bytes.'),
    chain_id: mapString('Chain ID.'),
    chest: mapString('Chest.'),
    chest_key: mapString('Chest key.'),
    contract: mapString('Contract address.'),
    int: mapBigNumber('Integer from -2^31-1 to 2^31.'),
    key: mapString('Key.'),
    key_hash: mapString('Key hash.'),
    lambda: mapUnknown('Lambda.'),
    mutez: mapBigNumber('Mutez - arbitrary big integer >= 0.'),
    nat: mapBigNumber('Nat - arbitrary big integer >= 0.'),
    never: mapTo(tsTypes.NEVER, 'Never.'),
    operation: mapUnknown('Operation.'),
    sapling_state: mapString('Sapling state.'),
    sapling_transaction: mapUnknown('Sapling transaction.'),
    sapling_transaction_deprecated: mapUnknown('Deprecated sapling transaction.'),
    signature: mapString('Signature.'),
    string: mapString('Arbitrary string.'),
    timestamp: mapString('Date ISO 8601 string.'),
    unit: mapTo(
        `typeof ${libTypes.UNIT_VALUE}`,
        'An empty result.',
        { import: libTypes.UNIT_VALUE, from: npmModules.taquito.MICHELSON_ENCODER },
    ),
});

function mapBigNumber(comment: string): GeneratedTypeParts {
    return mapTo(libTypes.BIG_NUMBER, comment, { import: libTypes.BIG_NUMBER, from: npmModules.BIGNUMBER });
}

function mapString(comment: string): GeneratedTypeParts {
    return mapTo(tsTypes.STRING, comment);
}

function mapUnknown(comment: string): GeneratedTypeParts {
    return mapTo(tsTypes.UNKNOWN, comment);
}

export function mapTo(typeName: string, comment: string, moduleImport?: ModuleImport): GeneratedTypeParts {
    return {
        typeDeclaration: typeName,
        commentLines: [comment],
        imports: moduleImport ? [moduleImport] : [],
        relatedCodeLines: [],
    };
}
