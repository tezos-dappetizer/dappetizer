import * as taquitoMichelson from '@taquito/michelson-encoder';
import { isNonEmpty } from '@tezos-dappetizer/utils';
import { DeepReadonly } from 'ts-essentials';
import { inject, singleton } from 'tsyringe';

import { GeneratedTypeParts, GenerateOptions, GeneratorRouter, ROUTER_GENERATOR_DI_TOKEN } from './generator-router';

export type ArraySchema = DeepReadonly<taquitoMichelson.ListTokenSchema | taquitoMichelson.SetTokenSchema>;

@singleton()
export class ArrayGenerator {
    constructor(
        @inject(ROUTER_GENERATOR_DI_TOKEN) private readonly generatorRouter: GeneratorRouter,
    ) {}

    generate(arraySchema: ArraySchema, options: GenerateOptions): GeneratedTypeParts {
        const generatedItem = this.generatorRouter.generate(arraySchema.schema, {
            ...options,
            typeNameToGenerate: `${options.typeNameToGenerate}Item`,
        });
        const commentLines = isNonEmpty(generatedItem.commentLines)
            ? [`Array of: ${generatedItem.commentLines[0]}`, ...generatedItem.commentLines.slice(1)]
            : [];

        return {
            ...generatedItem,
            typeDeclaration: `${generatedItem.typeDeclaration}[]`,
            commentLines,
        };
    }
}
