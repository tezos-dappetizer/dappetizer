import { errorToString, isWhiteSpace } from '@tezos-dappetizer/utils';
import { EOL } from 'os';

import { FileGenerator } from './file-generator';

export abstract class JsonFileGenerator<TContents, TOptions> implements FileGenerator<TOptions> {
    generateContents(options: TOptions, existingContents: string | null): string {
        const existingObj = !isWhiteSpace(existingContents)
            ? this.parseExistingObject(existingContents, options)
            : null;

        const newObj = this.generateContentsObject(options, existingObj);

        const newContents = JSON.stringify(newObj, undefined, 4);
        return newContents.replace(/(?:\r\n|\r|\n)/gu, EOL) + EOL;
    }

    protected validateExistingContents?(contents: TContents): void;

    private parseExistingObject(str: string, options: TOptions): TContents {
        try {
            const obj = JSON.parse(str) as TContents;

            this.validateExistingContents?.(obj);
            return obj;
        } catch (error) {
            const filePath = this.getRelPath(options);
            const errorMessage = errorToString(error, { onlyMessage: true });
            throw new Error(`Failed merging changes to file '${filePath}' because it is not valid. ${errorMessage}`);
        }
    }

    abstract getRelPath(options: TOptions): string;

    protected abstract generateContentsObject(options: TOptions, existingContents: TContents | null): TContents;
}
