import { errorToString, FILE_SYSTEM_DI_TOKEN, FileSystem, injectLogger, Logger } from '@tezos-dappetizer/utils';
import path from 'path';
import { inject, singleton } from 'tsyringe';

import { FileGenerator } from './file-generator';

@singleton()
export class FileEmitter {
    constructor(
        @inject(FILE_SYSTEM_DI_TOKEN) private readonly fileSystem: FileSystem,
        @injectLogger(FileEmitter) private readonly logger: Logger,
    ) {}

    async emitFiles<TGeneratorOptions>(
        outDirPath: string,
        generatorOptions: TGeneratorOptions,
        fileGenerators: readonly FileGenerator<TGeneratorOptions>[],
    ): Promise<void> {
        await Promise.all(fileGenerators.map(async fileGenerator => {
            const fileRelPath = fileGenerator.getRelPath(generatorOptions);
            const filePath = path.resolve(outDirPath, fileRelPath);

            try {
                const existingContents = await this.fileSystem.readFileTextIfExists(filePath);
                const contents = fileGenerator.generateContents(generatorOptions, existingContents);

                await this.fileSystem.writeFileText(filePath, contents);
                this.logger.logInformation('Generated {file}.', { file: fileRelPath });
            } catch (error) {
                throw new Error(`Failed to emit file '${filePath}'. ${errorToString(error)}`);
            }
        }));
    }
}
