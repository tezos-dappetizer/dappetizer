import { singleton } from 'tsyringe';

import { StructuralFileGenerator, StructuralFileOptions } from './structural-file-generator';
import { generatedPaths } from '../../helpers/generated-paths';
import { JsonFileGenerator } from '../json-file-generator';

@singleton()
export class TsconfigGenerator extends JsonFileGenerator<object, StructuralFileOptions> implements StructuralFileGenerator {
    override getRelPath(_options: StructuralFileOptions): string {
        return generatedPaths.TSCONFIG_JSON;
    }

    protected override generateContentsObject(_options: StructuralFileOptions, _existingContents: object | null): object {
        return {
            include: ['src/**/*'],
            compilerOptions: {
                outDir: 'dist',
                target: 'ES2021',
                lib: ['ES2021'],
                module: 'CommonJS',
                moduleResolution: 'Node',
                experimentalDecorators: true,
                emitDecoratorMetadata: true,
                strict: true,
                typeRoots: ['./node_modules/@types'],
            },
        };
    }
}
