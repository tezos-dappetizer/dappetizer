import { JSONSchemaForNPMPackageJsonFiles } from '@schemastore/package';
import { argGuard } from '@tezos-dappetizer/utils';
import path from 'path';
import { singleton } from 'tsyringe';

import { StructuralFileGenerator, StructuralFileOptions } from './structural-file-generator';
import { generatedPaths } from '../../helpers/generated-paths';
import { generatedNpmScripts } from '../../helpers/generator-defaults';
import { JsonFileGenerator } from '../json-file-generator';

@singleton()
export class PackageJsonGenerator extends JsonFileGenerator<JSONSchemaForNPMPackageJsonFiles, StructuralFileOptions>
    implements StructuralFileGenerator {
    override getRelPath(_options: StructuralFileOptions): string {
        return generatedPaths.PACKAGE_JSON;
    }

    protected override validateExistingContents(packageJson: JSONSchemaForNPMPackageJsonFiles): void {
        // Validate only properties that are used by this class.
        argGuard.object(packageJson, '$');
        argGuard.ifNotNullish(packageJson.scripts, s => argGuard.object(s, '$.scripts'));
    }

    protected override generateContentsObject(
        _options: StructuralFileOptions,
        existingPackageJson: JSONSchemaForNPMPackageJsonFiles | null,
    ): JSONSchemaForNPMPackageJsonFiles {
        const packageJson = existingPackageJson ?? {};

        packageJson.main = `${generatedPaths.DIST}/${path.basename(generatedPaths.INDEX_TS, '.ts')}.js`;
        packageJson.scripts = packageJson.scripts ?? {};
        packageJson.scripts[`pre${generatedNpmScripts.BUILD}`] = `rimraf ./${generatedPaths.DIST}`;
        packageJson.scripts[generatedNpmScripts.BUILD] = `tsc -p ./${generatedPaths.TSCONFIG_JSON}`;

        return packageJson;
    }
}
