import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';
import { DappetizerConfig, loadDappetizerNetworkConfigs } from '@tezos-dappetizer/indexer';
import { keyof, npmModules, typed } from '@tezos-dappetizer/utils';
import { StrictExtract } from 'ts-essentials';
import { singleton } from 'tsyringe';

import { StructuralFileGenerator, StructuralFileOptions } from './structural-file-generator';
import { generatedPaths } from '../../helpers/generated-paths';
import { TypeScriptBuilder } from '../../helpers/type-script-builder';

type SqliteConfig = StrictExtract<DappetizerConfigUsingDb['database'], { type: 'sqlite' }>;
type PostgresConfig = StrictExtract<DappetizerConfigUsingDb['database'], { type: 'postgres' }>;

@singleton()
export class DappetizerConfigGenerator implements StructuralFileGenerator {
    getRelPath(_options: StructuralFileOptions): string {
        return generatedPaths.DAPPETIZER_CONFIG_TS;
    }

    generateContents(options: StructuralFileOptions): string {
        const configBuilder = new DappetizerConfigBuilder();

        configBuilder.beginConfig();
        configBuilder.addModules();
        configBuilder.addNetworkSpecificParts();
        configBuilder.addDatabase();
        configBuilder.addUsageStatistics(options);
        configBuilder.endConfig();

        return configBuilder.getFinalCode();
    }
}

class DappetizerConfigBuilder extends TypeScriptBuilder {
    beginConfig(): void {
        this.imports.add({ import: 'DappetizerConfigUsingDb', from: npmModules.dappetizer.DATABASE });

        this.code.addLine(`const config: DappetizerConfigUsingDb = {`)
            .indent();
    }

    addModules(): void {
        this.code.addLine(`${keyof<DappetizerConfig>('modules')}: [{`)
            .indent()
            .addLine(`${keyof<DappetizerConfig['modules'][number]>('id')}: '.', // This project is the indexer module itself.`)
            .unindent()
            .addLine(`}],`);
    }

    addNetworkSpecificParts(): void {
        this.imports.add({ import: loadDappetizerNetworkConfigs.name, from: npmModules.dappetizer.INDEXER });

        this.code.addLine(`${keyof<DappetizerConfig>('networks')}: ${loadDappetizerNetworkConfigs.name}(__dirname),`);
    }

    addDatabase(): void {
        this.code.addLine(`${keyof<DappetizerConfigUsingDb>('database')}: {`)
            .indent()
            .addLine(`${keyof<SqliteConfig>('type')}: '${typed<SqliteConfig['type']>('sqlite')}',`)
            .addLine(`${keyof<SqliteConfig>('database')}: 'database.sqlite',`)
            .addEmptyLine()
            .addLine(`// If you want to use PostgreSQL:`)
            .addLine(`// ${keyof<PostgresConfig>('type')}: '${typed<PostgresConfig['type']>('postgres')}',`)
            .addLine(`// ${keyof<PostgresConfig>('host')}: 'localhost',`)
            .addLine(`// ${keyof<PostgresConfig>('port')}: 5432,`)
            .addLine(`// ${keyof<PostgresConfig>('username')}: 'postgres',`)
            .addLine(`// ${keyof<PostgresConfig>('password')}: 'postgrespassword',`)
            .addLine(`// ${keyof<PostgresConfig>('database')}: 'postgres',`)
            .addLine(`// ${keyof<PostgresConfig>('schema')}: 'indexer',`)
            .unindent()
            .addLine(`},`);
    }

    addUsageStatistics(options: StructuralFileOptions): void {
        this.code.addLine(`${keyof<DappetizerConfigUsingDb>('usageStatistics')}: {`)
            .indent()
            .addLine(`${keyof<NonNullable<DappetizerConfigUsingDb['usageStatistics']>>('enabled')}: ${options.enableUsageStatistics},`)
            .unindent()
            .addLine(`},`);
    }

    endConfig(): void {
        this.code.unindent()
            .addLine(`};`)
            .addEmptyLine()
            .addLine(`export default config;`)
            .addEmptyLine();
    }
}
