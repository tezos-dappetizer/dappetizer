import { ContractDetails } from '../../helpers/contract-details-resolver';
import { FileGenerator } from '../file-generator';

export interface StructuralFileOptions {
    readonly contractDetails: ContractDetails | null;
    readonly tezosNodeUrl: string;
    readonly isTezosNodeUrlExplicit: boolean;
    readonly tezosNetwork: string;
    readonly indexerModuleName: string;
    readonly enableUsageStatistics: boolean;
}

export type StructuralFileGenerator = FileGenerator<StructuralFileOptions>;
