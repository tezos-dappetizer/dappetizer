import { DappetizerConfig, NETWORK_CONFIG_FILE_PREFIX, NETWORK_CONFIG_FILE_SUFFIX } from '@tezos-dappetizer/indexer';
import { argGuard, keyof, MAINNET } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { StructuralFileGenerator, StructuralFileOptions } from './structural-file-generator';
import { JsonFileGenerator } from '../json-file-generator';

export type NetworkConfig = DappetizerConfig['networks'][string];

type GenerateConfigOptions = Pick<StructuralFileOptions, 'contractDetails' | 'isTezosNodeUrlExplicit' | 'tezosNetwork' | 'tezosNodeUrl'>;

@singleton()
export class DappetizerNetworkConfigGenerator extends JsonFileGenerator<NetworkConfig, GenerateConfigOptions> implements StructuralFileGenerator {
    override getRelPath(options: GenerateConfigOptions): string {
        return NETWORK_CONFIG_FILE_PREFIX + options.tezosNetwork + NETWORK_CONFIG_FILE_SUFFIX;
    }

    protected override validateExistingContents(config: NetworkConfig): void {
        // Validate only properties that are used by this class.
        argGuard.object(config, '$');
        argGuard.object(config.tezosNode, `$.${keyof<typeof config>('tezosNode')}`);
        argGuard.object(config.indexing, `$.${keyof<typeof config>('indexing')}`);

        const contractsName = `$.${keyof<typeof config>('indexing')}.${keyof<typeof config.indexing>('contracts')}`;
        argGuard.array(config.indexing.contracts ?? [], contractsName).forEach((contractConfig, contractIndex) => {
            argGuard.object(contractConfig, `${contractsName}[${contractIndex}]`);
        });
    }

    protected override generateContentsObject(options: GenerateConfigOptions, existingConfig: NetworkConfig | null): NetworkConfig {
        const config = existingConfig ?? this.createNewConfig(options);

        this.mergeTezosNodeUrl(config, options);
        this.mergeContractAddress(config, options);

        return config;
    }

    private createNewConfig(options: GenerateConfigOptions): NetworkConfig {
        return {
            indexing: {
                fromBlockLevel: options.tezosNetwork === MAINNET ? 851_969 : 0,
                contractBoost: options.tezosNetwork === MAINNET ? { type: 'tzkt' } : undefined,
            },
            tezosNode: { url: options.tezosNodeUrl },
        };
    }

    private mergeTezosNodeUrl(networkConfig: NetworkConfig, options: GenerateConfigOptions): void {
        if (options.isTezosNodeUrlExplicit) {
            networkConfig.tezosNode = {
                ...networkConfig.tezosNode,
                url: options.tezosNodeUrl,
            };
        }
    }

    private mergeContractAddress(networkConfig: NetworkConfig, options: GenerateConfigOptions): void {
        if (options.contractDetails) {
            const contractName = options.contractDetails.name;
            const contractConfig = networkConfig.indexing.contracts?.find(c => c.name === contractName);

            if (contractConfig) {
                contractConfig.addresses = [options.contractDetails.contract.address];
            } else {
                networkConfig.indexing.contracts ??= [];
                networkConfig.indexing.contracts.push({
                    name: options.contractDetails.name,
                    addresses: [options.contractDetails.contract.address],
                });
            }
        }
    }
}
