import { IndexerModuleUsingDb } from '@tezos-dappetizer/database';
import { createContractIndexerFromDecorators } from '@tezos-dappetizer/decorators';
import { INDEXER_MODULE_EXPORT_NAME } from '@tezos-dappetizer/indexer';
import { npmModules } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { StructuralFileGenerator, StructuralFileOptions } from './structural-file-generator';
import { CodeStringBuilder } from '../../helpers/code-string-builder';
import { generatedPaths } from '../../helpers/generated-paths';
import { ImportsBuilder } from '../../helpers/module-imports';
import { getIndexerClassFileNameWithoutExt, getIndexerClassName } from '../contract-indexer/indexer-class-generator';

const moduleKey = (k: keyof IndexerModuleUsingDb & string): string => k;

@singleton()
export class IndexerModuleGenerator implements StructuralFileGenerator {
    getRelPath(_options: StructuralFileOptions): string {
        return `${generatedPaths.SRC}/${generatedPaths.INDEX_TS}`;
    }

    generateContents(options: StructuralFileOptions): string {
        const codeBuilder = new CodeStringBuilder();
        const imports = new ImportsBuilder()
            .add({ import: 'IndexerModuleUsingDb', from: npmModules.dappetizer.DATABASE });

        codeBuilder.addLine(`export const ${INDEXER_MODULE_EXPORT_NAME}: IndexerModuleUsingDb = {`)
            .indent()
            .addLine(`${moduleKey('name')}: '${options.indexerModuleName}',`)
            .addLine(`${moduleKey('dbEntities')}: [`)
            .indent()
            .addLine(`// Register your DB entity classes to TypeORM here:`)
            .addLine(`// MyDbEntity,`)
            .unindent()
            .addLine(`],`)
            .addLine(`${moduleKey('contractIndexers')}: [`)
            .indent()
            .addLine(`// Create your contract indexers here:`);

        if (options.contractDetails) {
            const indexerClassName = getIndexerClassName(options.contractDetails);
            codeBuilder.addLine(`${createContractIndexerFromDecorators.name}(new ${indexerClassName}()),`);

            imports.add({ import: indexerClassName, from: `./${getIndexerClassFileNameWithoutExt(options.contractDetails)}` })
                .add({ import: createContractIndexerFromDecorators.name, from: npmModules.dappetizer.DECORATORS });
        } else {
            codeBuilder.addLine(`// new MyContractIndexer(),`);
        }

        codeBuilder.unindent()
            .addLine(`],`)
            .addLine(`${moduleKey('blockDataIndexers')}: [`)
            .indent()
            .addLine(`// Create your block data indexers here:`)
            .addLine(`// new MyBlockDataIndexer(),`)
            .unindent()
            .addLine(`],`)
            .addLine(`// Create your indexing cycle handler here:`)
            .addLine(`// ${moduleKey('indexingCycleHandler')}: new MyIndexingCycleHandler(),`)
            .unindent()
            .addLine(`};`)
            .addEmptyLine();

        return CodeStringBuilder.join(imports.getFinalLines(), codeBuilder.lines);
    }
}
