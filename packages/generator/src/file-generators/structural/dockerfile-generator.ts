import { singleton } from 'tsyringe';

import { StructuralFileGenerator, StructuralFileOptions } from './structural-file-generator';
import { CodeStringBuilder } from '../../helpers/code-string-builder';
import { generatedPaths } from '../../helpers/generated-paths';

@singleton()
export class DockerfileGenerator implements StructuralFileGenerator {
    getRelPath(_options: StructuralFileOptions): string {
        return generatedPaths.DOCKERFILE;
    }

    generateContents(_options: StructuralFileOptions): string {
        const image = 'node:16-alpine3.11';
        const localDir = '/app';
        const stage = 'builder';

        const fileNamesToBuild = [
            generatedPaths.PACKAGE_JSON,
            generatedPaths.PACKAGE_LOCK_JSON,
            generatedPaths.DAPPETIZER_CONFIG_TS,
            generatedPaths.TSCONFIG_JSON,
        ];
        const dirNamesToBuild = [
            generatedPaths.SRC,
        ];
        const fileNamesToPublish = [
            generatedPaths.PACKAGE_JSON,
            generatedPaths.DAPPETIZER_CONFIG_TS,
        ];
        const dirNamesToPublish = [
            generatedPaths.DIST,
            generatedPaths.NODE_MODULES,
        ];

        const builder = new CodeStringBuilder()
            .addLine(`# Prepare files and a temp image to build.`)
            .addLine(`FROM ${image} AS ${stage}`)
            .addLine(`COPY ${fileNamesToBuild.join(' ')} ${localDir}/`)
            .addLines(dirNamesToBuild.map(d => `COPY ${d} ${localDir}/${d}`))
            .addEmptyLine();

        builder.addLine(`# Build the app.`)
            .addLine(`WORKDIR ${localDir}`)
            .addLine(`RUN npm install && npm run build`)
            .addEmptyLine();

        builder.addLine(`# Publish files to a final image.`)
            .addLine(`FROM ${image}`)
            .addLine(`COPY --from=${stage} ${fileNamesToPublish.map(f => `${localDir}/${f}`).join(' ')} ${localDir}/`)
            .addLines(dirNamesToPublish.map(d => `COPY --from=${stage} ${localDir}/${d} ${localDir}/${d}`))
            .addEmptyLine();

        builder.addLine(`# Run the app.`)
            .addLine(`WORKDIR ${localDir}`)
            .addLine(`CMD ["./node_modules/.bin/dappetizer", "start"]`)
            .addEmptyLine();

        return builder.getFinalCode();
    }
}
