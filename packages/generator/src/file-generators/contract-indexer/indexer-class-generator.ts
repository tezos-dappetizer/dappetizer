import {
    contractFilter,
    indexBigMapUpdate,
    IndexBigMapUpdateOptions,
    indexEntrypoint,
    indexEvent,
    indexOrigination,
    indexStorageChange,
} from '@tezos-dappetizer/decorators';
import { dumpToString, isNullish, keyof, npmModules, pascalCase } from '@tezos-dappetizer/utils';
import { kebabCase } from 'lodash';
import { singleton } from 'tsyringe';

import { ContractFileGenerator, ContractFileOptions } from './contract-file-generator';
import {
    getBigMapTypeNames,
    getContractTypePrefix,
    getEventsByTag,
    getInterfacesFileNameWithoutExt,
    getStorageTypeNames,
    NormalizedEntrypoint,
    normalizeEntrypoints,
    StorageTypeNames,
} from './indexer-class-interfaces-generator';
import { ContractDetails } from '../../helpers/contract-details-resolver';
import { generatedPaths } from '../../helpers/generated-paths';
import { ModuleImport } from '../../helpers/module-imports';
import { TypeScriptBuilder } from '../../helpers/type-script-builder';
import { tsTypes } from '../../type-from-michelson-schema/simple-types';

@singleton()
export class IndexerClassGenerator implements ContractFileGenerator {
    getRelPath(options: ContractFileOptions): string {
        return `${generatedPaths.SRC}/${getIndexerClassFileNameWithoutExt(options.contractDetails)}.ts`;
    }

    generateContents(options: ContractFileOptions): string {
        const builder = new IndexerClassBuilder(options);

        builder.beginClass();
        builder.addIndexOriginationMethod();
        builder.addIndexEntrypointMethods();
        builder.addIndexStorageChangeMethod();
        builder.addIndexBigMapUpdateMethods();
        builder.addIndexEventMethods();
        builder.endClass();

        return builder.getFinalCode();
    }
}

const dbContextImport: ModuleImport = { import: 'DbContext', from: npmModules.dappetizer.DATABASE };

class IndexerClassBuilder extends TypeScriptBuilder {
    private readonly entrypoints: readonly NormalizedEntrypoint[];
    private readonly storageTypeNames: StorageTypeNames;
    private readonly generatedInterfacesModule: string;

    constructor(private readonly options: ContractFileOptions) {
        super();
        this.entrypoints = normalizeEntrypoints(options.contractDetails);
        this.storageTypeNames = getStorageTypeNames(options.contractDetails);
        this.generatedInterfacesModule = `./${getInterfacesFileNameWithoutExt(this.options.contractDetails)}`;
    }

    beginClass(): void {
        const className = getIndexerClassName(this.options.contractDetails);

        this.imports.add({ import: contractFilter.name, from: npmModules.dappetizer.DECORATORS });

        this.code.addLine(`@${contractFilter.name}({ name: '${this.options.contractDetails.name}' })`)
            .addLine(`export class ${className} {`)
            .indent();
    }

    addIndexOriginationMethod(): void {
        this.imports
            .add({ import: indexOrigination.name, from: npmModules.dappetizer.DECORATORS })
            .add({ import: this.storageTypeNames.initial, from: this.generatedInterfacesModule })
            .add(dbContextImport)
            .add({ import: 'OriginationIndexingContext', from: npmModules.dappetizer.INDEXER });

        this.addEmptyMethod({
            name: 'indexOrigination',
            decorator: `@${indexOrigination.name}()`,
            parameters: [
                { name: 'initialStorage', type: this.storageTypeNames.initial },
                { name: 'dbContext', type: dbContextImport.import },
                { name: 'indexingContext', type: 'OriginationIndexingContext' },
            ],
        });
    }

    addIndexEntrypointMethods(): void {
        for (const entrypoint of this.entrypoints) {
            this.imports
                .add({ import: indexEntrypoint.name, from: npmModules.dappetizer.DECORATORS })
                .add({ import: entrypoint.parameterTypeName, from: this.generatedInterfacesModule })
                .add(dbContextImport)
                .add({ import: 'TransactionIndexingContext', from: npmModules.dappetizer.INDEXER });

            this.addEmptyMethod({
                name: `index${entrypoint.normalizedName}`,
                decorator: `@${indexEntrypoint.name}('${entrypoint.tezosName}')`,
                parameters: [
                    { name: 'parameter', type: entrypoint.parameterTypeName },
                    { name: 'dbContext', type: dbContextImport.import },
                    { name: 'indexingContext', type: 'TransactionIndexingContext' },
                ],
            });
        }
    }

    addIndexStorageChangeMethod(): void {
        this.imports
            .add({ import: indexStorageChange.name, from: npmModules.dappetizer.DECORATORS })
            .add({ import: this.storageTypeNames.changed, from: this.generatedInterfacesModule })
            .add(dbContextImport)
            .add({ import: 'StorageChangeIndexingContext', from: npmModules.dappetizer.INDEXER });

        this.addEmptyMethod({
            name: 'indexStorageChange',
            decorator: `@${indexStorageChange.name}()`,
            parameters: [
                { name: 'newStorage', type: this.storageTypeNames.changed },
                { name: 'dbContext', type: dbContextImport.import },
                { name: 'indexingContext', type: 'StorageChangeIndexingContext' },
            ],
        });
    }

    addIndexBigMapUpdateMethods(): void {
        for (const bigMap of this.options.contractDetails.contract.bigMaps) {
            const typeNames = getBigMapTypeNames(bigMap, this.options.contractDetails);

            this.imports
                .add({ import: indexBigMapUpdate.name, from: npmModules.dappetizer.DECORATORS })
                .add({ import: typeNames.key, from: this.generatedInterfacesModule })
                .add({ import: typeNames.value, from: this.generatedInterfacesModule })
                .add(dbContextImport)
                .add({ import: 'BigMapUpdateIndexingContext', from: npmModules.dappetizer.INDEXER });

            this.addEmptyMethod({
                name: `index${pascalCase(bigMap.pathStr)}Update`,
                decorator: `@${indexBigMapUpdate.name}({ ${keyof<IndexBigMapUpdateOptions>('path')}: ${dumpToString(bigMap.path)} })`,
                parameters: [
                    { name: 'key', type: typeNames.key },
                    { name: 'value', type: `${typeNames.value} | undefined`, comment: 'Undefined represents a removal.' },
                    { name: 'dbContext', type: dbContextImport.import },
                    { name: 'indexingContext', type: 'BigMapUpdateIndexingContext' },
                ],
            });
        }
    }

    addIndexEventMethods(): void {
        for (const event of getEventsByTag(this.options.contractDetails)) {
            this.imports
                .add({ import: indexEvent.name, from: npmModules.dappetizer.DECORATORS })
                .add(dbContextImport).add({ import: 'EventIndexingContext', from: npmModules.dappetizer.INDEXER });

            let payloadTypeName: string = tsTypes.ANY;
            if (!event.schemas.some(s => isNullish(s))) {
                this.imports.add({ import: event.payloadTypeName, from: this.generatedInterfacesModule });
                payloadTypeName = event.payloadTypeName;
            }

            this.addEmptyMethod({
                name: `index${event.normalizedName}Event`,
                decorator: `@${indexEvent.name}(${event.tezosTag ? `'${event.tezosTag}'` : 'null'})`,
                parameters: [
                    { name: 'payload', type: payloadTypeName },
                    { name: 'dbContext', type: dbContextImport.import },
                    { name: 'indexingContext', type: 'EventIndexingContext' },
                ],
            });
        }
    }

    endClass(): void {
        this.code.trimEnd()
            .unindent()
            .addLine(`}`)
            .addEmptyLine();
    }

    private addEmptyMethod(options: { name: string; decorator: string; parameters: { name: string; type: string; comment?: string }[] }): void {
        this.code.addLine(options.decorator)
            .addLine(`async ${options.name}(`)
            .indent()
            .addLines(options.parameters.map(p => `${p.name}: ${p.type},${p.comment ? ` // ${p.comment}` : ''}`))
            .unindent()
            .addLine(`): Promise<void> {`)
            .indent()
            .addLine(`// Implement your indexing logic here or delete the method if not needed.`)
            .unindent()
            .addLine(`}`)
            .addEmptyLine();
    }
}

export function getIndexerClassFileNameWithoutExt(contractDetails: ContractDetails): string {
    return `${kebabCase(contractDetails.name)}-indexer`;
}

export function getIndexerClassName(contractDetails: ContractDetails): string {
    const typePrefix = getContractTypePrefix(contractDetails);
    return `${typePrefix}Indexer`;
}
