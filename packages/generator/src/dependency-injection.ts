import { registerDappetizerWithDatabase } from '@tezos-dappetizer/database';
import { DappetizerConfig, registerDappetizerIndexer } from '@tezos-dappetizer/indexer';
import {
    CONFIG_NETWORK_DI_TOKEN,
    ConfigObjectElement,
    registerOnce,
    registerSingleton,
    ROOT_CONFIG_ELEMENT_DI_TOKEN,
} from '@tezos-dappetizer/utils';
import { StrictOmit } from 'ts-essentials';
import { DependencyContainer } from 'tsyringe';

import { INDEXER_EMITTER_DI_TOKEN, IndexerEmitter } from './indexer-emitter';
import { IndexerInitialAppEmitter } from './indexer-initial-app-emitter';
import { IndexerUpdateContractEmitter } from './indexer-update-contract-emitter';

export function registerDappetizerGenerator(diContainer: DependencyContainer, tezosNodeUrl: string): void {
    registerOnce(diContainer, 'Dappetizer:Generator', () => {
        // Dependencies:
        registerDappetizerIndexer(diContainer);
        registerDappetizerWithDatabase(diContainer);

        // Public API:
        registerSingleton(diContainer, INDEXER_EMITTER_DI_TOKEN, {
            useFactory: c => {
                const initialAppEmitter = c.resolve(IndexerInitialAppEmitter);
                const updateContractEmitter = c.resolve(IndexerUpdateContractEmitter);
                return Object.freeze<IndexerEmitter>({
                    emitInitialApp: initialAppEmitter.emitInitialApp.bind(initialAppEmitter),
                    updateContractIndexerClass: updateContractEmitter.updateContractIndexerClass.bind(updateContractEmitter),
                });
            },
        });

        const tezosNetwork = 'generator';
        const config: StrictOmit<DappetizerConfig, 'modules'> = {
            networks: {
                [tezosNetwork]: {
                    indexing: { fromBlockLevel: 0 },
                    tezosNode: { url: tezosNodeUrl },
                },
            },
            logging: { console: { format: 'coloredSimpleMessages' } },
        };
        diContainer.registerInstance(ROOT_CONFIG_ELEMENT_DI_TOKEN, new ConfigObjectElement(config, '/generator-embedded', '$.generator'));
        diContainer.registerInstance(CONFIG_NETWORK_DI_TOKEN, tezosNetwork);
    });
}
