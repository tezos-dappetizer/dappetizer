import { CodeStringBuilder } from './code-string-builder';
import { ImportsBuilder } from './module-imports';

export class TypeScriptBuilder {
    preambleLines: string[] = [];
    readonly code = new CodeStringBuilder();
    readonly imports = new ImportsBuilder();

    getFinalCode(): string {
        return CodeStringBuilder.join(
            this.preambleLines,
            this.imports.getFinalLines(),
            this.code.lines,
        );
    }
}
