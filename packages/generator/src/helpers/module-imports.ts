import { groupBy, negate, orderBy } from 'lodash';

import { CodeStringBuilder } from './code-string-builder';

export interface ModuleImport {
    readonly import: string;
    readonly from: string;
}

export class ImportsBuilder {
    private importList: ModuleImport[] = [];

    get imports(): readonly ModuleImport[] {
        return this.importList;
    }

    add(importToAdd: ModuleImport): this {
        if (!this.importList.some(i => i.import === importToAdd.import && i.from === importToAdd.from)) {
            this.importList.push(importToAdd);
            this.importList = orderBy(this.importList, [i => i.from, i => i.import]);
        }
        return this;
    }

    getFinalLines(): readonly string[] {
        const builder = new CodeStringBuilder();
        addImportSection(builder, this.importList.filter(negate(isInternal)));
        addImportSection(builder, this.importList.filter(isInternal));

        return builder.lines;
    }
}

function addImportSection(builder: CodeStringBuilder, imports: readonly ModuleImport[]): void {
    if (imports.length === 0) {
        return;
    }

    for (const [module, moduleImports] of Object.entries(groupBy(imports, i => i.from))) {
        const values = moduleImports.map(i => i.import);

        if (values.length > 3) {
            builder.addLine(`import {`)
                .indent()
                .addLines(values.map(v => `${v},`))
                .unindent()
                .addLine(`} from '${module}';`);
        } else {
            builder.addLine(`import { ${values.join(', ')} } from '${module}';`);
        }
    }
    builder.addEmptyLine();
}

function isInternal(moduleImport: ModuleImport): boolean {
    return moduleImport.from.startsWith('.');
}
