import {
    locatePackageJsonInCurrentPackage,
    npmModules,
    PACKAGE_JSON_LOADER_DI_TOKEN,
    PackageJsonLoader,
} from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

@singleton()
export class GeneratorPackageJson {
    readonly devDependencies: GeneratorPackageJsonDevDependencies;

    constructor(@inject(PACKAGE_JSON_LOADER_DI_TOKEN) loader: PackageJsonLoader) {
        const packageJson = loader.load(locatePackageJsonInCurrentPackage(__filename));

        this.devDependencies = Object.freeze<GeneratorPackageJsonDevDependencies>({
            typescript: packageJson.getDevDependencyVersion(npmModules.TYPESCRIPT),
        });
    }
}

export interface GeneratorPackageJsonDevDependencies {
    readonly typescript: string;
}
