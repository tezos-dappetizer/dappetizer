import { FILE_SYSTEM_DI_TOKEN, FileSystem, injectLogger, Logger, fullCliOption } from '@tezos-dappetizer/utils';
import path from 'path';
import { inject, singleton } from 'tsyringe';

import { generatedPaths } from './generated-paths';

@singleton()
export class OutDirInitializer {
    constructor(
        @inject(FILE_SYSTEM_DI_TOKEN) private readonly fileSystem: FileSystem,
        @injectLogger(OutDirInitializer) private readonly logger: Logger,
    ) {}

    async init(outDirPath: string, description: string): Promise<void> {
        this.logger.logInformation(`Generating ${description} to {directory}. This is controlled using '${fullCliOption('OUT_DIR')}' option.`, {
            directory: path.resolve(outDirPath),
        });

        const packageJsonPath = path.resolve(outDirPath, generatedPaths.PACKAGE_JSON);
        const packageJsonExists = await this.fileSystem.existsAsFile(packageJsonPath);

        if (!packageJsonExists) {
            await this.fileSystem.writeFileText(packageJsonPath, '{}');
        }
    }
}
