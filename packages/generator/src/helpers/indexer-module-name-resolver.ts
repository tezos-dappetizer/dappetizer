import {
    FILE_SYSTEM_DI_TOKEN,
    FileSystem,
    fullCliOption,
    injectLogger,
    isNullish,
    isObjectLiteral,
    isWhiteSpace,
    Logger,
    pascalCase,
} from '@tezos-dappetizer/utils';
import path from 'path';
import { inject, singleton } from 'tsyringe';

import { generatedPaths } from './generated-paths';
import { generatorDefaults } from './generator-defaults';

const NAME_PURPOSE = 'naming the generated indexer module';
const CLI_OPTION_DISCLAIMER = `You can override it by specifying {cliOption}.`;
const cliOption = fullCliOption('INDEXER_MODULE');

@singleton()
export class IndexerModuleNameResolver {
    constructor(
        @inject(FILE_SYSTEM_DI_TOKEN) private readonly fileSystem: FileSystem,
        @injectLogger(IndexerModuleNameResolver) private readonly logger: Logger,
    ) {}

    async resolve(explicitName: string | null, outDirPath: string): Promise<string> {
        if (explicitName) {
            this.logger.logInformation(`Using {name} specified in {cliOption} for ${NAME_PURPOSE}.`, {
                name: explicitName,
                cliOption,
            });
            return explicitName;
        }

        const packageJsonPath = path.resolve(outDirPath, generatedPaths.PACKAGE_JSON);
        const packageName = await this.resolveFromPackageJson(packageJsonPath);

        if (packageName) {
            this.logger.logInformation(`Using {name} from {filePath} for ${NAME_PURPOSE}. ${CLI_OPTION_DISCLAIMER}`, {
                name: packageName,
                filePath: packageJsonPath,
                cliOption,
            });
            return packageName;
        }

        this.logger.logInformation(`Using {defaultName} for ${NAME_PURPOSE}. ${CLI_OPTION_DISCLAIMER}.`, {
            defaultName: generatorDefaults.INDEXER_MODULE_NAME,
            cliOption,
        });
        return generatorDefaults.INDEXER_MODULE_NAME;
    }

    private async resolveFromPackageJson(filePath: string): Promise<string | null> {
        try {
            const text = await this.fileSystem.readFileTextIfExists(filePath);
            if (isNullish(text)) {
                return null;
            }

            const json: unknown = JSON.parse(text);
            if (!isObjectLiteral(json)) {
                throw new Error('The file content must be an object.');
            }

            const name = json.name;
            if (!isNullish(name) && typeof name !== 'string') {
                throw new Error(`The 'name' must be a string.`);
            }

            return !isWhiteSpace(name) ? pascalCase(name) : null;
        } catch (error) {
            this.logger.logWarning(`Failed to get 'name' from JSON in {filePath} for ${NAME_PURPOSE}. {error}`, {
                filePath,
                error,
            });
            return null;
        }
    }
}
