import { MAINNET } from '@tezos-dappetizer/utils';

export const generatorDefaults = Object.freeze({
    tezosNodeUrlsByNetwork: Object.freeze({
        [MAINNET]: 'https://prod.tcinfra.net/rpc/mainnet/',
        ghostnet: 'https://ghostnet.smartpy.io/',
        jakartanet: 'https://jakartanet.smartpy.io/',
        kathmandunet: 'https://kathmandunet.tezos.marigold.dev/',
    }),
    INDEXER_MODULE_NAME: 'MyModule',
    CONTRACT_NAME: 'MyContract',
    OUT_DIR: '.',
    USAGE_STATISTICS_DOCS_URL: 'https://docs.dappetizer.dev/reference/usage-statistics',
});

export const generatedNpmScripts = Object.freeze({
    BUILD: 'build',
});
