import { DATABASE_PACKAGE_JSON_DI_TOKEN, DatabasePackageJson } from '@tezos-dappetizer/database';
import { INDEXER_PACKAGE_JSON_DI_TOKEN, IndexerPackageJson } from '@tezos-dappetizer/indexer';
import { npmModules } from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

import { GeneratorPackageJson } from './generator-package-json';
import { NpmDependenciesInstaller } from './npm-dependencies-installer';

@singleton()
export class NpmDappetizerInstaller {
    constructor(
        private readonly npmDependenciesInstaller: NpmDependenciesInstaller,
        @inject(INDEXER_PACKAGE_JSON_DI_TOKEN) private readonly indexerPackageJson: IndexerPackageJson,
        @inject(DATABASE_PACKAGE_JSON_DI_TOKEN) private readonly databasePackageJson: DatabasePackageJson,
        private readonly generatorPackageJson: GeneratorPackageJson,
    ) {}

    async installPackages(outDirPath: string): Promise<void> {
        const dappetizerVersion = this.indexerPackageJson.version;

        await this.npmDependenciesInstaller.install(outDirPath, [
            { package: npmModules.dappetizer.CLI, version: dappetizerVersion },
            { package: npmModules.dappetizer.DATABASE, version: dappetizerVersion },
            { package: npmModules.dappetizer.DECORATORS, version: dappetizerVersion },
            { package: npmModules.dappetizer.INDEXER, version: dappetizerVersion },
            { package: npmModules.dappetizer.UTILS, version: dappetizerVersion },

            { package: npmModules.taquito.TAQUITO, version: this.indexerPackageJson.dependencies.taquito },
            { package: npmModules.taquito.MICHELSON_ENCODER, version: this.indexerPackageJson.dependencies.taquitoMichelsonEncoder },
            { package: npmModules.BIGNUMBER, version: this.indexerPackageJson.dependencies.bignumber },
            { package: npmModules.TYPEORM, version: this.databasePackageJson.dependencies.typeorm },
            { package: 'sqlite3' }, // Peer dependency of typeorm -> must be installed after it to get correct version.

            { package: 'rimraf', isDev: true },
            { package: npmModules.TYPESCRIPT, isDev: true, version: this.generatorPackageJson.devDependencies.typescript },
        ]);
    }
}
