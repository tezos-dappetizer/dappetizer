import { errorToString, injectValue } from '@tezos-dappetizer/utils';
import nodeChildProcess, { ExecOptions } from 'child_process';
import { singleton } from 'tsyringe';

@singleton()
export class ChildProcessHelper {
    constructor(
        @injectValue(nodeChildProcess) private readonly childProcess: typeof nodeChildProcess,
    ) {}

    async exec(command: string, options: ExecOptions): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.childProcess.exec(command, options, (error, stdout, stderr) => {
                if (!error) {
                    resolve();
                } else {
                    const errorLines = [
                        `Failed to execute command: ${command}`,
                        `Working directory: ${options.cwd?.toString() ?? 'null'}`,
                        `Console standard output: ${stdout}`,
                        `Console error output: ${stderr}`,
                        errorToString(error),
                    ];
                    reject(new Error(errorLines.join('\n')));
                }
            });
        });
    }
}
