export const generatedPaths = Object.freeze({
    DIST: 'dist',
    NODE_MODULES: 'node_modules',
    SRC: 'src',

    DAPPETIZER_CONFIG_TS: 'dappetizer.config.ts',
    DOCKERFILE: 'Dockerfile',
    INDEX_TS: 'index.ts',
    PACKAGE_JSON: 'package.json',
    PACKAGE_LOCK_JSON: 'package-lock.json',
    TSCONFIG_JSON: 'tsconfig.json',
} as const);
