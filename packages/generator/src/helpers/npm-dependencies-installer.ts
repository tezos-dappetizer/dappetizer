import { injectLogger, Logger, fullCliOption, errorToString } from '@tezos-dappetizer/utils';
import { negate } from 'lodash';
import { singleton } from 'tsyringe';

import { ChildProcessHelper } from './child-process-helper';

export interface NpmDependency {
    readonly package: string;
    readonly isDev?: boolean;
    readonly version?: string;
}

@singleton()
export class NpmDependenciesInstaller {
    constructor(
        private readonly childProcess: ChildProcessHelper,
        @injectLogger(NpmDependenciesInstaller) private readonly logger: Logger,
    ) {}

    async install(workingDirPath: string, npmDependencies: readonly NpmDependency[]): Promise<void> {
        this.logger.logInformation(`Installing dependent NPM packages. This is controlled using '${fullCliOption('NPM_INSTALL')}' option.`);
        try {
            const installPackages = async (
                packagesDesc: string,
                packageFilter: (d: NpmDependency) => boolean,
                npmOptions: string,
            ): Promise<void> => {
                // The order of packages is significant if a package is a peer dependency of former package.
                const npmPackages = npmDependencies
                    .filter(packageFilter)
                    .map(d => (d.version ? `${d.package}@${d.version}` : d.package));

                this.logger.logInformation(`Installing ${packagesDesc} {npmPackages}.`, { npmPackages });
                await this.childProcess.exec(`npm install ${npmPackages.map(p => `"${p}"`).join(' ')} ${npmOptions}`, {
                    cwd: workingDirPath,
                });
            };

            await installPackages('app', negate(isDev), '--save');
            await installPackages('development', isDev, '--save-dev');

            this.logger.logInformation('Sucessfully installed all dependent NPM packages.');
        } catch (error) {
            throw new Error(`Failed installing NPM packages. You may try installing them manually and`
                + ` then run Dappetizer with '${fullCliOption('NPM_INSTALL', 'false')}' option.`
                + ` ${errorToString(error)}`);
        }
    }
}

function isDev(dependency: NpmDependency): boolean {
    return dependency.isDev === true;
}
