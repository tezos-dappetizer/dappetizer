import { Contract, CONTRACT_PROVIDER_DI_TOKEN, ContractProvider } from '@tezos-dappetizer/indexer';
import { fullCliOption, injectLogger, isWhiteSpace, Logger, pascalCase } from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

import { generatorDefaults } from './generator-defaults';

export interface ContractDetails {
    readonly name: string;
    readonly contract: Contract;
}

const namePurpose = 'naming generated classes related to specified {contract}';
const nameCliOptionDisclaimer = 'You can override it by specifying {cliOption} option.';
const cliOption = fullCliOption('CONTRACT_NAME');

@singleton()
export class ContractDetailsResolver {
    constructor(
        @inject(CONTRACT_PROVIDER_DI_TOKEN) private readonly contractProvider: ContractProvider,
        @injectLogger(ContractDetailsResolver) private readonly logger: Logger,
    ) {}

    async resolve(address: string, explicitName: string | null): Promise<ContractDetails> {
        const contract = await this.contractProvider.getContract(address);
        const name = await this.resolveName(contract, explicitName);
        return { contract, name };
    }

    private async resolveName(contract: Contract, explicitName: string | null): Promise<string> {
        if (!isWhiteSpace(explicitName)) {
            this.logger.logInformation(`Using specified {name} for ${namePurpose}.`, {
                contract: contract.address,
                name: explicitName,
            });
            return explicitName;
        }

        try {
            const { metadata } = await contract.abstraction.tzip16().getMetadata();

            if (isWhiteSpace(metadata.name)) {
                this.logger.logInformation(`Using {defaultName} for ${namePurpose} because no name found in contracts's TZIP-16 metadata.`
                    + ` ${nameCliOptionDisclaimer}`, {
                    contract: contract.address,
                    defaultName: generatorDefaults.CONTRACT_NAME,
                    cliOption,
                });
                return generatorDefaults.CONTRACT_NAME;
            }

            const name = pascalCase(metadata.name);
            this.logger.logInformation(`Using {name} from TZIP-16 metadata for ${namePurpose}. ${nameCliOptionDisclaimer}`, {
                name,
                contract: contract.address,
                cliOption,
            });
            return name;
        } catch (error) {
            this.logger.logWarning(`Failed to get contract's TZIP-16 metadata.`
                + ` Using {defaultName} instead for ${namePurpose}. ${nameCliOptionDisclaimer} {error}`, {
                contract: contract.address,
                defaultName: generatorDefaults.CONTRACT_NAME,
                cliOption,
                error,
            });
            return generatorDefaults.CONTRACT_NAME;
        }
    }
}
