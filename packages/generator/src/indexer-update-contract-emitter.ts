import { singleton } from 'tsyringe';

import { IndexerClassInterfacesGenerator } from './file-generators/contract-indexer/indexer-class-interfaces-generator';
import { FileEmitter } from './file-generators/file-emitter';
import { DappetizerNetworkConfigGenerator } from './file-generators/structural/dappetizer-network-config-generator';
import { ContractDetailsResolver } from './helpers/contract-details-resolver';
import { IndexerEmitter, UpdateContractIndexerClassOptions } from './indexer-emitter';

@singleton()
export class IndexerUpdateContractEmitter implements Pick<IndexerEmitter, 'updateContractIndexerClass'> {
    constructor(
        private readonly fileEmitter: FileEmitter,
        private readonly contractDetailsResolver: ContractDetailsResolver,
        private readonly indexerClassInterfacesGenerator: IndexerClassInterfacesGenerator,
        private readonly dappetizerNetworkConfigGenerator: DappetizerNetworkConfigGenerator,
    ) {}

    async updateContractIndexerClass(options: UpdateContractIndexerClassOptions): Promise<void> {
        const contractDetails = await this.contractDetailsResolver.resolve(options.contractAddress, options.contractName);

        await this.fileEmitter.emitFiles(options.outDirPath, { ...options, contractDetails }, [
            this.indexerClassInterfacesGenerator,
            this.dappetizerNetworkConfigGenerator,
        ]);
    }
}
