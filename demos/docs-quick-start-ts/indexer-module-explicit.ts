import { DbContext, IndexerModuleUsingDb } from '@tezos-dappetizer/database';
import { createContractIndexerFromDecorators, indexEntrypoint } from '@tezos-dappetizer/decorators';
import { TransactionIndexingContext } from '@tezos-dappetizer/indexer';
import { BigNumber } from 'bignumber.js';
import { DeepPartial } from 'typeorm';

import { Registration } from './entities';

interface BuyParameter {
    label: string;
    owner: string;
    duration: BigNumber;
}

class MyContractIndexer {
    @indexEntrypoint('buy')
    async indexBuy(buyParam: BuyParameter, dbContext: DbContext, indexingContext: TransactionIndexingContext): Promise<void> {
        const registration: DeepPartial<Registration> = {
            domain: Buffer.from(buyParam.label, 'hex') + '.tez',
            owner: buyParam.owner,
            duration: buyParam.duration.toNumber(),
            block: indexingContext.block.hash,
        };

        await dbContext.transaction.insert(Registration, registration);
    }
}

export const indexerModule: IndexerModuleUsingDb = {
    name: 'Registration',
    dbEntities: [Registration],
    contractIndexers: [createContractIndexerFromDecorators(new MyContractIndexer())],
};