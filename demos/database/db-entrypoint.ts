import { commonDbColumns, DbBlock } from '@tezos-dappetizer/database';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class DbEntrypoint {
    @PrimaryGeneratedColumn()
        id!: number;

    @Column()
        entrypointName!: string;

    @Column(commonDbColumns.address)
        contractAddress!: string;

    @Column(commonDbColumns.operationGroupHash)
        operationGroupHash!: string;

    @ManyToOne(() => DbBlock)
        block!: DbBlock;

    @Column({ ...commonDbColumns.blockHash, nullable: false })
        blockHash!: string;
}
