import { ContractIndexerUsingDb, DbContext } from '@tezos-dappetizer/database';
import { LazyContract, TransactionIndexingContext, TransactionParameter } from '@tezos-dappetizer/indexer';
import { Logger } from '@tezos-dappetizer/utils';

import { MyContextData } from './my-context-data';

export class MyContractIndexer implements ContractIndexerUsingDb<MyContextData> {
    constructor(
        private readonly logger: Logger,
    ) {}

    shouldIndex(_lazyContract: LazyContract): boolean {
        return true;
    }

    indexTransaction(
        transaction: TransactionParameter,
        _dbContext: DbContext,
        indexingContext: TransactionIndexingContext<MyContextData>,
    ): void {
        this.logger.logInformation('Found {entrypoint} of {contract}.', {
            entrypoint: transaction.entrypoint,
            contract: indexingContext.contract.address,
        });
        indexingContext.data.entrypoints.push({
            blockHash: indexingContext.block.hash,
            operationGroupHash: indexingContext.operationGroup.hash,
            contractAddress: indexingContext.contract.address,
            entrypointName: transaction.entrypoint,
        });
    }
}
