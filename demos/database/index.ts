import { IndexerModuleUsingDbFactory } from '@tezos-dappetizer/database';

import { DbEntrypoint } from './db-entrypoint';
import { MyContextData } from './my-context-data';
import { MyContractIndexer } from './my-contract-indexer';
import { MyIndexingCycleHandler } from './my-indexing-cycle-handler';

export const indexerModule: IndexerModuleUsingDbFactory<MyContextData> = options => {
    return {
        name: 'DatabaseDemo',
        dbEntities: [DbEntrypoint],
        indexingCycleHandler: new MyIndexingCycleHandler(),
        contractIndexers: [
            new MyContractIndexer(options.getLogger(MyContractIndexer)),
        ],
    };
};
