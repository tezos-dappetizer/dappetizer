import { config } from '../jest-config/unit-tests';

config.roots = ['<rootDir>'];
config.collectCoverageFrom = undefined;

export default config;
