import path from 'path';

import { fileOrDirExists, listDirEntries, readFileLines, writeFileText } from '../mocks';

describe('TypeScript API breaking changes', () => {
    const packageNames = getPackageNames();

    beforeAll(() => {
        for (const packageName of packageNames) {
            const exportedFiles = getExportedFilePaths(packageName);
            const exports = exportedFiles.flatMap(f => getExportStatements(readFileLines(f))).sort();
            const contents = [...disclaimerLines, ...exports].join('\n');

            writeFileText(getActualPublicApiFile(packageName), contents);
        }
    });

    it.each(packageNames)(`should not introduce breaking changes in package '%s'`, packageName => {
        const actualFilePath = getActualPublicApiFile(packageName);
        const expectedFilePath = getExpectedPublicApiFile(packageName);
        try {
            const actualLines = readFileLines(actualFilePath);
            const expectedLines = readFileLines(expectedFilePath);

            expect(actualLines).toEqual(expectedLines);
        } catch (error) {
            throw new Error(`Public API is broken!\n`
                + `Actual API file: ${actualFilePath}\n`
                + `Expected API file: ${expectedFilePath}\n`
                + `${error as string}`);
        }
    });

    it('there should NOT be unexpected packages', () => {
        const expectedFileNames = listDirEntries(path.dirname(getExpectedPublicApiFile('foo')));
        const actualFileNames = listDirEntries(path.dirname(getActualPublicApiFile('foo')));
        const unexpectedFileNames = expectedFileNames.filter(p => !actualFileNames.includes(p));

        expect(unexpectedFileNames).toBeEmpty();
    });
});

const disclaimerLines = [
    `// This is NOT a valid TypeScript with type definitions.`,
    `// This is a public API signature, sorted alphabetically, used to guard breaking changes.`,
    ``,
];

function getPackageNames(): string[] {
    const excludedPackages = ['cli', 'generator', 'taqueria'];
    return listDirEntries('packages').filter(p => !excludedPackages.includes(p));
}

function getActualPublicApiFile(packageName: string): string {
    return path.resolve(`.tmp-ts-public-api/${packageName}.d.ts`);
}

function getExpectedPublicApiFile(packageName: string): string {
    return path.resolve(`ts-public-api/${packageName}.d.ts`);
}

function getExportedFilePaths(packageName: string): string[] {
    const filePath = path.resolve(`packages/${packageName}/dist/types/public-api.d.ts`);
    const result: Set<string> = new Set();

    if (fileOrDirExists(filePath)) {
        addExportedFilePaths(filePath, result);
    }
    return Array.from(result.values());
}

const importedPathRegex = /'(?<path>(?:\.|\.\.)\/.*)'/u;

function addExportedFilePaths(filePath: string, result: Set<string>): void {
    if (result.has(filePath)) {
        return;
    }

    result.add(filePath);
    const lines = readFileLines(filePath);

    for (const line of lines) {
        const relImportPath = importedPathRegex.exec(line)?.groups?.path;
        if (relImportPath) {
            const importPath = path.resolve(path.dirname(filePath), `${relImportPath}.d.ts`);
            addExportedFilePaths(importPath, result);
        }
    }
}

const internalExportRegex = /(?<internal>\/\*\*(?:.*?) @internal (?:.*?)\*\/\s+export (?:.*?))(?:export |\/\*\*)/us;

function excludeInternalExports(lines: string[]): string[] {
    let str = lines.join('\n');
    let internalExport: string | undefined = '';

    while ((internalExport = internalExportRegex.exec(str)?.groups?.internal) !== undefined) {
        str = str.replace(internalExport, '');
    }
    return str.split('\n');
}

const excludedLinePrefixes = ['import ', 'export *', '/**', '*', '*/', '///'];

function getExportStatements(lines: string[]): string[] {
    const significantLines = excludeInternalExports(lines).filter(l => l && !excludedLinePrefixes.some(p => l.trimStart().startsWith(p)));
    return significantLines.join('\n').split('export ').filter(s => s !== '').map(s => `export ${s}`.trim());
}
