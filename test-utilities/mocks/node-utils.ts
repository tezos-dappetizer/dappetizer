import { randomBytes } from 'crypto';

export function getRandomDate(): Date {
    return new Date(Math.random() * 10_000_000_000_000);
}

export function getRandomString(): string {
    return randomBytes(20).toString('hex');
}

export async function sleep(millis: number): Promise<void> {
    return new Promise<void>(resolve => {
        setTimeout(() => resolve(), millis);
    });
}
