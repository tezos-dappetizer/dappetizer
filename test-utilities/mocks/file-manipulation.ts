import fs from 'fs';
import path from 'path';

export function readFileJson(filePath: string): any {
    const text = readFileText(filePath);
    return JSON.parse(text);
}

export function readFileLines(filePath: string): string[] {
    const text = readFileText(filePath);
    return splitToLines(text);
}

export function readFileText(filePath: string): string {
    return fs.readFileSync(filePath).toString();
}

export function writeFileJson(filePath: string, json: unknown): void {
    const text = JSON.stringify(json, undefined, 4);
    writeFileText(filePath, text);
}

export function writeFileText(filePath: string, text: string): void {
    ensureDirExists(path.dirname(filePath));
    fs.writeFileSync(filePath, text);
}

export function ensureDirExists(dirPath: string): void {
    if (!fs.existsSync(dirPath)) {
        fs.mkdirSync(dirPath, { recursive: true });
    }
}

export function deleteFileOrDir(fileOrDirPath: string): void {
    fs.rmSync(fileOrDirPath, { recursive: true, force: true });
}

export function listDirEntries(dirPath: string): string[] {
    return fs.readdirSync(dirPath);
}

export function fileOrDirExists(fileOrDirPath: string): boolean {
    return fs.existsSync(fileOrDirPath);
}

export function splitToLines(text: string): string[] {
    return text.split(/\r\n|\n|\r/u);
}
