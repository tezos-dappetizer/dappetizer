import { getRandomDate } from './node-utils';
import { Clock } from '../../packages/utils/src/time/clock';

export class TestClock implements Clock {
    readonly initialDate: Date;
    nowDate: Date;

    constructor(initialDate?: Date) {
        this.initialDate = initialDate ?? getRandomDate();
        this.nowDate = this.initialDate;
    }

    getNowDate(): Date {
        return this.nowDate;
    }

    getNowTimestamp(): number {
        return this.nowDate.getTime();
    }

    tick(millis?: number): void {
        this.nowDate = new Date(this.nowDate.getTime() + (millis ?? Math.random() * 1_000_000));
    }
}
