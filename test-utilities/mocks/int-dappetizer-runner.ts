import { npx } from 'node-npx';

import { CLI_SCRIPT } from '../../packages/utils/src/cli/cli-defaults';

export async function runDappetizer(args: string[], options?: { expectedExitCode?: number; cwd?: string }) {
    const childProcess = npx(CLI_SCRIPT, args, {
        stdio: 'pipe',
        cwd: options?.cwd,
    });

    childProcess.stdout!.pipe(process.stdout);
    childProcess.stderr!.pipe(process.stderr);

    // Remove handlers added by node-npx b/c they exit the process -> break test output.
    childProcess.removeAllListeners('close');
    childProcess.removeAllListeners('error');

    await new Promise<void>((resolve, reject) => {
        childProcess.on('close', exitCode => {
            const expectedExitCode = options?.expectedExitCode ?? 0;
            if (exitCode === expectedExitCode) {
                resolve();
            } else {
                reject(new Error(`Expected 'dappetizer' to exit with ${expectedExitCode} but the exit code is ${exitCode}.`));
            }
        });
        childProcess.on('error', error => {
            reject(error);
        });
    });
}
