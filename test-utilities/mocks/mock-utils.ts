export function throwShouldNotBeCalled(): never {
    throw new Error('This method (see stack trace) should NOT have been called.');
}
