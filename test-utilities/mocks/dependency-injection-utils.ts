import { DependencyContainer, InjectionToken } from 'tsyringe';

import { DB_INITIALIZERS_DI_TOKEN } from '../../packages/database/src/db-access/db-initializer-di-token';
import {
    PARTIAL_INDEXER_MODULE_VERIFIERS_DI_TOKEN,
} from '../../packages/indexer/src/client-indexers/verification/partial-indexer-module-verifier-di-token';
import { BACKGROUND_WORKERS_DI_TOKEN } from '../../packages/utils/src/app-control/background-worker-di-token';
import { Constructor } from '../../packages/utils/src/basics/reflection-types';
import { CONFIGS_WITH_DIAGNOSTIC_DI_TOKEN } from '../../packages/utils/src/configuration/config-diagnostics-di-token';
import { HEALTH_CHECKS_DI_TOKEN } from '../../packages/utils/src/health/health-check-di-token';
import { HTTP_HANDLERS_DI_TOKEN } from '../../packages/utils/src/http-server/http-handler-di-token';
import { METRICS_UPDATERS_DI_TOKEN } from '../../packages/utils/src/metrics/metrics-updater-di-token';

export function shouldResolvePublicApi(
    getDiContainer: () => DependencyContainer,
    testCases: (InjectionToken<unknown> | [InjectionToken<unknown>, 'frozen'])[],
): void {
    describe('should resolve from public API', () => {
        for (const testCase of testCases) {
            const [diToken, expectedFrozen] = Array.isArray(testCase) ? testCase : [testCase, ''];
            const testName = typeof diToken === 'function' ? diToken.name : diToken as string;

            it(testName, () => {
                const value = getDiContainer().resolve(diToken);

                if (expectedFrozen) {
                    expect(value).toBeFrozen();
                }
            });
        }
    });
}

export function shouldResolveImplsForDappetizerFeatures(
    getDiContainer: () => DependencyContainer,
    expected: {
        backgroundWorkerNames?: string[];
        configsWithDiagnostics?: Constructor[];
        dbInitializers?: Constructor[];
        healthCheckNames?: string[];
        httpHandlerUrlPaths?: string[];
        metricsUpdaters?: Constructor[];
        partialIndexerModuleVerifiers?: Constructor[];
    },
): void {
    describe('should resolve impls for Dappetizer features', () => {
        shouldResolve(BACKGROUND_WORKERS_DI_TOKEN, w => w.name, expected.backgroundWorkerNames);

        shouldResolve(CONFIGS_WITH_DIAGNOSTIC_DI_TOKEN, w => w.constructor, expected.configsWithDiagnostics);

        shouldResolve(DB_INITIALIZERS_DI_TOKEN, w => w.constructor, expected.dbInitializers);

        shouldResolve(HEALTH_CHECKS_DI_TOKEN, w => w.name, expected.healthCheckNames);

        shouldResolve(HTTP_HANDLERS_DI_TOKEN, w => w.urlPath, expected.httpHandlerUrlPaths);

        shouldResolve(METRICS_UPDATERS_DI_TOKEN, w => w.constructor, expected.metricsUpdaters);

        shouldResolve(PARTIAL_INDEXER_MODULE_VERIFIERS_DI_TOKEN, w => w.constructor, expected.partialIndexerModuleVerifiers);
    });

    function shouldResolve<TResolved, TCompared>(
        diToken: InjectionToken<TResolved>,
        getValueToCompare: (r: TResolved) => TCompared,
        expectedValues: TCompared[] | undefined,
    ) {
        if (expectedValues) {
            it(diToken as string, () => {
                const items = getDiContainer().resolveAll(diToken);

                expect(items.map(getValueToCompare)).toIncludeAllMembers(expectedValues);
            });
        }
    }
}
