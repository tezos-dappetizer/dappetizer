// Import of 'ts-mockito/lib/Mock' must be below import 'ts-mockito'. See https://github.com/NagRock/ts-mockito/issues/163
import { anything, capture, instance, verify } from 'ts-mockito';
import { Mocker } from 'ts-mockito/lib/Mock';

export function nullishInstance<T>(mockedValue: NonNullable<T> | null): NonNullable<T> | null {
    return mockedValue !== null ? instance(mockedValue) : mockedValue;
}

// See https://github.com/NagRock/ts-mockito/issues/163
/* eslint-disable */
export function fixedMock<T>(clazz?: (new(...args: any[]) => T) | (Function & { prototype: T })): T {
    const mocker = new Mocker(clazz);
    mocker['excludedPropertyNames'] = ['hasOwnProperty', 'then'];
    return mocker.getMock();
}

interface CalledVerifier<TArgs extends any[]> {
    // Direct validation:
    never(): void;
    once(): void;
    onceWith(...args: TArgs): void;

    // Fluent validation:
    with(...args: TArgs): this;
    thenWith(...args: TArgs): this;
    thenNoMore(): void;

    // Regular validation:
    times(count: number): this;
    atIndexWith(index: number, ...args: TArgs): this;
}

/**
 * Extracts args and compares it which gives much nicer error
 * then `verify(method(deepEqual(...)))` from *ts-mockito* which just does not match any executed call.
 * Also, it supports fluent API.
 */
export function verifyCalled<TArg>(
    method: (a: TArg) => unknown,
    options?: { argCount: number }, // Sometimes needed for mocked interfaces.
): CalledVerifier<[TArg]>
export function verifyCalled<TArg1, TArg2>(
    method: (a1: TArg1, a2: TArg2) => unknown,
    options?: { argCount: number }, // Sometimes needed for mocked interfaces.
): CalledVerifier<[TArg1, TArg2]>
export function verifyCalled<TArg1, TArg2, TArg3>(
    method: (a1: TArg1, a2: TArg2, a3: TArg3) => unknown,
    options?: { argCount: number }, // Sometimes needed for mocked interfaces.
): CalledVerifier<[TArg1, TArg2, TArg3]>
export function verifyCalled<TArg1, TArg2, TArg3, TArg4>(
    method: (a1: TArg1, a2: TArg2, a3: TArg3, a4: TArg4) => unknown,
    options?: { argCount: number }, // Sometimes needed for mocked interfaces.
): CalledVerifier<[TArg1, TArg2, TArg3, TArg3]>
export function verifyCalled(method: (...a: any[]) => unknown, options?: { argCount: number }) {
    let argCount = options?.argCount ?? method.length;
    let callIndex = 0;
    return {
        times(count: number) {
            const anythingArgs = Array(argCount).fill(anything());
            verify(method(...anythingArgs)).times(count);
            return this;
        },
        atIndexWith(index: number, ...expectedArgs: unknown[]) {
            argCount = expectedArgs.length;
            const actualArgs = capture(method).byCallIndex(index);
            expect(actualArgs).toMatchObject(expectedArgs);
            return this;
        },

        never() {
            this.times(0);
        },
        once() {
            this.times(1);
        },
        onceWith(...expectedArgs: unknown[]) {
            this.atIndexWith(0, ...expectedArgs).times(1);
        },

        with(...expectedArgs: unknown[]) {
            return this.atIndexWith(callIndex++, ...expectedArgs);
        },
        thenWith(...expectedArgs: unknown[]) {
            return this.with(...expectedArgs);
        },
        thenNoMore() {
            this.times(callIndex);
        },
    };
}
