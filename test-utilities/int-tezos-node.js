/* eslint-disable */

module.exports.tezosNodeUrlForIntegrationTests
    = process.env.TEZOS_NODE_URL
    || 'https://prod.tcinfra.net/rpc/mainnet/';
