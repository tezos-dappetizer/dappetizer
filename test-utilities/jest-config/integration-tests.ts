import { config } from './unit-tests';

config.roots = ['<rootDir>/integration-tests'];
config.collectCoverage = false;
config.testTimeout = 10 * 60 * 1_000;

export { config };
