# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [2.17.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.16.1...v2.17.0) (2024-06-20)


### Bug Fixes

* **indexer**: Indexer fails if `BalanceUpdateStakingDelegatorNumerator.delegatorContractAddress` is not `KT1` address but `tz1` address. ([38c00fb](https://gitlab.com/tezos-dappetizer/dappetizer/commit/38c00fb0c23cfb32c152cbdf96c658e6a41bc4c0))


### Features

* **indexer:** Removed strict checks for address prefixes when address is not used further e.g. to load contract details. ([38c00fb](https://gitlab.com/tezos-dappetizer/dappetizer/commit/38c00fb0c23cfb32c152cbdf96c658e6a41bc4c0))
* Logging Dappetizer and indexer module versions on startup. ([d2f2bb8](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d2f2bb8a81030160fde77b7fe38b2b6ec20d8aaf))
* Upgraded to [@taquito 20.0.1](https://github.com/ecadlabs/taquito/releases/tag/20.0.1). ([8fb7091](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8fb709188e1580164d7ddfee1a6afaa058d3aa36))






## [2.16.1](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.16.0...v2.16.1) (2024-06-11)


### Bug Fixes

* **indexer:** Fixed handling of smart rollup in `source` or `destination` of an internal operation. API is also adapted accordingly. ([01b8595](https://gitlab.com/tezos-dappetizer/dappetizer/commit/01b859524ad29a6c4ddd222ef9e55839698d1765))






# [2.16.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.15.0...v2.16.0) (2024-06-01)


### Features

* **indexer:** Partially upgraded to Paris protocol: ([ce1698a](https://gitlab.com/tezos-dappetizer/dappetizer/commit/ce1698a1cebe7eb35d31f6824f82447a854a8787))
	* Added `BalanceUpdateBakerEdge`. Existing `BalanceUpdateBaker` is used for both legacy baker and baker with own stake.
	* Deprecated endorsements as far as they were incomplete and with breaking changes. Let us know if you need them.	
	* **NOTE:** Balance updates from unsupported operations (e.g. endorsements or new operations from Oxford and Paris) are not being indexed despite their API exists.
* **indexer:** Not logging a warning anymore if an operation, a big map diff or a balance update is not supported by Dappetizer. Instead, a debug message is logged. ([cbb793b](https://gitlab.com/tezos-dappetizer/dappetizer/commit/cbb793bda3d82ff6af9bcd3b5c1930e3e2196837))
* Changed minimum required Node.js to 18 as far as taquito requires it too. ([1f673a9](https://gitlab.com/tezos-dappetizer/dappetizer/commit/1f673a9c76c7e9bb8048f1dac471c43ef6c60f3d))
* Upgraded to [@taquito 20.0.0](https://github.com/ecadlabs/taquito/releases/tag/20.0.0).






# [2.15.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.14.1...v2.15.0) (2024-02-06)


### Bug Fixes

* **indexer:** Fixed an error if `SmartRollupAddMessagesOperation.message` contains an empty string. ([481a697](https://gitlab.com/tezos-dappetizer/dappetizer/commit/481a697e79d450132bd723ff761cc31c9f9f07d7))


### Features

* **indexer:** Partially upgraded to Oxford protocol: ([060f88a](https://gitlab.com/tezos-dappetizer/dappetizer/commit/060f88a0dc6dc64d663066988906b7d040cb662b))
    * Adapted `BalanceUpdate` accordingly.
    * Removed types related to transactional rollups (TX rollups) as far as they are removed from Tezos RPC schema and have not beed used at all.
    * Some other minor changes according to `@taquito`.
* Upgraded to [@taquito 19.0.0](https://github.com/ecadlabs/taquito/releases/tag/19.0.0).






## [2.14.1](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.14.0...v2.14.1) (2024-01-24)


### Bug Fixes

* **indexer:** Fixed failing indexing if `TransferTicketOperation.destination` is not `KT1` address. This was changed in one of last protocols. ([0100a80](https://gitlab.com/tezos-dappetizer/dappetizer/commit/0100a80bb28179b867c2672ec1509dce32ea31d8))






# [2.14.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.13.0...v2.14.0) (2023-10-03)


### Features

* Added support for re-indexing explicitly specified blocks: ([10a7fe8](https://gitlab.com/tezos-dappetizer/dappetizer/commit/10a7fe864755407be351a397ef5653fdf40fdecd))
    * It is useful to fix incorrect data from the past without re-indexing entire blockchain.
    * Blocks can be specified using `--blocks` CLI option as list or levels or level ranges e.g. `--blocks 5,7-10,18` will indexed blocks with level 5, 7, 8, 9, 10 and 18.
    * Specified blocks are re-indexed instead of regular indexing based on Dappetizer configuration file.
    * Once specified blocks are re-indexed, then the app shuts down.
    * Old existing blocks (before re-indexing them) are rolled back using `IndexingCycleHandler.rollBackOnReorganization(...)` (the method will be renamed in the next major release).
* **indexer:** Added `debug` logging before and after client indexers are executed so that it is easier to diagnose the indexing. ([a930310](https://gitlab.com/tezos-dappetizer/dappetizer/commit/a93031021b5d447e05027cd85cdd1453467b7335))
* **taqueria:** Upgraded `@taqueria/node-sdk` to 0.40.0. ([6c1c389](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6c1c389858a7f1e4e1d2816a10666496d2ef65cd))






# [2.13.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.12.0...v2.13.0) (2023-09-08)


### Bug Fixes

* Upgraded NPM dependencies in order to fix NPM vulnerabilities during `npm install`. ([04d0c37](https://gitlab.com/tezos-dappetizer/dappetizer/commit/04d0c37a5ea1b2e12caca25c73f03381b843d246))


### Features

* **indexer:** Added `dbContext` as the second paramter to `shouldIndex(...)` method of `ContractIndexer` and `SmartRollupIndexer`. Note: It must execute only readonly queries so that it does not interfere with block indexing. ([2906c43](https://gitlab.com/tezos-dappetizer/dappetizer/commit/2906c4334e0a29493d0774011fa535b9a85f2ce7))
* **taqueria:** Upgraded `@taqueria/node-sdk` to 0.39.0. ([7432a4e](https://gitlab.com/tezos-dappetizer/dappetizer/commit/7432a4e14421b3f3427385f0aa8b9418d18c85b3))
* Upgraded to [@taquito 17.3.0](https://github.com/ecadlabs/taquito/releases/tag/17.3.0). ([6c788e8](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6c788e87031809eb81093fad9c55503302b61300))






## [2.12.1](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.12.0...v2.12.1) (2023-08-01)


### Bug Fixes

* Upgraded NPM dependencies in order to fix NPM vulnerabilities during `npm install`. ([04d0c37](https://gitlab.com/tezos-dappetizer/dappetizer/commit/04d0c37a5ea1b2e12caca25c73f03381b843d246))






# [2.12.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.11.0...v2.12.0) (2023-07-23)


### Bug Fixes

* **indexer:** `BlockIndexing` health check was not showing the error if it occurred just after app startup especially when Tezos node has an outage. ([3501766](https://gitlab.com/tezos-dappetizer/dappetizer/commit/3501766888cda91c7cf19ac4f9c088ccd6878444))


### Features

* **indexer:** Throwing descriptive error if a block is missing `metadata` when Tezos node is not in archive mode and old blocks are indexed. ([9c1b07e](https://gitlab.com/tezos-dappetizer/dappetizer/commit/9c1b07ebfa51065c477e816c76cf2496bef6983e))
* **utils:** Logging responses from `/check` and `/health` endpoints. ([28cab96](https://gitlab.com/tezos-dappetizer/dappetizer/commit/28cab96cd5c501dc4aaed1e559b3bf62134e11c0))
* Upgraded to `tsyringe` 4.8.0 ([a3fb46d](https://gitlab.com/tezos-dappetizer/dappetizer/commit/a3fb46dd0332b62fcc539afdeaf2cdc393b41f40))






# [2.11.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.10.0...v2.11.0) (2023-06-17)


### Features

* **indexer:** Upgraded to protocol 17 Nairobi: added `AppliedSmartRollupCementResult.commitmentHash`. ([807cd2d](https://gitlab.com/tezos-dappetizer/dappetizer/commit/807cd2d7107b3622bbd31bcd02583c66e8709277))
* **database:** Using [pluralize](https://www.npmjs.com/package/pluralize) for pluralizing Hasura GraphQL names instead of our simple function. ([cb659d8](https://gitlab.com/tezos-dappetizer/dappetizer/commit/cb659d843ce703e70dace7f855e57ecfb4275ebd))
* Upgraded to [@taquito 17.0.0](https://github.com/ecadlabs/taquito/releases/tag/17.0.0).






# [2.10.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.9.0...v2.10.0) (2023-06-05)


### BREAKING CHANGES

* Tezos type `option` used to be deserialized to bare value like `TValue | null`, now it is `{ Some: TValue } | null`. This is due to upgrade of `@taquito` to 16.2.0 (fresh Dappetizer projects would get it anyway) which brings [this breaking change](https://github.com/ecadlabs/taquito/releases/tag/16.2.0).

### Bug Fixes

* **generator:** Fixed generated TypeScript type for Tezos type `option` to be an object like `{ Some: TValue } | null` according to the breaking change in `@taquito`. ([692694b](https://gitlab.com/tezos-dappetizer/dappetizer/commit/692694bde37864a89fa71ffdd760cc9450b0c611))
* **indexer:** Fixed Dappetizer failing to index block if there is a storage change of the contract which was just originated within the block. Also, `StorageChange.oldValue` was incorrect if there were multiple changes within the block. **BEWARE** now big maps may be represented differently: initial storage from origination usually contains `MichelsonMap` vs. raw big map ID later. ([0c8de67](https://gitlab.com/tezos-dappetizer/dappetizer/commit/0c8de67008d4a7e0b84abcae7130fe2986032895))


### Features

* **database:** Added `typeorm` as a peer dependency in order to share single package instance. ([1306424](https://gitlab.com/tezos-dappetizer/dappetizer/commit/130642485a85ef9a13e71d7ee00b5d79aac3bcc6))
* **database:** Improved Hasura naming: ([475ac32](https://gitlab.com/tezos-dappetizer/dappetizer/commit/475ac32b85b65aa7c2f495112afb7fcd6d64d532))
    * Added configurable optional `graphQLPluralFieldName` and `graphQLAggregateFieldName` for `customMappings` and `autotrackedMappings`.
    * Improved automatic pluralization e.g. `item` -> `items`, `address` -> `addresses`, `entity` -> `entities`.
* Upgraded `@taquito` to 16.2.0.






# [2.9.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.8.0...v2.9.0) (2023-05-30)


### Bug Fixes

* **utils:** Fixed typing of `hasLength(...)` for read-only arrays. ([eea9ce9](https://gitlab.com/tezos-dappetizer/dappetizer/commit/eea9ce9d8cd074225a6273aa197ba4dafbbb0e94))


### Features

* **indexer:** Added `StorageChange.oldValue`. ([091470f](https://gitlab.com/tezos-dappetizer/dappetizer/commit/091470f873ce9b4865bf8d4f9aa32caaf32435cb))






# [2.8.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.7.1...v2.8.0) (2023-05-26)


### Features

* **indexer:** Exposed `SelectiveIndexingConfig` so that users can check if selective indexing is enabled and what contracts are selected. ([602b878](https://gitlab.com/tezos-dappetizer/dappetizer/commit/602b878820dd8e42f624c6f3b3d4edc2bd7716b6))
* **indexer:** Removed validation of addresses within RPC block to start with one of known prefixes in order to be forward compatible. ([6cabe1f](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6cabe1f391ecd377df71952eaa0c61f6251737b3))
* **taqueria:** Upgraded to `@taqueria/node-sdk` 0.28.6. ([370b42a](https://gitlab.com/tezos-dappetizer/dappetizer/commit/370b42a0d93655a9b16cf194e88bda52e268d9e5))






## [2.7.1](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.7.0...v2.7.1) (2023-05-10)


### Bug Fixes

* Transaction parameter with `sr1` address in `destination` causes the whole block to be rejected. ([52a7e7d](https://gitlab.com/tezos-dappetizer/dappetizer/commit/52a7e7d2046c0b799fbd42eb9e337973c2029031))






# [2.7.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.6.0...v2.7.0) (2023-04-12)


### Bug Fixes

* **generator:** Added `package-lock.json` to generated `Dockerfile` to fix NPM install. ([a47f4d0](https://gitlab.com/tezos-dappetizer/dappetizer/commit/a47f4d043c7187b94f8008bb5bfa55765e17d1b8))


### Features

* **decorators:** Added `createSmartRollupIndexerFromDecorators(...)` for creating `SmartRollupIndexer` from an arbitrary object with at least one of following decorators: `@indexAddMessages(...)`, `@indexCement(...)`, `@indexExecuteOutboxMessage(...)`, `@indexOriginate(...)`, `@indexPublish(...)`, `@indexRecoverBond(...)`, `@indexRefute(...)`, `@indexTimeout(...)`, `@shouldIndex(...)`, `@smartRollupFilter(...)`. ([92a860a](https://gitlab.com/tezos-dappetizer/dappetizer/commit/92a860ae3938a7fb72cc34b8be7c4753c481d6cf))
* **indexer:** Added `SmartRollupIndexer<TDbContext, TContextData, TRollupData>` (also corresponding `SmartRollupIndexerUsingDb`) for indexing smart rollups which is similar to `ContractIndexer`. ([5f2c528](https://gitlab.com/tezos-dappetizer/dappetizer/commit/5f2c528646fe1e80895a3a9b2e1cfc1b2bedeb5c))
* **indexer:** Added base union `OperationResult<TApplied>`. ([c2ce2cd](https://gitlab.com/tezos-dappetizer/dappetizer/commit/c2ce2cda5cbf76f013e4fce5e638d94b56e2b9f2))
* **indexer:** Optimized memory usage by disposing `Contract` objects which were not used in recent blocks. This can be configured using `IndexingDappetizerConfig.contractCacheMillis`. ([f29d6c6](https://gitlab.com/tezos-dappetizer/dappetizer/commit/f29d6c6b28a6425c55949f7e6e084c45dbe1e857))
* Upgraded to `@taquito` 16.1.1. ([a142366](https://gitlab.com/tezos-dappetizer/dappetizer/commit/a14236640dc964f51d9161d7fc9bd44f12b7533d))
* **utils:** Added `constructor` validation to `argGuard.object(...)`. ([ce62476](https://gitlab.com/tezos-dappetizer/dappetizer/commit/ce6247672a7fa5fea2c6ef10a25be60c091975a7))






# [2.6.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.5.0...v2.6.0) (2023-04-02)


### Bug Fixes

* **database:** Fixed Hasura initialization if db entites are circular. ([8899089](https://gitlab.com/tezos-dappetizer/dappetizer/commit/889908988a36edc893785c3e957b2978532305b8))
* **database:** Set default value of `IndexerModuleUsingDbFactory.TContextData` to `unknown` to make it consistent with other interfaces. ([0f8d0cc](https://gitlab.com/tezos-dappetizer/dappetizer/commit/0f8d0cc3e0bffa2a55212e45ca7841f0b451e7e5))
* Fixed build issue that DOM library gets included if the indexer is generated within the directory that references DOM via `node_modules/[@types](https://gitlab.com/types)` e.g. Taqueria demos are such directories. ([d8ac8a2](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d8ac8a22aef8384a5d8774e6600c5756ee711ad1))


### Features

* **indexer:** Upgraded to protocol 16 Mumbai with `@experimental` support: ([0c2382a](https://gitlab.com/tezos-dappetizer/dappetizer/commit/0c2382aad69d1d29d12939bee47664a2df6ed7ad))
    * Added new operations: `SmartRollupAddMessagesOperation`, `SmartRollupCementOperation`, `SmartRollupExecuteOutboxMessageOperation`, `SmartRollupOriginateOperation`, `SmartRollupPublishOperation`, `SmartRollupRecoverBondOperation`, `SmartRollupRefuteOperation`, `SmartRollupTimeoutOperation`.
    * Added properties with ticket updates: `AppliedTransactionResult.ticketUpdates`, `AppliedTransactionResult.ticketReceipts`, `AppliedTransferTicketResult.ticketUpdates`.
    * Deprecated `Ticket` in favour of `TicketToken`.
    * Replaced value `scr1` of `AddressPrefix` with final `sr1`.
* Specified Node.js 16 as minimum engine as far as that is the minimum for `@taquito` too. ([1e11fcb](https://gitlab.com/tezos-dappetizer/dappetizer/commit/1e11fcbc4c5f3301b7edafb91dc2b5d66c7d2be6))
* Using `AbortController` and `AbortSignal` from Node.js. Removed dependency to `abort-controller` polyfill. ([f87cd6e](https://gitlab.com/tezos-dappetizer/dappetizer/commit/f87cd6ef78ac307fc46676d5a0be952847cf4e2c))
* **taqueria:** Upgraded `@taqueria` to 0.28.4. ([c64edfa](https://gitlab.com/tezos-dappetizer/dappetizer/commit/c64edfa4c35103f7c0d20fb28645825d6e16c8e8))






# [2.5.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.4.0...v2.5.0) (2023-03-29)


### Features

* **utils:** Reworked Tezos node RPC health check to turn `Degraded` firstly, then `Unhealthy` after configured time. ([353deaa](https://gitlab.com/tezos-dappetizer/dappetizer/commit/353deaa699609e5271e2732e45e3e8d3df8fef3c))






# [2.4.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.3.0...v2.4.0) (2023-03-28)


### Features

* **indexer:** Improved error details if block from RPC is not well-formed block according to schema. ([c53bd68](https://gitlab.com/tezos-dappetizer/dappetizer/commit/c53bd68e49fe70674d8652e9f64f5e4633ea33cc))
* Upgraded to `@taquito` 16.0.0. ([ec19722](https://gitlab.com/tezos-dappetizer/dappetizer/commit/ec1972261f0ddd98acec483364f8b00c4c37406e))
* **utils:** Added timeout for RPC requests - `TezosNodeDappetizerConfig.timeoutMillis` with default `30_000` and `TezosNodeDappetizerConfig.health.timeoutMillis` with default `1_000`. ([f114445](https://gitlab.com/tezos-dappetizer/dappetizer/commit/f114445af7746c27b1f522b270541acfe7536515))






# [2.3.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.2.1...v2.3.0) (2023-03-01)


### Features

* **indexer:** Upgraded to protocol 15 Lima: Added `DrainDelegateOperation`, `UpdateConsensusKeyOperation`. ([57a3322](https://gitlab.com/tezos-dappetizer/dappetizer/commit/57a3322b62f58cb9edd9d62d293292938e8f6280))
* **generator:** Support for `ticket_deprecated` type. ([57a3322](https://gitlab.com/tezos-dappetizer/dappetizer/commit/57a3322b62f58cb9edd9d62d293292938e8f6280))
* **taqueria:** Upgraded `@taqueria/node-sdk` to 0.28.0. ([6fa2ad1](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6fa2ad16d499fb7f4025295c2e6021a2ce134dd6))
* Upgraded to `@taquito` 15.1.0.


### Reverts

* Revert "test(indexer): Increased number of tested blocks in regression tests." ([5855762](https://gitlab.com/tezos-dappetizer/dappetizer/commit/5855762422bee482c5b0b04dbfe5395626a01e82))






## [2.2.1](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.2.0...v2.2.1) (2022-12-06)


### Bug Fixes

* **generator:** Installation of NPM packages fails because of incorrectly quoted package name and version. ([0fd7530](https://gitlab.com/tezos-dappetizer/dappetizer/commit/0fd753002d523bc59ea3ca7fd178fd8aa715a6f3))
* **utils:** Fixed `InvalidRpcResponseError` if Tezos node RPC uses `brotli` compression e.g. via Cloudflare. This is also the case of `prod.tcinfra.net` which is used as a default by Dappetizer. ([12effa7](https://gitlab.com/tezos-dappetizer/dappetizer/commit/12effa7197d9e55de441b7e8120efefbd58faf2b))






# [2.2.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.1.0...v2.2.0) (2022-11-17)


### Bug Fixes

* **generator:** Excluded `DOM` lib (by explictly including only `ES2020`) in `tsconfig.json` as far as generate app does not run in browser. ([7bb4388](https://gitlab.com/tezos-dappetizer/dappetizer/commit/7bb4388af4c37033795b1b9fd40b98fe63d98c98))


### Features

* **indexer:** Completed upgrade to protocol 14 Kathmandu: ([1c0dc9a](https://gitlab.com/tezos-dappetizer/dappetizer/commit/1c0dc9a9b002c9417b9151d5341f3ab4b7b5b651))
    * Added remaining operations: `EventInternalOperation`, `IncreasePaidStorageOperation`, `VdfRevelationOperation`.
    * Added `ContractIndexer.indexEvent(...)` for indexing events related to particular contract.
* **indexer:** Added `Contract.events` with info about events that contract can emit. ([67fa9fc](https://gitlab.com/tezos-dappetizer/dappetizer/commit/67fa9fc6a2adc601745541c9f37497911ab7486d))
* **indexer:** Added `InternalOperation.sourceContract` and deprecated `InternalOperation.sourceAddress`. ([4726813](https://gitlab.com/tezos-dappetizer/dappetizer/commit/47268134230ed6a2c3445132731ddc0d46f67959))
* **indexer:** Changed `ContractIndexer.indexOrigination(...)` to accept more specific `ContractOrigination = Applied<OriginationOperation | OriginationInternalOperation>` instead of `OriginationOperation | OriginationInternalOperation`. ([efc039d](https://gitlab.com/tezos-dappetizer/dappetizer/commit/efc039d64c031037a8e391d4cb0379e9222686a3))
* **decorators:** Added `@indexEvent(tag)` contract indexer decorator. ([323800c](https://gitlab.com/tezos-dappetizer/dappetizer/commit/323800c0b935d029909bb6b251845dbe3c94c9c3))
* **generator:** Generating index methods for contract events. ([26ac000](https://gitlab.com/tezos-dappetizer/dappetizer/commit/26ac000bf334a1209a8dfe75fd4030c677aa083f))
* **taqueria:** Upgraded to `@taqueria` 0.24.1. ([7fe77c7](https://gitlab.com/tezos-dappetizer/dappetizer/commit/7fe77c71ad62c879f922de984e7236c8ce0d0852))






# [2.1.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v2.0.0...v2.1.0) (2022-11-07)


### Features

* **cli:** Informing user that usage statistics are enabled when `dappetizer init` script is executed. ([a558e14](https://gitlab.com/tezos-dappetizer/dappetizer/commit/a558e142c16d448c832596de48c71b3020e4094f))
* **cli:** Changed default letter casing for CLI options to be kebab-case e.g. `--tezos-endpoint`. Still both kebab and camel cases are supported. ([bac4bca](https://gitlab.com/tezos-dappetizer/dappetizer/commit/bac4bca08ffbe17c3a0e61e2cf6ac3fd0d9b7682))
* **indexer:** Added support for `unparsed-binary` in transaction parameter - if present, then `transactionParameter` is `null` and only raw RPC data is available. ([dbbf60f](https://gitlab.com/tezos-dappetizer/dappetizer/commit/dbbf60f6f7b09c838ae0eb3040daf4b82f4b57b9))
* **indexer:** Bootstrap protocols used for first two blocks on a chain are no longer considered as unsupported. This allows indexing from block 0 when running against local sandbox or ghostnet. ([bd9d553](https://gitlab.com/tezos-dappetizer/dappetizer/commit/bd9d553dbb6ac3d0ddd9e0ce26293b9334d2861e))
* **taqueria:** Adapted command names to be consistent with other Taqueria plugins: `taq create indexer`, `taq build-indexer`, `taq start-indexer`, `taq update-indexer`. ([7bd49e9](https://gitlab.com/tezos-dappetizer/dappetizer/commit/7bd49e9855eb583e84493cb02f39b0324276bca4))
* **taqueria:** Upgraded to `@taqueria` 0.22.2. ([e4988fe](https://gitlab.com/tezos-dappetizer/dappetizer/commit/e4988fea64079eba7635868bc3f530637d313d8f))
* **utils:** Improved error message if the config is invalid especially if the section for the specified network is missing. ([11b8652](https://gitlab.com/tezos-dappetizer/dappetizer/commit/11b86520975c4b6cc208a7d17df49308f4f7892d))






# [2.0.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v1.3.1...v2.0.0) (2022-10-03)


### Bug Fixes

* **generator:** If both `--tezosEndpoint` and `--network` are specified, then `--network` does not have to be one of built-in networks anymore. ([fa58377](https://gitlab.com/tezos-dappetizer/dappetizer/commit/fa58377937260fa4bbd9a2d3ad9e8df0592c3490))
* **indexer:** Indexing fails if the `script.code` of an origination contains a global constant. It is fixed by removing entire `ContractScript` and adding `AppliedOriginationResult.getInitialStorage()`. ([7c85e43](https://gitlab.com/tezos-dappetizer/dappetizer/commit/7c85e43d5cf5f217ce7255e1f54ec6abbe8428fb))


### Features

* **indexer:** Support for protocol 13 *Jakarta* : ([d33ccbe](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d33ccbe06ac52831b303e5ec084eadb06ac6442b))
    * Added new operation types: `TransferTicket`, `TxRollupCommit`, `TxRollupDispatchTickets`, `TxRollupFinalizeCommitment`, `TxRollupOrigination`, `TxRollupRejection`, `TxRollupRemoveCommitment`, `TxRollupReturnBond`, `TxRollupSubmitBatch`.
    * `BigMapDiff`-s are retrieved preferebly from `lazy_storage_diff` property instead of `big_map_diff` because `big_map_diff` was removed.
    * Added new `BalanceUpdateCategory` values: `Bonds`, `TxRollupRejectionRewards`, `TxRollupRejectionPunishments`.
    * Expanded `BalanceUpdate` union to more specific types corresponding exactly to RPC schema.
    * Added property `ticketHash` to `AppliedTransactionResult` and `BacktrackedTransactionResult`.
    * Support for new `trx1` rollup addresses.
    * Changed attribute in `uid`-s to be in brackets e.g. `block/foo/bar[transaction]` instead of underscores e.g. `block/foo/bar_transaction`.
* **indexer:** Partial support for protocol 14 *Kathmandu*: ([941dae6](https://gitlab.com/tezos-dappetizer/dappetizer/commit/941dae65644ee5ce0a37afb2de9fb6b0be1f7017))
    * Property `consumed_gas` on operation results is filled from `consumed_milligas` as far as it was removed in the protocol.
    * Support for smart contract rollup addresses with prefix `scr1`.
    * Adapted `BalanceUpdateFreezerBonds` to support both `TX` and `SC` bond rollups.
* **indexer:** Aborting taquito HTTP requests on app shutdown. ([be04b93](https://gitlab.com/tezos-dappetizer/dappetizer/commit/be04b93d21e6f274b7c814b7ff820a45d493b336))
* **indexer:** Added `loadDappetizerNetworkConfigs(...)` helper for loading network-specific part of `DappetizerConfig`. ([f3bc754](https://gitlab.com/tezos-dappetizer/dappetizer/commit/f3bc7547aac1e68cbdff356ff04ca064ee5e21ac))
* **indexer:** Once an IPFS request on one of configured gateway completes, then the same request to remaining gateways is aborted. ([d998dda](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d998ddada81643c82a16188eea151e970a1876c4))
* **database:** Added `resolveSchema(dataSourceOptions)` and `canContainSchema(dataSourceOptions)` helpers. ([2b4441c](https://gitlab.com/tezos-dappetizer/dappetizer/commit/2b4441c5fa58d52a1e52cd0361eb0539e90d8ca9))
* **database:** Added explicit check that the same instance of TypeORM NPM package is used by a module and Dappetizer. ([f9417e0](https://gitlab.com/tezos-dappetizer/dappetizer/commit/f9417e0f46e724eef0a4910d9a7ccf1fa0edb38d))
* **database:** Upgraded `typeorm` to `0.3.9` which uses `DataSource` instead of `Connection`. Therefore all usages are changed accordingly. ([e240990](https://gitlab.com/tezos-dappetizer/dappetizer/commit/e240990b518490f2835518e4332f9f99a9200570))
* **generator:** Added mapping of additional Tezos types: ([5c90cef](https://gitlab.com/tezos-dappetizer/dappetizer/commit/5c90cef30cd2e8e6d82a2141959117e1e7874aa1))
    * `bls12_381_fr` -> `string`.
    * `bls12_381_g1` -> `string`.
    * `bls12_381_g2` -> `string`.
    * `chest` -> `string`.
    * `chest_key` -> `string`.
    * `constant` -> Always throws because it should be expanded upfront.
    * `never` -> `never`.
    * `sapling_state` -> `string`.
* **generator:** Added support for `ticket` type. ([395f3a0](https://gitlab.com/tezos-dappetizer/dappetizer/commit/395f3a056b964092dd675c63f22797e1e2330ce4))
* **generator:** Added support for contracts which have only default entrypoint (no named entrypoints). ([886db91](https://gitlab.com/tezos-dappetizer/dappetizer/commit/886db9115ded9c2aeb6b74f1e0f2e98743486aaf))
* **generator:** Generating only imports that are actually used. ([0a69759](https://gitlab.com/tezos-dappetizer/dappetizer/commit/0a69759699a4fdb53bbb818be68b7a798b3d494a))
* **generator:** Generating dedicated Dappetizer JSON config for Tezos network e.g. `dappetizer.mainnet.config.json` so that generator can merge into it, it can be excluded from source control etc.
* **utils:** Added `@injectValue(...)` helper for Tsyringe. It should be also used to replace abstraction interfaces and related DI tokens. ([4c80af2](https://gitlab.com/tezos-dappetizer/dappetizer/commit/4c80af24b5bb8d23c3b111b746d1d3449fac03c4))
* **utils:** Added `JsonFetcher` which helps with HTTP requests to fetch a JSON. ([64730d0](https://gitlab.com/tezos-dappetizer/dappetizer/commit/64730d0d486f9274bdaf352599f28d869510766a))
* **utils:** Added explicit request info to `HttpRequestFailed` thrown by `@taquito`. ([10d0f64](https://gitlab.com/tezos-dappetizer/dappetizer/commit/10d0f64cb97924b0ad69e4ddf9263d933825b91b))
* **utils:** Added usage statistics. ([a11a511](https://gitlab.com/tezos-dappetizer/dappetizer/commit/a11a511005c6275da28a8d60301119d2e26f397e))
* Upgraded dependent NPM packages. ([5f0c060](https://gitlab.com/tezos-dappetizer/dappetizer/commit/5f0c06045a46bcc024db193836801ed2a1405643))
* Upgraded to `@taquito` 14.0.0. ([7f99bc1](https://gitlab.com/tezos-dappetizer/dappetizer/commit/7f99bc1c10bb64219be4434028128845db78bac1))


### BREAKING CHANGES

* **indexer:** Renamed `contract` of `BalanceUpdate` to `contractAddress`.
* **indexer:** Excluded specific properties from backtracked operation results as far as many of them did not make sense and used to throw an error e.g. `BacktrackedOriginationResult.originatedContract.getContract()`. ([2433851](https://gitlab.com/tezos-dappetizer/dappetizer/commit/24338510a9db566e4adfdd96af99cae2429eec83))
* **indexer:** Changed `AddressPrefix` to string union. ([3f20564](https://gitlab.com/tezos-dappetizer/dappetizer/commit/3f20564ddf012f4f69f1b2e8f6b52142a7911c6a))
* **indexer:** Changed `originatedContract` property of `AppliedOriginationResult` and `BacktrackedOriginationResult` to be non-nullable. ([b07e78d](https://gitlab.com/tezos-dappetizer/dappetizer/commit/b07e78d04e356ce0f2cc78d4e0ca7b008edd65ec))
* **indexer:** Refined union/alias types. ([f9d6b70](https://gitlab.com/tezos-dappetizer/dappetizer/commit/f9d6b70188a5e6d08ef02f9ea21146ca4ef263a0))
* **indexer:** Removed `RevealInternalOperation` as far as it is only present in RPC schema but does not exist on real blockchain. ([d05bfd6](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d05bfd6f6714db992e36b2eddfbb3559f6f51067))
* **indexer:** Removed `TransactionResult.originatedContract` as far as it cannot exist. It only exists in RPC schema. ([0028c22](https://gitlab.com/tezos-dappetizer/dappetizer/commit/0028c22cfcce8934bb1341b022b1a5dbc99cfb90))
* **utils:** Split `BaseLogger` class from form `Logger` which is now only an interface. ([a66e6c2](https://gitlab.com/tezos-dappetizer/dappetizer/commit/a66e6c2ff66eb4a0cedb12b6df77b739813a2b85))
* Changed DI tokens intended for extension of Dappetizer features to register functions in order to make the registration simpler and hide the token. For example: `HEALTH_CHECK_DI_TOKEN` is replaced with `registerHealthCheck(...)`. ([629761d](https://gitlab.com/tezos-dappetizer/dappetizer/commit/629761d18a0cc06c66ccba7df5b9be028f1d737e))
* Removed stuff from public API which was intended for internal usage.




## [1.3.1](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v1.3.0...v1.3.1) (2022-05-23)


### Bug Fixes

* **generator:** The generator was failing on a big map inside an entrypoint parameter. Now it generated it as `MichelsonMap` with values inlined. ([feb76af](https://gitlab.com/tezos-dappetizer/dappetizer/commit/feb76af8e9fa2cc5a5145c433f9432215a4e9853))





# [1.3.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v1.2.0...v1.3.0) (2022-04-28)


### Features

* **database:** Added possibility to hide autotracked entities and hide fields in Hasura. ([5976440](https://gitlab.com/tezos-dappetizer/dappetizer/commit/5976440811d5b195be619c3c677d43e6cf97c118))
* **database:** Added strict verification of `IndexerModule.dbEntities`. ([e0ea38e](https://gitlab.com/tezos-dappetizer/dappetizer/commit/e0ea38eb4c9faf82a05a2dc2e3f453abe89c30a5))
* **generator:** Command `dappetizer init` generates indexer module with name based on `name` from `package.json` if it already exists. ([28d6267](https://gitlab.com/tezos-dappetizer/dappetizer/commit/28d62675b20cbec45b555a35f10cba52142b80d0))
* **generator:** Installing same version of dependencies as used by Dappetizer. ([f5aee0a](https://gitlab.com/tezos-dappetizer/dappetizer/commit/f5aee0a481dc01a28c7990fa09c95fd201cff334))
* **indexer:** Added contract indexing boost using TzKT. ([9afad5f](https://gitlab.com/tezos-dappetizer/dappetizer/commit/9afad5f5f7ff39f4883f79bb04ff778c390e0b29))
* **indexer:** Changed default value of `indexing.headLevelOffset` from `0` to `1` because it is more useful for *Ithaca* protocol and later as far as there may be more blockchain reorganizations. ([1beaf7d](https://gitlab.com/tezos-dappetizer/dappetizer/commit/1beaf7d4a706cdfe9e87d03cf3be612aa4081cd3))
* **utils:** Added `keyof<T>(...)` helper function. It replaces deprecated `nameof()`. ([a28dbfe](https://gitlab.com/tezos-dappetizer/dappetizer/commit/a28dbfe0fb0c5510f0108df93ad38b865f2b8489))
* **utils:** Added custom `ConfigError` which is thrown by `ConfigElement` and exposes all source details. ([d3d3a7f](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d3d3a7fcd6c0b90f1415b561e8b43783f426b153))





# [1.2.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v1.1.1...v1.2.0) (2022-04-04)


### Features

* Added common interface `EntityToIndex<TRpcData>` for all entities to be indexed. ([e44ba32](https://gitlab.com/tezos-dappetizer/dappetizer/commit/e44ba329c718f56b6841b995ae46b5d8b5f503d1))
* Added Hasura integration. ([0cba4e0](https://gitlab.com/tezos-dappetizer/dappetizer/commit/0cba4e03b0260e2acad7f0924a9835fa6ddbc7cd))
* **generator:** Generating `contract` Michelson type as `string` which corresponds to Taquito `Schema.Execute()`. ([75a7fd2](https://gitlab.com/tezos-dappetizer/dappetizer/commit/75a7fd27c853b0db7b0599adc8ad40bdeda8e802))
* **indexer:** Added `uid` as universal ID of the entities on the blockchain, still human-readable. ([3deaba7](https://gitlab.com/tezos-dappetizer/dappetizer/commit/3deaba77796728256cc0a84aacce2aa100e95fd2))
* **indexer:** Added base interfaces with common properties: `BaseBigMapDiff`, `BaseInternalOperation<TRpcData>`, `BaseMainOperationWithResults<TRpcData>`, `BaseAppliedOperationResult<TRpcData>`, `BaseBacktrackedOperationResult<TRpcData>`. ([607f2a7](https://gitlab.com/tezos-dappetizer/dappetizer/commit/607f2a7e32ebb73e690528fe310b28b3fe1015a5))
* **indexer:** If big map ID is less than zero which represents a temporary big map, then we do not try to resolve a corresponding big map as far as it is always inaccessible. Therefore there will not be any related warning. ([fd8028e](https://gitlab.com/tezos-dappetizer/dappetizer/commit/fd8028e1083a4728e9472c21d3a73d862decb098))
* **indexer:** Upgraded API to Ithaca. Added: `DoublePreendorsementEvidenceOperation`, `PreendorsementOperation`, `SetDepositsLimitOperation`, `SetDepositsLimitInternalOperation`, new `BalanceUpdate`-s and some new properties on existing types. ([5d46103](https://gitlab.com/tezos-dappetizer/dappetizer/commit/5d46103ba4039b22bb9211a45d6aa66248159c39))
* Upgraded to [Taquito 12.0.0](https://github.com/ecadlabs/taquito/releases/tag/12.0.0). ([6c05091](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6c050918c63404ff9040fdb7cfeeb0395ab0f31f))
* **utils:** Increased default value of `longExecutionWarningMillis` in `time` config from 1 to 3 seconds. ([5c08edd](https://gitlab.com/tezos-dappetizer/dappetizer/commit/5c08edd0751bffd399b224c04a0670fc35f885d5))






## [1.1.1](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v1.1.0...v1.1.1) (2022-03-16)


### Bug Fixes

* **indexer:** Dappetizer failed to convert (prepare for indexing) the entire block if a transaction with a parameter was failed and no destination contract was resolved. Now it skips the transaction parameter. ([ae2d7c1](https://gitlab.com/tezos-dappetizer/dappetizer/commit/ae2d7c1baf0a03bab0c1c70660bec6e19908b14b))
* **utils:** Fixed: Config section `httpServer` was validated even if it is disabled (`enabled: false`). ([c785810](https://gitlab.com/tezos-dappetizer/dappetizer/commit/c785810efdc53ec309b786dbd0ed30fc3f977e6f))





# [1.1.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v1.0.0...v1.1.0) (2022-03-10)


### Bug Fixes

* **database:** Fixed version of `typeorm` dependency to be wildcard (with caret). ([32b01fa](https://gitlab.com/tezos-dappetizer/dappetizer/commit/32b01faee09c5f7d5876e4da9115edff919706a0))


### Features

* **indexer:** Added `RegisterGlobalConstantOperation` with related entities from Hangzhou protocol. ([91c3eb4](https://gitlab.com/tezos-dappetizer/dappetizer/commit/91c3eb4ff39b693368549e0495500afd30697bfd))





# [1.0.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v0.9.4...v1.0.0) (2022-03-08)


### Bug Fixes

* Header priority is now nullable for Ithacanet support. ([091e2c8](https://gitlab.com/tezos-dappetizer/dappetizer/commit/091e2c8108222a90a3e87bde2fab6b8f44931c5d))
* Allowed empty string for `block.header.fitness[number]` and similar cases. ([daeb82b](https://gitlab.com/tezos-dappetizer/dappetizer/commit/daeb82b8c69a4eeccfcd3a8f72f2ecfe3a81ffaf))
* If the configured `indexing.contracts` is an empty array, Dappetizer now performs selective indexing but with no contracts. ([2e76d5a](https://gitlab.com/tezos-dappetizer/dappetizer/commit/2e76d5a792c2f527feecca2c2f32ec20181411f5))
* If a big map value type is an `Option`, it was not possible to distinguish `None` from a removal of an entry. The value `undefined` now indicates a removal. ([cbbfd7f](https://gitlab.com/tezos-dappetizer/dappetizer/commit/cbbfd7f3f6277d69ab1468fdcd9d1d149a086fd5))


### chore

* Removed less used properties from `Block`, `BlockHeader`, `EndorsementOperation` and `DoubleEndorsementEvidenceOperation`. ([2f407e2](https://gitlab.com/tezos-dappetizer/dappetizer/commit/2f407e22d4644a71e0ff95ecce1e8c4dcbdf68a8))


### Code Refactoring

* Refactored `BigMapDiff` and `ContractIndexingContext`. ([528b897](https://gitlab.com/tezos-dappetizer/dappetizer/commit/528b897cdb4af66e247dbf3231e06078e81dde26))
* Removed support for implicit configs `dappetizer.local.config.ts`, `dappetizer.local.config.js` and `dappetizer.local.config.json`. ([82e1a36](https://gitlab.com/tezos-dappetizer/dappetizer/commit/82e1a36ad7db0b47b5ad57f28a71df4092f181c9))
* **utils:** Removed `guard`. ([d97bcc1](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d97bcc19dc4a9a48b2937dc4ae1eed923b5f98f8))


### Documentation

* Added type docs for `DappetizerConfig`. ([7da725c](https://gitlab.com/tezos-dappetizer/dappetizer/commit/7da725c4b6124912fd372f446b9ff19f5dc10dae))


### Features

* Added `Block.metadataBalanceUpdates` and indexing them. They correspond to balance updates from `metadata` property directly on block. ([49d883c](https://gitlab.com/tezos-dappetizer/dappetizer/commit/49d883c9a0235db8f1427c52f4cfb16475f5de96))
* Added `indexing.headLevelOffset` to account for many reorganization on HEAD block on Tenderbake protocols. ([e183fec](https://gitlab.com/tezos-dappetizer/dappetizer/commit/e183fec375d46492aaa2c9c2d091958f3931b737))
* Added `tezosNode.health.watcherCacheMillis` config value. ([a31e254](https://gitlab.com/tezos-dappetizer/dappetizer/commit/a31e2546416b3077c72ffcd114b972bf29413ffe))
* Added forward compatibility for new operations and other entity kinds in future protocols by skipping respective entities on conversion and indexing. They are still available in raw RPC data. ([8bcce43](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8bcce433414e2fff3509242a9f5ddf03777ad938))
* **decorators:** Added `@shouldIndex()` and `@contractFilter()` decorators. ([d1c7d23](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d1c7d23120e88200ea6f50cf8e7935ff028a0840))
* **decorators:** Added `@indexBigMapDiff()` decorator for a contract indexer class. ([fd26295](https://gitlab.com/tezos-dappetizer/dappetizer/commit/fd26295caf3ca7348edf54c986b53a930610c10c))
* **indexer:** Added optional `string` properties `ContractIndexer.name` and `BlockDataIndexer.name` used to easily identify the indexer, for example in error messages. ([d865c74](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d865c74673a38cf48630356d13e644e30afdd331))
* Logging the effective sanitized Dappetizer config (same as `/config` endpoint) on app startup instead of environment variables. ([b6fac3d](https://gitlab.com/tezos-dappetizer/dappetizer/commit/b6fac3d9f74436429f9edd2ac3b0c7aa41b7ae1c))
* Refined `ContractScript` in `OriginationOperation` for easier access and obtaining the converted initial storage. ([1d18703](https://gitlab.com/tezos-dappetizer/dappetizer/commit/1d18703998cc1d19b10de959e73aa50b0b077d0f))


### BREAKING CHANGES

* Removed less used properties from `Block`, `BlockHeader`, `EndorsementOperation` and `DoubleEndorsementEvidenceOperation`.
* **utils:** Removed `guard` and `PromiseForLater` from `@tezos-dappetizer/utils`.
* Properties `operationGroup`, `mainOperation`, and `operationWithBalanceUpdate` of `BalanceUpdateIndexingContext` are now nullable.
* Replaced `scripts: ContractScripts` with refined `script: ContractScript` on origination operation.
* Decorator `@indexOrigination()` passes converted initial storage as the first parameter instead of the origination operation, which can still be obtained from `indexingContext`.
* refactor: Removed support for implicit configs `dappetizer.local.config.ts`, `dappetizer.local.config.js` and `dappetizer.local.config.json`.
* Removed `health` config.
* Renamed nested types of `DappetizerConfig` and `DappetizerConfigUsingDb`.
* Changed `BigMapDiff` and `ContractIndexingContext` to contain already resolved data like `TransactionParameter` instead of `LazyTransactionParameter`. The reason is to make them easier to consume by indexer developers.





## [0.9.4](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v0.9.3...v0.9.4) (2022-02-17)


### Bug Fixes

* Fixed indexing on testnets ([d26486e](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d26486e7ef30a6d13ab78121af45c11740c17784))


### Features

* **generator:** Added support for `or` Michelson type generated as an interface with optional properties. ([b755541](https://gitlab.com/tezos-dappetizer/dappetizer/commit/b755541954cca4dd36794e8a4b5b835c95f86cf8))
* **generator:** Added support for `set` Michelson type. ([5b512a6](https://gitlab.com/tezos-dappetizer/dappetizer/commit/5b512a6396a1f4890548e8818c95548c2a0fac4f))
* **indexer:** Added retry of entire block indexing if it fails. It is configurable via: `retryDelaysMillis` and `retryIndefinitely` in `networks.*.indexing`. It replaces former `haltOnError`.



### BREAKING CHANGES

* Removed `haltOnError`; it is replaced by `retryIndefinitely`.
* Replaced `MichelsonSchema.extracted` with `MichelsonSchema.generated` based on the latest Taquito changes.





## [0.9.3](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v0.9.2...v0.9.3) (2022-02-10)


### Features

* Added Dockerfile verifier. ([2ecf861](https://gitlab.com/tezos-dappetizer/dappetizer/commit/2ecf861bdbaad16ab9354f9e922a105986bd989d))
* Added explicit `name` for `indexing.contracts` in config. Address is specified in `addresses` property. ([5a90377](https://gitlab.com/tezos-dappetizer/dappetizer/commit/5a90377c438465d81ec1821fe4b6cf3b696ac6dd))
* Added log file symlink ([32e7017](https://gitlab.com/tezos-dappetizer/dappetizer/commit/32e70175a493c5132c77e0d0ba13798f5297154c))


### BREAKING CHANGES

* Format of `indexing.contracts` in config is changed.






## [0.9.2](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v0.9.1...v0.9.2) (2022-02-07)


### Features

* **BREAKING CHANGE:** Merged indexer interfaces to simple `ContractIndexer` and `BlockDataIndexer`. ([fd6b5b6](https://gitlab.com/tezos-dappetizer/dappetizer/commit/fd6b5b630c616ffa24fc6b29bfceae16abe2670b))
* Added `@indexBigMapUpdate()` decorator. ([c003252](https://gitlab.com/tezos-dappetizer/dappetizer/commit/c00325246a229c7f4d43591e3a724c891645fdd9))
* Added `@indexOrigination()` decorator. ([20e8f03](https://gitlab.com/tezos-dappetizer/dappetizer/commit/20e8f03b742a3613a786bdb9b2fbf379da1ac584))
* Upgraded to [Taquito 11.2.0](https://github.com/ecadlabs/taquito/releases/tag/11.2.0) which fixes some issues related to Dappetizer.





## [0.9.1](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v0.9.0...v0.9.1) (2022-02-01)


### Features

* Generator generates strong types for indexing storage. ([75b4a5a](https://gitlab.com/tezos-dappetizer/dappetizer/commit/75b4a5a607d12002f93cffe24d1d16e72ed5c9fa))





# [0.9.0](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v0.0.9...v0.9.0) (2022-01-21)


### Bug Fixes

* `RootLogger.close()` does not flush pending log entries. ([643fd8b](https://gitlab.com/tezos-dappetizer/dappetizer/commit/643fd8ba98dfc32108d56897992f4900024e9ce1))
* Fixed typedoc artifacts. ([4590eb6](https://gitlab.com/tezos-dappetizer/dappetizer/commit/4590eb68d9c716a7eb7793e9f61bc52a8a703a5b))
* Fixed typedoc artifacts. ([f492ef7](https://gitlab.com/tezos-dappetizer/dappetizer/commit/f492ef7ed433adae19d56974c18f600247ffc837))
* Fixed typedoc artifacts. ([143e683](https://gitlab.com/tezos-dappetizer/dappetizer/commit/143e683ac2dac3ef49d6a6fb36eff127bfec39f6))
* Fixed typedoc artifacts. ([3526229](https://gitlab.com/tezos-dappetizer/dappetizer/commit/3526229adf5047ff28e55815c7a79b4727b294c2))


### Features

* Added `@indexEntrypoint(...)` TypeScript/JavaScript decorator for `ContractIndexer`-s. ([8d8bb48](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8d8bb487f91b9d6abb3b81ac46ab156d3751e146))
* Added `Dockerfile` to generated app. ([568a81b](https://gitlab.com/tezos-dappetizer/dappetizer/commit/568a81b7748e4e89c7aca532a727ed9c93da80b2))
* Explicit DI tokens for all exported services. ([805a459](https://gitlab.com/tezos-dappetizer/dappetizer/commit/805a4599148643f3ad79843dbf8a679610e73487))
* Changed constants (global variables with constant value during app lifetime) to upper-cased naming. ([ca504fe](https://gitlab.com/tezos-dappetizer/dappetizer/commit/ca504fef6f3c62ddf10a9707ee5d06e5a34481ac))
* Logging (as information) discovered DB entities from respective modules on app startup. ([034a116](https://gitlab.com/tezos-dappetizer/dappetizer/commit/034a116febd1b6fee8b707f5012ea37250ad13a3))
* Moved parts (`indexing`, `tezosNode`) of dappetizer config to network-specific node e.g. `networks.mainnet`. ([a4031f8](https://gitlab.com/tezos-dappetizer/dappetizer/commit/a4031f89502dbe5c011f8556531e42b4d1b1cf4c))
* Retrying of HTTP requests to GET a block by its level as same as failed requests. ([6b96073](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6b96073598c171b840f81d659a962937dddd237b))
* Simplified console messages logged by generator. ([410eec2](https://gitlab.com/tezos-dappetizer/dappetizer/commit/410eec2acf4727fc9c40382b628d2f658780390d))
* Support for additional Tezos types by Dappetizer generator. ([15baa3b](https://gitlab.com/tezos-dappetizer/dappetizer/commit/15baa3b5f6e99295d7baf5fc3b6f411a810cb2f9))
* Writing `critical`, `error` and `warning` log entries to stderr for console transport. Previously, everything went to stdout. ([b0a1604](https://gitlab.com/tezos-dappetizer/dappetizer/commit/b0a16044c360b3d32978de0d7a9b280bf7c37bd4))





## [0.0.9](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v0.0.8...v0.0.9) (2022-01-10)


### Bug Fixes

* Added transaction commit to roll-back. ([74e593c](https://gitlab.com/tezos-dappetizer/dappetizer/commit/74e593c6ec3a551f38e7a6563d6c42c5a188ccf3))
* Applied decimal hotfix. ([ecd9a7c](https://gitlab.com/tezos-dappetizer/dappetizer/commit/ecd9a7ccef02791d6789442288b89a77488b85e1))
* Fixed `ExtractedSchema` to include top-level string for simple schemas. ([50e79e8](https://gitlab.com/tezos-dappetizer/dappetizer/commit/50e79e8071d756ece53a8ae155e13f5d64996b85))
* Fixed code coverage. ([e87dced](https://gitlab.com/tezos-dappetizer/dappetizer/commit/e87dced18f3a360a50058ad2941787a42dfc073e))
* Set exact version of `colors` dependency because it was hacked (by author as a protest). ([c1f7aa9](https://gitlab.com/tezos-dappetizer/dappetizer/commit/c1f7aa991d2e35934f9322bef0e345061a51e5b1))


### Features

* Added `Applied<TOperation>` to indexing context where it already was. ([12eb05e](https://gitlab.com/tezos-dappetizer/dappetizer/commit/12eb05edc5a492ce13b7b5718ff2c85bc80c74e7))
* Added `BlockLevelIndexingListener` extension point to finish pending tasks when configured `toBlockLevel` is reached. ([66474eb](https://gitlab.com/tezos-dappetizer/dappetizer/commit/66474ebd9d0ecab594d1063ef88acade7d81c3a3))
* Added `DappetizerConfig` interface with corresponding unit tests. ([c20d453](https://gitlab.com/tezos-dappetizer/dappetizer/commit/c20d453a4ef1731e31479974a71236765a25f171))
* Added `DappetizerConfigUsingDb` interface with corresponding unit tests. ([63c2ac4](https://gitlab.com/tezos-dappetizer/dappetizer/commit/63c2ac4b3adeb6b8bf15c6981beec5c56aa5eeff))
* Added `toBlockLevel` feature. ([45b6ad5](https://gitlab.com/tezos-dappetizer/dappetizer/commit/45b6ad5586892457806cc27f8a32753c18339249))
* Added defaulting to the unknown type in catch variables. ([3df475e](https://gitlab.com/tezos-dappetizer/dappetizer/commit/3df475eed8180b5e3411aa2f8fd8fea582de520c))
* Added failing on very old protocols. ([ad98549](https://gitlab.com/tezos-dappetizer/dappetizer/commit/ad9854998bffef07ef7679558fd30a7461e01a01))
* Added generator for scaffolding a simple indexer built using Dappetizer. ([0854a0f](https://gitlab.com/tezos-dappetizer/dappetizer/commit/0854a0f0aa43d350b38aca9d84d77562de03e7e5))
* Added mandatory partial interfaces for indexer interfaces. ([22b5c8f](https://gitlab.com/tezos-dappetizer/dappetizer/commit/22b5c8fb52c41a24e33b3057407cea03981ad834))
* Added support for Dappetizer config written in TypeScript. ([8660fb2](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8660fb28dcadb200853d390e4b58477dede1d6c2))
* Changed `ContractIndexer.shouldIndex(...)` to accept `LazyContract` instead of `Contract` in order to be able to filter just based on the address. ([ec7a8aa](https://gitlab.com/tezos-dappetizer/dappetizer/commit/ec7a8aa3e1808b012860097d0bfc810bee72b07d))
* Included package versions in logs. ([4d11f8d](https://gitlab.com/tezos-dappetizer/dappetizer/commit/4d11f8dd16469c261455662af6b1860c52e1cf58))
* Upgraded to Taquito 11.0.2. ([50d1914](https://gitlab.com/tezos-dappetizer/dappetizer/commit/50d1914162923083e2e445899ab148e5f75b8bd9))





## [0.0.8](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v0.0.7...v0.0.8) (2021-11-16)


### Bug Fixes

* Fixed `critical` error on explicit app shutdown. ([4f0be3d](https://gitlab.com/tezos-dappetizer/dappetizer/commit/4f0be3d59ce4893f61ee0d91d7b716fc9f45f941))
* Fixed demos building and `private` so that they are not published. ([5fef239](https://gitlab.com/tezos-dappetizer/dappetizer/commit/5fef2399dc403e820e5a3224baac96972bac914e))
* Fixed deserialization of entrypoint explicitly called `default`. ([af6b9ec](https://gitlab.com/tezos-dappetizer/dappetizer/commit/af6b9ecf52eaba2aa36e37efa4bb25b3321e30bc))
* Fixed extraction of nested entrypoint by refactoring the code to use michelson. ([dd5517e](https://gitlab.com/tezos-dappetizer/dappetizer/commit/dd5517eadd289f6b8e27b663ce5be5c1991d2fba))
* Fixed indexing not being executed. ([4ae7876](https://gitlab.com/tezos-dappetizer/dappetizer/commit/4ae78761e74752fbd8c690aa22b771d7c8ff9f6d))
* Fixed missing configs in `/config` endpoint. ([40bd0bf](https://gitlab.com/tezos-dappetizer/dappetizer/commit/40bd0bfc2bef51cf1221f392a72db4e76386da9d))
* Improved (unrecoverable) error handling on app startup. ([92e5fbe](https://gitlab.com/tezos-dappetizer/dappetizer/commit/92e5fbe897b25bba6c81a69f889b15d8678a1782))
* Restored missing special race handling of IPFS gateway. ([f1de506](https://gitlab.com/tezos-dappetizer/dappetizer/commit/f1de5061c335b1babb5924c4c7d871a2abf7643f))


### Features

* Added `/config` diagnostic endpoint. ([359c435](https://gitlab.com/tezos-dappetizer/dappetizer/commit/359c435295b9eaa2dee80370f48896b071d3ea18))
* Added `ContextDataHandler.afterBlockIndexed()` extension point. ([06bd029](https://gitlab.com/tezos-dappetizer/dappetizer/commit/06bd029d3a8070dfd70a2c3e33dccd97120d169f))
* Added `DbInitializer` extension point. ([f2cb9cc](https://gitlab.com/tezos-dappetizer/dappetizer/commit/f2cb9ccf28082c5d241113875d8bbc38f8dad6f7))
* Added logs files rotating. ([03e8188](https://gitlab.com/tezos-dappetizer/dappetizer/commit/03e818885ae0b240949516f0159f20eaf1fb4d7f))
* Added long execution warning timeout configuration. ([876aeb3](https://gitlab.com/tezos-dappetizer/dappetizer/commit/876aeb3be71e92823c8bdecb3f7a1dd66ebf68f3))
* Added package version stamping. ([a5dc0ff](https://gitlab.com/tezos-dappetizer/dappetizer/commit/a5dc0ff66725e54be4d1d1ffa0a906cc78df0536))
* Added timestamp index. ([fb309c0](https://gitlab.com/tezos-dappetizer/dappetizer/commit/fb309c0893de6aa52d3f50116e20a1806fb059a7))
* Disabled http server by default. ([d4ce0c0](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d4ce0c0ed3670a47dbe5722399170675aa8c9e93))
* Changed extract of named entrypoint called via default to find the deepest one. ([4195169](https://gitlab.com/tezos-dappetizer/dappetizer/commit/41951697e31b4f13a44510f5fcdc0eacf9ed7514))
* Changed extraction of named entrypoint to consider all entrypoints -> `entrypointPath`. ([1f80bf7](https://gitlab.com/tezos-dappetizer/dappetizer/commit/1f80bf7fe22b6ee3f35aae95e1951e9dcad7c0f7))
* Reconnecting Tezos RPC monitor if there is no new item for long time. ([c8f752a](https://gitlab.com/tezos-dappetizer/dappetizer/commit/c8f752a4b78454d8cfdbd70d7e0a8cc1cc75a34c))
* Refactored enums. ([bf1dbd1](https://gitlab.com/tezos-dappetizer/dappetizer/commit/bf1dbd1a1f6f2f8af7601fdf2e3db31ffc7f816e))
* Refined DB initialization - logging about long exec, error handling. ([55a88ca](https://gitlab.com/tezos-dappetizer/dappetizer/commit/55a88ca5898dc13a0a4ea320c65c3557a307116e))
* Renamed timestamp to [@timestamp](https://gitlab.com/timestamp) on winston logger level. ([32d6c53](https://gitlab.com/tezos-dappetizer/dappetizer/commit/32d6c53ba29537729c3d059306717efded8ae546))
* Reverted [@timestamp](https://gitlab.com/timestamp) and logTime. ([31c50ed](https://gitlab.com/tezos-dappetizer/dappetizer/commit/31c50edcc767541fc82c574b93748b53be29f655))
* Strings in log entries are trimmed to configured `logging.maxLength`. ([e1c7b0a](https://gitlab.com/tezos-dappetizer/dappetizer/commit/e1c7b0a6e9524a1e3669a65b38825bc83dfc65f2))
* Support for aborting pending Taquito HTTP requests on app shutdown. ([f81645c](https://gitlab.com/tezos-dappetizer/dappetizer/commit/f81645cc2dc79a506aee02e29c72e6ebd805a62c))
* Using custom interfaces for block indexing. ([208e855](https://gitlab.com/tezos-dappetizer/dappetizer/commit/208e855874d62a7569a7661c55981ca1c5faf085))





## [0.0.7](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v0.0.5...v0.0.7) (2021-10-08)


### Features

* Added `indexBigMapUpdate()` and rename to `indexStorageChange()` on `ContractIndexer`. ([5777eda](https://gitlab.com/tezos-dappetizer/dappetizer/commit/5777edab6ef83bf99a305b099c2d94a95e45172e))
* Changed `indexBalanceUpdate()` to use more friendly `BalanceUpdate`. ([4dbdb37](https://gitlab.com/tezos-dappetizer/dappetizer/commit/4dbdb37c7be8df384fc0bf1762001ac254d0d934))






## [0.0.6](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v0.0.5...v0.0.6) (2021-10-07)


### Features

* Added `indexBigMapUpdate()` and rename to `indexStorageChange()` on `ContractIndexer`. ([5777eda](https://gitlab.com/tezos-dappetizer/dappetizer/commit/5777edab6ef83bf99a305b099c2d94a95e45172e))
* Changed `indexBalanceUpdate()` to use more friendly `BalanceUpdate`. ([4dbdb37](https://gitlab.com/tezos-dappetizer/dappetizer/commit/4dbdb37c7be8df384fc0bf1762001ac254d0d934))






## [0.0.5](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v0.0.4...v0.0.5) (2021-10-07)


### Bug Fixes

* Fixed some exports so that TS compilation for client is easier. ([a2d405e](https://gitlab.com/tezos-dappetizer/dappetizer/commit/a2d405e6f6f2e13ca61d42abe521f1f46f2d8d3c))


### Features

* Selective contract indexing using config: `indexing.contracts`. ([fcfd2f0](https://gitlab.com/tezos-dappetizer/dappetizer/commit/fcfd2f059e1c1ae2f684b02720e2f750d34cb390))






## [0.0.4](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v0.0.3...v0.0.4) (2021-10-01)


### Bug Fixes

* Fixed `dappetizer` CLI if config is not specified explicitly. ([6de4511](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6de451152b98ea5aecc2064d30c1d205b8d9bc47))
* Fixed `safeJsonStringify()` to consider `toJSON()`. ([6d6aad4](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6d6aad41c23d8452d49a7cb3e8cf72616d5acbeb))
* Fixed condition to check if some contract indexers are implemented. ([1520714](https://gitlab.com/tezos-dappetizer/dappetizer/commit/1520714c03eee901deb5d044d58873d4cdcb82d7))
* Fixed resolution of big maps if storage don't match - not using storage from current block because its input for origination hence misses big map ids. ([707e725](https://gitlab.com/tezos-dappetizer/dappetizer/commit/707e725c800d6da72243912ff8fc0978d77bfc50))
* Indexing only operation result details if its status is `applied`. ([6791901](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6791901aaed5f63054714e81f7ad868d3ad52170))
* Skipping bigmap diffs if ID is not found. ([d1ecd53](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d1ecd5358f44e05677bd8d66052eb2ce5f3fe915))
* Support for nullable entrypoint value. ([31d84fd](https://gitlab.com/tezos-dappetizer/dappetizer/commit/31d84fd7637a6a247a2e7537dde1e5d92a205ffa))


### Features

* Added `globalData` to all logged entries. ([b6b8961](https://gitlab.com/tezos-dappetizer/dappetizer/commit/b6b8961575de952e5051d9f9b93d6b00ccbe4821))
* Added arbitrary module configuration. ([6c69dbe](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6c69dbee4d3280f50d4fdf50676e51395645f08e))
* Added balance update indexing. ([ac2f0b4](https://gitlab.com/tezos-dappetizer/dappetizer/commit/ac2f0b47a7c6114b61da09042247d2159dc30b11))
* Added config for multiple IPFS gateways that race to get the response and configurable IPFS timeout. ([9e46f26](https://gitlab.com/tezos-dappetizer/dappetizer/commit/9e46f26ffed6556943f12a2ce2f9169c2626a515))
* Added correspodning resolved `BigMapInfo` to `BigMapDiff` instead of raw id. ([8c88f62](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8c88f62c120ee94b97059a1473824949a78ace2d))
* Added deserialized storage to respective indexer. ([926133a](https://gitlab.com/tezos-dappetizer/dappetizer/commit/926133a5c5f87559723699e1c96fb9ef88d3a413))
* Added entrypoint parameter also to `indexBigMapDiff()` and `indexStorage()`. ([af7d7f5](https://gitlab.com/tezos-dappetizer/dappetizer/commit/af7d7f59224b9a03fb80094b5800265dd2d1e899))
* Added check to verify that that same chain is indexed as it was used for already indexed data. ([ccd4978](https://gitlab.com/tezos-dappetizer/dappetizer/commit/ccd4978a18f05e6a5748b739542cd1e0438d5127))
* Added RPC operation and result to context of indexer interfaces. ([1536850](https://gitlab.com/tezos-dappetizer/dappetizer/commit/153685007f2cac31034f3059a256d774b0a19bf4))
* Added utilities for db naming. ([619d11b](https://gitlab.com/tezos-dappetizer/dappetizer/commit/619d11bcf58e536eb2148f1836b78a88b15276b1))
* Changed naming strategy to automatically calculate correct table names without `Db` prefix. ([fd61bb0](https://gitlab.com/tezos-dappetizer/dappetizer/commit/fd61bb0f496e80dbb4c570dafd8277470ed5f4d9))
* Removed numeric precision. ([8567bf8](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8567bf8c2e946efecb05f820727577d774e11f4b))
* Rename to [@tezos-dappetizer](https://gitlab.com/tezos-dappetizer). ([751553c](https://gitlab.com/tezos-dappetizer/dappetizer/commit/751553c9b285f35eeba040b81949bcaff9fd1f36))


### Reverts

* Revert "chore: Minor fix in CLI package.json" ([a3342d3](https://gitlab.com/tezos-dappetizer/dappetizer/commit/a3342d3ad622cde6c53be66d5816067bcba912dd))





## [0.0.4](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v0.0.3...v0.0.4) (2021-10-01)


### Bug Fixes

* Fixed `dappetizer` CLI if config is not specified explicitly. ([6de4511](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6de451152b98ea5aecc2064d30c1d205b8d9bc47))
* Fixed `safeJsonStringify()` to consider `toJSON()`. ([6d6aad4](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6d6aad41c23d8452d49a7cb3e8cf72616d5acbeb))
* Fixed condition to check if some contract indexers are implemented. ([1520714](https://gitlab.com/tezos-dappetizer/dappetizer/commit/1520714c03eee901deb5d044d58873d4cdcb82d7))
* Fixed resolution of big maps if storage don't match - not using storage from current block because its input for origination hence misses big map ids. ([707e725](https://gitlab.com/tezos-dappetizer/dappetizer/commit/707e725c800d6da72243912ff8fc0978d77bfc50))
* Indexing only operation result details if its status is `applied`. ([6791901](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6791901aaed5f63054714e81f7ad868d3ad52170))
* Skipping bigmap diffs if ID is not found. ([d1ecd53](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d1ecd5358f44e05677bd8d66052eb2ce5f3fe915))
* Support for nullable entrypoint value. ([31d84fd](https://gitlab.com/tezos-dappetizer/dappetizer/commit/31d84fd7637a6a247a2e7537dde1e5d92a205ffa))


### Features

* Added `globalData` to all logged entries. ([b6b8961](https://gitlab.com/tezos-dappetizer/dappetizer/commit/b6b8961575de952e5051d9f9b93d6b00ccbe4821))
* Added arbitrary module configuration. ([6c69dbe](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6c69dbee4d3280f50d4fdf50676e51395645f08e))
* Added balance update indexing. ([ac2f0b4](https://gitlab.com/tezos-dappetizer/dappetizer/commit/ac2f0b47a7c6114b61da09042247d2159dc30b11))
* Added config for multiple IPFS gateways that race to get the response and configurable IPFS timeout. ([9e46f26](https://gitlab.com/tezos-dappetizer/dappetizer/commit/9e46f26ffed6556943f12a2ce2f9169c2626a515))
* Added correspodning resolved `BigMapInfo` to `BigMapDiff` instead of raw id. ([8c88f62](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8c88f62c120ee94b97059a1473824949a78ace2d))
* Added deserialized storage to respective indexer. ([926133a](https://gitlab.com/tezos-dappetizer/dappetizer/commit/926133a5c5f87559723699e1c96fb9ef88d3a413))
* Added entrypoint parameter also to `indexBigMapDiff()` and `indexStorage()`. ([af7d7f5](https://gitlab.com/tezos-dappetizer/dappetizer/commit/af7d7f59224b9a03fb80094b5800265dd2d1e899))
* Added check to verify that that same chain is indexed as it was used for already indexed data. ([ccd4978](https://gitlab.com/tezos-dappetizer/dappetizer/commit/ccd4978a18f05e6a5748b739542cd1e0438d5127))
* Added RPC operation and result to context of indexer interfaces. ([1536850](https://gitlab.com/tezos-dappetizer/dappetizer/commit/153685007f2cac31034f3059a256d774b0a19bf4))
* Added utilities for db naming. ([619d11b](https://gitlab.com/tezos-dappetizer/dappetizer/commit/619d11bcf58e536eb2148f1836b78a88b15276b1))
* Changed naming strategy to automatically calculate correct table names without `Db` prefix. ([fd61bb0](https://gitlab.com/tezos-dappetizer/dappetizer/commit/fd61bb0f496e80dbb4c570dafd8277470ed5f4d9))
* Removed numeric precision. ([8567bf8](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8567bf8c2e946efecb05f820727577d774e11f4b))
* Rename to [@tezos-dappetizer](https://gitlab.com/tezos-dappetizer). ([751553c](https://gitlab.com/tezos-dappetizer/dappetizer/commit/751553c9b285f35eeba040b81949bcaff9fd1f36))


### Reverts

* Revert "chore: Minor fix in CLI package.json" ([a3342d3](https://gitlab.com/tezos-dappetizer/dappetizer/commit/a3342d3ad622cde6c53be66d5816067bcba912dd))





## [0.0.4](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v0.0.3...v0.0.4) (2021-10-01)


### Bug Fixes

* Fixed `dappetizer` CLI if config is not specified explicitly. ([6de4511](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6de451152b98ea5aecc2064d30c1d205b8d9bc47))
* Fixed `safeJsonStringify()` to consider `toJSON()`. ([6d6aad4](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6d6aad41c23d8452d49a7cb3e8cf72616d5acbeb))
* Fixed condition to check if some contract indexers are implemented. ([1520714](https://gitlab.com/tezos-dappetizer/dappetizer/commit/1520714c03eee901deb5d044d58873d4cdcb82d7))
* Fixed resolution of big maps if storage don't match - not using storage from current block because its input for origination hence misses big map ids. ([707e725](https://gitlab.com/tezos-dappetizer/dappetizer/commit/707e725c800d6da72243912ff8fc0978d77bfc50))
* Indexing only operation result details if its status is `applied`. ([6791901](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6791901aaed5f63054714e81f7ad868d3ad52170))
* Skipping bigmap diffs if ID is not found. ([d1ecd53](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d1ecd5358f44e05677bd8d66052eb2ce5f3fe915))
* Support for nullable entrypoint value. ([31d84fd](https://gitlab.com/tezos-dappetizer/dappetizer/commit/31d84fd7637a6a247a2e7537dde1e5d92a205ffa))


### Features

* Added `globalData` to all logged entries. ([b6b8961](https://gitlab.com/tezos-dappetizer/dappetizer/commit/b6b8961575de952e5051d9f9b93d6b00ccbe4821))
* Added arbitrary module configuration. ([6c69dbe](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6c69dbee4d3280f50d4fdf50676e51395645f08e))
* Added balance update indexing. ([ac2f0b4](https://gitlab.com/tezos-dappetizer/dappetizer/commit/ac2f0b47a7c6114b61da09042247d2159dc30b11))
* Added config for multiple IPFS gateways that race to get the response and configurable IPFS timeout. ([9e46f26](https://gitlab.com/tezos-dappetizer/dappetizer/commit/9e46f26ffed6556943f12a2ce2f9169c2626a515))
* Added correspodning resolved `BigMapInfo` to `BigMapDiff` instead of raw id. ([8c88f62](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8c88f62c120ee94b97059a1473824949a78ace2d))
* Added deserialized storage to respective indexer. ([926133a](https://gitlab.com/tezos-dappetizer/dappetizer/commit/926133a5c5f87559723699e1c96fb9ef88d3a413))
* Added entrypoint parameter also to `indexBigMapDiff()` and `indexStorage()`. ([af7d7f5](https://gitlab.com/tezos-dappetizer/dappetizer/commit/af7d7f59224b9a03fb80094b5800265dd2d1e899))
* Added check to verify that that same chain is indexed as it was used for already indexed data. ([ccd4978](https://gitlab.com/tezos-dappetizer/dappetizer/commit/ccd4978a18f05e6a5748b739542cd1e0438d5127))
* Added RPC operation and result to context of indexer interfaces. ([1536850](https://gitlab.com/tezos-dappetizer/dappetizer/commit/153685007f2cac31034f3059a256d774b0a19bf4))
* Added utilities for db naming. ([619d11b](https://gitlab.com/tezos-dappetizer/dappetizer/commit/619d11bcf58e536eb2148f1836b78a88b15276b1))
* Changed naming strategy to automatically calculate correct table names without `Db` prefix. ([fd61bb0](https://gitlab.com/tezos-dappetizer/dappetizer/commit/fd61bb0f496e80dbb4c570dafd8277470ed5f4d9))
* Removed numeric precision. ([8567bf8](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8567bf8c2e946efecb05f820727577d774e11f4b))
* Rename to [@tezos-dappetizer](https://gitlab.com/tezos-dappetizer). ([751553c](https://gitlab.com/tezos-dappetizer/dappetizer/commit/751553c9b285f35eeba040b81949bcaff9fd1f36))


### Reverts

* Revert "chore: Minor fix in CLI package.json" ([a3342d3](https://gitlab.com/tezos-dappetizer/dappetizer/commit/a3342d3ad622cde6c53be66d5816067bcba912dd))






## [0.0.4](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v0.0.3...v0.0.4) (2021-10-01)


### Bug Fixes

* Fixed `dappetizer` CLI if config is not specified explicitly. ([6de4511](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6de451152b98ea5aecc2064d30c1d205b8d9bc47))
* Fixed `safeJsonStringify()` to consider `toJSON()`. ([6d6aad4](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6d6aad41c23d8452d49a7cb3e8cf72616d5acbeb))
* Fixed condition to check if some contract indexers are implemented. ([1520714](https://gitlab.com/tezos-dappetizer/dappetizer/commit/1520714c03eee901deb5d044d58873d4cdcb82d7))
* Fixed resolution of big maps if storage don't match - not using storage from current block because its input for origination hence misses big map ids. ([707e725](https://gitlab.com/tezos-dappetizer/dappetizer/commit/707e725c800d6da72243912ff8fc0978d77bfc50))
* Indexing only operation result details if its status is `applied`. ([6791901](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6791901aaed5f63054714e81f7ad868d3ad52170))
* Skipping bigmap diffs if ID is not found. ([d1ecd53](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d1ecd5358f44e05677bd8d66052eb2ce5f3fe915))
* Support for nullable entrypoint value. ([31d84fd](https://gitlab.com/tezos-dappetizer/dappetizer/commit/31d84fd7637a6a247a2e7537dde1e5d92a205ffa))


### Features

* Added `globalData` to all logged entries. ([b6b8961](https://gitlab.com/tezos-dappetizer/dappetizer/commit/b6b8961575de952e5051d9f9b93d6b00ccbe4821))
* Added arbitrary module configuration. ([6c69dbe](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6c69dbee4d3280f50d4fdf50676e51395645f08e))
* Added balance update indexing. ([ac2f0b4](https://gitlab.com/tezos-dappetizer/dappetizer/commit/ac2f0b47a7c6114b61da09042247d2159dc30b11))
* Added config for multiple IPFS gateways that race to get the response and configurable IPFS timeout. ([9e46f26](https://gitlab.com/tezos-dappetizer/dappetizer/commit/9e46f26ffed6556943f12a2ce2f9169c2626a515))
* Added correspodning resolved `BigMapInfo` to `BigMapDiff` instead of raw id. ([8c88f62](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8c88f62c120ee94b97059a1473824949a78ace2d))
* Added deserialized storage to respective indexer. ([926133a](https://gitlab.com/tezos-dappetizer/dappetizer/commit/926133a5c5f87559723699e1c96fb9ef88d3a413))
* Added entrypoint parameter also to `indexBigMapDiff()` and `indexStorage()`. ([af7d7f5](https://gitlab.com/tezos-dappetizer/dappetizer/commit/af7d7f59224b9a03fb80094b5800265dd2d1e899))
* Added check to verify that that same chain is indexed as it was used for already indexed data. ([ccd4978](https://gitlab.com/tezos-dappetizer/dappetizer/commit/ccd4978a18f05e6a5748b739542cd1e0438d5127))
* Added RPC operation and result to context of indexer interfaces. ([1536850](https://gitlab.com/tezos-dappetizer/dappetizer/commit/153685007f2cac31034f3059a256d774b0a19bf4))
* Added utilities for db naming. ([619d11b](https://gitlab.com/tezos-dappetizer/dappetizer/commit/619d11bcf58e536eb2148f1836b78a88b15276b1))
* Changed naming strategy to automatically calculate correct table names without `Db` prefix. ([fd61bb0](https://gitlab.com/tezos-dappetizer/dappetizer/commit/fd61bb0f496e80dbb4c570dafd8277470ed5f4d9))
* Removed numeric precision. ([8567bf8](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8567bf8c2e946efecb05f820727577d774e11f4b))
* Rename to [@tezos-dappetizer](https://gitlab.com/tezos-dappetizer). ([751553c](https://gitlab.com/tezos-dappetizer/dappetizer/commit/751553c9b285f35eeba040b81949bcaff9fd1f36))


### Reverts

* Revert "chore: Minor fix in CLI package.json" ([a3342d3](https://gitlab.com/tezos-dappetizer/dappetizer/commit/a3342d3ad622cde6c53be66d5816067bcba912dd))






## [0.0.3](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v0.0.2...v0.0.3) (2021-09-08)


### Bug Fixes

* Fixed automatic prefixing of IPFS gateway by taquito with `https://`. ([0a0ca4c](https://gitlab.com/tezos-dappetizer/dappetizer/commit/0a0ca4c75b3cd7638c70f89f37fffd3d0db04075))
* Fixed big map indexing of big map instances that are removed and allocated again. ([6d5ac46](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6d5ac46714c51c017b6957e2361d9cf730a8941d))
* Fixed ci-build ([689791c](https://gitlab.com/tezos-dappetizer/dappetizer/commit/689791c66ea7192912ef0dfa35581e1413fd03bc))
* Fixed db connection - added explicit rollback and close. ([97f7ce5](https://gitlab.com/tezos-dappetizer/dappetizer/commit/97f7ce5257e0351061f02e3d2fdf1675d81b3ce9))
* Fixed group names, metric labels and semantic logging ([fec1c27](https://gitlab.com/tezos-dappetizer/dappetizer/commit/fec1c27ceade951e5645fc5bc809d366b9d526b8))
* Fixed halt on critical error message ([5885d0b](https://gitlab.com/tezos-dappetizer/dappetizer/commit/5885d0b46ff09f1991a506f60111d962e383f131))
* Fixed handling of SIGKILL signal. ([15adc91](https://gitlab.com/tezos-dappetizer/dappetizer/commit/15adc91bfffc0bd5972a85a9cba2497bec0e5edf))
* Fixed httpServer config to be optional. ([d0341aa](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d0341aa83922e496f73d13d19610077fb123a097))
* Fixed indexing if start block (from config or last from db) is just the next one from head. ([44ea994](https://gitlab.com/tezos-dappetizer/dappetizer/commit/44ea9940a6637e10983f8f88d7065e8dcd9ed081))
* Fixed logging ([4b26f7c](https://gitlab.com/tezos-dappetizer/dappetizer/commit/4b26f7c483924b4bb9e41e7018711735f70a1d99))
* Fixed logging of contract data so that useless `false` isn't logged because it collides in type on Elastic side. ([0842dfc](https://gitlab.com/tezos-dappetizer/dappetizer/commit/0842dfcb862ee78d4d5a18d2e61854c9f571173e))
* Fixed typings so that consumers don't need to install them if not needed. ([8eee990](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8eee990085a84fbd75d7b70582e74293068a50b2))
* Removed double slash in ipfs gateway url, added unit test, fixed cli.jso line endings. ([68b884d](https://gitlab.com/tezos-dappetizer/dappetizer/commit/68b884ddc59abffa3342da6f2b8422fd586fb69c))


### Features

* add duration logging to LoggingHttpBackend ([09fa547](https://gitlab.com/tezos-dappetizer/dappetizer/commit/09fa547398252233e6f373c7e36bc29b9efd485a))
* add performance section logging ([8778abe](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8778abeec301006057748a70d661399fdd5c3ad4))
* Added `formattedMessage` to JSON log. ([967c7b5](https://gitlab.com/tezos-dappetizer/dappetizer/commit/967c7b58569aaeb1af08893825e951e99ba987d0))
* Added `indexedContract` to scoped log data when contract details are being indexed. ([42d47a1](https://gitlab.com/tezos-dappetizer/dappetizer/commit/42d47a19001439e9e75a8fced5b446a92a6a4074))
* Added caching of contract objects accross modules. ([b0651a7](https://gitlab.com/tezos-dappetizer/dappetizer/commit/b0651a7ce3552ee43238c840fa30574e94f7c488))
* Added CLI to run [@tezos-dappetizer](https://gitlab.com/tezos-dappetizer) just from a config file. ([1010051](https://gitlab.com/tezos-dappetizer/dappetizer/commit/1010051dcf80776d6ebd7d22a830171fb50bd48e))
* Added database connect timeout configuration. ([1f5cf20](https://gitlab.com/tezos-dappetizer/dappetizer/commit/1f5cf20bbff6e3764386b92eb1c1e387da4f6b07))
* Added extensible API for metrics containers. ([b01a778](https://gitlab.com/tezos-dappetizer/dappetizer/commit/b01a77816e4ac1232946a89d5ec512abdb65ce73))
* Added grouping in retry metrics, made histograms buckets more convient ([2afc956](https://gitlab.com/tezos-dappetizer/dappetizer/commit/2afc95634dd4e3188580a1dfa5ef6a659d966ff7))
* Added human readable constraints names ([5794f60](https://gitlab.com/tezos-dappetizer/dappetizer/commit/5794f6054e998f6a80bb4080d11ad37f0268c128))
* Added human readable constraints names ([d0e7e69](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d0e7e69827880a0b61161a5b37f35841b7c6a116))
* Added packages versions http handler and tests ([57e2bbb](https://gitlab.com/tezos-dappetizer/dappetizer/commit/57e2bbb9d94b031114c1084da9bb5038328ae9ce))
* Added packages versions provider ([8b90208](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8b90208f09ff1086221fa6b9e83cbee5beda22c8))
* Added rpc block monitor metrics. ([e6f3387](https://gitlab.com/tezos-dappetizer/dappetizer/commit/e6f33878c838cc46cdb0398930432798e29266df))
* Added rpc requests metrics grouping ([3ebf615](https://gitlab.com/tezos-dappetizer/dappetizer/commit/3ebf615ee50f9b808bd2740ac2f3eb76324f9a2d))
* Added scoped logging using `logger.runWithData()`. ([0086bab](https://gitlab.com/tezos-dappetizer/dappetizer/commit/0086bab726a7da51644be9144bf5e1cefb86bcee))
* Added strict verification of client indexer modules. ([346cffa](https://gitlab.com/tezos-dappetizer/dappetizer/commit/346cffae0bb049650d8781530a9d438c6b831dce))
* Added support to run the indexer as web app. ([be39743](https://gitlab.com/tezos-dappetizer/dappetizer/commit/be397436476409ba3901cfb05ef75c21663464d9))
* Added warning about too long RPC and DB calls. ([1bdf012](https://gitlab.com/tezos-dappetizer/dappetizer/commit/1bdf01255bf995344a232585d80f7e1b8f8f620f))
* Changed configuration engine to use local JS/JSON file instead of environment variables. ([9c2c4fb](https://gitlab.com/tezos-dappetizer/dappetizer/commit/9c2c4fb8d0e27edf37d384ce52a7b4454b16cb37))
* Improved Utils API for running HTTP app. ([3ca2948](https://gitlab.com/tezos-dappetizer/dappetizer/commit/3ca294859812270cd110bc215a0f3925d1e1ce3d))
* Logging error that occur on app startup e.g. broken DI. ([6a4f8b8](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6a4f8b8b43785183e8dc49f84c91b7180138aac7))
* Made halt on critical error optional ([f08a248](https://gitlab.com/tezos-dappetizer/dappetizer/commit/f08a248aa894d7a2017f257e883cf82f8c24be00))
* Made IPFS gateway configurable. ([c140375](https://gitlab.com/tezos-dappetizer/dappetizer/commit/c140375d65185e219371979ba685c0125adb52dc))
* Moved responses from debug logging into trace. ([65ef9fe](https://gitlab.com/tezos-dappetizer/dappetizer/commit/65ef9fec82e519eb89526838f61d3e13b66ab7fe))
* Replaced DOM URL with npm url module. ([9958e73](https://gitlab.com/tezos-dappetizer/dappetizer/commit/9958e73bc3875799b9a9650c30aea6bdf5f9e529))


### Reverts

* Revert "chore: Created dummy change" ([9f09f39](https://gitlab.com/tezos-dappetizer/dappetizer/commit/9f09f392b6da5fe92c2bf74b29dc7ebbbd1e5665))






## [0.0.2](https://gitlab.com/tezos-dappetizer/dappetizer/compare/v0.0.1...v0.0.2) (2021-08-10)


### Features

* Changed data exposed to indexers to be immutable readonly. ([b777a27](https://gitlab.com/tezos-dappetizer/dappetizer/commit/b777a279ed1f34d9bb927d0c3eaf40c32a11302c))
* Changed db types from `char` to `character` so that they are compatible with SQLite. ([8dac4cb](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8dac4cb0f28153342215f7310783ad617fa6bd52))






## 0.0.1 (2021-08-03)


### Bug Fixes

* Fixed `isOfType()` which was sometimes breaking entire indexing. ([49e37d2](https://gitlab.com/tezos-dappetizer/dappetizer/commit/49e37d218f6444d7ba1a5baed56e823082c00db6))
* Fixed `safeJsonStringify()` if an object is repeated but not circular. ([8bb1001](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8bb100177ab86418080cef5b0edc6305210a3202))
* Fixed a memory leak in linked abortion signal. ([1edc9df](https://gitlab.com/tezos-dappetizer/dappetizer/commit/1edc9df6aacea2449411eefc0ec96dfab07c1a73))
* Fixed dependency injection of external classes (e.g. Taquito RpcClient) between packages. ([3976e9d](https://gitlab.com/tezos-dappetizer/dappetizer/commit/3976e9daeaffd89a9e505b1dc537b157e07b72b4))
* Fixed flatting of pairs to apply only for righ-combined. ([7f78f2c](https://gitlab.com/tezos-dappetizer/dappetizer/commit/7f78f2cbf43f121b1ce7b37821e56ee1260872b7))
* Fixed checking of instance types from external packages. ([aa641fd](https://gitlab.com/tezos-dappetizer/dappetizer/commit/aa641fda29fd7f47b55724f0f6a8b24a82082ca0))
* Fixed race condition in background worker startup = db connection vs indexing. ([bbcf628](https://gitlab.com/tezos-dappetizer/dappetizer/commit/bbcf628edba0d90b525a7f754e6767e40da0c634))
* **token-indexer:** Fixed matching of entrypoint names in indexers. ([54c5bbb](https://gitlab.com/tezos-dappetizer/dappetizer/commit/54c5bbb18f56e4d3c4d0015165c2256fe4f2f047))
* Support for empty mempool group from monitor because Tezos started sending an empty array on Granada. ([02dfd80](https://gitlab.com/tezos-dappetizer/dappetizer/commit/02dfd80f8d8c955e590ebfb54622a74a98804a21))


### Features

* Added `.env.local` overrides in demos. ([46844fd](https://gitlab.com/tezos-dappetizer/dappetizer/commit/46844fdd335fbeb88614e0ff1b8f32d40116921a))
* Added `coloredMessages` log format. ([742809a](https://gitlab.com/tezos-dappetizer/dappetizer/commit/742809a8fab5b037c407a1c145bb78dc332297d3))
* Added indexing of mint with token into. ([41c9925](https://gitlab.com/tezos-dappetizer/dappetizer/commit/41c99251a5b3e2838e3f19599e5e5096a5954527))
* Executing indexer modules in series. ([494904f](https://gitlab.com/tezos-dappetizer/dappetizer/commit/494904f244fe015136cf8f3e8ec736edcfdd1aba))
* Halting whole app when indexing fails on some block. ([3e17b59](https://gitlab.com/tezos-dappetizer/dappetizer/commit/3e17b597abb7cf30485e7646b43172953c029321))
* Improved ledger indexing to match it based on raw Michelson. ([0a55cb8](https://gitlab.com/tezos-dappetizer/dappetizer/commit/0a55cb85d7649fa60b3432cce92a8dc603557023))
* Re-implemented reorg to account for various node behaviors. ([4c494a5](https://gitlab.com/tezos-dappetizer/dappetizer/commit/4c494a58532bf75d2f9c207ce399a4b6431a97c0))
* Replaced `DbCommand`-s with multiple calls and open transaction using `QueryRunner`. ([e0001f5](https://gitlab.com/tezos-dappetizer/dappetizer/commit/e0001f5cf96f0f02e7340d15b8aab6601e224b00))
* Throwing an error if token resolution fails. Except not-found. ([659958f](https://gitlab.com/tezos-dappetizer/dappetizer/commit/659958fa847f8ad968e10b7dca145297adba258c))
* Turned on lerna packages publishing ([2d53fd0](https://gitlab.com/tezos-dappetizer/dappetizer/commit/2d53fd03abcb551908295afe75172c5acdf104ec))
* Turned on lerna packages publishing ([8538be8](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8538be87b87814d83efdaca3697e2b73f4337b76))
* Upgraded Taquito to 10.0.0. ([c33a2d6](https://gitlab.com/tezos-dappetizer/dappetizer/commit/c33a2d65e2b22b91e61e35336cb0af33d80dceb4))
* **token-indexer:** Added burn indexing. ([99a7f3d](https://gitlab.com/tezos-dappetizer/dappetizer/commit/99a7f3d079ea790ab710e90ccbe9580e78a46ddd))
* **token-indexer:** Added db relations. ([32f01ba](https://gitlab.com/tezos-dappetizer/dappetizer/commit/32f01ba0ddfa5ff15ee50773e2bf1790572eeb33))
* **token-indexer:** Added db relations. ([f29d3ea](https://gitlab.com/tezos-dappetizer/dappetizer/commit/f29d3ea890c1e7ba379410fb7061e922310182f5))
* **token-indexer:** Added db table `action` and sample indexing of transfers. ([1505075](https://gitlab.com/tezos-dappetizer/dappetizer/commit/1505075ee8ce5df90d66f1752d3820c218f2b65e))
* **token-indexer:** Added mint indexing. ([8d06a9d](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8d06a9d29c110cd3f26b03ddcac32ada8c1fb1bf))
* **token-indexer:** Added remaining tests and formats for ledger indexing. ([8da9ba7](https://gitlab.com/tezos-dappetizer/dappetizer/commit/8da9ba7c1dbc0b9992f713e46f9050d778ba3a43))
* **token-indexer:** Added reporting of incompatible contracts. It's configurable. ([30e4743](https://gitlab.com/tezos-dappetizer/dappetizer/commit/30e4743c9e40c5fb8fd92595ad9fc29f4e12f34b))
* **token-indexer:** Added token indexers to match more contracts. ([25679e5](https://gitlab.com/tezos-dappetizer/dappetizer/commit/25679e51472d6bc8f3cd5c2eaff58ebd9b5e2f3f))
* **token-indexer:** Added warning if ledger or entrypoint is found but no processor matches it. ([c8c586e](https://gitlab.com/tezos-dappetizer/dappetizer/commit/c8c586e4fdbc4744d8091087881ce7c8845d69bf))
* **token-indexer:** Implemented retry on Taquito HTTP backend. ([a07a1a4](https://gitlab.com/tezos-dappetizer/dappetizer/commit/a07a1a407ca068d9f5148658d345794813c8819a))
* **token-indexer:** Implemented retry on Taquito HTTP backend. ([c35779b](https://gitlab.com/tezos-dappetizer/dappetizer/commit/c35779b38e73b0c2d3c03e04d7b0bb64989a1a1c))
* **token-indexer:** Improved transferindexing to match it based on raw Michelson + related tests for actual contracts. ([453e506](https://gitlab.com/tezos-dappetizer/dappetizer/commit/453e5069347aca7b2fb1717adf154451c6372e88))
* **token-indexer:** Removed origination indexing as agreed. ([b90a4f9](https://gitlab.com/tezos-dappetizer/dappetizer/commit/b90a4f9fc4a7b94b500b1d1afb2a1081029c235f))
* Added `BackgroundWorkerExecutor`. ([70dce4b](https://gitlab.com/tezos-dappetizer/dappetizer/commit/70dce4beae0097f3316c5380b71596cdce3d4ec6))
* Added `DbSchemaInitializer`. ([cefccaf](https://gitlab.com/tezos-dappetizer/dappetizer/commit/cefccaf9d8246f6eb2e598ff5d7e776598aa222a))
* Added `MichelsonSchema`. ([c636633](https://gitlab.com/tezos-dappetizer/dappetizer/commit/c63663357f204dd0ff9eda54648819702f43762e))
* Added block queue to speed up indexing. ([37aeb3f](https://gitlab.com/tezos-dappetizer/dappetizer/commit/37aeb3f4e850006aca3e24e3f679ddd3f2c87a2c))
* Added deserialization of big map diffs. ([6745b20](https://gitlab.com/tezos-dappetizer/dappetizer/commit/6745b206bd3b5bac3da33a1280883c5dd270f3d5))
* Added deserialization of entrypoint parameters. ([d1b7186](https://gitlab.com/tezos-dappetizer/dappetizer/commit/d1b71867a29f8a913a1f1914e990ba6578bfc776))
* Added foreign key from custom entity to block. ([5316935](https://gitlab.com/tezos-dappetizer/dappetizer/commit/53169350cc6baf9a6bcc0c34c5a74a63c7f328e9))
* Added packages for storing indexed data to a database. ([23324aa](https://gitlab.com/tezos-dappetizer/dappetizer/commit/23324aa244488a35bdfb06aed3f92f555cd7648f))
* Added simple upsert command. ([1250001](https://gitlab.com/tezos-dappetizer/dappetizer/commit/1250001c22a89e104ef1234a2356ab8592aabfdb))
* Added token indexer. ([52be363](https://gitlab.com/tezos-dappetizer/dappetizer/commit/52be363ba1587f34ea33e82d19b0e8c7a89dbff0))
* Added token indexer. ([bfbea32](https://gitlab.com/tezos-dappetizer/dappetizer/commit/bfbea32afdc50f6369081178d00ac0fae7ce59c9))
* Added zero bootstrap. ([b77e788](https://gitlab.com/tezos-dappetizer/dappetizer/commit/b77e788586028dff167d844a33f30a6f1a9f3315))
* Errors from indexers are wrapped and the context info is added. ([439a39d](https://gitlab.com/tezos-dappetizer/dappetizer/commit/439a39d14881fc49008af4b0168291993cbb3723))
* Executing DB commands in a single call. ([4e0b778](https://gitlab.com/tezos-dappetizer/dappetizer/commit/4e0b77877a58cff11dd4cc9b807cb635ea7872f3))
* Extracted `IndexedBlockHandler` from `ContextDataHandler`. ([67e8ce1](https://gitlab.com/tezos-dappetizer/dappetizer/commit/67e8ce19a3cef9e17b7f0864dc6cc689f7c1f9ea))
* Improved `IntegerLimit` for env configs. ([1e6a418](https://gitlab.com/tezos-dappetizer/dappetizer/commit/1e6a418d10c76daf7bac4e969a7b17ae750f072e))
* Improved abortion in async workers. ([2ed840e](https://gitlab.com/tezos-dappetizer/dappetizer/commit/2ed840e26065aaeff325262487b31239cf2dbf52))
* Initial impl of indexer. ([dcd95d2](https://gitlab.com/tezos-dappetizer/dappetizer/commit/dcd95d23de91e5eef9e34b7b401fde0c6b3098d1))
* Initial indexer modules impl. ([2b81355](https://gitlab.com/tezos-dappetizer/dappetizer/commit/2b813552aed4b8602d91415ad9862f4b5d466046))
* Initial workspaces structure. ([619427d](https://gitlab.com/tezos-dappetizer/dappetizer/commit/619427d40422d3c790d2c4359315792b731a3b2f))
* Support for reorg. ([0aba6c7](https://gitlab.com/tezos-dappetizer/dappetizer/commit/0aba6c7734a4d07f70ebabe3f53a4ca69feaf3ad))
* Support for reorg. ([4019e7a](https://gitlab.com/tezos-dappetizer/dappetizer/commit/4019e7ad44c2c3c363470180f1d777d05eb1167c))
* Upgraded `taquito` 9.1.0 -> 9.2.0. ([535f7d2](https://gitlab.com/tezos-dappetizer/dappetizer/commit/535f7d2cbc6fd10afef956c30fd904c6b21f6dc9))


### Reverts

* Revert "chore:release" ([f44a3e7](https://gitlab.com/tezos-dappetizer/dappetizer/commit/f44a3e75353eebccd9a0a98561bd452d51ded54d))
